== For Translators ==

We are in the process of migrating translations to translate.wordpress.org.

https://translate.wordpress.org/projects/wp-plugins/bogo

The language files in this directory will no longer be updated and will be removed at some point in the future.

Thank you for your past contribution. See you at translate.wordpress.org :)
