<?php
/*
  Plugin Name: Google Maps Travel Route
  Description: With the Google Maps Travel Route plugin you can easily show a static route on Google Maps.
  Version: 1.3.1
  Author: traveller11
  Author URI: http://dev.egl.is/
  Plugin URI: http://dev.egl.is/
 */
 
global $gmr_is_script_included;
$gmr_is_script_included = FALSE;

require_once ('php/gmtr-shortcode.php');
require_once ('php/gmtr-widget.php');

define('GMTR_PLUGIN_URL', WP_PLUGIN_URL . '/' . dirname(plugin_basename(__FILE__)));
define('GMTR_PLUGIN_DIR', WP_PLUGIN_DIR . '/' . dirname(plugin_basename(__FILE__)));


add_action('admin_menu', 'gmtr_admin_menu');

/*
 * Administration Menu
 */

function gmtr_admin_menu() {
    add_object_page('Google Maps Travel Route', 'Travel Route', 'manage_options', 'gmtr-dashboard-page', 'page_dashboard', GMTR_PLUGIN_URL . '/images/google-marker-gray.png');
}

function page_dashboard() {
	
	// Load CSS
	wp_enqueue_style('gmtr-css', plugins_url('google-maps-travel-route/css/style.css'));

	$tabs = array( 'show-locations' => 'All Locations', 'add-location' => 'Add Location', 'shortcode-generator' => 'Shortcode', 'settings' => 'Settings' );
        $current = (isset($_GET['tab'])) ? $_GET['tab'] : 'show-locations';

	require 'views/dashboard.php';
}


register_activation_hook(__FILE__, 'gmtr_gmaps_install');

/**
 * @global <type> $wpdb
 * @global string $table_gmaps
 */
function gmtr_gmaps_install() {
    global $wpdb, $table_gmaps;
    $table_gmaps = $wpdb->base_prefix . 'gmtr_data';
    if (!empty($wpdb->charset))
        $charset_collate = "DEFAULT CHARACTER SET $wpdb->charset";
    if (!empty($wpdb->collate))
        $charset_collate .= " COLLATE $wpdb->collate";

    $sql = "CREATE TABLE {$table_gmaps} (
                            id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                            gid bigint(20),
                            orderid bigint(20),
                            `gmaps_address` longtext,
                            `gmaps_title` varchar(200),
                            `gmaps_description` longtext,
                            `gmaps_url` varchar(200) ,
                            `gmaps_lat_log` varchar(50),
                            `gmaps_icon` varchar(200),
                            `gmaps_infowindow_custom` text,
                            `created_date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
                            ) {$charset_collate};";
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql);
}

if ($_REQUEST['page'] === 'gmtr-dashboard-page' && is_admin()) {
    wp_enqueue_script('gmtr-gmaps-admin', GMTR_PLUGIN_URL . '/js/gmtr-admin.js', array('jquery','jquery-ui-core'));
}

// Set default marker icon
update_option('gmtr_default_marker_icon', GMTR_PLUGIN_URL . '/images/icons/default.png');

function gmtr_delete_gmaps() {
    global $wpdb, $table_gmaps;
    $table_gmaps = $wpdb->base_prefix . 'gmtr_data';
    $id = $_POST['id'];
    $sql = "DELETE FROM {$table_gmaps} WHERE id = {$id}";
    $sql_result = $wpdb->query($sql);
    if ($sql_result)
        echo 'true';
    die;
}

add_action('wp_ajax_gmtr_delete_gmaps', 'gmtr_delete_gmaps');

?>
