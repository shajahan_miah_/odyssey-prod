<?php

    global $wpdb, $table_gmaps;
    $table_gmaps = $wpdb->base_prefix . 'gmtr_data';
    if (isset($_POST['gmtr-save-gmaps'])) {

	$address = trim($_POST['gmtr-address']);
        $title = $_POST['gmtr-title'];
        $description = $_POST['gmtr-desc-gmaps'];
        $gmtr_url = $_POST['gmtr-url-gmaps'];
	$gmtr_info_custom = $_POST['gmtr-info-custom-gmaps'];
	$selection = $_POST['gmtr-selection-gmaps'];
	$selectedlatlong = 'gmtr-selection-latlong-'. $selection;
	$latlong = $_POST[$selectedlatlong];


	if (!empty($_FILES)) {

		$formOk = true;

		//Assign Variables
		$path = $_FILES['image']['tmp_name'];
		$iconType = $_FILES['image']['type'];

		if ($_FILES['image']['error'] || !is_uploaded_file($path)) {
			$formOk = false;
		}

		//check file extension
		if ($formOk && !in_array($iconType, array('image/png', 'image/x-png', 'image/jpeg', 'image/pjpeg', 'image/gif'))) {
			$formOk = false;
			echo '<div class="error">Error: Unsupported file extension. Supported extensions are JPG / PNG.</div>';
		}
		// check for file size.
		if ($formOk && filesize($path) > 500000) {
			$formOk = false;
			echo '<div class="error">Error: File size must be less than 500 KB.</div>';
		}

		if ($formOk) {
			if ( ! function_exists( 'wp_handle_upload' ) ) require_once( ABSPATH . 'wp-admin/includes/file.php' );
			$uploadedfile = $_FILES['image'];
			$upload_overrides = array( 'test_form' => false );
			$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );

			$upload_dir = wp_upload_dir();
			$path_parts = pathinfo($movefile['file']);
			$iconPath = $upload_dir['subdir'] .'/'. $path_parts['filename'] .'.'. $path_parts['extension'];
		}
	}

        if (!empty($latlong) && !empty($title)) {
		
            $result_id = $wpdb->insert(
                            $table_gmaps,
                            array(
                                'gmaps_address' => $address,
                                'gmaps_title' => $title,
                                'gmaps_description' => $description,
                                'gmaps_url' => $gmtr_url,
                                'gmaps_lat_log' => $latlong,
				'gmaps_icon' => $iconPath,
				'gmaps_infowindow_custom' => $gmtr_info_custom,
                            ),
                            array('%s', '%s', '%s', '%s', '%s', '%s', '%s'));
            if ($result_id) {
                echo '<div class="updated fade">Location Successfully Saved</div>';
		unset($address);
            } else {
                echo '<div class="error">Error Saving Data</div>';
            }
        } else {
            echo '<div class="error"><b>Please choose a location and add a title...</b></div>';
        }
    }

    if (isset($_POST['gmtr-search-gmaps'])) {
        $address = trim($_POST['gmtr-address']);
        $url = "http://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=" . urlencode($address);
		
	$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);
        $json = json_decode($output, true);
    }
?>
    <div class="wrap">
	<script type="text/javascript">
		window.onload = function(){
		  var text_input = document.getElementById ('gmtr-address');
		  text_input.focus();
		  text_input.select();
		}
	</script>

        <form method="post" name="gmtr-add-gmaps-form" enctype="multipart/form-data">
            <table class="form-table" border="0">
                <tr valign="top">
                    <td width="200">Address / Location</td>
                    <td>
			<input type="text" name="gmtr-address" id="gmtr-address" size="50" value="<?php echo $address;?>" onclick="this.select()">&nbsp;<input type="submit" value="Search Location" name="gmtr-search-gmaps" class="button-primary"><br />
			<span class="description">Hint : Submit the full location : number, street, city, country. For big cities and famous places, the country is optional. "Bastille Paris" or "Opera Sydney" will do.</span>
                    </td>
		</tr>
		<?php 
		     if(isset($_POST['gmtr-search-gmaps'])) {
		       if ($json['status'] == 'OK' ) { ?>
	  	     <tr>
			<td>Search Result</td>
			<td>
				<table class="widefat post">
		                  <thead>
		                    <tr style="background-color:#ddd;">
		                        <td>Select</td>
		                        <td>Location</td>
					<td></td>
					<td></td>
		                        <td></td>
		                    </tr>
		                  </thead>
		                  <tbody>        
				  <?php
					for ($i=0;$i<count($json['results']);$i++) {
        				      if ($i % 2 == 0) { echo '<tr class="gmtr-row alternate">'; }
					      else { echo '<tr class="gmtr-row">'; }

				              echo '<td><input type="radio" name="gmtr-selection-gmaps" value="'. $i .'" ';
					      if ($i == 0) { echo 'checked'; }
					      echo '>';

					      $latitude = $json['results'][$i]['geometry']['location']['lat'];
				              $longitude = $json['results'][$i]['geometry']['location']['lng']; 
					      $latlong = $latitude .','. $longitude;

				 	      echo '<input type="hidden" name="gmtr-selection-latlong-'. $i .'" value="'. $latlong .'">';
					      echo '</td>';
			        	      echo '<td>' . $json['results'][$i]['address_components'][0]['long_name'] . '</td>';
			        	      echo '<td>' . $json['results'][$i]['address_components'][1]['long_name'] . '</td>';
			        	      echo '<td>' . $json['results'][$i]['address_components'][2]['long_name'] . '</td>';
				              echo '<td>' . $json['results'][$i]['formatted_address'] . '</td>';
				              echo '</tr>';
					} // end for
				  ?>
			          </tbody>
			        </table>
			</td>
                </tr>
				<?php 
				  } else {
				    echo '<div class="error">Nothing found...</div>';
			          }} 
				?>
		<tr><td colspan="2"><hr size="1" style="margin:0px;"></td></tr>
                <tr valign="top">
                    <td>Title</td>
                    <td><input id="gmtr-title" type="text" size="67" name="gmtr-title" /></td>
                </tr>
                <tr valign="top">
                    <td>Description</td>
                    <td><textarea id="gmtr-desc-gmaps" name="gmtr-desc-gmaps" rows="3" cols="55"></textarea></td>
                </tr>
                <tr valign="top">
                    <td>URL</td>
                    <td><input type="text" value="" name="gmtr-url-gmaps" id="gmtr-url-gmaps" size="67"></td>
                </tr>
		<tr><td colspan="2"><hr size="1" style="margin:0px;"></td></tr>
                <tr valign="top">
                    <td>Custom HTML Code / Info Window<br><span class="description">Hint: If you add custom HTML code for the infowindow, Title, Description and URL will be ignored.</span></td>
                    <td><textarea id="gmtr-info-custom-gmaps" name="gmtr-info-custom-gmaps" rows="8" cols="55"></textarea></td>
                </tr>
		<tr><td colspan="2"><hr size="1" style="margin:0px;"></td></tr>
		<tr valign="top">
		    <td>Marker Icon</td>
		    <td>
			<input name="MAX_FILE_SIZE" value="102400" type="hidden">
			<input name="image" accept="image/*" type="file"></td>
		</tr>
                <tr valign="top">
                    <td></td>
                    <td><input type="submit" value="Save Location" name="gmtr-save-gmaps" class="button-primary"></td>
                </tr>
            </table>
        </form>
    </div>
