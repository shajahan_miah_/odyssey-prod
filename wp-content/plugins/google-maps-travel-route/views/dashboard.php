<div id="gmtr_admin" class="wrap">

	<?php
	    // Create tabs
    	    echo '<div id="icon-themes" class="icon32"><br></div>';
    	    echo '<h2 class="nav-tab-wrapper">';

            foreach( $tabs as $tab => $name ){
              $class = ( $tab == $current ) ? 'nav-tab-active' : '';
              echo '<a class="nav-tab '. $class .'" href="?page=gmtr-dashboard-page&tab='. $tab .'">'. $name .'</a>';
      	    }

    	    echo '</h2>';
	?>

        <div style="float:left; width:70%;"><br>
        <?php
	// Include the selected page
		switch ($current) {
  		  case 'add-location':
			require 'add-location.php';
        		break;
  		  case 'show-locations':
			require 'show-locations.php';
        		break;
  		  case 'edit-location':
			require 'edit-location.php';
        		break;
    		  case 'shortcode-generator':
			require 'shortcode-generator.php';
        		break;
    		  case 'settings':
			require 'settings.php';
        		break;
		}
	 ?>

          <p class="copyright-notice"></p>
        </div>
        <div style="width:25%; float:right; margin-left:3%; margin-top:27px; margin-right:20px;">

                <div class="box donatebox">
                        <h3>Donate $10, $20 or $50</h3>
                        <p>Hello! My name is Simon and I spent hours developing this plugin for <b>FREE</b>. If you like it, consider donating a small token of your appreciation. It is much appreciated!</p>
			<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank">
<input type="hidden" name="cmd" value="_donations">
<input type="hidden" name="business" value="simon@egl.is">
<input type="hidden" name="lc" value="CH">
<input type="hidden" name="item_name" value="Simon Egli">
<input type="hidden" name="currency_code" value="USD">
<input type="hidden" name="bn" value="PP-DonationsBF:btn_donateCC_LG.gif:NonHosted">
<input type="image" src="https://www.paypalobjects.com/en_US/CH/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>
                        <p>Alternatively, you can: </p>
            		<ul>
                	  <li><a target="_blank" href="http://wordpress.org/support/view/plugin-reviews/google-maps-travel-route?rate=5#postform">Give a 5&#9733; review on WordPress.org</a></li>
            		</ul>
        	</div>
	</div>
</div>
