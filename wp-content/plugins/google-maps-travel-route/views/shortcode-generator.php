<div class="wrap">
    <table class="form-table">
        <tr valign="top">
            <td>
		You can insert the map with the following shortcode into posts, pages etc:<br><br>
		[travelroute height="400" width="960"]<br><br>

		<b>Shortcode Parameters:</b><br>
		* height (height in pixel) (default: 360)<br>
		* width (width in pixel) (default: 640)<br>
		* maptype (roadmap, satellite, hybrid, terrain) (default: roadmap)<br>
		* maptypecontrol (true, false) (default: true)<br>
		* zoomcontrol (true, false) (default: true)<br>
		* maxzoom (0...20) (default: 17)<br>
		* minzoom (0...20) (default: 2)<br>
		* pancontrol (true, false) (default: false)<br>
		* streetviewcontrol (true, false) (default: false)<br>
		* draggable (true, false) (default: true)<br>
		* showmarker (true, false) (default: true)<br>
		* showinfowindow (true, false) (default: true)<br>
		* geodesic (true, false) (default: true)<br><br>

		<b>Examples:</b><br>
		[travelroute height="400" width="960" maptype="terrain" maptypecontrol="false" zoomcontrol="false"]<br>
            </td>
        </tr>
    </table>
</div>
