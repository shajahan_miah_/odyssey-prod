<?php

    global $wpdb;
    if (isset($_POST['gmtr-save-apikey']) == 'Save Key') {
        update_option('gmtr_gmaps_apikey', $_POST['gmtr-gmap-api-key']);
        update_option('gmtr_marker_color', $_POST['gmtr-marker-color']);
        update_option('gmtr_marker_width', $_POST['gmtr-marker-width']);
        update_option('gmtr_marker_transparent', $_POST['gmtr-marker-trans']);
        

        if (!empty($_FILES)) {

		 $formOk = true;

		 //Assign Variables
		 $path = $_FILES['image']['tmp_name'];
		 $iconType = $_FILES['image']['type'];

		 if ($_FILES['image']['error'] || !is_uploaded_file($path)) {
			$formOk = false;
		 }

		 //check file extension
		 if ($formOk && !in_array($iconType, array('image/png', 'image/x-png', 'image/jpeg', 'image/pjpeg', 'image/gif'))) {
			$formOk = false;
			echo '<div class="error">Error: Unsupported file extension. Supported extensions are JPG / PNG.</div>';
		 }
		  // check for file size.
		  if ($formOk && filesize($path) > 500000) {
			$formOk = false;
			echo '<div class="error">Error: File size must be less than 500 KB.</div>';
		  }

		  if ($formOk) {
			if ( ! function_exists( 'wp_handle_upload' ) ) require_once( ABSPATH . 'wp-admin/includes/file.php' );
			$uploadedfile = $_FILES['image'];
			$upload_overrides = array( 'test_form' => false );
			$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );

			$upload_dir = wp_upload_dir();
			$path_parts = pathinfo($movefile['file']);
			$iconPath = $upload_dir['subdir'] .'/'. $path_parts['filename'] .'.'. $path_parts['extension'];
		  
		  	update_option('gmtr_marker_icon', $iconPath);
		  }
		} 

		if (isset($_POST['gmtr-marker-reset'])) {
			update_option('gmtr_marker_icon', '');
		}
		        
        echo '<div class="updated fade">Settings Successfully Saved</div>';
    }
    
    $gmaps_api_key = get_option('gmtr_gmaps_apikey');
    $gmaps_marker_color = get_option('gmtr_marker_color');
    $gmaps_marker_width = get_option('gmtr_marker_width');
    $gmaps_marker_transparent = get_option('gmtr_marker_transparent');
    $gmaps_default_marker_icon = get_option('gmtr_default_marker_icon');
    $gmaps_marker_icon = get_option('gmtr_marker_icon');
    
?>

    <div class="wrap">
	<?php if (empty($gmaps_api_key)) { ?>
       		<div class="updated fade">If you don't have a Google Maps API Key, <a target="_blank" href="https://developers.google.com/maps/documentation/javascript/tutorial#api_key">click here</a>.</div>
    	<?php } ?>
    <form method="post" enctype="multipart/form-data">
        <table class="form-table">
            <tr valign="top">
                <td width="200">Enter Google Maps API Key</td>
                <td>
			<input id="gmtr-gmap-api-key" type="text" size="45" value="<?php echo $gmaps_api_key; ?>" name="gmtr-gmap-api-key" /><br>
			<span class="description"><a href="https://developers.google.com/maps/documentation/javascript/tutorial#api_key" target="_blank">Click here to get one...</a></span>
		</td>
            </tr>
            <tr valign="top">
                <td>Route Color</td>
                <td><input id="gmtr-marker-color" type="text" size="7" value="<?php echo $gmaps_marker_color; ?>" name="gmtr-marker-color" /><span class="description"> 6 digit hex color code. e.g.: #FFFFFF</span></td>
            </tr>
            <tr valign="top">
                <td>Route Width</td>
                <td><input id="gmtr-marker-width" type="text" size="7" value="<?php echo $gmaps_marker_width; ?>" name="gmtr-marker-width" /><span class="description"> Value between 1 to 10</span></td>
            <tr valign="top">
                <td>Route Transparency</td>
                <td><input id="gmtr-marker-trans" type="text" size="7" value="<?php echo $gmaps_marker_transparent; ?>" name="gmtr-marker-trans" /><span class="description"> Value between 0 to 1 including decimal value</span></td>
            </tr>
	    <tr><td colspan="2"><hr size="1" style="margin:0px;"></td></tr>
            <tr>
            	<td>Default Marker Icon</td>
            	<td>
            	<?php
            		
            		if (empty($gmaps_marker_icon)) {
            			echo '<img src="'. $gmaps_default_marker_icon .'" height="30"/>';
            		} else {
            			$uploadurl = wp_upload_dir();
						echo '<img src="'. $uploadurl['baseurl'] .'/'. $gmaps_marker_icon .'" height="30"/>';
            		}
            	?>
            	</td>
            </tr>
            <tr valign="top">
                <td></td>
                <td>
                    <input name="MAX_FILE_SIZE" value="102400" type="hidden">
                    <input name="image" accept="image/*" type="file">
				</td>
            </tr>
            <tr valign="top">
                <td></td>
                <td>
                	<input type="checkbox" name="gmtr-marker-reset" value="true"> Reset marker icon to default.
                </td>
            </tr>
            <tr valign="top">
                <td></td>
                <td><input type="submit" value="Save Settings" name="gmtr-save-apikey" class="button-primary" /></td>
            </tr>
	    <tr><td colspan="2"><br><br></td></tr>
	    <tr><td colspan="2"><hr size="1" style="margin:0px;"></td></tr>
            <tr>
                <td>Widget:</td>
		<td>Your theme should be Widget enabled.</span><br/>
                Go to Appearance > Widgets.</span><br/>
                Find Widget "Google Maps Travel Route". Drag and drop where you want it.</span><br/>
                </td>
            </tr>
        </table>
    </form>
    <br><br>
</div>
