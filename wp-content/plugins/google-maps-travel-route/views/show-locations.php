<div class ="wrap">

<?php

          global $wpdb, $table_gmaps;
    	  $table_gmaps = $wpdb->base_prefix . 'gmtr_data';	  
          $sql = "SELECT * from {$table_gmaps} ";
          $pagenum = isset($_GET['paged']) ? $_GET['paged'] : 1;
          $per_page = 20;
          $action_count = count($wpdb->get_results($sql));
          $total = ceil($action_count / $per_page);
          $action_offset = ($pagenum - 1) * $per_page;
          $page_links = paginate_links(array(
                    'base' => add_query_arg('paged', '%#%'),
                    'format' => '',
                    'prev_text' => __('&laquo;'),
                    'next_text' => __('&raquo;'),
                    'total' => ceil($action_count / $per_page),
                    'current' => $pagenum
	));

  	$sql .= " LIMIT {$action_offset}, {$per_page}";
 	$gmaps_ids = $wpdb->get_results($sql);

  	if (!empty($gmaps_ids)) {
    	  if ($page_links) { ?>
	    <div class="tablenav">
	      <div class="tablenav-pages">
	      <?php
		$page_links_text = sprintf('<span class="displaying-num">' . __('Displaying %s&#8211;%s of %s') . '</span> %s',
		  number_format_i18n(( $pagenum - 1 ) * $per_page + 1),
		  number_format_i18n(min($pagenum * $per_page, $action_count)),
		  number_format_i18n($action_count),
		  $page_links
		);
		echo $page_links_text;
	      ?>
	      </div>
	    </div>
        <?php
    	} // end if (page links)
       } // end if (empty)
?>

<div class="clear"></div>
<?php if (!empty($gmaps_ids)) { ?>

    	<table id="sort" class="widefat post fixed" cellspacing="0">
		<thead>
		    <tr style="background-color:#ddd;">
			<td class="check-column" scope="row"></td>
			<td>Location Address</td>
			<td>Title</td>
			<td>Description</td>
			<td width="30">URL</td>
			<td width="80">Date Created</td>
			<td width="80">Marker Icon</td>
		    </tr>
		</thead>
	<tbody>

<?php

    $i = 1;
    $count = (($pagenum-1)*$per_page)+1;
    foreach ($gmaps_ids as $gid) {

	if ($i % 2 == 0) {
	    echo '<tr class="alternate">';
	} else {
	    echo '<tr>';
	}
	
	echo '<td class="field_order">'. $count .'</td>';

	echo '<td>' . $gid->gmaps_address . '<p><a href="admin.php?page=gmtr-dashboard-page&tab=edit-location&action=edit&id=' . $gid->id . '">Edit</a>';
	echo ' | <a class="remove-admin-gmaps" style="cursor:pointer" rel="' . $gid->id . '">Delete</a></p></td>';
	echo '<td>' . strip_tags($gid->gmaps_title) . '</td>';

	echo '<td>' . strip_tags(substr($gid->gmaps_description, 0, 50)) . '</td>';

	echo '<td>';
		if (!empty($gid->gmaps_url)) {
			echo '<a href="'. $gid->gmaps_url .'" target="_blank"><img src="' . GMTR_PLUGIN_URL . '/images/link.png" width="20" height="20" title="'. $gid->gmaps_url .'"></a>';
		}
	echo '</td>';
	echo '<td>' . date('Y-m-d', strtotime($gid->created_date)) . '<br>' . date('H:i:s', strtotime($gid->created_date)) . '</td>';

		$gmaps_marker_icon = get_option('gmtr_marker_icon');
		
		if (!empty($gid->gmaps_icon)) {
			$uploadurl = wp_upload_dir();
			echo '<td><img src="'. $uploadurl['baseurl'] .'/'. $gid->gmaps_icon .'" height="30"/></td>';
		} elseif(!empty($gmaps_marker_icon)) {
			$uploadurl = wp_upload_dir();
			echo '<td><img src="'. $uploadurl['baseurl'] .'/'. $gmaps_marker_icon .'" height="30"/></td>';
		} else {
			$gmaps_default_marker_icon = get_option('gmtr_default_marker_icon');
			echo '<td><img src="'. $gmaps_default_marker_icon .'" height="30"/></td>';
		}
    
    	echo '</tr>';
    	$i++;
    	$count++;
    } // end foreach
?>
       		</tbody>

	</table><br>
	<a href="admin.php?page=gmtr-dashboard-page&tab=add-location"><input type="submit" value="Add Location" class="button-primary"></a>

        <?php } else {
            echo '<div class="error">No records found...</div>';
        } // end if empty 
	?>

</div><?php // end wrap ?>

