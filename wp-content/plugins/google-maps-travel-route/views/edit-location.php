<?php

    global $wpdb, $table_gmaps;
    $table_gmaps = $wpdb->base_prefix . 'gmtr_data';    

    if ($_REQUEST['page'] === 'gmtr-dashboard-page' && $_REQUEST['tab'] === 'edit-location' && $_REQUEST['action'] === 'edit') {
        if (isset($_POST['gmtr-update-gmaps'])) {
            $address = trim($_POST['gmtr-address']);
            $title = $_POST['gmtr-title'];
            $description = $_POST['gmtr-desc-gmaps'];
            $gmtr_url = $_POST['gmtr-url-gmaps'];
	    	$gmtr_info_custom = $_POST['gmtr-info-custom-gmaps'];
		
	    	$gmtr_icon = $_POST['gmtr-icon'];
        	$gmtr_icon_reset = $_POST['gmtr-icon-reset'];

        	if (!empty($_FILES)) {

                $formOk = true;

                //Assign Variables
                $path = $_FILES['image']['tmp_name'];
                $iconType = $_FILES['image']['type'];

                if ($_FILES['image']['error'] || !is_uploaded_file($path)) {
                        $formOk = false;
                }

                //check file extension
                if ($formOk && !in_array($iconType, array('image/png', 'image/x-png', 'image/jpeg', 'image/pjpeg', 'image/gif'))) {
                        $formOk = false;
                        echo '<div class="error">Error: Unsupported file extension. Supported extensions are JPG / PNG.</div>';
                }
                
                // check for file size.
                if ($formOk && filesize($path) > 500000) {
                        $formOk = false;
                        echo '<div class="error">Error: File size must be less than 500 KB.</div>';
                }

                if ($formOk) {
                        if ( ! function_exists( 'wp_handle_upload' ) ) require_once( ABSPATH . 'wp-admin/includes/file.php' );
                        $uploadedfile = $_FILES['image'];
                        $upload_overrides = array( 'test_form' => false );
                        $movefile = wp_handle_upload( $uploadedfile, $upload_overrides );

                        $upload_dir = wp_upload_dir();
                        $path_parts = pathinfo($movefile['file']);
                        $iconPath = $upload_dir['subdir'] .'/'. $path_parts['filename'] .'.'. $path_parts['extension'];
                }
        	}
			
			if ($_FILES['image']['error']) {
				if(!isset($gmtr_icon_reset)) {
					$iconPath = $gmtr_icon;
				}
			}

			$url = "http://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=" . urlencode($address);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$output = curl_exec($ch);
			curl_close($ch);
			$json = json_decode($output, true);

			if ($json['status'] == 'OK' ) {
				$latitude = $json['results'][0]['geometry']['location']['lat'];
				$longitude = $json['results'][0]['geometry']['location']['lng'];

				$result_id = $wpdb->update(
						$table_gmaps,
						array(
						   'gmaps_address' => $address,
						   'gmaps_title' => $title,
						   'gmaps_description' => $description,
						   'gmaps_url' => $gmtr_url,
						   'gmaps_icon' => $iconPath,
						   'gmaps_infowindow_custom' => $gmtr_info_custom,
						   'gmaps_lat_log' => $latitude . ',' . $longitude,
						),
						array('id' => $_GET['id']),
						array('%s', '%s', '%s', '%s', '%s', '%s', '%s'),
						array('%d'));
	  
					if ($result_id) {
						echo '<div class="updated fade">Location Successfully Edited</div>';
					} else {
						echo '<div class="error">Error Saving Data</div>';
					}         
			} else {      
				echo '<div class="error"><b>Http error ' . substr($data, 0, 3) . ' Error Saving Data</b></div>';
			}             
		}
		
		$id = $_GET['id'];
		$sql = "select * from {$table_gmaps} where `id`={$id}";
		$sql_result = $wpdb->get_row($sql, ARRAY_A);

		$sql1 = "select id from {$table_gmaps} where id = (select max(id) from {$table_gmaps} where id < {$id})";
		$previous = $wpdb->get_row($sql1, ARRAY_N);
		$sql2 = "select id from {$table_gmaps} where id = (select min(id) from {$table_gmaps} where id > {$id})";
		$next = $wpdb->get_row($sql2, ARRAY_N);

?>              
            
        <div class ="wrap">
        
            <form method="post" name="gmtr-edit-gmaps-form" enctype="multipart/form-data">
                <table class="form-table">
                    <tr valign="top">
                        <td width="200">Address</td>
                        <td>
				<input type="text" name="gmtr-address" id="gmtr-address" size="50" value="<?php echo $sql_result['gmaps_address'] ?>">&nbsp;&nbsp;&nbsp;&nbsp;
				<?php
					if(!empty($previous)) {
						echo '<a href="admin.php?page=gmtr-dashboard-page&tab=edit-location&action=edit&id='. $previous[0] .'">Previous</a>';
					} else {
						echo '&nbsp;&nbsp;&nbsp;...&nbsp;&nbsp;&nbsp;';
					}
					echo '&nbsp;&nbsp;|&nbsp;&nbsp;';
					if(!empty($next)) {
						echo '<a href="admin.php?page=gmtr-dashboard-page&tab=edit-location&action=edit&id='. $next[0] .'">Next</a>';
					} else {
						echo '&nbsp;&nbsp;&nbsp;...&nbsp;&nbsp;&nbsp;';
					}
				?>
			</td>
                    </tr>
		    <tr><td colspan="2"><hr size="1" style="margin:0px;"></td></tr>
                    <tr valign="top">
                        <td>Title</td>
                        <td><input type="text" size="67" name="gmtr-title" id="gmtr-title" value="<?php echo stripslashes($sql_result['gmaps_title']) ?>"></td>
                    </tr>
                    <tr valign="top">
                        <td>Description</td>
                        <td><textarea id="gmtr-desc-gmaps" name="gmtr-desc-gmaps" rows="3" cols="45"><?php echo stripslashes($sql_result['gmaps_description']) ?></textarea></td>
                    </tr>
                    <tr valign="top">
                        <td>URL</td>
                        <td><input type="text" value="<?php echo $sql_result['gmaps_url']; ?>" name="gmtr-url-gmaps" id="gmtr-url-gmaps" size="67"></td>
                    </tr>
		    <tr><td colspan="2"><hr size="1" style="margin:0px;"></td></tr>
		    <tr valign="top">
                        <td>Custom HTML Code / Info Window<br><span class="description">If you add custom HTML code for the infowindow, Title, Description and URL will be ignored.</span></td>
                        <td><textarea id="gmtr-info-custom-gmaps" name="gmtr-info-custom-gmaps" rows="8" cols="45"><?php echo str_replace('<br />', '<br />', stripslashes($sql_result['gmaps_infowindow_custom'])) ?></textarea></td>
                    </tr>
		    <tr><td colspan="2"><hr size="1" style="margin:0px;"></td></tr>
                    <tr valign="top">
	                <td>Current Marker Icon</td>
                    	<td>
			<?php
				$gmaps_marker_icon = get_option('gmtr_marker_icon');

				if (!empty($sql_result['gmaps_icon'])) {
        			        $uploadurl = wp_upload_dir();
			                echo '<img src="'. $uploadurl['baseurl'] .'/'. $sql_result['gmaps_icon'] .'" height="30"/>';
		                } elseif(!empty($gmaps_marker_icon)) {
					$uploadurl = wp_upload_dir();
					echo '<img src="'. $uploadurl['baseurl'] .'/'. $gmaps_marker_icon .'" height="30"/>';
				} else {
					$gmaps_default_marker_icon = get_option('gmtr_default_marker_icon');
					echo '<img src="'. $gmaps_default_marker_icon .'" height="30"/>';
				}
			?>
			</td>
		    </tr>
		    <tr valign="top">
	                <td>Upload Marker Icon</td>
		        <td>
                            <input name="MAX_FILE_SIZE" value="102400" type="hidden">
                            <input name="image" accept="image/*" type="file">
			</td>
                    </tr>
		    <tr valign="top">
	                <td>Reset to default marker icon</td>
		        <td>
				<input type="hidden" name="gmtr-icon" value="<?php echo $sql_result['gmaps_icon']; ?>">
                    		<input type="checkbox" name="gmtr-icon-reset" value="true">
			</td>
                    </tr>
                    <tr valign="top">
                        <td></td>
                        <td><input type="submit" value="Update Location" name="gmtr-update-gmaps" class="button-primary"></td>
                    </tr>
                </table>
            </form>

        </div>
<?php
    }
?>
