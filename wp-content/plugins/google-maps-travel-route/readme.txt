=== Plugin Name ===
Contributors: traveller11
Donate link: http://dev.egl.is/
Tags: google map trace, google map route, google maps, travel route, shortcode, maps, google, widget
Requires at least: 3.0
Tested up to: 3.8
Stable tag: 1.3.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

With the Google Maps Travel Route plugin you can easily show a static route on Google Maps.

== Description ==

Google Maps Travel Route enables you to easily insert Google Maps along with a travel route, tour, etc in your WordPress website. Via dashboard you can add,edit or delete locations or edit the options for the map itself. The map can be inserted in pages, post or sidebars via shortcode. A widget is also available. You will only need to have a valid Google Maps API key which you can get [here](https://developers.google.com/maps/documentation/javascript/tutorial#api_key "Obtaining an API Key").

This plugin is a new version of the outdated [Google Maps Route Plugin](http://wordpress.org/plugins/google-maps-route-plugin/ "Google Maps Route Plugin"). Thank you NetMadeEz for the initial version! 

You can find a demo [here](http://dev.egl.is/google-maps-travel-route/). And a lot more features are coming soon!! So stay tuned...

= Main Features: =

* Easily manage locations through Dashboard.
* Easily change the color, transparency and width of Route through Dashboard.
* Show map in your Post or Pages by simply adding Shortcode `[travelroute]`.
* Use different marker icons.
* Show map via Widget.

= Shortcode: =

[travelroute height="400" width="960"]

Shortcode Parameters:  
* height (height in pixel) (default: 360)  
* width (width in pixel) (default: 640)  
* maptype (roadmap, satellite, hybrid, terrain) (default: roadmap)  
* maptypecontrol (true, false) (default: true)  
* zoomcontrol (true, false) (default: true)  
* maxzoom (0...20) (default: 17)  
* minzoom (0...20) (default: 2)  
* pancontrol (true, false) (default: false)  
* streetviewcontrol (true, false) (default: false)  
* draggable (true, false) (default: true)  
* showmarker (true, false) (default: true)  
* showinfowindow (true, false) (default: true)  
* geodesic (true, false) (default: true)  
  
Examples:  
[travelroute height="400" width="960" maptype="terrain" maptypecontrol="false" zoomcontrol="false"]

= Coming soon... =
* Change the order of existing locations.
* Possibility to show multiple maps or multiple routes on the same map.

== Installation ==

1. Upload the Google Maps Travel Route folder to the `/wp-content/plugins/` directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. Create a Google Maps API key [here](https://developers.google.com/maps/documentation/javascript/tutorial#api_key "Obtaining an API Key").
4. Put your Google Maps API key in Travel Route > Settings and save it.
5. You can directly start using it by either using shortcode `[travelroute]` or widget.

== Frequently Asked Questions ==

= Where can I get a Google Maps API Key ? =

Create a Google Maps API key [here](https://developers.google.com/maps/documentation/javascript/tutorial#api_key "Obtaining an API Key").

= What unit do you expect the map width and height to be in ? =

Pixels are used to define the dimensions.

= How can I insert Google Maps Travel Route into my post/page ? =

By simply adding Shortcode `[travelroute]` in your post's /page's content.

= How will the route show on the Google maps ? =

Once you add a list of locations from Travel Route > Add New Location, the route will be automatically be mapped. You only have to put the locations address and optionally description.

= How do I define all the locations for my Google Maps Travel Route ? =

Through `Travel Route > Add New Location` you can “Add New Location” to the list of locations and view all your locations in Travel Route > List Locations.

== Screenshots ==

1. Sample Output of Google Maps Travel Route
2. Sample Output of Google Maps Travel Route
3. Sample Output of Google Maps Travel Route
4. Add a new location to the route
5. Location listing
6. Settings

== Changelog ==

= 1.3.1 =
* Add new icons

= 1.3 =
* Possibility to add a default marker
* Design update
* Little adjustments

= 1.2.1 =
* Replace file_get_contents() with curl.

= 1.2 =
* Design the info window with your own HTML code!

= 1.1 =
* Change menu structure
* Add shortcode help page
* Possibility to add custom marker icons

= 1.0.4 =
* Add more parameter to the shortcode [travelroute]

= 1.0.3 =
* Empty

= 1.0.2 =
* Update description
* Update and add screenshots
* Extend table scheme for new features

= 1.0.1 =
* Update description

= 1.0 =
* Trunk from outdated Google Maps Route Plugin
* Upgrade to Google Maps API v3
* Enhance the location finder
* Bugfixes

