<?php

add_shortcode('travelroute', 'google_map_travel_route_shortcode');

/**
 * Shortcode function
 */
function google_map_travel_route_shortcode($atts, $content=NULL) {
    if (get_option('gmtr_gmaps_apikey')) {
        global $wpdb, $table_gmaps, $gmr_is_script_included, $id;
        $table_gmaps = $wpdb->base_prefix . 'gmtr_data';
        $sql = "SELECT * from {$table_gmaps}";
        $data = $wpdb->get_results($sql, ARRAY_A);

        $gmr_id = 'gmr_id_'. $id .'_'. rand();
        extract(shortcode_atts( array(
                'height' => '360',
                'width' => '640',
                'maptype' => 'ROADMAP',
                'maptypecontrol' => 'true',
                'zoomcontrol' => 'true',
                'maxzoom' => '17',
                'minzoom' => '2',
                'pancontrol' => 'false',
                'streeviewcontrol' => 'false',
                'draggable' => 'true',
                'showmarker' => 'true',
				'showinfowindow' => 'true',
				'geodesic' => 'true'
                ), $atts));

        // String for output
        $op = '';

	// Fix
	$op.='<style type="text/css">#'. $gmr_id .' img { max-width:none; } </style>';
        $op.='<div id="' . $gmr_id . '" style="width:' . $width . 'px;height:' . $height . 'px"></div>';

        $op.='<script type="text/javascript">';
       	$op.='google.maps.visualRefresh = true;';

        $mapid = 'map_'. rand();
        $op.='var '. $mapid .';';
        //$op.='var iconBase = \''. GMTR_PLUGIN_URL .'/images/icons/\';';
        $op.='var bounds = new google.maps.LatLngBounds();';
        $op.='var routePath;';
        $op.='var infowindow = new google.maps.InfoWindow({});';

        $op.='function initialize() {';
            /**************************************
            *  General map options
            **************************************/
            $op.='var mapOptions = {';
                $op.='mapTypeId: google.maps.MapTypeId.'. strtoupper($maptype) .',';
                $op.='zoom: 0,';
                $op.='zoomControl: '. $zoomcontrol .',';
                $op.='zoomControlOptions: {';
                  $op.='style: google.maps.ZoomControlStyle.SMALL,';
                  $op.='position: google.maps.ControlPosition.LEFT_TOP';
                  $op.='},';
                $op.='maxZoom: '. $maxzoom .',';
                $op.='minZoom: '. $minzoom .',';
                $op.='mapTypeControl: '. $maptypecontrol .',';
                $op.='panControl: '. $pancontrol .',';
                $op.='streetViewControl: '. $streeviewcontrol .',';
                $op.='draggable: '. $draggable .',';
                $op.='center: new google.maps.LatLng("47.362645", "8.541784")';
            $op.='};';
	

	    // http://gmaps-samples-v3.googlecode.com/svn/trunk/styledmaps/wizard/index.html
	    $op.='var styles = [';
  	    $op.='{';
		$op.='featureType: "water",';
  		$op.='stylers: [';
  		$op.='{ hue: "#6E9AC4" },';
  		//$op.='{ color: "#FFFFFF" },';
  		$op.='{ saturation: -30 },';
  		$op.='{ gamma: 0.31 },';
		$op.='{ lightness: +30 },';
		$op.='{ visibility: "simplified" }';
  		$op.=']';
  	    $op.='},';
  	    $op.='{';
		$op.='featureType: "landscape",';
  		$op.='stylers: [';
  		$op.='{ hue: "#0866BF" },';
  		$op.='{ saturation: 10 },';
  		$op.='{ gamma: 0.91 },';
		$op.='{ lightness: +30 },';
		$op.='{ visibility: "simplified" }';
  		$op.=']';
  	    $op.='},';
  	    $op.='{';
		$op.='featureType: "road",';
  		$op.='stylers: [';
		$op.='{ visibility: "off" }';
  		$op.=']';
  	    $op.='},';
  	    $op.='{';
		$op.='featureType: "poi",';
  		$op.='stylers: [';
		$op.='{ visibility: "off" }';
  		$op.=']';
  	    $op.='}';
  	    $op.='];';

        // Create Google Map Object 
        $op.=''. $mapid .' = new google.maps.Map(document.getElementById("'. $gmr_id .'"), mapOptions);';
	    // Add map styles
	    //$op.=''. $mapid .'.setOptions({styles: styles});';

            /**************************************
            *  Line symbol - used for animation
            **************************************/
            $op.='var lineSymbol = {';
                //$op.='path: \'M 0,-1 0,1\',';
                $op.='path: google.maps.SymbolPath.CIRCLE,';
                $op.='scale: 2,';
                $op.='strokeColor: \'#fff\',';
                $op.='strokeOpacity: 1';
            $op.='};';

            /**************************************
            *  Polyline options
            **************************************/
            $op.='routePath = new google.maps.Polyline({';
                $op.='strokeColor: "'. get_option('gmtr_marker_color') .'",';
                $op.='strokeOpacity: '. get_option('gmtr_marker_transparent') .',';
                $op.='strokeWeight: '. get_option('gmtr_marker_width') .',';
                $op.='clickable: false,';
                // Coming soon...
                //$op.='icons: [{ icon: lineSymbol, offset: \'0\', repeat: \'20px\'}],';
                //$op.='icons: [{ icon: lineSymbol, offset: \'100%\'}],';
                $op.='map: '. $mapid .',';
                $op.='geodesic: '. $geodesic .'';
            $op.='});';

            /**************************************
            *  Create waypoint array
            **************************************/
            $op.='var pathCoordinates = new Array();';
            for($i=0;$i<count($data);$i++) {
                $value = explode(',', $data[$i]['gmaps_lat_log']);
                $op.='pathCoordinates.push(new google.maps.LatLng("'.$value[0].'", "'.$value[1].'"));';
            }

            // Add pathCoordinates to map
            $op.='routePath.setPath(pathCoordinates);';

            // Add pathCoordinates to google.maps.LatLngBounds
            $op.='for (var i = 0; i < pathCoordinates.length; i++) {';
            $op.='bounds.extend(pathCoordinates[i]);';
            $op.='}';

            /**************************************
            *  Create markers
            **************************************/
            if ($showmarker == 'true') {
              for($i=0;$i<count($data);$i++) {
                $value = explode(',', $data[$i]['gmaps_lat_log']);
                $op.='var marker'. $i .' = new google.maps.Marker({';
                $op.='position: new google.maps.LatLng("'.$value[0].'", "'.$value[1].'"),';
                $op.='title: \''. $data[$i]['gmaps_title'] .'\',';

			$gmaps_marker_icon = get_option('gmtr_marker_icon');
			$gmaps_default_marker_icon = get_option('gmtr_default_marker_icon');
			$uploadurl = wp_upload_dir();

		    if (!empty($data[$i]['gmaps_icon'])) {
		        $op.='icon: \''. $uploadurl['baseurl'] .'/'. $data[$i]['gmaps_icon'] .'\',';
		    } else if (!empty($gmaps_marker_icon)) {
	            $op.='icon: \''. $uploadurl['baseurl'] .'/'. $gmaps_marker_icon .'\',';
		    } else {
		    	$op.='icon: \''. $gmaps_default_marker_icon .'\',';
		    }

            //$op.='animation: google.maps.Animation.DROP,';
            $op.='map: '. $mapid .'';
            $op.='});';

        // Add event listener for popup infobox
		if ($showinfowindow == 'true') {
                $op.='google.maps.event.addListener(marker'.$i.', \'click\', function() {';

		    // Custom info window
		    if (!empty($data[$i]['gmaps_infowindow_custom'])) {
                      $op.='var content = \'';
				$breaks = array("\r\n", "\n", "\r");
    				$infowindow = str_replace($breaks, "", $data[$i]['gmaps_infowindow_custom']);
				$op.=''.nl2br(stripslashes($infowindow)) .'\';';
		    } else {
		    // Default info window with title, description and url
                      $op.='var content = \'<b>'. $data[$i]['gmaps_title'] .'</b>\'';
                                if($data[$i]['gmaps_description'] != '') {
                                    $op.='+\'<br>'. $data[$i]['gmaps_description'] .'\'';
                                }
                                if($data[$i]['gmaps_url'] != '') {
                                    $op.='+\'<br><a href="'. $data[$i]['gmaps_url'] .'" target="_blank">'. $data[$i]['gmaps_url'] .'</a>\'';
                                }
                      $op.=';';
		    }

                    $op.='infowindow.setContent(content);';
                    $op.='infowindow.open('. $mapid .', marker'. $i .' );});';
		}
              } // end for
            } // end show marker

            // Auto zoom, auto center
            if(!empty($data)) {
                $op.=''. $mapid .'.fitBounds(bounds);';
                $op.=''. $mapid .'.panToBounds(bounds);';

                // Because panToBounds is a bit too close, we will zoom out...
                $op.='zoomChangeBoundsListener = google.maps.event.addListenerOnce('. $mapid .', \'bounds_changed\', function(event) {';
                $op.='if (this.getZoom()){ this.setZoom('. $mapid .'.getZoom());}});';
                $op.='setTimeout(function(){google.maps.event.removeListener(zoomChangeBoundsListener)}, 2000);';
            }

            // Run animation 
            $op.='animateLine();';
        $op.='}';

        $op.='function animateLine() {';
                $op.='var count = 0;';
                $op.='timer = window.setInterval(function() {';
                $op.='count = (count + 1) % 200;';
                $op.='var icons = routePath.get(\'icons\');';
                $op.='icons[0].offset = (count / 2) + \'%\';';
                $op.='routePath.set(\'icons\', icons);';
                // Coming soon...
                // Examples1: http://stackoverflow.com/questions/16800865/animate-google-maps-polyline
                // Examples2: http://planefinder.net/route/SFO/
                $op.='}, 100);';
                $op.='';
        $op.='}';

        $op.='google.maps.event.addDomListener(window, \'load\', initialize);';
        $op.='</script>';

        //if (!$gmr_is_script_included) {
        if (true) {
            $content .= '<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=' . get_option('gmtr_gmaps_apikey') . '&sensor=false"></script>';
            $gmr_is_script_included = true;
        }
        $content.= $op;
    }
    return $content;
}

?>
