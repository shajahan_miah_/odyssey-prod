<?php
/*
 * Add Google Maps Travel Route Location Page
 */

function gmtr_gmaps_page() {
    global $wpdb, $table_gmaps;
    $table_gmaps = $wpdb->base_prefix . 'gmtr_data';
    if (isset($_POST['gmtr-save-gmaps'])) {
	$address = trim($_POST['gmtr-address']);
        $title = $_POST['gmtr-title'];
        $description = $_POST['gmtr-desc-gmaps'];
        $gmtr_url = $_POST['gmtr-url-gmaps'];
	$selection = $_POST['gmtr-selection-gmaps'];

        $url = "http://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=" . urlencode($address);
	$request = file_get_contents($url);
	$json = json_decode($request, true);

        if ($json['status'] == 'OK' ) {
	    $latitude = $json['results'][$selection]['geometry']['location']['lat'];
	    $longitude = $json['results'][$selection]['geometry']['location']['lng'];
		
            $result_id = $wpdb->insert(
                            $table_gmaps,
                            array(
                                'gmaps_address' => $address,
                                'gmaps_title' => $title,
                                'gmaps_description' => $description,
                                'gmaps_url' => $gmtr_url,
                                'gmaps_lat_log' => $latitude . ',' . $longitude,
                            ),
                            array('%s', '%s', '%s', '%s', '%s'));
            if ($result_id) {
                echo '<div class="updated fade">Location Successfully Saved !!!</div>';
		unset($address);
            } else {
                echo '<div class="error">Error Saving Data</div>';
            }
        } else {
            echo '<div class="error"><b>Http error ' . substr($data, 0, 3) . ' Error Saving Data</b></div>';
        }
    }

    if (isset($_POST['gmtr-search-gmaps'])) {
        $address = trim($_POST['gmtr-address']);
        $url = "http://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=" . urlencode($address);
        $request = file_get_contents($url);
        $json = json_decode($request, true);
    }
?>
    <div class="wrap">
	<script type="text/javascript">
		window.onload = function(){
		  var text_input = document.getElementById ('gmtr-address');
		  text_input.focus();
		  text_input.select();
	}
	</script>
        <h2>Add Location</h2>
        <form method="post" name="gmtr-add-gmaps-form">
            <table class="form-table" border="0">
                <tr valign="top">
                    <th width="120" scope="row"><label for="gmtr-address">Address / Location</label></th>
                    <td><input type="text" name="gmtr-address" id="gmtr-address" size="62" value="<?php echo $address;?>" onclick="this.select()">&nbsp;<input type="submit" value="Search Location" name="gmtr-search-gmaps" class="button-primary"><br /><span class="description">Hint : Submit the full location : number, street, city, country. For big cities and famous places, the country is optional. "Bastille Paris" or "Opera Sydney" will do.</span>
                    </td>
		</tr>
		<tr>
			<th>Search Result</th>
			<td>
			<?php if(isset($_POST['gmtr-search-gmaps'])) {
				if ($json['status'] == 'OK' ) { 
			?>
		            <table class="widefat post fixed" cellspacing="0">
		                <thead>
		                    <tr>
		                        <th class="check-column" scope="row">&nbsp;&nbsp;Nr.</th>
		                        <th>Select</th>
		                        <th>Location</th>
					<th></th>
					<th></th>
		                        <th class="column-title"></th>
		                    </tr>
		                </thead>
		                <tbody>        
				<?php
					for ($i=0;$i<count($json['results']);$i++) {
	
        				        if ($i % 2 == 0) {
		                		    echo '<tr class="alternate">';
			                	} else {
				                    echo '<tr>';
				                }
				                echo '<td class="check-column">&nbsp;&nbsp;&nbsp;'. $i .'</td>';
				                echo '<td><input type="radio" name="gmtr-selection-gmaps" value="'. $i .'" ';
						if ($i == 0) { echo 'checked'; }
						echo '></td>';
			        	        echo '<td>' . $json['results'][$i]['address_components'][0]['long_name'] . '</td>';
			        	        echo '<td>' . $json['results'][$i]['address_components'][1]['long_name'] . '</td>';
			        	        echo '<td>' . $json['results'][$i]['address_components'][2]['long_name'] . '</td>';
				                echo '<td>' . $json['results'][$i]['formatted_address'] . '</td>';
				                echo '</tr>';
					}
				?>
			       </tbody>
			    </table>
				<?php 
					} else {
				            echo '<b>Nothing found...</b>';
				        }} 
				?>
			</td>
                </tr>
		<tr height="20"><td>&nbsp;</td></tr>
                <tr valign="top">
                    <th scope="row"><label for="gmtr-title">Title</label></th>
                    <td><input id="gmtr-title" type="text" size="62" name="gmtr-title" /></td>
                </tr>
                <tr valign="top">
                    <th><label for="gmtr-desc-gmaps">Description</label></th>
                    <td><textarea id="gmtr-desc-gmaps" name="gmtr-desc-gmaps" rows="5" cols="55"></textarea></td>
                </tr>
                <tr valign="top">
                    <th scope="row"><label for="gmtr-url-gmaps">URL</label></th>
                    <td><input type="text" value="" name="gmtr-url-gmaps" id="gmtr-url-gmaps" size="62"></td>
                </tr>
                <tr valign="top">
                    <td></td>
                    <td><input type="submit" value="Save Location" name="gmtr-save-gmaps" class="button-primary"></td>
                </tr>
            </table>
        </form>
    </div>
<?php
}

function gmtr_list_gmaps_page() {
    global $wpdb;
    $table_gmaps = $wpdb->base_prefix . 'gmtr_data';
    if ($_REQUEST['page'] === 'gmtr-list-gmaps-page' && $_REQUEST['action'] === 'edit') {
        if (isset($_POST['gmtr-update-gmaps'])) {
            $address = trim($_POST['gmtr-address']);
            $title = $_POST['gmtr-title'];
            $description = $_POST['gmtr-desc-gmaps'];
            $gmtr_url = $_POST['gmtr-url-gmaps'];

            $url = "http://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=" . urlencode($address);
            $request = file_get_contents($url);
	    $json = json_decode($request, true);

            if ($json['status'] == 'OK' ) {
   	        $latitude = $json['results'][0]['geometry']['location']['lat'];
    	        $longitude = $json['results'][0]['geometry']['location']['lng'];

                $result_id = $wpdb->update(
                                $table_gmaps,
                                array(
                                    'gmaps_address' => $address,
                                    'gmaps_title' => $title,
                                    'gmaps_description' => $description,
                                    'gmaps_url' => $gmtr_url,
                                    'gmaps_lat_log' => $latitude . ',' . $longitude,
                                ),
                                array('id' => $_GET['id']),
                                array('%s', '%s', '%s', '%s', '%s'),
                                array('%d'));
                if ($result_id) {
                    echo '<div class="updated fade">Location Successfully Edited !!!</div>';
                } else {
                    echo '<div class="error">Error Saving Data</div>';
                }
            } else {
                echo '<div class="error"><b>Http error ' . substr($data, 0, 3) . ' Error Saving Data</b></div>';
            }
        }
        $id = $_GET['id'];
        $sql = "select * from {$table_gmaps} WHERE `id`={$id}";
        $sql_result = $wpdb->get_row($sql, ARRAY_A);
?>
        <div class ="wrap">
            <h2>Edit Location</h2>

            <form method="post" name="gmtr-edit-gmaps-form">
                <table class="form-table">
                    <tr valign="top">
                        <th scope="row"><label for="gmtr-address">Address</label></th>
                        <td><input type="text" name="gmtr-address" id="gmtr-address" value="<?php echo $sql_result['gmaps_address'] ?>" size="80"></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row"><label for="gmtr-title">Title</label></th>
                        <td><input type="text" size="35" name="gmtr-title" id="gmtr-title" value="<?php echo $sql_result['gmaps_title'] ?>"></td>
                    </tr>
                    <tr valign="top">
                        <th><label for="gmtr-desc-gmaps">Description</label></th>
                        <td><textarea id="gmtr-desc-gmaps" name="gmtr-desc-gmaps" rows="5" cols="55"><?php echo $sql_result['gmaps_description'] ?></textarea></td>
                    </tr>
                    <tr valign="top">
                        <th><label for="gmtr-url-gmaps">URL</label></th>
                        <td><input type="text" value="<?php echo $sql_result['gmaps_url'] ?>" name="gmtr-url-gmaps" id="gmtr-url-gmaps" size="35"></td>
                    </tr>
                    <tr valign="top">
                        <td></td>
                        <td><input type="submit" value="Update Location" name="gmtr-update-gmaps" class="button-primary"></td>
                    </tr>
                </table>
            </form>

        </div>
<?php
    } else {
?>
        <div class ="wrap">
            <h2>Location Listing</h2>
<?php
        $sql = "SELECT * from {$table_gmaps} ";
        $pagenum = isset($_GET['paged']) ? $_GET['paged'] : 1;
        $per_page = 20;
        $action_count = count($wpdb->get_results($sql));
        $total = ceil($action_count / $per_page);
        $action_offset = ($pagenum - 1) * $per_page;
        $page_links = paginate_links(array(
                    'base' => add_query_arg('paged', '%#%'),
                    'format' => '',
                    'prev_text' => __('&laquo;'),
                    'next_text' => __('&raquo;'),
                    'total' => ceil($action_count / $per_page),
                    'current' => $pagenum
                ));
        $sql .= " LIMIT {$action_offset}, {$per_page}";
        $gmaps_ids = $wpdb->get_results($sql);

        if (!empty($gmaps_ids)) {
            if ($page_links) {
?>
                <div class="tablenav">
                    <div class="tablenav-pages">
    <?php
                $page_links_text = sprintf('<span class="displaying-num">' . __('Displaying %s&#8211;%s of %s') . '</span>%s',
                                number_format_i18n(( $pagenum - 1 ) * $per_page + 1),
                                number_format_i18n(min($pagenum * $per_page, $action_count)),
                                number_format_i18n($action_count),
                                $page_links
                );
                echo $page_links_text;
    ?>
            </div>
        </div>
            <?php
            }
        }
            ?>
    <div class="clear"></div>
    <?php if (!empty($gmaps_ids)) { ?>
            <table class="widefat post fixed" cellspacing="0">
                <thead>
                    <tr>
                        <th class="check-column" scope="row"></th>
                        <th>Location Address</th>
                        <th>Title</th>
                        <th class="column-title">Description</th>
                        <th>URL</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>        
<?php
            $i = 1;
            $count = (($pagenum-1)*$per_page)+1;
            foreach ($gmaps_ids as $gid) {

                if ($i % 2 == 0) {
                    echo '<tr class="alternate">';
                } else {
                    echo '<tr>';
                }
                echo '<td class="check-column">&nbsp;&nbsp;' . $count . '</td>';
                echo '<td>' . $gid->gmaps_address . '<p><a href="admin.php?page=gmtr-list-gmaps-page&action=edit&id=' . $gid->id . '">Edit</a> | <a class="remove-admin-gmaps" style="cursor:pointer" rel="' . $gid->id . '">Delete</a></p></td>';
                echo '<td>' . $gid->gmaps_title . '</td>';
                echo '<td>' . substr($gid->gmaps_description, 0, 50) . '...</td>';
                echo '<td>' . $gid->gmaps_url . '</td>';
                echo '<td>' . date($gid->created_date) . '</td>';
                echo '</tr>';
                $i++;
                $count++;
            }
?>
        </tbody>

    </table>
            <?php } else {
            echo 'No records found!!!';
        } ?>
</div>
<?php
    }
}

/*
 * API Key Settings Page
 */

function gmtr_settings_gmaps_page() {
    global $wpdb;
    if (isset($_POST['gmtr-save-apikey']) == 'Save Key') {
        update_option('gmtr_gmaps_apikey', $_POST['gmtr-gmap-api-key']);
        update_option('gmtr_marker_color', $_POST['gmtr-marker-color']);
        update_option('gmtr_marker_width', $_POST['gmtr-marker-width']);
        update_option('gmtr_marker_transparent', $_POST['gmtr-marker-trans']);
        update_option('gmtr_link_back', $_POST['gmtr-link-back']);
        update_option('gmtr_link_back_hidden', $_POST['gmtr-link-back-hidden']);

        echo '<div class="updated fade">Settings Successfully Saved</div>';
    }
    $gmaps_api_key = get_option('gmtr_gmaps_apikey');
    $gmaps_marker = get_option('gmtr_marker_color');
    $gmaps_marker_width = get_option('gmtr_marker_width');
    $gmaps_marker_transparent = get_option('gmtr_marker_transparent');
    $gmaps_linkback = get_option('gmtr_link_back');
    $gmaps_linkback_hidden = get_option('gmtr_link_back_hidden');
?>
    <div class="wrap">
        <h2>Settings</h2>
<?php if (empty($gmaps_api_key)) {
 ?>
            <p>If you don't have a Google Maps API Key, <a target="_blank" href="https://developers.google.com/maps/documentation/javascript/tutorial#api_key">click here</a>.</p>
    <?php } ?>
    <form method="post">
        <table class="form-table">
            <tr valign="top">
                <th scope="row"><label for="gmtr-gmap-api-key">Enter Google Maps API Key</label></th>
                <td><input id="gmtr-gmap-api-key" type="text" size="45" value="<?php echo $gmaps_api_key; ?>" name="gmtr-gmap-api-key" /></td>
            </tr>
            <tr valign="top">
                <th scope="row"><label for="gmtr-marker-color">Route Color</label></th>
                <td><input id="gmtr-marker-color" type="text" size="7" value="<?php echo $gmaps_marker; ?>" name="gmtr-marker-color" /><span class="description">6 digit hex color code. e.g.: #FFFFFF</span></td>
            </tr>
            <tr valign="top">
                <th scope="row"><label for="gmtr-marker-width">Route Width</label></th>
                <td><input id="gmtr-marker-width" type="text" size="3" value="<?php echo $gmaps_marker_width; ?>" name="gmtr-marker-width" /><span class="description">Value between 1 to 10</span></td>
            <tr valign="top">
                <th scope="row"><label for="gmtr-marker-trans">Route Transparency</label></th>
                <td><input id="gmtr-marker-trans" type="text" size="3" value="<?php echo $gmaps_marker_transparent; ?>" name="gmtr-marker-trans" /><span class="description">Value between 0 to 1 including decimal value</span></td>
            </tr>
            <tr valign="top">
                <th></th>
                <td><input type="submit" value="Save Settings" name="gmtr-save-apikey" class="button-primary" /></td>
            </tr>
        </table>
    </form>
    <table class="form-table">
        <tr valign="top">
            <th scope="row">
                <strong>Shortcode:</strong>
            </th>
            <td>
                [travelroute height="500" width="500"]<br/>
                <span class="description">height: Height which you want to assign to the Map.</span>
                <span class="description">width: Width which you want to assign to the Map.</span>
            </td>
        </tr>
        <tr valign="top">
            <th scope="row">
                <strong>Widget:</strong>
            </th>
            <td>
                <span class="description">Note: Your theme should be Widget enabled.</span><br/>
                <span class="description">Go to Appearance > Widgets.</span><br/>
                <span class="description">Find Widget "Google Maps Travel Route". Drag and drop where you want it.</span><br/>
            </td>
        </tr>
    </table>
</div>

<?php
}

/*
 * Fucntion to get the latitude and longitute
 */

function getLatLong($id=FALSE) {
    global $wpdb, $table_gmaps;
    $latlong = array();
    $table_gmaps = $wpdb->base_prefix . 'gmtr_data';
    $sql = "SELECT `gmaps_lat_log` from `{$table_gmaps}` WHERE id={$id}";
    $sql_r = $wpdb->get_var($sql);
    $latlong[] = explode(',', $sql_r);
    return $latlong;
}

function gmtr_delete_gmaps() {
    global $wpdb, $table_gmaps;
    $table_gmaps = $wpdb->base_prefix . 'gmtr_data';
    $id = $_POST['id'];
    $sql = "DELETE FROM {$table_gmaps} WHERE id = {$id}";
    $sql_result = $wpdb->query($sql);
    if ($sql_result)
        echo 'true';
    die;
}

add_action('wp_ajax_gmtr_delete_gmaps', 'gmtr_delete_gmaps');
?>
