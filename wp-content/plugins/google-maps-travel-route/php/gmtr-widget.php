<?php

function gmtr_widget_init() {
    register_widget('WP_GMTR_Widget');
}

add_action('widgets_init', 'gmtr_widget_init', 1);

class WP_GMTR_Widget extends WP_Widget {

    function WP_GMTR_Widget() {
        $widget_ops = array('classname' => 'widget_gmaps', 'description' => __('Google Maps Travel Route Widget'));
        $this->WP_Widget('gmtr_widget', __('Google Maps Travel Route'), $widget_ops);
    }

    function widget($args, $instance) {

        echo $before_widget;

        if (get_option('gmtr_gmaps_apikey')) {
           global $gmr_is_script_included, $wpdb;
            extract($args);
            $id = 'gmr_widget_' . $widget_id;
            extract(shortcode_atts( array(
                'height' => '300',
                'width' => '300',
                'title' => '',
                'disable_popup' => ''
                ), $instance));
            $h = !empty($height) ? $height : '200';
            $w = !empty($width) ? $width : '280';

            $table_gmaps = $wpdb->base_prefix . 'gmtr_data';
            $sql = "SELECT * from {$table_gmaps}";
            $data = $wpdb->get_results($sql, ARRAY_A);

            if (!empty($title)) {
                echo $before_title . $title . $after_title;
            }

        // String for output
        $op = '';
	$content = '';
        $op.='<div id="' . $id . '" style="width:' . $w . 'px;height:' . $h . 'px"></div>';

        $op.='<script type="text/javascript">';
        $op.='google.maps.visualRefresh = true;';

        $op.='var map;';
        $op.='var iconBase = \''. GMTR_PLUGIN_URL .'/images/icons/\';';
        $op.='var bounds = new google.maps.LatLngBounds();';
        $op.='var routePath;';
        $op.='var infowindow = new google.maps.InfoWindow({});';

        $op.='function initialize() {';

            /**************************************
            *  General map options
            **************************************/
            $op.='var mapOptions = {';
                $op.='streetViewControl: false,';
                $op.='draggable: true,';
                $op.='mapTypeId: google.maps.MapTypeId.ROADMAP,';
                $op.='zoom: 0,';
                $op.='zoomControl: true,';
                $op.='zoomControlOptions: {';
                  $op.='style: google.maps.ZoomControlStyle.SMALL,';
                  $op.='position: google.maps.ControlPosition.LEFT_TOP';
                  $op.='},';
                $op.='maxZoom: 12,';
                $op.='minZoom: 2,';
                $op.='mapTypeControl: false,';
                $op.='panControl: false,';
                $op.='center: new google.maps.LatLng("47.362645", "8.541784")';
            $op.='};';

            // Create Google Map Object
            $op.='map = new google.maps.Map(document.getElementById("'. $id .'"), mapOptions);';

	    /**************************************
            *  Line symbol - used for animation
            **************************************/
            $op.='var lineSymbol = {';
                //$op.='path: \'M 0,-1 0,1\',';
                $op.='path: google.maps.SymbolPath.CIRCLE,';
                $op.='scale: 2,';
                $op.='strokeColor: \'#fff\',';
                $op.='strokeOpacity: 1';
            $op.='};';

            /**************************************
            *  Polyline options
            **************************************/
            $op.='routePath = new google.maps.Polyline({';
                $op.='strokeColor: "'. get_option('gmtr_marker_color') .'",';
                $op.='strokeOpacity: '. get_option('gmtr_marker_transparent') .',';
                $op.='strokeWeight: '. get_option('gmtr_marker_width') .',';
                $op.='clickable: false,';
                // Coming soon...
                //$op.='icons: [{ icon: lineSymbol, offset: \'0\', repeat: \'20px\'}],';
                //$op.='icons: [{ icon: lineSymbol, offset: \'100%\'}],';
                $op.='map: map,';
                $op.='geodesic: true';
            $op.='});';

            /**************************************
            *  Create waypoint array
            **************************************/
            $op.='var pathCoordinates = new Array();';
            for($i=0;$i<count($data);$i++) {
                $value = explode(',', $data[$i]['gmaps_lat_log']);
                $op.='pathCoordinates.push(new google.maps.LatLng("'.$value[0].'", "'.$value[1].'"));';
            }

            // Add pathCoordinates to map
            $op.='routePath.setPath(pathCoordinates);';

            // Add pathCoordinates to google.maps.LatLngBounds
            $op.='for (var i = 0; i < pathCoordinates.length; i++) {';
            $op.='bounds.extend(pathCoordinates[i]);';
            $op.='}';

            /**************************************
            *  Create markers
            **************************************/
              for($i=0;$i<count($data);$i++) {
                $value = explode(',', $data[$i]['gmaps_lat_log']);
                $op.='var marker'. $i .' = new google.maps.Marker({';
                    $op.='position: new google.maps.LatLng("'.$value[0].'", "'.$value[1].'"),';
                    $op.='title: \''. $data[$i]['gmaps_title'] .'\',';
                    $op.='icon: iconBase + \'flag-white.png\',';
                    //$op.='animation: google.maps.Animation.DROP,';
                    $op.='map: map';
                $op.='});';

                // Add event listener for popup infobox
                $op.='google.maps.event.addListener(marker'.$i.', \'click\', function() {';
                    $op.='var content = \'<b>'. $data[$i]['gmaps_title'] .'</b>\'';
                                if($data[$i]['gmaps_description'] != '') {
                                    $op.='+\'<br>'. $data[$i]['gmaps_description'] .'\'';
                                }
                                if($data[$i]['gmaps_url'] != '') {
                                    $op.='+\'<br><a href="'. $data[$i]['gmaps_url'] .'" target="_blank">'. $data[$i]['gmaps_url'] .'</a>\'';
                                }
                    $op.=';';

                    $op.='infowindow.setContent(content);';
                    $op.='infowindow.open(map, marker'. $i .' );});';
              }

            // Auto zoom, auto center
            if(!empty($data)) {
                $op.='map.fitBounds(bounds);';
                $op.='map.panToBounds(bounds);';

                // Because panToBounds is a bit too close, we will zoom out...
                $op.='zoomChangeBoundsListener = google.maps.event.addListenerOnce(map, \'bounds_changed\', function(event) {';
                $op.='if (this.getZoom()){ this.setZoom(map.getZoom()-1);}});';
                $op.='setTimeout(function(){google.maps.event.removeListener(zoomChangeBoundsListener)}, 2000);';
            }

	    // Run animation
            //$op.='animateLine();';
        $op.='}';

        $op.='function animateLine() {';
                $op.='var count = 0;';
                $op.='timer = window.setInterval(function() {';
                $op.='count = (count + 1) % 200;';
                $op.='var icons = routePath.get(\'icons\');';
                $op.='icons[0].offset = (count / 2) + \'%\';';
                $op.='routePath.set(\'icons\', icons);';
                // Coming soon...
                // Examples1: http://stackoverflow.com/questions/16800865/animate-google-maps-polyline
                // Examples2: http://planefinder.net/route/SFO/
                $op.='}, 100);';
                $op.='';
        $op.='}';

        $op.='google.maps.event.addDomListener(window, \'load\', initialize);';
        $op.='</script><br><br>';

        if (!$gmr_is_script_included) {
            $content .= '<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=' . get_option('gmtr_gmaps_apikey') . '&sensor=false"></script>';
            $gmr_is_script_included = true;
        }
	$content .= $op;

	echo $content;
        }

        echo $after_widget;
    }

    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['height'] = strip_tags($new_instance['height']);
        $instance['width'] = strip_tags($new_instance['width']);
        $instance['disable_popup'] = strip_tags($new_instance['disable_popup']);

        return $instance;
    }

    function form($instance) {
        $instance = wp_parse_args((array) $instance, array('title' => '', 'height' => '', 'width' => '', 'disable_popup' =>  'checked'));
        $title = strip_tags($instance['title']);
        $height = strip_tags($instance['height']);
        $width = strip_tags($instance['width']);
        $disabled_popup = strip_tags($instance['disable_popup']);
?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>

        <p><label for="<?php echo $this->get_field_id('height'); ?>"><?php _e('Height: (in px)'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('height'); ?>" name="<?php echo $this->get_field_name('height'); ?>" type="text" value="<?php echo esc_attr($height); ?>" size="3" /></p>

        <p><label for="<?php echo $this->get_field_id('width'); ?>"><?php _e('Width: (in px)'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('width'); ?>" name="<?php echo $this->get_field_name('width'); ?>" type="text" value="<?php echo esc_attr($width); ?>" size="3" /></p>

<?php
    }

}
?>
