jQuery(document).ready(function(){
    jQuery('.remove-admin-gmaps').live('click',function(){
        var action = confirm('Do you really want to perform this action?');
        if(action==true){
            jQuery(this).parent().parent().parent().addClass('del-current');
            var id = jQuery(this).attr('rel');

            var data = {
                action:'gmtr_delete_gmaps',
                id:id
            };

            jQuery.post(ajaxurl,data,function(response){
                if(response=='true'){
                    jQuery('.del-current').css('background-color','#FFF000');
                    jQuery('.del-current').remove();
                }
            });
        }
    });

    jQuery('.gmtr-row').click(function(event) {

        var $target = jQuery(event.target);

        if(!$target.is('input[name=gmtr-selection-gmaps]:radio')) {
            jQuery(this).find('input[name=gmtr-selection-gmaps]:radio').each(function($) {
                if(this.checked) this.checked = false;
                else this.checked = true;
            })
        }
    });

});
