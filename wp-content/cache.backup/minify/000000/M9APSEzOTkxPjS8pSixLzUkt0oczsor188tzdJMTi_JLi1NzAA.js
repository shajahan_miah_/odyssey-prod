
/* owl-carousel.js */

/* 1    */ /*
/* 2    *|  *  jQuery OwlCarousel v1.3.2
/* 3    *|  *
/* 4    *|  *  Copyright (c) 2013 Bartosz Wojciechowski
/* 5    *|  *  http://www.owlgraphic.com/owlcarousel/
/* 6    *|  *
/* 7    *|  *  Licensed under MIT
/* 8    *|  *
/* 9    *|  */
/* 10   */ 
/* 11   */ /*JS Lint helpers: */
/* 12   */ /*global dragMove: false, dragEnd: false, $, jQuery, alert, window, document */
/* 13   */ /*jslint nomen: true, continue:true */
/* 14   */ 
/* 15   */ if (typeof Object.create !== "function") {
/* 16   */     Object.create = function(obj) {
/* 17   */         function F() {}
/* 18   */         F.prototype = obj;
/* 19   */         return new F();
/* 20   */     };
/* 21   */ }
/* 22   */ (function($, window, document) {
/* 23   */ 
/* 24   */     var Carousel = {
/* 25   */         init: function(options, el) {
/* 26   */             var base = this;
/* 27   */ 
/* 28   */             base.$elem = $(el);
/* 29   */             base.options = $.extend({}, $.fn.owlCarousel.options, base.$elem.data(), options);
/* 30   */ 
/* 31   */             base.userOptions = options;
/* 32   */             base.loadContent();
/* 33   */         },
/* 34   */ 
/* 35   */         loadContent: function() {
/* 36   */             var base = this,
/* 37   */                 url;
/* 38   */ 
/* 39   */             function getData(data) {
/* 40   */                 var i, content = "";
/* 41   */                 if (typeof base.options.jsonSuccess === "function") {
/* 42   */                     base.options.jsonSuccess.apply(this, [data]);
/* 43   */                 } else {
/* 44   */                     for (i in data.owl) {
/* 45   */                         if (data.owl.hasOwnProperty(i)) {
/* 46   */                             content += data.owl[i].item;
/* 47   */                         }
/* 48   */                     }
/* 49   */                     base.$elem.html(content);
/* 50   */                 }

/* owl-carousel.js */

/* 51   */                 base.logIn();
/* 52   */             }
/* 53   */ 
/* 54   */             if (typeof base.options.beforeInit === "function") {
/* 55   */                 base.options.beforeInit.apply(this, [base.$elem]);
/* 56   */             }
/* 57   */ 
/* 58   */             if (typeof base.options.jsonPath === "string") {
/* 59   */                 url = base.options.jsonPath;
/* 60   */                 $.getJSON(url, getData);
/* 61   */             } else {
/* 62   */                 base.logIn();
/* 63   */             }
/* 64   */         },
/* 65   */ 
/* 66   */         logIn: function() {
/* 67   */             var base = this;
/* 68   */ 
/* 69   */             base.$elem.data({
/* 70   */                 "owl-originalStyles": base.$elem.attr("style"),
/* 71   */                 "owl-originalClasses": base.$elem.attr("class")
/* 72   */             });
/* 73   */ 
/* 74   */             base.$elem.css({
/* 75   */                 opacity: 0
/* 76   */             });
/* 77   */             base.orignalItems = base.options.items;
/* 78   */             base.checkBrowser();
/* 79   */             base.wrapperWidth = 0;
/* 80   */             base.checkVisible = null;
/* 81   */             base.setVars();
/* 82   */         },
/* 83   */ 
/* 84   */         setVars: function() {
/* 85   */             var base = this;
/* 86   */             if (base.$elem.children().length === 0) {
/* 87   */                 return false;
/* 88   */             }
/* 89   */             base.baseClass();
/* 90   */             base.eventTypes();
/* 91   */             base.$userItems = base.$elem.children();
/* 92   */             base.itemsAmount = base.$userItems.length;
/* 93   */             base.wrapItems();
/* 94   */             base.$owlItems = base.$elem.find(".owl-item");
/* 95   */             base.$owlWrapper = base.$elem.find(".owl-wrapper");
/* 96   */             base.playDirection = "next";
/* 97   */             base.prevItem = 0;
/* 98   */             base.prevArr = [0];
/* 99   */             base.currentItem = 0;
/* 100  */             base.customEvents();

/* owl-carousel.js */

/* 101  */             base.onStartup();
/* 102  */         },
/* 103  */ 
/* 104  */         onStartup: function() {
/* 105  */             var base = this;
/* 106  */             base.updateItems();
/* 107  */             base.calculateAll();
/* 108  */             base.buildControls();
/* 109  */             base.updateControls();
/* 110  */             base.response();
/* 111  */             base.moveEvents();
/* 112  */             base.stopOnHover();
/* 113  */             base.owlStatus();
/* 114  */ 
/* 115  */             if (base.options.transitionStyle !== false) {
/* 116  */                 base.transitionTypes(base.options.transitionStyle);
/* 117  */             }
/* 118  */             if (base.options.autoPlay === true) {
/* 119  */                 base.options.autoPlay = 5000;
/* 120  */             }
/* 121  */             base.play();
/* 122  */ 
/* 123  */             base.$elem.find(".owl-wrapper").css("display", "block");
/* 124  */ 
/* 125  */             if (!base.$elem.is(":visible")) {
/* 126  */                 base.watchVisibility();
/* 127  */             } else {
/* 128  */                 base.$elem.css("opacity", 1);
/* 129  */             }
/* 130  */             base.onstartup = false;
/* 131  */             base.eachMoveUpdate();
/* 132  */             if (typeof base.options.afterInit === "function") {
/* 133  */                 base.options.afterInit.apply(this, [base.$elem]);
/* 134  */             }
/* 135  */         },
/* 136  */ 
/* 137  */         eachMoveUpdate: function() {
/* 138  */             var base = this;
/* 139  */ 
/* 140  */             if (base.options.lazyLoad === true) {
/* 141  */                 base.lazyLoad();
/* 142  */             }
/* 143  */             if (base.options.autoHeight === true) {
/* 144  */                 base.autoHeight();
/* 145  */             }
/* 146  */             base.onVisibleItems();
/* 147  */ 
/* 148  */             if (typeof base.options.afterAction === "function") {
/* 149  */                 base.options.afterAction.apply(this, [base.$elem]);
/* 150  */             }

/* owl-carousel.js */

/* 151  */         },
/* 152  */ 
/* 153  */         updateVars: function() {
/* 154  */             var base = this;
/* 155  */             if (typeof base.options.beforeUpdate === "function") {
/* 156  */                 base.options.beforeUpdate.apply(this, [base.$elem]);
/* 157  */             }
/* 158  */             base.watchVisibility();
/* 159  */             base.updateItems();
/* 160  */             base.calculateAll();
/* 161  */             base.updatePosition();
/* 162  */             base.updateControls();
/* 163  */             base.eachMoveUpdate();
/* 164  */             if (typeof base.options.afterUpdate === "function") {
/* 165  */                 base.options.afterUpdate.apply(this, [base.$elem]);
/* 166  */             }
/* 167  */         },
/* 168  */ 
/* 169  */         reload: function() {
/* 170  */             var base = this;
/* 171  */             window.setTimeout(function() {
/* 172  */                 base.updateVars();
/* 173  */             }, 0);
/* 174  */         },
/* 175  */ 
/* 176  */         watchVisibility: function() {
/* 177  */             var base = this;
/* 178  */ 
/* 179  */             if (base.$elem.is(":visible") === false) {
/* 180  */                 base.$elem.css({
/* 181  */                     opacity: 0
/* 182  */                 });
/* 183  */                 window.clearInterval(base.autoPlayInterval);
/* 184  */                 window.clearInterval(base.checkVisible);
/* 185  */             } else {
/* 186  */                 return false;
/* 187  */             }
/* 188  */             base.checkVisible = window.setInterval(function() {
/* 189  */                 if (base.$elem.is(":visible")) {
/* 190  */                     base.reload();
/* 191  */                     base.$elem.animate({
/* 192  */                         opacity: 1
/* 193  */                     }, 200);
/* 194  */                     window.clearInterval(base.checkVisible);
/* 195  */                 }
/* 196  */             }, 500);
/* 197  */         },
/* 198  */ 
/* 199  */         wrapItems: function() {
/* 200  */             var base = this;

/* owl-carousel.js */

/* 201  */             base.$userItems.wrapAll("<div class=\"owl-wrapper\">").wrap("<div class=\"owl-item\"></div>");
/* 202  */             base.$elem.find(".owl-wrapper").wrap("<div class=\"owl-wrapper-outer\">");
/* 203  */             base.wrapperOuter = base.$elem.find(".owl-wrapper-outer");
/* 204  */             base.$elem.css("display", "block");
/* 205  */         },
/* 206  */ 
/* 207  */         baseClass: function() {
/* 208  */             var base = this,
/* 209  */                 hasBaseClass = base.$elem.hasClass(base.options.baseClass),
/* 210  */                 hasThemeClass = base.$elem.hasClass(base.options.theme);
/* 211  */ 
/* 212  */             if (!hasBaseClass) {
/* 213  */                 base.$elem.addClass(base.options.baseClass);
/* 214  */             }
/* 215  */ 
/* 216  */             if (!hasThemeClass) {
/* 217  */                 base.$elem.addClass(base.options.theme);
/* 218  */             }
/* 219  */         },
/* 220  */ 
/* 221  */         updateItems: function() {
/* 222  */             var base = this,
/* 223  */                 width, i;
/* 224  */ 
/* 225  */             if (base.options.responsive === false) {
/* 226  */                 return false;
/* 227  */             }
/* 228  */             if (base.options.singleItem === true) {
/* 229  */                 base.options.items = base.orignalItems = 1;
/* 230  */                 base.options.itemsCustom = false;
/* 231  */                 base.options.itemsDesktop = false;
/* 232  */                 base.options.itemsDesktopSmall = false;
/* 233  */                 base.options.itemsTablet = false;
/* 234  */                 base.options.itemsTabletSmall = false;
/* 235  */                 base.options.itemsMobile = false;
/* 236  */                 return false;
/* 237  */             }
/* 238  */ 
/* 239  */             width = $(base.options.responsiveBaseWidth).width();
/* 240  */ 
/* 241  */             if (width > (base.options.itemsDesktop[0] || base.orignalItems)) {
/* 242  */                 base.options.items = base.orignalItems;
/* 243  */             }
/* 244  */             if (base.options.itemsCustom !== false) {
/* 245  */                 //Reorder array by screen size
/* 246  */                 base.options.itemsCustom.sort(function(a, b) {
/* 247  */                     return a[0] - b[0];
/* 248  */                 });
/* 249  */ 
/* 250  */                 for (i = 0; i < base.options.itemsCustom.length; i += 1) {

/* owl-carousel.js */

/* 251  */                     if (base.options.itemsCustom[i][0] <= width) {
/* 252  */                         base.options.items = base.options.itemsCustom[i][1];
/* 253  */                     }
/* 254  */                 }
/* 255  */ 
/* 256  */             } else {
/* 257  */ 
/* 258  */                 if (width <= base.options.itemsDesktop[0] && base.options.itemsDesktop !== false) {
/* 259  */                     base.options.items = base.options.itemsDesktop[1];
/* 260  */                 }
/* 261  */ 
/* 262  */                 if (width <= base.options.itemsDesktopSmall[0] && base.options.itemsDesktopSmall !== false) {
/* 263  */                     base.options.items = base.options.itemsDesktopSmall[1];
/* 264  */                 }
/* 265  */ 
/* 266  */                 if (width <= base.options.itemsTablet[0] && base.options.itemsTablet !== false) {
/* 267  */                     base.options.items = base.options.itemsTablet[1];
/* 268  */                 }
/* 269  */ 
/* 270  */                 if (width <= base.options.itemsTabletSmall[0] && base.options.itemsTabletSmall !== false) {
/* 271  */                     base.options.items = base.options.itemsTabletSmall[1];
/* 272  */                 }
/* 273  */ 
/* 274  */                 if (width <= base.options.itemsMobile[0] && base.options.itemsMobile !== false) {
/* 275  */                     base.options.items = base.options.itemsMobile[1];
/* 276  */                 }
/* 277  */             }
/* 278  */ 
/* 279  */             //if number of items is less than declared
/* 280  */             if (base.options.items > base.itemsAmount && base.options.itemsScaleUp === true) {
/* 281  */                 base.options.items = base.itemsAmount;
/* 282  */             }
/* 283  */         },
/* 284  */ 
/* 285  */         response: function() {
/* 286  */             var base = this,
/* 287  */                 smallDelay,
/* 288  */                 lastWindowWidth;
/* 289  */ 
/* 290  */             if (base.options.responsive !== true) {
/* 291  */                 return false;
/* 292  */             }
/* 293  */             lastWindowWidth = $(window).width();
/* 294  */ 
/* 295  */             base.resizer = function() {
/* 296  */                 if ($(window).width() !== lastWindowWidth) {
/* 297  */                     if (base.options.autoPlay !== false) {
/* 298  */                         window.clearInterval(base.autoPlayInterval);
/* 299  */                     }
/* 300  */                     window.clearTimeout(smallDelay);

/* owl-carousel.js */

/* 301  */                     smallDelay = window.setTimeout(function() {
/* 302  */                         lastWindowWidth = $(window).width();
/* 303  */                         base.updateVars();
/* 304  */                     }, base.options.responsiveRefreshRate);
/* 305  */                 }
/* 306  */             };
/* 307  */             $(window).resize(base.resizer);
/* 308  */         },
/* 309  */ 
/* 310  */         updatePosition: function() {
/* 311  */             var base = this;
/* 312  */             base.jumpTo(base.currentItem);
/* 313  */             if (base.options.autoPlay !== false) {
/* 314  */                 base.checkAp();
/* 315  */             }
/* 316  */         },
/* 317  */ 
/* 318  */         appendItemsSizes: function() {
/* 319  */             var base = this,
/* 320  */                 roundPages = 0,
/* 321  */                 lastItem = base.itemsAmount - base.options.items;
/* 322  */ 
/* 323  */             base.$owlItems.each(function(index) {
/* 324  */                 var $this = $(this);
/* 325  */                 $this
/* 326  */                     .css({
/* 327  */                         "width": base.itemWidth
/* 328  */                     })
/* 329  */                     .data("owl-item", Number(index));
/* 330  */ 
/* 331  */                 if (index % base.options.items === 0 || index === lastItem) {
/* 332  */                     if (!(index > lastItem)) {
/* 333  */                         roundPages += 1;
/* 334  */                     }
/* 335  */                 }
/* 336  */                 $this.data("owl-roundPages", roundPages);
/* 337  */             });
/* 338  */         },
/* 339  */ 
/* 340  */         appendWrapperSizes: function() {
/* 341  */             var base = this,
/* 342  */                 width = base.$owlItems.length * base.itemWidth;
/* 343  */ 
/* 344  */             base.$owlWrapper.css({
/* 345  */                 "width": width * 2,
/* 346  */                 "left": 0
/* 347  */             });
/* 348  */             base.appendItemsSizes();
/* 349  */         },
/* 350  */ 

/* owl-carousel.js */

/* 351  */         calculateAll: function() {
/* 352  */             var base = this;
/* 353  */             base.calculateWidth();
/* 354  */             base.appendWrapperSizes();
/* 355  */             base.loops();
/* 356  */             base.max();
/* 357  */         },
/* 358  */ 
/* 359  */         calculateWidth: function() {
/* 360  */             var base = this;
/* 361  */             base.itemWidth = Math.round(base.$elem.width() / base.options.items);
/* 362  */         },
/* 363  */ 
/* 364  */         max: function() {
/* 365  */             var base = this,
/* 366  */                 maximum = ((base.itemsAmount * base.itemWidth) - base.options.items * base.itemWidth) * -1;
/* 367  */             if (base.options.items > base.itemsAmount) {
/* 368  */                 base.maximumItem = 0;
/* 369  */                 maximum = 0;
/* 370  */                 base.maximumPixels = 0;
/* 371  */             } else {
/* 372  */                 base.maximumItem = base.itemsAmount - base.options.items;
/* 373  */                 base.maximumPixels = maximum;
/* 374  */             }
/* 375  */             return maximum;
/* 376  */         },
/* 377  */ 
/* 378  */         min: function() {
/* 379  */             return 0;
/* 380  */         },
/* 381  */ 
/* 382  */         loops: function() {
/* 383  */             var base = this,
/* 384  */                 prev = 0,
/* 385  */                 elWidth = 0,
/* 386  */                 i,
/* 387  */                 item,
/* 388  */                 roundPageNum;
/* 389  */ 
/* 390  */             base.positionsInArray = [0];
/* 391  */             base.pagesInArray = [];
/* 392  */ 
/* 393  */             for (i = 0; i < base.itemsAmount; i += 1) {
/* 394  */                 elWidth += base.itemWidth;
/* 395  */                 base.positionsInArray.push(-elWidth);
/* 396  */ 
/* 397  */                 if (base.options.scrollPerPage === true) {
/* 398  */                     item = $(base.$owlItems[i]);
/* 399  */                     roundPageNum = item.data("owl-roundPages");
/* 400  */                     if (roundPageNum !== prev) {

/* owl-carousel.js */

/* 401  */                         base.pagesInArray[prev] = base.positionsInArray[i];
/* 402  */                         prev = roundPageNum;
/* 403  */                     }
/* 404  */                 }
/* 405  */             }
/* 406  */         },
/* 407  */ 
/* 408  */         buildControls: function() {
/* 409  */             var base = this;
/* 410  */             if (base.options.navigation === true || base.options.pagination === true) {
/* 411  */                 base.owlControls = $("<div class=\"owl-controls\"/>").toggleClass("clickable", !base.browser.isTouch).appendTo(base.$elem);
/* 412  */             }
/* 413  */             if (base.options.pagination === true) {
/* 414  */                 base.buildPagination();
/* 415  */             }
/* 416  */             if (base.options.navigation === true) {
/* 417  */                 base.buildButtons();
/* 418  */             }
/* 419  */         },
/* 420  */ 
/* 421  */         buildButtons: function() {
/* 422  */             var base = this,
/* 423  */                 buttonsWrapper = $("<div class=\"owl-buttons\"/>");
/* 424  */             base.owlControls.append(buttonsWrapper);
/* 425  */ 
/* 426  */             base.buttonPrev = $("<div/>", {
/* 427  */                 "class": "owl-prev",
/* 428  */                 "html": base.options.navigationText[0] || ""
/* 429  */             });
/* 430  */ 
/* 431  */             base.buttonNext = $("<div/>", {
/* 432  */                 "class": "owl-next",
/* 433  */                 "html": base.options.navigationText[1] || ""
/* 434  */             });
/* 435  */ 
/* 436  */             buttonsWrapper
/* 437  */                 .append(base.buttonPrev)
/* 438  */                 .append(base.buttonNext);
/* 439  */ 
/* 440  */             buttonsWrapper.on("touchstart.owlControls mousedown.owlControls", "div[class^=\"owl\"]", function(event) {
/* 441  */                 event.preventDefault();
/* 442  */             });
/* 443  */ 
/* 444  */             buttonsWrapper.on("touchend.owlControls mouseup.owlControls", "div[class^=\"owl\"]", function(event) {
/* 445  */                 event.preventDefault();
/* 446  */                 if ($(this).hasClass("owl-next")) {
/* 447  */                     base.next();
/* 448  */                 } else {
/* 449  */                     base.prev();
/* 450  */                 }

/* owl-carousel.js */

/* 451  */             });
/* 452  */         },
/* 453  */ 
/* 454  */         buildPagination: function() {
/* 455  */             var base = this;
/* 456  */ 
/* 457  */             base.paginationWrapper = $("<div class=\"owl-pagination\"/>");
/* 458  */             base.owlControls.append(base.paginationWrapper);
/* 459  */ 
/* 460  */             base.paginationWrapper.on("touchend.owlControls mouseup.owlControls", ".owl-page", function(event) {
/* 461  */                 event.preventDefault();
/* 462  */                 if (Number($(this).data("owl-page")) !== base.currentItem) {
/* 463  */                     base.goTo(Number($(this).data("owl-page")), true);
/* 464  */                 }
/* 465  */             });
/* 466  */         },
/* 467  */ 
/* 468  */         updatePagination: function() {
/* 469  */             var base = this,
/* 470  */                 counter,
/* 471  */                 lastPage,
/* 472  */                 lastItem,
/* 473  */                 i,
/* 474  */                 paginationButton,
/* 475  */                 paginationButtonInner;
/* 476  */ 
/* 477  */             if (base.options.pagination === false) {
/* 478  */                 return false;
/* 479  */             }
/* 480  */ 
/* 481  */             base.paginationWrapper.html("");
/* 482  */ 
/* 483  */             counter = 0;
/* 484  */             lastPage = base.itemsAmount - base.itemsAmount % base.options.items;
/* 485  */ 
/* 486  */             for (i = 0; i < base.itemsAmount; i += 1) {
/* 487  */                 if (i % base.options.items === 0) {
/* 488  */                     counter += 1;
/* 489  */                     if (lastPage === i) {
/* 490  */                         lastItem = base.itemsAmount - base.options.items;
/* 491  */                     }
/* 492  */                     paginationButton = $("<div/>", {
/* 493  */                         "class": "owl-page"
/* 494  */                     });
/* 495  */                     paginationButtonInner = $("<span></span>", {
/* 496  */                         "text": base.options.paginationNumbers === true ? counter : "",
/* 497  */                         "class": base.options.paginationNumbers === true ? "owl-numbers" : ""
/* 498  */                     });
/* 499  */                     paginationButton.append(paginationButtonInner);
/* 500  */ 

/* owl-carousel.js */

/* 501  */                     paginationButton.data("owl-page", lastPage === i ? lastItem : i);
/* 502  */                     paginationButton.data("owl-roundPages", counter);
/* 503  */ 
/* 504  */                     base.paginationWrapper.append(paginationButton);
/* 505  */                 }
/* 506  */             }
/* 507  */             base.checkPagination();
/* 508  */         },
/* 509  */         checkPagination: function() {
/* 510  */             var base = this;
/* 511  */             if (base.options.pagination === false) {
/* 512  */                 return false;
/* 513  */             }
/* 514  */             base.paginationWrapper.find(".owl-page").each(function() {
/* 515  */                 if ($(this).data("owl-roundPages") === $(base.$owlItems[base.currentItem]).data("owl-roundPages")) {
/* 516  */                     base.paginationWrapper
/* 517  */                         .find(".owl-page")
/* 518  */                         .removeClass("active");
/* 519  */                     $(this).addClass("active");
/* 520  */                 }
/* 521  */             });
/* 522  */         },
/* 523  */ 
/* 524  */         checkNavigation: function() {
/* 525  */             var base = this;
/* 526  */ 
/* 527  */             if (base.options.navigation === false) {
/* 528  */                 return false;
/* 529  */             }
/* 530  */             if (base.options.rewindNav === false) {
/* 531  */                 if (base.currentItem === 0 && base.maximumItem === 0) {
/* 532  */                     base.buttonPrev.addClass("disabled");
/* 533  */                     base.buttonNext.addClass("disabled");
/* 534  */                 } else if (base.currentItem === 0 && base.maximumItem !== 0) {
/* 535  */                     base.buttonPrev.addClass("disabled");
/* 536  */                     base.buttonNext.removeClass("disabled");
/* 537  */                 } else if (base.currentItem === base.maximumItem) {
/* 538  */                     base.buttonPrev.removeClass("disabled");
/* 539  */                     base.buttonNext.addClass("disabled");
/* 540  */                 } else if (base.currentItem !== 0 && base.currentItem !== base.maximumItem) {
/* 541  */                     base.buttonPrev.removeClass("disabled");
/* 542  */                     base.buttonNext.removeClass("disabled");
/* 543  */                 }
/* 544  */             }
/* 545  */         },
/* 546  */ 
/* 547  */         updateControls: function() {
/* 548  */             var base = this;
/* 549  */             base.updatePagination();
/* 550  */             base.checkNavigation();

/* owl-carousel.js */

/* 551  */             if (base.owlControls) {
/* 552  */                 if (base.options.items >= base.itemsAmount) {
/* 553  */                     base.owlControls.hide();
/* 554  */                 } else {
/* 555  */                     base.owlControls.show();
/* 556  */                 }
/* 557  */             }
/* 558  */         },
/* 559  */ 
/* 560  */         destroyControls: function() {
/* 561  */             var base = this;
/* 562  */             if (base.owlControls) {
/* 563  */                 base.owlControls.remove();
/* 564  */             }
/* 565  */         },
/* 566  */ 
/* 567  */         next: function(speed) {
/* 568  */             var base = this;
/* 569  */ 
/* 570  */             if (base.isTransition) {
/* 571  */                 return false;
/* 572  */             }
/* 573  */ 
/* 574  */             base.currentItem += base.options.scrollPerPage === true ? base.options.items : 1;
/* 575  */             if (base.currentItem > base.maximumItem + (base.options.scrollPerPage === true ? (base.options.items - 1) : 0)) {
/* 576  */                 if (base.options.rewindNav === true) {
/* 577  */                     base.currentItem = 0;
/* 578  */                     speed = "rewind";
/* 579  */                 } else {
/* 580  */                     base.currentItem = base.maximumItem;
/* 581  */                     return false;
/* 582  */                 }
/* 583  */             }
/* 584  */             base.goTo(base.currentItem, speed);
/* 585  */         },
/* 586  */ 
/* 587  */         prev: function(speed) {
/* 588  */             var base = this;
/* 589  */ 
/* 590  */             if (base.isTransition) {
/* 591  */                 return false;
/* 592  */             }
/* 593  */ 
/* 594  */             if (base.options.scrollPerPage === true && base.currentItem > 0 && base.currentItem < base.options.items) {
/* 595  */                 base.currentItem = 0;
/* 596  */             } else {
/* 597  */                 base.currentItem -= base.options.scrollPerPage === true ? base.options.items : 1;
/* 598  */             }
/* 599  */             if (base.currentItem < 0) {
/* 600  */                 if (base.options.rewindNav === true) {

/* owl-carousel.js */

/* 601  */                     base.currentItem = base.maximumItem;
/* 602  */                     speed = "rewind";
/* 603  */                 } else {
/* 604  */                     base.currentItem = 0;
/* 605  */                     return false;
/* 606  */                 }
/* 607  */             }
/* 608  */             base.goTo(base.currentItem, speed);
/* 609  */         },
/* 610  */ 
/* 611  */         goTo: function(position, speed, drag) {
/* 612  */             var base = this,
/* 613  */                 goToPixel;
/* 614  */ 
/* 615  */             if (base.isTransition) {
/* 616  */                 return false;
/* 617  */             }
/* 618  */             if (typeof base.options.beforeMove === "function") {
/* 619  */                 base.options.beforeMove.apply(this, [base.$elem]);
/* 620  */             }
/* 621  */             if (position >= base.maximumItem) {
/* 622  */                 position = base.maximumItem;
/* 623  */             } else if (position <= 0) {
/* 624  */                 position = 0;
/* 625  */             }
/* 626  */ 
/* 627  */             base.currentItem = base.owl.currentItem = position;
/* 628  */             if (base.options.transitionStyle !== false && drag !== "drag" && base.options.items === 1 && base.browser.support3d === true) {
/* 629  */                 base.swapSpeed(0);
/* 630  */                 if (base.browser.support3d === true) {
/* 631  */                     base.transition3d(base.positionsInArray[position]);
/* 632  */                 } else {
/* 633  */                     base.css2slide(base.positionsInArray[position], 1);
/* 634  */                 }
/* 635  */                 base.afterGo();
/* 636  */                 base.singleItemTransition();
/* 637  */                 return false;
/* 638  */             }
/* 639  */             goToPixel = base.positionsInArray[position];
/* 640  */ 
/* 641  */             if (base.browser.support3d === true) {
/* 642  */                 base.isCss3Finish = false;
/* 643  */ 
/* 644  */                 if (speed === true) {
/* 645  */                     base.swapSpeed("paginationSpeed");
/* 646  */                     window.setTimeout(function() {
/* 647  */                         base.isCss3Finish = true;
/* 648  */                     }, base.options.paginationSpeed);
/* 649  */ 
/* 650  */                 } else if (speed === "rewind") {

/* owl-carousel.js */

/* 651  */                     base.swapSpeed(base.options.rewindSpeed);
/* 652  */                     window.setTimeout(function() {
/* 653  */                         base.isCss3Finish = true;
/* 654  */                     }, base.options.rewindSpeed);
/* 655  */ 
/* 656  */                 } else {
/* 657  */                     base.swapSpeed("slideSpeed");
/* 658  */                     window.setTimeout(function() {
/* 659  */                         base.isCss3Finish = true;
/* 660  */                     }, base.options.slideSpeed);
/* 661  */                 }
/* 662  */                 base.transition3d(goToPixel);
/* 663  */             } else {
/* 664  */                 if (speed === true) {
/* 665  */                     base.css2slide(goToPixel, base.options.paginationSpeed);
/* 666  */                 } else if (speed === "rewind") {
/* 667  */                     base.css2slide(goToPixel, base.options.rewindSpeed);
/* 668  */                 } else {
/* 669  */                     base.css2slide(goToPixel, base.options.slideSpeed);
/* 670  */                 }
/* 671  */             }
/* 672  */             base.afterGo();
/* 673  */         },
/* 674  */ 
/* 675  */         jumpTo: function(position) {
/* 676  */             var base = this;
/* 677  */             if (typeof base.options.beforeMove === "function") {
/* 678  */                 base.options.beforeMove.apply(this, [base.$elem]);
/* 679  */             }
/* 680  */             if (position >= base.maximumItem || position === -1) {
/* 681  */                 position = base.maximumItem;
/* 682  */             } else if (position <= 0) {
/* 683  */                 position = 0;
/* 684  */             }
/* 685  */             base.swapSpeed(0);
/* 686  */             if (base.browser.support3d === true) {
/* 687  */                 base.transition3d(base.positionsInArray[position]);
/* 688  */             } else {
/* 689  */                 base.css2slide(base.positionsInArray[position], 1);
/* 690  */             }
/* 691  */             base.currentItem = base.owl.currentItem = position;
/* 692  */             base.afterGo();
/* 693  */         },
/* 694  */ 
/* 695  */         afterGo: function() {
/* 696  */             var base = this;
/* 697  */ 
/* 698  */             base.prevArr.push(base.currentItem);
/* 699  */             base.prevItem = base.owl.prevItem = base.prevArr[base.prevArr.length - 2];
/* 700  */             base.prevArr.shift(0);

/* owl-carousel.js */

/* 701  */ 
/* 702  */             if (base.prevItem !== base.currentItem) {
/* 703  */                 base.checkPagination();
/* 704  */                 base.checkNavigation();
/* 705  */                 base.eachMoveUpdate();
/* 706  */ 
/* 707  */                 if (base.options.autoPlay !== false) {
/* 708  */                     base.checkAp();
/* 709  */                 }
/* 710  */             }
/* 711  */             if (typeof base.options.afterMove === "function" && base.prevItem !== base.currentItem) {
/* 712  */                 base.options.afterMove.apply(this, [base.$elem]);
/* 713  */             }
/* 714  */         },
/* 715  */ 
/* 716  */         stop: function() {
/* 717  */             var base = this;
/* 718  */             base.apStatus = "stop";
/* 719  */             window.clearInterval(base.autoPlayInterval);
/* 720  */         },
/* 721  */ 
/* 722  */         checkAp: function() {
/* 723  */             var base = this;
/* 724  */             if (base.apStatus !== "stop") {
/* 725  */                 base.play();
/* 726  */             }
/* 727  */         },
/* 728  */ 
/* 729  */         play: function() {
/* 730  */             var base = this;
/* 731  */             base.apStatus = "play";
/* 732  */             if (base.options.autoPlay === false) {
/* 733  */                 return false;
/* 734  */             }
/* 735  */             window.clearInterval(base.autoPlayInterval);
/* 736  */             base.autoPlayInterval = window.setInterval(function() {
/* 737  */                 base.next(true);
/* 738  */             }, base.options.autoPlay);
/* 739  */         },
/* 740  */ 
/* 741  */         swapSpeed: function(action) {
/* 742  */             var base = this;
/* 743  */             if (action === "slideSpeed") {
/* 744  */                 base.$owlWrapper.css(base.addCssSpeed(base.options.slideSpeed));
/* 745  */             } else if (action === "paginationSpeed") {
/* 746  */                 base.$owlWrapper.css(base.addCssSpeed(base.options.paginationSpeed));
/* 747  */             } else if (typeof action !== "string") {
/* 748  */                 base.$owlWrapper.css(base.addCssSpeed(action));
/* 749  */             }
/* 750  */         },

/* owl-carousel.js */

/* 751  */ 
/* 752  */         addCssSpeed: function(speed) {
/* 753  */             return {
/* 754  */                 "-webkit-transition": "all " + speed + "ms ease",
/* 755  */                 "-moz-transition": "all " + speed + "ms ease",
/* 756  */                 "-o-transition": "all " + speed + "ms ease",
/* 757  */                 "transition": "all " + speed + "ms ease"
/* 758  */             };
/* 759  */         },
/* 760  */ 
/* 761  */         removeTransition: function() {
/* 762  */             return {
/* 763  */                 "-webkit-transition": "",
/* 764  */                 "-moz-transition": "",
/* 765  */                 "-o-transition": "",
/* 766  */                 "transition": ""
/* 767  */             };
/* 768  */         },
/* 769  */ 
/* 770  */         doTranslate: function(pixels) {
/* 771  */             return {
/* 772  */                 "-webkit-transform": "translate3d(" + pixels + "px, 0px, 0px)",
/* 773  */                 "-moz-transform": "translate3d(" + pixels + "px, 0px, 0px)",
/* 774  */                 "-o-transform": "translate3d(" + pixels + "px, 0px, 0px)",
/* 775  */                 "-ms-transform": "translate3d(" + pixels + "px, 0px, 0px)",
/* 776  */                 "transform": "translate3d(" + pixels + "px, 0px,0px)"
/* 777  */             };
/* 778  */         },
/* 779  */ 
/* 780  */         transition3d: function(value) {
/* 781  */             var base = this;
/* 782  */             base.$owlWrapper.css(base.doTranslate(value));
/* 783  */         },
/* 784  */ 
/* 785  */         css2move: function(value) {
/* 786  */             var base = this;
/* 787  */             base.$owlWrapper.css({
/* 788  */                 "left": value
/* 789  */             });
/* 790  */         },
/* 791  */ 
/* 792  */         css2slide: function(value, speed) {
/* 793  */             var base = this;
/* 794  */ 
/* 795  */             base.isCssFinish = false;
/* 796  */             base.$owlWrapper.stop(true, true).animate({
/* 797  */                 "left": value
/* 798  */             }, {
/* 799  */                 duration: speed || base.options.slideSpeed,
/* 800  */                 complete: function() {

/* owl-carousel.js */

/* 801  */                     base.isCssFinish = true;
/* 802  */                 }
/* 803  */             });
/* 804  */         },
/* 805  */ 
/* 806  */         checkBrowser: function() {
/* 807  */             var base = this,
/* 808  */                 translate3D = "translate3d(0px, 0px, 0px)",
/* 809  */                 tempElem = document.createElement("div"),
/* 810  */                 regex,
/* 811  */                 asSupport,
/* 812  */                 support3d,
/* 813  */                 isTouch;
/* 814  */ 
/* 815  */             tempElem.style.cssText = "  -moz-transform:" + translate3D +
/* 816  */                 "; -ms-transform:" + translate3D +
/* 817  */                 "; -o-transform:" + translate3D +
/* 818  */                 "; -webkit-transform:" + translate3D +
/* 819  */                 "; transform:" + translate3D;
/* 820  */             regex = /translate3d\(0px, 0px, 0px\)/g;
/* 821  */             asSupport = tempElem.style.cssText.match(regex);
/* 822  */             support3d = (asSupport !== null && asSupport.length === 1);
/* 823  */ 
/* 824  */             isTouch = "ontouchstart" in window || window.navigator.msMaxTouchPoints;
/* 825  */ 
/* 826  */             base.browser = {
/* 827  */                 "support3d": support3d,
/* 828  */                 "isTouch": isTouch
/* 829  */             };
/* 830  */         },
/* 831  */ 
/* 832  */         moveEvents: function() {
/* 833  */             var base = this;
/* 834  */             if (base.options.mouseDrag !== false || base.options.touchDrag !== false) {
/* 835  */                 base.gestures();
/* 836  */                 base.disabledEvents();
/* 837  */             }
/* 838  */         },
/* 839  */ 
/* 840  */         eventTypes: function() {
/* 841  */             var base = this,
/* 842  */                 types = ["s", "e", "x"];
/* 843  */ 
/* 844  */             base.ev_types = {};
/* 845  */ 
/* 846  */             if (base.options.mouseDrag === true && base.options.touchDrag === true) {
/* 847  */                 types = [
/* 848  */                     "touchstart.owl mousedown.owl",
/* 849  */                     "touchmove.owl mousemove.owl",
/* 850  */                     "touchend.owl touchcancel.owl mouseup.owl"

/* owl-carousel.js */

/* 851  */                 ];
/* 852  */             } else if (base.options.mouseDrag === false && base.options.touchDrag === true) {
/* 853  */                 types = [
/* 854  */                     "touchstart.owl",
/* 855  */                     "touchmove.owl",
/* 856  */                     "touchend.owl touchcancel.owl"
/* 857  */                 ];
/* 858  */             } else if (base.options.mouseDrag === true && base.options.touchDrag === false) {
/* 859  */                 types = [
/* 860  */                     "mousedown.owl",
/* 861  */                     "mousemove.owl",
/* 862  */                     "mouseup.owl"
/* 863  */                 ];
/* 864  */             }
/* 865  */ 
/* 866  */             base.ev_types.start = types[0];
/* 867  */             base.ev_types.move = types[1];
/* 868  */             base.ev_types.end = types[2];
/* 869  */         },
/* 870  */ 
/* 871  */         disabledEvents: function() {
/* 872  */             var base = this;
/* 873  */             base.$elem.on("dragstart.owl", function(event) {
/* 874  */                 event.preventDefault();
/* 875  */             });
/* 876  */             base.$elem.on("mousedown.disableTextSelect", function(e) {
/* 877  */                 return $(e.target).is('input, textarea, select, option');
/* 878  */             });
/* 879  */         },
/* 880  */ 
/* 881  */         gestures: function() {
/* 882  */             /*jslint unparam: true*/
/* 883  */             var base = this,
/* 884  */                 locals = {
/* 885  */                     offsetX: 0,
/* 886  */                     offsetY: 0,
/* 887  */                     baseElWidth: 0,
/* 888  */                     relativePos: 0,
/* 889  */                     position: null,
/* 890  */                     minSwipe: null,
/* 891  */                     maxSwipe: null,
/* 892  */                     sliding: null,
/* 893  */                     dargging: null,
/* 894  */                     targetElement: null
/* 895  */                 };
/* 896  */ 
/* 897  */             base.isCssFinish = true;
/* 898  */ 
/* 899  */             function getTouches(event) {
/* 900  */                 if (event.touches !== undefined) {

/* owl-carousel.js */

/* 901  */                     return {
/* 902  */                         x: event.touches[0].pageX,
/* 903  */                         y: event.touches[0].pageY
/* 904  */                     };
/* 905  */                 }
/* 906  */ 
/* 907  */                 if (event.touches === undefined) {
/* 908  */                     if (event.pageX !== undefined) {
/* 909  */                         return {
/* 910  */                             x: event.pageX,
/* 911  */                             y: event.pageY
/* 912  */                         };
/* 913  */                     }
/* 914  */                     if (event.pageX === undefined) {
/* 915  */                         return {
/* 916  */                             x: event.clientX,
/* 917  */                             y: event.clientY
/* 918  */                         };
/* 919  */                     }
/* 920  */                 }
/* 921  */             }
/* 922  */ 
/* 923  */             function swapEvents(type) {
/* 924  */                 if (type === "on") {
/* 925  */                     $(document).on(base.ev_types.move, dragMove);
/* 926  */                     $(document).on(base.ev_types.end, dragEnd);
/* 927  */                 } else if (type === "off") {
/* 928  */                     $(document).off(base.ev_types.move);
/* 929  */                     $(document).off(base.ev_types.end);
/* 930  */                 }
/* 931  */             }
/* 932  */ 
/* 933  */             function dragStart(event) {
/* 934  */                 var ev = event.originalEvent || event || window.event,
/* 935  */                     position;
/* 936  */ 
/* 937  */                 if (ev.which === 3) {
/* 938  */                     return false;
/* 939  */                 }
/* 940  */                 if (base.itemsAmount <= base.options.items) {
/* 941  */                     return;
/* 942  */                 }
/* 943  */                 if (base.isCssFinish === false && !base.options.dragBeforeAnimFinish) {
/* 944  */                     return false;
/* 945  */                 }
/* 946  */                 if (base.isCss3Finish === false && !base.options.dragBeforeAnimFinish) {
/* 947  */                     return false;
/* 948  */                 }
/* 949  */ 
/* 950  */                 if (base.options.autoPlay !== false) {

/* owl-carousel.js */

/* 951  */                     window.clearInterval(base.autoPlayInterval);
/* 952  */                 }
/* 953  */ 
/* 954  */                 if (base.browser.isTouch !== true && !base.$owlWrapper.hasClass("grabbing")) {
/* 955  */                     base.$owlWrapper.addClass("grabbing");
/* 956  */                 }
/* 957  */ 
/* 958  */                 base.newPosX = 0;
/* 959  */                 base.newRelativeX = 0;
/* 960  */ 
/* 961  */                 $(this).css(base.removeTransition());
/* 962  */ 
/* 963  */                 position = $(this).position();
/* 964  */                 locals.relativePos = position.left;
/* 965  */ 
/* 966  */                 locals.offsetX = getTouches(ev).x - position.left;
/* 967  */                 locals.offsetY = getTouches(ev).y - position.top;
/* 968  */ 
/* 969  */                 swapEvents("on");
/* 970  */ 
/* 971  */                 locals.sliding = false;
/* 972  */                 locals.targetElement = ev.target || ev.srcElement;
/* 973  */             }
/* 974  */ 
/* 975  */             function dragMove(event) {
/* 976  */                 var ev = event.originalEvent || event || window.event,
/* 977  */                     minSwipe,
/* 978  */                     maxSwipe;
/* 979  */ 
/* 980  */                 base.newPosX = getTouches(ev).x - locals.offsetX;
/* 981  */                 base.newPosY = getTouches(ev).y - locals.offsetY;
/* 982  */                 base.newRelativeX = base.newPosX - locals.relativePos;
/* 983  */ 
/* 984  */                 if (typeof base.options.startDragging === "function" && locals.dragging !== true && base.newRelativeX !== 0) {
/* 985  */                     locals.dragging = true;
/* 986  */                     base.options.startDragging.apply(base, [base.$elem]);
/* 987  */                 }
/* 988  */ 
/* 989  */                 if ((base.newRelativeX > 8 || base.newRelativeX < -8) && (base.browser.isTouch === true)) {
/* 990  */                     if (ev.preventDefault !== undefined) {
/* 991  */                         ev.preventDefault();
/* 992  */                     } else {
/* 993  */                         ev.returnValue = false;
/* 994  */                     }
/* 995  */                     locals.sliding = true;
/* 996  */                 }
/* 997  */ 
/* 998  */                 if ((base.newPosY > 10 || base.newPosY < -10) && locals.sliding === false) {
/* 999  */                     $(document).off("touchmove.owl");
/* 1000 */                 }

/* owl-carousel.js */

/* 1001 */ 
/* 1002 */                 minSwipe = function() {
/* 1003 */                     return base.newRelativeX / 5;
/* 1004 */                 };
/* 1005 */ 
/* 1006 */                 maxSwipe = function() {
/* 1007 */                     return base.maximumPixels + base.newRelativeX / 5;
/* 1008 */                 };
/* 1009 */ 
/* 1010 */                 base.newPosX = Math.max(Math.min(base.newPosX, minSwipe()), maxSwipe());
/* 1011 */                 if (base.browser.support3d === true) {
/* 1012 */                     base.transition3d(base.newPosX);
/* 1013 */                 } else {
/* 1014 */                     base.css2move(base.newPosX);
/* 1015 */                 }
/* 1016 */             }
/* 1017 */ 
/* 1018 */             function dragEnd(event) {
/* 1019 */                 var ev = event.originalEvent || event || window.event,
/* 1020 */                     newPosition,
/* 1021 */                     handlers,
/* 1022 */                     owlStopEvent;
/* 1023 */ 
/* 1024 */                 ev.target = ev.target || ev.srcElement;
/* 1025 */ 
/* 1026 */                 locals.dragging = false;
/* 1027 */ 
/* 1028 */                 if (base.browser.isTouch !== true) {
/* 1029 */                     base.$owlWrapper.removeClass("grabbing");
/* 1030 */                 }
/* 1031 */ 
/* 1032 */                 if (base.newRelativeX < 0) {
/* 1033 */                     base.dragDirection = base.owl.dragDirection = "left";
/* 1034 */                 } else {
/* 1035 */                     base.dragDirection = base.owl.dragDirection = "right";
/* 1036 */                 }
/* 1037 */ 
/* 1038 */                 if (base.newRelativeX !== 0) {
/* 1039 */                     newPosition = base.getNewPosition();
/* 1040 */                     base.goTo(newPosition, false, "drag");
/* 1041 */                     if (locals.targetElement === ev.target && base.browser.isTouch !== true) {
/* 1042 */                         $(ev.target).on("click.disable", function(ev) {
/* 1043 */                             ev.stopImmediatePropagation();
/* 1044 */                             ev.stopPropagation();
/* 1045 */                             ev.preventDefault();
/* 1046 */                             $(ev.target).off("click.disable");
/* 1047 */                         });
/* 1048 */                         handlers = $._data(ev.target, "events").click;
/* 1049 */                         owlStopEvent = handlers.pop();
/* 1050 */                         handlers.splice(0, 0, owlStopEvent);

/* owl-carousel.js */

/* 1051 */                     }
/* 1052 */                 }
/* 1053 */                 swapEvents("off");
/* 1054 */             }
/* 1055 */             base.$elem.on(base.ev_types.start, ".owl-wrapper", dragStart);
/* 1056 */         },
/* 1057 */ 
/* 1058 */         getNewPosition: function() {
/* 1059 */             var base = this,
/* 1060 */                 newPosition = base.closestItem();
/* 1061 */ 
/* 1062 */             if (newPosition > base.maximumItem) {
/* 1063 */                 base.currentItem = base.maximumItem;
/* 1064 */                 newPosition = base.maximumItem;
/* 1065 */             } else if (base.newPosX >= 0) {
/* 1066 */                 newPosition = 0;
/* 1067 */                 base.currentItem = 0;
/* 1068 */             }
/* 1069 */             return newPosition;
/* 1070 */         },
/* 1071 */         closestItem: function() {
/* 1072 */             var base = this,
/* 1073 */                 array = base.options.scrollPerPage === true ? base.pagesInArray : base.positionsInArray,
/* 1074 */                 goal = base.newPosX,
/* 1075 */                 closest = null;
/* 1076 */ 
/* 1077 */             $.each(array, function(i, v) {
/* 1078 */                 if (goal - (base.itemWidth / 20) > array[i + 1] && goal - (base.itemWidth / 20) < v && base.moveDirection() === "left") {
/* 1079 */                     closest = v;
/* 1080 */                     if (base.options.scrollPerPage === true) {
/* 1081 */                         base.currentItem = $.inArray(closest, base.positionsInArray);
/* 1082 */                     } else {
/* 1083 */                         base.currentItem = i;
/* 1084 */                     }
/* 1085 */                 } else if (goal + (base.itemWidth / 20) < v && goal + (base.itemWidth / 20) > (array[i + 1] || array[i] - base.itemWidth) && base.moveDirection() === "right") {
/* 1086 */                     if (base.options.scrollPerPage === true) {
/* 1087 */                         closest = array[i + 1] || array[array.length - 1];
/* 1088 */                         base.currentItem = $.inArray(closest, base.positionsInArray);
/* 1089 */                     } else {
/* 1090 */                         closest = array[i + 1];
/* 1091 */                         base.currentItem = i + 1;
/* 1092 */                     }
/* 1093 */                 }
/* 1094 */             });
/* 1095 */             return base.currentItem;
/* 1096 */         },
/* 1097 */ 
/* 1098 */         moveDirection: function() {
/* 1099 */             var base = this,
/* 1100 */                 direction;

/* owl-carousel.js */

/* 1101 */             if (base.newRelativeX < 0) {
/* 1102 */                 direction = "right";
/* 1103 */                 base.playDirection = "next";
/* 1104 */             } else {
/* 1105 */                 direction = "left";
/* 1106 */                 base.playDirection = "prev";
/* 1107 */             }
/* 1108 */             return direction;
/* 1109 */         },
/* 1110 */ 
/* 1111 */         customEvents: function() {
/* 1112 */             /*jslint unparam: true*/
/* 1113 */             var base = this;
/* 1114 */             base.$elem.on("owl.next", function() {
/* 1115 */                 base.next();
/* 1116 */             });
/* 1117 */             base.$elem.on("owl.prev", function() {
/* 1118 */                 base.prev();
/* 1119 */             });
/* 1120 */             base.$elem.on("owl.play", function(event, speed) {
/* 1121 */                 base.options.autoPlay = speed;
/* 1122 */                 base.play();
/* 1123 */                 base.hoverStatus = "play";
/* 1124 */             });
/* 1125 */             base.$elem.on("owl.stop", function() {
/* 1126 */                 base.stop();
/* 1127 */                 base.hoverStatus = "stop";
/* 1128 */             });
/* 1129 */             base.$elem.on("owl.goTo", function(event, item) {
/* 1130 */                 base.goTo(item);
/* 1131 */             });
/* 1132 */             base.$elem.on("owl.jumpTo", function(event, item) {
/* 1133 */                 base.jumpTo(item);
/* 1134 */             });
/* 1135 */         },
/* 1136 */ 
/* 1137 */         stopOnHover: function() {
/* 1138 */             var base = this;
/* 1139 */             if (base.options.stopOnHover === true && base.browser.isTouch !== true && base.options.autoPlay !== false) {
/* 1140 */                 base.$elem.on("mouseover", function() {
/* 1141 */                     base.stop();
/* 1142 */                 });
/* 1143 */                 base.$elem.on("mouseout", function() {
/* 1144 */                     if (base.hoverStatus !== "stop") {
/* 1145 */                         base.play();
/* 1146 */                     }
/* 1147 */                 });
/* 1148 */             }
/* 1149 */         },
/* 1150 */ 

/* owl-carousel.js */

/* 1151 */         lazyLoad: function() {
/* 1152 */             var base = this,
/* 1153 */                 i,
/* 1154 */                 $item,
/* 1155 */                 itemNumber,
/* 1156 */                 $lazyImg,
/* 1157 */                 follow;
/* 1158 */ 
/* 1159 */             if (base.options.lazyLoad === false) {
/* 1160 */                 return false;
/* 1161 */             }
/* 1162 */             for (i = 0; i < base.itemsAmount; i += 1) {
/* 1163 */                 $item = $(base.$owlItems[i]);
/* 1164 */ 
/* 1165 */                 if ($item.data("owl-loaded") === "loaded") {
/* 1166 */                     continue;
/* 1167 */                 }
/* 1168 */ 
/* 1169 */                 itemNumber = $item.data("owl-item");
/* 1170 */                 $lazyImg = $item.find(".lazyOwl");
/* 1171 */ 
/* 1172 */                 if (typeof $lazyImg.data("src") !== "string") {
/* 1173 */                     $item.data("owl-loaded", "loaded");
/* 1174 */                     continue;
/* 1175 */                 }
/* 1176 */                 if ($item.data("owl-loaded") === undefined) {
/* 1177 */                     $lazyImg.hide();
/* 1178 */                     $item.addClass("loading").data("owl-loaded", "checked");
/* 1179 */                 }
/* 1180 */                 if (base.options.lazyFollow === true) {
/* 1181 */                     follow = itemNumber >= base.currentItem;
/* 1182 */                 } else {
/* 1183 */                     follow = true;
/* 1184 */                 }
/* 1185 */                 if (follow && itemNumber < base.currentItem + base.options.items && $lazyImg.length) {
/* 1186 */                     $lazyImg.each(function() {
/* 1187 */                         base.lazyPreload($item, $(this));
/* 1188 */                     });
/* 1189 */                 }
/* 1190 */             }
/* 1191 */         },
/* 1192 */ 
/* 1193 */         lazyPreload: function($item, $lazyImg) {
/* 1194 */             var base = this,
/* 1195 */                 iterations = 0,
/* 1196 */                 isBackgroundImg;
/* 1197 */ 
/* 1198 */             if ($lazyImg.prop("tagName") === "DIV") {
/* 1199 */                 $lazyImg.css("background-image", "url(" + $lazyImg.data("src") + ")");
/* 1200 */                 isBackgroundImg = true;

/* owl-carousel.js */

/* 1201 */             } else {
/* 1202 */                 $lazyImg[0].src = $lazyImg.data("src");
/* 1203 */             }
/* 1204 */ 
/* 1205 */             function showImage() {
/* 1206 */                 $item.data("owl-loaded", "loaded").removeClass("loading");
/* 1207 */                 $lazyImg.removeAttr("data-src");
/* 1208 */                 if (base.options.lazyEffect === "fade") {
/* 1209 */                     $lazyImg.fadeIn(400);
/* 1210 */                 } else {
/* 1211 */                     $lazyImg.show();
/* 1212 */                 }
/* 1213 */                 if (typeof base.options.afterLazyLoad === "function") {
/* 1214 */                     base.options.afterLazyLoad.apply(this, [base.$elem]);
/* 1215 */                 }
/* 1216 */             }
/* 1217 */ 
/* 1218 */             function checkLazyImage() {
/* 1219 */                 iterations += 1;
/* 1220 */                 if (base.completeImg($lazyImg.get(0)) || isBackgroundImg === true) {
/* 1221 */                     showImage();
/* 1222 */                 } else if (iterations <= 100) { //if image loads in less than 10 seconds 
/* 1223 */                     window.setTimeout(checkLazyImage, 100);
/* 1224 */                 } else {
/* 1225 */                     showImage();
/* 1226 */                 }
/* 1227 */             }
/* 1228 */ 
/* 1229 */             checkLazyImage();
/* 1230 */         },
/* 1231 */ 
/* 1232 */         autoHeight: function() {
/* 1233 */             var base = this,
/* 1234 */                 $currentimg = $(base.$owlItems[base.currentItem]).find("img"),
/* 1235 */                 iterations;
/* 1236 */ 
/* 1237 */             function addHeight() {
/* 1238 */                 var $currentItem = $(base.$owlItems[base.currentItem]).height();
/* 1239 */                 base.wrapperOuter.css("height", $currentItem + "px");
/* 1240 */                 if (!base.wrapperOuter.hasClass("autoHeight")) {
/* 1241 */                     window.setTimeout(function() {
/* 1242 */                         base.wrapperOuter.addClass("autoHeight");
/* 1243 */                     }, 0);
/* 1244 */                 }
/* 1245 */             }
/* 1246 */ 
/* 1247 */             function checkImage() {
/* 1248 */                 iterations += 1;
/* 1249 */                 if (base.completeImg($currentimg.get(0))) {
/* 1250 */                     addHeight();

/* owl-carousel.js */

/* 1251 */                 } else if (iterations <= 100) { //if image loads in less than 10 seconds 
/* 1252 */                     window.setTimeout(checkImage, 100);
/* 1253 */                 } else {
/* 1254 */                     base.wrapperOuter.css("height", ""); //Else remove height attribute
/* 1255 */                 }
/* 1256 */             }
/* 1257 */ 
/* 1258 */             if ($currentimg.get(0) !== undefined) {
/* 1259 */                 iterations = 0;
/* 1260 */                 checkImage();
/* 1261 */             } else {
/* 1262 */                 addHeight();
/* 1263 */             }
/* 1264 */         },
/* 1265 */ 
/* 1266 */         completeImg: function(img) {
/* 1267 */             var naturalWidthType;
/* 1268 */ 
/* 1269 */             if (!img.complete) {
/* 1270 */                 return false;
/* 1271 */             }
/* 1272 */             naturalWidthType = typeof img.naturalWidth;
/* 1273 */             if (naturalWidthType !== "undefined" && img.naturalWidth === 0) {
/* 1274 */                 return false;
/* 1275 */             }
/* 1276 */             return true;
/* 1277 */         },
/* 1278 */ 
/* 1279 */         onVisibleItems: function() {
/* 1280 */             var base = this,
/* 1281 */                 i;
/* 1282 */ 
/* 1283 */             if (base.options.addClassActive === true) {
/* 1284 */                 base.$owlItems.removeClass("active");
/* 1285 */             }
/* 1286 */             base.visibleItems = [];
/* 1287 */             for (i = base.currentItem; i < base.currentItem + base.options.items; i += 1) {
/* 1288 */                 base.visibleItems.push(i);
/* 1289 */ 
/* 1290 */                 if (base.options.addClassActive === true) {
/* 1291 */                     $(base.$owlItems[i]).addClass("active");
/* 1292 */                 }
/* 1293 */             }
/* 1294 */             base.owl.visibleItems = base.visibleItems;
/* 1295 */         },
/* 1296 */ 
/* 1297 */         transitionTypes: function(className) {
/* 1298 */             var base = this;
/* 1299 */             //Currently available: "fade", "backSlide", "goDown", "fadeUp"
/* 1300 */             base.outClass = "owl-" + className + "-out";

/* owl-carousel.js */

/* 1301 */             base.inClass = "owl-" + className + "-in";
/* 1302 */         },
/* 1303 */ 
/* 1304 */         singleItemTransition: function() {
/* 1305 */             var base = this,
/* 1306 */                 outClass = base.outClass,
/* 1307 */                 inClass = base.inClass,
/* 1308 */                 $currentItem = base.$owlItems.eq(base.currentItem),
/* 1309 */                 $prevItem = base.$owlItems.eq(base.prevItem),
/* 1310 */                 prevPos = Math.abs(base.positionsInArray[base.currentItem]) + base.positionsInArray[base.prevItem],
/* 1311 */                 origin = Math.abs(base.positionsInArray[base.currentItem]) + base.itemWidth / 2,
/* 1312 */                 animEnd = 'webkitAnimationEnd oAnimationEnd MSAnimationEnd animationend';
/* 1313 */ 
/* 1314 */             base.isTransition = true;
/* 1315 */ 
/* 1316 */             base.$owlWrapper
/* 1317 */                 .addClass('owl-origin')
/* 1318 */                 .css({
/* 1319 */                     "-webkit-transform-origin": origin + "px",
/* 1320 */                     "-moz-perspective-origin": origin + "px",
/* 1321 */                     "perspective-origin": origin + "px"
/* 1322 */                 });
/* 1323 */ 
/* 1324 */             function transStyles(prevPos) {
/* 1325 */                 return {
/* 1326 */                     "position": "relative",
/* 1327 */                     "left": prevPos + "px"
/* 1328 */                 };
/* 1329 */             }
/* 1330 */ 
/* 1331 */             $prevItem
/* 1332 */                 .css(transStyles(prevPos, 10))
/* 1333 */                 .addClass(outClass)
/* 1334 */                 .on(animEnd, function() {
/* 1335 */                     base.endPrev = true;
/* 1336 */                     $prevItem.off(animEnd);
/* 1337 */                     base.clearTransStyle($prevItem, outClass);
/* 1338 */                 });
/* 1339 */ 
/* 1340 */             $currentItem
/* 1341 */                 .addClass(inClass)
/* 1342 */                 .on(animEnd, function() {
/* 1343 */                     base.endCurrent = true;
/* 1344 */                     $currentItem.off(animEnd);
/* 1345 */                     base.clearTransStyle($currentItem, inClass);
/* 1346 */                 });
/* 1347 */         },
/* 1348 */ 
/* 1349 */         clearTransStyle: function(item, classToRemove) {
/* 1350 */             var base = this;

/* owl-carousel.js */

/* 1351 */             item.css({
/* 1352 */                 "position": "",
/* 1353 */                 "left": ""
/* 1354 */             }).removeClass(classToRemove);
/* 1355 */ 
/* 1356 */             if (base.endPrev && base.endCurrent) {
/* 1357 */                 base.$owlWrapper.removeClass('owl-origin');
/* 1358 */                 base.endPrev = false;
/* 1359 */                 base.endCurrent = false;
/* 1360 */                 base.isTransition = false;
/* 1361 */             }
/* 1362 */         },
/* 1363 */ 
/* 1364 */         owlStatus: function() {
/* 1365 */             var base = this;
/* 1366 */             base.owl = {
/* 1367 */                 "userOptions": base.userOptions,
/* 1368 */                 "baseElement": base.$elem,
/* 1369 */                 "userItems": base.$userItems,
/* 1370 */                 "owlItems": base.$owlItems,
/* 1371 */                 "currentItem": base.currentItem,
/* 1372 */                 "prevItem": base.prevItem,
/* 1373 */                 "visibleItems": base.visibleItems,
/* 1374 */                 "isTouch": base.browser.isTouch,
/* 1375 */                 "browser": base.browser,
/* 1376 */                 "dragDirection": base.dragDirection
/* 1377 */             };
/* 1378 */         },
/* 1379 */ 
/* 1380 */         clearEvents: function() {
/* 1381 */             var base = this;
/* 1382 */             base.$elem.off(".owl owl mousedown.disableTextSelect");
/* 1383 */             $(document).off(".owl owl");
/* 1384 */             $(window).off("resize", base.resizer);
/* 1385 */         },
/* 1386 */ 
/* 1387 */         unWrap: function() {
/* 1388 */             var base = this;
/* 1389 */             if (base.$elem.children().length !== 0) {
/* 1390 */                 base.$owlWrapper.unwrap();
/* 1391 */                 base.$userItems.unwrap().unwrap();
/* 1392 */                 if (base.owlControls) {
/* 1393 */                     base.owlControls.remove();
/* 1394 */                 }
/* 1395 */             }
/* 1396 */             base.clearEvents();
/* 1397 */             base.$elem.attr({
/* 1398 */                 style: base.$elem.data("owl-originalStyles") || "",
/* 1399 */                 class: base.$elem.data("owl-originalClasses")
/* 1400 */             });

/* owl-carousel.js */

/* 1401 */         },
/* 1402 */ 
/* 1403 */         destroy: function() {
/* 1404 */             var base = this;
/* 1405 */             base.stop();
/* 1406 */             window.clearInterval(base.checkVisible);
/* 1407 */             base.unWrap();
/* 1408 */             base.$elem.removeData();
/* 1409 */         },
/* 1410 */ 
/* 1411 */         reinit: function(newOptions) {
/* 1412 */             var base = this,
/* 1413 */                 options = $.extend({}, base.userOptions, newOptions);
/* 1414 */             base.unWrap();
/* 1415 */             base.init(options, base.$elem);
/* 1416 */         },
/* 1417 */ 
/* 1418 */         addItem: function(htmlString, targetPosition) {
/* 1419 */             var base = this,
/* 1420 */                 position;
/* 1421 */ 
/* 1422 */             if (!htmlString) {
/* 1423 */                 return false;
/* 1424 */             }
/* 1425 */ 
/* 1426 */             if (base.$elem.children().length === 0) {
/* 1427 */                 base.$elem.append(htmlString);
/* 1428 */                 base.setVars();
/* 1429 */                 return false;
/* 1430 */             }
/* 1431 */             base.unWrap();
/* 1432 */             if (targetPosition === undefined || targetPosition === -1) {
/* 1433 */                 position = -1;
/* 1434 */             } else {
/* 1435 */                 position = targetPosition;
/* 1436 */             }
/* 1437 */             if (position >= base.$userItems.length || position === -1) {
/* 1438 */                 base.$userItems.eq(-1).after(htmlString);
/* 1439 */             } else {
/* 1440 */                 base.$userItems.eq(position).before(htmlString);
/* 1441 */             }
/* 1442 */ 
/* 1443 */             base.setVars();
/* 1444 */         },
/* 1445 */ 
/* 1446 */         removeItem: function(targetPosition) {
/* 1447 */             var base = this,
/* 1448 */                 position;
/* 1449 */ 
/* 1450 */             if (base.$elem.children().length === 0) {

/* owl-carousel.js */

/* 1451 */                 return false;
/* 1452 */             }
/* 1453 */             if (targetPosition === undefined || targetPosition === -1) {
/* 1454 */                 position = -1;
/* 1455 */             } else {
/* 1456 */                 position = targetPosition;
/* 1457 */             }
/* 1458 */ 
/* 1459 */             base.unWrap();
/* 1460 */             base.$userItems.eq(position).remove();
/* 1461 */             base.setVars();
/* 1462 */         }
/* 1463 */ 
/* 1464 */     };
/* 1465 */ 
/* 1466 */     $.fn.owlCarousel = function(options) {
/* 1467 */         return this.each(function() {
/* 1468 */             if ($(this).data("owl-init") === true) {
/* 1469 */                 return false;
/* 1470 */             }
/* 1471 */             $(this).data("owl-init", true);
/* 1472 */             var carousel = Object.create(Carousel);
/* 1473 */             carousel.init(options, this);
/* 1474 */             $.data(this, "owlCarousel", carousel);
/* 1475 */         });
/* 1476 */     };
/* 1477 */ 
/* 1478 */     $.fn.owlCarousel.options = {
/* 1479 */ 
/* 1480 */         items: 5,
/* 1481 */         itemsCustom: false,
/* 1482 */         itemsDesktop: [1199, 4],
/* 1483 */         itemsDesktopSmall: [979, 3],
/* 1484 */         itemsTablet: [768, 2],
/* 1485 */         itemsTabletSmall: false,
/* 1486 */         itemsMobile: [479, 1],
/* 1487 */         singleItem: false,
/* 1488 */         itemsScaleUp: false,
/* 1489 */ 
/* 1490 */         slideSpeed: 200,
/* 1491 */         paginationSpeed: 800,
/* 1492 */         rewindSpeed: 1000,
/* 1493 */ 
/* 1494 */         autoPlay: false,
/* 1495 */         stopOnHover: false,
/* 1496 */ 
/* 1497 */         navigation: false,
/* 1498 */         navigationText: ["prev", "next"],
/* 1499 */         rewindNav: true,
/* 1500 */         scrollPerPage: false,

/* owl-carousel.js */

/* 1501 */ 
/* 1502 */         pagination: true,
/* 1503 */         paginationNumbers: false,
/* 1504 */ 
/* 1505 */         responsive: true,
/* 1506 */         responsiveRefreshRate: 200,
/* 1507 */         responsiveBaseWidth: window,
/* 1508 */ 
/* 1509 */         baseClass: "owl-carousel",
/* 1510 */         theme: "owl-theme",
/* 1511 */ 
/* 1512 */         lazyLoad: false,
/* 1513 */         lazyFollow: true,
/* 1514 */         lazyEffect: "fade",
/* 1515 */ 
/* 1516 */         autoHeight: false,
/* 1517 */ 
/* 1518 */         jsonPath: false,
/* 1519 */         jsonSuccess: false,
/* 1520 */ 
/* 1521 */         dragBeforeAnimFinish: true,
/* 1522 */         mouseDrag: true,
/* 1523 */         touchDrag: true,
/* 1524 */ 
/* 1525 */         addClassActive: false,
/* 1526 */         transitionStyle: false,
/* 1527 */ 
/* 1528 */         beforeUpdate: false,
/* 1529 */         afterUpdate: false,
/* 1530 */         beforeInit: false,
/* 1531 */         afterInit: false,
/* 1532 */         beforeMove: false,
/* 1533 */         afterMove: false,
/* 1534 */         afterAction: false,
/* 1535 */         startDragging: false,
/* 1536 */         afterLazyLoad: false
/* 1537 */     };
/* 1538 */ }(jQuery, window, document));
