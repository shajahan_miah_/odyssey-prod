
/* custom-script.js */

/* 1  */ jQuery(document).ready(function($) {
/* 2  */   $('.cf7-datepicker').datepicker({
/* 3  */      autoclose: true,
/* 4  */      showAnim: setting.effect,
/* 5  */      changeMonth: setting.monyearmenu,
/* 6  */      changeYear: setting.monyearmenu,
/* 7  */      showWeek: setting.showWeek,
/* 8  */   });
/* 9  */ 
/* 10 */ 
/* 11 */   //verion 1.0 fail-safe
/* 12 */   $('#cf7-datepicker').datepicker({
/* 13 */      autoclose: true,
/* 14 */      showAnim: setting.effect,
/* 15 */      changeMonth: setting.monyearmenu,
/* 16 */      changeYear: setting.monyearmenu,
/* 17 */      showWeek: setting.showWeek,
/* 18 */   });
/* 19 */ 
/* 20 */ });
