
/* bootstrap-multiselect.js */

/* 1   */ /**
/* 2   *|  * bootstrap-multiselect.js
/* 3   *|  * https://github.com/davidstutz/bootstrap-multiselect
/* 4   *|  *
/* 5   *|  * Copyright 2012 - 2014 David Stutz
/* 6   *|  *
/* 7   *|  * Dual licensed under the BSD-3-Clause and the Apache License, Version 2.0.
/* 8   *|  */
/* 9   */ !function($) {
/* 10  */ 
/* 11  */     "use strict";// jshint ;_;
/* 12  */ 
/* 13  */     if (typeof ko !== 'undefined' && ko.bindingHandlers && !ko.bindingHandlers.multiselect) {
/* 14  */         ko.bindingHandlers.multiselect = {
/* 15  */ 
/* 16  */             init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
/* 17  */ 
/* 18  */                 var listOfSelectedItems = allBindingsAccessor().selectedOptions,
/* 19  */                     config = ko.utils.unwrapObservable(valueAccessor());
/* 20  */ 
/* 21  */                 $(element).multiselect(config);
/* 22  */ 
/* 23  */                 if (isObservableArray(listOfSelectedItems)) {
/* 24  */                     // Subscribe to the selectedOptions: ko.observableArray
/* 25  */                     listOfSelectedItems.subscribe(function (changes) {
/* 26  */                         var addedArray = [], deletedArray = [];
/* 27  */                         changes.forEach(function (change) {
/* 28  */                             switch (change.status) {
/* 29  */                                 case 'added':
/* 30  */                                     addedArray.push(change.value);
/* 31  */                                     break;
/* 32  */                                 case 'deleted':
/* 33  */                                     deletedArray.push(change.value);
/* 34  */                                     break;
/* 35  */                             }
/* 36  */                         });
/* 37  */                         if (addedArray.length > 0) {
/* 38  */                             $(element).multiselect('select', addedArray);
/* 39  */                         };
/* 40  */                         if (deletedArray.length > 0) {
/* 41  */                             $(element).multiselect('deselect', deletedArray);
/* 42  */                         };
/* 43  */                     }, null, "arrayChange");
/* 44  */                 }
/* 45  */             },
/* 46  */ 
/* 47  */             update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
/* 48  */ 
/* 49  */                 var listOfItems = allBindingsAccessor().options,
/* 50  */                     ms = $(element).data('multiselect'),

/* bootstrap-multiselect.js */

/* 51  */                     config = ko.utils.unwrapObservable(valueAccessor());
/* 52  */ 
/* 53  */                 if (isObservableArray(listOfItems)) {
/* 54  */                     // Subscribe to the options: ko.observableArray incase it changes later
/* 55  */                     listOfItems.subscribe(function (theArray) {
/* 56  */                         $(element).multiselect('rebuild');
/* 57  */                     });
/* 58  */                 }
/* 59  */ 
/* 60  */                 if (!ms) {
/* 61  */                     $(element).multiselect(config);
/* 62  */                 }
/* 63  */                 else {
/* 64  */                     ms.updateOriginalOptions();
/* 65  */                 }
/* 66  */             }
/* 67  */         };
/* 68  */     }
/* 69  */ 
/* 70  */     function isObservableArray(obj) {
/* 71  */         return ko.isObservable(obj) && !(obj.destroyAll === undefined);
/* 72  */     }
/* 73  */ 
/* 74  */     /**
/* 75  *|      * Constructor to create a new multiselect using the given select.
/* 76  *|      * 
/* 77  *|      * @param {jQuery} select
/* 78  *|      * @param {Object} options
/* 79  *|      * @returns {Multiselect}
/* 80  *|      */
/* 81  */     function Multiselect(select, options) {
/* 82  */ 
/* 83  */         this.options = this.mergeOptions(options);
/* 84  */         this.$select = $(select);
/* 85  */ 
/* 86  */         // Initialization.
/* 87  */         // We have to clone to create a new reference.
/* 88  */         this.originalOptions = this.$select.clone()[0].options;
/* 89  */         this.query = '';
/* 90  */         this.searchTimeout = null;
/* 91  */ 
/* 92  */         this.options.multiple = this.$select.attr('multiple') === "multiple";
/* 93  */         this.options.onChange = $.proxy(this.options.onChange, this);
/* 94  */         this.options.onDropdownShow = $.proxy(this.options.onDropdownShow, this);
/* 95  */         this.options.onDropdownHide = $.proxy(this.options.onDropdownHide, this);
/* 96  */ 
/* 97  */         // Build select all if enabled.
/* 98  */         this.buildContainer();
/* 99  */         this.buildButton();
/* 100 */         this.buildSelectAll();

/* bootstrap-multiselect.js */

/* 101 */         this.buildDropdown();
/* 102 */         this.buildDropdownOptions();
/* 103 */         this.buildFilter();
/* 104 */         
/* 105 */         this.updateButtonText();
/* 106 */         this.updateSelectAll();
/* 107 */         
/* 108 */         this.$select.hide().after(this.$container);
/* 109 */     };
/* 110 */ 
/* 111 */     Multiselect.prototype = {
/* 112 */ 
/* 113 */         defaults: {
/* 114 */             /**
/* 115 *|              * Default text function will either print 'None selected' in case no
/* 116 *|              * option is selected or a list of the selected options up to a length of 3 selected options.
/* 117 *|              * 
/* 118 *|              * @param {jQuery} options
/* 119 *|              * @param {jQuery} select
/* 120 *|              * @returns {String}
/* 121 *|              */
/* 122 */             buttonText: function(options, select) {
/* 123 */                 if (options.length === 0) {
/* 124 */                     return this.nonSelectedText + ' <b class="caret"></b>';
/* 125 */                 }
/* 126 */                 else {
/* 127 */                     if (options.length > this.numberDisplayed) {
/* 128 */                         return options.length + ' ' + this.nSelectedText + ' <b class="caret"></b>';
/* 129 */                     }
/* 130 */                     else {
/* 131 */                         var selected = '';
/* 132 */                         options.each(function() {
/* 133 */                             var label = ($(this).attr('label') !== undefined) ? $(this).attr('label') : $(this).html();
/* 134 */ 
/* 135 */                             selected += label + ', ';
/* 136 */                         });
/* 137 */                         return selected.substr(0, selected.length - 2) + ' <b class="caret"></b>';
/* 138 */                     }
/* 139 */                 }
/* 140 */             },
/* 141 */             /**
/* 142 *|              * Updates the title of the button similar to the buttonText function.
/* 143 *|              * @param {jQuery} options
/* 144 *|              * @param {jQuery} select
/* 145 *|              * @returns {@exp;selected@call;substr}
/* 146 *|              */
/* 147 */             buttonTitle: function(options, select) {
/* 148 */                 if (options.length === 0) {
/* 149 */                     return this.nonSelectedText;
/* 150 */                 }

/* bootstrap-multiselect.js */

/* 151 */                 else {
/* 152 */                     var selected = '';
/* 153 */                     options.each(function () {
/* 154 */                         selected += $(this).text() + ', ';
/* 155 */                     });
/* 156 */                     return selected.substr(0, selected.length - 2);
/* 157 */                 }
/* 158 */             },
/* 159 */             /**
/* 160 *|              * Create a label.
/* 161 *|              * 
/* 162 *|              * @param {jQuery} element
/* 163 *|              * @returns {String}
/* 164 *|              */
/* 165 */             label: function(element){
/* 166 */                 return $(element).attr('label') || $(element).html();
/* 167 */             },
/* 168 */             /**
/* 169 *|              * Triggered on change of the multiselect.
/* 170 *|              * Not triggered when selecting/deselecting options manually.
/* 171 *|              * 
/* 172 *|              * @param {jQuery} option
/* 173 *|              * @param {Boolean} checked
/* 174 *|              */
/* 175 */             onChange : function(option, checked) {
/* 176 */ 
/* 177 */             },
/* 178 */             /**
/* 179 *|              * Triggered when the dropdown is shown.
/* 180 *|              * 
/* 181 *|              * @param {jQuery} event
/* 182 *|              */
/* 183 */             onDropdownShow: function(event) {
/* 184 */ 
/* 185 */             },
/* 186 */             /**
/* 187 *|              * Triggered when the dropdown is hidden.
/* 188 *|              * 
/* 189 *|              * @param {jQuery} event
/* 190 *|              */
/* 191 */             onDropdownHide: function(event) {
/* 192 */ 
/* 193 */             },
/* 194 */             buttonClass: 'btn btn-default',
/* 195 */             dropRight: false,
/* 196 */             selectedClass: 'active',
/* 197 */             buttonWidth: 'auto',
/* 198 */             buttonContainer: '<div class="btn-group" />',
/* 199 */             // Maximum height of the dropdown menu.
/* 200 */             // If maximum height is exceeded a scrollbar will be displayed.

/* bootstrap-multiselect.js */

/* 201 */             maxHeight: false,
/* 202 */             includeSelectAllOption: false,
/* 203 */             selectAllText: ' Select all',
/* 204 */             selectAllValue: 'multiselect-all',
/* 205 */             enableFiltering: false,
/* 206 */             enableCaseInsensitiveFiltering: false,
/* 207 */             filterPlaceholder: 'Search',
/* 208 */             // possible options: 'text', 'value', 'both'
/* 209 */             filterBehavior: 'text',
/* 210 */             preventInputChangeEvent: false,
/* 211 */             nonSelectedText: 'None selected',
/* 212 */             nSelectedText: 'selected',
/* 213 */             numberDisplayed: 3
/* 214 */         },
/* 215 */ 
/* 216 */         templates: {
/* 217 */             button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"></button>',
/* 218 */             ul: '<ul class="multiselect-container dropdown-menu"></ul>',
/* 219 */             filter: '<div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span><input class="form-control multiselect-search" type="text"></div>',
/* 220 */             li: '<li><a href="javascript:void(0);"><label></label></a></li>',
/* 221 */             divider: '<li class="divider"></li>',
/* 222 */             liGroup: '<li><label class="multiselect-group"></label></li>'
/* 223 */         },
/* 224 */ 
/* 225 */         constructor: Multiselect,
/* 226 */ 
/* 227 */         /**
/* 228 *|          * Builds the container of the multiselect.
/* 229 *|          */
/* 230 */         buildContainer: function() {
/* 231 */             this.$container = $(this.options.buttonContainer);
/* 232 */             this.$container.on('show.bs.dropdown', this.options.onDropdownShow);
/* 233 */             this.$container.on('hide.bs.dropdown', this.options.onDropdownHide);
/* 234 */         },
/* 235 */ 
/* 236 */         /**
/* 237 *|          * Builds the button of the multiselect.
/* 238 *|          */
/* 239 */         buildButton: function() {
/* 240 */             this.$button = $(this.templates.button).addClass(this.options.buttonClass);
/* 241 */ 
/* 242 */             // Adopt active state.
/* 243 */             if (this.$select.prop('disabled')) {
/* 244 */                 this.disable();
/* 245 */             }
/* 246 */             else {
/* 247 */                 this.enable();
/* 248 */             }
/* 249 */ 
/* 250 */             // Manually add button width if set.

/* bootstrap-multiselect.js */

/* 251 */             if (this.options.buttonWidth && this.options.buttonWidth != 'auto') {
/* 252 */                 this.$button.css({
/* 253 */                     'width' : this.options.buttonWidth
/* 254 */                 });
/* 255 */             }
/* 256 */ 
/* 257 */             // Keep the tab index from the select.
/* 258 */             var tabindex = this.$select.attr('tabindex');
/* 259 */             if (tabindex) {
/* 260 */                 this.$button.attr('tabindex', tabindex);
/* 261 */             }
/* 262 */ 
/* 263 */             this.$container.prepend(this.$button);
/* 264 */         },
/* 265 */ 
/* 266 */         /**
/* 267 *|          * Builds the ul representing the dropdown menu.
/* 268 *|          */
/* 269 */         buildDropdown: function() {
/* 270 */ 
/* 271 */             // Build ul.
/* 272 */             this.$ul = $(this.templates.ul);
/* 273 */ 
/* 274 */             if (this.options.dropRight) {
/* 275 */                 this.$ul.addClass('pull-right');
/* 276 */             }
/* 277 */ 
/* 278 */             // Set max height of dropdown menu to activate auto scrollbar.
/* 279 */             if (this.options.maxHeight) {
/* 280 */                 // TODO: Add a class for this option to move the css declarations.
/* 281 */                 this.$ul.css({
/* 282 */                     'max-height': this.options.maxHeight + 'px',
/* 283 */                     'overflow-y': 'auto',
/* 284 */                     'overflow-x': 'hidden'
/* 285 */                 });
/* 286 */             }
/* 287 */ 
/* 288 */             this.$container.append(this.$ul);
/* 289 */         },
/* 290 */ 
/* 291 */         /**
/* 292 *|          * Build the dropdown options and binds all nessecary events.
/* 293 *|          * Uses createDivider and createOptionValue to create the necessary options.
/* 294 *|          */
/* 295 */         buildDropdownOptions: function() {
/* 296 */ 
/* 297 */             this.$select.children().each($.proxy(function(index, element) {
/* 298 */                 
/* 299 */                 // Support optgroups and options without a group simultaneously.
/* 300 */                 var tag = $(element).prop('tagName')

/* bootstrap-multiselect.js */

/* 301 */                     .toLowerCase();
/* 302 */ 
/* 303 */                 if (tag === 'optgroup') {
/* 304 */                     this.createOptgroup(element);
/* 305 */                 }
/* 306 */                 else if (tag === 'option') {
/* 307 */ 
/* 308 */                     if ($(element).data('role') === 'divider') {
/* 309 */                         this.createDivider();
/* 310 */                     }
/* 311 */                     else {
/* 312 */                         this.createOptionValue(element);
/* 313 */                     }
/* 314 */ 
/* 315 */                 }
/* 316 */                 
/* 317 */                 // Other illegal tags will be ignored.
/* 318 */             }, this));
/* 319 */ 
/* 320 */             // Bind the change event on the dropdown elements.
/* 321 */             $('li input', this.$ul).on('change', $.proxy(function(event) {
/* 322 */                 var checked = $(event.target).prop('checked') || false;
/* 323 */                 var isSelectAllOption = $(event.target).val() === this.options.selectAllValue;
/* 324 */ 
/* 325 */                 // Apply or unapply the configured selected class.
/* 326 */                 if (this.options.selectedClass) {
/* 327 */                     if (checked) {
/* 328 */                         $(event.target).parents('li')
/* 329 */                             .addClass(this.options.selectedClass);
/* 330 */                     }
/* 331 */                     else {
/* 332 */                         $(event.target).parents('li')
/* 333 */                             .removeClass(this.options.selectedClass);
/* 334 */                     }
/* 335 */                 }
/* 336 */ 
/* 337 */                 // Get the corresponding option.
/* 338 */                 var value = $(event.target).val();
/* 339 */                 var $option = this.getOptionByValue(value);
/* 340 */ 
/* 341 */                 var $optionsNotThis = $('option', this.$select).not($option);
/* 342 */                 var $checkboxesNotThis = $('input', this.$container).not($(event.target));
/* 343 */ 
/* 344 */                 if (isSelectAllOption) {
/* 345 */                     if (this.$select[0][0].value === this.options.selectAllValue) {
/* 346 */                         var values = [];
/* 347 */                         var options = $('option[value!="' + this.options.selectAllValue + '"]', this.$select);
/* 348 */                         for (var i = 0; i < options.length; i++) {
/* 349 */                             // Additionally check whether the option is visible within the dropcown.
/* 350 */                             if (options[i].value !== this.options.selectAllValue && this.getInputByValue(options[i].value).is(':visible')) {

/* bootstrap-multiselect.js */

/* 351 */                                 values.push(options[i].value);
/* 352 */                             }
/* 353 */                         }
/* 354 */ 
/* 355 */                         if (checked) {
/* 356 */                             this.select(values);
/* 357 */                         }
/* 358 */                         else {
/* 359 */                             this.deselect(values);
/* 360 */                         }
/* 361 */                     }
/* 362 */                 }
/* 363 */ 
/* 364 */                 if (checked) {
/* 365 */                     $option.prop('selected', true);
/* 366 */ 
/* 367 */                     if (this.options.multiple) {
/* 368 */                         // Simply select additional option.
/* 369 */                         $option.prop('selected', true);
/* 370 */                     }
/* 371 */                     else {
/* 372 */                         // Unselect all other options and corresponding checkboxes.
/* 373 */                         if (this.options.selectedClass) {
/* 374 */                             $($checkboxesNotThis).parents('li').removeClass(this.options.selectedClass);
/* 375 */                         }
/* 376 */ 
/* 377 */                         $($checkboxesNotThis).prop('checked', false);
/* 378 */                         $optionsNotThis.prop('selected', false);
/* 379 */ 
/* 380 */                         // It's a single selection, so close.
/* 381 */                         this.$button.click();
/* 382 */                     }
/* 383 */ 
/* 384 */                     if (this.options.selectedClass === "active") {
/* 385 */                         $optionsNotThis.parents("a").css("outline", "");
/* 386 */                     }
/* 387 */                 }
/* 388 */                 else {
/* 389 */                     // Unselect option.
/* 390 */                     $option.prop('selected', false);
/* 391 */                 }
/* 392 */ 
/* 393 */                 this.$select.change();
/* 394 */                 this.options.onChange($option, checked);
/* 395 */                 
/* 396 */                 this.updateButtonText();
/* 397 */                 this.updateSelectAll();
/* 398 */ 
/* 399 */                 if(this.options.preventInputChangeEvent) {
/* 400 */                     return false;

/* bootstrap-multiselect.js */

/* 401 */                 }
/* 402 */             }, this));
/* 403 */ 
/* 404 */             $('li a', this.$ul).on('touchstart click', function(event) {
/* 405 */                 event.stopPropagation();
/* 406 */ 
/* 407 */                 if (event.shiftKey) {
/* 408 */                     var checked = $(event.target).prop('checked') || false;
/* 409 */ 
/* 410 */                     if (checked) {
/* 411 */                         var prev = $(event.target).parents('li:last')
/* 412 */                             .siblings('li[class="active"]:first');
/* 413 */ 
/* 414 */                         var currentIdx = $(event.target).parents('li')
/* 415 */                             .index();
/* 416 */                         var prevIdx = prev.index();
/* 417 */ 
/* 418 */                         if (currentIdx > prevIdx) {
/* 419 */                             $(event.target).parents("li:last").prevUntil(prev).each(
/* 420 */                                 function() {
/* 421 */                                     $(this).find("input:first").prop("checked", true)
/* 422 */                                         .trigger("change");
/* 423 */                                 }
/* 424 */                             );
/* 425 */                         }
/* 426 */                         else {
/* 427 */                             $(event.target).parents("li:last").nextUntil(prev).each(
/* 428 */                                 function() {
/* 429 */                                     $(this).find("input:first").prop("checked", true)
/* 430 */                                         .trigger("change");
/* 431 */                                 }
/* 432 */                             );
/* 433 */                         }
/* 434 */                     }
/* 435 */                 }
/* 436 */ 
/* 437 */                 $(event.target).blur();
/* 438 */             });
/* 439 */ 
/* 440 */             // Keyboard support.
/* 441 */             this.$container.on('keydown', $.proxy(function(event) {
/* 442 */                 if ($('input[type="text"]', this.$container).is(':focus')) {
/* 443 */                     return;
/* 444 */                 }
/* 445 */                 if ((event.keyCode === 9 || event.keyCode === 27)
/* 446 */                         && this.$container.hasClass('open')) {
/* 447 */                     
/* 448 */                     // Close on tab or escape.
/* 449 */                     this.$button.click();
/* 450 */                 }

/* bootstrap-multiselect.js */

/* 451 */                 else {
/* 452 */                     var $items = $(this.$container).find("li:not(.divider):visible a");
/* 453 */ 
/* 454 */                     if (!$items.length) {
/* 455 */                         return;
/* 456 */                     }
/* 457 */ 
/* 458 */                     var index = $items.index($items.filter(':focus'));
/* 459 */ 
/* 460 */                     // Navigation up.
/* 461 */                     if (event.keyCode === 38 && index > 0) {
/* 462 */                         index--;
/* 463 */                     }
/* 464 */                     // Navigate down.
/* 465 */                     else if (event.keyCode === 40 && index < $items.length - 1) {
/* 466 */                         index++;
/* 467 */                     }
/* 468 */                     else if (!~index) {
/* 469 */                         index = 0;
/* 470 */                     }
/* 471 */ 
/* 472 */                     var $current = $items.eq(index);
/* 473 */                     $current.focus();
/* 474 */ 
/* 475 */                     if (event.keyCode === 32 || event.keyCode === 13) {
/* 476 */                         var $checkbox = $current.find('input');
/* 477 */ 
/* 478 */                         $checkbox.prop("checked", !$checkbox.prop("checked"));
/* 479 */                         $checkbox.change();
/* 480 */                     }
/* 481 */ 
/* 482 */                     event.stopPropagation();
/* 483 */                     event.preventDefault();
/* 484 */                 }
/* 485 */             }, this));
/* 486 */         },
/* 487 */ 
/* 488 */         /**
/* 489 *|          * Create an option using the given select option.
/* 490 *|          * 
/* 491 *|          * @param {jQuery} element
/* 492 *|          */
/* 493 */         createOptionValue: function(element) {
/* 494 */             if ($(element).is(':selected')) {
/* 495 */                 $(element).prop('selected', true);
/* 496 */             }
/* 497 */ 
/* 498 */             // Support the label attribute on options.
/* 499 */             var label = this.options.label(element);
/* 500 */             var value = $(element).val();

/* bootstrap-multiselect.js */

/* 501 */             var inputType = this.options.multiple ? "checkbox" : "radio";
/* 502 */ 
/* 503 */             var $li = $(this.templates.li);
/* 504 */             $('label', $li).addClass(inputType);
/* 505 */             $('label', $li).append('<input type="' + inputType + '" />');
/* 506 */ 
/* 507 */             var selected = $(element).prop('selected') || false;
/* 508 */             var $checkbox = $('input', $li);
/* 509 */             $checkbox.val(value);
/* 510 */ 
/* 511 */             if (value === this.options.selectAllValue) {
/* 512 */                 $checkbox.parent().parent()
/* 513 */                     .addClass('multiselect-all');
/* 514 */             }
/* 515 */ 
/* 516 */             $('label', $li).append(" " + label);
/* 517 */ 
/* 518 */             this.$ul.append($li);
/* 519 */ 
/* 520 */             if ($(element).is(':disabled')) {
/* 521 */                 $checkbox.attr('disabled', 'disabled')
/* 522 */                     .prop('disabled', true)
/* 523 */                     .parents('li')
/* 524 */                     .addClass('disabled');
/* 525 */             }
/* 526 */ 
/* 527 */             $checkbox.prop('checked', selected);
/* 528 */ 
/* 529 */             if (selected && this.options.selectedClass) {
/* 530 */                 $checkbox.parents('li')
/* 531 */                     .addClass(this.options.selectedClass);
/* 532 */             }
/* 533 */         },
/* 534 */ 
/* 535 */         /**
/* 536 *|          * Creates a divider using the given select option.
/* 537 *|          * 
/* 538 *|          * @param {jQuery} element
/* 539 *|          */
/* 540 */         createDivider: function(element) {
/* 541 */             var $divider = $(this.templates.divider);
/* 542 */             this.$ul.append($divider);
/* 543 */         },
/* 544 */ 
/* 545 */         /**
/* 546 *|          * Creates an optgroup.
/* 547 *|          * 
/* 548 *|          * @param {jQuery} group
/* 549 *|          */
/* 550 */         createOptgroup: function(group) {

/* bootstrap-multiselect.js */

/* 551 */             var groupName = $(group).prop('label');
/* 552 */ 
/* 553 */             // Add a header for the group.
/* 554 */             var $li = $(this.templates.liGroup);
/* 555 */             $('label', $li).text(groupName);
/* 556 */ 
/* 557 */             this.$ul.append($li);
/* 558 */ 
/* 559 */             if ($(group).is(':disabled')) {
/* 560 */                 $li.addClass('disabled');
/* 561 */             }
/* 562 */ 
/* 563 */             // Add the options of the group.
/* 564 */             $('option', group).each($.proxy(function(index, element) {
/* 565 */                 this.createOptionValue(element);
/* 566 */             }, this));
/* 567 */         },
/* 568 */ 
/* 569 */         /**
/* 570 *|          * Build the selct all.
/* 571 *|          * Checks if a select all ahs already been created.
/* 572 *|          */
/* 573 */         buildSelectAll: function() {
/* 574 */             var alreadyHasSelectAll = this.hasSelectAll();
/* 575 */             
/* 576 */             // If options.includeSelectAllOption === true, add the include all checkbox.
/* 577 */             if (this.options.includeSelectAllOption && this.options.multiple && !alreadyHasSelectAll) {
/* 578 */                 if (this.options.includeSelectAllDivider) {
/* 579 */                     this.$select.prepend('<option value="" disabled="disabled" data-role="divider">');
/* 580 */                 }
/* 581 */                 this.$select.prepend('<option value="' + this.options.selectAllValue + '">' + this.options.selectAllText + '</option>');
/* 582 */             }
/* 583 */         },
/* 584 */ 
/* 585 */         /**
/* 586 *|          * Builds the filter.
/* 587 *|          */
/* 588 */         buildFilter: function() {
/* 589 */ 
/* 590 */             // Build filter if filtering OR case insensitive filtering is enabled and the number of options exceeds (or equals) enableFilterLength.
/* 591 */             if (this.options.enableFiltering || this.options.enableCaseInsensitiveFiltering) {
/* 592 */                 var enableFilterLength = Math.max(this.options.enableFiltering, this.options.enableCaseInsensitiveFiltering);
/* 593 */ 
/* 594 */                 if (this.$select.find('option').length >= enableFilterLength) {
/* 595 */ 
/* 596 */                     this.$filter = $(this.templates.filter);
/* 597 */                     $('input', this.$filter).attr('placeholder', this.options.filterPlaceholder);
/* 598 */                     this.$ul.prepend(this.$filter);
/* 599 */ 
/* 600 */                     this.$filter.val(this.query).on('click', function(event) {

/* bootstrap-multiselect.js */

/* 601 */                         event.stopPropagation();
/* 602 */                     }).on('input keydown', $.proxy(function(event) {
/* 603 */                         // This is useful to catch "keydown" events after the browser has updated the control.
/* 604 */                         clearTimeout(this.searchTimeout);
/* 605 */ 
/* 606 */                         this.searchTimeout = this.asyncFunction($.proxy(function() {
/* 607 */ 
/* 608 */                             if (this.query !== event.target.value) {
/* 609 */                                 this.query = event.target.value;
/* 610 */ 
/* 611 */                                 $.each($('li', this.$ul), $.proxy(function(index, element) {
/* 612 */                                     var value = $('input', element).val();
/* 613 */                                     var text = $('label', element).text();
/* 614 */ 
/* 615 */                                     if (value !== this.options.selectAllValue && text) {
/* 616 */                                         // by default lets assume that element is not
/* 617 */                                         // interesting for this search
/* 618 */                                         var showElement = false;
/* 619 */ 
/* 620 */                                         var filterCandidate = '';
/* 621 */                                         if ((this.options.filterBehavior === 'text' || this.options.filterBehavior === 'both')) {
/* 622 */                                             filterCandidate = text;
/* 623 */                                         }
/* 624 */                                         if ((this.options.filterBehavior === 'value' || this.options.filterBehavior === 'both')) {
/* 625 */                                             filterCandidate = value;
/* 626 */                                         }
/* 627 */ 
/* 628 */                                         if (this.options.enableCaseInsensitiveFiltering && filterCandidate.toLowerCase().indexOf(this.query.toLowerCase()) > -1) {
/* 629 */                                             showElement = true;
/* 630 */                                         }
/* 631 */                                         else if (filterCandidate.indexOf(this.query) > -1) {
/* 632 */                                             showElement = true;
/* 633 */                                         }
/* 634 */ 
/* 635 */                                         if (showElement) {
/* 636 */                                             $(element).show();
/* 637 */                                         }
/* 638 */                                         else {
/* 639 */                                             $(element).hide();
/* 640 */                                         }
/* 641 */                                     }
/* 642 */                                 }, this));
/* 643 */                             }
/* 644 */ 
/* 645 */                             // TODO: check whether select all option needs to be updated.
/* 646 */                         }, this), 300, this);
/* 647 */                     }, this));
/* 648 */                 }
/* 649 */             }
/* 650 */         },

/* bootstrap-multiselect.js */

/* 651 */ 
/* 652 */         /**
/* 653 *|          * Unbinds the whole plugin.
/* 654 *|          */
/* 655 */         destroy: function() {
/* 656 */             this.$container.remove();
/* 657 */             this.$select.show();
/* 658 */         },
/* 659 */ 
/* 660 */         /**
/* 661 *|          * Refreshs the multiselect based on the selected options of the select.
/* 662 *|          */
/* 663 */         refresh: function() {
/* 664 */             $('option', this.$select).each($.proxy(function(index, element) {
/* 665 */                 var $input = $('li input', this.$ul).filter(function() {
/* 666 */                     return $(this).val() === $(element).val();
/* 667 */                 });
/* 668 */ 
/* 669 */                 if ($(element).is(':selected')) {
/* 670 */                     $input.prop('checked', true);
/* 671 */ 
/* 672 */                     if (this.options.selectedClass) {
/* 673 */                         $input.parents('li')
/* 674 */                             .addClass(this.options.selectedClass);
/* 675 */                     }
/* 676 */                 }
/* 677 */                 else {
/* 678 */                     $input.prop('checked', false);
/* 679 */ 
/* 680 */                     if (this.options.selectedClass) {
/* 681 */                         $input.parents('li')
/* 682 */                             .removeClass(this.options.selectedClass);
/* 683 */                     }
/* 684 */                 }
/* 685 */ 
/* 686 */                 if ($(element).is(":disabled")) {
/* 687 */                     $input.attr('disabled', 'disabled')
/* 688 */                         .prop('disabled', true)
/* 689 */                         .parents('li')
/* 690 */                         .addClass('disabled');
/* 691 */                 }
/* 692 */                 else {
/* 693 */                     $input.prop('disabled', false)
/* 694 */                         .parents('li')
/* 695 */                         .removeClass('disabled');
/* 696 */                 }
/* 697 */             }, this));
/* 698 */ 
/* 699 */             this.updateButtonText();
/* 700 */             this.updateSelectAll();

/* bootstrap-multiselect.js */

/* 701 */         },
/* 702 */ 
/* 703 */         /**
/* 704 *|          * Select all options of the given values.
/* 705 *|          * 
/* 706 *|          * @param {Array} selectValues
/* 707 *|          */
/* 708 */         select: function(selectValues) {
/* 709 */             if(selectValues && !$.isArray(selectValues)) {
/* 710 */                 selectValues = [selectValues];
/* 711 */             }
/* 712 */ 
/* 713 */             for (var i = 0; i < selectValues.length; i++) {
/* 714 */                 var value = selectValues[i];
/* 715 */ 
/* 716 */                 var $option = this.getOptionByValue(value);
/* 717 */                 var $checkbox = this.getInputByValue(value);
/* 718 */ 
/* 719 */                 if (this.options.selectedClass) {
/* 720 */                     $checkbox.parents('li')
/* 721 */                         .addClass(this.options.selectedClass);
/* 722 */                 }
/* 723 */ 
/* 724 */                 $checkbox.prop('checked', true);
/* 725 */                 $option.prop('selected', true);
/* 726 */             }
/* 727 */ 
/* 728 */             this.updateButtonText();
/* 729 */         },
/* 730 */ 
/* 731 */         /**
/* 732 *|          * Clears all selected items
/* 733 *|          * 
/* 734 *|          */
/* 735 */         clearSelection: function () {
/* 736 */ 
/* 737 */             var selected = this.getSelected();
/* 738 */ 
/* 739 */             if (selected.length) {
/* 740 */ 
/* 741 */                 var arry = [];
/* 742 */ 
/* 743 */                 for (var i = 0; i < selected.length; i = i + 1) {
/* 744 */                     arry.push(selected[i].value);
/* 745 */                 }
/* 746 */ 
/* 747 */                 this.deselect(arry);
/* 748 */                 this.$select.change();
/* 749 */             }
/* 750 */         },

/* bootstrap-multiselect.js */

/* 751 */ 
/* 752 */         /**
/* 753 *|          * Deselects all options of the given values.
/* 754 *|          * 
/* 755 *|          * @param {Array} deselectValues
/* 756 *|          */
/* 757 */         deselect: function(deselectValues) {
/* 758 */             if(deselectValues && !$.isArray(deselectValues)) {
/* 759 */                 deselectValues = [deselectValues];
/* 760 */             }
/* 761 */ 
/* 762 */             for (var i = 0; i < deselectValues.length; i++) {
/* 763 */ 
/* 764 */                 var value = deselectValues[i];
/* 765 */ 
/* 766 */                 var $option = this.getOptionByValue(value);
/* 767 */                 var $checkbox = this.getInputByValue(value);
/* 768 */ 
/* 769 */                 if (this.options.selectedClass) {
/* 770 */                     $checkbox.parents('li')
/* 771 */                         .removeClass(this.options.selectedClass);
/* 772 */                 }
/* 773 */ 
/* 774 */                 $checkbox.prop('checked', false);
/* 775 */                 $option.prop('selected', false);
/* 776 */             }
/* 777 */ 
/* 778 */             this.updateButtonText();
/* 779 */         },
/* 780 */ 
/* 781 */         /**
/* 782 *|          * Rebuild the plugin.
/* 783 *|          * Rebuilds the dropdown, the filter and the select all option.
/* 784 *|          */
/* 785 */         rebuild: function() {
/* 786 */             this.$ul.html('');
/* 787 */ 
/* 788 */             // Remove select all option in select.
/* 789 */             $('option[value="' + this.options.selectAllValue + '"]', this.$select).remove();
/* 790 */ 
/* 791 */             // Important to distinguish between radios and checkboxes.
/* 792 */             this.options.multiple = this.$select.attr('multiple') === "multiple";
/* 793 */ 
/* 794 */             this.buildSelectAll();
/* 795 */             this.buildDropdownOptions();
/* 796 */             this.buildFilter();
/* 797 */             
/* 798 */             this.updateButtonText();
/* 799 */             this.updateSelectAll();
/* 800 */         },

/* bootstrap-multiselect.js */

/* 801 */ 
/* 802 */         /**
/* 803 *|          * The provided data will be used to build the dropdown.
/* 804 *|          * 
/* 805 *|          * @param {Array} dataprovider
/* 806 *|          */
/* 807 */         dataprovider: function(dataprovider) {
/* 808 */             var optionDOM = "";
/* 809 */             dataprovider.forEach(function (option) {
/* 810 */                 optionDOM += '<option value="' + option.value + '">' + option.label + '</option>';
/* 811 */             });
/* 812 */ 
/* 813 */             this.$select.html(optionDOM);
/* 814 */             this.rebuild();
/* 815 */         },
/* 816 */ 
/* 817 */         /**
/* 818 *|          * Enable the multiselect.
/* 819 *|          */
/* 820 */         enable: function() {
/* 821 */             this.$select.prop('disabled', false);
/* 822 */             this.$button.prop('disabled', false)
/* 823 */                 .removeClass('disabled');
/* 824 */         },
/* 825 */ 
/* 826 */         /**
/* 827 *|          * Disable the multiselect.
/* 828 *|          */
/* 829 */         disable: function() {
/* 830 */             this.$select.prop('disabled', true);
/* 831 */             this.$button.prop('disabled', true)
/* 832 */                 .addClass('disabled');
/* 833 */         },
/* 834 */ 
/* 835 */         /**
/* 836 *|          * Set the options.
/* 837 *|          * 
/* 838 *|          * @param {Array} options
/* 839 *|          */
/* 840 */         setOptions: function(options) {
/* 841 */             this.options = this.mergeOptions(options);
/* 842 */         },
/* 843 */ 
/* 844 */         /**
/* 845 *|          * Merges the given options with the default options.
/* 846 *|          * 
/* 847 *|          * @param {Array} options
/* 848 *|          * @returns {Array}
/* 849 *|          */
/* 850 */         mergeOptions: function(options) {

/* bootstrap-multiselect.js */

/* 851 */             return $.extend({}, this.defaults, this.options, options);
/* 852 */         },
/* 853 */         
/* 854 */         /**
/* 855 *|          * Checks whether a select all option is present.
/* 856 *|          * 
/* 857 *|          * @returns {Boolean}
/* 858 *|          */
/* 859 */         hasSelectAll: function() {
/* 860 */             return this.$select[0][0] ? this.$select[0][0].value === this.options.selectAllValue : false;
/* 861 */         },
/* 862 */         
/* 863 */         /**
/* 864 *|          * Updates the select all option based on the currently selected options.
/* 865 *|          */
/* 866 */         updateSelectAll: function() {
/* 867 */             if (this.hasSelectAll()) {
/* 868 */                 var selected = this.getSelected();
/* 869 */                 
/* 870 */                 if (selected.length === $('option:not([data-role=divider])', this.$select).length - 1) {
/* 871 */                     this.select(this.options.selectAllValue);
/* 872 */                 }
/* 873 */                 else {
/* 874 */                     this.deselect(this.options.selectAllValue);
/* 875 */                 }
/* 876 */             }
/* 877 */         },
/* 878 */         
/* 879 */         /**
/* 880 *|          * Update the button text and its title based on the currently selected options.
/* 881 *|          */
/* 882 */         updateButtonText: function() {
/* 883 */             var options = this.getSelected();
/* 884 */             
/* 885 */             // First update the displayed button text.
/* 886 */             $('button', this.$container).html(this.options.buttonText(options, this.$select));
/* 887 */             
/* 888 */             // Now update the title attribute of the button.
/* 889 */             $('button', this.$container).attr('title', this.options.buttonTitle(options, this.$select));
/* 890 */ 
/* 891 */         },
/* 892 */ 
/* 893 */         /**
/* 894 *|          * Get all selected options.
/* 895 *|          * 
/* 896 *|          * @returns {jQUery}
/* 897 *|          */
/* 898 */         getSelected: function() {
/* 899 */             return $('option[value!="' + this.options.selectAllValue + '"]:selected', this.$select).filter(function() {
/* 900 */                 return $(this).prop('selected');

/* bootstrap-multiselect.js */

/* 901 */             });
/* 902 */         },
/* 903 */ 
/* 904 */         /**
/* 905 *|          * Gets a select option by its value.
/* 906 *|          * 
/* 907 *|          * @param {String} value
/* 908 *|          * @returns {jQuery}
/* 909 *|          */
/* 910 */         getOptionByValue: function (value) {
/* 911 */ 
/* 912 */             var options = $('option', this.$select);
/* 913 */             var valueToCompare = value.toString();
/* 914 */ 
/* 915 */             for (var i = 0; i < options.length; i = i + 1) {
/* 916 */                 var option = options[i];
/* 917 */                 if (option.value == valueToCompare) {
/* 918 */                     return $(option);
/* 919 */                 }
/* 920 */             }
/* 921 */         },
/* 922 */ 
/* 923 */         /**
/* 924 *|          * Get the input (radio/checkbox) by its value.
/* 925 *|          * 
/* 926 *|          * @param {String} value
/* 927 *|          * @returns {jQuery}
/* 928 *|          */
/* 929 */         getInputByValue: function (value) {
/* 930 */ 
/* 931 */             var checkboxes = $('li input', this.$ul);
/* 932 */             var valueToCompare = value.toString();
/* 933 */ 
/* 934 */             for (var i = 0; i < checkboxes.length; i = i + 1) {
/* 935 */                 var checkbox = checkboxes[i];
/* 936 */                 if (checkbox.value == valueToCompare) {
/* 937 */                     return $(checkbox);
/* 938 */                 }
/* 939 */             }
/* 940 */         },
/* 941 */ 
/* 942 */         /**
/* 943 *|          * Used for knockout integration.
/* 944 *|          */
/* 945 */         updateOriginalOptions: function() {
/* 946 */             this.originalOptions = this.$select.clone()[0].options;
/* 947 */         },
/* 948 */ 
/* 949 */         asyncFunction: function(callback, timeout, self) {
/* 950 */             var args = Array.prototype.slice.call(arguments, 3);

/* bootstrap-multiselect.js */

/* 951 */             return setTimeout(function() {
/* 952 */                 callback.apply(self || window, args);
/* 953 */             }, timeout);
/* 954 */         }
/* 955 */     };
/* 956 */ 
/* 957 */     $.fn.multiselect = function(option, parameter) {
/* 958 */         return this.each(function() {
/* 959 */             var data = $(this).data('multiselect');
/* 960 */             var options = typeof option === 'object' && option;
/* 961 */             
/* 962 */             // Initialize the multiselect.
/* 963 */             if (!data) {
/* 964 */                 data = new Multiselect(this, options);
/* 965 */                 $(this).data('multiselect', data);
/* 966 */             }
/* 967 */ 
/* 968 */             // Call multiselect method.
/* 969 */             if (typeof option === 'string') {
/* 970 */                 data[option](parameter);
/* 971 */                 
/* 972 */                 if (option === 'destroy') {
/* 973 */                     $(this).data('multiselect', false);
/* 974 */                 }
/* 975 */             }
/* 976 */         });
/* 977 */     };
/* 978 */ 
/* 979 */     $.fn.multiselect.Constructor = Multiselect;
/* 980 */ 
/* 981 */     $(function() {
/* 982 */         $("select[data-role=multiselect]").multiselect();
/* 983 */     });
/* 984 */ 
/* 985 */ }(window.jQuery);
/* 986 */ 
