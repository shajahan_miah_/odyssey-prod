���W<?php exit; ?>a:1:{s:7:"content";a:80:{s:11:"rate_review";a:6:{i:0;s:1:"0";i:1;s:1:"0";i:2;s:1:"0";i:3;s:1:"0";i:4;s:1:"0";i:5;s:1:"0";}s:10:"sale_price";a:4:{i:0;s:4:"3499";i:1;s:4:"3499";i:2;s:4:"3499";i:3;s:4:"3499";}s:11:"is_featured";a:1:{i:0;s:3:"off";}s:12:"gallery_tyle";a:1:{i:0;s:4:"grid";}s:11:"id_location";a:1:{i:0;s:4:"5172";}s:7:"gallery";a:1:{i:0;s:74:"5167,6456,6455,6454,6453,6452,6451,6450,6449,6448,6447,6446,6445,6444,6443";}s:13:"contact_email";a:1:{i:0;s:26:"support@odysseysafaris.com";}s:11:"description";a:1:{i:0;s:175:"All brand icons are trademarks of their respective owners.
The use of these trademarks does not indicate endorsement of the trademark holder by Font Awesome, nor vice versa.

";}s:8:"map_zoom";a:1:{i:0;s:2:"10";}s:13:"gallery_style";a:1:{i:0;s:4:"grid";}s:7:"map_lat";a:1:{i:0;s:10:"-30.559482";}s:7:"map_lng";a:1:{i:0;s:9:"22.937506";}s:5:"video";a:1:{i:0;s:43:"https://www.youtube.com/watch?v=Lw2dPccmyjs";}s:16:"post_views_count";a:1:{i:0;s:0:"";}s:9:"min_price";a:1:{i:0;s:0:"";}s:5:"price";a:1:{i:0;s:4:"3499";}s:10:"max_people";a:1:{i:0;s:2:"6 ";}s:12:"duration_day";a:1:{i:0;s:2:"10";}s:7:"address";a:1:{i:0;s:12:"south africa";}s:8:"discount";a:1:{i:0;s:0:"";}s:10:"price_sale";a:1:{i:0;s:3:"675";}s:13:"tours_program";a:1:{i:0;s:16510:"a:10:{i:0;a:2:{s:5:"title";s:49:"Day 1:   Cape Town airport ‐ Cape Town (38 kms)";s:4:"desc";s:1987:" Meet and greet at Cape Town International Airport. This personalised service sees your guide meet you off your flight. While you
enjoy a complimentary beverage, our guide will explain your itinerary in detail, give you general advice and information that will
help you along your trip. Before handing over your travel documents and escorting you to your onward transportation, our guide
will assist you with any queries you may have before you embark on your holiday.
 
‐ Half day Cape Town city tour. Be captivated by the multiethnic character and historical wealth of the Mother City. Cape Town is a
rare  cultural  gem,  resulting  from  the  amalgamation  of  different  nationalities  and  indigenous  tribes.  Alongside  high‐rise  office
blocks, a harmonious blend of architectural styles has been meticulously preserved, including Edwardian, Victorian and Cape Dutch.
Narrow, cobblestone streets and the Islamic character of the Bo‐Kaap enhance the cosmopolitan ambiance. The itinerary takes you
to the City Hall, the Castle of Good Hope – this pentagon‐shaped fort is the oldest surviving building in South Africa; the Company's
Garden – a large public park, originally Jan van Riebeeck's vegetable garden, which he grew to feed the original colony as early as
1652; and Signal Hill with spectacular panoramas over the city.
 
‐ Take the cableway up Table Mountain. In 2011, Table Mountain was named among the New7Wonders of Nature. You can get to
the top of Cape Town's most famous icon in just five minutes by taking a cable car up. The Aerial Cableway, established in 1929,
takes visitors to the top in one of two cable cars, each with rotating floors and huge windows to ensure your views while travelling
are almost as spectacular as those on the summit. Cable cars depart every 10 to 15 minutes. The Cableway operates only when
weather permits and does not take bookings.
 
‐ Night: Vineyard Hotel (Courtyard Room ‐ Bed and Breakfast)";}i:1;a:2:{s:5:"title";s:17:"Day 2:  Cape Town";s:4:"desc";s:659:" Breakfast
 
‐ Full day Cape Peninsula tour. Discover both the Atlantic and Indian Ocean coastlines. The first stop is at the charming fishing village
of Hout Bay. Then continue along the Atlantic coastline to the Cape of Good Hope Nature Reserve and Cape Point – the most south‐
westerly tip of Africa and the mythical meeting place of two great oceans. On the return trip, pass through the naval town of
Simon's Town and then traverse the fishing port of Kalk Bay and the lush suburb of Constantia. The day will end with a visit to the
renowned Kirstenbosch Botanical Gardens.
 
‐ Night: Vineyard Hotel (Courtyard Room ‐ Bed and Breakfast)";}i:2;a:2:{s:5:"title";s:17:"Day 3:  Cape Town";s:4:"desc";s:720:" Breakfast
 
‐ Full day Cape Wine Route tour. Learn everything there is to know about wines, from the vineyard to the bottle. The Cape Wine
Route is defined by the triangle that forms the towns of Stellenbosch, Paarl and Franschhoek. Stellenbosch is a charming old town
with oak‐shaded avenues and historical buildings. Franschhoek owes its name to the French Huguenots, who came to settle there
in 1685 and planted the first quality vineyards, while Paarl has been a centre for the Afrikaner culture since the 19th century. Along
the Route, wine estates welcome you to taste wines and cheeses and invite you to learn how the wines are made.
 
‐ Night: Vineyard Hotel (Courtyard Room ‐ Bed and Breakfast)";}i:3;a:2:{s:5:"title";s:37:"Day 4:  Cape Town ‐ Durban (62 kms)";s:4:"desc";s:1492:"Breakfast
 
‐ Transfer from Cape Town to Cape Town airport
 
‐ Take your time to explore the Victoria and Alfred Waterfront. The V&A Waterfront is situated on South Africa’s oldest working
harbour in the centre of Cape Town and offers over 450 retail outlets selling everything from high‐end fashion and jewellery to food
and crafts. It is one of Cape Town’s most popular destinations, attracting in excess of 23 million visitors a year. Besides offering
vibrant and chic indoor and outdoor shopping and restaurants there is the added bonus of watching fishing boats dock with fresh
fish or see a container ship being towed into the harbour by a tug boat. Draw‐ bridges over the water open and close every so often
to allow smaller sailing vessels access to the docks, while the Cape Wheel gives unsurpassed 360 degree views of the city, and the
outdoor amphitheatre plays host to music, dance and theatrical performances throughout the year. There is also a variety of other
activities on offer ranging from historical walking tours to air charters and kids clubs.
 
‐ Flight CapeTown ‐ Durban (Economy Class) (Optional rates: Flight CPT ‐ Durban )
 
‐ Meet and Greet at the airport.
 
‐ Minibus with Guide Overland : service starts at Durban airport and ends at Johannesburg airport (D10 12/03/16) ‐ 7 days
 
‐ Dinner: Southern Sun Elangeni & Maharani
 
‐ Night: Southern Sun Elangeni & Maharani (Standard Room ‐ Dinner, Bed and Breakfast)";}i:4;a:2:{s:5:"title";s:36:"Day 5: Durban ‐ Hluhluwe (273 kms)";s:4:"desc";s:2753:"Breakfast
 
‐ Discover Durban's “Golden Mile”. This six kilometre long stretch of sandy beach starts roughly at South Beach and uShaka Marine
World and ends at the Suncoast Casino and Entertainment World to the north. The Golden Mile is also a huge pleasure resort with
dozens of swimming and splash pools, fountains and waterslides, curio markets and merry‐go‐rounds, uShaka Marine World, exotic
restaurants, nightclubs and many hotels and apartment complexes directly at the beach. Swimming is good all year round due to
the warm, subtropical climate, and the beaches are well monitored and protected by shark nets. An unusual feature at the Golden
Mile is the Zulu Rikshas. The vehicles and their drivers are colourfully decorated. For a small fee, you can let yourself be pulled along
the beach promenade.
 
‐ Visit to DumaZulu Cultural Village. Experience traditional hospitality and learn about Zulu life at this informative and authentic
cultural village in Hluhluwe, KwaZulu‐Natal. The village is a living museum of traditional Zulu customs, crafts and way of life where
you can feel the beat of the Zulu drums and discover the rich cultural significance of the tribe's famous beadwork, weaving and
pottery. Zulu social values are integrated into their arts and crafts and their artefacts, besides being beautifully made, also hold
strong cultural significance. The village is the largest in South Africa and not only affords guests the opportunity to learn about the
rich history of tribal Africa but also serves as a means for the local community to continue practicing their rural ways of living as
well as earning an income from selling their crafts. Other interesting activities on offer include tastings of locally crafted beer and
dancing demonstrations.
 
‐ Lunch
 
‐ Afternoon 4x4 safari in the Hluhluwe reserve. For Big Five game viewing in extremely scenic surroundings, Hluhluwe Game Reserve
is hard to match. Proclaimed in 1897, This KwaZulu‐Natal game reserve north of Durban is one of the oldest in Africa. Hluhluwe
Game Reserve is world renowned for its role in Rhino conservation and the Hluhluwe‐Imfolozi Centenary Capture Centre sets a
benchmark for animal capture and sustainable utilization throughout Africa. This KwaZulu‐Natal Game Reserve stretches over 96
000 hectares of hilly topography and contains a diverse range of fauna and flora. This area was once the exclusive hunting ground
of  King  Shaka.  View  the  Big  Five  in  Hluhluwe  Game  Reserve  as  well  as  several  more  elusive  animals,  such  Wild  Dog,  Giraffe,
Cheetah and Nyala.
 
‐ Dinner: Zulu Nyala Game Lodge
 
‐ Night: Zulu Nyala Game Lodge (Standard Room ‐ Dinner, Bed and Breakfast)";}i:5;a:2:{s:5:"title";s:39:"Day 6: Hluhluwe ‐ Swaziland (249 kms)";s:4:"desc";s:950:" Breakfast
 
‐ The plains in the south‐western part of Swaziland contrast sharply with the mountainous landscapes of the north. You will discover
lush sugarcane fields alternating with bush savannah dotted with Swazi kraals. Near the southern border post of Lavumisa, the
Lubombo Mountains are very apparent from the road. This mountain range covers practically the entire eastern side of the country
and forms the natural border between Swaziland and Mozambique.
 
‐ Lunch Menu 2
 
‐ Visit  the  Swazi  candles  at  Malkerns.  On  weekdays  you  will  be  able  to  view  the  craftspeople  moulding  the  colourful  wax  into
fascinating  shapes  and  sizes.  These  delightful  candles  are  exported  across  the  world  and  have  become  very  popular  as  Swazi
mementos for friends and family back home.
 
‐ Dinner: Summerfield Luxury Resort
 
‐ Night: Summerfield Luxury Resort (Luxury Suite ‐ Bed and Breakfast)";}i:6;a:2:{s:5:"title";s:38:"Day 7:  Swaziland ‐ Kruger (331 kms)";s:4:"desc";s:2342:"Breakfast
 
‐ Visit  the  Ngwenya  Glass  factory.  Aside  from  providing  charming  glass  souvenirs  and  glassware,  this  enterprise  also  has  an
important social function. Entrepreneurs ranging from school children to adults collect discarded glass objects and bring them to
the factory in return for payment. An interesting way of combining environmental care with social upliftment!
 
‐ Arrival in Sabi Sands Game reserve, the most famous and well established of the private reserves, home to some of the finest safari
lodges in South Africa. The 650 square kilometres wildlife sanctuary forms part of the greater Kruger National Park area. The
reserve is the birthplace of sustainable wildlife tourism in Southern Africa, and is the oldest of all the private reserves in the country.
The Sabi Reserve was proclaimed in 1898. The history of today's Sabi Sands Reserve as a formal association dates back to 1948
when the landowners formed the private nature reserve, it is still largely owned and operated by 3rd & 4th generation families who
share a common vision with their ancestors. It has only ever been a wilderness area and is home to a vast wildlife population,
including  The  Big  Five,  and  a  number  of  endangered  species  such  as:  Wild  Dog,  Bats,  Honey  Badgers,  Oxpeckers  and  Ground
Hornbill. Open safari vehicles head out into the wild under the expert guidance of experienced rangers and insightful trackers. You
will be able to get in close contact with Africa's most exciting wildlife species, and will be presented with unparalleled photographic
opportunities. Early morning safaris head out as the sun rises and as the bush stirs to life. The ranger and tracker team searches for
animals by tracking their spoor, and by listening for the telltale signs of activity. Late afternoon safaris commence as the sun burns
its  way  towards  the  horizon.  The  animals  are  now  getting  more  active,  many  preparing  for  the  great  hunt  after  dark.  After
sundown, the drive continues with the aid of powerful spotlights which reveal the nocturnal species. (Activities included in your
package as per program below)
 
‐ Lunch
 
‐ Afternoon 4x4 Game Drive
 
‐ Dinner: Lion Sands : River Lodge
 
‐ Night: Lion Sands : River Lodge (Luxury Room ‐ Fully Inclusive)";}i:7;a:2:{s:5:"title";s:14:"Day 8:  Kruger";s:4:"desc";s:203:" Morning 4x4 Game drive
 
‐ Breakfast
 
‐ Lunch
 
‐ Afternoon 4x4 Game Drive
 
‐ Dinner: Lion Sands : River Lodge
 
‐ Night: Lion Sands : River Lodge (Luxury Room ‐ Fully Inclusive)";}i:8;a:2:{s:5:"title";s:41:"Day 9:  Kruger ‐ Johannesburg (522 kms)";s:4:"desc";s:2442:" Morning 4x4 Game drive
 
‐ Breakfast
 
‐ The Blyde River Canyon is the third deepest canyon in the world (after the Grand Canyon in the western U.S. and Namibia's Fish
River Canyon). It ranks as one of the most spectacular sights in Africa. The mountain scenery and panoramic views over the Klein
Drakensberg escarpment are quite spectacular and give the area its name of the Panorama Route. Viewpoints are named for the
vistas they offer, and God's Window and Wonder View hint at the magnitude of the scenery. The 'Pinnacle' is a single quartzite
column  rising  out  of  the  deep  wooded  canyon  and  the  Three  Rondavels  (also  called  'Three  Sisters')  are  three  huge  spirals  of
dolomite rock rising out of the far wall of the Blyde River Canyon. The entire canyon extends over twenty kilometres in length.
 
‐ Lunch
 
‐ Visit  Pilgrim's  Rest.  The  village  is  situated  on  the  magnificent  Panorama  Route  on  the  eastern  escarpment  region  of  the
Mpumalanga province. The entire town of Pilgrim's Rest has been declared a national monument in 1986. Here, visitors can relive
the days of the old Transvaal gold rush. Pilgrim's Rest was declared a gold field in 1873, soon after digger Alec "Wheelbarrow"
Patterson discovered gold deposits in Pilgrim's Creek. The Valley proved to be rich in gold and by the end of the year, there were
about 1500 diggers working in the area. As a result, Pilgrim's Rest became a social centre of the diggings. There is plenty to do and
experience here, from browsing through exciting curio and craft shops to discovering fascinating historical sights.
 
‐ Travel to Johannesburg
 
‐ Dinner: Carnivore Restaurant
A franchise of the world‐renowned Carnivore restaurant in Nairobi, Kenya, the restaurant is located in some natural surroundings
overlooking the Krugersdorp hills. Its main attraction is a large circular open fire with 52 converted Masaai tribal spears skewed
with a variety of meat, such as pork, lamb, beef, chicken, ribs and sausages, and also crocodile, zebra, giraffe, impala, ostrich and
kudu, roasting slowly and waiting for you to begin your feast! Waiters walk from the fire to your table holding spears skewed with
the meat and carve the cuts of your choice directly onto your plate until you have eaten your fill. Surely Africa's Greatest Eating
Experience.
 
‐ Night: 54 on Bath (Deluxe Room ‐ Bed and Breakfast)";}i:9;a:2:{s:5:"title";s:43:"Day 10: Johannesburg ‐ Pretoria (110 kms)";s:4:"desc";s:2118:"‐ Breakfast
 
‐ Visit the Apartheid Museum, dedicated to recounting the history of the rise and fall of apartheid in 20th century South Africa.
Interactive  multimedia  displays  recreate  the  experience  of  racial  segregation  with  exhibits  dedicated  to  historical  facts
accompanied by personal testimony : harrowing, unforgettable and uniquely South African. (No Guided tours on Mondays)
 
‐ Visit Soweto. Cursed child of apartheid, Johannesburg's South Western Township today comprises close to 4 million inhabitants.

The revolts of '76 that took place there marked the beginning of the decline of the segregation policy.
 
‐ Buffet lunch: Sakhumzi Restaurant 
Sakhumzi Restaurant is nestled in the heart of Soweto, in Orlando west's famous Vilakazi Street. The Street has a rich historical
background  that  boasts  two  Nobel  prize  winners:  Archbishop  Desmond  Mpilo  Tutu  and  former  president  Nelson  Mandela.
Previously  a  "Shebeen",  illegal  bar  during  Apartheid,  Sakhumzi  is  now  a  traditional  local  restaurant  that  specializes  in  typical
Sowetan cuisine. The buffet lunch ranges from pap(corn flour), rice, dumplings, vegetables, grilled chicken, stews and curries to
mogodu (tripe).
 
‐ Orientation tour of Pretoria, South Africa's capital. The city was founded in 1855 and became the capital of the Boer state, Zuid
Afrikaanse Republiek, 5 years later. Many reminders of the history of the Boers can still be seen whilst driving around the city; such
as  Church  Square  with  its  imposing  statue  of  Uncle  Paul  Kruger  ‐  ex‐president  of  the  ZAR  and  much  loved  Boer  leader.  The
Voortrekker Monument, rising like a sentinel in the south of Pretoria, is an essential piece of the puzzle in understanding the
Afrikaner people. The Union Buildings, which dominate the western side of the city, form the official seat of the South African
government, and feature a nine‐metre‐tall statue of the former president of South Africa, Nelson Mandela, erected shortly after his
death on 5 December 2013.
 
‐ End of our services";}}";}s:16:"st_custom_layout";a:1:{i:0;s:4:"1118";}s:16:"is_sale_schedule";a:1:{i:0;s:3:"off";}s:8:"check_in";a:1:{i:0;s:10:"2015-07-25";}s:9:"check_out";a:1:{i:0;s:10:"2015-08-01";}s:17:"_vc_post_settings";a:1:{i:0;s:30:"a:1:{s:10:"vc_grid_id";a:0:{}}";}s:10:"_edit_lock";a:1:{i:0;s:12:"1469724411:2";}s:13:"_thumbnail_id";a:1:{i:0;s:4:"5167";}s:10:"_edit_last";a:1:{i:0;s:1:"2";}s:12:"_wp_old_slug";a:3:{i:0;s:14:"tour-ultricies";i:1;s:44:"discover-south-africa-9-nights-luxury-option";i:2;s:44:"discover-south-africa-luxury-safari__trashed";}s:10:"type_price";a:1:{i:0;s:10:"tour_price";}s:11:"adult_price";a:1:{i:0;s:4:"2999";}s:11:"child_price";a:1:{i:0;s:4:"3499";}s:9:"type_tour";a:1:{i:0;s:10:"daily_tour";}s:15:"sale_price_from";a:1:{i:0;s:10:"2015-07-01";}s:13:"sale_price_to";a:1:{i:0;s:10:"2015-07-29";}s:19:"accommodation_video";a:1:{i:0;s:72:"https://www.youtube.com/playlist?list=PLjqSFpDvnZb6oM0zIagz54NGtuGmwZXpm";}s:20:"_accommodation_video";a:1:{i:0;s:19:"field_55c5e8f26c5e9";}s:11:"video_title";a:1:{i:0;s:21:"Discover South Africa";}s:12:"_video_title";a:1:{i:0;s:19:"field_55c5ed93ae5be";}s:10:"video_text";a:1:{i:0;s:239:"Cape Town:  Vineyard Hotel <br />
Durban:  Southern Sun Elangeni & Maharani<br />
Hluhluwe:  Zulu Nyala Game Lodge<br />
Swaziland:  Summerfield Luxury Resort<br />
Kruger:  Lion Sands River Lodge<br />
Johannesburg:  54 on Bath<br />";}s:11:"_video_text";a:1:{i:0;s:19:"field_55c5edabae5bf";}s:3:"map";a:1:{i:0;s:1331:"<iframe src="https://www.google.com/maps/embed?pb=!1m58!1m12!1m3!1d7107870.695564671!2d20.62184711706626!3d-29.555976896321436!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m43!3e0!4m5!1s0x1dcc4542f7400bbd%3A0x40487579e3cf5e90!2sCape+Town+International+Airport%2C+Cape+Town%2C+7490!3m2!1d-33.971463!2d18.6020851!4m5!1s0x1dcc6761cca039ab%3A0xc06da1cc10cb0436!2sThe+Capetonian%2C+Heerengracht+Street%2C+Cape+Town%2C+Western+Cape%2C+South+Africa!3m2!1d-33.918076!2d18.426735!4m5!1s0x1ef7aa0001bc61b7%3A0xcca75546c4aa6e81!2sDurban%2C+South+Africa!3m2!1d-29.858680399999997!2d31.0218404!4m5!1s0x1efafb4cc12f0b5b%3A0xe3b98c582a712337!2sZulu+Nyala+Heritage+Safari+Lodge%2C+Hluhluwe%2C+South+Africa!3m2!1d-27.951459999999997!2d32.247087!4m5!1s0x1eef34afc66a6725%3A0x6d3cf6626743860e!2sMantenga+Lodge%2C+Mantenga+Falls+Road%2C+2%2C+H106%2C+Swaziland!3m2!1d-26.4412001!2d31.1813343!4m5!1s0x1ee831093f79e607%3A0xf00a2920ed593685!2sCasterbridge+Hollow+Boutique+Hotel%2C+White+River%2C+Mpumalanga%2C+South+Africa!3m2!1d-25.328438499999997!2d31.017783599999998!4m5!1s0x1e959c679e657fbf%3A0x581955ea1822582d!2sMisty+Hills+Country+Hotel%2C+Conference+Centre+%26+Spa%2C+Johannesburg%2C+South+Africa!3m2!1d-26.028807999999998!2d27.8567!5e0!3m2!1sen!2sus!4v1439404296921" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>";}s:4:"_map";a:1:{i:0;s:19:"field_55c5fc80010d9";}s:21:"international_airfare";a:1:{i:0;s:35:"Not included *contact for discounts";}s:22:"_international_airfare";a:1:{i:0;s:19:"field_55c609ee3dfdc";}s:15:"departure_dates";a:1:{i:0;s:18:"At your discretion";}s:16:"_departure_dates";a:1:{i:0;s:19:"field_55c609f63dfdd";}s:9:"tour_type";a:1:{i:0;s:7:"Private";}s:10:"_tour_type";a:1:{i:0;s:19:"field_55c60a373dfde";}s:4:"what";a:1:{i:0;s:415:"• All accommodation <br />
• Unlimited game drives in parks and reserves as per itinerary<br />
• Meals as noted 3 meals daily on safari (B,L,D) <br />
• All national park, reserve, and conservation fees as per itinerary<br />
• All transportation including airport transfers as per itinerary<br />
• Complimentary bottled water daily<br />
• Services of an English speaking driver/guide<br />
";}s:5:"_what";a:1:{i:0;s:19:"field_563c35c45737f";}s:15:"whats__included";a:1:{i:0;s:266:"• International flights<br />
• Visas<br />
• Driver/guide/cook/porter gratuities<br />
• Telephone bills, laundry, and any items of a personal nature<br />
• Cost associated with activities not listed on the itinerary<br />
• Travel insurance<br />";}s:16:"_whats__included";a:1:{i:0;s:19:"field_563c367c57380";}s:19:"summary_of_wildlife";a:1:{i:0;s:151:"Lion, Rhino, Elephant, Leopard, Cape Buffalo, Cheetah, Giraffe, Zebra, Wildebeest, Eland, Kudu, Thompsons Gazelle, Topi, Ostrich, 400+ species of birds";}s:20:"_summary_of_wildlife";a:1:{i:0;s:19:"field_563c36ac57381";}s:15:"whats_included_";a:1:{i:0;s:20:"a:1:{i:0;s:3:"Yes";}";}s:16:"_whats_included_";a:1:{i:0;s:19:"field_564040b08ad76";}s:29:"click_show_whats_not_included";a:1:{i:0;s:20:"a:1:{i:0;s:3:"Yes";}";}s:30:"_click_show_whats_not_included";a:1:{i:0;s:19:"field_564041e58ad77";}s:25:"click_summary_of_wildlife";a:1:{i:0;s:20:"a:1:{i:0;s:3:"Yes";}";}s:26:"_click_summary_of_wildlife";a:1:{i:0;s:19:"field_564042388ad78";}s:8:"order_by";a:1:{i:0;s:2:"18";}s:9:"_order_by";a:1:{i:0;s:19:"field_566026addd597";}s:11:"travel_date";a:1:{i:0;s:0:"";}s:12:"_travel_date";a:1:{i:0;s:19:"field_56604f0ae0688";}s:11:"last_minute";a:1:{i:0;s:0:"";}s:12:"_last_minute";a:1:{i:0;s:19:"field_5662c08454ce7";}s:9:"home_page";a:1:{i:0;s:0:"";}s:10:"_home_page";a:1:{i:0;s:19:"field_5662d8f31d20a";}s:12:"_dp_original";a:1:{i:0;s:4:"6430";}s:14:"seasonal_price";a:1:{i:0;s:5:"$4000";}s:15:"_seasonal_price";a:1:{i:0;s:19:"field_5758328d87a85";}s:33:"_yoast_wpseo_primary_st_tour_type";a:1:{i:0;s:0:"";}s:35:"_yoast_wpseo_primary_seasonal-price";a:1:{i:0;s:2:"65";}s:36:"_yoast_wpseo_primary_seasonal-price1";a:1:{i:0;s:0:"";}s:26:"_yoast_wpseo_content_score";a:1:{i:0;s:2:"60";}s:21:"_wp_trash_meta_status";a:1:{i:0;s:7:"publish";}s:19:"_wp_trash_meta_time";a:1:{i:0;s:10:"1469412010";}}}