���W<?php exit; ?>a:1:{s:7:"content";a:78:{s:11:"rate_review";a:7:{i:0;s:1:"0";i:1;s:1:"0";i:2;s:1:"0";i:3;s:1:"0";i:4;s:1:"0";i:5;s:1:"0";i:6;s:1:"0";}s:10:"sale_price";a:5:{i:0;s:4:"1599";i:1;s:4:"1599";i:2;s:4:"1599";i:3;s:4:"1599";i:4;s:4:"1599";}s:11:"is_featured";a:1:{i:0;s:3:"off";}s:12:"gallery_tyle";a:1:{i:0;s:4:"grid";}s:11:"id_location";a:1:{i:0;s:4:"5172";}s:7:"gallery";a:1:{i:0;s:4:"5167";}s:13:"contact_email";a:1:{i:0;s:26:"support@odysseysafaris.com";}s:11:"description";a:1:{i:0;s:175:"All brand icons are trademarks of their respective owners.
The use of these trademarks does not indicate endorsement of the trademark holder by Font Awesome, nor vice versa.

";}s:8:"map_zoom";a:1:{i:0;s:2:"10";}s:13:"gallery_style";a:1:{i:0;s:4:"grid";}s:7:"map_lat";a:1:{i:0;s:10:"-19.015438";}s:7:"map_lng";a:1:{i:0;s:9:"29.154857";}s:5:"video";a:1:{i:0;s:72:"https://www.youtube.com/playlist?list=PLjqSFpDvnZb6oM0zIagz54NGtuGmwZXpm";}s:16:"post_views_count";a:1:{i:0;s:0:"";}s:9:"min_price";a:1:{i:0;s:0:"";}s:5:"price";a:1:{i:0;s:4:"1599";}s:10:"max_people";a:1:{i:0;s:2:"6 ";}s:12:"duration_day";a:1:{i:0;s:1:"9";}s:7:"address";a:1:{i:0;s:12:"south africa";}s:8:"discount";a:1:{i:0;s:0:"";}s:10:"price_sale";a:1:{i:0;s:3:"675";}s:13:"tours_program";a:1:{i:0;s:19870:"a:9:{i:0;a:2:{s:5:"title";s:49:"Day 1:   Cape Town airport ‐ Cape Town (29 kms)";s:4:"desc";s:1995:"‐ Meet and greet at Cape Town International Airport. This personalised service sees your guide meet you off your flight. While you
enjoy a complimentary beverage, our guide will explain your itinerary in detail, give you general advice and information that will
help you along your trip. Before handing over your travel documents and escorting you to your onward transportation, our guide
will assist you with any queries you may have before you embark on your holiday.
 
‐ Half day Cape Town city tour. Be captivated by the multiethnic character and historical wealth of the Mother City. Cape Town is a
rare  cultural  gem,  resulting  from  the  amalgamation  of  different  nationalities  and  indigenous  tribes.  Alongside  high‐rise  office
blocks, a harmonious blend of architectural styles has been meticulously preserved, including Edwardian, Victorian and Cape Dutch.
Narrow, cobblestone streets and the Islamic character of the Bo‐Kaap enhance the cosmopolitan ambiance. The itinerary takes you
to the City Hall, the Castle of Good Hope – this pentagon‐shaped fort is the oldest surviving building in South Africa; the Company's
Garden – a large public park, originally Jan van Riebeeck's vegetable garden, which he grew to feed the original colony as early as
1652; and Signal Hill with spectacular panoramas over the city.
 
‐ Take the cableway up Table Mountain. In 2011, Table Mountain was named among the New7Wonders of Nature. You can get to
the top of Cape Town's most famous icon in just five minutes by taking a cable car up. The Aerial Cableway, established in 1929,
takes visitors to the top in one of two cable cars, each with rotating floors and huge windows to ensure your views while travelling
are almost as spectacular as those on the summit. Cable cars depart every 10 to 15 minutes. The Cableway operates only when
weather permits and does not take bookings.
 
‐ Night: The Capetonian Hotel (Superior Room ‐ Bed and Breakfast)";}i:1;a:2:{s:5:"title";s:17:"Day 2:  Cape Town";s:4:"desc";s:667:"‐ Breakfast
 
‐ Full day Cape Peninsula tour. Discover both the Atlantic and Indian Ocean coastlines. The first stop is at the charming fishing village
of Hout Bay. Then continue along the Atlantic coastline to the Cape of Good Hope Nature Reserve and Cape Point – the most south‐
westerly tip of Africa and the mythical meeting place of two great oceans. On the return trip, pass through the naval town of
Simon's Town and then traverse the fishing port of Kalk Bay and the lush suburb of Constantia. The day will end with a visit to the
renowned Kirstenbosch Botanical Gardens.
 
‐ Night: The Capetonian Hotel (Superior Room ‐ Bed and Breakfast)";}i:2;a:2:{s:5:"title";s:18:"Day 3:   Cape Town";s:4:"desc";s:773:"‐ Breakfast
 
‐ Full day Cape Wine Route tour. Learn everything there is to know about wines, from the vineyard to the bottle. The Cape Wine
Route is defined by the triangle that forms the towns of Stellenbosch, Paarl and Franschhoek. Stellenbosch is a charming old town
with oak‐shaded avenues and historical buildings. Franschhoek owes its name to the French Huguenots, who came to settle there
in 1685 and planted the first quality vineyards, while Paarl has been a centre for the Afrikaner culture since the 19th century. Along
the Route, wine estates welcome you to taste wines and cheeses and invite you to learn how the wines are made.
 
‐ Dinner: The Capetonian Hotel
 
‐ Night: The Capetonian Hotel (Superior Room ‐ Dinner, Bed and Breakfast)";}i:3;a:2:{s:5:"title";s:37:"Day 4:  Cape Town ‐ Durban (33 kms)";s:4:"desc";s:1886:"‐ Breakfast
 
‐ Take your time to explore the Victoria and Alfred Waterfront. The V&A Waterfront is situated on South Africa’s oldest working
harbour in the centre of Cape Town and offers over 450 retail outlets selling everything from high‐end fashion and jewellery to food
and crafts. It is one of Cape Town’s most popular destinations, attracting in excess of 23 million visitors a year. Besides offering
vibrant and chic indoor and outdoor shopping and restaurants there is the added bonus of watching fishing boats dock with fresh
fish or see a container ship being towed into the harbour by a tug boat. Draw‐ bridges over the water open and close every so often
to allow smaller sailing vessels access to the docks, while the Cape Wheel gives unsurpassed 360 degree views of the city, and the
outdoor amphitheatre plays host to music, dance and theatrical performances throughout the year. There is also a variety of other
activities on offer ranging from historical walking tours to air charters and kids clubs. (suggestion not included in rates)
 
‐ Transfer from Cape Town to Cape Town airport
 
‐ Flight CapeTown ‐ Durban (Economy Class) (Optional rates: Flight CPT ‐ Durban )
 
‐ Meet and Greet at the airport.
 
‐ Pick up your vehicle Avis Premium Cat A (7‐13 days) at Durban airport ‐ Drop Off Johannesburg airport (D10 12/03/16) ‐ 7 days
(Optional rates: Car rental 5 seater) 

 
‐ Pick up your vehicle Avis Premium Cat N (7‐13 days) at Durban airport ‐ Drop Off Johannesburg airport (D10 12/03/16) ‐ 7 days
(Optional rates: Car rental 5 seater) 

 
‐ Pick up your vehicle Avis Premium Cat O (7‐13 days) at Durban airport ‐ Drop Off Johannesburg airport (D10 12/03/16) ‐ 7 days
(Optional rates: Car rental 8 seater) 

‐ Night: The Balmoral Hotel (Standard Sea Facing Room ‐ Bed and Breakfast)
";}i:4;a:2:{s:5:"title";s:37:"Day 5:  Durban ‐ Hluhluwe (269 kms)";s:4:"desc";s:2840:"‐ Breakfast
 
‐ Discover Durban's “Golden Mile”. This six kilometre long stretch of sandy beach starts roughly at South Beach and uShaka Marine
World and ends at the Suncoast Casino and Entertainment World to the north. The Golden Mile is also a huge pleasure resort with
dozens of swimming and splash pools, fountains and waterslides, curio markets and merry‐go‐rounds, uShaka Marine World, exotic
restaurants, nightclubs and many hotels and apartment complexes directly at the beach. Swimming is good all year round due to
the warm, subtropical climate, and the beaches are well monitored and protected by shark nets. An unusual feature at the Golden
Mile is the Zulu Rikshas. The vehicles and their drivers are colourfully decorated. For a small fee, you can let yourself be pulled along
the beach promenade. (suggestion not included in rates)
 
‐ Travel to Hluhluwe
 
‐ Visit to DumaZulu Cultural Village. Experience traditional hospitality and learn about Zulu life at this informative and authentic
cultural village in Hluhluwe, KwaZulu‐Natal. The village is a living museum of traditional Zulu customs, crafts and way of life where
you can feel the beat of the Zulu drums and discover the rich cultural significance of the tribe's famous beadwork, weaving and
pottery. Zulu social values are integrated into their arts and crafts and their artefacts, besides being beautifully made, also hold
strong cultural significance. The village is the largest in South Africa and not only affords guests the opportunity to learn about the
rich history of tribal Africa but also serves as a means for the local community to continue practicing their rural ways of living as
well as earning an income from selling their crafts. Other interesting activities on offer include tastings of locally crafted beer and
dancing demonstrations.
 
‐ Lunch
 
‐ Afternoon 4x4 safari in the Hluhluwe reserve. For Big Five game viewing in extremely scenic surroundings, Hluhluwe Game Reserve
is hard to match. Proclaimed in 1897, This KwaZulu‐Natal game reserve north of Durban is one of the oldest in Africa. Hluhluwe
Game Reserve is world renowned for its role in Rhino conservation and the Hluhluwe‐Imfolozi Centenary Capture Centre sets a
benchmark for animal capture and sustainable utilization throughout Africa. This KwaZulu‐Natal Game Reserve stretches over 96
000 hectares of hilly topography and contains a diverse range of fauna and flora. This area was once the exclusive hunting ground
of  King  Shaka.  View  the  Big  Five  in  Hluhluwe  Game  Reserve  as  well  as  several  more  elusive  animals,  such  Wild  Dog,  Giraffe,
Cheetah and Nyala.
 
‐ Dinner: Zulu Nyala Heritage Safari Lodge
 
‐ Night: Zulu Nyala Heritage Safari Lodge (Standard Room ‐ Dinner, Bed and Breakfast";}i:5;a:2:{s:5:"title";s:39:"Day 6: Hluhluwe ‐ Swaziland (252 kms)";s:4:"desc";s:3382:" Breakfast
 
‐ The plains in the south‐western part of Swaziland contrast sharply with the mountainous landscapes of the north. You will discover
lush sugarcane fields alternating with bush savannah dotted with Swazi kraals. Near the southern border post of Lavumisa, the
Lubombo Mountains are very apparent from the road. This mountain range covers practically the entire eastern side of the country
and forms the natural border between Swaziland and Mozambique. (suggestion not included in rates)
 
‐ Travel to Swaziland
 
‐ Lunch Menu 2
 
‐ Visit  the  Swazi  candles  at  Malkerns.  On  weekdays  you  will  be  able  to  view  the  craftspeople  moulding  the  colourful  wax  into
fascinating  shapes  and  sizes. 
‐ Dinner: Mantenga Lodge
 
‐ Night: Mantenga Lodge (Sheba Room ‐ Dinner, Bed and Breakfast)
Image_46_0
Lavumisa Discover ‐ Manzini
Swazi Candles ‐ Malkerns
Lunch menu 2 ‐ Malkerns
Image_47_0
D7 09/03/16 Swaziland ‐ Kruger (265 kms)
Image_48_0
 
‐ Breakfast
 
‐ Visit  the  Ngwenya  Glass  factory.  Aside  from  providing  charming  glass  souvenirs  and  glassware,  this  enterprise  also  has  an
important social function. Entrepreneurs ranging from school children to adults collect discarded glass objects and bring them to
the factory in return for payment. An interesting way of combining environmental care with social upliftment! (suggestion not
included in rates)
 
‐ Dinner: Hippo Hollow Country Estate
 
‐ Night: Hippo Hollow Country Estate (Standard Room ‐ Dinner, Bed and Breakfast)
Image_49_0
Ngwenya Glass Factory ‐ Piggs Peak
African Eagle
Hippo Hollow Country Estate ‐ Hazyview
Image_50_0
D8 10/03/16 Kruger ‐ Kruger (Karongwe) (145 kms)
Image_51_0
 
‐ Breakfast
 
‐ Arrival in Karongwe Private Game reserve, in the heart of the Limpopo. It lies adjacent to the Greater Makalali Private Game
Reserve just 40 minutes due east of the Kruger National Park. This is the land of combretum and acacia thornveld, a land where
bushveld reigns supreme and the city skyline is a thing only of the imagination; where sightings of major mammals such as giraffe,
wildebeest, rhino and zebra are regular, and night visits by aardvark, bush babies, genets and porcupines a reality. The night skies
are dark, peppered by a myriad stars, the Milky Way, and the calls of nocturnal birds. This is the wild as portrayed in postcards.
Karongwe Game reserve lies in the heart of the bushveld, given over to the conservation of what is left of South Africa's wild
animals. Witness dramatic sunsets ‐ a time in the bush where the wild begin to stir, where drama is doubtless brewing, and where
sightings of game is guaranteed, whilst glorious views of the Drakensberg and Murchison Mountains litter the horizon with gentle
undulations and create gorgeous backdrops to your experience. Within Karongwe are numerous nature reserves, lodges, safaris
and trails. You can look forward to an encounter with over twenty different plains game species and the Big Five (buffalo, leopard,
elephan, lion and rhino). (Activities included in your package as per program below)
 
‐ Lunch
 
‐ Afternoon 4x4 Game Drive
 
‐ Dinner: Karongwe River Lodge (ex Edeni River Lodge)
 
‐ Night: Karongwe River Lodge (ex Edeni River Lodge) (Luxury Suite ‐ Fully Inclusive)
";}i:6;a:2:{s:5:"title";s:38:"Day 7:  Swaziland ‐ Kruger (265 kms)";s:4:"desc";s:2421:"‐ Breakfast
 
‐ Visit  the  Ngwenya  Glass  factory.  Aside  from  providing  charming  glass  souvenirs  and  glassware,  this  enterprise  also  has  an
important social function. Entrepreneurs ranging from school children to adults collect discarded glass objects and bring them to
the factory in return for payment. An interesting way of combining environmental care with social upliftment! (suggestion not
included in rates)
 
‐ Dinner: Hippo Hollow Country Estate
 
‐ Night: Hippo Hollow Country Estate (Standard Room ‐ Dinner, Bed and Breakfast)
Image_49_0
Ngwenya Glass Factory ‐ Piggs Peak
African Eagle
Hippo Hollow Country Estate ‐ Hazyview
Image_50_0
D8 10/03/16 Kruger ‐ Kruger (Karongwe) (145 kms)
Image_51_0
 
‐ Breakfast
 
‐ Arrival in Karongwe Private Game reserve, in the heart of the Limpopo. It lies adjacent to the Greater Makalali Private Game
Reserve just 40 minutes due east of the Kruger National Park. This is the land of combretum and acacia thornveld, a land where
bushveld reigns supreme and the city skyline is a thing only of the imagination; where sightings of major mammals such as giraffe,
wildebeest, rhino and zebra are regular, and night visits by aardvark, bush babies, genets and porcupines a reality. The night skies
are dark, peppered by a myriad stars, the Milky Way, and the calls of nocturnal birds. This is the wild as portrayed in postcards.
Karongwe Game reserve lies in the heart of the bushveld, given over to the conservation of what is left of South Africa's wild
animals. Witness dramatic sunsets ‐ a time in the bush where the wild begin to stir, where drama is doubtless brewing, and where
sightings of game is guaranteed, whilst glorious views of the Drakensberg and Murchison Mountains litter the horizon with gentle
undulations and create gorgeous backdrops to your experience. Within Karongwe are numerous nature reserves, lodges, safaris
and trails. You can look forward to an encounter with over twenty different plains game species and the Big Five (buffalo, leopard,
elephan, lion and rhino). (Activities included in your package as per program below)
 
‐ Lunch
 
‐ Afternoon 4x4 Game Drive
 
‐ Dinner: Karongwe River Lodge (ex Edeni River Lodge)
 
‐ Night: Karongwe River Lodge (ex Edeni River Lodge) (Luxury Suite ‐ Fully Inclusive)

Ngwenya Glass Factory ‐ Piggs Peak
African Eagle";}i:7;a:2:{s:5:"title";s:46:"Day 8:  Kruger ‐ Kruger (Karongwe) (145 kms)";s:4:"desc";s:1609:"‐ Breakfast
 
‐ Arrival in Karongwe Private Game reserve, in the heart of the Limpopo. It lies adjacent to the Greater Makalali Private Game
Reserve just 40 minutes due east of the Kruger National Park. This is the land of combretum and acacia thornveld, a land where
bushveld reigns supreme and the city skyline is a thing only of the imagination; where sightings of major mammals such as giraffe,
wildebeest, rhino and zebra are regular, and night visits by aardvark, bush babies, genets and porcupines a reality. The night skies
are dark, peppered by a myriad stars, the Milky Way, and the calls of nocturnal birds. This is the wild as portrayed in postcards.
Karongwe Game reserve lies in the heart of the bushveld, given over to the conservation of what is left of South Africa's wild
animals. Witness dramatic sunsets ‐ a time in the bush where the wild begin to stir, where drama is doubtless brewing, and where
sightings of game is guaranteed, whilst glorious views of the Drakensberg and Murchison Mountains litter the horizon with gentle
undulations and create gorgeous backdrops to your experience. Within Karongwe are numerous nature reserves, lodges, safaris
and trails. You can look forward to an encounter with over twenty different plains game species and the Big Five (buffalo, leopard,
elephan, lion and rhino). (Activities included in your package as per program below)
 
‐ Lunch
 
‐ Afternoon 4x4 Game Drive
 
‐ Dinner: Karongwe River Lodge (ex Edeni River Lodge)
 
‐ Night: Karongwe River Lodge (ex Edeni River Lodge) (Luxury Suite ‐ Fully Inclusive)
";}i:8;a:2:{s:5:"title";s:52:"Day 9:  Kruger (Karongwe) ‐ Johannesburg (527 kms)";s:4:"desc";s:3501:" Morning 4x4 game drive.
 
‐ Breakfast
 
‐ The Blyde River Canyon is the third deepest canyon in the world (after the Grand Canyon in the western U.S. and Namibia's Fish
River Canyon). It ranks as one of the most spectacular sights in Africa. The mountain scenery and panoramic views over the Klein
Drakensberg escarpment are quite spectacular and give the area its name of the Panorama Route. Viewpoints are named for the
vistas they offer, and God's Window and Wonder View hint at the magnitude of the scenery. The 'Pinnacle' is a single quartzite
column  rising  out  of  the  deep  wooded  canyon  and  the  Three  Rondavels  (also  called  'Three  Sisters')  are  three  huge  spirals  of
dolomite rock rising out of the far wall of the Blyde River Canyon. The entire canyon extends over twenty kilometres in length.
(suggestion not included in rates)
 
‐ Lunch
 
‐ Visit  Pilgrim's  Rest.  The  village  is  situated  on  the  magnificent  Panorama  Route  on  the  eastern  escarpment  region  of  the
Mpumalanga province. The entire town of Pilgrim's Rest has been declared a national monument in 1986. Here, visitors can relive
the days of the old Transvaal gold rush. Pilgrim's Rest was declared a gold field in 1873, soon after digger Alec "Wheelbarrow"
Patterson discovered gold deposits in Pilgrim's Creek. The Valley proved to be rich in gold and by the end of the year, there were
about 1500 diggers working in the area. As a result, Pilgrim's Rest became a social centre of the diggings. There is plenty to do and
experience here, from browsing through exciting curio and craft shops to discovering fascinating historical sights. (suggestion not
included in rates)
 
‐ Travel to Johannesburg
 
‐ Dinner: Carnivore Restaurant
A franchise of the world‐renowned Carnivore restaurant in Nairobi, Kenya, the restaurant is located in some natural surroundings
overlooking the Krugersdorp hills. Its main attraction is a large circular open fire with 52 converted Masaai tribal spears skewed
with a variety of meat, such as pork, lamb, beef, chicken, ribs and sausages, and also crocodile, zebra, giraffe, impala, ostrich and
kudu, roasting slowly and waiting for you to begin your feast! Waiters walk from the fire to your table holding spears skewed with
the meat and carve the cuts of your choice directly onto your plate until you have eaten your fill. Surely Africa's Greatest Eating
Experience.
 
‐ Night: Misty Hills (Delux Room ‐ Bed and Breakfast (Ger/Fre))
Image_57_0
Blyde River canyon ‐ Three Rondavels
Pilgrims Rest ‐ Pilgrims Rest
Dinner ‐ Johannesburg suburbs
Image_58_0
D10 12/03/16 Johannesburg ‐ Pretoria (131 kms)
Image_59_0
 
‐ Breakfast
 
‐ Visit the Apartheid Museum, dedicated to recounting the history of the rise and fall of apartheid in 20th century South Africa.
Interactive  multimedia  displays  recreate  the  experience  of  racial  segregation  with  exhibits  dedicated  to  historical  facts
accompanied  by  personal  testimony  :  harrowing,  unforgettable  and  uniquely  South  African.  (No  Guided  tours  on  Mondays)
(suggestion not included in rates)
 
‐ Visit Soweto. Cursed child of apartheid, Johannesburg's South Western Township today comprises close to 4 million inhabitants.
The revolts of '76 that took place there marked the beginning of the decline of the segregation policy. (suggestion not included in
rates)
 
‐ Buffet lunch: Sakhumzi Restaurant 

";}}";}s:16:"st_custom_layout";a:1:{i:0;s:4:"1118";}s:16:"is_sale_schedule";a:1:{i:0;s:3:"off";}s:8:"check_in";a:1:{i:0;s:10:"2015-07-25";}s:9:"check_out";a:1:{i:0;s:10:"2015-08-01";}s:17:"_vc_post_settings";a:1:{i:0;s:30:"a:1:{s:10:"vc_grid_id";a:0:{}}";}s:10:"_edit_lock";a:1:{i:0;s:12:"1469724466:2";}s:10:"_edit_last";a:1:{i:0;s:1:"2";}s:12:"_wp_old_slug";a:2:{i:0;s:14:"tour-ultricies";i:1;s:39:"discover-south-africa-13-days-12-nights";}s:10:"type_price";a:1:{i:0;s:10:"tour_price";}s:11:"adult_price";a:1:{i:0;s:4:"2999";}s:11:"child_price";a:1:{i:0;s:4:"3499";}s:9:"type_tour";a:1:{i:0;s:10:"daily_tour";}s:15:"sale_price_from";a:1:{i:0;s:10:"2015-07-01";}s:13:"sale_price_to";a:1:{i:0;s:10:"2015-07-29";}s:19:"accommodation_video";a:1:{i:0;s:72:"https://www.youtube.com/playlist?list=PLjqSFpDvnZb6oM0zIagz54NGtuGmwZXpm";}s:20:"_accommodation_video";a:1:{i:0;s:19:"field_55c5e8f26c5e9";}s:11:"video_title";a:1:{i:0;s:34:"Discover South Africa and Zimbabwe";}s:12:"_video_title";a:1:{i:0;s:19:"field_55c5ed93ae5be";}s:10:"video_text";a:1:{i:0;s:295:"Cape Town:  Capetonian Hotel <br />
Durban:  The Boulevard 44 Boutique Hotel<br />
Hluhluwe:  Zulu Nyala Heritage Safari Lodge<br />
Mbabane:  Mantenga Lodge<br />
Kruger:  Hippo Hollow Country Estate<br />
Kruger:  Karongwe River Lodge<br />
Johannesburg:  Misty Hills Country Hotel<br />";}s:11:"_video_text";a:1:{i:0;s:19:"field_55c5edabae5bf";}s:3:"map";a:1:{i:0;s:1331:"<iframe src="https://www.google.com/maps/embed?pb=!1m58!1m12!1m3!1d7107870.695564671!2d20.62184711706626!3d-29.555976896321436!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m43!3e0!4m5!1s0x1dcc4542f7400bbd%3A0x40487579e3cf5e90!2sCape+Town+International+Airport%2C+Cape+Town%2C+7490!3m2!1d-33.971463!2d18.6020851!4m5!1s0x1dcc6761cca039ab%3A0xc06da1cc10cb0436!2sThe+Capetonian%2C+Heerengracht+Street%2C+Cape+Town%2C+Western+Cape%2C+South+Africa!3m2!1d-33.918076!2d18.426735!4m5!1s0x1ef7aa0001bc61b7%3A0xcca75546c4aa6e81!2sDurban%2C+South+Africa!3m2!1d-29.858680399999997!2d31.0218404!4m5!1s0x1efafb4cc12f0b5b%3A0xe3b98c582a712337!2sZulu+Nyala+Heritage+Safari+Lodge%2C+Hluhluwe%2C+South+Africa!3m2!1d-27.951459999999997!2d32.247087!4m5!1s0x1eef34afc66a6725%3A0x6d3cf6626743860e!2sMantenga+Lodge%2C+Mantenga+Falls+Road%2C+2%2C+H106%2C+Swaziland!3m2!1d-26.4412001!2d31.1813343!4m5!1s0x1ee831093f79e607%3A0xf00a2920ed593685!2sCasterbridge+Hollow+Boutique+Hotel%2C+White+River%2C+Mpumalanga%2C+South+Africa!3m2!1d-25.328438499999997!2d31.017783599999998!4m5!1s0x1e959c679e657fbf%3A0x581955ea1822582d!2sMisty+Hills+Country+Hotel%2C+Conference+Centre+%26+Spa%2C+Johannesburg%2C+South+Africa!3m2!1d-26.028807999999998!2d27.8567!5e0!3m2!1sen!2sus!4v1439404296921" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>";}s:4:"_map";a:1:{i:0;s:19:"field_55c5fc80010d9";}s:21:"international_airfare";a:1:{i:0;s:35:"Not included *contact for discounts";}s:22:"_international_airfare";a:1:{i:0;s:19:"field_55c609ee3dfdc";}s:15:"departure_dates";a:1:{i:0;s:34:"52 weeks/year, Thursday - Saturday";}s:16:"_departure_dates";a:1:{i:0;s:19:"field_55c609f63dfdd";}s:9:"tour_type";a:1:{i:0;s:7:"Private";}s:10:"_tour_type";a:1:{i:0;s:19:"field_55c60a373dfde";}s:4:"what";a:1:{i:0;s:178:"• All accommodation <br />
• Vehicle rental<br />
• Mileage<br />
• Meals as noted<br />
• All transportation including airport transfers as per itinerary<br />

";}s:5:"_what";a:1:{i:0;s:19:"field_563c35c45737f";}s:15:"whats__included";a:1:{i:0;s:340:"• International flights<br />
• Visas<br />
• Driver/guide/cook/porter gratuities<br />
• Telephone bills, laundry, and any items of a personal nature<br />
• Cost associated with activities not listed on the itinerary<br />
• Travel insurance<br />
• National park, reserve, and conservation fees as per itinerary<br />";}s:16:"_whats__included";a:1:{i:0;s:19:"field_563c367c57380";}s:19:"summary_of_wildlife";a:1:{i:0;s:151:"Lion, Rhino, Elephant, Leopard, Cape Buffalo, Cheetah, Giraffe, Zebra, Wildebeest, Eland, Kudu, Thompsons Gazelle, Topi, Ostrich, 400+ species of birds";}s:20:"_summary_of_wildlife";a:1:{i:0;s:19:"field_563c36ac57381";}s:15:"whats_included_";a:1:{i:0;s:20:"a:1:{i:0;s:3:"Yes";}";}s:16:"_whats_included_";a:1:{i:0;s:19:"field_564040b08ad76";}s:29:"click_show_whats_not_included";a:1:{i:0;s:20:"a:1:{i:0;s:3:"Yes";}";}s:30:"_click_show_whats_not_included";a:1:{i:0;s:19:"field_564041e58ad77";}s:25:"click_summary_of_wildlife";a:1:{i:0;s:20:"a:1:{i:0;s:3:"Yes";}";}s:26:"_click_summary_of_wildlife";a:1:{i:0;s:19:"field_564042388ad78";}s:8:"order_by";a:1:{i:0;s:2:"18";}s:9:"_order_by";a:1:{i:0;s:19:"field_566026addd597";}s:11:"travel_date";a:1:{i:0;s:0:"";}s:12:"_travel_date";a:1:{i:0;s:19:"field_56604f0ae0688";}s:11:"last_minute";a:1:{i:0;s:0:"";}s:12:"_last_minute";a:1:{i:0;s:19:"field_5662c08454ce7";}s:9:"home_page";a:1:{i:0;s:0:"";}s:10:"_home_page";a:1:{i:0;s:19:"field_5662d8f31d20a";}s:12:"_dp_original";a:1:{i:0;s:4:"6431";}s:13:"_thumbnail_id";a:1:{i:0;s:4:"6449";}s:14:"seasonal_price";a:1:{i:0;s:0:"";}s:15:"_seasonal_price";a:1:{i:0;s:19:"field_5758328d87a85";}s:35:"_yoast_wpseo_primary_seasonal-price";a:1:{i:0;s:0:"";}s:36:"_yoast_wpseo_primary_seasonal-price1";a:1:{i:0;s:0:"";}s:33:"_yoast_wpseo_primary_st_tour_type";a:1:{i:0;s:0:"";}s:26:"_yoast_wpseo_content_score";a:1:{i:0;s:2:"60";}}}