���W<?php exit; ?>a:1:{s:7:"content";a:1:{i:0;O:8:"stdClass":4:{s:7:"meta_id";s:5:"20375";s:7:"post_id";s:4:"6304";s:8:"meta_key";s:13:"tours_program";s:10:"meta_value";s:5864:"a:8:{i:6;a:2:{s:5:"title";s:28:"Day 1,  Friday:  Depart Home";s:4:"desc";s:154:"Today your board your international departure from your home city bound for Nairobi, Kenya.  Your baggage is checked to and claimed in Nairobi on arrival.";}i:0;a:2:{s:5:"title";s:32:"Day 2, Saturday:  Arrive Nairobi";s:4:"desc";s:488:"Following your flight Odyssey Safari staff will greet you at the airport and load your bags for transport to the hotel. Once you are settled in you may select an optional activity or simply relax at the camp for the trip ahead. Some of the possible optional activities include: Bomas of Kenya, Sheldrick Elephant Orphanage, Karen Blixen Museum, Tour of Downtown Nairobi, National Museum, or Langata Giraffe Center.
Overnight at Safari Park hotel & Casino 
Meal Plan:  Bed & Breakfast 
";}i:1;a:2:{s:5:"title";s:51:"Day 3-4, Sunday - Monday:  Samburu National Reserve";s:4:"desc";s:1230:"This morning we travel by road to Kenya’s northern frontier. Wild and rugged, Samburu is fiercely beautiful with its arid savannahs watered by the lovely Uaso Nyiro River. It is also one of Kenya’s most protected areas and home to species only found north of the equator, from the reticulated giraffe to Grevy’s zebras and Beisa Oryxes. Enjoy Ashnil Tented Camp, our hostelry set beneath a canopy of acacia trees along the banks of the river.
Overnight at Ashnil Samburu tented camp
Travel time: 4hrs – 5hrs

Samburu Game Reserve
Dawn breaks in golden bands as we start our day in the bush by scanning the river from our tent’s veranda with a cup of steaming coffee in hand. Before the dew is off the grass, we’re off in pursuit of the elephants, lions, leopards, and cheetahs that thrive in the reserve’s grasslands and acacia woodlands. And so the day is spent in alternating pursuits of exploration in the bush and relaxation in our cozy camp.
Overnight at Ashnil Samburu tented camp
Morning game drive – 6.15 a.m. to 9.00 a.m.
Mid day game drive – 11.00am to 1.00 p.m. or optional visit Samburu cultural village
Evening game drive – 4.00 p.m. to 6.45 p.m.
Meal Plan:  Breakfast, Lunch & Dinner 
";}i:2;a:2:{s:5:"title";s:54:"Day 5, Tuesday:  Ol Pejeta & Sweetwaters Conservancies";s:4:"desc";s:811:"Today we travel to the exotic Sweetwaters Tented Camp in the secluded heart of Ol Pejeta Ranch and Rhino Reserve. Here, these magnificent beasts are given sanctuary from those who would take their lives to sell their horns. Here too, the Jane Goodall Institute operates a remarkable Chimpanzee Sanctuary where we meet the primates face to- face. Our camp also overlooks one of the busiest waterholes in the area and offers elephant, giraffe and myriad plains game. This is Africa as Teddy Roosevelt saw it years ago when he led one of the first safaris to tour Africa in comfort and splendor; our tents at Sweetwaters Camp will remind us of this bygone era.
Overnight at Sweetwaters Tented Camp 
Drive time: 2 1⁄2 hrs
Evening game drive – 3.00 p.m. to 6.45 p.m. 
Meal Plan:  Breakfast, Lunch & Dinner 
";}i:3;a:2:{s:5:"title";s:44:"Day 6, Wednesday:  Lake Nakuru National Park";s:4:"desc";s:765:"Today we travel to the exotic Lake Nakuru National Park located in the Great Rift Valley. After lunch an exciting game run is planned for the afternoon. The game in this park is plentiful and the magnificent bird watching will make a devotee out of even the most reluctant. The Park is a home to stunning flocks of lesser and Greater Flamingos, which literally turn the lakeshores in to a magnificent pink stretch. The bird life here is world renown and over 400 bird species exist here, White Pelicans, Plovers, Egrets and Marabou Stork. It is also one of the very few places in Africa to see the white Rhino and rare Rothschild Giraffe.
Overnight at Sarova Lionhill Lodge 
Evening game drive – 3.00 p.m. to 6.15 p.m. 
Meal Plan:  Breakfast, Lunch & Dinner 
";}i:4;a:2:{s:5:"title";s:57:"Day 7-8, Thursday - Friday:  Maasai Mara National Reserve";s:4:"desc";s:1146:"This morning we drive through the dramatic Great Rift Valley to the Masai Mara National Reserve. This enormous reserve is actually part of the vast Serengeti plains famously known for its spectacular great wildebeest’s migration and is home of the Big Five: Lion, Elephant, Leopard, Buffalo and Rhino. Lunch at our Lodge and relax before departing for the afternoon game run. The Mara Game Reserve- one of the greatest wildernesses of the world. Large mammals are varied, and easy to see. Residents among the Park's are: Masai Giraffe, Buffalo, Eland and thousand of plain game including Impala, Zebra, Topi, both Thomson's and grants Gazelles.
Overnight at Ashnil Mara Tented camp
Evening game drive – 3.30 p.m. to 6.45 p.m. 
Meal Plan:  Breakfast, Lunch & Dinner 

Discover Masai Mara
The search for great predators and perhaps even cubs continues today during extensive game drives. On the plains are enormous herds of grazing animals plus the elusive cheetah and leopard hiding amidst acacia boughs.
Overnight at Ashnil Mara Tented camp
Full day game drive: 8.00 a.m. to 6.00 p.m. 
Meal Plan:  Breakfast, Picnic Lunch & Dinner 
";}i:5;a:2:{s:5:"title";s:39:"Day 9, Saturday:  Maasai Mara - Nairobi";s:4:"desc";s:466:"Enjoy early Breakfast at the Camp. Checkout of the Camp and conduct a game drive en-route with an optional opportunity to visit a village of the Maasai people to witness the singing and dancing that are part of their daily lives and sacred rituals. A glimpse into their homes and social structure is a poignant experience. Drive to Nairobi and thereafter to the airport for outbound flight.
Meal Plan:  Breakfast, lunch
 
Meal Plan:  Breakfast and Picnic lunch 
";}i:7;a:2:{s:5:"title";s:28:"Day 10, Sunday:  Arrive Home";s:4:"desc";s:64:"Welcome home!  Your flight touches down today in your home city.";}}";}}}