<?php
/*
Template Name: Home
*/
/**
 * @package WordPress
 * @subpackage Traveler
 * @since 1.0
 *
 * Template Name : Home
 *
 * Created by ShineTheme
 *
 */
get_header();
while(have_posts()){
    the_post();
    the_content();
}
?>
<div id="home_page" style="display:none">
<div class="row row-wrap">
<?php
 //query_posts('post_type=st_tours&posts_per_page=5&meta_key=home_page'); ?>
 <?php //if (have_posts()) : while (have_posts()) : the_post();
//$image_id = get_post_thumbnail_id();
//$image_url = wp_get_attachment_image_src($image_id, 'large', true);
//$thumburl = $image_url[0];
//$Url=get_the_permalink();
//$last_id=get_the_ID();
//$home_val = get_field('home_page',$last_id);
//if($home_val[0] == 'Yes'){

$the_query = new WP_Query(array('post_type' => 'st_tours') );
while ($the_query->have_posts()) : $the_query->the_post();
$Url=get_the_permalink();
$image_id = get_post_thumbnail_id();
$image_url = wp_get_attachment_image_src($image_id, 'large', true);
$thumburl = $image_url[0];
 $last_id=get_the_ID();
 $home_val = get_field('home_page',$last_id);
 $home_val[0];
if($home_val[0] == 'Yes'){
     ?>

  <div class="col-md-3 style_box col-sm-6 col-xs-12 st_fix_4_col" >
    <div class="thumb" id="<?php echo $last_id=get_the_ID(); ?>">
      <header class="thumb-header"> <a href="<?php echo $Url; ?>" class="hover-img"> <img width="800" height="600" src="<?php echo $thumburl ?>" class="attachment-800x600x1 wp-post-image" alt="<?php echo the_title(); ?>">
        <h5 class="hover-title hover-hold"> <?php echo the_title(); ?> </h5>
        </a> </header>
      <div class="thumb-caption" id="thumb_caption">
        <div class="row mt10">
          <div class="col-md-6 col-sm-6 col-xs-6"> <i class="fa fa-location-arrow"></i> <?php echo get_the_title(get_post_meta($last_id , 'id_location', true)); ?> </div>
          <div class="col-md-6 col-sm-6 col-xs-6 text-right">
            <ul class="icon-group text-color pull-right">
              <li><i class="fa fa-star-o"></i></li>
              <li><i class="fa fa-star-o"></i></li>
              <li><i class="fa fa-star-o"></i></li>
              <li><i class="fa fa-star-o"></i></li>
              <li><i class="fa fa-star-o"></i></li>
            </ul>
            <div class="pull-right"><i class="fa fa-thumbs-o-up icon-like"> &nbsp;</i> </div>
          </div>
        </div>
        <div class="row mt10">
          <div class="col-md-6 col-sm-6 col-xs-6">
            <p class="mb0 text-darken"> <i class="fa fa-money"></i> Price: <span class="text-lg lh1em text-color"> <?php echo STTour::get_price_html(); ?> </span> </p>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-6 text-right"> <a href="<?php echo $Url; ?>" type="button" class="btn btn-primary">Book Now <i class="fa fa-shopping-cart"></i></a> </div>
        </div>
      </div>
    </div>
  </div>

<?php
}
endwhile;
//wp_reset_postdata(); ?>

 </div>
 </div>
<script>
jQuery(document).ready(function($){
	$(".wpb_button").css( { "margin-left" : "155px"} );
	$(".wpb_video_wrapper").css({"height" : "500px", "text-align" : "center", "border-style" : "solid", "border-width" : "2px", "border-color" : "#5e1e03", "background-color" : "#fff", "padding" : "4px"});
	var address = $(".form-group-lg > label:first").text();
	if(address=='Address'){
	$(".form-group-lg > label:first").text('Country');
	}
	$('.search').find('.row').after('<div style="color: #93a465;text-align: center;padding: 0px 0px 15px 0px;font-size: 16px;font-weight: bold;">Enter your travel window and we will return all tours available for that date period.  </div>');
	var sels = jQuery('#home_page').html();
	//jQuery('.row-wrap').hide();
	jQuery(".list_tours").find('.row-wrap').css('display','block')
	jQuery('.list_tours').append(sels);
	jQuery('.vc_custom_1433348111833').hide();
	jQuery('.no-boder-search').show();
	jQuery('#tab-tour').show();
	jQuery('.vc_custom_1420711771496').hide();
	var serch = jQuery('.no-boder-search').html();
	var video = jQuery('.vc_custom_1433348111833').html();
	jQuery('.list_tours').after(video);
	jQuery('.vc_custom_1439605325510').after(serch);
	jQuery('.tabbable').css({"background": "rgb(255, 255, 255) none repeat scroll 0% 0%","padding": "26px"});
   });
</script>
<?php get_footer(); ?>
