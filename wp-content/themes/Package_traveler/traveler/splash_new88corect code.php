<?php
/**
 * @package WordPress
 * @subpackage Traveler
 * @since 1.0
 *	Template name: splash_new2
 * Single blog
 *
 * Created by ShineTheme
 *
 */
get_header();
?>
<?php $con=$_GET['con'];

$offers=$_GET['offers']; 
$schedule=$_GET['scheduled'];
$private=$_GET['private'];
if(!empty($offers)){
$tur=$offers;
}else if(!empty($schedule)){
$tur=$schedule;
}else if(!empty($private)) {
$tur=$private;
}
?>
<style> a{ text-decoration:none !important;}
 hover{ text-decoration:none !important;}
 p {
    padding: 0;
    margin: 0;
    display: inline;
}
</style>
    <div class="container">

        <h1 class="page-title">
	<?php	if(isset($_GET["con"])) { ?>
		<?php  $text = str_replace("-", '   ', $con);
		if($text == 'South'){
		echo "South Africa Safaris"; }else{
		echo ucwords($text)."&nbsp;Safaris";
		} //echo ucfirst(strtolower($con)); ?>
        <?php }else if($_GET['scheduled']){ 
		$text = str_replace("-", '   ', $schedule);
		echo ucwords($text)."&nbsp;Safaris"; } else if($_GET['private']){
		$text = str_replace("-", '   ', $private);
		echo ucwords($text)."&nbsp;Safaris"; }else if($_GET['offers']){ 
		$text = str_replace("-", '   ', $offers);
		echo ucwords($text)."&nbsp;Safaris";}else{ echo "Safaris Tours"; }?>
        </h1>
        <div class="row">
        <?php if(!empty($tur)){ ?>
        <?php $offer=mysql_query("SELECT  c.* FROM wp_terms a INNER JOIN wp_term_taxonomy b ON a.term_id = b.term_id INNER JOIN wp_term_relationships c ON b.term_taxonomy_id = c.term_taxonomy_id WHERE  a.slug = '$tur'"); 
while($offetch=mysql_fetch_array($offer)){

 $tid=$offetch['object_id']; ?>

       
<?php query_posts('p='.$tid.'&post_type=st_tours');
while (have_posts()): the_post(); ?>
  <?php //$tours=get_post_meta('');
 //var_dump($tours)or die();   ?>
     <?php
                        $image_id = get_post_thumbnail_id();
                        $image_url = wp_get_attachment_image_src($image_id, 'large', true);
                        $thumburl = $image_url[0];
                        ?> 
    <?php   $type_price = get_post_meta(get_the_ID(),'type_price',true);
$type_tour = get_post_meta(get_the_ID(),'type_tour',true);

if($type_price=='people_price')
{
    $info_price=STTour::get_price_person(get_the_ID());
}else{
    $info_price = STTour::get_info_price(get_the_ID());

}
 $tourid=get_the_ID();
 $Url=get_the_permalink();

?>  
    
<div class="package-info-wrapper pull-left vc_custom_1435841910582" style="width: 100%">
    <div class="row">
        <div class="col-md-7">
        <div class="col-md-4 rowvalue wpb_column column_container vc_custom_1435845291177" style="padding-left:0px;">
			
	<div class="wpb_single_image wpb_content_element imagerad vc_align_left">
		<div class="wpb_wrapper">
			
			<div class="vc_single_image-wrapper" style="margin-bottom:90px;"><img width="300" height="300" alt="lo_20121030_061219_5_1" class="vc_single_image-img attachment-medium" src="<?php echo $thumburl ?>" style="border: 3px #93a465 solid !important;">
            <div class="package-info pull-right" style=" margin:22px">
               <!--<div class="pull-left">
                    <i class="fa fa-info"></i>
                    <span class="head"><?php st_the_language('tour_rate') ?>:</span>
                </div> 
                
                <div  class="pull-left pl-5">
                    <ul class="icon-group booking-item-rating-stars">
                        <?php
                        $avg = STReview::get_avg_rate();
                        echo TravelHelper::rate_to_string($avg);
                        ?>
                    </ul>
                </div>-->
                <script type="text/javascript">(function(){var sb=document.createElement('script');sb.type='text/javascript';sb.async=true;sb.src='https://s3.amazonaws.com/z_437er23a/895898.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(sb,s);})();</script>

<div style="display: inline-block; float: left; padding: 0 10px 5px 0;"><span class="1fd0285910">&nbsp;</span></div>
            </div></div>
            
		</div> 
        
	</div> 
    
	</div>
    
		<div class="wpb_wrapper">
			<div class="heading active">
<h3><?php the_title(); ?></h3>
</div>

		</div> 
	<div class="package-info">
                <i class="fa fa-location-arrow"></i>
                <span class="head"><?php st_the_language('tour_location') ?>: </span>
                <?php echo get_the_title(get_post_meta(get_the_ID() , 'id_location', true)); ?>
            </div>
            
              <?php if($tourid == '5158'){ ?>  
    		
<?php
$args = array( 'post_type' => 'kenya-safari-tour', 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
<?php $tile=get_the_title(); ?>
                <?php if($tile == 'TOUR TYPE'){ ?>
    <div class="package-info">
                <i class="fa fa-certificate"></i>
                
            <span class="head certi"><?php the_title(); ?>: </span><?php the_content(); ?>
                         </div>  
                           <?php  } else{}?>       
    <?php
 endwhile;
wp_reset_postdata(); 
} else if($tourid == '1031'){ ?>  
    		
<?php
$args = array( 'post_type' => 'highlights-of-tanzan', 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
<?php $tile=get_the_title(); ?>
                <?php if($tile == 'TOUR TYPE'){ ?>
    <div class="package-info">
                <i class="fa fa-certificate"></i>
            <span class="head certi"><?php the_title(); ?>: </span><?php the_content(); ?>
                         </div>
                         <?php  } else{}?>        
    <?php
 endwhile;
 wp_reset_postdata();
} else if($tourid == '1030'){ ?>  
    		
<?php
$args = array( 'post_type' => 'discover-south-afric', 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
<?php $tile=get_the_title(); ?>
                <?php if($tile == 'TOUR TYPE'){ ?>
    <div class="package-info">
                <i class="fa fa-certificate"></i>
            <span class="head certi"><?php the_title(); ?>: </span><?php the_content(); ?>
                         </div> 
                         <?php  } else{}?>      
    <?php
 endwhile;
 wp_reset_postdata();
} else if($tourid == '1029'){ ?>  
    		
<?php
$args = array( 'post_type' => 'elephant-experience', 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
 <?php $tile=get_the_title(); ?>
                <?php if($tile == 'TOUR TYPE'){ ?>
    <div class="package-info">
                <i class="fa fa-certificate"></i>
               
            <span class="head certi"><?php the_title(); ?>: </span><?php the_content(); ?>
          
          </div>
          <?php  } else{}?>       
    <?php
	
 endwhile;
 wp_reset_postdata();
}  ?>     
              
            <?php if($type_tour == 'specific_date'){ ?>
                <div class="package-info">
                    <i class="fa fa-calendar"></i>
                    <span class="head"><?php st_the_language('tour_duration') ?>: </span>
                    <?php
                    $check_in = get_post_meta(get_the_ID() , 'check_in' ,true);
                    $check_out = get_post_meta(get_the_ID() , 'check_out' ,true);
                    $date = mysql2date('m/d/Y',$check_in).' <i class="fa fa-long-arrow-right"></i> '.mysql2date('m/d/Y',$check_out);
                    if(!empty($check_in) and !empty($check_out)){
                        echo balanceTags($date);
                    }else{
                        st_the_language('tour_none');
                    }
                    ?>
                </div>
            <?php }else{ ?>
                <div class="package-info">
                    <i class="fa fa-calendar"></i>
                    <span class="head"><?php _e('Duration',ST_TEXTDOMAIN) ?>: </span>
                    <?php $duration_day =  get_post_meta(get_the_ID() , 'duration_day', true); ?>
                    <?php if($duration_day>1){
                        printf(__('%d days',ST_TEXTDOMAIN),$duration_day);
                    }elseif($duration_day==1){
                        _e('1 day',ST_TEXTDOMAIN);
                    } ?>
                </div>
            <?php } ?>
            
              <?php if($tourid == '5158'){ ?>  
    		
<?php
$args = array( 'post_type' => 'kenya-safari-tour', 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
<?php $tile=get_the_title(); ?>
                <?php if($tile !== 'TOUR TYPE'){ ?>
    <div class="package-info">
                <i class="fa fa-certificate"></i>
                
            <span class="head certi"><?php the_title(); ?>: </span><?php the_content(); ?>
                         </div>  
                           <?php  } else{}?>       
    <?php
 endwhile;
wp_reset_postdata(); 
} else if($tourid == '1031'){ ?>  
    		
<?php
$args = array( 'post_type' => 'highlights-of-tanzan', 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
<?php $tile=get_the_title(); ?>
                <?php if($tile !== 'TOUR TYPE'){ ?>
    <div class="package-info">
                <i class="fa fa-certificate"></i>
            <span class="head certi"><?php the_title(); ?>: </span><?php the_content(); ?>
                         </div>
                         <?php  } else{}?>        
    <?php
 endwhile;
 wp_reset_postdata();
} else if($tourid == '1030'){ ?>  
    		
<?php
$args = array( 'post_type' => 'discover-south-afric', 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
<?php $tile=get_the_title(); ?>
                <?php if($tile !== 'TOUR TYPE'){ ?>
    <div class="package-info">
                <i class="fa fa-certificate"></i>
            <span class="head certi"><?php the_title(); ?>: </span><?php the_content(); ?>
                         </div> 
                         <?php  } else{}?>      
    <?php
 endwhile;
 wp_reset_postdata();
} else if($tourid == '1029'){ ?>  
    		
<?php
$args = array( 'post_type' => 'elephant-experience', 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
 <?php $tile=get_the_title(); ?>
                <?php if($tile !== 'TOUR TYPE'){ ?>
    <div class="package-info">
                <i class="fa fa-certificate"></i>
               
            <span class="head certi"><?php the_title(); ?>: </span><?php the_content(); ?>
          
          </div>
          <?php  } else{}?>       
    <?php
	
 endwhile;
 wp_reset_postdata();
}  ?>     
            
            <div class="package-info">
                <?php $max_people = get_post_meta(get_the_ID(),'max_people', true) ?>
                <i class="fa    fa-users"></i>
                <span class="head"><?php st_the_language('tour_max_people') ?>: </span>
                <?php echo esc_html($max_people) ?>
            </div>

            
            <div class="package-info">
                <i class="fa fa-money"></i>
                <span class="head"><?php st_the_language('tour_price') ?>: </span>
                <?php echo STTour::get_price_html(); ?>
            </div>
           
    
    

            
             <?php 
	
	$query=mysql_query("select post_content from wp_posts where ID ='$tourid'");
	$row=mysql_fetch_assoc($query); ?>
	<div style="margin-left: -11px;"><?php echo $row['post_content'];?></div>
            
            <div><a href="<?php echo $Url ; ?>" style="float:right; margin-top:20px; padding-top:20px">Read More</a></div>
        </div>
        		
         <?php   $title=get_the_title()." Accommodation";   
  $video=mysql_query("select * from wp_terms where name like '%".$title."%'");
  $cat=mysql_fetch_assoc($video);
   $catname = $cat['slug']; 
 $catname2 = $cat['name'];?> 
      <?php  if($title == $catname2){ ?>    
      <?php
$my_query = new WP_Query('category_name='.$catname.'&showposts=6');
while ($my_query->have_posts()) : $my_query->the_post();

?>
 <?php  $post_id=$post->ID;?>
 <div style="text-align:center">
			<h3>Accommodation</h3>
</div> 

        <div class="col-md-5">
<div class="panel-group" id="accordion_tours">
                    <div class="panel panel-default">
                <div class="panel-heading">
                    <h5 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion_tours" href="#collapse-<?php echo $post_id?>">
                           <h5><?php  the_title(); ?> </h5></a>
                    </h5>
                </div>
                <div class="panel-collapse collapse" id="collapse-<?php echo $post_id?>" style="height: 0px;">
                    <div class="panel-body">
                      <p><?php the_content(); ?></p> 
                      <p> <?php $query=mysql_query("select meta_value from wp_postmeta where meta_key='media' and post_id='$post_id'");
$video=mysql_fetch_assoc($query); 
$link = $video['meta_value']; 
 if(!empty($link)){
$videoso=str_replace("https://www.youtube.com/watch?v=", "", $link); ?>
<br/>
<center ><iframe title="YouTube video player" class="youtube-player" type="text/html" 
width="320"  height="220" src="https://youtube.com/embed/<?php echo  $videoso; ?>" frameborder="0" style="border:1px #5E1E03 solid;" allowFullScreen></iframe></center>
<?php }else{} ?></p> </div>
                </div>
            </div>
   </div>
<?php endwhile; ?>
      <?php } else {}?>                      
      </div>
       </div>
           
            <?php if(!empty($info_price['discount'])){ ?>
        <span class="box_sale sale_small btn-primary"> <?php echo esc_html($info_price['discount']) ?>% </span>
    <?php } ?>
    

</div>


<?php endwhile; ?> 

<?php }		  ?>
        
        
        
	<?php	}else if(!empty($con)){
		$con=mysql_query("select * from wp_posts where post_type='st_tours' and post_title like '%".$con."%' ");
		while($row=mysql_fetch_array($con)){
		 $tid=$row['ID'];  ?>
     
       
<?php query_posts('p='.$tid.'&post_type=st_tours');
while (have_posts()): the_post(); ?>
  <?php //$tours=get_post_meta('');
 //var_dump($tours)or die();   ?>
     <?php
                        $image_id = get_post_thumbnail_id();
                        $image_url = wp_get_attachment_image_src($image_id, 'large', true);
                        $thumburl = $image_url[0];
                        ?> 
    <?php   $type_price = get_post_meta(get_the_ID(),'type_price',true);
$type_tour = get_post_meta(get_the_ID(),'type_tour',true);

if($type_price=='people_price')
{
    $info_price=STTour::get_price_person(get_the_ID());
}else{
    $info_price = STTour::get_info_price(get_the_ID());

}
 $tourid=get_the_ID();
 $Url=get_the_permalink();

?>  
    
<div class="package-info-wrapper pull-left vc_custom_1435841910582" style="width: 100%">
    <div class="row">
        <div class="col-md-7">
        <div class="col-md-4 rowvalue wpb_column column_container vc_custom_1435845291177" style="padding-left:0px;">
			
	<div class="wpb_single_image wpb_content_element imagerad vc_align_left">
		<div class="wpb_wrapper">
			
			<div class="vc_single_image-wrapper" style="margin-bottom:90px;"><img width="300" height="300" alt="lo_20121030_061219_5_1" class="vc_single_image-img attachment-medium" src="<?php echo $thumburl ?>" style="border: 3px #93a465 solid !important;">
            <div class="package-info pull-right" style=" margin:22px">
               <!--<div class="pull-left">
                    <i class="fa fa-info"></i>
                    <span class="head"><?php st_the_language('tour_rate') ?>:</span>
                </div> 
                
                <div  class="pull-left pl-5">
                    <ul class="icon-group booking-item-rating-stars">
                        <?php
                        $avg = STReview::get_avg_rate();
                        echo TravelHelper::rate_to_string($avg);
                        ?>
                    </ul>
                </div>-->
                <script type="text/javascript">(function(){var sb=document.createElement('script');sb.type='text/javascript';sb.async=true;sb.src='https://s3.amazonaws.com/z_437er23a/895898.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(sb,s);})();</script>

<div style="display: inline-block; float: left; padding: 0 10px 5px 0;"><span class="1fd0285910">&nbsp;</span></div>
            </div></div>
            
		</div> 
        
	</div> 
    
	</div>
    
		<div class="wpb_wrapper">
			<div class="heading active">
<h3><?php the_title(); ?></h3>
</div>

		</div> 
	<div class="package-info">
                <i class="fa fa-location-arrow"></i>
                <span class="head"><?php st_the_language('tour_location') ?>: </span>
                <?php echo get_the_title(get_post_meta(get_the_ID() , 'id_location', true)); ?>
            </div>
            
              <?php if($tourid == '5158'){ ?>  
    		
<?php
$args = array( 'post_type' => 'kenya-safari-tour', 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
<?php $tile=get_the_title(); ?>
                <?php if($tile == 'TOUR TYPE'){ ?>
    <div class="package-info">
                <i class="fa fa-certificate"></i>
                
            <span class="head certi"><?php the_title(); ?>: </span><?php the_content(); ?>
                         </div>  
                           <?php  } else{}?>       
    <?php
 endwhile;
wp_reset_postdata(); 
} else if($tourid == '1031'){ ?>  
    		
<?php
$args = array( 'post_type' => 'highlights-of-tanzan', 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
<?php $tile=get_the_title(); ?>
                <?php if($tile == 'TOUR TYPE'){ ?>
    <div class="package-info">
                <i class="fa fa-certificate"></i>
            <span class="head certi"><?php the_title(); ?>: </span><?php the_content(); ?>
                         </div>
                         <?php  } else{}?>        
    <?php
 endwhile;
 wp_reset_postdata();
} else if($tourid == '1030'){ ?>  
    		
<?php
$args = array( 'post_type' => 'discover-south-afric', 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
<?php $tile=get_the_title(); ?>
                <?php if($tile == 'TOUR TYPE'){ ?>
    <div class="package-info">
                <i class="fa fa-certificate"></i>
            <span class="head certi"><?php the_title(); ?>: </span><?php the_content(); ?>
                         </div> 
                         <?php  } else{}?>      
    <?php
 endwhile;
 wp_reset_postdata();
} else if($tourid == '1029'){ ?>  
    		
<?php
$args = array( 'post_type' => 'elephant-experience', 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
 <?php $tile=get_the_title(); ?>
                <?php if($tile == 'TOUR TYPE'){ ?>
    <div class="package-info">
                <i class="fa fa-certificate"></i>
               
            <span class="head certi"><?php the_title(); ?>: </span><?php the_content(); ?>
          
          </div>
          <?php  } else{}?>       
    <?php
	
 endwhile;
 wp_reset_postdata();
}  ?>     
              
            <?php if($type_tour == 'specific_date'){ ?>
                <div class="package-info">
                    <i class="fa fa-calendar"></i>
                    <span class="head"><?php st_the_language('tour_duration') ?>: </span>
                    <?php
                    $check_in = get_post_meta(get_the_ID() , 'check_in' ,true);
                    $check_out = get_post_meta(get_the_ID() , 'check_out' ,true);
                    $date = mysql2date('m/d/Y',$check_in).' <i class="fa fa-long-arrow-right"></i> '.mysql2date('m/d/Y',$check_out);
                    if(!empty($check_in) and !empty($check_out)){
                        echo balanceTags($date);
                    }else{
                        st_the_language('tour_none');
                    }
                    ?>
                </div>
            <?php }else{ ?>
                <div class="package-info">
                    <i class="fa fa-calendar"></i>
                    <span class="head"><?php _e('Duration',ST_TEXTDOMAIN) ?>: </span>
                    <?php $duration_day =  get_post_meta(get_the_ID() , 'duration_day', true); ?>
                    <?php if($duration_day>1){
                        printf(__('%d days',ST_TEXTDOMAIN),$duration_day);
                    }elseif($duration_day==1){
                        _e('1 day',ST_TEXTDOMAIN);
                    } ?>
                </div>
            <?php } ?>
            
              <?php if($tourid == '5158'){ ?>  
    		
<?php
$args = array( 'post_type' => 'kenya-safari-tour', 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
<?php $tile=get_the_title(); ?>
                <?php if($tile !== 'TOUR TYPE'){ ?>
    <div class="package-info">
                <i class="fa fa-certificate"></i>
                
            <span class="head certi"><?php the_title(); ?>: </span><?php the_content(); ?>
                         </div>  
                           <?php  } else{}?>       
    <?php
 endwhile;
wp_reset_postdata(); 
} else if($tourid == '1031'){ ?>  
    		
<?php
$args = array( 'post_type' => 'highlights-of-tanzan', 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
<?php $tile=get_the_title(); ?>
                <?php if($tile !== 'TOUR TYPE'){ ?>
    <div class="package-info">
                <i class="fa fa-certificate"></i>
            <span class="head certi"><?php the_title(); ?>: </span><?php the_content(); ?>
                         </div>
                         <?php  } else{}?>        
    <?php
 endwhile;
 wp_reset_postdata();
} else if($tourid == '1030'){ ?>  
    		
<?php
$args = array( 'post_type' => 'discover-south-afric', 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
<?php $tile=get_the_title(); ?>
                <?php if($tile !== 'TOUR TYPE'){ ?>
    <div class="package-info">
                <i class="fa fa-certificate"></i>
            <span class="head certi"><?php the_title(); ?>: </span><?php the_content(); ?>
                         </div> 
                         <?php  } else{}?>      
    <?php
 endwhile;
 wp_reset_postdata();
} else if($tourid == '1029'){ ?>  
    		
<?php
$args = array( 'post_type' => 'elephant-experience', 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
 <?php $tile=get_the_title(); ?>
                <?php if($tile !== 'TOUR TYPE'){ ?>
    <div class="package-info">
                <i class="fa fa-certificate"></i>
               
            <span class="head certi"><?php the_title(); ?>: </span><?php the_content(); ?>
          
          </div>
          <?php  } else{}?>       
    <?php
	
 endwhile;
 wp_reset_postdata();
}  ?>     
            
            
            <div class="package-info">
                <?php $max_people = get_post_meta(get_the_ID(),'max_people', true) ?>
                <i class="fa    fa-users"></i>
                <span class="head"><?php st_the_language('tour_max_people') ?>: </span>
                <?php echo esc_html($max_people) ?>
            </div>

            
            <div class="package-info">
                <i class="fa fa-money"></i>
                <span class="head"><?php st_the_language('tour_price') ?>: </span>
                <?php echo STTour::get_price_html(); ?>
            </div>
           
    
    

            
             <?php 
	
	$query=mysql_query("select post_content from wp_posts where ID ='$tourid'");
	$row=mysql_fetch_assoc($query); ?>
	<div style="margin-left: -11px;"><?php echo $row['post_content'];?></div>
            
            <div><a href="<?php echo $Url ; ?>" style="float:right; margin-top:20px; padding-top:20px">Read More</a></div>
        </div>
        		
      <?php   $title=get_the_title()." Accommodation";   
  $video=mysql_query("select * from wp_terms where name like '%".$title."%'");
  $cat=mysql_fetch_assoc($video);
   $catname = $cat['slug']; 
 $catname2 = $cat['name'];?>    
      <?php
	  if($title == $catname2){ 
$my_query = new WP_Query('category_name='.$catname2.'&showposts=6');
while ($my_query->have_posts()) : $my_query->the_post();

?>
 <?php  $post_id=$post->ID;?>
 <div style="text-align:center">
			<h3>Accommodation</h3>
</div> 

        <div class="col-md-5">
<div class="panel-group" id="accordion_tours">
                    <div class="panel panel-default">
                <div class="panel-heading">
                    <h5 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion_tours" href="#collapse-<?php echo $post_id?>">
                           <h5><?php  the_title(); ?> </h5></a>
                    </h5>
                </div>
                <div class="panel-collapse collapse" id="collapse-<?php echo $post_id?>" style="height: 0px;">
                    <div class="panel-body">
                      <p><?php the_content(); ?></p> 
                      <p> <?php $query=mysql_query("select meta_value from wp_postmeta where meta_key='media' and post_id='$post_id'");
$video=mysql_fetch_assoc($query); 
$link = $video['meta_value']; 
 if(!empty($link)){
$videoso=str_replace("https://www.youtube.com/watch?v=", "", $link); ?>
<br/>
<center ><iframe title="YouTube video player" class="youtube-player" type="text/html" 
width="320"  height="220" src="https://youtube.com/embed/<?php echo  $videoso; ?>" frameborder="0" style="border:1px #5E1E03 solid;" allowFullScreen></iframe></center>
<?php }else{} ?></p> </div>
                </div>
            </div>
   </div>
<?php endwhile; ?>
      <?php } else {}?>                      
      </div>
       </div>
           
            <?php if(!empty($info_price['discount'])){ ?>
        <span class="box_sale sale_small btn-primary"> <?php echo esc_html($info_price['discount']) ?>% </span>
    <?php } ?>
    

</div>


<?php endwhile; ?> 
       
	<?php	}  ?>


 <?php } else {
		//var_dump($con)or die();?>
		  <?php query_posts('post_type=st_tours&posts_per_page=30'); ?>
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <?php
                        $image_id = get_post_thumbnail_id();
                        $image_url = wp_get_attachment_image_src($image_id, 'large', true);
                        $thumburl = $image_url[0];
                        ?>	
                       
                     <?php   $type_price = get_post_meta(get_the_ID(),'type_price',true);
$type_tour = get_post_meta(get_the_ID(),'type_tour',true);

if($type_price=='people_price')
{
    $info_price=STTour::get_price_person(get_the_ID());
}else{
    $info_price = STTour::get_info_price(get_the_ID());

}
 $tourid=get_the_ID();
 $Url=get_the_permalink();

?>
<div class="package-info-wrapper pull-left vc_custom_1435841910582" style="width: 100%">
    <div class="row">
        <div class="col-md-7">
        <div class="col-md-4 rowvalue wpb_column column_container vc_custom_1435845291177" style="padding-left:0px;">
			
	<div class="wpb_single_image wpb_content_element imagerad vc_align_left">
		<div class="wpb_wrapper">
			
			<div class="vc_single_image-wrapper" style="margin-bottom:90px; "><img width="300" height="300" alt="lo_20121030_061219_5_1" class="vc_single_image-img attachment-medium" src="<?php echo $thumburl ?>" style="border: 3px #93a465 solid !important;">
            <div class="package-info pull-right" style=" margin:22px">
               <!--<div class="pull-left">
                    <i class="fa fa-info"></i>
                    <span class="head"><?php st_the_language('tour_rate') ?>:</span>
                </div> 
                
                <div  class="pull-left pl-5">
                    <ul class="icon-group booking-item-rating-stars">
                        <?php
                        $avg = STReview::get_avg_rate();
                        echo TravelHelper::rate_to_string($avg);
                        ?>
                    </ul>
                </div>-->
                <script type="text/javascript">(function(){var sb=document.createElement('script');sb.type='text/javascript';sb.async=true;sb.src='https://s3.amazonaws.com/z_437er23a/895898.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(sb,s);})();</script>

<div style="display: inline-block; float: left; padding: 0 10px 5px 0;"><span class="1fd0285910">&nbsp;</span></div>
            </div>
            </div>
		</div> 
	</div> 
	</div>
    
		<div class="wpb_wrapper">
			<div class="heading active">
<h3><?php the_title(); ?></h3>
</div>

		</div> 
	<div class="package-info">
                <i class="fa fa-location-arrow"></i>
                <span class="head"><?php st_the_language('tour_location') ?>: </span>
                <?php echo get_the_title(get_post_meta(get_the_ID() , 'id_location', true)); ?>
            </div>
              
              
             <?php  //$title=get_the_title();
			 
			 if($tourid == '5158'){ ?>  
    		
<?php
$args = array( 'post_type' => 'kenya-safari-tour', 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
<?php //echo $tile=get_the_title();
$tile=get_the_ID(); ?>
                <?php if(strpos($tile, "Tour Type")){ ?>
    <div class="package-info">
                <i class="fa fa-certificate"></i>
                
            <span class="head certi"><?php the_title(); ?>: </span><?php the_content(); ?>
                         </div>  
                           <?php  } else{}?>       
    <?php
 endwhile;
wp_reset_postdata(); 
} else if($tourid == '1031'){ ?>  
    		
<?php
$args = array( 'post_type' => 'highlights-of-tanzan', 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
<?php $tile=get_the_title(); ?>
                <?php if($tile == 'TOUR TYPE'){ ?>
    <div class="package-info">
                <i class="fa fa-certificate"></i>
            <span class="head certi"><?php the_title(); ?>: </span><?php the_content(); ?>
                         </div>
                         <?php  } else{}?>        
    <?php
 endwhile;
 wp_reset_postdata();
} else if($tourid == '1030'){ ?>  
    		
<?php
$args = array( 'post_type' => 'discover-south-afric', 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
<?php $tile=get_the_title(); ?>
                <?php if(strpos($tile, "Tour Type")){ ?>
    <div class="package-info">
                <i class="fa fa-certificate"></i>
            <span class="head certi"><?php the_title(); ?>: </span><?php the_content(); ?>
                         </div> 
                         <?php  } else{}?>      
    <?php
 endwhile;
 wp_reset_postdata();
} else if($tourid == '1029'){ ?>  
    		
<?php
$args = array( 'post_type' => 'elephant-experience', 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
 <?php $tile=get_the_title(); ?>
                <?php if($tile == 'TOUR TYPE'){ ?>
    <div class="package-info">
                <i class="fa fa-certificate"></i>
               
            <span class="head certi"><?php the_title(); ?>: </span><?php the_content(); ?>
          
          </div>
          <?php  } else{}?>       
    <?php
	
 endwhile;
 wp_reset_postdata();
}  ?>     

 
              
              
              
              
            <?php if($type_tour == 'specific_date'){ ?>
                <div class="package-info">
                    <i class="fa fa-calendar"></i>
                    <span class="head"><?php st_the_language('tour_duration') ?>: </span>
                    <?php
                    $check_in = get_post_meta(get_the_ID() , 'check_in' ,true);
                    $check_out = get_post_meta(get_the_ID() , 'check_out' ,true);
                    $date = mysql2date('m/d/Y',$check_in).' <i class="fa fa-long-arrow-right"></i> '.mysql2date('m/d/Y',$check_out);
                    if(!empty($check_in) and !empty($check_out)){
                        echo balanceTags($date);
                    }else{
                        st_the_language('tour_none');
                    }
                    ?>
                </div>
            <?php }else{ ?>
                <div class="package-info">
                    <i class="fa fa-calendar"></i>
                    <span class="head"><?php _e('Duration',ST_TEXTDOMAIN) ?>: </span>
                    <?php $duration_day =  get_post_meta(get_the_ID() , 'duration_day', true); ?>
                    <?php if($duration_day>1){
                        printf(__('%d days',ST_TEXTDOMAIN),$duration_day);
                    }elseif($duration_day==1){
                        _e('1 day',ST_TEXTDOMAIN);
                    } ?>
                </div>
            <?php } ?>
            
                          
             <?php if($tourid == '5158'){ ?>  
    		
<?php
$args = array( 'post_type' => 'kenya-safari-tour', 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
 <?php $tile=get_the_title(); ?>
                <?php if($tile !== 'TOUR TYPE'){ ?>
    <div class="package-info">
                <i class="fa fa-certificate"></i>
                
            <span class="head certi"><?php the_title(); ?>: </span><?php the_content(); ?>
                         </div>  
                           <?php  } else{}?>     
    <?php
 endwhile;
wp_reset_postdata(); 
} else if($tourid == '1031'){ ?>  
    		
<?php
$args = array( 'post_type' => 'highlights-of-tanzan', 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
<?php $tile=get_the_title(); ?>
                <?php if($tile !== 'TOUR TYPE'){ ?>
    <div class="package-info">
                <i class="fa fa-certificate"></i>
            <span class="head certi"><?php the_title(); ?>: </span><?php the_content(); ?>
                         </div>
                         <?php  } else{}?>       
    <?php
 endwhile;
 wp_reset_postdata();
} else if($tourid == '1030'){ ?>  
    		
<?php
$args = array( 'post_type' => 'discover-south-afric', 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
<?php $tile=get_the_title(); ?>
                <?php if($tile !== 'TOUR TYPE'){ ?>
    <div class="package-info">
                <i class="fa fa-certificate"></i>
            <span class="head certi"><?php the_title(); ?>: </span><?php the_content(); ?>
                         </div>    
                         <?php  } else{}?>   
    <?php
 endwhile;
 wp_reset_postdata();
} else if($tourid == '1029'){ ?>  
    		
<?php
$args = array( 'post_type' => 'elephant-experience', 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
 <?php $tile=get_the_title(); ?>
                <?php if($tile !== 'TOUR TYPE'){ ?>
    <div class="package-info">
                <i class="fa fa-certificate"></i>
               
            <span class="head certi"><?php the_title(); ?>: </span><?php the_content(); ?>
          
          </div>
          <?php  } else{}?>       
    <?php
	
 endwhile;
 wp_reset_postdata();
}  ?> 
            
            
            
            
            <div class="package-info">
                <?php $max_people = get_post_meta(get_the_ID(),'max_people', true) ?>
                <i class="fa    fa-users"></i>
                <span class="head"><?php st_the_language('tour_max_people') ?>: </span>
                <?php echo esc_html($max_people) ?>
            </div>

            
            <div class="package-info">
                <i class="fa fa-money"></i>
                <span class="head"><?php st_the_language('tour_price') ?>: </span>
                <?php echo STTour::get_price_html(); ?>
            </div>
    
    

            
             <?php 
	
	$query=mysql_query("select post_content from wp_posts where ID ='$tourid'");
	$row=mysql_fetch_assoc($query); ?>
	<div style="margin-left: -11px;"><?php echo $row['post_content'];?></div>
            
            <div><a href="<?php echo $Url ; ?>" style="float:right; margin-top:20px; padding-top:20px">Read More</a></div>
        </div>
        <?php  $title=get_the_title()." Accommodation";   
  $video=mysql_query("select * from wp_terms where name like '%".$title."%'");
  $cat=mysql_fetch_assoc($video);
   $catname = $cat['slug']; 
 $catname2 = $cat['name']; ?>
 
      <?php    if($title == $catname2){ ?>    
        		<div style="text-align:center">
			<h3>Accommodation</h3>
</div> 

        <div class="col-md-5">
        
      <?php
$my_query = new WP_Query('category_name='.$catname.'&showposts=6');
while ($my_query->have_posts()) : $my_query->the_post();

?>
 <?php  $post_id=$post->ID;?>
<div class="panel-group" id="accordion_tours">
                    <div class="panel panel-default">
                <div class="panel-heading">
                    <h5 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion_tours" href="#collapse-<?php echo $post_id?>">
                           <h5><?php  the_title(); ?> </h5></a>
                    </h5>
                </div>
                <div class="panel-collapse collapse" id="collapse-<?php echo $post_id?>" style="height: 0px;">
                    <div class="panel-body">
                      <p><?php the_content(); ?></p> 
                      <p> <?php $query=mysql_query("select meta_value from wp_postmeta where meta_key='media' and post_id='$post_id'");
$video=mysql_fetch_assoc($query); 
$link = $video['meta_value']; 
 if(!empty($link)){
$videoso=str_replace("https://www.youtube.com/watch?v=", "", $link); ?>
<br/>
<center ><iframe title="YouTube video player" class="youtube-player" type="text/html" 
width="320"  height="220" src="https://youtube.com/embed/<?php echo  $videoso; ?>" frameborder="0" style="border:1px #5E1E03 solid;" allowFullScreen></iframe></center>
<?php }else{} ?></p> </div>
                </div>
            </div>
   </div>
<?php endwhile; ?>
      <?php } else {}?>                      
      </div>
       </div>
           
            <?php if(!empty($info_price['discount'])){ ?>
        <span class="box_sale sale_small btn-primary"> <?php echo esc_html($info_price['discount']) ?>% </span>
    <?php } ?>
    

</div>



	 
 <?php endwhile;
endif;
wp_reset_query(); ?>
<?php } ?>
        </div>
    </div>
<?php get_footer(); ?>