jQuery(document).ready(function($) {

    var newLabel = '';
    $('#vfb-16').on('change', function(){
     
    switch(this.value){
        case 'Visa':
            newLabel = '<a href="https://www.odysseysafaris.com/odyssey-terms-and-conditions" target="_new">Terms and Conditions visa</a>';
            break;
        case 'MasterCard':
            newLabel = '<a href="https://www.odysseysafaris.com/odyssey-terms-and-conditions" target="_new">Terms and Conditions mastercard</a>';
            break;
        case 'Discover':
            newLabel = '<a href="https://www.odysseysafaris.com/odyssey-terms-and-conditions" target="_new">Terms and Conditions discover</a>';
            break;
        case 'Amex':
            newLabel = '<a href="https://www.odysseysafaris.com/odyssey-terms-and-conditions" target="_new">Terms and Conditions amex</a>';
            break;
    }

    $('#item-vfb-37 label').html(newLabel); //Change the text before changing the value

    }).trigger('change');

    $('#credit-card-submission-authorization-form-1').validate({
        rules:{
            'vfb-35':{
                required: true,
                minlength: 3,
		maxlength: 4
            },
            'vfb-19':{
                required: true,
                minlength: 15,
		maxlength: 16
            },
            'vfb-confirm-test-answer-621':{
                equalTo: '#vfb-test-answer-620'
            },
            'vfb-end-date-85':{
                required: function(e){
                    return $('#vfb-start-date-80').val() !== '';
                }
            }
        },
        messages:{
            'vfb-35':{
                minlength: 'CVV2 number cannot be less than 3 characters long',
		maxlength: 'CVV2 number cannot be more than 4 characters long'
            },
            'vfb-19':{
                minlength: 'Credit Card numbers cannot be less than 15 characters long',
		maxlength: 'Credit Card numbers cannot be more than 16 characters long'
            },
            'vfb-confirm-test-answer-621':{
                equalTo: 'Your answers do not match.'
            }
        },
        errorPlacement: function(error, element) {
	        if ( element.is( ':radio' ) || element.is( ':checkbox' ) )
		    error.appendTo( element.parent().parent() );
	        else if ( element.is( ':password' ) )
		    error.hide();
	        else
		    error.insertAfter( element );
	    }
    });
});
