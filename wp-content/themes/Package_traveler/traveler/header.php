<?php
session_start();
/**
 * @package WordPress
 * @subpackage Traveler
 * @since 1.0
 *
 * Header
 *
 * Created by ShineTheme
 *
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="yoz5fED7gpa4txySMqH7qXfGZ4cSWwAkOAIWjdRTmYQ" />
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.simpleWeather/3.1.0/jquery.simpleWeather.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/bootstrap-multiselect.js"></script>
    <script type="text/javascript">$(function () { $('#chkveg').multiselect({ }); });
    </script>

    <!-- JQuery for Visual Form Builder -->
    <script type="text/javascript">
    jQuery(document).ready(function($) {
    $('#vfb-15').keyup(function(){
       var vfbVal = $(this).val().replace("$", "");
       var vfb15Val = (vfbVal*3.25)/100;
       var vfb15Val = vfb15Val.toFixed(2);	   
       $('#vfb-17').val(vfb15Val);
       //console.log(vfb15Val);
       //var total = Math.round(parseInt(vfb15Val)+parseInt(vfbVal));
       var total = parseFloat(vfb15Val)+parseFloat(vfbVal);
       console.log(total);
       var total = total.toFixed(2);

       $('#vfb-18').val(total);	
     });

     $("#credit-card-submission-authorization-form-1").validate({
	rules: {
	       'vfb-35': {
	       		 required: true,
			 minlength: 3,
			 maxlength: 4
		}
	}
     });

    });


    </script>
    <!-- End JQuery for Visual Form Builder -->

    <script src="https://s.codepen.io/assets/libs/modernizr.js" type="text/javascript"></script>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    <style>

i {
  color: #5e1e03;
  font-family: weather;
  font-size: 30px;
  font-weight: normal;
  font-style: normal;
  line-height: 1.0;
  text-transform: none;
}

#weather {
  text-align: center;
}

#weather h2 {
  margin: 0 0 8px;
  color: #5e1e03;
  font-size: 30px;
  font-weight: 300;
  text-align: center;
  text-shadow: 0px 1px 3px rgba(0, 0, 0, 0.15);
}

#weather ul {
  margin: 0;
  padding: 0;
  text-align: center;
}

#weather li {
  background: #fff;
  background: rgba(255,255,255,0.90);
  padding: 20px;
  display: inline-block;
  border-radius: 5px;
}

#weather .currently {
  margin: 0 20px;
}

#weather {
  text-align: center;
}

#weather2 h2 {
  margin: 0 0 8px;
  color: #5e1e03;
  font-size: 30px;
  font-weight: 300;
  text-align: center;
  text-shadow: 0px 1px 3px rgba(0, 0, 0, 0.15);
}

#weather2 ul {
  margin: 0;
  padding: 0;
  text-align: center;
}

#weather2 li {
  background: #fff;
  background: rgba(255,255,255,0.90);
  padding: 20px;
  display: inline-block;
  border-radius: 5px;
}

#weather2 .currently {
  margin: 0 20px;
}

#weather3 h2 {
  margin: 0 0 8px;
  color: #5e1e03;
  font-size: 30px;
  font-weight: 300;
  text-align: center;
  text-shadow: 0px 1px 3px rgba(0, 0, 0, 0.15);
}

#weather3 ul {
  margin: 0;
  padding: 0;
  text-align: center;
}

#weather3 li {
  background: #fff;
  background: rgba(255,255,255,0.90);
  padding: 20px;
  display: inline-block;
  border-radius: 5px;
}

#weather3 .currently {
  margin: 0 20px;
}

#weather4 h2 {
  margin: 0 0 8px;
  color: #5e1e03;
  font-size: 30px;
  font-weight: 300;
  text-align: center;
  text-shadow: 0px 1px 3px rgba(0, 0, 0, 0.15);
}

#weather4 ul {
  margin: 0;
  padding: 0;
  text-align: center;
}

#weather4 li {
  background: #fff;
  background: rgba(255,255,255,0.90);
  padding: 20px;
  display: inline-block;
  border-radius: 5px;
}

#weather4 .currently {
  margin: 0 20px;
}
</style>

<script type="text/javascript">
// v3.1.0
//Docs at http://simpleweatherjs.com
$(document).ready(function() {
  $.simpleWeather({
    location: 'Harare, Zimbabwe',
    woeid: '',
    unit: 'f',
    success: function(weather) {
      html = '<h2>'+weather.temp+'&deg;'+weather.units.temp+'</h2>';
      html += '<ul><li>'+weather.city+', '+weather.region+'</li>';

      html += '</ul>';
  
      $("#weather").html(html);
    },
    error: function(error) {
      $("#weather").html('<p>'+error+'</p>');
    }
  });
});

$(document).ready(function() {
  $.simpleWeather({
    location: 'Cape Town, South Africa',
    woeid: '',
    unit: 'f',
    success: function(weather2) {
      html = '<h2>'+weather2.temp+'&deg;'+weather2.units.temp+'</h2>';
      html += '<ul><li>'+weather2.city+', '+weather2.region+'</li>';

      html += '</ul>';
  
      $("#weather2").html(html);
    },
    error: function(error) {
      $("#weather2").html('<p>'+error+'</p>');
    }
  });
});

$(document).ready(function() {
  $.simpleWeather({
    location: 'Arusha, Tanzania',
    woeid: '',
    unit: 'f',
    success: function(weather3) {
      html = '<h2>'+weather3.temp+'&deg;'+weather3.units.temp+'</h2>';
      html += '<ul><li>'+weather3.city+', '+weather3.region+'</li>';

      html += '</ul>';
  
      $("#weather3").html(html);
    },
    error: function(error) {
      $("#weather3").html('<p>'+error+'</p>');
    }
  });
});

$(document).ready(function() {
  $.simpleWeather({
    location: 'Voi, Kenya',
    woeid: '',
    unit: 'f',
    success: function(weather4) {
      html = '<h2>'+weather4.temp+'&deg;'+weather4.units.temp+'</h2>';
      html += '<ul><li>'+weather4.city+', '+weather4.region+'</li>';

      html += '</ul>';
  
      $("#weather4").html(html);
    },
    error: function(error) {
      $("#weather4").html('<p>'+error+'</p>');
    }
  });
});
</script>


    <script type="text/javascript">
    jQuery(document).ready(function($){
jQuery('.main-header-search').after("<p style='text-align: center;color: #93a465;font-size: 16px;font-weight: bold;margin: 9px;'><i class='fa fa-phone'></i>&nbsp;1-855-2SAFARI (855-272-3274)</p>");
    });
    </script>
             
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php do_action('before_body_content')?>
<div class="global-wrap <?php echo apply_filters('st_container',true) ?>">
<div class="row">
    <header id="main-header">
        <div class="header-top">
        <div class="header-top" style="float: right; padding-right: 20px; font-size:12px";>

    <?php
        if ( is_user_logged_in() ) {
            echo '<a href="https://www.odysseysafaris.com/page-user-settings/">Upload Trip Documents</a>';
        } else {
            echo '<a href="https://www.odysseysafaris.com/page-login-normal/">Login to upload trip documents</a>';
        }
    ?>
        </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <a class="logo" href="<?php echo site_url()?>">
                            <img src="<?php echo st()->get_option('logo',get_template_directory_uri().'/img/logo-invert.png') ?>" alt="logo" title="<?php bloginfo('name')?>">
                        </a>
                    </div>
                                        
                    <?php get_template_part('users/user','nav');?>                
                </div>
            </div>
        </div>
        <div class="main_menu_wrap">
            <div class="container">
                <div class="nav">
                    <?php if(has_nav_menu('primary')){
                        wp_nav_menu(array('theme_location'=>'primary',
                                            "container"=>"",
                                            'items_wrap'      => '<ul id="slimmenu" class="%2$s slimmenu">%3$s</ul>',
                        ));
                    }
                    ?>
                </div>
            </div>
        </div><!-- End .main_menu_wrap-->
    </header>

