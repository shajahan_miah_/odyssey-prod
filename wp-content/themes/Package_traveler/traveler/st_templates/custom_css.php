<?php
/**
 * Created by PhpStorm.
 * User: me664
 * Date: 3/6/15
 * Time: 3:26 PM
 */

$main_color =st()->get_option('main_color');

if(isset($_GET['main_color']))
{
    $main_color.='#'.$_GET['main_color'];
}
if(isset($main_color_char))
{
    $main_color=$main_color_char;
}


$hex=st_hex2rgb($main_color);
$star_color=st()->get_option('star_color');

?>
.sort_icon .active{
color:<?php echo esc_attr($main_color)?>;
cursor: default;
}
.package-info-wrapper .icon-group i{
   color:<?php echo esc_attr($main_color)?>;
}
a,
a:hover,
.list-category > li > a:hover,
.pagination > li > a,
.top-user-area .top-user-area-list > li > a:hover,
.sidebar-widget.widget_archive ul> li > a:hover,
.sidebar-widget.widget_categories ul> li > a:hover,
.comment-form .add_rating,
.booking-item-reviews > li .booking-item-review-content .booking-item-review-expand span,
.booking-item-reviews > li .booking-item-rating-stars,
.booking-item-rating .booking-item-rating-stars,
.form-group.form-group-focus .input-icon.input-icon-hightlight,
.search_advance .expand_search_box span,
.booking-item-payment .booking-item-rating-stars .fa-star,
.booking-item-raiting-summary-list > li .booking-item-rating-stars
{
color:<?php echo esc_attr($main_color)?>
}
::selection {
background: <?php echo esc_attr($main_color)?>;
color: #fff;
}
.text-color,
.share ul li a:hover{
color:<?php echo esc_attr($main_color)?>!important;
}

header#main-header,
.btn-primary,
.post .post-header,
.top-user-area .top-user-area-list > li.top-user-area-avatar > a:hover > img,

.booking-item:hover, .booking-item.active,
.booking-item-dates-change,
.btn-group-select-num >.btn.active, .btn-group-select-num >.btn.active:hover,
.btn-primary:hover,
.booking-item-features > li:hover > i,
.form-control:active,
.form-control:focus,
.fotorama__thumb-border,
.sticky-wrapper.is-sticky .main_menu_wrap
{
border-color:<?php echo esc_attr($main_color)?>;
}



.pagination > li > a.current, .pagination > li > a.current:hover,
.btn-primary,
ul.slimmenu li.active > a, ul.slimmenu li:hover > a,
.nav-drop > .nav-drop-menu > li > a:hover,
.btn-group-select-num >.btn.active, .btn-group-select-num >.btn.active:hover,
.btn-primary:hover,
.pagination > li.active > a, .pagination > li.active > a:hover,
.box-icon, [class^="box-icon-"], [class*=" box-icon-"],
.booking-item-raiting-list > li > div.booking-item-raiting-list-bar > div, .booking-item-raiting-summary-list > li > div.booking-item-raiting-list-bar > div,
.irs-bar,
.nav-pills > li.active > a,
.search-tabs-bg > .tabbable > .nav-tabs > li.active > a,
.search-tabs-bg > .tabbable > .nav-tabs > li > a:hover > .fa,
.irs-slider,
.post .post-header .post-link,
.hover-img .hover-title, .hover-img [class^="hover-title-"], .hover-img [class*=" hover-title-"],
.post .post-header .post-link:hover,
#gotop:hover
{
background:<?php echo esc_attr($main_color)?>
}

.datepicker table tr td.active:hover, .datepicker table tr td.active:hover:hover, .datepicker table tr td.active.disabled:hover, .datepicker table tr td.active.disabled:hover:hover, .datepicker table tr td.active:focus, .datepicker table tr td.active:hover:focus, .datepicker table tr td.active.disabled:focus, .datepicker table tr td.active.disabled:hover:focus, .datepicker table tr td.active:active, .datepicker table tr td.active:hover:active, .datepicker table tr td.active.disabled:active, .datepicker table tr td.active.disabled:hover:active, .datepicker table tr td.active.active, .datepicker table tr td.active:hover.active, .datepicker table tr td.active.disabled.active, .datepicker table tr td.active.disabled:hover.active, .open .dropdown-toggle.datepicker table tr td.active, .open .dropdown-toggle.datepicker table tr td.active:hover, .open .dropdown-toggle.datepicker table tr td.active.disabled, .open .dropdown-toggle.datepicker table tr td.active.disabled:hover
{
background-color:<?php echo esc_attr($main_color)?>;

border-color: <?php echo esc_attr($main_color)?>;
}

.datepicker table tr td.today:before, .datepicker table tr td.today:hover:before, .datepicker table tr td.today.disabled:before, .datepicker table tr td.today.disabled:hover:before{
border-bottom-color: <?php echo esc_attr($main_color)?>;
}

.box-icon:hover, [class^="box-icon-"]:hover, [class*=" box-icon-"]:hover
{
background:rgba(<?php echo esc_attr($hex[0].','.$hex[1].','.$hex[2].',0.7') ?>);
}
.booking-item-reviews > li .booking-item-review-person-avatar:hover
{
-webkit-box-shadow: 0 0 0 2px <?php echo esc_attr($main_color)?>;
box-shadow: 0 0 0 2px <?php echo esc_attr($main_color)?>;
}
ul.slimmenu li.current-menu-item > a, ul.slimmenu li:hover > a,
.menu .current-menu-ancestor >a

{
background:<?php echo esc_attr($main_color)?>;
color:white;
}

.menu .current-menu-item > a
{
background:<?php echo esc_attr($main_color)?> !important;
color:white !important;
}


.i-check.checked, .i-radio.checked
{

border-color: <?php echo esc_attr($main_color)?>;
background: <?php echo esc_attr($main_color)?>;
}


.i-check.hover, .i-radio.hover
{
border-color: <?php echo esc_attr($main_color)?>;
}


.irs-diapason{

background: <?php echo esc_attr($main_color)?>;
}

<?php if($star_color):?>
    .booking-item-rating .fa ,
    .booking-item.booking-item-small .booking-item-rating-stars,
    .comment-form .add_rating,
    .booking-item-payment .booking-item-rating-stars .fa-star{
    color:<?php echo esc_attr($star_color)?>
    }
<?php endif;?>







/*
* Custom Color
*/

.box-icon-black {
background: #333;
}
.box-icon-black:hover {
background: #000;
}
.box-icon-gray {
background: #808080;
}
.box-icon-gray:hover {
background: #4d4d4d;
}
.box-icon-white {
background: #fff;
color: #ed8323;
}
.box-icon-white:hover {
color: #ed8323;
background: #e6e6e6;
}
.box-icon-info {
background: #2f96b4;
}
.box-icon-info:hover {
background: #267890;
}
.box-icon-success {
background: #51a351;
}
.box-icon-success:hover {
background: #418241;
}
.box-icon-warning {
background: #f89406;
}
.box-icon-warning:hover {
background: #c67605;
}
.box-icon-danger {
background: #bd362f;
}
.box-icon-danger:hover {
background: #972b26;
}
.box-icon-inverse {
background: #127cdc;
}
.box-icon-inverse:hover {
background: #0e63b0;
}
.box-icon-to-normal:hover {
background: #ed8323;
}
.box-icon-to-black:hover {
background: #333;
}
.box-icon-to-gray:hover {
background: #808080;
}
.box-icon-to-white:hover {
background: #fff;
color: #ed8323;
}
.box-icon-to-info:hover {
background: #2f96b4;
}
.box-icon-to-success:hover {
background: #51a351;
}
.box-icon-to-warning:hover {
background: #f89406;
}
.box-icon-to-danger:hover {
background: #bd362f;
}
.box-icon-to-inverse:hover {
background: #127cdc;
}
.box-icon-border,
[class^="box-icon-border"],
[class*=" box-icon-border"] {
background: none;
border: 1px solid #ed8323;
color: #ed8323;
}
.box-icon-border:hover,
[class^="box-icon-border"]:hover,
[class*=" box-icon-border"]:hover {
background: #ed8323;
color: #fff !important;
}
.box-icon-border.box-icon-black,
[class^="box-icon-border"].box-icon-black,
[class*=" box-icon-border"].box-icon-black,
.box-icon-border.box-icon-to-black:hover,
[class^="box-icon-border"].box-icon-to-black:hover,
[class*=" box-icon-border"].box-icon-to-black:hover {
border-color: #333;
color: #333;
}
.box-icon-border.box-icon-black:hover,
[class^="box-icon-border"].box-icon-black:hover,
[class*=" box-icon-border"].box-icon-black:hover,
.box-icon-border.box-icon-to-black:hover:hover,
[class^="box-icon-border"].box-icon-to-black:hover:hover,
[class*=" box-icon-border"].box-icon-to-black:hover:hover {
background: #333;
}
.box-icon-border.box-icon-gray,
[class^="box-icon-border"].box-icon-gray,
[class*=" box-icon-border"].box-icon-gray,
.box-icon-border.box-icon-to-gray:hover,
[class^="box-icon-border"].box-icon-to-gray:hover,
[class*=" box-icon-border"].box-icon-to-gray:hover {
border-color: #808080;
color: #808080;
}
.box-icon-border.box-icon-gray:hover,
[class^="box-icon-border"].box-icon-gray:hover,
[class*=" box-icon-border"].box-icon-gray:hover,
.box-icon-border.box-icon-to-gray:hover:hover,
[class^="box-icon-border"].box-icon-to-gray:hover:hover,
[class*=" box-icon-border"].box-icon-to-gray:hover:hover {
background: #808080;
}
.box-icon-border.box-icon-white,
[class^="box-icon-border"].box-icon-white,
[class*=" box-icon-border"].box-icon-white,
.box-icon-border.box-icon-to-white:hover,
[class^="box-icon-border"].box-icon-to-white:hover,
[class*=" box-icon-border"].box-icon-to-white:hover {
border-color: #fff;
color: #fff;
}
.box-icon-border.box-icon-white:hover,
[class^="box-icon-border"].box-icon-white:hover,
[class*=" box-icon-border"].box-icon-white:hover,
.box-icon-border.box-icon-to-white:hover:hover,
[class^="box-icon-border"].box-icon-to-white:hover:hover,
[class*=" box-icon-border"].box-icon-to-white:hover:hover {
color: #ed8323 !important;
background: #fff;
}
.box-icon-border.box-icon-info,
[class^="box-icon-border"].box-icon-info,
[class*=" box-icon-border"].box-icon-info,
.box-icon-border.box-icon-to-info:hover,
[class^="box-icon-border"].box-icon-to-info:hover,
[class*=" box-icon-border"].box-icon-to-info:hover {
border-color: #2f96b4;
color: #2f96b4;
}
.box-icon-border.box-icon-info:hover,
[class^="box-icon-border"].box-icon-info:hover,
[class*=" box-icon-border"].box-icon-info:hover,
.box-icon-border.box-icon-to-info:hover:hover,
[class^="box-icon-border"].box-icon-to-info:hover:hover,
[class*=" box-icon-border"].box-icon-to-info:hover:hover {
background: #2f96b4;
}
.box-icon-border.box-icon-success,
[class^="box-icon-border"].box-icon-success,
[class*=" box-icon-border"].box-icon-success,
.box-icon-border.box-icon-to-success:hover,
[class^="box-icon-border"].box-icon-to-success:hover,
[class*=" box-icon-border"].box-icon-to-success:hover {
border-color: #51a351;
color: #51a351;
}
.box-icon-border.box-icon-success:hover,
[class^="box-icon-border"].box-icon-success:hover,
[class*=" box-icon-border"].box-icon-success:hover,
.box-icon-border.box-icon-to-success:hover:hover,
[class^="box-icon-border"].box-icon-to-success:hover:hover,
[class*=" box-icon-border"].box-icon-to-success:hover:hover {
background: #51a351;
}
.box-icon-border.box-icon-warning,
[class^="box-icon-border"].box-icon-warning,
[class*=" box-icon-border"].box-icon-warning,
.box-icon-border.box-icon-to-warning:hover,
[class^="box-icon-border"].box-icon-to-warning:hover,
[class*=" box-icon-border"].box-icon-to-warning:hover {
border-color: #f89406;
color: #f89406;
}
.box-icon-border.box-icon-warning:hover,
[class^="box-icon-border"].box-icon-warning:hover,
[class*=" box-icon-border"].box-icon-warning:hover,
.box-icon-border.box-icon-to-warning:hover:hover,
[class^="box-icon-border"].box-icon-to-warning:hover:hover,
[class*=" box-icon-border"].box-icon-to-warning:hover:hover {
background: #f89406;
}
.box-icon-border.box-icon-danger,
[class^="box-icon-border"].box-icon-danger,
[class*=" box-icon-border"].box-icon-danger,
.box-icon-border.box-icon-to-danger:hover,
[class^="box-icon-border"].box-icon-to-danger:hover,
[class*=" box-icon-border"].box-icon-to-danger:hover {
border-color: #bd362f;
color: #bd362f;
}
.box-icon-border.box-icon-danger:hover,
[class^="box-icon-border"].box-icon-danger:hover,
[class*=" box-icon-border"].box-icon-danger:hover,
.box-icon-border.box-icon-to-danger:hover:hover,
[class^="box-icon-border"].box-icon-to-danger:hover:hover,
[class*=" box-icon-border"].box-icon-to-danger:hover:hover {
background: #bd362f;
}
.box-icon-border.box-icon-inverse,
[class^="box-icon-border"].box-icon-inverse,
[class*=" box-icon-border"].box-icon-inverse,
.box-icon-border.box-icon-to-inverse:hover,
[class^="box-icon-border"].box-icon-to-inverse:hover,
[class*=" box-icon-border"].box-icon-to-inverse:hover {
border-color: #127cdc;
color: #127cdc;
}
.box-icon-border.box-icon-inverse:hover,
[class^="box-icon-border"].box-icon-inverse:hover,
[class*=" box-icon-border"].box-icon-inverse:hover,
.box-icon-border.box-icon-to-inverse:hover:hover,
[class^="box-icon-border"].box-icon-to-inverse:hover:hover,
[class*=" box-icon-border"].box-icon-to-inverse:hover:hover {
background: #127cdc;
}
.box-icon-border.box-icon-to-normal:hover,
[class^="box-icon-border"].box-icon-to-normal:hover,
[class*=" box-icon-border"].box-icon-to-normal:hover {
border-color: #ed8323;
background: #ed8323;
}

<?php $color_featured = st()->get_option('st_text_featured_color');
      $bg_featured = st()->get_option('st_text_featured_bg');
?>
.st_featured{
 color: <?php echo esc_attr($color_featured) ?>;
 background: <?php echo esc_attr($bg_featured) ?>;
}

.st_featured::before {
   border-color: <?php echo esc_attr($bg_featured) ?> <?php echo esc_attr($bg_featured) ?> transparent transparent;
}
.st_featured::after {
    border-color: <?php echo esc_attr($bg_featured) ?> transparent <?php echo esc_attr($bg_featured) ?> <?php echo esc_attr($bg_featured) ?>;
}
.featured_single .st_featured::before{
   border-color: transparent <?php echo esc_attr($bg_featured) ?> transparent transparent;
}



.item-nearby .st_featured{
   padding: 0 13px 0 0;
}
.item-nearby .st_featured::before {
    right: 0px;
    left: auto;
    border-color: transparent transparent <?php echo esc_attr($bg_featured) ?> <?php echo esc_attr($bg_featured) ?>;
    top: -10px;
}
.item-nearby .st_featured::after {
   left: -28px;
   right:auto;
   border-width:14px;
   border-color: <?php echo esc_attr($bg_featured) ?> <?php echo esc_attr($bg_featured) ?> <?php echo esc_attr($bg_featured) ?> transparent  ;
}
.item-nearby .st_featured{
  font: bold 14px/28px Cambria,Georgia,Times,serif;
}

<?php  if(st()->get_option('right_to_left') == 'on' ){ ?>
    .st_featured{
       padding: 0 13px 0 3px;
    }
    .st_featured::before {
        border-color: <?php echo esc_attr($bg_featured) ?> transparent transparent <?php echo esc_attr($bg_featured) ?>;
    }
    .st_featured::after {
        border-color: <?php echo esc_attr($bg_featured) ?> <?php echo esc_attr($bg_featured) ?> <?php echo esc_attr($bg_featured) ?> transparent;
    }
    .featured_single .st_featured::before {
        border-color: transparent transparent transparent <?php echo esc_attr($bg_featured) ?> ;
        right: -34px;
    }
    .item-nearby  .st_featured::before {
    border-color: <?php echo esc_attr($bg_featured) ?> transparent transparent <?php echo esc_attr($bg_featured) ?>;
    }

    .item-nearby .st_featured {
        bottom: 10px;
        left: -10px;
        right: auto;
        top: auto;
         padding: 0  0 0 13px;
    }
    .item-nearby  .st_featured::before {
        left: 0;
        right:auto;
    }
    .item-nearby .st_featured::before {
          border-color: <?php echo esc_attr($bg_featured) ?> <?php echo esc_attr($bg_featured) ?> transparent transparent;
    }
    .item-nearby .st_featured::after {
        border-color: <?php echo esc_attr($bg_featured) ?>  transparent <?php echo esc_attr($bg_featured) ?> <?php echo esc_attr($bg_featured) ?>;
        border-width: 14px;
        right: -27px;
    }
    .featured_single {
        padding-left: 70px;
        padding-right: 0px;
    }
<?php } ?>
