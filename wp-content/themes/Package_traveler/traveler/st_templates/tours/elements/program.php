<?php
/**
 * @package WordPress
 * @subpackage Traveler
 * @since 1.0
 *
 * Tours program
 *
 * Created by ShineTheme
 *
 */
$list_day  = get_post_meta(get_the_ID() , 'tours_program',true);
if(!empty($list_day)):
    ?>

<div id="accordion_tours" class="panel-group">
  <?php
        $i=0;
        foreach($list_day as $k=>$v):
            $id=rand();
            ?>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title"> <a href="#collapse-<?php echo esc_html($id) ?>" data-parent="#accordion_tours" data-toggle="collapse" class=""> <?php echo balanceTags($v['title']) ?> </a> </h4>
    </div>
    <div id="collapse-<?php echo esc_html($id) ?>" class="panel-collapse collapse <?php if($i==0)echo 'in'; ?>">
      <div class="panel-body"> <?php echo balanceTags($v['desc']) ?> </div>
    </div>
  </div>
  <?php $i++; endforeach; ?>
  <?php  $value = get_field( "accommodation_video",get_the_ID()); ?>
  <div class="wpb_text_column wpb_content_element " style="margin-top:15px">
    <div class="wpb_wrapper">
      <h3 id="booknow">Accommodation</h3>
    </div>
  </div>
  <div class="panel-group" id="accordion_tours">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title"> <a class="collapsed" data-toggle="collapse" data-parent="#accordion_tours" href="#collapse-<?php echo get_the_ID(); ?>">
          <h4><?php echo get_field( "video_title",get_the_ID()); ?></h4>
          </a> </h4>
      </div>
      <div class="panel-collapse collapse" id="collapse-<?php echo get_the_ID(); ?>" style="height: 0px;">
        <div class="panel-body">
          <p><?php echo get_field( "video_text",get_the_ID()); ?></p>
          <p>
            <?php  if(!empty($value)){
$videoso=str_replace("https://www.youtube.com/watch?v=", "", $value); ?>
            <br/>
            <iframe title="YouTube video player" class="youtube-player" type="text/html" 
width="320"  height="220" src="https://youtube.com/embed/<?php echo  $videoso; ?>" frameborder="0" allowFullScreen></iframe>
            <?php }else{} ?>
          </p>
        </div>
      </div>
    </div>
  </div>
  <!--<div class="map" style="display:none"><?php echo do_shortcode( '[travelroute height="400" width="727"  maxzoom="20" showmarker="true"]'); ?></div>
-->
  <style>
.side_bar:hover { border: 2px solid !important;border-block-end: 8px solid rgb(94, 30, 3) !important;box-shadow: 0 2px 1px rgba(0,0,0,0.2) !important;color:#5e1e03 !important;}
</style>
  <div class="map" style="display:none; width:727px"> <?php echo get_field( "map",get_the_ID()); ?> </div>
  <div class="side" style="display:none;">
    <?php  $whates_include=get_field("whats_included_",get_the_ID());
//print_r($whates_include);die;?>
    <h3 style="font-size: 27px;">What's Included</h3>
    <div class="side_bar" style="border: 2px solid;color: rgb(230, 230, 230);border-radius: 10px;margin: 6px 0px 12px;border-block-end: 8px solid rgb(218, 215, 215);overflow: hidden;">
      <p style="text-align: justify;display: inline-block;vertical-align: top;padding: 10px;color: rgb(71, 67, 67); font-size:12.9px">
        <?php  
$what_value = get_field("what",get_the_ID());
if($what_value ==""){ ?>
        <?php $my_postid = 6041;//This is page id or post id
$content_post = get_post($my_postid);
echo get_field("whats_included",$my_postid); 
}else if($whates_include[0]=='Yes') {
echo $what_value;
}else if($whates_include[0]=='' && $what_value !=""){
$my_postid = 6041;//This is page id or post id
$content_post = get_post($my_postid);
echo get_field("whats_included",$my_postid); 
}
 ?>
      </p>
    </div>
    <?php  $click_show_whats_not_included=get_field( "click_show_whats_not_included",get_the_ID());
//if($click_show_whats_not_included[0]=='Yes'){ ?>
    <h3 style="font-size: 27px;">What's Not Included</h3>
    <div class="side_bar" style="border: 2px solid;color: rgb(230, 230, 230);border-radius: 10px;margin: 6px 0px 12px;border-block-end: 8px solid rgb(218, 215, 215);overflow: hidden;">
      <p style="text-align: justify;display: inline-block;vertical-align: top;padding: 10px;color: rgb(71, 67, 67);font-size:12.9px">
        <?php $whats__includedvalue = get_field("whats__included",get_the_ID());
if($whats__includedvalue ==""){ ?>
        <?php $my_postid = 6041;//This is page id or post id
$content_post = get_post($my_postid);
echo get_field("whats_not_included",$my_postid); 
}else if($click_show_whats_not_included[0]=='Yes'){
echo $whats__includedvalue;
}else if($click_show_whats_not_included[0]=='' && $whats__includedvalue !=''){
$my_postid = 6041;//This is page id or post id
$content_post = get_post($my_postid);
echo get_field("whats_not_included",$my_postid); 
} ?>
      </p>
    </div>
    <?php //} ?>
    <?php  $summary_of_wildlife=get_field("click_summary_of_wildlife",get_the_ID());
//if($summary_of_wildlife[0]=='Yes'){ ?>
    <h3 style="font-size: 27px;">Summary of Wildlife</h3>
    <div class="side_bar" style="border: 2px solid;color: rgb(230, 230, 230);border-radius: 10px;margin: 6px 0px 12px;border-block-end: 8px solid rgb(218, 215, 215);overflow: hidden;">
      <p style="text-align: justify;display: inline-block;vertical-align: top;padding: 10px;color: rgb(71, 67, 67);font-size:12.9px">
        <?php $summary_of_wildlifevalue = get_field("summary_of_wildlife",get_the_ID());
if($summary_of_wildlifevalue ==""){ ?>
        <?php $my_postid = 6041;//This is page id or post id
$content_post = get_post($my_postid);
echo get_field("summary_of_wildlife",$my_postid); 
}else if($summary_of_wildlife[0]=='Yes') {
echo $summary_of_wildlifevalue;
}else if($summary_of_wildlife[0]=='' && $summary_of_wildlifevalue !=''){
$my_postid = 6041;//This is page id or post id
$content_post = get_post($my_postid);
echo get_field("summary_of_wildlife",$my_postid); 
} ?>
      </p>
    </div>
    <?php //} ?>
  </div>
</div>
<?php endif; ?>
<div id="last_minute" style="display:none">
<ul class="booking-list">
<?php
$args = array( 'post_type' => 'st_tours', 'posts_per_page' => 4);
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); 
$image_id = get_post_thumbnail_id();
$image_url = wp_get_attachment_image_src($image_id, 'large', true);
$thumburl = $image_url[0];
$Url=get_the_permalink();
$last_id=get_the_ID();
$last_minute_deal = get_field('last_minute',get_the_ID());
if($last_minute_deal[0]=='Yes') {
     ?>

  <li class="item-nearby post-1029 st_tours type-st_tours status-publish has-post-thumbnail hentry st_tour_type-adventure st_tour_type-family st_tour_type-luxury st_tour_type-private">
    <div class="booking-item booking-item-small">
      <div class="row">
        <div class="col-xs-4"> <a href="<?php echo $Url; ?>"> <img width="960" height="300" src="<?php echo $thumburl ?>" class="attachment-post-thumbnail wp-post-image" alt="<?php echo the_title(); ?>"> </a> </div>
        <div class="col-xs-4">
          <h5 class="booking-item-title"><a href="<?php echo $Url; ?>"><?php echo the_title(); ?></a> </h5>
          <ul class="icon-group booking-item-rating-stars">
            <li><i class="fa fa-star-o"></i></li>
            <li><i class="fa fa-star-o"></i></li>
            <li><i class="fa fa-star-o"></i></li>
            <li><i class="fa fa-star-o"></i></li>
            <li><i class="fa fa-star-o"></i></li>
          </ul>
        </div>
        <div class="col-xs-4"><span class="booking-item-price-from">from</span> <span class="booking-item-price"> <?php echo STTour::get_price_html(); ?> </span> </div>
      </div>
    </div>
  </li>

<?php 
}     
endwhile;

 ?>
 </ul>
 </div>
<script>
jQuery(document).ready(function($){
var html = jQuery('.map').html();
var side = jQuery('.side').html();
var last_minute = jQuery('#last_minute').html();
//alert(html);
jQuery('.list_tours').hide();
jQuery('.list_tours').after(last_minute);
jQuery('.st_google_map').hide();
jQuery('.st_google_map').after(html);
jQuery('.vc_custom_1420002423206').after(side);
jQuery(".widget_calendar h4").html("Calendar");
  });
</script>
