<?php
/**
 * @package WordPress
 * @subpackage Traveler
 * @since 1.0
 *
 * Tours field address
 *
 * Created by ShineTheme
 *
 */
$default=array(
    'title'=>''
);

if(isset($data)){
    extract(wp_parse_args($data,$default));
}else{
    extract($default);
}
$old_location=STInput::get('location_id');
?>
<div class="form-group form-group-lg form-group-icon-left">
    <i class="fa fa-map-marker input-icon input-icon-highlight"></i>
    <label><?php echo esc_html($title)?></label>
    <select name="s" class="form-control" placeholder="<?php st_the_language('tours_or_us_zip_Code')?>">
     <option value="kenya">Kenya</option>
     <option value="tanzania">Tanzania</option>
     <option value="kenya tanzania">Kenya & Tanzania</option>
     <option value="south africa">South Africa</option>
     <option value="zimbabwe">Zimbabwe</option>
    </select> 
    <!-- <input name="s" autocomplete="on" class="typeahead_location form-control" placeholder="<?php st_the_language('tours_or_us_zip_Code')?>" type="text" /> -->
    <input type="hidden" name="location_id" value="<?php echo STInput::get('location_id') ?>" --> 
</div>