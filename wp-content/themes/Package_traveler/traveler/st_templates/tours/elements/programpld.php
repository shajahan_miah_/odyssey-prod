<?php
/**
 * @package WordPress
 * @subpackage Traveler
 * @since 1.0
 *
 * Tours program
 *
 * Created by ShineTheme
 *
 */
$list_day  = get_post_meta(get_the_ID() , 'tours_program',true);
if(!empty($list_day)):
    ?>
    <div id="accordion_tours" class="panel-group">
        <?php
        $i=0;
        foreach($list_day as $k=>$v):
            $id=rand();
            ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a href="#collapse-<?php echo esc_html($id) ?>" data-parent="#accordion_tours" data-toggle="collapse" class="">
                            <?php echo balanceTags($v['title']) ?>
                        </a>
                    </h4>
                </div>
                <div id="collapse-<?php echo esc_html($id) ?>" class="panel-collapse collapse <?php if($i==0)echo 'in'; ?>">
                    <div class="panel-body">
                        <?php echo balanceTags($v['desc']) ?>
                    </div>
                </div>
            </div>
            <?php $i++; endforeach; ?>
           <div class="wpb_text_column wpb_content_element " style="margin-top:15px">
		<div class="wpb_wrapper">
			<h3 id="booknow">Accommodation</h3>

		</div> 
	</div> 
  <?php $tourid=get_the_ID(); 
  $title=get_the_title();   
  $video=mysql_query("select slug from wp_terms where name like '%".$title."%'");
  $cat=mysql_fetch_assoc($video);
  $catname=$cat['slug'];
 ?>    
      <?php
$my_query = new WP_Query('category_name='.$catname.'&showposts=6');
while ($my_query->have_posts()) : $my_query->the_post();
?>
  <?php  $post_id=get_the_id();?>
<div class="panel-group" id="accordion_tours">
                    <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion_tours" href="#collapse-<?php echo $post_id?>">
                           <h4><?php  the_title(); ?> </h4>                        </a>
                    </h4>
                </div>
                <div class="panel-collapse collapse" id="collapse-<?php echo $post_id?>" style="height: 0px;">
                    <div class="panel-body">
                        <p><?php the_content(); ?></p> 
                        <p>
                         <?php $query=mysql_query("select meta_value from wp_postmeta where meta_key='media' and post_id='$post_id'");
$video=mysql_fetch_assoc($query); 
$link = $video['meta_value']; 
 if(!empty($link)){
$videoso=str_replace("https://www.youtube.com/watch?v=", "", $link); ?>
<br/>
<iframe title="YouTube video player" class="youtube-player" type="text/html" 
width="320"  height="220" src="https://youtube.com/embed/<?php echo  $videoso; ?>" frameborder="0" allowFullScreen></iframe>
<?php }else{} ?>
</p></div>
                </div>
            </div>
   </div>
   <?php endwhile; ?>
 
  
  
   <!--<div class="map" style="display:none"><?php echo do_shortcode( '[travelroute height="400" width="727"  maxzoom="20" showmarker="true"]'); ?></div>
-->
            <?php if($tourid == '5158'){ ?>  
			
			<?php query_posts('post_type=kenya-safari-map&posts_per_page=1'); ?>
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="map" style="display:none"> <?php the_content(); ?>  </div>     
 <?php endwhile; 
 endif;
wp_reset_query();?>
  <?php } else if($tourid == '1031'){  ?>
  <?php query_posts('post_type=tanzania-safari-map&posts_per_page=1'); ?>
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
      <div class="map" style="display:none ; width:727px"> <?php the_content(); ?>  </div>         
 <?php endwhile; 
 endif;
wp_reset_query();?>

 <?php }else if($tourid == '1030'){  ?>
  <?php query_posts('post_type=south-africa-map&posts_per_page=1'); ?>
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="map" style="display:none;width:727px"> <?php the_content(); ?>  </div>  
 <?php endwhile; 
 endif;
wp_reset_query();?>
<?php  }else if($tourid == '1029'){  ?>
  <?php query_posts('post_type=elephant-safari-map&posts_per_page=1'); ?>
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <div class="map" style="display:none; width:727px"> <?php the_content(); ?>  </div>  
 <?php endwhile; 
 endif;
wp_reset_query();?>
<?php }else{} ?>
    </div>
<?php endif; ?>
<script>
jQuery(document).ready(function($){
var html = jQuery('.map').html();
//alert(html);
jQuery('.st_google_map').hide();
jQuery('.st_google_map').after(html);

  });
</script>