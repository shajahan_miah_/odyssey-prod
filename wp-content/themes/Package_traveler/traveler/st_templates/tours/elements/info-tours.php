<?php
/**
 * @package WordPress
 * @subpackage Traveler
 * @since 1.0
 *
 * Tours info
 *
 * Created by ShineTheme
 *
 */
$type_price = get_post_meta(get_the_ID(),'type_price',true);
$type_tour = get_post_meta(get_the_ID(),'type_tour',true);

if($type_price=='people_price')
{
    $info_price=STTour::get_price_person(get_the_ID());
}else{
    $info_price = STTour::get_info_price(get_the_ID());

}
	$tourid=get_the_ID();
?>

<style> a{ text-decoration:none !important;}
 hover{ text-decoration:none !important;}
 p {
    padding: 0;
    margin: 0;
    display: inline;
}
 </style>
<div class="package-info-wrapper pull-left" style="width: 100%">
    <div class="row">
        <div class="col-md-6">
        <div class="package-info">
                <i class="fa fa-location-arrow"></i>
                <span class="head "><?php st_the_language('tour_location') ?>: </span>
              <?php echo get_the_title(get_post_meta(get_the_ID() , 'id_location', true)); ?>
            </div>
<?php $tourtype = get_field( "tour_type",get_the_ID());
if(!empty($tourtype)){ ?>
        <div class="package-info">
                <i class="fa fa-certificate"></i>
            <span class="head">Tour Type: </span><?php echo $tourtype ; ?>
                         </div>
<?php } else {}?>
            <?php if($type_tour == 'specific_date'){ ?>
                <div class="package-info">
                    <i class="fa fa-calendar"></i>
                    <span class="head"><?php st_the_language('tour_duration') ?>: </span>
                    <?php
                    $check_in = get_post_meta(get_the_ID() , 'check_in' ,true);
                    $check_out = get_post_meta(get_the_ID() , 'check_out' ,true);
                    $date = mysql2date('m/d/Y',$check_in).' <i class="fa fa-long-arrow-right"></i> '.mysql2date('m/d/Y',$check_out);
                    if(!empty($check_in) and !empty($check_out)){
                        echo balanceTags($date);
                    }else{
                        st_the_language('tour_none');
                    }
                    ?>
                </div>
            <?php }else{ ?>
                <div class="package-info">
                    <i class="fa fa-calendar"></i>
                    <span class="head"><?php _e('Duration',ST_TEXTDOMAIN) ?>: </span>
                    <?php $duration_day =  get_post_meta(get_the_ID() , 'duration_day', true); ?>
                    <?php if($duration_day>1){
                        printf(__('%d days',ST_TEXTDOMAIN),$duration_day);
                    }elseif($duration_day==1){
                        _e('1 day',ST_TEXTDOMAIN);
                    } ?>
                </div>
            <?php } ?>
            
            
             <?php  $dep = get_field( "departure_dates",get_the_ID());
			 if(!empty($dep)){ ?>  

               
    <div class="package-info">
                <i class="fa fa-certificate"></i>
               
            <span class="head certi">DEPARTURE DATES: </span><?php echo $dep ;?>
          
          </div>
          <?php  } else{}?>   
                       <?php  $airfire = get_field( "international_airfare",get_the_ID());
					   if(!empty($airfire)){ ?>  

               
    <div class="package-info">
                <i class="fa fa-certificate"></i>
               
            <span class="head certi">INTERNATIONAL AIRFARE: </span><?php echo  $airfire ; ?>
          
          </div>
          <?php  } else{}?>     
  
            
            
            
            
            
            <div class="package-info">
                <?php $max_people = get_post_meta(get_the_ID(),'max_people', true) ?>
                <i class="fa    fa-users"></i>
                <span class="head"><?php st_the_language('tour_max_people') ?>: </span>
                <?php echo esc_html($max_people) ?>
            </div>

                        <div class="package-info">
                <i class="fa fa-money"></i>
                <span class="head"><?php st_the_language('tour_price') ?>: </span>
                <?php echo STTour::get_price_html(); ?>

             <?php
	     //Seasonal Price 
             $connection = mysql_connect("localhost","root","qwer12#$");
             mysql_set_charset('utf8',$connection);

	     //Select odyssey_prod db
	     mysql_select_db("odyssey_prod", $connection);

	     //Grab post_content 
	     $query=mysql_query("select post_content from wp_posts where ID ='$tourid'");
	     $row=mysql_fetch_assoc($query); 

	     //Seasonal price
 	     $seasonal_price = $row['post_content'];
	     
	     //Split individual lines into an array
	     $lines = preg_split("/[\r\n]+/", strip_tags($seasonal_price), -1, PREG_SPLIT_NO_EMPTY);	     	  
		  
             //Define seasons
	     $season = array("January", "Jan", "February", "Feb", "March", "Mar", "April", "Apr", "May", "June", "Jun", "July", "Jul", "August", "Aug", "September", "Sep", "October", "Oct", "November", "Nov", "December", "Dec");	     

	     echo "<br>";
	     foreach ($lines as $key => $value) {

    	     	     $search_string = explode(" ", $value);    
		     $search_string = trim($search_string[0]);

    		     if (in_array($search_string, $season)) {
        	         echo '<i class="fa fa-money"></i>&nbsp;<span class="head" style="font-weight:bold;">';echo$value;echo'</span>';
    		     } else {
        	         echo '<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';echo$value;echo'</span>';
    		     }
		     echo "<br>";
	    }

        ?>
        </div>
	<div style="width: 110%;margin-left: -11px;"></div>
                       
            
            
        </div>
        <div class="col-md-6">
            <div class="package-book-now-button">
                <form method="get" action="<?php echo home_url('/')?>">
                    <input type="hidden" name="action" value="tours_add_to_cart" >
                    <input type="hidden" name="item_id" value="<?php echo get_the_ID()?>">
                    <input type="hidden" name="discount" value="<?php echo esc_html($info_price['discount']) ?>">
                    <input name="price" value="<?php echo get_post_meta(get_the_ID(),'price',true) ?>" type="hidden">
                    <input type="hidden" name="type_tour" value="<?php echo esc_html($type_tour) ?>">
                    <input type="hidden" name="type_price" value="<?php echo esc_html($type_price) ?>">
                    <div class="div_book">
                        <div class="div_book_tour">
                            <?php if($type_price == 'people_price'){?>

                                <span><?php _e('Adults',ST_TEXTDOMAIN)?>: </span>
                                <select class="form-control st_tour_adult" name="adult_number" required>
                                    <?php for($i=1;$i<=20;$i++){
                                        echo  "<option value='{$i}'>{$i}</option>";
                                    } ?>
                                </select>
                                <span><?php _e('Children',ST_TEXTDOMAIN)?>: </span>
                                <select class="form-control st_tour_children" name="children_number" required>
                                    <?php for($i=0;$i<=20;$i++){
                                        echo  "<option value='{$i}'>{$i}</option>";
                                    } ?>
                                </select>
                            <?php }else{ ?>
                                <span><?php _e('Number Tours',ST_TEXTDOMAIN)?>: </span>
                                <select class="form-control" name="number" required>
                                    <?php for($i=1;$i<=20;$i++){
                                        echo  "<option value='{$i}'>{$i}</option>";
                                    } ?>
                                </select>
                            <?php } ?>
                            <?php if($type_tour == 'daily_tour'){ ?>
                                <input name="duration" class="" type="hidden" value="<?php echo esc_attr($duration_day) ?>">
                                <span><?php _e('Travel Date',ST_TEXTDOMAIN)?>: </span>
                              <input name="check_in" style="display:none" data-date-format="<?php echo TravelHelper::get_js_date_format()?>"   type="text" value="<?php echo get_field("travel_date",get_the_ID()); ?>" class="tour_book_date form-control" required /> 
                              <input name="" type="text" value="<?php echo get_field("travel_date",get_the_ID()); ?>" class="form-control"/> 
                            <?php }else{ ?>
                                <input name="check_in"  type="hidden"  value="<?php echo get_post_meta(get_the_ID(),'check_in',true) ?>">
                                <input name="check_out" type="hidden"  value="<?php echo get_post_meta(get_the_ID(),'check_out',true) ?>">
                            <?php } ?>
                        </div>
                        <div class="div_btn_book_tour">
                            <?php if(st()->get_option('booking_modal','off')=='on'){ ?>
                                <a href="#tour_booking_<?php the_ID() ?>" class="btn btn-primary popup-text" data-effect="mfp-zoom-out" ><?php st_the_language('book_now') ?></a>
                            <?php }else{ ?>
                                <button class="btn btn-primary" type="submit"><?php st_the_language('book_now')?></button>
                            <?php } ?>
                            <?php echo st()->load_template('user/html/html_add_wishlist',null,array("title"=>'','class'=>'')) ?>
			      <?php $current_page = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>
                              <div id="show"><a href="http://pdfmyurl.com/?url=<?=$current_page?>">Generate Tour PDF</a></div>
			      <?
			      
			      if ($_REQUEST['TourPDF'] == "TRUE"){?>
			      <?
				  //system('killall wkhtmltopdf', $retval);
				 // echo $retval;
		                  //echo "|";
                                  //system('rm -Rf /var/www/html/odyssey_prod/tour.pdf', $retval);
                                  //echo $retval;
				  //echo "|";
			          $output = shell_exec('sudo /usr/local/bin/wkhtmltopdf https://www.odysseysafaris.com/tours/elephant-experience-safari/?TourPDF=TRUE /var/www/html/odyssey_prod/tour.pdf');
                                  //echo $output;
				  //system("/usr/local/bin/wkhtmltopdf.sh $current_page /var/www/html/odyssey_prod/tour.pdf");
				  
                                  //system("/usr/local/bin/wkhtmltopdf.sh $current_page /var/www/html/odyssey_prod/tour.pdf", $retval);
				  //echo $return_var;
				      //$file = "/var/www/html/odyssey_prod/tour.pdf";
				      
				      //echo "<div><a href='http://www.odysseysafaris.com/tour.pdf' target='_new'>Download the PDF</a></div>";?>

				      <script type="text/js">
				      $(document).ready(function() {
					  $('show').hide();
					});
				      </script>

			      <?
				      /*
			 	      $pdf = file_get_contents($file);                                  
				          header('Content-Type: application/pdf');
				          header('Cache-Control: public, must-revalidate, max-age=0'); // HTTP/1.1
				          header('Pragma: public');
				          header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
				          header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
				          header('Content-Length: '.strlen($pdf));
				          header('Content-Disposition: inline; filename="'.basename($file).'";');
				          ob_clean(); 
				          flush(); 
				          echo $pdf;
				          exit()				  
				      

				      $fp = fopen($file, "r") ;

				      header("Cache-Control: maxage=1");
				      header("Pragma: public");
				      header("Content-type: application/pdf");
				      header("Content-Disposition: inline; filename=".$file."");
				      header("Content-Description: PHP Generated Data");
                                      header('Accept-Ranges: bytes');
				      header("Content-Transfer-Encoding: binary");
				      header('Content-Length:' . filesize($file));
				      ob_clean();
				      flush();
				      while (!feof($fp)) {
					$buff = fread($fp, 1024);
					print $buff;
				      }
				      exit;
				      */
                                  ?>
				
			     <?}?>
                        </div>
                    </div>
                </form>
                
            </div>
           <div class="package-info pull-left" style="margin-top: 83px;
padding-left: 16px;">
<script type="text/javascript">// <![CDATA[
(function(){var sb=document.createElement('script');sb.type='text/javascript';sb.async=true;sb.src='https://s3.amazonaws.com/z_437er23a/895898.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(sb,s);})();
// ]]></script>
<div style="display: inline-block; float: left; padding: 0 10px 5px 0;"><span class="1fd0285910"> </span></div>
                <!--<div class="pull-left">
                    <i class="fa fa-info"></i>
                    <span class="head"><?php st_the_language('tour_rate') ?>:</span>
                </div>
                <div  class="pull-left pl-5">
                    <ul class="icon-group booking-item-rating-stars">
                        <?php
                        $avg = STReview::get_avg_rate();
                        echo TravelHelper::rate_to_string($avg);
                        ?>
                    </ul>
                </div>-->
            </div> 
        </div>


    </div>
           
            <?php if(!empty($info_price['discount'])){ ?>
        <span class="box_sale sale_small btn-primary"> <?php echo esc_html($info_price['discount']) ?>% </span>
    <?php } ?>

</div>

<?php
if(st()->get_option('booking_modal','off')=='on'){?>
    <div class="mfp-with-anim mfp-dialog mfp-search-dialog mfp-hide" id="tour_booking_<?php the_ID()?>">
        <?php echo st()->load_template('tours/modal_booking');?>
    </div>

<?php }?>

