<?php
/**
 * @package WordPress
 * @subpackage Traveler
 * @since 1.0
 *
 * form new login
 *
 * Created by ShineTheme
 *
 */
if(isset($_REQUEST['btn-reg'])){
    STUser_f::registration_user();
}
$class_form = "";
if(is_page_template('template-login.php')){
    $class_form = 'form-group-ghost';
}
?>

<form  method="post" action="<?php echo TravelHelper::build_url('url',STInput::get('url')) ?>">
    <div class="form-group <?php echo esc_attr($class_form); ?> form-group-icon-left"><i class="fa fa-user input-icon input-icon-show"></i>
        <label><?php st_the_language('full_name') ?></label>
        <input name="full_name" class="form-control" placeholder="e.g. John Doe" type="text" />
    </div>
    <div class="form-group <?php echo esc_attr($class_form); ?> form-group-icon-left"><i class="fa fa-envelope input-icon input-icon-show"></i>
        <label><?php st_the_language('email') ?></label>
        <input name="email" class="form-control" placeholder="e.g. johndoe@gmail.com" type="text" />
    </div>
    <div class="form-group <?php echo esc_attr($class_form); ?> form-group-icon-left"><i class="fa fa-lock input-icon input-icon-show"></i>
        <label><?php st_the_language('password') ?></label>
        <input name="password" class="form-control" type="password" placeholder="<?php _e('my secret password',ST_TEXTDOMAIN)?>" />
    </div>
    <input name="btn-reg" class="btn btn-primary" type="submit" value="<?php st_the_language('sign_up') ?>" />
</form>

