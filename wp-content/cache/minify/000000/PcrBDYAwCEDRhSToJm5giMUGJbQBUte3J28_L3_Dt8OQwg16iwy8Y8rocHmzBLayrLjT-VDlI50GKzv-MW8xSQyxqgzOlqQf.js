
/* wpvp-front-end.js */

/* 1   */ upload_size = wpvp_vars.upload_size;
/* 2   */ file_upload_limit = wpvp_vars.file_upload_limit;
/* 3   */ wpvp_ajax = wpvp_vars.wpvp_ajax;
/* 4   */ var files;
/* 5   */ jQuery(document).ready(function(){
/* 6   */ 	jQuery('.wpvp-submit').on('click',wpvp_uploadFiles);
/* 7   */ 	jQuery('input#async-upload').on('change', wpvp_prepareUpload);
/* 8   */ 	jQuery('.video-js').each(function(){
/* 9   */ 		var objId = jQuery(this).attr('id');
/* 10  */ 		var vol = jQuery(this).data('audio');
/* 11  */ 		if(objId !== 'undefined' && (vol < 100 && vol !== 'undefined')){
/* 12  */ 			var player = videojs(objId);
/* 13  */ 			player.ready(function(){
/* 14  */ 				vol = parseFloat("0."+vol);
/* 15  */ 				var playerObj = this;
/* 16  */ 			  	playerObj.volume(vol);
/* 17  */ 			});
/* 18  */ 		}
/* 19  */ 	});
/* 20  */ });
/* 21  */ // Grab the files and set them to our variable
/* 22  */ function wpvp_prepareUpload(event){
/* 23  */ 	if(typeof event==='undefined')
/* 24  */ 		var event = e;
/* 25  */ 	files = event.target.files;
/* 26  */ }
/* 27  */ // Catch the form submit and upload the files
/* 28  */ function wpvp_uploadFiles(event){
/* 29  */ 	if(typeof event==='undefined')
/* 30  */ 		var event = e;
/* 31  */ 	event.stopPropagation();
/* 32  */ 	event.preventDefault();
/* 33  */ 	var action = jQuery(this).attr('action');
/* 34  */ 	var deferred_validation = jQuery.Deferred();
/* 35  */ 	error = false;
/* 36  */ 	var form = jQuery('form.wpvp-processing-form');
/* 37  */ 	var formData = form.serialize();
/* 38  */ 	form.find('.wpvp_require').each(function(){
/* 39  */ 		if(!jQuery(this).val()){
/* 40  */ 			error = true;
/* 41  */ 			jQuery(this).next('.wpvp_error').html('This field is required.');
/* 42  */ 		} else {
/* 43  */ 			jQuery(this).next('.wpvp_error').html('');
/* 44  */ 		}
/* 45  */ 	}).promise().done(function(){
/* 46  */ 		deferred_validation.resolve();
/* 47  */ 	});
/* 48  */ 	deferred_validation.done(function(){
/* 49  */ 		if(error)
/* 50  */ 			return false;

/* wpvp-front-end.js */

/* 51  */ 		if(action=='update'){
/* 52  */ 			var data = {
/* 53  */ 				action: 'wpvp_process_update',
/* 54  */ 				'cookie': encodeURIComponent(document.cookie),
/* 55  */ 				formData: formData
/* 56  */ 			};
/* 57  */ 			jQuery.post(wpvp_ajax,data,function(response){
/* 58  */ 				var obj = JSON.parse(response);
/* 59  */ 				var status = '';
/* 60  */ 				if(obj.hasOwnProperty('status'))
/* 61  */ 					status = obj.status;
/* 62  */ 				var msg = [];
/* 63  */ 				if(obj.hasOwnProperty('msg'))
/* 64  */ 					msg = obj.msg;
/* 65  */ 				if(msg instanceof Array){
/* 66  */ 					var msgBlock = jQuery('.wpvp_msg');
/* 67  */ 					msgBlock.html('');
/* 68  */ 					for(var i=0; i < msg.length; i++){
/* 69  */ 						msgBlock.append(msg[i]);
/* 70  */ 					}
/* 71  */ 				}
/* 72  */ 			});
/* 73  */ 		} else if(action=='create'){
/* 74  */ 			var deferred = jQuery.Deferred();
/* 75  */ 			var errors = [];
/* 76  */ 			// Pre-loader Start
/* 77  */ 			wpvp_progressBar(1);
/* 78  */ 			// Check files
/* 79  */ 			error = false;
/* 80  */ 			jQuery.each(files, function(key, value){
/* 81  */ 				if(value.size>file_upload_limit){
/* 82  */ 					error = true;
/* 83  */ 					errors.push(value.name+' file exceeds allowed size.');
/* 84  */ 				}
/* 85  */ 				if(key==(files.length-1))
/* 86  */ 					deferred.resolve();
/* 87  */ 			});
/* 88  */ 			deferred.done(function(){
/* 89  */ 				if(error){
/* 90  */ 					if(errors instanceof Array){
/* 91  */ 						for(x=0;x<errors.length;x++){
/* 92  */ 							jQuery('.wpvp_file_error').append(errors[x]+'<br />');
/* 93  */ 						}
/* 94  */ 					}
/* 95  */ 					//hide loader
/* 96  */ 					wpvp_progressBar(0);
/* 97  */ 				} else {
/* 98  */ 					//clear file errors
/* 99  */ 					jQuery('.wpvp_file_error').html('');
/* 100 */ 					//process form

/* wpvp-front-end.js */

/* 101 */ 					var data = {
/* 102 */ 						action: 'wpvp_process_form',
/* 103 */ 						'cookie': encodeURIComponent(document.cookie),
/* 104 */ 						data: formData
/* 105 */ 					};
/* 106 */ 					var wpvp_form_done = jQuery.post(wpvp_ajax,data);
/* 107 */ 					jQuery.when(wpvp_form_done).done(function(response){
/* 108 */ 						var obj = JSON.parse(response);
/* 109 */ 						var status = '';
/* 110 */ 						var msg = '';
/* 111 */ 						var postid = 0;
/* 112 */ 						if(obj.hasOwnProperty('status'))
/* 113 */ 							status = obj.status;
/* 114 */ 						if(obj.hasOwnProperty('msg'))
/* 115 */ 							msg = obj.msg;
/* 116 */ 						if(obj.hasOwnProperty('post_id'))
/* 117 */ 							postid = obj.post_id;	
/* 118 */ 						var data = new FormData();
/* 119 */ 						jQuery.each(files, function(key, value){
/* 120 */ 							data.append(key, value);
/* 121 */ 							data.append('postid',postid);
/* 122 */ 						});
/* 123 */ 						jQuery.ajax({
/* 124 */ 							url: wpvp_ajax+'?action=wpvp_process_files',
/* 125 */ 							type: 'POST',
/* 126 */ 							data: data,
/* 127 */ 							cache: false,
/* 128 */ 							dataType: 'json',
/* 129 */ 							processData: false, // Don't process the files
/* 130 */ 							contentType: false, // Set content type to false
/* 131 */ 							success: function(obj, textStatus, jqXHR){
/* 132 */ 								var status = '';
/* 133 */ 								var errors = [];
/* 134 */ 								var html = '';
/* 135 */ 								var url = '';
/* 136 */ 								if(obj.hasOwnProperty('status'))
/* 137 */ 									status = obj.status;
/* 138 */ 								if(obj.hasOwnProperty('errors'))
/* 139 */ 									errors = obj.errors;
/* 140 */ 								if(obj.hasOwnProperty('html'))
/* 141 */ 									html = obj.html;
/* 142 */ 								if(obj.hasOwnProperty('url'))
/* 143 */ 									url = obj.url;
/* 144 */ 								if(status=='success'){
/* 145 */ 									jQuery('.wpvp_file_error').html('');
/* 146 */ 									form.html(html);
/* 147 */ 									if(url!=''){
/* 148 */ 										setTimeout(function(){
/* 149 */ 											window.location.href = url;
/* 150 */ 										},5000);

/* wpvp-front-end.js */

/* 151 */ 									}
/* 152 */ 								} else if(status=='error'){
/* 153 */ 									if( errors instanceof Array){
/* 154 */ 										for(i=1; i<errors.length ; i++){
/* 155 */ 											jQuery('.wpvp_file_error').append(errors[i]+'<br />');
/* 156 */ 										}
/* 157 */ 									}
/* 158 */ 								}
/* 159 */ 							},
/* 160 */ 							error: function(jqXHR, textStatus, errorThrown){
/* 161 */ 								// Handle errors here
/* 162 */ 								console.log('ERRORS: ' + textStatus);
/* 163 */ 								wpvp_progressBar(0);
/* 164 */ 							}
/* 165 */ 						});
/* 166 */ 					});
/* 167 */ 				}
/* 168 */ 			});
/* 169 */ 		}
/* 170 */ 	});
/* 171 */ }
/* 172 */ function wpvp_progressBar(show) {
/* 173 */ 	if(show){
/* 174 */ 		jQuery('.wpvp_upload_progress').css('display','block');
/* 175 */ 	} else {
/* 176 */ 		jQuery('.wpvp_upload_progress').css('display','none');
/* 177 */ 	}
/* 178 */ };	

;
/* single-rental.js */

/* 1  */ /**
/* 2  *|  * Created by me664 on 3/3/15.
/* 3  *|  */
/* 4  */ jQuery(document).ready(function($){
/* 5  */ 
/* 6  */     $('.btn_booking_modal').click(function(){
/* 7  */        var form=$(this).closest('form');
/* 8  */        $('.alert',form).remove();
/* 9  */         var validate_form=true;
/* 10 */         var data=[];
/* 11 */ 
/* 12 */         $('input.required,textarea.required,select.required',form).each(function(){
/* 13 */ 
/* 14 */             $(this).removeClass('error');
/* 15 */             if(!$(this).val()){
/* 16 */                 validate_form=false;
/* 17 */                 $(this).addClass('error');
/* 18 */             }
/* 19 */ 
/* 20 */             if($(this).attr('name')){
/* 21 */                 data.push({
/* 22 */                     'value':$(this).val(),
/* 23 */                     'name':$(this).attr('name')
/* 24 */                 });
/* 25 */             }
/* 26 */ 
/* 27 */         });
/* 28 */ 
/* 29 */         if(!validate_form)
/* 30 */         {
/* 31 */             form.prepend('<div class="alert alert-danger">'+st_checkout_text.validate_form+'</div>');
/* 32 */             return false;
/* 33 */         }else
/* 34 */         {
/* 35 */             var tar_get=$(this).data('target');
/* 36 */ 
/* 37 */             for(i=0;i<data.length;i++)
/* 38 */             {
/* 39 */                 var val=data[i];
/* 40 */                 $(tar_get).find('.booking_modal_form').prepend('<input type="hidden" name="'+val.name+'" value="'+val.value+'">');
/* 41 */             }
/* 42 */ 
/* 43 */             $.magnificPopup.open({
/* 44 */                 items: {
/* 45 */                     type: 'inline',
/* 46 */                     src: tar_get
/* 47 */                 }
/* 48 */ 
/* 49 */             });
/* 50 */         }

/* single-rental.js */

/* 51 */     });
/* 52 */ 
/* 53 */ 
/* 54 */ });
