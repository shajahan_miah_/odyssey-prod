
/* gmap3.js */

/* 1    */ /*!
/* 2    *|  *  GMAP3 Plugin for jQuery
/* 3    *|  *  Version   : 6.0.0
/* 4    *|  *  Date      : 2014-04-25
/* 5    *|  *  Author    : DEMONTE Jean-Baptiste
/* 6    *|  *  Contact   : jbdemonte@gmail.com
/* 7    *|  *  Web site  : http://gmap3.net
/* 8    *|  *  Licence   : GPL v3 : http://www.gnu.org/licenses/gpl.html
/* 9    *|  *
/* 10   *|  *  Copyright (c) 2010-2014 Jean-Baptiste DEMONTE
/* 11   *|  *  All rights reserved.
/* 12   *|  */
/* 13   */ ;(function ($, undef) {
/* 14   */ 
/* 15   */     var defaults, gm,
/* 16   */         gId = 0,
/* 17   */         isFunction = $.isFunction,
/* 18   */         isArray = $.isArray;
/* 19   */ 
/* 20   */     function isObject(m) {
/* 21   */         return typeof m === "object";
/* 22   */     }
/* 23   */ 
/* 24   */     function isString(m) {
/* 25   */         return typeof m === "string";
/* 26   */     }
/* 27   */ 
/* 28   */     function isNumber(m) {
/* 29   */         return typeof m === "number";
/* 30   */     }
/* 31   */ 
/* 32   */     function isUndefined(m) {
/* 33   */         return m === undef;
/* 34   */     }
/* 35   */ 
/* 36   */     /**
/* 37   *|      * Initialize default values
/* 38   *|      * defaults are defined at first gmap3 call to pass the rails asset pipeline and jasmine while google library is not yet loaded
/* 39   *|      */
/* 40   */     function initDefaults() {
/* 41   */         gm = google.maps;
/* 42   */         if (!defaults) {
/* 43   */             defaults = {
/* 44   */                 verbose: false,
/* 45   */                 queryLimit: {
/* 46   */                     attempt: 5,
/* 47   */                     delay: 250, // setTimeout(..., delay + random);
/* 48   */                     random: 250
/* 49   */                 },
/* 50   */                 classes: (function () {

/* gmap3.js */

/* 51   */                     var r = {};
/* 52   */                     $.each("Map Marker InfoWindow Circle Rectangle OverlayView StreetViewPanorama KmlLayer TrafficLayer BicyclingLayer GroundOverlay StyledMapType ImageMapType".split(" "), function (_, k) {
/* 53   */                         r[k] = gm[k];
/* 54   */                     });
/* 55   */                     return r;
/* 56   */                 }()),
/* 57   */                 map: {
/* 58   */                     mapTypeId : gm.MapTypeId.ROADMAP,
/* 59   */                     center: [46.578498, 2.457275],
/* 60   */                     zoom: 2
/* 61   */                 },
/* 62   */                 overlay: {
/* 63   */                     pane: "floatPane",
/* 64   */                     content: "",
/* 65   */                     offset: {
/* 66   */                         x: 0,
/* 67   */                         y: 0
/* 68   */                     }
/* 69   */                 },
/* 70   */                 geoloc: {
/* 71   */                     getCurrentPosition: {
/* 72   */                         maximumAge: 60000,
/* 73   */                         timeout: 5000
/* 74   */                     }
/* 75   */                 }
/* 76   */             }
/* 77   */         }
/* 78   */     }
/* 79   */ 
/* 80   */ 
/* 81   */     /**
/* 82   *|      * Generate a new ID if not defined
/* 83   *|      * @param id {string} (optional)
/* 84   *|      * @param simulate {boolean} (optional)
/* 85   *|      * @returns {*}
/* 86   *|      */
/* 87   */     function globalId(id, simulate) {
/* 88   */         return isUndefined(id) ? "gmap3_" + (simulate ? gId + 1 : ++gId) : id;
/* 89   */     }
/* 90   */ 
/* 91   */ 
/* 92   */     /**
/* 93   *|      * Return true if current version of Google Maps is equal or above to these in parameter
/* 94   *|      * @param version {string} Minimal version required
/* 95   *|      * @return {Boolean}
/* 96   *|      */
/* 97   */     function googleVersionMin(version) {
/* 98   */         var i,
/* 99   */             gmVersion = gm.version.split(".");
/* 100  */         version = version.split(".");

/* gmap3.js */

/* 101  */         for (i = 0; i < gmVersion.length; i++) {
/* 102  */             gmVersion[i] = parseInt(gmVersion[i], 10);
/* 103  */         }
/* 104  */         for (i = 0; i < version.length; i++) {
/* 105  */             version[i] = parseInt(version[i], 10);
/* 106  */             if (gmVersion.hasOwnProperty(i)) {
/* 107  */                 if (gmVersion[i] < version[i]) {
/* 108  */                     return false;
/* 109  */                 }
/* 110  */             } else {
/* 111  */                 return false;
/* 112  */             }
/* 113  */         }
/* 114  */         return true;
/* 115  */     }
/* 116  */ 
/* 117  */ 
/* 118  */     /**
/* 119  *|      * attach events from a container to a sender
/* 120  *|      * td[
/* 121  *|      *  events => { eventName => function, }
/* 122  *|      *  onces  => { eventName => function, }
/* 123  *|      *  data   => mixed data
/* 124  *|      * ]
/* 125  *|      **/
/* 126  */     function attachEvents($container, args, sender, id, senders) {
/* 127  */         var td = args.td || {},
/* 128  */             context = {
/* 129  */                 id: id,
/* 130  */                 data: td.data,
/* 131  */                 tag: td.tag
/* 132  */             };
/* 133  */         function bind(items, handler) {
/* 134  */             if (items) {
/* 135  */                 $.each(items, function (name, f) {
/* 136  */                     var self = $container, fn = f;
/* 137  */                     if (isArray(f)) {
/* 138  */                         self = f[0];
/* 139  */                         fn = f[1];
/* 140  */                     }
/* 141  */                     handler(sender, name, function (event) {
/* 142  */                         fn.apply(self, [senders || sender, event, context]);
/* 143  */                     });
/* 144  */                 });
/* 145  */             }
/* 146  */         }
/* 147  */         bind(td.events, gm.event.addListener);
/* 148  */         bind(td.onces, gm.event.addListenerOnce);
/* 149  */     }
/* 150  */ 

/* gmap3.js */

/* 151  */     /**
/* 152  *|      * Extract keys from object
/* 153  *|      * @param obj {object}
/* 154  *|      * @returns {Array}
/* 155  *|      */
/* 156  */     function getKeys(obj) {
/* 157  */         var k, keys = [];
/* 158  */         for (k in obj) {
/* 159  */             if (obj.hasOwnProperty(k)) {
/* 160  */                 keys.push(k);
/* 161  */             }
/* 162  */         }
/* 163  */         return keys;
/* 164  */     }
/* 165  */ 
/* 166  */     /**
/* 167  *|      * copy a key content
/* 168  *|      **/
/* 169  */     function copyKey(target, key) {
/* 170  */         var i,
/* 171  */             args = arguments;
/* 172  */         for (i = 2; i < args.length; i++) {
/* 173  */             if (key in args[i]) {
/* 174  */                 if (args[i].hasOwnProperty(key)) {
/* 175  */                     target[key] = args[i][key];
/* 176  */                     return;
/* 177  */                 }
/* 178  */             }
/* 179  */         }
/* 180  */     }
/* 181  */ 
/* 182  */     /**
/* 183  *|      * Build a tuple
/* 184  *|      * @param args {object}
/* 185  *|      * @param value {object}
/* 186  *|      * @returns {object}
/* 187  *|      */
/* 188  */     function tuple(args, value) {
/* 189  */         var k, i,
/* 190  */             keys = ["data", "tag", "id", "events",  "onces"],
/* 191  */             td = {};
/* 192  */ 
/* 193  */         // "copy" the common data
/* 194  */         if (args.td) {
/* 195  */             for (k in args.td) {
/* 196  */                 if (args.td.hasOwnProperty(k)) {
/* 197  */                     if ((k !== "options") && (k !== "values")) {
/* 198  */                         td[k] = args.td[k];
/* 199  */                     }
/* 200  */                 }

/* gmap3.js */

/* 201  */             }
/* 202  */         }
/* 203  */         // "copy" some specific keys from value first else args.td
/* 204  */         for (i = 0; i < keys.length; i++) {
/* 205  */             copyKey(td, keys[i], value, args.td);
/* 206  */         }
/* 207  */ 
/* 208  */         // create an extended options
/* 209  */         td.options = $.extend({}, args.opts || {}, value.options || {});
/* 210  */ 
/* 211  */         return td;
/* 212  */     }
/* 213  */ 
/* 214  */     /**
/* 215  *|      * Log error
/* 216  *|      */
/* 217  */     function error() {
/* 218  */         if (defaults.verbose) {
/* 219  */             var i, err = [];
/* 220  */             if (window.console && (isFunction(console.error))) {
/* 221  */                 for (i = 0; i < arguments.length; i++) {
/* 222  */                     err.push(arguments[i]);
/* 223  */                 }
/* 224  */                 console.error.apply(console, err);
/* 225  */             } else {
/* 226  */                 err = "";
/* 227  */                 for (i = 0; i < arguments.length; i++) {
/* 228  */                     err += arguments[i].toString() + " ";
/* 229  */                 }
/* 230  */                 alert(err);
/* 231  */             }
/* 232  */         }
/* 233  */     }
/* 234  */ 
/* 235  */     /**
/* 236  *|      * return true if mixed is usable as number
/* 237  *|      **/
/* 238  */     function numeric(mixed) {
/* 239  */         return (isNumber(mixed) || isString(mixed)) && mixed !== "" && !isNaN(mixed);
/* 240  */     }
/* 241  */ 
/* 242  */     /**
/* 243  *|      * convert data to array
/* 244  *|      **/
/* 245  */     function array(mixed) {
/* 246  */         var k, a = [];
/* 247  */         if (!isUndefined(mixed)) {
/* 248  */             if (isObject(mixed)) {
/* 249  */                 if (isNumber(mixed.length)) {
/* 250  */                     a = mixed;

/* gmap3.js */

/* 251  */                 } else {
/* 252  */                     for (k in mixed) {
/* 253  */                         a.push(mixed[k]);
/* 254  */                     }
/* 255  */                 }
/* 256  */             } else {
/* 257  */                 a.push(mixed);
/* 258  */             }
/* 259  */         }
/* 260  */         return a;
/* 261  */     }
/* 262  */ 
/* 263  */     /**
/* 264  *|      * create a function to check a tag
/* 265  *|      */
/* 266  */     function ftag(tag) {
/* 267  */         if (tag) {
/* 268  */             if (isFunction(tag)) {
/* 269  */                 return tag;
/* 270  */             }
/* 271  */             tag = array(tag);
/* 272  */             return function (val) {
/* 273  */                 var i;
/* 274  */                 if (isUndefined(val)) {
/* 275  */                     return false;
/* 276  */                 }
/* 277  */                 if (isObject(val)) {
/* 278  */                     for (i = 0; i < val.length; i++) {
/* 279  */                         if ($.inArray(val[i], tag) >= 0) {
/* 280  */                             return true;
/* 281  */                         }
/* 282  */                     }
/* 283  */                     return false;
/* 284  */                 }
/* 285  */                 return $.inArray(val, tag) >= 0;
/* 286  */             };
/* 287  */         }
/* 288  */     }
/* 289  */ 
/* 290  */ 
/* 291  */     /**
/* 292  *|      * convert mixed [ lat, lng ] objet to gm.LatLng
/* 293  *|      **/
/* 294  */     function toLatLng(mixed, emptyReturnMixed, noFlat) {
/* 295  */         var empty = emptyReturnMixed ? mixed : null;
/* 296  */         if (!mixed || (isString(mixed))) {
/* 297  */             return empty;
/* 298  */         }
/* 299  */         // defined latLng
/* 300  */         if (mixed.latLng) {

/* gmap3.js */

/* 301  */             return toLatLng(mixed.latLng);
/* 302  */         }
/* 303  */         // gm.LatLng object
/* 304  */         if (mixed instanceof gm.LatLng) {
/* 305  */             return mixed;
/* 306  */         }
/* 307  */         // {lat:X, lng:Y} object
/* 308  */         if (numeric(mixed.lat)) {
/* 309  */             return new gm.LatLng(mixed.lat, mixed.lng);
/* 310  */         }
/* 311  */         // [X, Y] object
/* 312  */         if (!noFlat && isArray(mixed)) {
/* 313  */             if (!numeric(mixed[0]) || !numeric(mixed[1])) {
/* 314  */                 return empty;
/* 315  */             }
/* 316  */             return new gm.LatLng(mixed[0], mixed[1]);
/* 317  */         }
/* 318  */         return empty;
/* 319  */     }
/* 320  */ 
/* 321  */     /**
/* 322  *|      * convert mixed [ sw, ne ] object by gm.LatLngBounds
/* 323  *|      **/
/* 324  */     function toLatLngBounds(mixed) {
/* 325  */         var ne, sw;
/* 326  */         if (!mixed || mixed instanceof gm.LatLngBounds) {
/* 327  */             return mixed || null;
/* 328  */         }
/* 329  */         if (isArray(mixed)) {
/* 330  */             if (mixed.length === 2) {
/* 331  */                 ne = toLatLng(mixed[0]);
/* 332  */                 sw = toLatLng(mixed[1]);
/* 333  */             } else if (mixed.length === 4) {
/* 334  */                 ne = toLatLng([mixed[0], mixed[1]]);
/* 335  */                 sw = toLatLng([mixed[2], mixed[3]]);
/* 336  */             }
/* 337  */         } else {
/* 338  */             if (("ne" in mixed) && ("sw" in mixed)) {
/* 339  */                 ne = toLatLng(mixed.ne);
/* 340  */                 sw = toLatLng(mixed.sw);
/* 341  */             } else if (("n" in mixed) && ("e" in mixed) && ("s" in mixed) && ("w" in mixed)) {
/* 342  */                 ne = toLatLng([mixed.n, mixed.e]);
/* 343  */                 sw = toLatLng([mixed.s, mixed.w]);
/* 344  */             }
/* 345  */         }
/* 346  */         if (ne && sw) {
/* 347  */             return new gm.LatLngBounds(sw, ne);
/* 348  */         }
/* 349  */         return null;
/* 350  */     }

/* gmap3.js */

/* 351  */ 
/* 352  */     /**
/* 353  *|      * resolveLatLng
/* 354  *|      **/
/* 355  */     function resolveLatLng(ctx, method, runLatLng, args, attempt) {
/* 356  */         var latLng = runLatLng ? toLatLng(args.td, false, true) : false,
/* 357  */             conf = latLng ?  {latLng: latLng} : (args.td.address ? (isString(args.td.address) ? {address: args.td.address} : args.td.address) : false),
/* 358  */             cache = conf ? geocoderCache.get(conf) : false,
/* 359  */             self = this;
/* 360  */         if (conf) {
/* 361  */             attempt = attempt || 0; // convert undefined to int
/* 362  */             if (cache) {
/* 363  */                 args.latLng = cache.results[0].geometry.location;
/* 364  */                 args.results = cache.results;
/* 365  */                 args.status = cache.status;
/* 366  */                 method.apply(ctx, [args]);
/* 367  */             } else {
/* 368  */                 if (conf.location) {
/* 369  */                     conf.location = toLatLng(conf.location);
/* 370  */                 }
/* 371  */                 if (conf.bounds) {
/* 372  */                     conf.bounds = toLatLngBounds(conf.bounds);
/* 373  */                 }
/* 374  */                 geocoder().geocode(
/* 375  */                     conf,
/* 376  */                     function (results, status) {
/* 377  */                         if (status === gm.GeocoderStatus.OK) {
/* 378  */                             geocoderCache.store(conf, {results: results, status: status});
/* 379  */                             args.latLng = results[0].geometry.location;
/* 380  */                             args.results = results;
/* 381  */                             args.status = status;
/* 382  */                             method.apply(ctx, [args]);
/* 383  */                         } else if ((status === gm.GeocoderStatus.OVER_QUERY_LIMIT) && (attempt < defaults.queryLimit.attempt)) {
/* 384  */                             setTimeout(
/* 385  */                                 function () {
/* 386  */                                     resolveLatLng.apply(self, [ctx, method, runLatLng, args, attempt + 1]);
/* 387  */                                 },
/* 388  */                                 defaults.queryLimit.delay + Math.floor(Math.random() * defaults.queryLimit.random)
/* 389  */                             );
/* 390  */                         } else {
/* 391  */                             error("geocode failed", status, conf);
/* 392  */                             args.latLng = args.results = false;
/* 393  */                             args.status = status;
/* 394  */                             method.apply(ctx, [args]);
/* 395  */                         }
/* 396  */                     }
/* 397  */                 );
/* 398  */             }
/* 399  */         } else {
/* 400  */             args.latLng = toLatLng(args.td, false, true);

/* gmap3.js */

/* 401  */             method.apply(ctx, [args]);
/* 402  */         }
/* 403  */     }
/* 404  */ 
/* 405  */     function resolveAllLatLng(list, ctx, method, args) {
/* 406  */         var self = this, i = -1;
/* 407  */ 
/* 408  */         function resolve() {
/* 409  */             // look for next address to resolve
/* 410  */             do {
/* 411  */                 i++;
/* 412  */             } while ((i < list.length) && !("address" in list[i]));
/* 413  */ 
/* 414  */             // no address found, so run method
/* 415  */             if (i >= list.length) {
/* 416  */                 method.apply(ctx, [args]);
/* 417  */                 return;
/* 418  */             }
/* 419  */ 
/* 420  */             resolveLatLng(
/* 421  */                 self,
/* 422  */                 function (args) {
/* 423  */                     delete args.td;
/* 424  */                     $.extend(list[i], args);
/* 425  */                     resolve.apply(self, []); // resolve next (using apply avoid too much recursion)
/* 426  */                 },
/* 427  */                 true,
/* 428  */                 {td: list[i]}
/* 429  */             );
/* 430  */         }
/* 431  */         resolve();
/* 432  */     }
/* 433  */ 
/* 434  */ 
/* 435  */ 
/* 436  */     /**
/* 437  *|      * geolocalise the user and return a LatLng
/* 438  *|      **/
/* 439  */     function geoloc(ctx, method, args) {
/* 440  */         var is_echo = false; // sometime, a kind of echo appear, this trick will notice once the first call is run to ignore the next one
/* 441  */         if (navigator && navigator.geolocation) {
/* 442  */             navigator.geolocation.getCurrentPosition(
/* 443  */                 function (pos) {
/* 444  */                     if (!is_echo) {
/* 445  */                         is_echo = true;
/* 446  */                         args.latLng = new gm.LatLng(pos.coords.latitude, pos.coords.longitude);
/* 447  */                         method.apply(ctx, [args]);
/* 448  */                     }
/* 449  */                 },
/* 450  */                 function () {

/* gmap3.js */

/* 451  */                     if (!is_echo) {
/* 452  */                         is_echo = true;
/* 453  */                         args.latLng = false;
/* 454  */                         method.apply(ctx, [args]);
/* 455  */                     }
/* 456  */                 },
/* 457  */                 args.opts.getCurrentPosition
/* 458  */             );
/* 459  */         } else {
/* 460  */             args.latLng = false;
/* 461  */             method.apply(ctx, [args]);
/* 462  */         }
/* 463  */     }
/* 464  */ 
/* 465  */     /**
/* 466  *|      * Return true if get is a direct call
/* 467  *|      * it means :
/* 468  *|      *   - get is the only key
/* 469  *|      *   - get has no callback
/* 470  *|      * @param obj {Object} The request to check
/* 471  *|      * @return {Boolean}
/* 472  *|      */
/* 473  */     function isDirectGet(obj) {
/* 474  */         var k,
/* 475  */             result = false;
/* 476  */         if (isObject(obj) && obj.hasOwnProperty("get")) {
/* 477  */             for (k in obj) {
/* 478  */                 if (k !== "get") {
/* 479  */                     return false;
/* 480  */                 }
/* 481  */             }
/* 482  */             result = !obj.get.hasOwnProperty("callback");
/* 483  */         }
/* 484  */         return result;
/* 485  */     }
/* 486  */     var services = {},
/* 487  */         geocoderCache = new GeocoderCache();
/* 488  */ 
/* 489  */ 
/* 490  */     function geocoder(){
/* 491  */         if (!services.geocoder) {
/* 492  */             services.geocoder = new gm.Geocoder();
/* 493  */         }
/* 494  */         return services.geocoder;
/* 495  */     }
/* 496  */     /**
/* 497  *|      * Class GeocoderCache
/* 498  *|      * @constructor
/* 499  *|      */
/* 500  */     function GeocoderCache() {

/* gmap3.js */

/* 501  */         var cache = [];
/* 502  */ 
/* 503  */         this.get = function (request) {
/* 504  */             if (cache.length) {
/* 505  */                 var i, j, k, item, eq,
/* 506  */                     keys = getKeys(request);
/* 507  */                 for (i = 0; i < cache.length; i++) {
/* 508  */                     item = cache[i];
/* 509  */                     eq = keys.length === item.keys.length;
/* 510  */                     for (j = 0; (j < keys.length) && eq; j++) {
/* 511  */                         k = keys[j];
/* 512  */                         eq = k in item.request;
/* 513  */                         if (eq) {
/* 514  */                             if (isObject(request[k]) && ("equals" in request[k]) && isFunction(request[k])) {
/* 515  */                                 eq = request[k].equals(item.request[k]);
/* 516  */                             } else {
/* 517  */                                 eq = request[k] === item.request[k];
/* 518  */                             }
/* 519  */                         }
/* 520  */                     }
/* 521  */                     if (eq) {
/* 522  */                         return item.results;
/* 523  */                     }
/* 524  */                 }
/* 525  */             }
/* 526  */         };
/* 527  */ 
/* 528  */         this.store = function (request, results) {
/* 529  */             cache.push({request: request, keys: getKeys(request), results: results});
/* 530  */         };
/* 531  */     }
/* 532  */     /**
/* 533  *|      * Class Stack
/* 534  *|      * @constructor
/* 535  *|      */
/* 536  */     function Stack() {
/* 537  */         var st = [],
/* 538  */             self = this;
/* 539  */ 
/* 540  */         self.empty = function () {
/* 541  */             return !st.length;
/* 542  */         };
/* 543  */ 
/* 544  */         self.add = function (v) {
/* 545  */             st.push(v);
/* 546  */         };
/* 547  */ 
/* 548  */         self.get = function () {
/* 549  */             return st.length ? st[0] : false;
/* 550  */         };

/* gmap3.js */

/* 551  */ 
/* 552  */         self.ack = function () {
/* 553  */             st.shift();
/* 554  */         };
/* 555  */     }
/* 556  */     /**
/* 557  *|      * Class Store
/* 558  *|      * @constructor
/* 559  *|      */
/* 560  */     function Store() {
/* 561  */         var store = {}, // name => [id, ...]
/* 562  */             objects = {}, // id => object
/* 563  */             self = this;
/* 564  */ 
/* 565  */         function normalize(res) {
/* 566  */             return {
/* 567  */                 id: res.id,
/* 568  */                 name: res.name,
/* 569  */                 object: res.obj,
/* 570  */                 tag: res.tag,
/* 571  */                 data: res.data
/* 572  */             };
/* 573  */         }
/* 574  */ 
/* 575  */         /**
/* 576  *|          * add a mixed to the store
/* 577  *|          **/
/* 578  */         self.add = function (args, name, obj, sub) {
/* 579  */             var td = args.td || {},
/* 580  */                 id = globalId(td.id);
/* 581  */             if (!store[name]) {
/* 582  */                 store[name] = [];
/* 583  */             }
/* 584  */             if (id in objects) { // object already exists: remove it
/* 585  */                 self.clearById(id);
/* 586  */             }
/* 587  */             objects[id] = {obj: obj, sub: sub, name: name, id: id, tag: td.tag, data: td.data};
/* 588  */             store[name].push(id);
/* 589  */             return id;
/* 590  */         };
/* 591  */ 
/* 592  */         /**
/* 593  *|          * return a stored object by its id
/* 594  *|          **/
/* 595  */         self.getById = function (id, sub, full) {
/* 596  */             var result = false;
/* 597  */             if (id in objects) {
/* 598  */                 if (sub) {
/* 599  */                     result = objects[id].sub;
/* 600  */                 } else if (full) {

/* gmap3.js */

/* 601  */                     result = normalize(objects[id]);
/* 602  */                 } else {
/* 603  */                     result = objects[id].obj;
/* 604  */                 }
/* 605  */             }
/* 606  */             return result;
/* 607  */         };
/* 608  */ 
/* 609  */         /**
/* 610  *|          * return a stored value
/* 611  *|          **/
/* 612  */         self.get = function (name, last, tag, full) {
/* 613  */             var n, id, check = ftag(tag);
/* 614  */             if (!store[name] || !store[name].length) {
/* 615  */                 return null;
/* 616  */             }
/* 617  */             n = store[name].length;
/* 618  */             while (n) {
/* 619  */                 n--;
/* 620  */                 id = store[name][last ? n : store[name].length - n - 1];
/* 621  */                 if (id && objects[id]) {
/* 622  */                     if (check && !check(objects[id].tag)) {
/* 623  */                         continue;
/* 624  */                     }
/* 625  */                     return full ? normalize(objects[id]) : objects[id].obj;
/* 626  */                 }
/* 627  */             }
/* 628  */             return null;
/* 629  */         };
/* 630  */ 
/* 631  */         /**
/* 632  *|          * return all stored values
/* 633  *|          **/
/* 634  */         self.all = function (name, tag, full) {
/* 635  */             var result = [],
/* 636  */                 check = ftag(tag),
/* 637  */                 find = function (n) {
/* 638  */                     var i, id;
/* 639  */                     for (i = 0; i < store[n].length; i++) {
/* 640  */                         id = store[n][i];
/* 641  */                         if (id && objects[id]) {
/* 642  */                             if (check && !check(objects[id].tag)) {
/* 643  */                                 continue;
/* 644  */                             }
/* 645  */                             result.push(full ? normalize(objects[id]) : objects[id].obj);
/* 646  */                         }
/* 647  */                     }
/* 648  */                 };
/* 649  */             if (name in store) {
/* 650  */                 find(name);

/* gmap3.js */

/* 651  */             } else if (isUndefined(name)) { // internal use only
/* 652  */                 for (name in store) {
/* 653  */                     find(name);
/* 654  */                 }
/* 655  */             }
/* 656  */             return result;
/* 657  */         };
/* 658  */ 
/* 659  */         /**
/* 660  *|          * hide and remove an object
/* 661  *|          **/
/* 662  */         function rm(obj) {
/* 663  */             // Google maps element
/* 664  */             if (isFunction(obj.setMap)) {
/* 665  */                 obj.setMap(null);
/* 666  */             }
/* 667  */             // jQuery
/* 668  */             if (isFunction(obj.remove)) {
/* 669  */                 obj.remove();
/* 670  */             }
/* 671  */             // internal (cluster)
/* 672  */             if (isFunction(obj.free)) {
/* 673  */                 obj.free();
/* 674  */             }
/* 675  */             obj = null;
/* 676  */         }
/* 677  */ 
/* 678  */         /**
/* 679  *|          * remove one object from the store
/* 680  *|          **/
/* 681  */         self.rm = function (name, check, pop) {
/* 682  */             var idx, id;
/* 683  */             if (!store[name]) {
/* 684  */                 return false;
/* 685  */             }
/* 686  */             if (check) {
/* 687  */                 if (pop) {
/* 688  */                     for (idx = store[name].length - 1; idx >= 0; idx--) {
/* 689  */                         id = store[name][idx];
/* 690  */                         if (check(objects[id].tag)) {
/* 691  */                             break;
/* 692  */                         }
/* 693  */                     }
/* 694  */                 } else {
/* 695  */                     for (idx = 0; idx < store[name].length; idx++) {
/* 696  */                         id = store[name][idx];
/* 697  */                         if (check(objects[id].tag)) {
/* 698  */                             break;
/* 699  */                         }
/* 700  */                     }

/* gmap3.js */

/* 701  */                 }
/* 702  */             } else {
/* 703  */                 idx = pop ? store[name].length - 1 : 0;
/* 704  */             }
/* 705  */             if (!(idx in store[name])) {
/* 706  */                 return false;
/* 707  */             }
/* 708  */             return self.clearById(store[name][idx], idx);
/* 709  */         };
/* 710  */ 
/* 711  */         /**
/* 712  *|          * remove object from the store by its id
/* 713  *|          **/
/* 714  */         self.clearById = function (id, idx) {
/* 715  */             if (id in objects) {
/* 716  */                 var i, name = objects[id].name;
/* 717  */                 for (i = 0; isUndefined(idx) && i < store[name].length; i++) {
/* 718  */                     if (id === store[name][i]) {
/* 719  */                         idx = i;
/* 720  */                     }
/* 721  */                 }
/* 722  */                 rm(objects[id].obj);
/* 723  */                 if (objects[id].sub) {
/* 724  */                     rm(objects[id].sub);
/* 725  */                 }
/* 726  */                 delete objects[id];
/* 727  */                 store[name].splice(idx, 1);
/* 728  */                 return true;
/* 729  */             }
/* 730  */             return false;
/* 731  */         };
/* 732  */ 
/* 733  */         /**
/* 734  *|          * return an object from a container object in the store by its id
/* 735  *|          * ! for now, only cluster manage this feature
/* 736  *|          **/
/* 737  */         self.objGetById = function (id) {
/* 738  */             var result, idx;
/* 739  */             if (store.clusterer) {
/* 740  */                 for (idx in store.clusterer) {
/* 741  */                     if ((result = objects[store.clusterer[idx]].obj.getById(id)) !== false) {
/* 742  */                         return result;
/* 743  */                     }
/* 744  */                 }
/* 745  */             }
/* 746  */             return false;
/* 747  */         };
/* 748  */ 
/* 749  */         /**
/* 750  *|          * remove object from a container object in the store by its id

/* gmap3.js */

/* 751  *|          * ! for now, only cluster manage this feature
/* 752  *|          **/
/* 753  */         self.objClearById = function (id) {
/* 754  */             var idx;
/* 755  */             if (store.clusterer) {
/* 756  */                 for (idx in store.clusterer) {
/* 757  */                     if (objects[store.clusterer[idx]].obj.clearById(id)) {
/* 758  */                         return true;
/* 759  */                     }
/* 760  */                 }
/* 761  */             }
/* 762  */             return null;
/* 763  */         };
/* 764  */ 
/* 765  */         /**
/* 766  *|          * remove objects from the store
/* 767  *|          **/
/* 768  */         self.clear = function (list, last, first, tag) {
/* 769  */             var k, i, name,
/* 770  */                 check = ftag(tag);
/* 771  */             if (!list || !list.length) {
/* 772  */                 list = [];
/* 773  */                 for (k in store) {
/* 774  */                     list.push(k);
/* 775  */                 }
/* 776  */             } else {
/* 777  */                 list = array(list);
/* 778  */             }
/* 779  */             for (i = 0; i < list.length; i++) {
/* 780  */                 name = list[i];
/* 781  */                 if (last) {
/* 782  */                     self.rm(name, check, true);
/* 783  */                 } else if (first) {
/* 784  */                     self.rm(name, check, false);
/* 785  */                 } else { // all
/* 786  */                     while (self.rm(name, check, false)) {
/* 787  */                     }
/* 788  */                 }
/* 789  */             }
/* 790  */         };
/* 791  */ 
/* 792  */         /**
/* 793  *|          * remove object from a container object in the store by its tags
/* 794  *|          * ! for now, only cluster manage this feature
/* 795  *|          **/
/* 796  */         self.objClear = function (list, last, first, tag) {
/* 797  */             var idx;
/* 798  */             if (store.clusterer && ($.inArray("marker", list) >= 0 || !list.length)) {
/* 799  */                 for (idx in store.clusterer) {
/* 800  */                     objects[store.clusterer[idx]].obj.clear(last, first, tag);

/* gmap3.js */

/* 801  */                 }
/* 802  */             }
/* 803  */         };
/* 804  */     }
/* 805  */     /**
/* 806  *|      * Class Task
/* 807  *|      * @param ctx
/* 808  *|      * @param onEnd
/* 809  *|      * @param td
/* 810  *|      * @constructor
/* 811  *|      */
/* 812  */     function Task(ctx, onEnd, td) {
/* 813  */         var session = {},
/* 814  */             self = this,
/* 815  */             current,
/* 816  */             resolve = {
/* 817  */                 latLng: { // function => bool (=> address = latLng)
/* 818  */                     map: false,
/* 819  */                     marker: false,
/* 820  */                     infowindow: false,
/* 821  */                     circle: false,
/* 822  */                     overlay: false,
/* 823  */                     getlatlng: false,
/* 824  */                     getmaxzoom: false,
/* 825  */                     getelevation: false,
/* 826  */                     streetviewpanorama: false,
/* 827  */                     getaddress: true
/* 828  */                 },
/* 829  */                 geoloc: {
/* 830  */                     getgeoloc: true
/* 831  */                 }
/* 832  */             };
/* 833  */ 
/* 834  */         function unify(td) {
/* 835  */             var result = {};
/* 836  */             result[td] = {};
/* 837  */             return result;
/* 838  */         }
/* 839  */ 
/* 840  */         if (isString(td)) {
/* 841  */             td =  unify(td);
/* 842  */         }
/* 843  */ 
/* 844  */         function next() {
/* 845  */             var k;
/* 846  */             for (k in td) {
/* 847  */                 if (td.hasOwnProperty(k) && !session.hasOwnProperty(k)) {
/* 848  */                     return k;
/* 849  */                 }
/* 850  */             }

/* gmap3.js */

/* 851  */         }
/* 852  */ 
/* 853  */         self.run = function () {
/* 854  */             var k, opts;
/* 855  */             while (k = next()) {
/* 856  */                 if (isFunction(ctx[k])) {
/* 857  */                     current = k;
/* 858  */                     opts = $.extend(true, {}, defaults[k] || {}, td[k].options || {});
/* 859  */                     if (k in resolve.latLng) {
/* 860  */                         if (td[k].values) {
/* 861  */                             resolveAllLatLng(td[k].values, ctx, ctx[k], {td: td[k], opts: opts, session: session});
/* 862  */                         } else {
/* 863  */                             resolveLatLng(ctx, ctx[k], resolve.latLng[k], {td: td[k], opts: opts, session: session});
/* 864  */                         }
/* 865  */                     } else if (k in resolve.geoloc) {
/* 866  */                         geoloc(ctx, ctx[k], {td: td[k], opts: opts, session: session});
/* 867  */                     } else {
/* 868  */                         ctx[k].apply(ctx, [{td: td[k], opts: opts, session: session}]);
/* 869  */                     }
/* 870  */                     return; // wait until ack
/* 871  */                 } else {
/* 872  */                     session[k] = null;
/* 873  */                 }
/* 874  */             }
/* 875  */             onEnd.apply(ctx, [td, session]);
/* 876  */         };
/* 877  */ 
/* 878  */         self.ack = function(result){
/* 879  */             session[current] = result;
/* 880  */             self.run.apply(self, []);
/* 881  */         };
/* 882  */     }
/* 883  */ 
/* 884  */     function directionsService(){
/* 885  */         if (!services.ds) {
/* 886  */             services.ds = new gm.DirectionsService();
/* 887  */         }
/* 888  */         return services.ds;
/* 889  */     }
/* 890  */ 
/* 891  */     function distanceMatrixService() {
/* 892  */         if (!services.dms) {
/* 893  */             services.dms = new gm.DistanceMatrixService();
/* 894  */         }
/* 895  */         return services.dms;
/* 896  */     }
/* 897  */ 
/* 898  */     function maxZoomService() {
/* 899  */         if (!services.mzs) {
/* 900  */             services.mzs = new gm.MaxZoomService();

/* gmap3.js */

/* 901  */         }
/* 902  */         return services.mzs;
/* 903  */     }
/* 904  */ 
/* 905  */     function elevationService() {
/* 906  */         if (!services.es) {
/* 907  */             services.es = new gm.ElevationService();
/* 908  */         }
/* 909  */         return services.es;
/* 910  */     }
/* 911  */ 
/* 912  */     /**
/* 913  *|      * Usefull to get a projection
/* 914  *|      * => done in a function, to let dead-code analyser works without google library loaded
/* 915  *|      **/
/* 916  */     function newEmptyOverlay(map, radius) {
/* 917  */         function Overlay() {
/* 918  */             var self = this;
/* 919  */             self.onAdd = function () {};
/* 920  */             self.onRemove = function () {};
/* 921  */             self.draw = function () {};
/* 922  */             return defaults.classes.OverlayView.apply(self, []);
/* 923  */         }
/* 924  */         Overlay.prototype = defaults.classes.OverlayView.prototype;
/* 925  */         var obj = new Overlay();
/* 926  */         obj.setMap(map);
/* 927  */         return obj;
/* 928  */     }
/* 929  */ 
/* 930  */     /**
/* 931  *|      * Class InternalClusterer
/* 932  *|      * This class manage clusters thanks to "td" objects
/* 933  *|      *
/* 934  *|      * Note:
/* 935  *|      * Individuals marker are created on the fly thanks to the td objects, they are
/* 936  *|      * first set to null to keep the indexes synchronised with the td list
/* 937  *|      * This is the "display" function, set by the gmap3 object, which uses theses data
/* 938  *|      * to create markers when clusters are not required
/* 939  *|      * To remove a marker, the objects are deleted and set not null in arrays
/* 940  *|      *    markers[key]
/* 941  *|      *      = null : marker exist but has not been displayed yet
/* 942  *|      *      = false : marker has been removed
/* 943  *|      **/
/* 944  */     function InternalClusterer($container, map, raw) {
/* 945  */         var timer, projection,
/* 946  */             ffilter, fdisplay, ferror, // callback function
/* 947  */             updating = false,
/* 948  */             updated = false,
/* 949  */             redrawing = false,
/* 950  */             ready = false,

/* gmap3.js */

/* 951  */             enabled = true,
/* 952  */             self = this,
/* 953  */             events =  [],
/* 954  */             store = {},   // combin of index (id1-id2-...) => object
/* 955  */             ids = {},     // unique id => index
/* 956  */             idxs = {},    // index => unique id
/* 957  */             markers = [], // index => marker
/* 958  */             tds = [],   // index => td or null if removed
/* 959  */             values = [],  // index => value
/* 960  */             overlay = newEmptyOverlay(map, raw.radius);
/* 961  */ 
/* 962  */         main();
/* 963  */ 
/* 964  */         function prepareMarker(index) {
/* 965  */             if (!markers[index]) {
/* 966  */                 delete tds[index].options.map;
/* 967  */                 markers[index] = new defaults.classes.Marker(tds[index].options);
/* 968  */                 attachEvents($container, {td: tds[index]}, markers[index], tds[index].id);
/* 969  */             }
/* 970  */         }
/* 971  */ 
/* 972  */         /**
/* 973  *|          * return a marker by its id, null if not yet displayed and false if no exist or removed
/* 974  *|          **/
/* 975  */         self.getById = function (id) {
/* 976  */             if (id in ids) {
/* 977  */                 prepareMarker(ids[id]);
/* 978  */                 return  markers[ids[id]];
/* 979  */             }
/* 980  */             return false;
/* 981  */         };
/* 982  */ 
/* 983  */         /**
/* 984  *|          * remove one object from the store
/* 985  *|          **/
/* 986  */         self.rm = function (id) {
/* 987  */             var index = ids[id];
/* 988  */             if (markers[index]) { // can be null
/* 989  */                 markers[index].setMap(null);
/* 990  */             }
/* 991  */             delete markers[index];
/* 992  */             markers[index] = false;
/* 993  */ 
/* 994  */             delete tds[index];
/* 995  */             tds[index] = false;
/* 996  */ 
/* 997  */             delete values[index];
/* 998  */             values[index] = false;
/* 999  */ 
/* 1000 */             delete ids[id];

/* gmap3.js */

/* 1001 */             delete idxs[index];
/* 1002 */             updated = true;
/* 1003 */         };
/* 1004 */ 
/* 1005 */         /**
/* 1006 *|          * remove a marker by its id
/* 1007 *|          **/
/* 1008 */         self.clearById = function (id) {
/* 1009 */             if (id in ids){
/* 1010 */                 self.rm(id);
/* 1011 */                 return true;
/* 1012 */             }
/* 1013 */         };
/* 1014 */ 
/* 1015 */         /**
/* 1016 *|          * remove objects from the store
/* 1017 *|          **/
/* 1018 */         self.clear = function (last, first, tag) {
/* 1019 */             var start, stop, step, index, i,
/* 1020 */                 list = [],
/* 1021 */                 check = ftag(tag);
/* 1022 */             if (last) {
/* 1023 */                 start = tds.length - 1;
/* 1024 */                 stop = -1;
/* 1025 */                 step = -1;
/* 1026 */             } else {
/* 1027 */                 start = 0;
/* 1028 */                 stop =  tds.length;
/* 1029 */                 step = 1;
/* 1030 */             }
/* 1031 */             for (index = start; index !== stop; index += step) {
/* 1032 */                 if (tds[index]) {
/* 1033 */                     if (!check || check(tds[index].tag)) {
/* 1034 */                         list.push(idxs[index]);
/* 1035 */                         if (first || last) {
/* 1036 */                             break;
/* 1037 */                         }
/* 1038 */                     }
/* 1039 */                 }
/* 1040 */             }
/* 1041 */             for (i = 0; i < list.length; i++) {
/* 1042 */                 self.rm(list[i]);
/* 1043 */             }
/* 1044 */         };
/* 1045 */ 
/* 1046 */         // add a "marker td" to the cluster
/* 1047 */         self.add = function (td, value) {
/* 1048 */             td.id = globalId(td.id);
/* 1049 */             self.clearById(td.id);
/* 1050 */             ids[td.id] = markers.length;

/* gmap3.js */

/* 1051 */             idxs[markers.length] = td.id;
/* 1052 */             markers.push(null); // null = marker not yet created / displayed
/* 1053 */             tds.push(td);
/* 1054 */             values.push(value);
/* 1055 */             updated = true;
/* 1056 */         };
/* 1057 */ 
/* 1058 */         // add a real marker to the cluster
/* 1059 */         self.addMarker = function (marker, td) {
/* 1060 */             td = td || {};
/* 1061 */             td.id = globalId(td.id);
/* 1062 */             self.clearById(td.id);
/* 1063 */             if (!td.options) {
/* 1064 */                 td.options = {};
/* 1065 */             }
/* 1066 */             td.options.position = marker.getPosition();
/* 1067 */             attachEvents($container, {td: td}, marker, td.id);
/* 1068 */             ids[td.id] = markers.length;
/* 1069 */             idxs[markers.length] = td.id;
/* 1070 */             markers.push(marker);
/* 1071 */             tds.push(td);
/* 1072 */             values.push(td.data || {});
/* 1073 */             updated = true;
/* 1074 */         };
/* 1075 */ 
/* 1076 */         // return a "marker td" by its index
/* 1077 */         self.td = function (index) {
/* 1078 */             return tds[index];
/* 1079 */         };
/* 1080 */ 
/* 1081 */         // return a "marker value" by its index
/* 1082 */         self.value = function (index) {
/* 1083 */             return values[index];
/* 1084 */         };
/* 1085 */ 
/* 1086 */         // return a marker by its index
/* 1087 */         self.marker = function (index) {
/* 1088 */             if (index in markers) {
/* 1089 */                 prepareMarker(index);
/* 1090 */                 return  markers[index];
/* 1091 */             }
/* 1092 */             return false;
/* 1093 */         };
/* 1094 */ 
/* 1095 */         // return a marker by its index
/* 1096 */         self.markerIsSet = function (index) {
/* 1097 */             return Boolean(markers[index]);
/* 1098 */         };
/* 1099 */ 
/* 1100 */         // store a new marker instead if the default "false"

/* gmap3.js */

/* 1101 */         self.setMarker = function (index, marker) {
/* 1102 */             markers[index] = marker;
/* 1103 */         };
/* 1104 */ 
/* 1105 */         // link the visible overlay to the logical data (to hide overlays later)
/* 1106 */         self.store = function (cluster, obj, shadow) {
/* 1107 */             store[cluster.ref] = {obj: obj, shadow: shadow};
/* 1108 */         };
/* 1109 */ 
/* 1110 */         // free all objects
/* 1111 */         self.free = function () {
/* 1112 */             var i;
/* 1113 */             for(i = 0; i < events.length; i++) {
/* 1114 */                 gm.event.removeListener(events[i]);
/* 1115 */             }
/* 1116 */             events = [];
/* 1117 */ 
/* 1118 */             $.each(store, function (key) {
/* 1119 */                 flush(key);
/* 1120 */             });
/* 1121 */             store = {};
/* 1122 */ 
/* 1123 */             $.each(tds, function (i) {
/* 1124 */                 tds[i] = null;
/* 1125 */             });
/* 1126 */             tds = [];
/* 1127 */ 
/* 1128 */             $.each(markers, function (i) {
/* 1129 */                 if (markers[i]) { // false = removed
/* 1130 */                     markers[i].setMap(null);
/* 1131 */                     delete markers[i];
/* 1132 */                 }
/* 1133 */             });
/* 1134 */             markers = [];
/* 1135 */ 
/* 1136 */             $.each(values, function (i) {
/* 1137 */                 delete values[i];
/* 1138 */             });
/* 1139 */             values = [];
/* 1140 */ 
/* 1141 */             ids = {};
/* 1142 */             idxs = {};
/* 1143 */         };
/* 1144 */ 
/* 1145 */         // link the display function
/* 1146 */         self.filter = function (f) {
/* 1147 */             ffilter = f;
/* 1148 */             redraw();
/* 1149 */         };
/* 1150 */ 

/* gmap3.js */

/* 1151 */         // enable/disable the clustering feature
/* 1152 */         self.enable = function (value) {
/* 1153 */             if (enabled !== value) {
/* 1154 */                 enabled = value;
/* 1155 */                 redraw();
/* 1156 */             }
/* 1157 */         };
/* 1158 */ 
/* 1159 */         // link the display function
/* 1160 */         self.display = function (f) {
/* 1161 */             fdisplay = f;
/* 1162 */         };
/* 1163 */ 
/* 1164 */         // link the errorfunction
/* 1165 */         self.error = function (f) {
/* 1166 */             ferror = f;
/* 1167 */         };
/* 1168 */ 
/* 1169 */         // lock the redraw
/* 1170 */         self.beginUpdate = function () {
/* 1171 */             updating = true;
/* 1172 */         };
/* 1173 */ 
/* 1174 */         // unlock the redraw
/* 1175 */         self.endUpdate = function () {
/* 1176 */             updating = false;
/* 1177 */             if (updated) {
/* 1178 */                 redraw();
/* 1179 */             }
/* 1180 */         };
/* 1181 */ 
/* 1182 */         // extends current bounds with internal markers
/* 1183 */         self.autofit = function (bounds) {
/* 1184 */             var i;
/* 1185 */             for (i = 0; i < tds.length; i++) {
/* 1186 */                 if (tds[i]) {
/* 1187 */                     bounds.extend(tds[i].options.position);
/* 1188 */                 }
/* 1189 */             }
/* 1190 */         };
/* 1191 */ 
/* 1192 */         // bind events
/* 1193 */         function main() {
/* 1194 */             projection = overlay.getProjection();
/* 1195 */             if (!projection) {
/* 1196 */                 setTimeout(function () { main.apply(self, []); }, 25);
/* 1197 */                 return;
/* 1198 */             }
/* 1199 */             ready = true;
/* 1200 */             events.push(gm.event.addListener(map, "zoom_changed", delayRedraw));

/* gmap3.js */

/* 1201 */             events.push(gm.event.addListener(map, "bounds_changed", delayRedraw));
/* 1202 */             redraw();
/* 1203 */         }
/* 1204 */ 
/* 1205 */         // flush overlays
/* 1206 */         function flush(key) {
/* 1207 */             if (isObject(store[key])) { // is overlay
/* 1208 */                 if (isFunction(store[key].obj.setMap)) {
/* 1209 */                     store[key].obj.setMap(null);
/* 1210 */                 }
/* 1211 */                 if (isFunction(store[key].obj.remove)) {
/* 1212 */                     store[key].obj.remove();
/* 1213 */                 }
/* 1214 */                 if (isFunction(store[key].shadow.remove)) {
/* 1215 */                     store[key].obj.remove();
/* 1216 */                 }
/* 1217 */                 if (isFunction(store[key].shadow.setMap)) {
/* 1218 */                     store[key].shadow.setMap(null);
/* 1219 */                 }
/* 1220 */                 delete store[key].obj;
/* 1221 */                 delete store[key].shadow;
/* 1222 */             } else if (markers[key]) { // marker not removed
/* 1223 */                 markers[key].setMap(null);
/* 1224 */                 // don't remove the marker object, it may be displayed later
/* 1225 */             }
/* 1226 */             delete store[key];
/* 1227 */         }
/* 1228 */ 
/* 1229 */         /**
/* 1230 *|          * return the distance between 2 latLng couple into meters
/* 1231 *|          * Params :
/* 1232 *|          *  Lat1, Lng1, Lat2, Lng2
/* 1233 *|          *  LatLng1, Lat2, Lng2
/* 1234 *|          *  Lat1, Lng1, LatLng2
/* 1235 *|          *  LatLng1, LatLng2
/* 1236 *|          **/
/* 1237 */         function distanceInMeter() {
/* 1238 */             var lat1, lat2, lng1, lng2, e, f, g, h,
/* 1239 */                 cos = Math.cos,
/* 1240 */                 sin = Math.sin,
/* 1241 */                 args = arguments;
/* 1242 */             if (args[0] instanceof gm.LatLng) {
/* 1243 */                 lat1 = args[0].lat();
/* 1244 */                 lng1 = args[0].lng();
/* 1245 */                 if (args[1] instanceof gm.LatLng) {
/* 1246 */                     lat2 = args[1].lat();
/* 1247 */                     lng2 = args[1].lng();
/* 1248 */                 } else {
/* 1249 */                     lat2 = args[1];
/* 1250 */                     lng2 = args[2];

/* gmap3.js */

/* 1251 */                 }
/* 1252 */             } else {
/* 1253 */                 lat1 = args[0];
/* 1254 */                 lng1 = args[1];
/* 1255 */                 if (args[2] instanceof gm.LatLng) {
/* 1256 */                     lat2 = args[2].lat();
/* 1257 */                     lng2 = args[2].lng();
/* 1258 */                 } else {
/* 1259 */                     lat2 = args[2];
/* 1260 */                     lng2 = args[3];
/* 1261 */                 }
/* 1262 */             }
/* 1263 */             e = Math.PI * lat1 / 180;
/* 1264 */             f = Math.PI * lng1 / 180;
/* 1265 */             g = Math.PI * lat2 / 180;
/* 1266 */             h = Math.PI * lng2 / 180;
/* 1267 */             return 1000 * 6371 * Math.acos(Math.min(cos(e) * cos(g) * cos(f) * cos(h) + cos(e) * sin(f) * cos(g) * sin(h) + sin(e) * sin(g), 1));
/* 1268 */         }
/* 1269 */ 
/* 1270 */         // extend the visible bounds
/* 1271 */         function extendsMapBounds() {
/* 1272 */             var radius = distanceInMeter(map.getCenter(), map.getBounds().getNorthEast()),
/* 1273 */                 circle = new gm.Circle({
/* 1274 */                     center: map.getCenter(),
/* 1275 */                     radius: 1.25 * radius // + 25%
/* 1276 */                 });
/* 1277 */             return circle.getBounds();
/* 1278 */         }
/* 1279 */ 
/* 1280 */         // return an object where keys are store keys
/* 1281 */         function getStoreKeys() {
/* 1282 */             var k,
/* 1283 */                 keys = {};
/* 1284 */             for (k in store) {
/* 1285 */                 keys[k] = true;
/* 1286 */             }
/* 1287 */             return keys;
/* 1288 */         }
/* 1289 */ 
/* 1290 */         // async the delay function
/* 1291 */         function delayRedraw() {
/* 1292 */             clearTimeout(timer);
/* 1293 */             timer = setTimeout(redraw, 25);
/* 1294 */         }
/* 1295 */ 
/* 1296 */         // generate bounds extended by radius
/* 1297 */         function extendsBounds(latLng) {
/* 1298 */             var p = projection.fromLatLngToDivPixel(latLng),
/* 1299 */                 ne = projection.fromDivPixelToLatLng(new gm.Point(p.x + raw.radius, p.y - raw.radius)),
/* 1300 */                 sw = projection.fromDivPixelToLatLng(new gm.Point(p.x - raw.radius, p.y + raw.radius));

/* gmap3.js */

/* 1301 */             return new gm.LatLngBounds(sw, ne);
/* 1302 */         }
/* 1303 */ 
/* 1304 */         // run the clustering process and call the display function
/* 1305 */         function redraw() {
/* 1306 */             if (updating || redrawing || !ready) {
/* 1307 */                 return;
/* 1308 */             }
/* 1309 */ 
/* 1310 */             var i, j, k, indexes, check = false, bounds, cluster, position, previous, lat, lng, loop,
/* 1311 */                 keys = [],
/* 1312 */                 used = {},
/* 1313 */                 zoom = map.getZoom(),
/* 1314 */                 forceDisabled = ("maxZoom" in raw) && (zoom > raw.maxZoom),
/* 1315 */                 previousKeys = getStoreKeys();
/* 1316 */ 
/* 1317 */             // reset flag
/* 1318 */             updated = false;
/* 1319 */ 
/* 1320 */             if (zoom > 3) {
/* 1321 */                 // extend the bounds of the visible map to manage clusters near the boundaries
/* 1322 */                 bounds = extendsMapBounds();
/* 1323 */ 
/* 1324 */                 // check contain only if boundaries are valid
/* 1325 */                 check = bounds.getSouthWest().lng() < bounds.getNorthEast().lng();
/* 1326 */             }
/* 1327 */ 
/* 1328 */             // calculate positions of "visibles" markers (in extended bounds)
/* 1329 */             for (i = 0; i < tds.length; i++) {
/* 1330 */                 if (tds[i] && (!check || bounds.contains(tds[i].options.position)) && (!ffilter || ffilter(values[i]))) {
/* 1331 */                     keys.push(i);
/* 1332 */                 }
/* 1333 */             }
/* 1334 */ 
/* 1335 */             // for each "visible" marker, search its neighbors to create a cluster
/* 1336 */             // we can't do a classical "for" loop, because, analysis can bypass a marker while focusing on cluster
/* 1337 */             while (1) {
/* 1338 */                 i = 0;
/* 1339 */                 while (used[i] && (i < keys.length)) { // look for the next marker not used
/* 1340 */                     i++;
/* 1341 */                 }
/* 1342 */                 if (i === keys.length) {
/* 1343 */                     break;
/* 1344 */                 }
/* 1345 */ 
/* 1346 */                 indexes = [];
/* 1347 */ 
/* 1348 */                 if (enabled && !forceDisabled) {
/* 1349 */                     loop = 10;
/* 1350 */                     do {

/* gmap3.js */

/* 1351 */                         previous = indexes;
/* 1352 */                         indexes = [];
/* 1353 */                         loop--;
/* 1354 */ 
/* 1355 */                         if (previous.length) {
/* 1356 */                             position = bounds.getCenter();
/* 1357 */                         } else {
/* 1358 */                             position = tds[keys[i]].options.position;
/* 1359 */                         }
/* 1360 */                         bounds = extendsBounds(position);
/* 1361 */ 
/* 1362 */                         for (j = i; j < keys.length; j++) {
/* 1363 */                             if (used[j]) {
/* 1364 */                                 continue;
/* 1365 */                             }
/* 1366 */                             if (bounds.contains(tds[keys[j]].options.position)) {
/* 1367 */                                 indexes.push(j);
/* 1368 */                             }
/* 1369 */                         }
/* 1370 */                     } while ((previous.length < indexes.length) && (indexes.length > 1) && loop);
/* 1371 */                 } else {
/* 1372 */                     for (j = i; j < keys.length; j++) {
/* 1373 */                         if (!used[j]) {
/* 1374 */                             indexes.push(j);
/* 1375 */                             break;
/* 1376 */                         }
/* 1377 */                     }
/* 1378 */                 }
/* 1379 */ 
/* 1380 */                 cluster = {indexes: [], ref: []};
/* 1381 */                 lat = lng = 0;
/* 1382 */                 for (k = 0; k < indexes.length; k++) {
/* 1383 */                     used[indexes[k]] = true;
/* 1384 */                     cluster.indexes.push(keys[indexes[k]]);
/* 1385 */                     cluster.ref.push(keys[indexes[k]]);
/* 1386 */                     lat += tds[keys[indexes[k]]].options.position.lat();
/* 1387 */                     lng += tds[keys[indexes[k]]].options.position.lng();
/* 1388 */                 }
/* 1389 */                 lat /= indexes.length;
/* 1390 */                 lng /= indexes.length;
/* 1391 */                 cluster.latLng = new gm.LatLng(lat, lng);
/* 1392 */ 
/* 1393 */                 cluster.ref = cluster.ref.join("-");
/* 1394 */ 
/* 1395 */                 if (cluster.ref in previousKeys) { // cluster doesn't change
/* 1396 */                     delete previousKeys[cluster.ref]; // remove this entry, these still in this array will be removed
/* 1397 */                 } else { // cluster is new
/* 1398 */                     if (indexes.length === 1) { // alone markers are not stored, so need to keep the key (else, will be displayed every time and marker will blink)
/* 1399 */                         store[cluster.ref] = true;
/* 1400 */                     }

/* gmap3.js */

/* 1401 */                     fdisplay(cluster);
/* 1402 */                 }
/* 1403 */             }
/* 1404 */ 
/* 1405 */             // flush the previous overlays which are not still used
/* 1406 */             $.each(previousKeys, function (key) {
/* 1407 */                 flush(key);
/* 1408 */             });
/* 1409 */             redrawing = false;
/* 1410 */         }
/* 1411 */     }
/* 1412 */     /**
/* 1413 *|      * Class Clusterer
/* 1414 *|      * a facade with limited method for external use
/* 1415 *|      **/
/* 1416 */     function Clusterer(id, internalClusterer) {
/* 1417 */         var self = this;
/* 1418 */         self.id = function () {
/* 1419 */             return id;
/* 1420 */         };
/* 1421 */         self.filter = function (f) {
/* 1422 */             internalClusterer.filter(f);
/* 1423 */         };
/* 1424 */         self.enable = function () {
/* 1425 */             internalClusterer.enable(true);
/* 1426 */         };
/* 1427 */         self.disable = function () {
/* 1428 */             internalClusterer.enable(false);
/* 1429 */         };
/* 1430 */         self.add = function (marker, td, lock) {
/* 1431 */             if (!lock) {
/* 1432 */                 internalClusterer.beginUpdate();
/* 1433 */             }
/* 1434 */             internalClusterer.addMarker(marker, td);
/* 1435 */             if (!lock) {
/* 1436 */                 internalClusterer.endUpdate();
/* 1437 */             }
/* 1438 */         };
/* 1439 */         self.getById = function (id) {
/* 1440 */             return internalClusterer.getById(id);
/* 1441 */         };
/* 1442 */         self.clearById = function (id, lock) {
/* 1443 */             var result;
/* 1444 */             if (!lock) {
/* 1445 */                 internalClusterer.beginUpdate();
/* 1446 */             }
/* 1447 */             result = internalClusterer.clearById(id);
/* 1448 */             if (!lock) {
/* 1449 */                 internalClusterer.endUpdate();
/* 1450 */             }

/* gmap3.js */

/* 1451 */             return result;
/* 1452 */         };
/* 1453 */         self.clear = function (last, first, tag, lock) {
/* 1454 */             if (!lock) {
/* 1455 */                 internalClusterer.beginUpdate();
/* 1456 */             }
/* 1457 */             internalClusterer.clear(last, first, tag);
/* 1458 */             if (!lock) {
/* 1459 */                 internalClusterer.endUpdate();
/* 1460 */             }
/* 1461 */         };
/* 1462 */     }
/* 1463 */ 
/* 1464 */     /**
/* 1465 *|      * Class OverlayView
/* 1466 *|      * @constructor
/* 1467 *|      */
/* 1468 */     function OverlayView(map, opts, latLng, $div) {
/* 1469 */         var self = this,
/* 1470 */             listeners = [];
/* 1471 */ 
/* 1472 */         defaults.classes.OverlayView.call(self);
/* 1473 */         self.setMap(map);
/* 1474 */ 
/* 1475 */         self.onAdd = function () {
/* 1476 */             var panes = self.getPanes();
/* 1477 */             if (opts.pane in panes) {
/* 1478 */                 $(panes[opts.pane]).append($div);
/* 1479 */             }
/* 1480 */             $.each("dblclick click mouseover mousemove mouseout mouseup mousedown".split(" "), function (i, name) {
/* 1481 */                 listeners.push(
/* 1482 */                     gm.event.addDomListener($div[0], name, function (e) {
/* 1483 */                         $.Event(e).stopPropagation();
/* 1484 */                         gm.event.trigger(self, name, [e]);
/* 1485 */                         self.draw();
/* 1486 */                     })
/* 1487 */                 );
/* 1488 */             });
/* 1489 */             listeners.push(
/* 1490 */                 gm.event.addDomListener($div[0], "contextmenu", function (e) {
/* 1491 */                     $.Event(e).stopPropagation();
/* 1492 */                     gm.event.trigger(self, "rightclick", [e]);
/* 1493 */                     self.draw();
/* 1494 */                 })
/* 1495 */             );
/* 1496 */         };
/* 1497 */ 
/* 1498 */         self.getPosition = function () {
/* 1499 */             return latLng;
/* 1500 */         };

/* gmap3.js */

/* 1501 */ 
/* 1502 */         self.setPosition = function (newLatLng) {
/* 1503 */             latLng = newLatLng;
/* 1504 */             self.draw();
/* 1505 */         };
/* 1506 */ 
/* 1507 */         self.draw = function () {
/* 1508 */             var ps = self.getProjection().fromLatLngToDivPixel(latLng);
/* 1509 */             $div
/* 1510 */                 .css("left", (ps.x + opts.offset.x) + "px")
/* 1511 */                 .css("top", (ps.y + opts.offset.y) + "px");
/* 1512 */         };
/* 1513 */ 
/* 1514 */         self.onRemove = function () {
/* 1515 */             var i;
/* 1516 */             for (i = 0; i < listeners.length; i++) {
/* 1517 */                 gm.event.removeListener(listeners[i]);
/* 1518 */             }
/* 1519 */             $div.remove();
/* 1520 */         };
/* 1521 */ 
/* 1522 */         self.hide = function () {
/* 1523 */             $div.hide();
/* 1524 */         };
/* 1525 */ 
/* 1526 */         self.show = function () {
/* 1527 */             $div.show();
/* 1528 */         };
/* 1529 */ 
/* 1530 */         self.toggle = function () {
/* 1531 */             if ($div) {
/* 1532 */                 if ($div.is(":visible")) {
/* 1533 */                     self.show();
/* 1534 */                 } else {
/* 1535 */                     self.hide();
/* 1536 */                 }
/* 1537 */             }
/* 1538 */         };
/* 1539 */ 
/* 1540 */         self.toggleDOM = function () {
/* 1541 */             self.setMap(self.getMap() ? null : map);
/* 1542 */         };
/* 1543 */ 
/* 1544 */         self.getDOMElement = function () {
/* 1545 */             return $div[0];
/* 1546 */         };
/* 1547 */     }
/* 1548 */ 
/* 1549 */     function Gmap3($this) {
/* 1550 */         var self = this,

/* gmap3.js */

/* 1551 */             stack = new Stack(),
/* 1552 */             store = new Store(),
/* 1553 */             map = null,
/* 1554 */             task;
/* 1555 */ 
/* 1556 */         /**
/* 1557 *|          * if not running, start next action in stack
/* 1558 *|          **/
/* 1559 */         function run() {
/* 1560 */             if (!task && (task = stack.get())) {
/* 1561 */                 task.run();
/* 1562 */             }
/* 1563 */         }
/* 1564 */ 
/* 1565 */         /**
/* 1566 *|          * called when action in finished, to acknoledge the current in stack and start next one
/* 1567 *|          **/
/* 1568 */         function end() {
/* 1569 */             task = null;
/* 1570 */             stack.ack();
/* 1571 */             run.call(self); // restart to high level scope
/* 1572 */         }
/* 1573 */ 
/* 1574 */ //-----------------------------------------------------------------------//
/* 1575 */ // Tools
/* 1576 */ //-----------------------------------------------------------------------//
/* 1577 */ 
/* 1578 */         /**
/* 1579 *|          * execute callback functions
/* 1580 *|          **/
/* 1581 */         function callback(args) {
/* 1582 */             var params,
/* 1583 */                 cb = args.td.callback;
/* 1584 */             if (cb) {
/* 1585 */                 params = Array.prototype.slice.call(arguments, 1);
/* 1586 */                 if (isFunction(cb)) {
/* 1587 */                     cb.apply($this, params);
/* 1588 */                 } else if (isArray(cb)) {
/* 1589 */                     if (isFunction(cb[1])) {
/* 1590 */                         cb[1].apply(cb[0], params);
/* 1591 */                     }
/* 1592 */                 }
/* 1593 */             }
/* 1594 */         }
/* 1595 */ 
/* 1596 */         /**
/* 1597 *|          * execute ending functions
/* 1598 *|          **/
/* 1599 */         function manageEnd(args, obj, id) {
/* 1600 */             if (id) {

/* gmap3.js */

/* 1601 */                 attachEvents($this, args, obj, id);
/* 1602 */             }
/* 1603 */             callback(args, obj);
/* 1604 */             task.ack(obj);
/* 1605 */         }
/* 1606 */ 
/* 1607 */         /**
/* 1608 *|          * initialize the map if not yet initialized
/* 1609 *|          **/
/* 1610 */         function newMap(latLng, args) {
/* 1611 */             args = args || {};
/* 1612 */             var opts = args.td && args.td.options ? args.td.options : 0;
/* 1613 */             if (map) {
/* 1614 */                 if (opts) {
/* 1615 */                     if (opts.center) {
/* 1616 */                         opts.center = toLatLng(opts.center);
/* 1617 */                     }
/* 1618 */                     map.setOptions(opts);
/* 1619 */                 }
/* 1620 */             } else {
/* 1621 */                 opts = args.opts || $.extend(true, {}, defaults.map, opts || {});
/* 1622 */                 opts.center = latLng || toLatLng(opts.center);
/* 1623 */                 map = new defaults.classes.Map($this.get(0), opts);
/* 1624 */             }
/* 1625 */         }
/* 1626 */ 
/* 1627 */         /**
/* 1628 *|          * store actions to execute in a stack manager
/* 1629 *|          **/
/* 1630 */         self._plan = function (list) {
/* 1631 */             var k;
/* 1632 */             for (k = 0; k < list.length; k++) {
/* 1633 */                 stack.add(new Task(self, end, list[k]));
/* 1634 */             }
/* 1635 */             run();
/* 1636 */         };
/* 1637 */ 
/* 1638 */         /**
/* 1639 *|          * Initialize gm.Map object
/* 1640 *|          **/
/* 1641 */         self.map = function (args) {
/* 1642 */             newMap(args.latLng, args);
/* 1643 */             attachEvents($this, args, map);
/* 1644 */             manageEnd(args, map);
/* 1645 */         };
/* 1646 */ 
/* 1647 */         /**
/* 1648 *|          * destroy an existing instance
/* 1649 *|          **/
/* 1650 */         self.destroy = function (args) {

/* gmap3.js */

/* 1651 */             store.clear();
/* 1652 */             $this.empty();
/* 1653 */             if (map) {
/* 1654 */                 map = null;
/* 1655 */             }
/* 1656 */             manageEnd(args, true);
/* 1657 */         };
/* 1658 */ 
/* 1659 */         /**
/* 1660 *|          * add an overlay
/* 1661 *|          **/
/* 1662 */         self.overlay = function (args, internal) {
/* 1663 */             var objs = [],
/* 1664 */                 multiple = "values" in args.td;
/* 1665 */             if (!multiple) {
/* 1666 */                 args.td.values = [{latLng: args.latLng, options: args.opts}];
/* 1667 */             }
/* 1668 */             if (!args.td.values.length) {
/* 1669 */                 manageEnd(args, false);
/* 1670 */                 return;
/* 1671 */             }
/* 1672 */             if (!OverlayView.__initialised) {
/* 1673 */                 OverlayView.prototype = new defaults.classes.OverlayView();
/* 1674 */                 OverlayView.__initialised = true;
/* 1675 */             }
/* 1676 */             $.each(args.td.values, function (i, value) {
/* 1677 */                 var id, obj, td = tuple(args, value),
/* 1678 */                     $div = $(document.createElement("div")).css({
/* 1679 */                         border: "none",
/* 1680 */                         borderWidth: 0,
/* 1681 */                         position: "absolute"
/* 1682 */                     });
/* 1683 */                 $div.append(td.options.content);
/* 1684 */                 obj = new OverlayView(map, td.options, toLatLng(td) || toLatLng(value), $div);
/* 1685 */                 objs.push(obj);
/* 1686 */                 $div = null; // memory leak
/* 1687 */                 if (!internal) {
/* 1688 */                     id = store.add(args, "overlay", obj);
/* 1689 */                     attachEvents($this, {td: td}, obj, id);
/* 1690 */                 }
/* 1691 */             });
/* 1692 */             if (internal) {
/* 1693 */                 return objs[0];
/* 1694 */             }
/* 1695 */             manageEnd(args, multiple ? objs : objs[0]);
/* 1696 */         };
/* 1697 */ 
/* 1698 */         /**
/* 1699 *|          * Create an InternalClusterer object
/* 1700 *|          **/

/* gmap3.js */

/* 1701 */         function createClusterer(raw) {
/* 1702 */             var internalClusterer = new InternalClusterer($this, map, raw),
/* 1703 */                 td = {},
/* 1704 */                 styles = {},
/* 1705 */                 thresholds = [],
/* 1706 */                 isInt = /^[0-9]+$/,
/* 1707 */                 calculator,
/* 1708 */                 k;
/* 1709 */ 
/* 1710 */             for (k in raw) {
/* 1711 */                 if (isInt.test(k)) {
/* 1712 */                     thresholds.push(1 * k); // cast to int
/* 1713 */                     styles[k] = raw[k];
/* 1714 */                     styles[k].width = styles[k].width || 0;
/* 1715 */                     styles[k].height = styles[k].height || 0;
/* 1716 */                 } else {
/* 1717 */                     td[k] = raw[k];
/* 1718 */                 }
/* 1719 */             }
/* 1720 */             thresholds.sort(function (a, b) { return a > b; });
/* 1721 */ 
/* 1722 */             // external calculator
/* 1723 */             if (td.calculator) {
/* 1724 */                 calculator = function (indexes) {
/* 1725 */                     var data = [];
/* 1726 */                     $.each(indexes, function (i, index) {
/* 1727 */                         data.push(internalClusterer.value(index));
/* 1728 */                     });
/* 1729 */                     return td.calculator.apply($this, [data]);
/* 1730 */                 };
/* 1731 */             } else {
/* 1732 */                 calculator = function (indexes) {
/* 1733 */                     return indexes.length;
/* 1734 */                 };
/* 1735 */             }
/* 1736 */ 
/* 1737 */             // set error function
/* 1738 */             internalClusterer.error(function () {
/* 1739 */                 error.apply(self, arguments);
/* 1740 */             });
/* 1741 */ 
/* 1742 */             // set display function
/* 1743 */             internalClusterer.display(function (cluster) {
/* 1744 */                 var i, style, atd, obj, offset, shadow,
/* 1745 */                     cnt = calculator(cluster.indexes);
/* 1746 */ 
/* 1747 */                 // look for the style to use
/* 1748 */                 if (raw.force || cnt > 1) {
/* 1749 */                     for (i = 0; i < thresholds.length; i++) {
/* 1750 */                         if (thresholds[i] <= cnt) {

/* gmap3.js */

/* 1751 */                             style = styles[thresholds[i]];
/* 1752 */                         }
/* 1753 */                     }
/* 1754 */                 }
/* 1755 */ 
/* 1756 */                 if (style) {
/* 1757 */                     offset = style.offset || [-style.width/2, -style.height/2];
/* 1758 */                     // create a custom overlay command
/* 1759 */                     // nb: 2 extends are faster self a deeper extend
/* 1760 */                     atd = $.extend({}, td);
/* 1761 */                     atd.options = $.extend({
/* 1762 */                             pane: "overlayLayer",
/* 1763 */                             content: style.content ? style.content.replace("CLUSTER_COUNT", cnt) : "",
/* 1764 */                             offset: {
/* 1765 */                                 x: ("x" in offset ? offset.x : offset[0]) || 0,
/* 1766 */                                 y: ("y" in offset ? offset.y : offset[1]) || 0
/* 1767 */                             }
/* 1768 */                         },
/* 1769 */                         td.options || {});
/* 1770 */ 
/* 1771 */                     obj = self.overlay({td: atd, opts: atd.options, latLng: toLatLng(cluster)}, true);
/* 1772 */ 
/* 1773 */                     atd.options.pane = "floatShadow";
/* 1774 */                     atd.options.content = $(document.createElement("div")).width(style.width + "px").height(style.height + "px").css({cursor: "pointer"});
/* 1775 */                     shadow = self.overlay({td: atd, opts: atd.options, latLng: toLatLng(cluster)}, true);
/* 1776 */ 
/* 1777 */                     // store data to the clusterer
/* 1778 */                     td.data = {
/* 1779 */                         latLng: toLatLng(cluster),
/* 1780 */                         markers:[]
/* 1781 */                     };
/* 1782 */                     $.each(cluster.indexes, function(i, index){
/* 1783 */                         td.data.markers.push(internalClusterer.value(index));
/* 1784 */                         if (internalClusterer.markerIsSet(index)){
/* 1785 */                             internalClusterer.marker(index).setMap(null);
/* 1786 */                         }
/* 1787 */                     });
/* 1788 */                     attachEvents($this, {td: td}, shadow, undef, {main: obj, shadow: shadow});
/* 1789 */                     internalClusterer.store(cluster, obj, shadow);
/* 1790 */                 } else {
/* 1791 */                     $.each(cluster.indexes, function (i, index) {
/* 1792 */                         internalClusterer.marker(index).setMap(map);
/* 1793 */                     });
/* 1794 */                 }
/* 1795 */             });
/* 1796 */ 
/* 1797 */             return internalClusterer;
/* 1798 */         }
/* 1799 */ 
/* 1800 */         /**

/* gmap3.js */

/* 1801 *|          *  add a marker
/* 1802 *|          **/
/* 1803 */         self.marker = function (args) {
/* 1804 */             var objs,
/* 1805 */                 clusterer, internalClusterer,
/* 1806 */                 multiple = "values" in args.td,
/* 1807 */                 init = !map;
/* 1808 */             if (!multiple) {
/* 1809 */                 args.opts.position = args.latLng || toLatLng(args.opts.position);
/* 1810 */                 args.td.values = [{options: args.opts}];
/* 1811 */             }
/* 1812 */             if (!args.td.values.length) {
/* 1813 */                 manageEnd(args, false);
/* 1814 */                 return;
/* 1815 */             }
/* 1816 */             if (init) {
/* 1817 */                 newMap();
/* 1818 */             }
/* 1819 */             if (args.td.cluster && !map.getBounds()) { // map not initialised => bounds not available : wait for map if clustering feature is required
/* 1820 */                 gm.event.addListenerOnce(map, "bounds_changed", function () { self.marker.apply(self, [args]); });
/* 1821 */                 return;
/* 1822 */             }
/* 1823 */             if (args.td.cluster) {
/* 1824 */                 if (args.td.cluster instanceof Clusterer) {
/* 1825 */                     clusterer = args.td.cluster;
/* 1826 */                     internalClusterer = store.getById(clusterer.id(), true);
/* 1827 */                 } else {
/* 1828 */                     internalClusterer = createClusterer(args.td.cluster);
/* 1829 */                     clusterer = new Clusterer(globalId(args.td.id, true), internalClusterer);
/* 1830 */                     store.add(args, "clusterer", clusterer, internalClusterer);
/* 1831 */                 }
/* 1832 */                 internalClusterer.beginUpdate();
/* 1833 */ 
/* 1834 */                 $.each(args.td.values, function (i, value) {
/* 1835 */                     var td = tuple(args, value);
/* 1836 */                     td.options.position = td.options.position ? toLatLng(td.options.position) : toLatLng(value);
/* 1837 */                     if (td.options.position) {
/* 1838 */                         td.options.map = map;
/* 1839 */                         if (init) {
/* 1840 */                             map.setCenter(td.options.position);
/* 1841 */                             init = false;
/* 1842 */                         }
/* 1843 */                         internalClusterer.add(td, value);
/* 1844 */                     }
/* 1845 */                 });
/* 1846 */ 
/* 1847 */                 internalClusterer.endUpdate();
/* 1848 */                 manageEnd(args, clusterer);
/* 1849 */ 
/* 1850 */             } else {

/* gmap3.js */

/* 1851 */                 objs = [];
/* 1852 */                 $.each(args.td.values, function (i, value) {
/* 1853 */                     var id, obj,
/* 1854 */                         td = tuple(args, value);
/* 1855 */                     td.options.position = td.options.position ? toLatLng(td.options.position) : toLatLng(value);
/* 1856 */                     if (td.options.position) {
/* 1857 */                         td.options.map = map;
/* 1858 */                         if (init) {
/* 1859 */                             map.setCenter(td.options.position);
/* 1860 */                             init = false;
/* 1861 */                         }
/* 1862 */                         obj = new defaults.classes.Marker(td.options);
/* 1863 */                         objs.push(obj);
/* 1864 */                         id = store.add({td: td}, "marker", obj);
/* 1865 */                         attachEvents($this, {td: td}, obj, id);
/* 1866 */                     }
/* 1867 */                 });
/* 1868 */                 manageEnd(args, multiple ? objs : objs[0]);
/* 1869 */             }
/* 1870 */         };
/* 1871 */ 
/* 1872 */         /**
/* 1873 *|          * return a route
/* 1874 *|          **/
/* 1875 */         self.getroute = function (args) {
/* 1876 */             args.opts.origin = toLatLng(args.opts.origin, true);
/* 1877 */             args.opts.destination = toLatLng(args.opts.destination, true);
/* 1878 */             directionsService().route(
/* 1879 */                 args.opts,
/* 1880 */                 function (results, status) {
/* 1881 */                     callback(args, status === gm.DirectionsStatus.OK ? results : false, status);
/* 1882 */                     task.ack();
/* 1883 */                 }
/* 1884 */             );
/* 1885 */         };
/* 1886 */ 
/* 1887 */         /**
/* 1888 *|          * return the distance between an origin and a destination
/* 1889 *|          *
/* 1890 *|          **/
/* 1891 */         self.getdistance = function (args) {
/* 1892 */             var i;
/* 1893 */             args.opts.origins = array(args.opts.origins);
/* 1894 */             for (i = 0; i < args.opts.origins.length; i++) {
/* 1895 */                 args.opts.origins[i] = toLatLng(args.opts.origins[i], true);
/* 1896 */             }
/* 1897 */             args.opts.destinations = array(args.opts.destinations);
/* 1898 */             for (i = 0; i < args.opts.destinations.length; i++) {
/* 1899 */                 args.opts.destinations[i] = toLatLng(args.opts.destinations[i], true);
/* 1900 */             }

/* gmap3.js */

/* 1901 */             distanceMatrixService().getDistanceMatrix(
/* 1902 */                 args.opts,
/* 1903 */                 function (results, status) {
/* 1904 */                     callback(args, status === gm.DistanceMatrixStatus.OK ? results : false, status);
/* 1905 */                     task.ack();
/* 1906 */                 }
/* 1907 */             );
/* 1908 */         };
/* 1909 */ 
/* 1910 */         /**
/* 1911 *|          * add an infowindow
/* 1912 *|          **/
/* 1913 */         self.infowindow = function (args) {
/* 1914 */             var objs = [],
/* 1915 */                 multiple = "values" in args.td;
/* 1916 */             if (!multiple) {
/* 1917 */                 if (args.latLng) {
/* 1918 */                     args.opts.position = args.latLng;
/* 1919 */                 }
/* 1920 */                 args.td.values = [{options: args.opts}];
/* 1921 */             }
/* 1922 */             $.each(args.td.values, function (i, value) {
/* 1923 */                 var id, obj,
/* 1924 */                     td = tuple(args, value);
/* 1925 */                 td.options.position = td.options.position ? toLatLng(td.options.position) : toLatLng(value.latLng);
/* 1926 */                 if (!map) {
/* 1927 */                     newMap(td.options.position);
/* 1928 */                 }
/* 1929 */                 obj = new defaults.classes.InfoWindow(td.options);
/* 1930 */                 if (obj && (isUndefined(td.open) || td.open)) {
/* 1931 */                     if (multiple) {
/* 1932 */                         obj.open(map, td.anchor || undef);
/* 1933 */                     } else {
/* 1934 */                         obj.open(map, td.anchor || (args.latLng ? undef : (args.session.marker ? args.session.marker : undef)));
/* 1935 */                     }
/* 1936 */                 }
/* 1937 */                 objs.push(obj);
/* 1938 */                 id = store.add({td: td}, "infowindow", obj);
/* 1939 */                 attachEvents($this, {td: td}, obj, id);
/* 1940 */             });
/* 1941 */             manageEnd(args, multiple ? objs : objs[0]);
/* 1942 */         };
/* 1943 */ 
/* 1944 */         /**
/* 1945 *|          * add a circle
/* 1946 *|          **/
/* 1947 */         self.circle = function (args) {
/* 1948 */             var objs = [],
/* 1949 */                 multiple = "values" in args.td;
/* 1950 */             if (!multiple) {

/* gmap3.js */

/* 1951 */                 args.opts.center = args.latLng || toLatLng(args.opts.center);
/* 1952 */                 args.td.values = [{options: args.opts}];
/* 1953 */             }
/* 1954 */             if (!args.td.values.length) {
/* 1955 */                 manageEnd(args, false);
/* 1956 */                 return;
/* 1957 */             }
/* 1958 */             $.each(args.td.values, function (i, value) {
/* 1959 */                 var id, obj,
/* 1960 */                     td = tuple(args, value);
/* 1961 */                 td.options.center = td.options.center ? toLatLng(td.options.center) : toLatLng(value);
/* 1962 */                 if (!map) {
/* 1963 */                     newMap(td.options.center);
/* 1964 */                 }
/* 1965 */                 td.options.map = map;
/* 1966 */                 obj = new defaults.classes.Circle(td.options);
/* 1967 */                 objs.push(obj);
/* 1968 */                 id = store.add({td: td}, "circle", obj);
/* 1969 */                 attachEvents($this, {td: td}, obj, id);
/* 1970 */             });
/* 1971 */             manageEnd(args, multiple ? objs : objs[0]);
/* 1972 */         };
/* 1973 */ 
/* 1974 */         /**
/* 1975 *|          * returns address structure from latlng
/* 1976 *|          **/
/* 1977 */         self.getaddress = function (args) {
/* 1978 */             callback(args, args.results, args.status);
/* 1979 */             task.ack();
/* 1980 */         };
/* 1981 */ 
/* 1982 */         /**
/* 1983 *|          * returns latlng from an address
/* 1984 *|          **/
/* 1985 */         self.getlatlng = function (args) {
/* 1986 */             callback(args, args.results, args.status);
/* 1987 */             task.ack();
/* 1988 */         };
/* 1989 */ 
/* 1990 */         /**
/* 1991 *|          * return the max zoom of a location
/* 1992 *|          **/
/* 1993 */         self.getmaxzoom = function (args) {
/* 1994 */             maxZoomService().getMaxZoomAtLatLng(
/* 1995 */                 args.latLng,
/* 1996 */                 function (result) {
/* 1997 */                     callback(args, result.status === gm.MaxZoomStatus.OK ? result.zoom : false, status);
/* 1998 */                     task.ack();
/* 1999 */                 }
/* 2000 */             );

/* gmap3.js */

/* 2001 */         };
/* 2002 */ 
/* 2003 */         /**
/* 2004 *|          * return the elevation of a location
/* 2005 *|          **/
/* 2006 */         self.getelevation = function (args) {
/* 2007 */             var i,
/* 2008 */                 locations = [],
/* 2009 */                 f = function (results, status) {
/* 2010 */                     callback(args, status === gm.ElevationStatus.OK ? results : false, status);
/* 2011 */                     task.ack();
/* 2012 */                 };
/* 2013 */ 
/* 2014 */             if (args.latLng) {
/* 2015 */                 locations.push(args.latLng);
/* 2016 */             } else {
/* 2017 */                 locations = array(args.td.locations || []);
/* 2018 */                 for (i = 0; i < locations.length; i++) {
/* 2019 */                     locations[i] = toLatLng(locations[i]);
/* 2020 */                 }
/* 2021 */             }
/* 2022 */             if (locations.length) {
/* 2023 */                 elevationService().getElevationForLocations({locations: locations}, f);
/* 2024 */             } else {
/* 2025 */                 if (args.td.path && args.td.path.length) {
/* 2026 */                     for (i = 0; i < args.td.path.length; i++) {
/* 2027 */                         locations.push(toLatLng(args.td.path[i]));
/* 2028 */                     }
/* 2029 */                 }
/* 2030 */                 if (locations.length) {
/* 2031 */                     elevationService().getElevationAlongPath({path: locations, samples:args.td.samples}, f);
/* 2032 */                 } else {
/* 2033 */                     task.ack();
/* 2034 */                 }
/* 2035 */             }
/* 2036 */         };
/* 2037 */ 
/* 2038 */         /**
/* 2039 *|          * define defaults values
/* 2040 *|          **/
/* 2041 */         self.defaults = function (args) {
/* 2042 */             $.each(args.td, function(name, value) {
/* 2043 */                 if (isObject(defaults[name])) {
/* 2044 */                     defaults[name] = $.extend({}, defaults[name], value);
/* 2045 */                 } else {
/* 2046 */                     defaults[name] = value;
/* 2047 */                 }
/* 2048 */             });
/* 2049 */             task.ack(true);
/* 2050 */         };

/* gmap3.js */

/* 2051 */ 
/* 2052 */         /**
/* 2053 *|          * add a rectangle
/* 2054 *|          **/
/* 2055 */         self.rectangle = function (args) {
/* 2056 */             var objs = [],
/* 2057 */                 multiple = "values" in args.td;
/* 2058 */             if (!multiple) {
/* 2059 */                 args.td.values = [{options: args.opts}];
/* 2060 */             }
/* 2061 */             if (!args.td.values.length) {
/* 2062 */                 manageEnd(args, false);
/* 2063 */                 return;
/* 2064 */             }
/* 2065 */             $.each(args.td.values, function (i, value) {
/* 2066 */                 var id, obj,
/* 2067 */                     td = tuple(args, value);
/* 2068 */                 td.options.bounds = td.options.bounds ? toLatLngBounds(td.options.bounds) : toLatLngBounds(value);
/* 2069 */                 if (!map) {
/* 2070 */                     newMap(td.options.bounds.getCenter());
/* 2071 */                 }
/* 2072 */                 td.options.map = map;
/* 2073 */ 
/* 2074 */                 obj = new defaults.classes.Rectangle(td.options);
/* 2075 */                 objs.push(obj);
/* 2076 */                 id = store.add({td: td}, "rectangle", obj);
/* 2077 */                 attachEvents($this, {td: td}, obj, id);
/* 2078 */             });
/* 2079 */             manageEnd(args, multiple ? objs : objs[0]);
/* 2080 */         };
/* 2081 */ 
/* 2082 */         /**
/* 2083 *|          * add a polygone / polyline
/* 2084 *|          **/
/* 2085 */         function poly(args, poly, path) {
/* 2086 */             var objs = [],
/* 2087 */                 multiple = "values" in args.td;
/* 2088 */             if (!multiple) {
/* 2089 */                 args.td.values = [{options: args.opts}];
/* 2090 */             }
/* 2091 */             if (!args.td.values.length) {
/* 2092 */                 manageEnd(args, false);
/* 2093 */                 return;
/* 2094 */             }
/* 2095 */             newMap();
/* 2096 */             $.each(args.td.values, function (_, value) {
/* 2097 */                 var id, i, j, obj,
/* 2098 */                     td = tuple(args, value);
/* 2099 */                 if (td.options[path]) {
/* 2100 */                     if (td.options[path][0][0] && isArray(td.options[path][0][0])) {

/* gmap3.js */

/* 2101 */                         for (i = 0; i < td.options[path].length; i++) {
/* 2102 */                             for (j = 0; j < td.options[path][i].length; j++) {
/* 2103 */                                 td.options[path][i][j] = toLatLng(td.options[path][i][j]);
/* 2104 */                             }
/* 2105 */                         }
/* 2106 */                     } else {
/* 2107 */                         for (i = 0; i < td.options[path].length; i++) {
/* 2108 */                             td.options[path][i] = toLatLng(td.options[path][i]);
/* 2109 */                         }
/* 2110 */                     }
/* 2111 */                 }
/* 2112 */                 td.options.map = map;
/* 2113 */                 obj = new gm[poly](td.options);
/* 2114 */                 objs.push(obj);
/* 2115 */                 id = store.add({td: td}, poly.toLowerCase(), obj);
/* 2116 */                 attachEvents($this, {td: td}, obj, id);
/* 2117 */             });
/* 2118 */             manageEnd(args, multiple ? objs : objs[0]);
/* 2119 */         }
/* 2120 */ 
/* 2121 */         self.polyline = function (args) {
/* 2122 */             poly(args, "Polyline", "path");
/* 2123 */         };
/* 2124 */ 
/* 2125 */         self.polygon = function (args) {
/* 2126 */             poly(args, "Polygon", "paths");
/* 2127 */         };
/* 2128 */ 
/* 2129 */         /**
/* 2130 *|          * add a traffic layer
/* 2131 *|          **/
/* 2132 */         self.trafficlayer = function (args) {
/* 2133 */             newMap();
/* 2134 */             var obj = store.get("trafficlayer");
/* 2135 */             if (!obj) {
/* 2136 */                 obj = new defaults.classes.TrafficLayer();
/* 2137 */                 obj.setMap(map);
/* 2138 */                 store.add(args, "trafficlayer", obj);
/* 2139 */             }
/* 2140 */             manageEnd(args, obj);
/* 2141 */         };
/* 2142 */ 
/* 2143 */         /**
/* 2144 *|          * add a bicycling layer
/* 2145 *|          **/
/* 2146 */         self.bicyclinglayer = function (args) {
/* 2147 */             newMap();
/* 2148 */             var obj = store.get("bicyclinglayer");
/* 2149 */             if (!obj) {
/* 2150 */                 obj = new defaults.classes.BicyclingLayer();

/* gmap3.js */

/* 2151 */                 obj.setMap(map);
/* 2152 */                 store.add(args, "bicyclinglayer", obj);
/* 2153 */             }
/* 2154 */             manageEnd(args, obj);
/* 2155 */         };
/* 2156 */ 
/* 2157 */         /**
/* 2158 *|          * add a ground overlay
/* 2159 *|          **/
/* 2160 */         self.groundoverlay = function (args) {
/* 2161 */             args.opts.bounds = toLatLngBounds(args.opts.bounds);
/* 2162 */             if (args.opts.bounds){
/* 2163 */                 newMap(args.opts.bounds.getCenter());
/* 2164 */             }
/* 2165 */             var id,
/* 2166 */                 obj = new defaults.classes.GroundOverlay(args.opts.url, args.opts.bounds, args.opts.opts);
/* 2167 */             obj.setMap(map);
/* 2168 */             id = store.add(args, "groundoverlay", obj);
/* 2169 */             manageEnd(args, obj, id);
/* 2170 */         };
/* 2171 */ 
/* 2172 */         /**
/* 2173 *|          * set a streetview
/* 2174 *|          **/
/* 2175 */         self.streetviewpanorama = function (args) {
/* 2176 */             if (!args.opts.opts) {
/* 2177 */                 args.opts.opts = {};
/* 2178 */             }
/* 2179 */             if (args.latLng) {
/* 2180 */                 args.opts.opts.position = args.latLng;
/* 2181 */             } else if (args.opts.opts.position) {
/* 2182 */                 args.opts.opts.position = toLatLng(args.opts.opts.position);
/* 2183 */             }
/* 2184 */             if (args.td.divId) {
/* 2185 */                 args.opts.container = document.getElementById(args.td.divId);
/* 2186 */             } else if (args.opts.container) {
/* 2187 */                 args.opts.container = $(args.opts.container).get(0);
/* 2188 */             }
/* 2189 */             var id, obj = new defaults.classes.StreetViewPanorama(args.opts.container, args.opts.opts);
/* 2190 */             if (obj) {
/* 2191 */                 map.setStreetView(obj);
/* 2192 */             }
/* 2193 */             id = store.add(args, "streetviewpanorama", obj);
/* 2194 */             manageEnd(args, obj, id);
/* 2195 */         };
/* 2196 */ 
/* 2197 */         self.kmllayer = function (args) {
/* 2198 */             var objs = [],
/* 2199 */                 multiple = "values" in args.td;
/* 2200 */             if (!multiple) {

/* gmap3.js */

/* 2201 */                 args.td.values = [{options: args.opts}];
/* 2202 */             }
/* 2203 */             if (!args.td.values.length) {
/* 2204 */                 manageEnd(args, false);
/* 2205 */                 return;
/* 2206 */             }
/* 2207 */             $.each(args.td.values, function (i, value) {
/* 2208 */                 var id, obj, options,
/* 2209 */                     td = tuple(args, value);
/* 2210 */                 if (!map) {
/* 2211 */                     newMap();
/* 2212 */                 }
/* 2213 */                 options = td.options;
/* 2214 */                 // compatibility 5.0-
/* 2215 */                 if (td.options.opts) {
/* 2216 */                     options = td.options.opts;
/* 2217 */                     if (td.options.url) {
/* 2218 */                         options.url = td.options.url;
/* 2219 */                     }
/* 2220 */                 }
/* 2221 */                 // -- end --
/* 2222 */                 options.map = map;
/* 2223 */                 if (googleVersionMin("3.10")) {
/* 2224 */                     obj = new defaults.classes.KmlLayer(options);
/* 2225 */                 } else {
/* 2226 */                     obj = new defaults.classes.KmlLayer(options.url, options);
/* 2227 */                 }
/* 2228 */                 objs.push(obj);
/* 2229 */                 id = store.add({td: td}, "kmllayer", obj);
/* 2230 */                 attachEvents($this, {td: td}, obj, id);
/* 2231 */             });
/* 2232 */             manageEnd(args, multiple ? objs : objs[0]);
/* 2233 */         };
/* 2234 */ 
/* 2235 */         /**
/* 2236 *|          * add a fix panel
/* 2237 *|          **/
/* 2238 */         self.panel = function (args) {
/* 2239 */             newMap();
/* 2240 */             var id, $content,
/* 2241 */                 x = 0,
/* 2242 */                 y = 0,
/* 2243 */                 $div = $(document.createElement("div"));
/* 2244 */ 
/* 2245 */             $div.css({
/* 2246 */                 position: "absolute",
/* 2247 */                 zIndex: 1000,
/* 2248 */                 visibility: "hidden"
/* 2249 */             });
/* 2250 */ 

/* gmap3.js */

/* 2251 */             if (args.opts.content) {
/* 2252 */                 $content = $(args.opts.content);
/* 2253 */                 $div.append($content);
/* 2254 */                 $this.first().prepend($div);
/* 2255 */ 
/* 2256 */                 if (!isUndefined(args.opts.left)) {
/* 2257 */                     x = args.opts.left;
/* 2258 */                 } else if (!isUndefined(args.opts.right)) {
/* 2259 */                     x = $this.width() - $content.width() - args.opts.right;
/* 2260 */                 } else if (args.opts.center) {
/* 2261 */                     x = ($this.width() - $content.width()) / 2;
/* 2262 */                 }
/* 2263 */ 
/* 2264 */                 if (!isUndefined(args.opts.top)) {
/* 2265 */                     y = args.opts.top;
/* 2266 */                 } else if (!isUndefined(args.opts.bottom)) {
/* 2267 */                     y = $this.height() - $content.height() - args.opts.bottom;
/* 2268 */                 } else if (args.opts.middle) {
/* 2269 */                     y = ($this.height() - $content.height()) / 2
/* 2270 */                 }
/* 2271 */ 
/* 2272 */                 $div.css({
/* 2273 */                     top: y,
/* 2274 */                     left: x,
/* 2275 */                     visibility: "visible"
/* 2276 */                 });
/* 2277 */             }
/* 2278 */ 
/* 2279 */             id = store.add(args, "panel", $div);
/* 2280 */             manageEnd(args, $div, id);
/* 2281 */             $div = null; // memory leak
/* 2282 */         };
/* 2283 */ 
/* 2284 */         /**
/* 2285 *|          * add a direction renderer
/* 2286 *|          **/
/* 2287 */         self.directionsrenderer = function (args) {
/* 2288 */             args.opts.map = map;
/* 2289 */             var id,
/* 2290 */                 obj = new gm.DirectionsRenderer(args.opts);
/* 2291 */             if (args.td.divId) {
/* 2292 */                 obj.setPanel(document.getElementById(args.td.divId));
/* 2293 */             } else if (args.td.container) {
/* 2294 */                 obj.setPanel($(args.td.container).get(0));
/* 2295 */             }
/* 2296 */             id = store.add(args, "directionsrenderer", obj);
/* 2297 */             manageEnd(args, obj, id);
/* 2298 */         };
/* 2299 */ 
/* 2300 */         /**

/* gmap3.js */

/* 2301 *|          * returns latLng of the user
/* 2302 *|          **/
/* 2303 */         self.getgeoloc = function (args) {
/* 2304 */             manageEnd(args, args.latLng);
/* 2305 */         };
/* 2306 */ 
/* 2307 */         /**
/* 2308 *|          * add a style
/* 2309 *|          **/
/* 2310 */         self.styledmaptype = function (args) {
/* 2311 */             newMap();
/* 2312 */             var obj = new defaults.classes.StyledMapType(args.td.styles, args.opts);
/* 2313 */             map.mapTypes.set(args.td.id, obj);
/* 2314 */             manageEnd(args, obj);
/* 2315 */         };
/* 2316 */ 
/* 2317 */         /**
/* 2318 *|          * add an imageMapType
/* 2319 *|          **/
/* 2320 */         self.imagemaptype = function (args) {
/* 2321 */             newMap();
/* 2322 */             var obj = new defaults.classes.ImageMapType(args.opts);
/* 2323 */             map.mapTypes.set(args.td.id, obj);
/* 2324 */             manageEnd(args, obj);
/* 2325 */         };
/* 2326 */ 
/* 2327 */         /**
/* 2328 *|          * autofit a map using its overlays (markers, rectangles ...)
/* 2329 *|          **/
/* 2330 */         self.autofit = function (args) {
/* 2331 */             var bounds = new gm.LatLngBounds();
/* 2332 */             $.each(store.all(), function (i, obj) {
/* 2333 */                 if (obj.getPosition) {
/* 2334 */                     bounds.extend(obj.getPosition());
/* 2335 */                 } else if (obj.getBounds) {
/* 2336 */                     bounds.extend(obj.getBounds().getNorthEast());
/* 2337 */                     bounds.extend(obj.getBounds().getSouthWest());
/* 2338 */                 } else if (obj.getPaths) {
/* 2339 */                     obj.getPaths().forEach(function (path) {
/* 2340 */                         path.forEach(function (latLng) {
/* 2341 */                             bounds.extend(latLng);
/* 2342 */                         });
/* 2343 */                     });
/* 2344 */                 } else if (obj.getPath) {
/* 2345 */                     obj.getPath().forEach(function (latLng) {
/* 2346 */                         bounds.extend(latLng);
/* 2347 */                     });
/* 2348 */                 } else if (obj.getCenter) {
/* 2349 */                     bounds.extend(obj.getCenter());
/* 2350 */                 } else if (typeof Clusterer === "function" && obj instanceof Clusterer) {

/* gmap3.js */

/* 2351 */                     obj = store.getById(obj.id(), true);
/* 2352 */                     if (obj) {
/* 2353 */                         obj.autofit(bounds);
/* 2354 */                     }
/* 2355 */                 }
/* 2356 */             });
/* 2357 */ 
/* 2358 */             if (!bounds.isEmpty() && (!map.getBounds() || !map.getBounds().equals(bounds))) {
/* 2359 */                 if ("maxZoom" in args.td) {
/* 2360 */                     // fitBouds Callback event => detect zoom level and check maxZoom
/* 2361 */                     gm.event.addListenerOnce(
/* 2362 */                         map,
/* 2363 */                         "bounds_changed",
/* 2364 */                         function () {
/* 2365 */                             if (this.getZoom() > args.td.maxZoom) {
/* 2366 */                                 this.setZoom(args.td.maxZoom);
/* 2367 */                             }
/* 2368 */                         }
/* 2369 */                     );
/* 2370 */                 }
/* 2371 */                 map.fitBounds(bounds);
/* 2372 */             }
/* 2373 */             manageEnd(args, true);
/* 2374 */         };
/* 2375 */ 
/* 2376 */         /**
/* 2377 *|          * remove objects from a map
/* 2378 *|          **/
/* 2379 */         self.clear = function (args) {
/* 2380 */             if (isString(args.td)) {
/* 2381 */                 if (store.clearById(args.td) || store.objClearById(args.td)) {
/* 2382 */                     manageEnd(args, true);
/* 2383 */                     return;
/* 2384 */                 }
/* 2385 */                 args.td = {name: args.td};
/* 2386 */             }
/* 2387 */             if (args.td.id) {
/* 2388 */                 $.each(array(args.td.id), function (i, id) {
/* 2389 */                     store.clearById(id) || store.objClearById(id);
/* 2390 */                 });
/* 2391 */             } else {
/* 2392 */                 store.clear(array(args.td.name), args.td.last, args.td.first, args.td.tag);
/* 2393 */                 store.objClear(array(args.td.name), args.td.last, args.td.first, args.td.tag);
/* 2394 */             }
/* 2395 */             manageEnd(args, true);
/* 2396 */         };
/* 2397 */ 
/* 2398 */         /**
/* 2399 *|          * return objects previously created
/* 2400 *|          **/

/* gmap3.js */

/* 2401 */         self.get = function (args, direct, full) {
/* 2402 */             var name, res,
/* 2403 */                 td = direct ? args : args.td;
/* 2404 */             if (!direct) {
/* 2405 */                 full = td.full;
/* 2406 */             }
/* 2407 */             if (isString(td)) {
/* 2408 */                 res = store.getById(td, false, full) || store.objGetById(td);
/* 2409 */                 if (res === false) {
/* 2410 */                     name = td;
/* 2411 */                     td = {};
/* 2412 */                 }
/* 2413 */             } else {
/* 2414 */                 name = td.name;
/* 2415 */             }
/* 2416 */             if (name === "map") {
/* 2417 */                 res = map;
/* 2418 */             }
/* 2419 */             if (!res) {
/* 2420 */                 res = [];
/* 2421 */                 if (td.id) {
/* 2422 */                     $.each(array(td.id), function (i, id) {
/* 2423 */                         res.push(store.getById(id, false, full) || store.objGetById(id));
/* 2424 */                     });
/* 2425 */                     if (!isArray(td.id)) {
/* 2426 */                         res = res[0];
/* 2427 */                     }
/* 2428 */                 } else {
/* 2429 */                     $.each(name ? array(name) : [undef], function (i, aName) {
/* 2430 */                         var result;
/* 2431 */                         if (td.first) {
/* 2432 */                             result = store.get(aName, false, td.tag, full);
/* 2433 */                             if (result) {
/* 2434 */                                 res.push(result);
/* 2435 */                             }
/* 2436 */                         } else if (td.all) {
/* 2437 */                             $.each(store.all(aName, td.tag, full), function (i, result) {
/* 2438 */                                 res.push(result);
/* 2439 */                             });
/* 2440 */                         } else {
/* 2441 */                             result = store.get(aName, true, td.tag, full);
/* 2442 */                             if (result) {
/* 2443 */                                 res.push(result);
/* 2444 */                             }
/* 2445 */                         }
/* 2446 */                     });
/* 2447 */                     if (!td.all && !isArray(name)) {
/* 2448 */                         res = res[0];
/* 2449 */                     }
/* 2450 */                 }

/* gmap3.js */

/* 2451 */             }
/* 2452 */             res = isArray(res) || !td.all ? res : [res];
/* 2453 */             if (direct) {
/* 2454 */                 return res;
/* 2455 */             } else {
/* 2456 */                 manageEnd(args, res);
/* 2457 */             }
/* 2458 */         };
/* 2459 */ 
/* 2460 */         /**
/* 2461 *|          * run a function on each items selected
/* 2462 *|          **/
/* 2463 */         self.exec = function (args) {
/* 2464 */             $.each(array(args.td.func), function (i, func) {
/* 2465 */                 $.each(self.get(args.td, true, args.td.hasOwnProperty("full") ? args.td.full : true), function (j, res) {
/* 2466 */                     func.call($this, res);
/* 2467 */                 });
/* 2468 */             });
/* 2469 */             manageEnd(args, true);
/* 2470 */         };
/* 2471 */ 
/* 2472 */         /**
/* 2473 *|          * trigger events on the map
/* 2474 *|          **/
/* 2475 */         self.trigger = function (args) {
/* 2476 */             if (isString(args.td)) {
/* 2477 */                 gm.event.trigger(map, args.td);
/* 2478 */             } else {
/* 2479 */                 var options = [map, args.td.eventName];
/* 2480 */                 if (args.td.var_args) {
/* 2481 */                     $.each(args.td.var_args, function (i, v) {
/* 2482 */                         options.push(v);
/* 2483 */                     });
/* 2484 */                 }
/* 2485 */                 gm.event.trigger.apply(gm.event, options);
/* 2486 */             }
/* 2487 */             callback(args);
/* 2488 */             task.ack();
/* 2489 */         };
/* 2490 */     }
/* 2491 */ 
/* 2492 */     $.fn.gmap3 = function () {
/* 2493 */         var i,
/* 2494 */             list = [],
/* 2495 */             empty = true,
/* 2496 */             results = [];
/* 2497 */ 
/* 2498 */         // init library
/* 2499 */         initDefaults();
/* 2500 */ 

/* gmap3.js */

/* 2501 */         // store all arguments in a td list
/* 2502 */         for (i = 0; i < arguments.length; i++) {
/* 2503 */             if (arguments[i]) {
/* 2504 */                 list.push(arguments[i]);
/* 2505 */             }
/* 2506 */         }
/* 2507 */ 
/* 2508 */         // resolve empty call - run init
/* 2509 */         if (!list.length) {
/* 2510 */             list.push("map");
/* 2511 */         }
/* 2512 */ 
/* 2513 */         // loop on each jQuery object
/* 2514 */         $.each(this, function () {
/* 2515 */             var $this = $(this),
/* 2516 */                 gmap3 = $this.data("gmap3");
/* 2517 */             empty = false;
/* 2518 */             if (!gmap3) {
/* 2519 */                 gmap3 = new Gmap3($this);
/* 2520 */                 $this.data("gmap3", gmap3);
/* 2521 */             }
/* 2522 */             if (list.length === 1 && (list[0] === "get" || isDirectGet(list[0]))) {
/* 2523 */                 if (list[0] === "get") {
/* 2524 */                     results.push(gmap3.get("map", true));
/* 2525 */                 } else {
/* 2526 */                     results.push(gmap3.get(list[0].get, true, list[0].get.full));
/* 2527 */                 }
/* 2528 */             } else {
/* 2529 */                 gmap3._plan(list);
/* 2530 */             }
/* 2531 */         });
/* 2532 */ 
/* 2533 */         // return for direct call only
/* 2534 */         if (results.length) {
/* 2535 */             if (results.length === 1) { // 1 css selector
/* 2536 */                 return results[0];
/* 2537 */             }
/* 2538 */             return results;
/* 2539 */         }
/* 2540 */ 
/* 2541 */         return this;
/* 2542 */     };
/* 2543 */ })(jQuery);

;
/* gmap.init.js */

/* 1  */ /**
/* 2  *|  * Created by me664 on 11/17/14.
/* 3  *|  */
/* 4  */ 
/* 5  */ jQuery(document).ready(function($){
/* 6  */ 
/* 7  */ 
/* 8  */     $.each($(".st_google_map"),function(i,value){
/* 9  */         var address,icon,v,saturation,lightness,gamma,map_config;
/* 10 */         v=$(value);
/* 11 */         address= v.data('address');
/* 12 */         icon= v.data('marker');
/* 13 */         saturation=v.data('saturation');
/* 14 */         lightness=v.data('lightness');
/* 15 */         gamma=v.data('gamma');
/* 16 */ 
/* 17 */         map_config={ map:{
/* 18 */             options:{
/* 19 */                 styles: [ {
/* 20 */                     stylers: [ { "saturation":v.data('saturation') }, { "lightness": v.data('lightness') }, { "gamma": v.data('gamma') }]}
/* 21 */                 ],
/* 22 */                 zoom: v.data('zoom'),
/* 23 */                 scrollwheel:false,
/* 24 */                 draggable: true }
/* 25 */         }
/* 26 */ 
/* 27 */         };
/* 28 */ 
/* 29 */         if(v.data('type')==1)
/* 30 */         {
/* 31 */             map_config.marker={
/* 32 */                 address:v.data('address')
/* 33 */ 
/* 34 */             };
/* 35 */             map_config.map.address= v.data('address');
/* 36 */         }else
/* 37 */         {
/* 38 */             map_config.marker={
/* 39 */                 latLng: [v.data('lat'), v.data('lng')]
/* 40 */ 
/* 41 */             };
/* 42 */             map_config.map.options.center=[v.data('lat'), v.data('lng')];
/* 43 */         }
/* 44 */         map_config.marker.options={
/* 45 */             icon:icon
/* 46 */         };
/* 47 */         try
/* 48 */         {
/* 49 */ 
/* 50 */             v.gmap3(map_config);

/* gmap.init.js */

/* 51 */ 
/* 52 */             var index= v.parents('.ui-tabs-panel').index();
/* 53 */             index--;
/* 54 */             var item_click=v.parents('.ui-tabs').children('.nav-tabs').find('li:eq('+index+')');
/* 55 */             if(item_click.length)
/* 56 */             {
/* 57 */                 item_click.click(function(){
/* 58 */                     v.gmap3({trigger:"resize"});
/* 59 */ 
/* 60 */                     v.gmap3('get').setCenter({
/* 61 */                         lat:parseFloat(v.data('lat')),
/* 62 */                         lng:parseFloat(v.data('lng'))
/* 63 */                     });
/* 64 */                 });
/* 65 */             }
/* 66 */ 
/* 67 */         }catch(e){
/* 68 */             console.log(e);
/* 69 */         }
/* 70 */ 
/* 71 */     });
/* 72 */ 
/* 73 */ });
/* 74 */ 

;
/* hotel-ajax.js */

/* 1   */ /**
/* 2   *|  * Created by me664 on 11/24/14.
/* 3   *|  */
/* 4   */ var last_search_room_error;
/* 5   */ jQuery(document).ready(function($){
/* 6   */ 
/* 7   */ 
/* 8   */     var last_error='';
/* 9   */ 
/* 10  */     $('.btn-do-search-room').click(function(){
/* 11  */         var searchbox=$(this).parents('.booking-item-dates-change');
/* 12  */         do_search_room(searchbox);
/* 13  */     });
/* 14  */ 
/* 15  */     $('.btn-clr-search-room').click(function(){
/* 16  */         var searchbox=$(this).parents('.booking-item-dates-change');
/* 17  */         do_clear_search_form(searchbox);
/* 18  */         do_search_room(searchbox);
/* 19  */     });
/* 20  */ 
/* 21  */     function do_clear_search_form(searchbox){
/* 22  */         searchbox.find('input[type=text]').val('');
/* 23  */         searchbox.find('select').each(function(i,v){
/* 24  */ 
/* 25  */             v.selectedIndex=0;
/* 26  */         });
/* 27  */         searchbox.find('.btn-group-select-num .btn-primary').removeClass('active');
/* 28  */ 
/* 29  */         $('.search_room_alert').html('');
/* 30  */ 
/* 31  */         $('.age_of_child_input').removeClass('error');
/* 32  */         searchbox.find('.form-control').removeClass('error');
/* 33  */     }
/* 34  */ 
/* 35  */     function do_search_room(searchbox)
/* 36  */     {
/* 37  */         var me=$('.booking-list.loop-room');
/* 38  */ 
/* 39  */ 
/* 40  */ 
/* 41  */         var data={
/* 42  */             'nonce':$('input[name=room_search]').val()
/* 43  */         };
/* 44  */ 
/* 45  */         if(typeof searchbox!="undefined")
/* 46  */         {
/* 47  */ 
/* 48  */             data=searchbox.find('input,select,textarea').serializeArray();
/* 49  */         }
/* 50  */ 

/* hotel-ajax.js */

/* 51  */         var dataobj = {};
/* 52  */         for (var i = 0; i < data.length; ++i)
/* 53  */             dataobj[data[i].name] = data[i].value;
/* 54  */         var holder=$('.search_room_alert');
/* 55  */ 
/* 56  */         holder.html('');
/* 57  */         searchbox.find('.age_of_child_input').removeClass('error');
/* 58  */         searchbox.find('.form-control').removeClass('error');
/* 59  */         searchbox.find('.form_input').removeClass('error');
/* 60  */ 
/* 61  */         if(dataobj.start=="" || dataobj.end=='')
/* 62  */         {
/* 63  */             if(dataobj.start==""){
/* 64  */                 searchbox.find('[name=start]').addClass('error');
/* 65  */             }
/* 66  */             if(dataobj.end==""){
/* 67  */                 searchbox.find('[name=end]').addClass('error');
/* 68  */             }
/* 69  */             setMessage(holder,st_hotel_localize.is_not_select_date,'danger');
/* 70  */             return false;
/* 71  */         }
/* 72  */ 
/* 73  */         //Validate Data
/* 74  */         if(dataobj.room_num_search==1){
/* 75  */             if(dataobj.adult_num=="" || dataobj.child_num==''){
/* 76  */ 
/* 77  */                 setMessage(holder,st_hotel_localize.booking_required_adult_children,'danger');
/* 78  */                 return false;
/* 79  */             }
/* 80  */ 
/* 81  */ 
/* 82  */         }else
/* 83  */         {
/* 84  */             var is_aoc_fail=false;
/* 85  */             searchbox.find('.room_num_children_input').each(function(){
/* 86  */ 
/* 87  */                 if($(this).val()>0)
/* 88  */                 {
/* 89  */ 
/* 90  */                     $(this).closest('tr').find('.age_of_child_input').each(function(){
/* 91  */                         if($(this).val()=='-1'){
/* 92  */                             $(this).addClass('error');
/* 93  */ 
/* 94  */ 
/* 95  */ 
/* 96  */                             is_aoc_fail=true;
/* 97  */                         }else
/* 98  */                         {
/* 99  */                             $(this).removeClass('error');
/* 100 */                         }

/* hotel-ajax.js */

/* 101 */                     });
/* 102 */ 
/* 103 */                 }
/* 104 */ 
/* 105 */ 
/* 106 */             });
/* 107 */ 
/* 108 */             if(is_aoc_fail){
/* 109 */                 setMessage(holder,st_hotel_localize.is_aoc_fail,'danger');
/* 110 */                 return false;
/* 111 */             }
/* 112 */ 
/* 113 */ 
/* 114 */             //Validate Host Name
/* 115 */             //is_host_name_fail
/* 116 */             var is_host_name_fail=false;
/* 117 */             searchbox.find('.room_num_host_name_input').each(function(){
/* 118 */                 if($(this).val()==''){
/* 119 */                     $(this).addClass('error');
/* 120 */                     is_host_name_fail=true;
/* 121 */                 }else
/* 122 */                 {
/* 123 */                     $(this).removeClass('error');
/* 124 */                 }
/* 125 */             });
/* 126 */ 
/* 127 */             if(is_host_name_fail){
/* 128 */                 setMessage(holder,st_hotel_localize.is_host_name_fail,'danger');
/* 129 */                 return false;
/* 130 */             }
/* 131 */         }
/* 132 */ 
/* 133 */ 
/* 134 */ 
/* 135 */         if(me.hasClass('loading'))
/* 136 */         {
/* 137 */             alert('Still loading');
/* 138 */             return;
/* 139 */         }
/* 140 */ 
/* 141 */         me.addClass('loading');
/* 142 */ 
/* 143 */         $.ajax({
/* 144 */             'type':'post',
/* 145 */             'dataType':'json',
/* 146 */             'url':"",
/* 147 */             'data':data,
/* 148 */             'success':function(data){
/* 149 */                 me.removeClass('loading');
/* 150 */                 if(data.status)

/* hotel-ajax.js */

/* 151 */                 {
/* 152 */                     if(typeof data.data!="undefined" && data.data)
/* 153 */                     {
/* 154 */                         me.html(data.data);
/* 155 */                     }else
/* 156 */                     {
/* 157 */                         me.html('');
/* 158 */                     }
/* 159 */                     $('body').tooltip({
/* 160 */                         selector: '[rel=tooltip]'
/* 161 */                     });
/* 162 */                     $('.i-check, .i-radio').iCheck({
/* 163 */                         checkboxClass: 'i-check',
/* 164 */                         radioClass: 'i-radio'
/* 165 */                     });
/* 166 */ 
/* 167 */                 }
/* 168 */                 if(data.message){
/* 169 */                     setMessage(holder,data.message,'danger');
/* 170 */                 }
/* 171 */ 
/* 172 */ 
/* 173 */             },
/* 174 */             error:function(data){
/* 175 */ 
/* 176 */                 me.removeClass('loading');
/* 177 */                 alert('Ajax Fail');
/* 178 */             }
/* 179 */         })
/* 180 */     }
/* 181 */ 
/* 182 */     function setMessage(holder,message,type)
/* 183 */     {
/* 184 */         if(typeof  type=='undefined'){
/* 185 */             type='infomation';
/* 186 */         }
/* 187 */         var html='<div class="alert alert-'+type+'">'+message+'</div>';
/* 188 */         if(!holder.length) return;
/* 189 */         holder.html('');
/* 190 */         holder.html(html);
/* 191 */         do_scrollTo(holder);
/* 192 */     }
/* 193 */ 
/* 194 */ 
/* 195 */     //$('[name=room_num_search]').change(function(){
/* 196 */     //    var val=$(this).val();
/* 197 */     //    if(val==1){
/* 198 */     //        $('.room_num_config').addClass('hidden');
/* 199 */     //        $('.age_of_children_col').addClass('hidden');
/* 200 */     //    }else

/* hotel-ajax.js */

/* 201 */     //    {
/* 202 */     //        var body_table=$('.room_num_config >table>tbody');
/* 203 */     //        $('.room_num_config').removeClass('hidden');
/* 204 */     //        body_table.html('');
/* 205 */     //        for(i=0;i<val;i++){
/* 206 */     //            var html=$('#example_room_config tbody').html();
/* 207 */     //            html=$(html);
/* 208 */     //            html.find('.room_num_text').html(st_hotel_localize.room+' '+(i+1));
/* 209 */     //            html.find('[name=room_num_adults]').attr('name','room_num_config['+i+'][adults]');
/* 210 */     //            html.find('[name=room_num_children]').attr('name','room_num_config['+i+'][children]');
/* 211 */     //            html.find('[name=room_num_host_name]').attr('name','room_num_config['+i+'][host_name]');
/* 212 */     //            body_table.append(html);
/* 213 */     //        }
/* 214 */     //    }
/* 215 */     //});
/* 216 */ 
/* 217 */     $(document).on('change','.room_num_children_input',function(){
/* 218 */         var val=$(this).val();
/* 219 */         var parent=$(this).closest('tr');
/* 220 */ 
/* 221 */         show_child_age_col();
/* 222 */         if(val==0)
/* 223 */         {
/* 224 */             parent.find('.room_num_age_of_children').html('');
/* 225 */         }else
/* 226 */         {
/* 227 */             parent.find('.room_num_age_of_children').html('');
/* 228 */ 
/* 229 */             var room_num=$(this).closest('.room-item').data('room-num');
/* 230 */ 
/* 231 */             for(i=1;i<=val;i++){
/* 232 */                 var html=$('#example_age_of_child').clone();
/* 233 */                 html.removeAttr('id');
/* 234 */                 html.removeClass('hidden');
/* 235 */                 html.addClass('required');
/* 236 */ 
/* 237 */                 html.attr('name','room_data['+room_num+'][age_of_children]['+i+']');
/* 238 */                 parent.find('.room_num_age_of_children').append(html);
/* 239 */             }
/* 240 */         }
/* 241 */     });
/* 242 */ 
/* 243 */     function show_child_age_col()
/* 244 */     {
/* 245 */         $('.room_num_config tbody tr').each(function(){
/* 246 */ 
/* 247 */             var select=$(this).find('.room_num_children_input');
/* 248 */             if(select.val()>0){
/* 249 */                 $('.age_of_children_col').removeClass('hidden');
/* 250 */             }

/* hotel-ajax.js */

/* 251 */ 
/* 252 */         });
/* 253 */     }
/* 254 */     show_child_age_col();
/* 255 */ 
/* 256 */     function do_scrollTo(el)
/* 257 */     {
/* 258 */         if(el.length){
/* 259 */             var top=el.offset().top;
/* 260 */             if($('#wpadminbar').length && $('#wpadminbar').css('position')=='fixed')
/* 261 */             {
/* 262 */                 console.log(top);
/* 263 */                 top-=32;
/* 264 */             }
/* 265 */             top-=50;
/* 266 */             $('html,body').animate({
/* 267 */                 'scrollTop':top
/* 268 */             },500);
/* 269 */         }
/* 270 */     }
/* 271 */ 
/* 272 */ 
/* 273 */ });

;
/* booking_modal.js */

/* 1   */ /**
/* 2   *|  * Created by me664 on 12/29/14.
/* 3   *|  */
/* 4   */ jQuery(document).ready(function($){
/* 5   */ 
/* 6   */     $(document).on('click','.payment_gateway_item .st_payment_gatewaw_submit',function(){
/* 7   */         var me=$(this).parents('.booking_modal_form');
/* 8   */ 
/* 9   */         $(this).next('.icon_loading').remove();
/* 10  */         $(this).after('<span class="icon_loading"><i class="fa fa-spin fa-refresh"></i></span>');
/* 11  */ 
/* 12  */         submit_form(me,$(this));
/* 13  */     });
/* 14  */ 
/* 15  */     $(document).on('click','.btn_hotel_booking',function(){
/* 16  */         var form=$(this).closest('form');
/* 17  */         if(!checkRequiredBooking(form))
/* 18  */         {
/* 19  */             return false;
/* 20  */         }
/* 21  */ 
/* 22  */         var tar_get=$(this).data('target');
/* 23  */ 
/* 24  */         $.magnificPopup.open({
/* 25  */             items: {
/* 26  */                 type: 'inline',
/* 27  */                 src: tar_get
/* 28  */             }
/* 29  */ 
/* 30  */         });
/* 31  */ 
/* 32  */     });
/* 33  */     function do_scrollTo(el)
/* 34  */     {
/* 35  */         if(el.length){
/* 36  */             var top=el.offset().top;
/* 37  */             if($('#wpadminbar').length && $('#wpadminbar').css('position')=='fixed')
/* 38  */             {
/* 39  */                 top-=32;
/* 40  */             }
/* 41  */             top-=50;
/* 42  */             $('html,body').animate({
/* 43  */                 'scrollTop':top
/* 44  */             },500);
/* 45  */         }
/* 46  */     }
/* 47  */ 
/* 48  */     function setMessage(holder,message,type)
/* 49  */     {
/* 50  */         if(typeof  type=='undefined'){

/* booking_modal.js */

/* 51  */             type='infomation';
/* 52  */         }
/* 53  */         var html='<div class="alert alert-'+type+'">'+message+'</div>';
/* 54  */         if(!holder.length) return;
/* 55  */         holder.html('');
/* 56  */         holder.html(html);
/* 57  */         do_scrollTo(holder);
/* 58  */     }
/* 59  */ 
/* 60  */ 
/* 61  */     function checkRequiredBooking(searchbox)
/* 62  */     {
/* 63  */         var searchform=$('.booking-item-dates-change');
/* 64  */         if(typeof searchbox!="undefined")
/* 65  */         {
/* 66  */             var data=searchbox.find('input,select,textarea').serializeArray();
/* 67  */         }
/* 68  */ 
/* 69  */         var dataobj = {};
/* 70  */         for (var i = 0; i < data.length; ++i)
/* 71  */             dataobj[data[i].name] = data[i].value;
/* 72  */ 
/* 73  */         var holder=$('.search_room_alert');
/* 74  */ 
/* 75  */         holder.html('');
/* 76  */         if(dataobj.room_num_search=="1"){
/* 77  */             if(dataobj.adult_num=="" || dataobj.child_num=='' ||typeof dataobj.adult_num=='undefined' || typeof dataobj.child_num=='undefined'){
/* 78  */ 
/* 79  */                 setMessage(holder,st_hotel_localize.booking_required_adult_children,'danger');
/* 80  */                 return false;
/* 81  */             }
/* 82  */ 
/* 83  */         }
/* 84  */         if(dataobj.check_in=="" || dataobj.check_out=='')
/* 85  */         {
/* 86  */             if(dataobj.check_in==""){
/* 87  */                 searchform.find('[name=start]').addClass('error');
/* 88  */             }
/* 89  */             if(dataobj.check_out==""){
/* 90  */                 searchform.find('[name=end]').addClass('error');
/* 91  */             }
/* 92  */             setMessage(holder,st_hotel_localize.is_not_select_date,'danger');
/* 93  */             return false;
/* 94  */         }
/* 95  */ 
/* 96  */         return true;
/* 97  */ 
/* 98  */     }
/* 99  */ 
/* 100 */     function submit_form(me,clicked)

/* booking_modal.js */

/* 101 */     {
/* 102 */         var data=me.find("select,textarea, input").serializeArray();
/* 103 */ 
/* 104 */         me.find('.form-control').removeClass('error');
/* 105 */ 
/* 106 */ 
/* 107 */         data.push(
/* 108 */             {
/* 109 */                 name:'action',
/* 110 */                 value:'booking_form_submit'
/* 111 */             }
/* 112 */         );
/* 113 */ 
/* 114 */ 
/* 115 */         me.find('.form_alert').addClass('hidden');
/* 116 */         var dataobj = {};
/* 117 */ 
/* 118 */         var form_validate=true;
/* 119 */ 
/* 120 */ 
/* 121 */         for (var i = 0; i < data.length; ++i)
/* 122 */         {
/* 123 */             dataobj[data[i].name] = data[i].value;
/* 124 */             var name=data[i].name;
/* 125 */ 
/* 126 */         }
/* 127 */         me.find('input.required,select.required,textarea.required').removeClass('error');
/* 128 */         me.find('input.required,select.required,textarea.required').each(function(){
/* 129 */             if(!$(this).val())
/* 130 */             {
/* 131 */                 $(this).addClass('error');
/* 132 */                 form_validate=false;
/* 133 */ 
/* 134 */             }
/* 135 */         });
/* 136 */ 
/* 137 */ 
/* 138 */ 
/* 139 */         dataobj[clicked.attr('name')]=clicked.attr('value');
/* 140 */ 
/* 141 */         if(form_validate==false){
/* 142 */             me.find('.form_alert').addClass('alert-danger').removeClass('hidden');
/* 143 */             me.find('.form_alert').html(st_checkout_text.validate_form);
/* 144 */             me.find('.icon_loading').remove();
/* 145 */             return false;
/* 146 */         }
/* 147 */ 
/* 148 */         //term_condition
/* 149 */         if(!dataobj.term_condition){
/* 150 */             me.find('.form_alert').addClass('alert-danger').removeClass('hidden');

/* booking_modal.js */

/* 151 */             me.find('.form_alert').html(st_checkout_text.error_accept_term);
/* 152 */ 
/* 153 */             me.find('.icon_loading').remove();
/* 154 */             return false;
/* 155 */         }
/* 156 */         //console.log(dataobj);
/* 157 */ 
/* 158 */ 
/* 159 */         me.addClass('loading');
/* 160 */         $.ajax({
/* 161 */             'type':'post',
/* 162 */             'dataType':'json',
/* 163 */             'url':st_params.ajax_url,
/* 164 */             'data':dataobj,
/* 165 */             'success':function(data){
/* 166 */                 me.removeClass('loading');
/* 167 */ 
/* 168 */                 if(data.message){
/* 169 */                     me.find('.form_alert').addClass('alert-danger').removeClass('hidden');
/* 170 */                     me.find('.form_alert').html(data.message);
/* 171 */                 }
/* 172 */ 
/* 173 */                 if(data.redirect){
/* 174 */                     window.location.href=data.redirect;
/* 175 */                 }
/* 176 */ 
/* 177 */                 me.find('.icon_loading').remove();
/* 178 */ 
/* 179 */                 var widget_id='st_recaptchar_'+dataobj.item_id;
/* 180 */ 
/* 181 */                 get_new_captcha(me);
/* 182 */             },
/* 183 */             error:function(data){
/* 184 */ 
/* 185 */                 me.removeClass('loading');
/* 186 */                 alert('Ajax Fail');
/* 187 */ 
/* 188 */                 me.find('.icon_loading').remove();
/* 189 */ 
/* 190 */                 var widget_id='st_recaptchar_'+dataobj.item_id;
/* 191 */ 
/* 192 */                 get_new_captcha(me);
/* 193 */ 
/* 194 */             }
/* 195 */         });
/* 196 */ 
/* 197 */         function get_new_captcha(me)
/* 198 */         {
/* 199 */             var captcha_box=me.find('.captcha_box');
/* 200 */             url=captcha_box.find('.captcha_img').attr('src');

/* booking_modal.js */

/* 201 */             captcha_box.find('.captcha_img').attr('src',url);
/* 202 */         }
/* 203 */     }
/* 204 */ });

;
/* jquery.noty.packaged.min.js */

/* 1 */ !function(a,b){"function"==typeof define&&define.amd?define(["jquery"],b):b(a.jQuery)}(this,function(a){"function"!=typeof Object.create&&(Object.create=function(a){function b(){}return b.prototype=a,new b});var b={init:function(b){return this.options=a.extend({},a.noty.defaults,b),this.options.layout=this.options.custom?a.noty.layouts.inline:a.noty.layouts[this.options.layout],a.noty.themes[this.options.theme]?this.options.theme=a.noty.themes[this.options.theme]:b.themeClassName=this.options.theme,delete b.layout,delete b.theme,this.options=a.extend({},this.options,this.options.layout.options),this.options.id="noty_"+(new Date).getTime()*Math.floor(1e6*Math.random()),this.options=a.extend({},this.options,b),this._build(),this},_build:function(){var b=a('<div class="noty_bar noty_type_'+this.options.type+'"></div>').attr("id",this.options.id);if(b.append(this.options.template).find(".noty_text").html(this.options.text),this.$bar=null!==this.options.layout.parent.object?a(this.options.layout.parent.object).css(this.options.layout.parent.css).append(b):b,this.options.themeClassName&&this.$bar.addClass(this.options.themeClassName).addClass("noty_container_type_"+this.options.type),this.options.buttons){this.options.closeWith=[],this.options.timeout=!1;var c=a("<div/>").addClass("noty_buttons");null!==this.options.layout.parent.object?this.$bar.find(".noty_bar").append(c):this.$bar.append(c);var d=this;a.each(this.options.buttons,function(b,c){var e=a("<button/>").addClass(c.addClass?c.addClass:"gray").html(c.text).attr("id",c.id?c.id:"button-"+b).appendTo(d.$bar.find(".noty_buttons")).on("click",function(b){a.isFunction(c.onClick)&&c.onClick.call(e,d,b)})})}this.$message=this.$bar.find(".noty_message"),this.$closeButton=this.$bar.find(".noty_close"),this.$buttons=this.$bar.find(".noty_buttons"),a.noty.store[this.options.id]=this},show:function(){var b=this;return b.options.custom?b.options.custom.find(b.options.layout.container.selector).append(b.$bar):a(b.options.layout.container.selector).append(b.$bar),b.options.theme&&b.options.theme.style&&b.options.theme.style.apply(b),"function"===a.type(b.options.layout.css)?this.options.layout.css.apply(b.$bar):b.$bar.css(this.options.layout.css||{}),b.$bar.addClass(b.options.layout.addClass),b.options.layout.container.style.apply(a(b.options.layout.container.selector)),b.showing=!0,b.options.theme&&b.options.theme.style&&b.options.theme.callback.onShow.apply(this),a.inArray("click",b.options.closeWith)>-1&&b.$bar.css("cursor","pointer").one("click",function(a){b.stopPropagation(a),b.options.callback.onCloseClick&&b.options.callback.onCloseClick.apply(b),b.close()}),a.inArray("hover",b.options.closeWith)>-1&&b.$bar.one("mouseenter",function(){b.close()}),a.inArray("button",b.options.closeWith)>-1&&b.$closeButton.one("click",function(a){b.stopPropagation(a),b.close()}),-1==a.inArray("button",b.options.closeWith)&&b.$closeButton.remove(),b.options.callback.onShow&&b.options.callback.onShow.apply(b),"string"==typeof b.options.animation.open?(b.$bar.css("height",b.$bar.innerHeight()),b.$bar.show().addClass(b.options.animation.open).one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend",function(){b.options.callback.afterShow&&b.options.callback.afterShow.apply(b),b.showing=!1,b.shown=!0})):b.$bar.animate(b.options.animation.open,b.options.animation.speed,b.options.animation.easing,function(){b.options.callback.afterShow&&b.options.callback.afterShow.apply(b),b.showing=!1,b.shown=!0}),b.options.timeout&&b.$bar.delay(b.options.timeout).promise().done(function(){b.close()}),this},close:function(){if(!(this.closed||this.$bar&&this.$bar.hasClass("i-am-closing-now"))){var b=this;if(this.showing)return b.$bar.queue(function(){b.close.apply(b)}),void 0;if(!this.shown&&!this.showing){var c=[];return a.each(a.noty.queue,function(a,d){d.options.id!=b.options.id&&c.push(d)}),a.noty.queue=c,void 0}b.$bar.addClass("i-am-closing-now"),b.options.callback.onClose&&b.options.callback.onClose.apply(b),"string"==typeof b.options.animation.close?b.$bar.addClass(b.options.animation.close).one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend",function(){b.options.callback.afterClose&&b.options.callback.afterClose.apply(b),b.closeCleanUp()}):b.$bar.clearQueue().stop().animate(b.options.animation.close,b.options.animation.speed,b.options.animation.easing,function(){b.options.callback.afterClose&&b.options.callback.afterClose.apply(b)}).promise().done(function(){b.closeCleanUp()})}},closeCleanUp:function(){var b=this;b.options.modal&&(a.notyRenderer.setModalCount(-1),0==a.notyRenderer.getModalCount()&&a(".noty_modal").fadeOut("fast",function(){a(this).remove()})),a.notyRenderer.setLayoutCountFor(b,-1),0==a.notyRenderer.getLayoutCountFor(b)&&a(b.options.layout.container.selector).remove(),"undefined"!=typeof b.$bar&&null!==b.$bar&&("string"==typeof b.options.animation.close?(b.$bar.css("transition","all 100ms ease").css("border",0).css("margin",0).height(0),b.$bar.one("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd",function(){b.$bar.remove(),b.$bar=null,b.closed=!0,b.options.theme.callback&&b.options.theme.callback.onClose&&b.options.theme.callback.onClose.apply(b)})):(b.$bar.remove(),b.$bar=null,b.closed=!0)),delete a.noty.store[b.options.id],b.options.theme.callback&&b.options.theme.callback.onClose&&b.options.theme.callback.onClose.apply(b),b.options.dismissQueue||(a.noty.ontap=!0,a.notyRenderer.render()),b.options.maxVisible>0&&b.options.dismissQueue&&a.notyRenderer.render()},setText:function(a){return this.closed||(this.options.text=a,this.$bar.find(".noty_text").html(a)),this},setType:function(a){return this.closed||(this.options.type=a,this.options.theme.style.apply(this),this.options.theme.callback.onShow.apply(this)),this},setTimeout:function(a){if(!this.closed){var b=this;this.options.timeout=a,b.$bar.delay(b.options.timeout).promise().done(function(){b.close()})}return this},stopPropagation:function(a){a=a||window.event,"undefined"!=typeof a.stopPropagation?a.stopPropagation():a.cancelBubble=!0},closed:!1,showing:!1,shown:!1};a.notyRenderer={},a.notyRenderer.init=function(c){var d=Object.create(b).init(c);return d.options.killer&&a.noty.closeAll(),d.options.force?a.noty.queue.unshift(d):a.noty.queue.push(d),a.notyRenderer.render(),"object"==a.noty.returns?d:d.options.id},a.notyRenderer.render=function(){var b=a.noty.queue[0];"object"===a.type(b)?b.options.dismissQueue?b.options.maxVisible>0?a(b.options.layout.container.selector+" li").length<b.options.maxVisible&&a.notyRenderer.show(a.noty.queue.shift()):a.notyRenderer.show(a.noty.queue.shift()):a.noty.ontap&&(a.notyRenderer.show(a.noty.queue.shift()),a.noty.ontap=!1):a.noty.ontap=!0},a.notyRenderer.show=function(b){b.options.modal&&(a.notyRenderer.createModalFor(b),a.notyRenderer.setModalCount(1)),b.options.custom?0==b.options.custom.find(b.options.layout.container.selector).length?b.options.custom.append(a(b.options.layout.container.object).addClass("i-am-new")):b.options.custom.find(b.options.layout.container.selector).removeClass("i-am-new"):0==a(b.options.layout.container.selector).length?a("body").append(a(b.options.layout.container.object).addClass("i-am-new")):a(b.options.layout.container.selector).removeClass("i-am-new"),a.notyRenderer.setLayoutCountFor(b,1),b.show()},a.notyRenderer.createModalFor=function(b){if(0==a(".noty_modal").length){var c=a("<div/>").addClass("noty_modal").addClass(b.options.theme).data("noty_modal_count",0);b.options.theme.modal&&b.options.theme.modal.css&&c.css(b.options.theme.modal.css),c.prependTo(a("body")).fadeIn("fast"),a.inArray("backdrop",b.options.closeWith)>-1&&c.on("click",function(){a.noty.closeAll()})}},a.notyRenderer.getLayoutCountFor=function(b){return a(b.options.layout.container.selector).data("noty_layout_count")||0},a.notyRenderer.setLayoutCountFor=function(b,c){return a(b.options.layout.container.selector).data("noty_layout_count",a.notyRenderer.getLayoutCountFor(b)+c)},a.notyRenderer.getModalCount=function(){return a(".noty_modal").data("noty_modal_count")||0},a.notyRenderer.setModalCount=function(b){return a(".noty_modal").data("noty_modal_count",a.notyRenderer.getModalCount()+b)},a.fn.noty=function(b){return b.custom=a(this),a.notyRenderer.init(b)},a.noty={},a.noty.queue=[],a.noty.ontap=!0,a.noty.layouts={},a.noty.themes={},a.noty.returns="object",a.noty.store={},a.noty.get=function(b){return a.noty.store.hasOwnProperty(b)?a.noty.store[b]:!1},a.noty.close=function(b){return a.noty.get(b)?a.noty.get(b).close():!1},a.noty.setText=function(b,c){return a.noty.get(b)?a.noty.get(b).setText(c):!1},a.noty.setType=function(b,c){return a.noty.get(b)?a.noty.get(b).setType(c):!1},a.noty.clearQueue=function(){a.noty.queue=[]},a.noty.closeAll=function(){a.noty.clearQueue(),a.each(a.noty.store,function(a,b){b.close()})};var c=window.alert;a.noty.consumeAlert=function(b){window.alert=function(c){b?b.text=c:b={text:c},a.notyRenderer.init(b)}},a.noty.stopConsumeAlert=function(){window.alert=c},a.noty.defaults={layout:"top",theme:"defaultTheme",type:"alert",text:"",dismissQueue:!0,template:'<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',animation:{open:{height:"toggle"},close:{height:"toggle"},easing:"swing",speed:500},timeout:!1,force:!1,modal:!1,maxVisible:5,killer:!1,closeWith:["click"],callback:{onShow:function(){},afterShow:function(){},onClose:function(){},afterClose:function(){},onCloseClick:function(){}},buttons:!1},a(window).on("resize",function(){a.each(a.noty.layouts,function(b,c){c.container.style.apply(a(c.container.selector))})}),window.noty=function(a){return jQuery.notyRenderer.init(a)},a.noty.layouts.bottom={name:"bottom",options:{},container:{object:'<ul id="noty_bottom_layout_container" />',selector:"ul#noty_bottom_layout_container",style:function(){a(this).css({bottom:0,left:"5%",position:"fixed",width:"90%",height:"auto",margin:0,padding:0,listStyleType:"none",zIndex:9999999})}},parent:{object:"<li />",selector:"li",css:{}},css:{display:"none"},addClass:""},a.noty.layouts.bottomCenter={name:"bottomCenter",options:{},container:{object:'<ul id="noty_bottomCenter_layout_container" />',selector:"ul#noty_bottomCenter_layout_container",style:function(){a(this).css({bottom:20,left:0,position:"fixed",width:"310px",height:"auto",margin:0,padding:0,listStyleType:"none",zIndex:1e7}),a(this).css({left:(a(window).width()-a(this).outerWidth(!1))/2+"px"})}},parent:{object:"<li />",selector:"li",css:{}},css:{display:"none",width:"310px"},addClass:""},a.noty.layouts.bottomLeft={name:"bottomLeft",options:{},container:{object:'<ul id="noty_bottomLeft_layout_container" />',selector:"ul#noty_bottomLeft_layout_container",style:function(){a(this).css({bottom:20,left:20,position:"fixed",width:"310px",height:"auto",margin:0,padding:0,listStyleType:"none",zIndex:1e7}),window.innerWidth<600&&a(this).css({left:5})}},parent:{object:"<li />",selector:"li",css:{}},css:{display:"none",width:"310px"},addClass:""},a.noty.layouts.bottomRight={name:"bottomRight",options:{},container:{object:'<ul id="noty_bottomRight_layout_container" />',selector:"ul#noty_bottomRight_layout_container",style:function(){a(this).css({bottom:20,right:20,position:"fixed",width:"310px",height:"auto",margin:0,padding:0,listStyleType:"none",zIndex:1e7}),window.innerWidth<600&&a(this).css({right:5})}},parent:{object:"<li />",selector:"li",css:{}},css:{display:"none",width:"310px"},addClass:""},a.noty.layouts.center={name:"center",options:{},container:{object:'<ul id="noty_center_layout_container" />',selector:"ul#noty_center_layout_container",style:function(){a(this).css({position:"fixed",width:"310px",height:"auto",margin:0,padding:0,listStyleType:"none",zIndex:1e7});var b=a(this).clone().css({visibility:"hidden",display:"block",position:"absolute",top:0,left:0}).attr("id","dupe");a("body").append(b),b.find(".i-am-closing-now").remove(),b.find("li").css("display","block");var c=b.height();b.remove(),a(this).hasClass("i-am-new")?a(this).css({left:(a(window).width()-a(this).outerWidth(!1))/2+"px",top:(a(window).height()-c)/2+"px"}):a(this).animate({left:(a(window).width()-a(this).outerWidth(!1))/2+"px",top:(a(window).height()-c)/2+"px"},500)}},parent:{object:"<li />",selector:"li",css:{}},css:{display:"none",width:"310px"},addClass:""},a.noty.layouts.centerLeft={name:"centerLeft",options:{},container:{object:'<ul id="noty_centerLeft_layout_container" />',selector:"ul#noty_centerLeft_layout_container",style:function(){a(this).css({left:20,position:"fixed",width:"310px",height:"auto",margin:0,padding:0,listStyleType:"none",zIndex:1e7});var b=a(this).clone().css({visibility:"hidden",display:"block",position:"absolute",top:0,left:0}).attr("id","dupe");a("body").append(b),b.find(".i-am-closing-now").remove(),b.find("li").css("display","block");var c=b.height();b.remove(),a(this).hasClass("i-am-new")?a(this).css({top:(a(window).height()-c)/2+"px"}):a(this).animate({top:(a(window).height()-c)/2+"px"},500),window.innerWidth<600&&a(this).css({left:5})}},parent:{object:"<li />",selector:"li",css:{}},css:{display:"none",width:"310px"},addClass:""},a.noty.layouts.centerRight={name:"centerRight",options:{},container:{object:'<ul id="noty_centerRight_layout_container" />',selector:"ul#noty_centerRight_layout_container",style:function(){a(this).css({right:20,position:"fixed",width:"310px",height:"auto",margin:0,padding:0,listStyleType:"none",zIndex:1e7});var b=a(this).clone().css({visibility:"hidden",display:"block",position:"absolute",top:0,left:0}).attr("id","dupe");a("body").append(b),b.find(".i-am-closing-now").remove(),b.find("li").css("display","block");var c=b.height();b.remove(),a(this).hasClass("i-am-new")?a(this).css({top:(a(window).height()-c)/2+"px"}):a(this).animate({top:(a(window).height()-c)/2+"px"},500),window.innerWidth<600&&a(this).css({right:5})}},parent:{object:"<li />",selector:"li",css:{}},css:{display:"none",width:"310px"},addClass:""},a.noty.layouts.inline={name:"inline",options:{},container:{object:'<ul class="noty_inline_layout_container" />',selector:"ul.noty_inline_layout_container",style:function(){a(this).css({width:"100%",height:"auto",margin:0,padding:0,listStyleType:"none",zIndex:9999999})}},parent:{object:"<li />",selector:"li",css:{}},css:{display:"none"},addClass:""},a.noty.layouts.top={name:"top",options:{},container:{object:'<ul id="noty_top_layout_container" />',selector:"ul#noty_top_layout_container",style:function(){a(this).css({top:0,left:"5%",position:"fixed",width:"90%",height:"auto",margin:0,padding:0,listStyleType:"none",zIndex:9999999})}},parent:{object:"<li />",selector:"li",css:{}},css:{display:"none"},addClass:""},a.noty.layouts.topCenter={name:"topCenter",options:{},container:{object:'<ul id="noty_topCenter_layout_container" />',selector:"ul#noty_topCenter_layout_container",style:function(){a(this).css({top:20,left:0,position:"fixed",width:"310px",height:"auto",margin:0,padding:0,listStyleType:"none",zIndex:1e7}),a(this).css({left:(a(window).width()-a(this).outerWidth(!1))/2+"px"})}},parent:{object:"<li />",selector:"li",css:{}},css:{display:"none",width:"310px"},addClass:""},a.noty.layouts.topLeft={name:"topLeft",options:{},container:{object:'<ul id="noty_topLeft_layout_container" />',selector:"ul#noty_topLeft_layout_container",style:function(){a(this).css({top:20,left:20,position:"fixed",width:"310px",height:"auto",margin:0,padding:0,listStyleType:"none",zIndex:1e7}),window.innerWidth<600&&a(this).css({left:5})}},parent:{object:"<li />",selector:"li",css:{}},css:{display:"none",width:"310px"},addClass:""},a.noty.layouts.topRight={name:"topRight",options:{},container:{object:'<ul id="noty_topRight_layout_container" />',selector:"ul#noty_topRight_layout_container",style:function(){a(this).css({top:20,right:20,position:"fixed",width:"310px",height:"auto",margin:0,padding:0,listStyleType:"none",zIndex:1e7}),window.innerWidth<600&&a(this).css({right:5})}},parent:{object:"<li />",selector:"li",css:{}},css:{display:"none",width:"310px"},addClass:""},a.noty.themes.bootstrapTheme={name:"bootstrapTheme",modal:{css:{position:"fixed",width:"100%",height:"100%",backgroundColor:"#000",zIndex:1e4,opacity:.6,display:"none",left:0,top:0}},style:function(){var b=this.options.layout.container.selector;switch(a(b).addClass("list-group"),this.$closeButton.append('<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>'),this.$closeButton.addClass("close"),this.$bar.addClass("list-group-item").css("padding","0px"),this.options.type){case"alert":case"notification":this.$bar.addClass("list-group-item-info");break;case"warning":this.$bar.addClass("list-group-item-warning");break;case"error":this.$bar.addClass("list-group-item-danger");break;case"information":this.$bar.addClass("list-group-item-info");break;case"success":this.$bar.addClass("list-group-item-success")}this.$message.css({fontSize:"13px",lineHeight:"16px",textAlign:"center",padding:"8px 10px 9px",width:"auto",position:"relative"})},callback:{onShow:function(){},onClose:function(){}}},a.noty.themes.defaultTheme={name:"defaultTheme",helpers:{borderFix:function(){if(this.options.dismissQueue){var b=this.options.layout.container.selector+" "+this.options.layout.parent.selector;switch(this.options.layout.name){case"top":a(b).css({borderRadius:"0px 0px 0px 0px"}),a(b).last().css({borderRadius:"0px 0px 5px 5px"});break;case"topCenter":case"topLeft":case"topRight":case"bottomCenter":case"bottomLeft":case"bottomRight":case"center":case"centerLeft":case"centerRight":case"inline":a(b).css({borderRadius:"0px 0px 0px 0px"}),a(b).first().css({"border-top-left-radius":"5px","border-top-right-radius":"5px"}),a(b).last().css({"border-bottom-left-radius":"5px","border-bottom-right-radius":"5px"});break;case"bottom":a(b).css({borderRadius:"0px 0px 0px 0px"}),a(b).first().css({borderRadius:"5px 5px 0px 0px"})}}}},modal:{css:{position:"fixed",width:"100%",height:"100%",backgroundColor:"#000",zIndex:1e4,opacity:.6,display:"none",left:0,top:0}},style:function(){switch(this.$bar.css({overflow:"hidden",background:"url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAoCAQAAAClM0ndAAAAhklEQVR4AdXO0QrCMBBE0bttkk38/w8WRERpdyjzVOc+HxhIHqJGMQcFFkpYRQotLLSw0IJ5aBdovruMYDA/kT8plF9ZKLFQcgF18hDj1SbQOMlCA4kao0iiXmah7qBWPdxpohsgVZyj7e5I9KcID+EhiDI5gxBYKLBQYKHAQoGFAoEks/YEGHYKB7hFxf0AAAAASUVORK5CYII=') repeat-x scroll left top #fff"}),this.$message.css({fontSize:"13px",lineHeight:"16px",textAlign:"center",padding:"8px 10px 9px",width:"auto",position:"relative"}),this.$closeButton.css({position:"absolute",top:4,right:4,width:10,height:10,background:"url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAQAAAAnOwc2AAAAxUlEQVR4AR3MPUoDURSA0e++uSkkOxC3IAOWNtaCIDaChfgXBMEZbQRByxCwk+BasgQRZLSYoLgDQbARxry8nyumPcVRKDfd0Aa8AsgDv1zp6pYd5jWOwhvebRTbzNNEw5BSsIpsj/kurQBnmk7sIFcCF5yyZPDRG6trQhujXYosaFoc+2f1MJ89uc76IND6F9BvlXUdpb6xwD2+4q3me3bysiHvtLYrUJto7PD/ve7LNHxSg/woN2kSz4txasBdhyiz3ugPGetTjm3XRokAAAAASUVORK5CYII=)",display:"none",cursor:"pointer"}),this.$buttons.css({padding:5,textAlign:"right",borderTop:"1px solid #ccc",backgroundColor:"#fff"}),this.$buttons.find("button").css({marginLeft:5}),this.$buttons.find("button:first").css({marginLeft:0}),this.$bar.on({mouseenter:function(){a(this).find(".noty_close").stop().fadeTo("normal",1)},mouseleave:function(){a(this).find(".noty_close").stop().fadeTo("normal",0)}}),this.options.layout.name){case"top":this.$bar.css({borderRadius:"0px 0px 5px 5px",borderBottom:"2px solid #eee",borderLeft:"2px solid #eee",borderRight:"2px solid #eee",boxShadow:"0 2px 4px rgba(0, 0, 0, 0.1)"});break;case"topCenter":case"center":case"bottomCenter":case"inline":this.$bar.css({borderRadius:"5px",border:"1px solid #eee",boxShadow:"0 2px 4px rgba(0, 0, 0, 0.1)"}),this.$message.css({fontSize:"13px",textAlign:"center"});break;case"topLeft":case"topRight":case"bottomLeft":case"bottomRight":case"centerLeft":case"centerRight":this.$bar.css({borderRadius:"5px",border:"1px solid #eee",boxShadow:"0 2px 4px rgba(0, 0, 0, 0.1)"}),this.$message.css({fontSize:"13px",textAlign:"left"});break;case"bottom":this.$bar.css({borderRadius:"5px 5px 0px 0px",borderTop:"2px solid #eee",borderLeft:"2px solid #eee",borderRight:"2px solid #eee",boxShadow:"0 -2px 4px rgba(0, 0, 0, 0.1)"});break;default:this.$bar.css({border:"2px solid #eee",boxShadow:"0 2px 4px rgba(0, 0, 0, 0.1)"})}switch(this.options.type){case"alert":case"notification":this.$bar.css({backgroundColor:"#FFF",borderColor:"#CCC",color:"#444"});break;case"warning":this.$bar.css({backgroundColor:"#FFEAA8",borderColor:"#FFC237",color:"#826200"}),this.$buttons.css({borderTop:"1px solid #FFC237"});break;case"error":this.$bar.css({backgroundColor:"red",borderColor:"darkred",color:"#FFF"}),this.$message.css({fontWeight:"bold"}),this.$buttons.css({borderTop:"1px solid darkred"});break;case"information":this.$bar.css({backgroundColor:"#57B7E2",borderColor:"#0B90C4",color:"#FFF"}),this.$buttons.css({borderTop:"1px solid #0B90C4"});break;case"success":this.$bar.css({backgroundColor:"lightgreen",borderColor:"#50C24E",color:"darkgreen"}),this.$buttons.css({borderTop:"1px solid #50C24E"});break;default:this.$bar.css({backgroundColor:"#FFF",borderColor:"#CCC",color:"#444"})}},callback:{onShow:function(){a.noty.themes.defaultTheme.helpers.borderFix.apply(this)},onClose:function(){a.noty.themes.defaultTheme.helpers.borderFix.apply(this)}}},a.noty.themes.relax={name:"relax",helpers:{},modal:{css:{position:"fixed",width:"100%",height:"100%",backgroundColor:"#000",zIndex:1e4,opacity:.6,display:"none",left:0,top:0}},style:function(){switch(this.$bar.css({overflow:"hidden",margin:"4px 0",borderRadius:"2px"}),this.$message.css({fontSize:"14px",lineHeight:"16px",textAlign:"center",padding:"10px",width:"auto",position:"relative"}),this.$closeButton.css({position:"absolute",top:4,right:4,width:10,height:10,background:"url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAQAAAAnOwc2AAAAxUlEQVR4AR3MPUoDURSA0e++uSkkOxC3IAOWNtaCIDaChfgXBMEZbQRByxCwk+BasgQRZLSYoLgDQbARxry8nyumPcVRKDfd0Aa8AsgDv1zp6pYd5jWOwhvebRTbzNNEw5BSsIpsj/kurQBnmk7sIFcCF5yyZPDRG6trQhujXYosaFoc+2f1MJ89uc76IND6F9BvlXUdpb6xwD2+4q3me3bysiHvtLYrUJto7PD/ve7LNHxSg/woN2kSz4txasBdhyiz3ugPGetTjm3XRokAAAAASUVORK5CYII=)",display:"none",cursor:"pointer"}),this.$buttons.css({padding:5,textAlign:"right",borderTop:"1px solid #ccc",backgroundColor:"#fff"}),this.$buttons.find("button").css({marginLeft:5}),this.$buttons.find("button:first").css({marginLeft:0}),this.$bar.on({mouseenter:function(){a(this).find(".noty_close").stop().fadeTo("normal",1)},mouseleave:function(){a(this).find(".noty_close").stop().fadeTo("normal",0)}}),this.options.layout.name){case"top":this.$bar.css({borderBottom:"2px solid #eee",borderLeft:"2px solid #eee",borderRight:"2px solid #eee",borderTop:"2px solid #eee",boxShadow:"0 2px 4px rgba(0, 0, 0, 0.1)"});break;case"topCenter":case"center":case"bottomCenter":case"inline":this.$bar.css({border:"1px solid #eee",boxShadow:"0 2px 4px rgba(0, 0, 0, 0.1)"}),this.$message.css({fontSize:"13px",textAlign:"center"});break;case"topLeft":case"topRight":case"bottomLeft":case"bottomRight":case"centerLeft":case"centerRight":this.$bar.css({border:"1px solid #eee",boxShadow:"0 2px 4px rgba(0, 0, 0, 0.1)"}),this.$message.css({fontSize:"13px",textAlign:"left"});break;case"bottom":this.$bar.css({borderTop:"2px solid #eee",borderLeft:"2px solid #eee",borderRight:"2px solid #eee",borderBottom:"2px solid #eee",boxShadow:"0 -2px 4px rgba(0, 0, 0, 0.1)"});break;default:this.$bar.css({border:"2px solid #eee",boxShadow:"0 2px 4px rgba(0, 0, 0, 0.1)"})}switch(this.options.type){case"alert":case"notification":this.$bar.css({backgroundColor:"#FFF",borderColor:"#dedede",color:"#444"});break;case"warning":this.$bar.css({backgroundColor:"#FFEAA8",borderColor:"#FFC237",color:"#826200"}),this.$buttons.css({borderTop:"1px solid #FFC237"});break;case"error":this.$bar.css({backgroundColor:"#FF8181",borderColor:"#e25353",color:"#FFF"}),this.$message.css({fontWeight:"bold"}),this.$buttons.css({borderTop:"1px solid darkred"});break;case"information":this.$bar.css({backgroundColor:"#78C5E7",borderColor:"#3badd6",color:"#FFF"}),this.$buttons.css({borderTop:"1px solid #0B90C4"});break;case"success":this.$bar.css({backgroundColor:"#BCF5BC",borderColor:"#7cdd77",color:"darkgreen"}),this.$buttons.css({borderTop:"1px solid #50C24E"});break;default:this.$bar.css({backgroundColor:"#FFF",borderColor:"#CCC",color:"#444"})}},callback:{onShow:function(){},onClose:function(){}}}});

;
/* class.notice.js */

/* 1  */ /**
/* 2  *|  * Created by me664 on 1/14/15.
/* 3  *|  */
/* 4  */ (function($){
/* 5  */ 
/* 6  */ 
/* 7  */     var STNotice;
/* 8  */     STNotice = function () {
/* 9  */         var self = this;
/* 10 */ 
/* 11 */         this.make = function (text, type,layout) {
/* 12 */             console.log(layout);
/* 13 */             var n;
/* 14 */             if (typeof type == 'undefined') {
/* 15 */                 type = 'infomation';
/* 16 */             }
/* 17 */ 
/* 18 */             if(typeof layout=='undefined'){
/* 19 */                 layout='topRight';
/* 20 */             }
/* 21 */ 
/* 22 */             n = noty({
/* 23 */                 text: text,
/* 24 */                 layout: layout,
/* 25 */                 type: type,
/* 26 */                 animation: {
/* 27 */                     open: 'animated bounceInRight', // Animate.css class names
/* 28 */                     close: 'animated bounceOutRight', // Animate.css class names
/* 29 */                     easing: 'swing', // unavailable - no need
/* 30 */                     speed: 500 // unavailable - no need
/* 31 */                 },
/* 32 */                 theme: 'relax'
/* 33 */                 ,timeout: 6000
/* 34 */             });
/* 35 */         };
/* 36 */ 
/* 37 */         this.template = function (icon, html) {
/* 38 */             if (typeof icon != "undefined") {
/* 39 */                 icon = "<i class='fa fa-" + icon + "'></i>";
/* 40 */ 
/* 41 */             }
/* 42 */             return "<div class='st_notice_template'>" + icon + " <div class='display_table'>" + html + "</div>  </div>";
/* 43 */         }
/* 44 */     };
/* 45 */ 
/* 46 */     STNotice=new STNotice;
/* 47 */     window.STNotice = STNotice;
/* 48 */ 
/* 49 */     //if(typeof stanalytics!='undefined'){
/* 50 */     //

/* class.notice.js */

/* 51 */     //    for(i=0;i<stanalytics.noty.length;i++){
/* 52 */     //        var val=stanalytics.noty[i];
/* 53 */     //
/* 54 */     //        STNotice.make(STNotice.template(val.icon,val.message),val.type);
/* 55 */     //    }
/* 56 */     //}
/* 57 */     var i=0;
/* 58 */     function show_noty(i)
/* 59 */     {
/* 60 */         if(typeof stanalytics.noty=="undefined") return false;
/* 61 */         if(i>=stanalytics.noty.length) return false;
/* 62 */ 
/* 63 */         window.setTimeout(function(){
/* 64 */             var val=stanalytics.noty[i];
/* 65 */             var layout=stanalytics.noti_position;
/* 66 */             STNotice.make(STNotice.template(val.icon,val.message),val.type,layout);
/* 67 */ 
/* 68 */             i++;
/* 69 */             show_noty(i);
/* 70 */ 
/* 71 */         },500*i);
/* 72 */ 
/* 73 */     }
/* 74 */     if(typeof stanalytics!='undefined')
/* 75 */     show_noty(0);
/* 76 */ })(jQuery);

;
/* custom.js */

/* 1   */ jQuery(document).ready(function($){
/* 2   */ 
/* 3   */     "use strict";
/* 4   */ 
/* 5   */     $('ul.slimmenu').slimmenu({
/* 6   */         resizeWidth: '992',
/* 7   */         collapserTitle: 'Main Menu',
/* 8   */         animSpeed: 250,
/* 9   */         indentChildren: true,
/* 10  */         childrenIndenter: ''
/* 11  */     });
/* 12  */ 
/* 13  */ 
/* 14  */     // Countdown
/* 15  */     $('.countdown').each(function() {
/* 16  */         var count = $(this);
/* 17  */         var count = $(this);
/* 18  */         $(this).countdown({
/* 19  */             zeroCallback: function(options) {
/* 20  */                 var newDate = new Date(),
/* 21  */                     newDate = newDate.setHours(newDate.getHours() + 130);
/* 22  */ 
/* 23  */                 $(count).attr("data-countdown", newDate);
/* 24  */                 $(count).countdown({
/* 25  */                     unixFormat: true
/* 26  */                 });
/* 27  */             }
/* 28  */         });
/* 29  */     });
/* 30  */ 
/* 31  */ 
/* 32  */     $('.btn').button();
/* 33  */ 
/* 34  */     $("[rel='tooltip']").tooltip();
/* 35  */ 
/* 36  */     $('.form-group').each(function() {
/* 37  */         var self = $(this),
/* 38  */             input = self.find('input');
/* 39  */ 
/* 40  */         input.focus(function() {
/* 41  */             self.addClass('form-group-focus');
/* 42  */         })
/* 43  */ 
/* 44  */         input.blur(function() {
/* 45  */             if (input.val()) {
/* 46  */                 self.addClass('form-group-filled');
/* 47  */             } else {
/* 48  */                 self.removeClass('form-group-filled');
/* 49  */             }
/* 50  */             self.removeClass('form-group-focus');

/* custom.js */

/* 51  */         });
/* 52  */     });
/* 53  */ 
/* 54  */     $('.typeahead').typeahead({
/* 55  */             hint: true,
/* 56  */             highlight: true,
/* 57  */             minLength: 3,
/* 58  */             limit: 8
/* 59  */         }, {
/* 60  */             source: function(q, cb) {
/* 61  */                 return $.ajax({
/* 62  */                     dataType: 'json',
/* 63  */                     type: 'get',
/* 64  */                     url: 'http://gd.geobytes.com/AutoCompleteCity?callback=?&q=' + q,
/* 65  */                     chache: false,
/* 66  */                     success: function(data) {
/* 67  */                         var result = [];
/* 68  */                         $.each(data, function(index, val) {
/* 69  */                             result.push({
/* 70  */                                 value: val
/* 71  */                             });
/* 72  */                         });
/* 73  */                         cb(result);
/* 74  */                     }
/* 75  */                 });
/* 76  */             }
/* 77  */         });
/* 78  */ 
/* 79  */     $('.typeahead_location').typeahead({
/* 80  */             hint: true,
/* 81  */             highlight: true,
/* 82  */             minLength: 3,
/* 83  */             limit: 8
/* 84  */         },
/* 85  */         {
/* 86  */             source: function(q, cb) {
/* 87  */                 return $.ajax({
/* 88  */                     dataType: 'json',
/* 89  */                     type: 'get',
/* 90  */                     url: st_params.ajax_url,
/* 91  */                     data:{
/* 92  */                         security:st_params.st_search_nonce,
/* 93  */                         action:'st_search_location',
/* 94  */                         s:q
/* 95  */                     },
/* 96  */                     cache: true,
/* 97  */                     success: function(data) {
/* 98  */                         var result = [];
/* 99  */                         if(data.data){
/* 100 */                             $.each(data.data, function(index, val) {

/* custom.js */

/* 101 */                                 result.push({
/* 102 */                                     value: val.title,
/* 103 */                                     location_id:val.id,
/* 104 */                                     type_color:'success',
/* 105 */                                     type:'city'
/* 106 */                                 });
/* 107 */                             });
/* 108 */                             cb(result);
/* 109 */                         }
/* 110 */ 
/* 111 */                     }
/* 112 */                 });
/* 113 */             },
/* 114 */             templates: {
/* 115 */                 suggestion: Handlebars.compile('<p><label class="label label-{{type_color}}">{{type}}</label><strong> {{value}}</strong></p>')
/* 116 */             }
/* 117 */         });
/* 118 */     $('.typeahead_location').bind('typeahead:selected', function(obj, datum, name) {
/* 119 */         var parent=$(this).parents('.form-group');
/* 120 */         parent.find('.location_id').val(datum.location_id);
/* 121 */     });
/* 122 */     $('.typeahead_location').keyup(function(){
/* 123 */         var parent=$(this).parents('.form-group');
/* 124 */         parent.find('.location_id').val('');
/* 125 */     });
/* 126 */ 
/* 127 */     $('input.date-pick, .input-daterange, .date-pick-inline').datepicker({
/* 128 */         todayHighlight: true
/* 129 */     });
/* 130 */ 
/* 131 */ 
/* 132 */ 
/* 133 */ 
/* 134 */     //$('input.date-pick, .input-daterange input[name="start"]').datepicker(/*'setDate', 'today'*/);
/* 135 */     //$('.input-daterange input[name="end"]').datepicker(/*'setDate', '+1d'*/);
/* 136 */ 
/* 137 */ 
/* 138 */ 
/* 139 */     $('.input-daterange input[name="start"]').each(function(){
/* 140 */         var form=$(this).closest('form');
/* 141 */         var me=$(this);
/* 142 */         $(this).datepicker(
/* 143 */             'setStartDate','today'
/* 144 */         );
/* 145 */         $(this).datepicker().on('changeDate', function(e) {
/* 146 */ 
/* 147 */                 var new_date= e.date;
/* 148 */                 console.log(new_date);
/* 149 */                 new_date.setDate(new_date.getDate() + 1);
/* 150 */                 form.find('.input-daterange [name="end"]').datepicker('setStartDate',new_date);

/* custom.js */

/* 151 */             }
/* 152 */         );
/* 153 */ 
/* 154 */         form.find('.input-daterange [name="end"]').datepicker(
/* 155 */             'setStartDate','+1d'
/* 156 */         );
/* 157 */         form.find('.input-daterange [name="end"]').on('changeDate', function(e) {
/* 158 */ 
/* 159 */                 var new_date= e.date;
/* 160 */                 console.log(new_date);
/* 161 */                 new_date.setDate(new_date.getDate() - 1);
/* 162 */                 me.datepicker('setEndDate',new_date);
/* 163 */             }
/* 164 */         );
/* 165 */     })
/* 166 */ 
/* 167 */     $('.pick-up-date').each(function(){
/* 168 */         var form=$(this).closest('form');
/* 169 */         var me=$(this);
/* 170 */         $(this).datepicker(
/* 171 */             'setStartDate','today'
/* 172 */         );
/* 173 */         $(this).datepicker().on('changeDate', function(e) {
/* 174 */                 var new_date= e.date;
/* 175 */                 console.log(new_date);
/* 176 */                 new_date.setDate(new_date.getDate() + 1);
/* 177 */                 form.find('.drop-off-date').datepicker('setStartDate',new_date);
/* 178 */             }
/* 179 */         );
/* 180 */ 
/* 181 */         form.find('.drop-off-date').datepicker(
/* 182 */             'setStartDate','+1d'
/* 183 */         );
/* 184 */         form.find('.drop-off-date').on('changeDate', function(e) {
/* 185 */ 
/* 186 */                 var new_date= e.date;
/* 187 */                 console.log(new_date);
/* 188 */                 new_date.setDate(new_date.getDate() - 1);
/* 189 */                 me.datepicker('setEndDate',new_date);
/* 190 */             }
/* 191 */         );
/* 192 */     })
/* 193 */ 
/* 194 */     $('.tour_book_date').datepicker(
/* 195 */         'setStartDate','today'
/* 196 */     );
/* 197 */     $('.tour_book_date').datepicker(
/* 198 */         'setDate','today'
/* 199 */     );
/* 200 */ 

/* custom.js */

/* 201 */     /*$('.tour_book_date').datepicker(
/* 202 *|         'setStartDate',$('.tour_book_date').attr('data-start')
/* 203 *|     );
/* 204 *|     $('.tour_book_date').datepicker(
/* 205 *|         'setEndDate',$('.tour_book_date').attr('data-end')
/* 206 *|     );
/* 207 *|     $('.tour_book_date').datepicker(
/* 208 *|         'setDate',$('.tour_book_date').attr('')
/* 209 *|     );*/
/* 210 */ 
/* 211 */ 
/* 212 */     /*$('.pick-up-date').datepicker(
/* 213 *|         'setStartDate','today'
/* 214 *|     );
/* 215 *|     $('.drop-off-date').datepicker(
/* 216 *|         'setStartDate','+1d'
/* 217 *|     );
/* 218 *|     if($('.pick-up-date').val()){
/* 219 *|         console.log($('.pick-up-date').val());
/* 220 *|         $('.pick-up-date').datepicker('setDate', $('.pick-up-date').val());
/* 221 *|     }else{
/* 222 *|         $('.pick-up-date').datepicker('setDate', 'today');
/* 223 *|     }
/* 224 *|     if($('.drop-off-date').val()){
/* 225 *|         $('.drop-off-date').datepicker('setDate', $('.drop-off-date').val());
/* 226 *|     }else{
/* 227 *|         $('.drop-off-date').datepicker('setDate', '+1d');
/* 228 *|     }*/
/* 229 */ 
/* 230 */     $('input.time-pick').timepicker({
/* 231 */         minuteStep: 15,
/* 232 */         showInpunts: false
/* 233 */     })
/* 234 */ 
/* 235 */     $('input.date-pick-years').datepicker({
/* 236 */         startView: 2
/* 237 */     });
/* 238 */ 
/* 239 */ 
/* 240 */ 
/* 241 */ 
/* 242 */     $('.booking-item-price-calc .checkbox label').click(function() {
/* 243 */         var checkbox = $(this).find('input'),
/* 244 */             // checked = $(checkboxDiv).hasClass('checked'),
/* 245 */             checked = $(checkbox).prop('checked'),
/* 246 */             price = parseInt($(this).find('span.pull-right').html().replace('$', '')),
/* 247 */             eqPrice = $('#car-equipment-total'),
/* 248 */             tPrice = $('#car-total'),
/* 249 */             eqPriceInt = parseInt(eqPrice.attr('data-value')),
/* 250 */             tPriceInt = parseInt(tPrice.attr('data-value')),

/* custom.js */

/* 251 */             value,
/* 252 */             animateInt = function(val, el, plus) {
/* 253 */                 value = function() {
/* 254 */                     if (plus) {
/* 255 */                         return el.attr('data-value', val + price);
/* 256 */                     } else {
/* 257 */                         return el.attr('data-value', val - price);
/* 258 */                     }
/* 259 */                 };
/* 260 */                 return $({
/* 261 */                     val: val
/* 262 */                 }).animate({
/* 263 */                     val: parseInt(value().attr('data-value'))
/* 264 */                 }, {
/* 265 */                     duration: 500,
/* 266 */                     easing: 'swing',
/* 267 */                     step: function() {
/* 268 */                         if (plus) {
/* 269 */                             el.text(Math.ceil(this.val));
/* 270 */                         } else {
/* 271 */                             el.text(Math.floor(this.val));
/* 272 */                         }
/* 273 */                     }
/* 274 */                 });
/* 275 */             };
/* 276 */         if (!checked) {
/* 277 */             animateInt(eqPriceInt, eqPrice, true);
/* 278 */             animateInt(tPriceInt, tPrice, true);
/* 279 */         } else {
/* 280 */             animateInt(eqPriceInt, eqPrice, false);
/* 281 */             animateInt(tPriceInt, tPrice, false);
/* 282 */         }
/* 283 */     });
/* 284 */ 
/* 285 */ 
/* 286 */     $('div.bg-parallax').each(function() {
/* 287 */         var $obj = $(this);
/* 288 */         if($(window).width() > 992 ){
/* 289 */             $(window).scroll(function() {
/* 290 */                 var animSpeed;
/* 291 */                 if ($obj.hasClass('bg-blur')) {
/* 292 */                     animSpeed = 10;
/* 293 */                 } else {
/* 294 */                     animSpeed = 15;
/* 295 */                 }
/* 296 */                 var yPos = -($(window).scrollTop() / animSpeed);
/* 297 */                 var bgpos = '50% ' + yPos + 'px';
/* 298 */                 $obj.css('background-position', bgpos);
/* 299 */ 
/* 300 */             });

/* custom.js */

/* 301 */         }
/* 302 */     });
/* 303 */ 
/* 304 */ 
/* 305 */ 
/* 306 */     $(document).ready(
/* 307 */         function() {
/* 308 */ 
/* 309 */ 
/* 310 */ 
/* 311 */ 
/* 312 */             // Owl Carousel
/* 313 */             var owlCarousel = $('#owl-carousel'),
/* 314 */                 owlItems = owlCarousel.attr('data-items'),
/* 315 */                 owlCarouselSlider = $('#owl-carousel-slider'),
/* 316 */                 owlNav = owlCarouselSlider.attr('data-nav');
/* 317 */             // owlSliderPagination = owlCarouselSlider.attr('data-pagination');
/* 318 */ 
/* 319 */             owlCarousel.owlCarousel({
/* 320 */                 items: owlItems,
/* 321 */                 navigation: true,
/* 322 */                 navigationText: ['', '']
/* 323 */             });
/* 324 */ 
/* 325 */             owlCarouselSlider.owlCarousel({
/* 326 */                 slideSpeed: 300,
/* 327 */                 paginationSpeed: 400,
/* 328 */                 // pagination: owlSliderPagination,
/* 329 */                 singleItem: true,
/* 330 */                 navigation: true,
/* 331 */                 navigationText: ['', ''],
/* 332 */                 transitionStyle: 'fade',
/* 333 */                 autoPlay: 4500
/* 334 */             });
/* 335 */ 
/* 336 */ 
/* 337 */         // footer always on bottom
/* 338 */         var docHeight = $(window).height();
/* 339 */        var footerHeight = $('#main-footer').height();
/* 340 */        var footerTop = $('#main-footer').position().top + footerHeight;
/* 341 */ 
/* 342 */        if (footerTop < docHeight) {
/* 343 */         $('#main-footer').css('margin-top', (docHeight - footerTop) + 'px');
/* 344 */        }
/* 345 */         }
/* 346 */ 
/* 347 */ 
/* 348 */     );
/* 349 */     $(document).ready(function(){
/* 350 */         $('#slide-testimonial').each(function(){

/* custom.js */

/* 351 */             var $this = $(this);
/* 352 */             $this.owlCarousel({
/* 353 */                 slideSpeed: $this.attr('data-speed'),
/* 354 */                 paginationSpeed: 400,
/* 355 */                 // pagination: owlSliderPagination,
/* 356 */                 singleItem: true,
/* 357 */                 navigation: true,
/* 358 */                 navigationText: ['', ''],
/* 359 */                 transitionStyle: 'fade',
/* 360 */                 autoPlay: $this.attr('data-play')
/* 361 */             });
/* 362 */         })
/* 363 */     });
/* 364 */ 
/* 365 */ 
/* 366 */     $('.nav-drop').click(function(){
/* 367 */         if($(this).hasClass('active-drop'))
/* 368 */         {
/* 369 */             $(this).removeClass('active-drop');
/* 370 */         }else
/* 371 */         {
/* 372 */             $('.nav-drop').removeClass('active-drop');
/* 373 */             $(this).addClass('active-drop');
/* 374 */ 
/* 375 */         }
/* 376 */     });
/* 377 */ 
/* 378 */ 
/* 379 */     $(document).mouseup(function (e)
/* 380 */     {
/* 381 */         var container = $(".nav-drop");
/* 382 */ 
/* 383 */         if (!container.is(e.target) // if the target of the click isn't the container...
/* 384 */             && container.has(e.target).length === 0) // ... nor a descendant of the container
/* 385 */         {
/* 386 */             $('.nav-drop').removeClass('active-drop');
/* 387 */         }
/* 388 */     });
/* 389 */ 
/* 390 */     $(".price-slider").each(function(){
/* 391 */         var min=$(this).data('min');
/* 392 */         var max=$(this).data('max');
/* 393 */         var step=$(this).data('step');
/* 394 */ 
/* 395 */         var value=$(this).val();
/* 396 */ 
/* 397 */         var from=value.split(';');
/* 398 */ 
/* 399 */         var to=from[1]
/* 400 */         from=from[0];

/* custom.js */

/* 401 */ 
/* 402 */         var arg={
/* 403 */             min: min,
/* 404 */             max: max,
/* 405 */             type: 'double',
/* 406 */             prefix: "$",
/* 407 */             // maxPostfix: "+",
/* 408 */             prettify: false,
/* 409 */             grid: true,
/* 410 */             step:step,
/* 411 */             grid_snap:true,
/* 412 */             onFinish:function(data){
/* 413 */                 //console.log(data);
/* 414 */                 //console.log(window.location.href);
/* 415 */             },
/* 416 */             from:from,
/* 417 */             to:to
/* 418 */         };
/* 419 */ 
/* 420 */         if(!step){
/* 421 */             delete arg.step;
/* 422 */             delete arg.grid_snap;
/* 423 */         }
/* 424 */ 
/* 425 */         console.log(arg);
/* 426 */ 
/* 427 */         //console.log(min);
/* 428 */         $(this).ionRangeSlider(arg);
/* 429 */     });
/* 430 */     $("#price-slider").ionRangeSlider({
/* 431 */         min: 130,
/* 432 */         max: 575,
/* 433 */         type: 'double',
/* 434 */         prefix: "$",
/* 435 */         // maxPostfix: "+",
/* 436 */         prettify: false,
/* 437 */         grid: true
/* 438 */     });
/* 439 */ 
/* 440 */     $('.i-check, .i-radio').iCheck({
/* 441 */         checkboxClass: 'i-check',
/* 442 */         radioClass: 'i-radio'
/* 443 */     });
/* 444 */ 
/* 445 */ 
/* 446 */ 
/* 447 */     $('.booking-item-review-expand').click(function(event) {
/* 448 */         var parent = $(this).parent('.booking-item-review-content');
/* 449 */         if (parent.hasClass('expanded')) {
/* 450 */             parent.removeClass('expanded');

/* custom.js */

/* 451 */         } else {
/* 452 */             parent.addClass('expanded');
/* 453 */         }
/* 454 */     });
/* 455 */     $('.expand_search_box').click(function(event) {
/* 456 */         var parent = $(this).parent('.search_advance');
/* 457 */         if (parent.hasClass('expanded')) {
/* 458 */             parent.removeClass('expanded');
/* 459 */         } else {
/* 460 */             parent.addClass('expanded');
/* 461 */         }
/* 462 */     });
/* 463 */ 
/* 464 */ 
/* 465 */     $('.stats-list-select > li > .booking-item-rating-stars > li').each(function() {
/* 466 */         var list = $(this).parent(),
/* 467 */             listItems = list.children(),
/* 468 */             itemIndex = $(this).index(),
/* 469 */              parentItem=list.parent();
/* 470 */ 
/* 471 */         $(this).hover(function() {
/* 472 */             for (var i = 0; i < listItems.length; i++) {
/* 473 */                 if (i <= itemIndex) {
/* 474 */                     $(listItems[i]).addClass('hovered');
/* 475 */                 } else {
/* 476 */                     break;
/* 477 */                 }
/* 478 */             };
/* 479 */             $(this).click(function() {
/* 480 */                 for (var i = 0; i < listItems.length; i++) {
/* 481 */                     if (i <= itemIndex) {
/* 482 */                         $(listItems[i]).addClass('selected');
/* 483 */                     } else {
/* 484 */                         $(listItems[i]).removeClass('selected');
/* 485 */                     }
/* 486 */                 };
/* 487 */ 
/* 488 */                 parentItem.children('.st_review_stats').val(itemIndex+1);
/* 489 */ 
/* 490 */             });
/* 491 */         }, function() {
/* 492 */             listItems.removeClass('hovered');
/* 493 */         });
/* 494 */     });
/* 495 */ 
/* 496 */ 
/* 497 */ 
/* 498 */     $('.booking-item-container').children('.booking-item').click(function(event) {
/* 499 */         if ($(this).hasClass('active')) {
/* 500 */             $(this).removeClass('active');

/* custom.js */

/* 501 */             $(this).parent().removeClass('active');
/* 502 */         } else {
/* 503 */             $(this).addClass('active');
/* 504 */             $(this).parent().addClass('active');
/* 505 */             $(this).delay(1500).queue(function() {
/* 506 */                 $(this).addClass('viewed')
/* 507 */             });
/* 508 */         }
/* 509 */     });
/* 510 */ 
/* 511 */ 
/* 512 */     //$('.form-group-cc-number input').payment('formatCardNumber');
/* 513 */     //$('.form-group-cc-date input').payment('formatCardExpiry');
/* 514 */     //$('.form-group-cc-cvc input').payment('formatCardCVC');
/* 515 */ 
/* 516 */ 
/* 517 */ 
/* 518 */ 
/* 519 */     if ($('#map-canvas').length) {
/* 520 */         var map,
/* 521 */             service;
/* 522 */ 
/* 523 */         jQuery(function($) {
/* 524 */             $(document).ready(function() {
/* 525 */                 var latlng = new google.maps.LatLng(40.7564971, -73.9743277);
/* 526 */                 var myOptions = {
/* 527 */                     zoom: 16,
/* 528 */                     center: latlng,
/* 529 */                     mapTypeId: google.maps.MapTypeId.ROADMAP,
/* 530 */                     scrollwheel: false
/* 531 */                 };
/* 532 */ 
/* 533 */                 map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
/* 534 */ 
/* 535 */ 
/* 536 */                 var marker = new google.maps.Marker({
/* 537 */                     position: latlng,
/* 538 */                     map: map
/* 539 */                 });
/* 540 */                 marker.setMap(map);
/* 541 */ 
/* 542 */ 
/* 543 */                 $('a[href="#google-map-tab"]').on('shown.bs.tab', function(e) {
/* 544 */                     google.maps.event.trigger(map, 'resize');
/* 545 */                     map.setCenter(latlng);
/* 546 */                 });
/* 547 */             });
/* 548 */         });
/* 549 */     }
/* 550 */ 

/* custom.js */

/* 551 */ 
/* 552 */     $('.card-select > li').click(function() {
/* 553 */          var self = this;
/* 554 */         $(self).addClass('card-item-selected');
/* 555 */         $(self).siblings('li').removeClass('card-item-selected');
/* 556 */         $('.form-group-cc-number input').click(function() {
/* 557 */             $(self).removeClass('card-item-selected');
/* 558 */         });
/* 559 */     });
/* 560 */     // Lighbox gallery
/* 561 */     $('#popup-gallery').each(function() {
/* 562 */         $(this).magnificPopup({
/* 563 */             delegate: 'a.popup-gallery-image',
/* 564 */             type: 'image',
/* 565 */             gallery: {
/* 566 */                 enabled: true
/* 567 */             }
/* 568 */         });
/* 569 */     });
/* 570 */ 
/* 571 */     $('.st-popup-gallery').each(function() {
/* 572 */         $(this).magnificPopup({
/* 573 */             delegate: '.st-gp-item',
/* 574 */             type: 'image',
/* 575 */             gallery: {
/* 576 */                 enabled: true
/* 577 */             }
/* 578 */         });
/* 579 */     });
/* 580 */ 
/* 581 */     // Lighbox image
/* 582 */     $('.popup-image').magnificPopup({
/* 583 */         type: 'image'
/* 584 */     });
/* 585 */ 
/* 586 */     // Lighbox text
/* 587 */     $('.popup-text').magnificPopup({
/* 588 */         removalDelay: 500,
/* 589 */         closeBtnInside: true,
/* 590 */         callbacks: {
/* 591 */             beforeOpen: function() {
/* 592 */                 this.st.mainClass = this.st.el.attr('data-effect');
/* 593 */             }
/* 594 */         },
/* 595 */         midClick: true
/* 596 */     });
/* 597 */ 
/* 598 */     // Lightbox iframe
/* 599 */     $('.popup-iframe').magnificPopup({
/* 600 */         dispableOn: 700,

/* custom.js */

/* 601 */         type: 'iframe',
/* 602 */         removalDelay: 160,
/* 603 */         mainClass: 'mfp-fade',
/* 604 */         preloader: false
/* 605 */     });
/* 606 */ 
/* 607 */ 
/* 608 */     $('.form-group-select-plus').each(function() {
/* 609 */         var self = $(this),
/* 610 */             btnGroup = self.find('.btn-group').first(),
/* 611 */             select = self.find('select');
/* 612 */         btnGroup.children('label').last().click(function() {
/* 613 */             btnGroup.addClass('hidden');
/* 614 */             select.removeClass('hidden');
/* 615 */         });
/* 616 */         btnGroup.children('label').click(function(){
/* 617 */             var c=$(this);
/* 618 */             select.find('option[value='+ c.children('input').val()+']').prop('selected','selected');
/* 619 */             if(!c.hasClass('active'))
/* 620 */             select.trigger('change');
/* 621 */         });
/* 622 */     });
/* 623 */     // Responsive videos
/* 624 */     $(document).ready(function() {
/* 625 */         //$("body").fitVids();
/* 626 */     });
/* 627 */ 
/* 628 */     //$(function($) {
/* 629 */     //    $("#twitter").tweet({
/* 630 */     //        username: "remtsoy", //!paste here your twitter username!
/* 631 */     //        count: 3
/* 632 */     //    });
/* 633 */     //});
/* 634 */ 
/* 635 */     //$(function($) {
/* 636 */     //    $("#twitter-ticker").tweet({
/* 637 */     //        username: "remtsoy", //!paste here your twitter username!
/* 638 */     //        page: 1,
/* 639 */     //        count: 20
/* 640 */     //    });
/* 641 */     //});
/* 642 */ 
/* 643 */     $(document).ready(function() {
/* 644 */         var ul = $('#twitter-ticker').find(".tweet-list");
/* 645 */         var ticker = function() {
/* 646 */             setTimeout(function() {
/* 647 */                 ul.find('li:first').animate({
/* 648 */                     marginTop: '-4.7em'
/* 649 */                 }, 850, function() {
/* 650 */                     $(this).detach().appendTo(ul).removeAttr('style');

/* custom.js */

/* 651 */                 });
/* 652 */                 ticker();
/* 653 */             }, 5000);
/* 654 */         };
/* 655 */         ticker();
/* 656 */     });
/* 657 */     $(function() {
/* 658 */         var $girl_ri = $('#ri-grid');
/* 659 */         if($.fn.gridrotator!== undefined){
/* 660 */             $girl_ri.gridrotator({
/* 661 */                 rows: $girl_ri.attr('data-row'),
/* 662 */                 columns: $girl_ri.attr('data-col'),
/* 663 */                 animType: 'random',
/* 664 */                 animSpeed: 1200,
/* 665 */                 interval: $girl_ri.attr('data-speed'),
/* 666 */                 step: 'random',
/* 667 */                 preventClick: false,
/* 668 */                 maxStep: 2,
/* 669 */                 w992: {
/* 670 */                     rows: 5,
/* 671 */                     columns: 4
/* 672 */                 },
/* 673 */                 w768: {
/* 674 */                     rows: 6,
/* 675 */                     columns: 3
/* 676 */                 },
/* 677 */                 w480: {
/* 678 */                     rows: 8,
/* 679 */                     columns: 3
/* 680 */                 },
/* 681 */                 w320: {
/* 682 */                     rows: 5,
/* 683 */                     columns: 4
/* 684 */                 },
/* 685 */                 w240: {
/* 686 */                     rows: 6,
/* 687 */                     columns: 4
/* 688 */                 }
/* 689 */             });
/* 690 */         }
/* 691 */ 
/* 692 */ 
/* 693 */     });
/* 694 */ 
/* 695 */ 
/* 696 */     $(function() {
/* 697 */         if($.fn.gridrotator!== undefined) {
/* 698 */             $('#ri-grid-no-animation').gridrotator({
/* 699 */                 rows: 4,
/* 700 */                 columns: 8,

/* custom.js */

/* 701 */                 slideshow: false,
/* 702 */                 w1024: {
/* 703 */                     rows: 4,
/* 704 */                     columns: 6
/* 705 */                 },
/* 706 */                 w768: {
/* 707 */                     rows: 3,
/* 708 */                     columns: 3
/* 709 */                 },
/* 710 */                 w480: {
/* 711 */                     rows: 4,
/* 712 */                     columns: 4
/* 713 */                 },
/* 714 */                 w320: {
/* 715 */                     rows: 5,
/* 716 */                     columns: 4
/* 717 */                 },
/* 718 */                 w240: {
/* 719 */                     rows: 6,
/* 720 */                     columns: 4
/* 721 */                 }
/* 722 */             });
/* 723 */         }
/* 724 */ 
/* 725 */     });
/* 726 */ 
/* 727 */     var tid = setInterval(tagline_vertical_slide, 2500);
/* 728 */ 
/* 729 */     // vertical slide
/* 730 */     function tagline_vertical_slide() {
/* 731 */         var curr = $("#tagline ul li.active");
/* 732 */         curr.removeClass("active").addClass("vs-out");
/* 733 */         setTimeout(function() {
/* 734 */             curr.removeClass("vs-out");
/* 735 */         }, 500);
/* 736 */ 
/* 737 */         var nextTag = curr.next('li');
/* 738 */         if (!nextTag.length) {
/* 739 */             nextTag = $("#tagline ul li").first();
/* 740 */         }
/* 741 */         nextTag.addClass("active");
/* 742 */     }
/* 743 */ 
/* 744 */     function abortTimer() { // to be called when you want to stop the timer
/* 745 */         clearInterval(tid);
/* 746 */     }
/* 747 */ 
/* 748 */     $('#comments #submit').addClass('btn btn-primary');
/* 749 */ 
/* 750 */ 

/* custom.js */

/* 751 */     //Button Like Review
/* 752 */     $('.st-like-review').click(function(e){
/* 753 */ 
/* 754 */         e.preventDefault();
/* 755 */ 
/* 756 */         var me=$(this);
/* 757 */ 
/* 758 */ 
/* 759 */         if(!me.hasClass('loading'))
/* 760 */         {
/* 761 */             var comment_id=me.data('id');
/* 762 */             var loading=$('<i class="loading_icon fa fa-spinner fa-spin"></i>');
/* 763 */ 
/* 764 */             me.addClass('loading');
/* 765 */             me.before(loading);
/* 766 */ 
/* 767 */             $.ajax({
/* 768 */ 
/* 769 */                 url:st_params.ajax_url,
/* 770 */                 type:'post',
/* 771 */                 dataType:'json',
/* 772 */                 data:{
/* 773 */                   action:'like_review',
/* 774 */                   comment_ID:comment_id
/* 775 */                 },
/* 776 */                 success:function(res)
/* 777 */                 {
/* 778 */                     if(res.status)
/* 779 */                     {
/* 780 */                         if(res.data.like_status)
/* 781 */                         {
/* 782 */                             me.addClass('fa-thumbs-o-down').removeClass('fa-thumbs-o-up');
/* 783 */                         }else
/* 784 */                         {
/* 785 */                             me.addClass('fa-thumbs-o-up').removeClass('fa-thumbs-o-down');
/* 786 */                         }
/* 787 */ 
/* 788 */                         if(typeof res.data.like_count!=undefined)
/* 789 */                         {
/* 790 */                             res.data.like_count=parseInt(res.data.like_count);
/* 791 */                             me.next('.text-color').html(' '+res.data.like_count);
/* 792 */                         }
/* 793 */                     }else
/* 794 */                     {
/* 795 */                         if(res.error.error_message)
/* 796 */                         {
/* 797 */                             alert(res.error.error_message);
/* 798 */                         }
/* 799 */                     }
/* 800 */                     me.removeClass('loading');

/* custom.js */

/* 801 */                     loading.remove();
/* 802 */                 },
/* 803 */                 error:function(res){
/* 804 */                     console.log(res);
/* 805 */                     alert('Ajax Faild');
/* 806 */                     me.removeClass('loading');
/* 807 */                     loading.remove();
/* 808 */                 }
/* 809 */             });
/* 810 */         }
/* 811 */ 
/* 812 */ 
/* 813 */     });
/* 814 */ 
/* 815 */     //Button Like Review
/* 816 */     $('.st-like-comment').click(function(e){
/* 817 */ 
/* 818 */         e.preventDefault();
/* 819 */ 
/* 820 */         var me=$(this);
/* 821 */ 
/* 822 */ 
/* 823 */         if(!me.hasClass('loading'))
/* 824 */         {
/* 825 */             var comment_id=me.data('id');
/* 826 */             var loading=$('<i class="loading_icon fa fa-spinner fa-spin"></i>');
/* 827 */ 
/* 828 */             me.addClass('loading');
/* 829 */             me.before(loading);
/* 830 */ 
/* 831 */             $.ajax({
/* 832 */ 
/* 833 */                 url:st_params.ajax_url,
/* 834 */                 type:'post',
/* 835 */                 dataType:'json',
/* 836 */                 data:{
/* 837 */                     action:'like_review',
/* 838 */                     comment_ID:comment_id
/* 839 */                 },
/* 840 */                 success:function(res)
/* 841 */                 {
/* 842 */                     console.log(res);
/* 843 */                     if(res.status)
/* 844 */                     {
/* 845 */                         if(res.data.like_status)
/* 846 */                         {
/* 847 */                             me.addClass('fa-heart').removeClass('fa-heart-o');
/* 848 */                         }else
/* 849 */                         {
/* 850 */                             me.addClass('fa-heart-o').removeClass('fa-heart');

/* custom.js */

/* 851 */                         }
/* 852 */ 
/* 853 */                         if(typeof res.data.like_count!=undefined)
/* 854 */                         {
/* 855 */                             res.data.like_count=parseInt(res.data.like_count);
/* 856 */                             me.next('.text-color').html(' '+res.data.like_count);
/* 857 */                         }
/* 858 */                     }else
/* 859 */                     {
/* 860 */                         if(res.error.error_message)
/* 861 */                         {
/* 862 */                             alert(res.error.error_message);
/* 863 */                         }
/* 864 */                     }
/* 865 */                     me.removeClass('loading');
/* 866 */                     loading.remove();
/* 867 */                 },
/* 868 */                 error:function(res){
/* 869 */                     console.log(res);
/* 870 */                     alert('Ajax Faild');
/* 871 */                     me.removeClass('loading');
/* 872 */                     loading.remove();
/* 873 */                 }
/* 874 */             });
/* 875 */         }
/* 876 */ 
/* 877 */ 
/* 878 */     });
/* 879 */ 
/* 880 */ 
/* 881 */ 
/* 882 */ 
/* 883 */     // vc-element cars
/* 884 */     $('.singe_cars .iCheck-helper').click(function(){
/* 885 */         var price_total_item = 0;
/* 886 */         var person_ob = new Object();
/* 887 */        $('.singe_cars').find('.equipment').each(function(event){
/* 888 */             if($(this)[0].checked == true){
/* 889 */                 person_ob[ $(this).attr('data-title') ] = str2num($(this).attr('data-price'));
/* 890 */                 price_total_item = price_total_item + str2num($(this).attr('data-price'));
/* 891 */             }
/* 892 */         });
/* 893 */         $('.data_price_items').val( JSON.stringify(person_ob) );
/* 894 */ 
/* 895 */         var price_total = price_total_item + str2num($('.st_cars_price').attr('data-value'));
/* 896 */         $.ajax({
/* 897 */             url: st_params.ajax_url,
/* 898 */             type: "POST",
/* 899 */             data: {
/* 900 */                 action           : "st_price_cars",

/* custom.js */

/* 901 */                 price_total_item : price_total_item,
/* 902 */                 price_total      : price_total
/* 903 */             },
/* 904 */             dataType: "json",
/* 905 */             beforeSend: function() {
/* 906 */                 $('.cars_price_img_loading ').html('<div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div>');
/* 907 */             }
/* 908 */         }).done(function( html ) {
/* 909 */             $('.st_data_car_equipment_total').attr('data-value',html.price_total_item_number).html(html.price_total_item_text);
/* 910 */             $('.data_price_total').val(html.price_total_number);
/* 911 */             $('.st_data_car_total').html(html.price_total_text);
/* 912 */             $('.cars_price_img_loading ').html('');
/* 913 */         });
/* 914 */     });
/* 915 */     function str2num(val)
/* 916 */     {
/* 917 */         val = '0' + val;
/* 918 */         val = parseFloat(val);
/* 919 */         return val;
/* 920 */     }
/* 921 */ 
/* 922 */     $('.share li>a').click(function(){
/* 923 */         var href=$(this).attr('href');
/* 924 */         if(href && $(this).hasClass('no-open')==false){
/* 925 */ 
/* 926 */ 
/* 927 */             popupwindow(href,'',600,600);
/* 928 */             return false;
/* 929 */         }
/* 930 */     });
/* 931 */ 
/* 932 */     function popupwindow(url, title, w, h) {
/* 933 */         var left = (screen.width/2)-(w/2);
/* 934 */         var top = (screen.height/2)-(h/2);
/* 935 */         return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
/* 936 */     }
/* 937 */ 
/* 938 */     $('.social_login_nav_drop .login_social_link').click(function(){
/* 939 */             var href=$(this).attr('href');
/* 940 */ 
/* 941 */             popupwindow(href,'',600,450);
/* 942 */             return false;
/* 943 */         }
/* 944 */ 
/* 945 */     );
/* 946 */ 
/* 947 */ 
/* 948 */     // Reload for social window login
/* 949 */ 
/* 950 */ });

/* custom.js */

/* 951 */ 
/* 952 */ 

;
/* custom2.js */

/* 1   */ /**
/* 2   *|  * Created by me664 on 1/13/15.
/* 3   *|  */
/* 4   */ jQuery(document).ready(function($){
/* 5   */     if(typeof $.fn.sticky!='undefined'){
/* 6   */         var topSpacing=0;
/* 7   */         if($(window).width()>481 && $('body').hasClass('admin-bar')){
/* 8   */             topSpacing=$('#wpadminbar').height();
/* 9   */         }
/* 10  */         console.log(topSpacing);
/* 11  */ 
/* 12  */         $('.enable_sticky_menu .main_menu_wrap').sticky({topSpacing:topSpacing});
/* 13  */     }
/* 14  */ 
/* 15  */ 
/* 16  */     if($('body').hasClass('search_enable_preload'))
/* 17  */     {
/* 18  */         window.setTimeout(function(){
/* 19  */             $('.full-page-absolute').fadeOut().addClass('.hidden');
/* 20  */         },3000);
/* 21  */     }
/* 22  */     /*Begin gotop*/
/* 23  */     $('#gotop').click(function(){
/* 24  */         $("body,html").animate({
/* 25  */             scrollTop:0
/* 26  */         },1000,function(){
/* 27  */             $('#gotop').fadeOut();
/* 28  */         });
/* 29  */     });
/* 30  */ 
/* 31  */     $(window).scroll(function(){
/* 32  */         var scrolltop=$(window).scrollTop();
/* 33  */ 
/* 34  */         if(scrolltop>200){
/* 35  */             $('#gotop').fadeIn();
/* 36  */         }else{
/* 37  */             $('#gotop').fadeOut();
/* 38  */         }
/* 39  */ 
/* 40  */     });
/* 41  */ 
/* 42  */     var top_ajax_search=$('.st-top-ajax-search');
/* 43  */ 
/* 44  */     top_ajax_search.typeahead({
/* 45  */             hint: true,
/* 46  */             highlight: true,
/* 47  */             minLength: 3,
/* 48  */             limit: 8
/* 49  */         },
/* 50  */         {

/* custom2.js */

/* 51  */             source: function(q, cb) {
/* 52  */                 $('.st-top-ajax-search').parent().addClass('loading');
/* 53  */                 return $.ajax({
/* 54  */                     dataType: 'json',
/* 55  */                     type: 'get',
/* 56  */                     url: st_params.ajax_url,
/* 57  */                     data:{
/* 58  */                         security:st_params.st_search_nonce,
/* 59  */                         action:'st_top_ajax_search',
/* 60  */                         s:q
/* 61  */                     },
/* 62  */                     cache:true,
/* 63  */                     success: function(data) {
/* 64  */ 					
/* 65  */                         $('.st-top-ajax-search').parent().removeClass('loading');
/* 66  */                         var result = [];
/* 67  */                         if(data.data){
/* 68  */                             $.each(data.data, function(index, val) {
/* 69  */                                 result.push({
/* 70  */                                     value: val.title,
/* 71  */                                     location_id:val.id,
/* 72  */                                     type_color:'success',
/* 73  */                                     type:val.type,
/* 74  */                                     url:val.url
/* 75  */                                 });
/* 76  */ 								
/* 77  */                             });
/* 78  */                             cb(result);
/* 79  */                             console.log(result);
/* 80  */                         }
/* 81  */ $('.tt-suggestion').each(function(){
/* 82  */ 	if($(this).find('.label-success').html() !='Tour'){
/* 83  */ 		$(this).css('display','none')
/* 84  */ 	}
/* 85  */ });
/* 86  */                     },
/* 87  */                     error:function(e){
/* 88  */                         $('.st-top-ajax-search').parent().removeClass('loading');
/* 89  */                     }
/* 90  */                 });
/* 91  */             },
/* 92  */             templates: {
/* 93  */                 suggestion: Handlebars.compile('<p class="search-line-item"><label class="label label-{{type_color}}">{{type}}</label><strong> {{value}}</strong></p>')
/* 94  */             }
/* 95  */         });
/* 96  */ 
/* 97  */     top_ajax_search.bind('typeahead:selected', function(obj, datum, name) {
/* 98  */         if(datum.url){
/* 99  */             window.location.href=datum.url;
/* 100 */         }

/* custom2.js */

/* 101 */     });
/* 102 */ 
/* 103 */     // Single Tours
/* 104 */     $('.st_tour_number').change(function(){
/* 105 */         var $number = $(this).val();
/* 106 */         $('.st_tour_number').val($number);
/* 107 */     });
/* 108 */     $('.tour_book_date').change(function(){
/* 109 */         var $number = $(this).val();
/* 110 */         $('.tour_book_date').val($number);
/* 111 */     });
/* 112 */     $('.st_tour_adult').change(function(){
/* 113 */         var $number = $(this).val();
/* 114 */         $('.st_tour_adult').val($number);
/* 115 */     });
/* 116 */ 
/* 117 */     $('.st_tour_children').change(function(){
/* 118 */         var $number = $(this).val();
/* 119 */         $('.st_tour_children').val($number);
/* 120 */     });
/* 121 */ 
/* 122 */ });

;
/* user.js */

/* 1   */ jQuery(document).ready(function($){
/* 2   */     $('.btn_add_wishlist').click(function(){
/* 3   */         console.log(st_params.ajax_url);
/* 4   */         $.ajax({
/* 5   */             url: st_params.ajax_url,
/* 6   */             type: "POST",
/* 7   */             data: {
/* 8   */                 action      : "st_add_wishlist",
/* 9   */                 data_id     : $(this).data('id'),
/* 10  */                 data_type   : $(this).data('type')
/* 11  */             },
/* 12  */             dataType: "json",
/* 13  */             beforeSend: function() {
/* 14  */ 
/* 15  */             }
/* 16  */         }).done(function( html ) {
/* 17  */             $('.btn_add_wishlist').html(html.icon).attr("data-original-title",html.title);
/* 18  */         });
/* 19  */     });
/* 20  */ 
/* 21  */     $('.btn_remove_wishlist').click(function(){
/* 22  */         var $this = $(this);
/* 23  */         $.ajax({
/* 24  */             url: st_params.ajax_url,
/* 25  */             type: "POST",
/* 26  */             data: {
/* 27  */                 action      : "st_remove_wishlist",
/* 28  */                 data_id     : $(this).data('id'),
/* 29  */                 data_type   : $(this).data('type')
/* 30  */             },
/* 31  */             dataType: "json",
/* 32  */             beforeSend: function() {
/* 33  */                 $('.post-'+$this.attr('data-id')+' .user_img_loading').show();
/* 34  */             }
/* 35  */         }).done(function( html ) {
/* 36  */             if(html.status == 'true'){
/* 37  */                 $('.post-'+html.msg).html( console_msg(html.type , html.content ) ).attr("data-original-title",html.title);;
/* 38  */             }else{
/* 39  */                 $('.post-'+html.msg).append( console_msg(html.type , html.content ) ).attr("data-original-title",html.title);;
/* 40  */             }
/* 41  */         });
/* 42  */     });
/* 43  */     $('.btn_load_more_wishlist').click(function(){
/* 44  */         var $this = $(this);
/* 45  */         var txt_me =$this.html();
/* 46  */         $.ajax({
/* 47  */             url: st_params.ajax_url,
/* 48  */             type: "GET",
/* 49  */             data: {
/* 50  */                 action      : "st_load_more_wishlist",

/* user.js */

/* 51  */                 data_per    : $('.btn_load_more_wishlist').attr('data-per'),
/* 52  */                 data_next    : $('.btn_load_more_wishlist').attr('data-next')
/* 53  */             },
/* 54  */             dataType: "json",
/* 55  */             beforeSend: function() {
/* 56  */                 $this.html('Loading...');
/* 57  */             }
/* 58  */         }).done(function( html ) {
/* 59  */             $this.html(txt_me);
/* 60  */             $('#data_whislist').append(html.msg);
/* 61  */             if(html.status == 'true'){
/* 62  */                 console.log(html);
/* 63  */                 $('.btn_load_more_wishlist').attr('data-per' , html.data_per );
/* 64  */             }else{
/* 65  */                 $('.btn_load_more_wishlist').attr('disabled','disabled');
/* 66  */                 $('.btn_load_more_wishlist').html('No More');
/* 67  */             }
/* 68  */ 
/* 69  */         });
/* 70  */     });
/* 71  */ 
/* 72  */     $('#btn_add_media').click(function(){
/* 73  */         $('#my_image_upload').click();
/* 74  */     });
/* 75  */     $('#my_image_upload').change(function(){
/* 76  */         $('#submit_my_image_upload').click();
/* 77  */     });
/* 78  */ 
/* 79  */     $('.btn_remove_post_type').click(function(){
/* 80  */         var $this = $(this);
/* 81  */         $.ajax({
/* 82  */             url: st_params.ajax_url,
/* 83  */             type: "POST",
/* 84  */             data: {
/* 85  */                 action      : "st_remove_post_type",
/* 86  */                 data_id     : $(this).attr('data-id'),
/* 87  */                 data_id_user   : $(this).attr('data-id-user')
/* 88  */             },
/* 89  */             dataType: "json",
/* 90  */             beforeSend: function() {
/* 91  */                 $('.post-'+$this.attr('data-id')+' .user_img_loading').show();
/* 92  */             }
/* 93  */         }).done(function( html ) {
/* 94  */             console.log(html);
/* 95  */             if(html.status == 'true'){
/* 96  */                 $('.post-'+html.msg).html( console_msg(html.type , html.content ) );
/* 97  */             }else{
/* 98  */                 $('.post-'+html.msg).append( console_msg(html.type , html.content ) );
/* 99  */             }
/* 100 */ 

/* user.js */

/* 101 */         });
/* 102 */     });
/* 103 */     function console_msg( type , content ){
/* 104 */         var txt = '<div class="alert alert-'+type+'"> <button data-dismiss="alert" type="button" class="close"><span aria-hidden="true">×</span> </button> <p class="text-small">'+content+'</p> </div>';
/* 105 */         return txt;
/* 106 */     }
/* 107 */ 
/* 108 */     $('#btn_check_insert_post_type_hotel').click(function(){
/* 109 */         var dk = true;
/* 110 */         if(kt_rong('title','Warning : Title could not left empty')!=true){
/* 111 */             dk = false;
/* 112 */         }
/* 113 */         if(kt_chieudai('title','Warning : Title no shorter than 6 characters',6)!=true){
/* 114 */             dk = false;
/* 115 */         }
/* 116 */         if(kt_rong('desc','Warning : Description could not left empty')!=true){
/* 117 */             dk = false;
/* 118 */         }
/* 119 */         if(kt_rong('id_location','Warning : Location could not left empty')!=true){
/* 120 */             dk = false;
/* 121 */         }
/* 122 */         if(kt_rong('address','Warning : Address could not left empty')!=true){
/* 123 */             dk = false;
/* 124 */         }
/* 125 */         if(kt_rong('email','Warning : Email could not left empty')!=true){
/* 126 */             dk = false;
/* 127 */         }
/* 128 */         if(checkEmail('email','Warning : Email is invalid')!=true){
/* 129 */             dk = false;
/* 130 */         }
/* 131 */         if(kt_rong('website','Warning : Website could not left empty')!=true){
/* 132 */             dk = false;
/* 133 */         }
/* 134 */         if(kt_rong('phone','Warning : Phone could not left empty')!=true){
/* 135 */             dk = false;
/* 136 */         }
/* 137 */         if(kt_rong('map_lat','Warning : Latitude could not left empty')!=true){
/* 138 */             dk = false;
/* 139 */         }
/* 140 */         if(kt_rong('map_lng','Warning : Longitude could not left empty')!=true){
/* 141 */             dk = false;
/* 142 */         }
/* 143 */         if(dk == true){
/* 144 */             console.log('Submit create hotel !');
/* 145 */             $('#btn_insert_post_type_hotel').click();
/* 146 */         }
/* 147 */ 
/* 148 */     });
/* 149 */ 
/* 150 */     $('#btn_check_insert_post_type_room').click(function(){

/* user.js */

/* 151 */         var dk = true;
/* 152 */         if(kt_rong('title','Warning : Title could not left empty')!=true){
/* 153 */             dk = false;
/* 154 */         }
/* 155 */         if(kt_chieudai('title','Warning : Title no shorter than 4 characters',4)!=true){
/* 156 */             dk = false;
/* 157 */         }
/* 158 */         if(kt_rong('room_parent','Warning : Hotel could not left empty')!=true){
/* 159 */             dk = false;
/* 160 */         }
/* 161 */         if(kt_rong('number_room','Warning : Number Room could not left empty')!=true){
/* 162 */             dk = false;
/* 163 */         }
/* 164 */         if(kt_rong('price','Warning : Price Per night could not left empty')!=true){
/* 165 */             dk = false;
/* 166 */         }
/* 167 */         if(kt_rong('room_footage','Warning : Room footage could not left empty')!=true){
/* 168 */             dk = false;
/* 169 */         }
/* 170 */ 
/* 171 */         if(dk == true){
/* 172 */             console.log('Submit create hotel !');
/* 173 */             $('#btn_insert_post_type_room').click();
/* 174 */         }
/* 175 */     });
/* 176 */ 
/* 177 */     /* Tours */
/* 178 */ 
/* 179 */     $('#btn_check_insert_post_type_tours').click(function(){
/* 180 */         var dk = true;
/* 181 */         if(kt_rong('title','Warning : Title could not left empty')!=true){
/* 182 */             dk = false;
/* 183 */         }
/* 184 */         if(kt_chieudai('title','Warning : Title no shorter than 4 characters',4)!=true){
/* 185 */             dk = false;
/* 186 */         }
/* 187 */         if(dk == true){
/* 188 */             console.log('Submit create Tours !');
/* 189 */             $('#btn_insert_post_type_tours').click();
/* 190 */         }
/* 191 */     });
/* 192 */ 
/* 193 */     /* activity */
/* 194 */     $('#btn_check_insert_activity').click(function(){
/* 195 */         var dk = true;
/* 196 */         if(kt_rong('title','Warning : Title could not left empty')!=true){
/* 197 */             dk = false;
/* 198 */         }
/* 199 */         if(kt_chieudai('title','Warning : Title no shorter than 4 characters',4)!=true){
/* 200 */             dk = false;

/* user.js */

/* 201 */         }
/* 202 */         if(dk == true){
/* 203 */             console.log('Submit create Activity !');
/* 204 */             $('#btn_insert_post_type_activity').click();
/* 205 */         }
/* 206 */     });
/* 207 */ 
/* 208 */     /* Cars */
/* 209 */     $('#btn_check_insert_cars').click(function(){
/* 210 */         var dk = true;
/* 211 */         if(kt_rong('title','Warning : Title could not left empty')!=true){
/* 212 */             dk = false;
/* 213 */         }
/* 214 */         if(kt_chieudai('title','Warning : Title no shorter than 4 characters',4)!=true){
/* 215 */             dk = false;
/* 216 */         }
/* 217 */         if(dk == true){
/* 218 */             console.log('Submit create Cars !');
/* 219 */             $('#btn_insert_post_type_cars').click();
/* 220 */         }
/* 221 */     });
/* 222 */ 
/* 223 */     /* Rental */
/* 224 */     $('#btn_check_insert_post_type_rental').click(function(){
/* 225 */         var dk = true;
/* 226 */         if(kt_rong('title','Warning : Title could not left empty')!=true){
/* 227 */             dk = false;
/* 228 */         }
/* 229 */         if(kt_chieudai('title','Warning : Title no shorter than 4 characters',4)!=true){
/* 230 */             dk = false;
/* 231 */         }
/* 232 */         if(dk == true){
/* 233 */             console.log('Submit create Rental !');
/* 234 */             $('#btn_insert_post_type_rental').click();
/* 235 */         }
/* 236 */     });
/* 237 */ 
/* 238 */     /* Cruise */
/* 239 */     $('#btn_check_insert_post_type_cruise').click(function(){
/* 240 */         var dk = true;
/* 241 */         if(kt_rong('title','Warning : Title could not left empty')!=true){
/* 242 */             dk = false;
/* 243 */         }
/* 244 */         if(kt_chieudai('title','Warning : Title no shorter than 4 characters',4)!=true){
/* 245 */             dk = false;
/* 246 */         }
/* 247 */         if(dk == true){
/* 248 */             console.log('Submit create cruise !');
/* 249 */             $('#btn_insert_post_type_cruise').click();
/* 250 */         }

/* user.js */

/* 251 */     });
/* 252 */ 
/* 253 */     /* Cruise Cabin */
/* 254 */     $('#btn_check_insert_cruise_cabin').click(function(){
/* 255 */         var dk = true;
/* 256 */         if(kt_rong('title','Warning : Title could not left empty')!=true){
/* 257 */             dk = false;
/* 258 */         }
/* 259 */         if(kt_chieudai('title','Warning : Title no shorter than 4 characters',4)!=true){
/* 260 */             dk = false;
/* 261 */         }
/* 262 */         if(dk == true){
/* 263 */             console.log('Submit create cruise !');
/* 264 */             $('#btn_insert_cruise_cabin').click();
/* 265 */         }
/* 266 */     });
/* 267 */ 
/* 268 */     function kt_rong(div,thongbao){
/* 269 */         var value = $('#'+div).val();
/* 270 */         if(value=="" ||value==null)
/* 271 */         {
/* 272 */             $('.console_msg_'+div).html( console_msg('danger',thongbao) );
/* 273 */             $('#'+div).css('borderColor',"red");
/* 274 */             return false;
/* 275 */         }else{
/* 276 */             $('.console_msg_'+div).html('');
/* 277 */             $('#'+div).css('borderColor',"#C6DBE0");
/* 278 */             return true;
/* 279 */         }
/* 280 */     }
/* 281 */     function kt_chieudai(div,thongbao,chieudai){
/* 282 */         var value = $('#'+div).val();
/* 283 */         if(value.length ==chieudai || value.length <chieudai)
/* 284 */         {
/* 285 */             $('.console_msg_'+div).html( console_msg('danger',thongbao) );
/* 286 */             $('#'+div).css('borderColor',"red");
/* 287 */             return false;
/* 288 */         }else{
/* 289 */             $('.console_msg_'+div).html('');
/* 290 */             $('#'+div).css('borderColor',"#C6DBE0")
/* 291 */             ;return true;
/* 292 */         }
/* 293 */     }
/* 294 */     function kt_so(div,thongbao){
/* 295 */         var value = $('#'+div).val();
/* 296 */         if(isNaN(value)==true)
/* 297 */         {
/* 298 */             $('.console_msg_'+div).html( console_msg('danger',thongbao) );
/* 299 */             $('#'+div).css('borderColor',"red");
/* 300 */             return false;

/* user.js */

/* 301 */         }else{
/* 302 */             $('.console_msg_'+div).html('');
/* 303 */             $('#'+div).css('borderColor',"#C6DBE0");
/* 304 */             return true;
/* 305 */         }
/* 306 */     }
/* 307 */     function checkEmail(div,thongbao) {
/* 308 */         var value = $('#'+div).val();
/* 309 */         if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value)){
/* 310 */             $('.console_msg_'+div).html('');
/* 311 */             $('#'+div).css('borderColor',"#C6DBE0");
/* 312 */             return true;
/* 313 */         }else{
/* 314 */             $('.console_msg_'+div).html( console_msg('danger',thongbao) );
/* 315 */             $('#'+div).css('borderColor',"red");
/* 316 */             return false;
/* 317 */         }
/* 318 */     }
/* 319 */ 
/* 320 */     $(document).on('change', '.btn-file :file', function() {
/* 321 */         var input = $(this),
/* 322 */             label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
/* 323 */         $('.data_lable').val(label);
/* 324 */     });
/* 325 */     $('.btn_del_avatar').click(function(){
/* 326 */         $('#id_avatar_user_setting').val('');
/* 327 */         $('.data_lable').val('');
/* 328 */         $(this).parent().remove();
/* 329 */     });
/* 330 */ 
/* 331 */     function str2num(val)
/* 332 */     {
/* 333 */         val = '0' + val;
/* 334 */         val = parseFloat(val);
/* 335 */         return val;
/* 336 */     }
/* 337 */ 
/* 338 */ 
/* 339 */     /* load more histtory book */
/* 340 */     $('.btn_load_his_book').click(function(){
/* 341 */         var $this = $(this);
/* 342 */         var txt_me =$this.html();
/* 343 */         $.ajax({
/* 344 */             url: st_params.ajax_url,
/* 345 */             type: "GET",
/* 346 */             data: {
/* 347 */                 action      : "st_load_more_history_book",
/* 348 */                 paged    : $('.btn_load_his_book').attr('data-per'),
/* 349 */                 show        : "json"
/* 350 */             },

/* user.js */

/* 351 */             dataType: "json",
/* 352 */             beforeSend: function() {
/* 353 */                 $this.html('Loading...');
/* 354 */             }
/* 355 */         }).done(function( html ) {
/* 356 */             $this.html(txt_me);
/* 357 */ 
/* 358 */             if(html.status == 'true'){
/* 359 */                 console.log(html);
/* 360 */                 $('.btn_load_his_book').attr('data-per' , html.data_per );
/* 361 */                 $('#data_history_book').append(html.html);
/* 362 */             }else{
/* 363 */                 $('.btn_load_his_book').attr('disabled','disabled');
/* 364 */                 $('.btn_load_his_book').html('No More');
/* 365 */             }
/* 366 */ 
/* 367 */         });
/* 368 */     });
/* 369 */ 
/* 370 */     $('#btn_add_program').click(function(){
/* 371 */         var html = $('#html_program').html();
/* 372 */         console.log(html);
/* 373 */         $('#data_program').append(html);
/* 374 */     });
/* 375 */     $('#btn_add_equipment_item').click(function(){
/* 376 */         var html = $('#html_equipment_item').html();
/* 377 */         console.log(html);
/* 378 */         $('#data_equipment_item').append(html);
/* 379 */     });
/* 380 */ 
/* 381 */     $('#btn_add_features').click(function(){
/* 382 */         var html = $('#html_features').html();
/* 383 */         console.log(html);
/* 384 */         $('#data_features').append(html);
/* 385 */         $('.taxonomy_car').each(function() {
/* 386 */             var $this = $(this);
/* 387 */             var $select_index = $this.context.options.selectedIndex;
/* 388 */             var $icon = $($this.context.options[$select_index]).attr('data-icon');
/* 389 */             $this.parent().find('i').removeAttr('class').attr('class',$icon+' input-icon input-icon-hightlight');
/* 390 */         });
/* 391 */ 
/* 392 */     });
/* 393 */ 
/* 394 */     $('#btn_add_features_rental').click(function(){
/* 395 */         var html = $('#html_features_rental').html();
/* 396 */         console.log(html);
/* 397 */         $('#data_features_rental').append(html);
/* 398 */     });
/* 399 */ 
/* 400 */     $(document).on('click', '.btn_del_program', function() {

/* user.js */

/* 401 */         $(this).parent().parent().parent().remove();
/* 402 */     });
/* 403 */ 
/* 404 */     $(document).on('change', '.taxonomy_car', function() {
/* 405 */         var $this = $(this);
/* 406 */         var $select_index = $this.context.options.selectedIndex;
/* 407 */         var $icon = $($this.context.options[$select_index]).attr('data-icon');
/* 408 */         $this.parent().find('i').removeAttr('class').attr('class',$icon+' input-icon input-icon-hightlight');
/* 409 */     });
/* 410 */ 
/* 411 */     $('#menu_partner').click(function(){
/* 412 */          var type = $('#sub_partner').css('display');
/* 413 */         console.log(type);
/* 414 */          if(type == "none"){
/* 415 */              $('#sub_partner').slideDown(1000);
/* 416 */              $('.icon_partner').removeClass("fa-angle-left").addClass("fa-angle-down");
/* 417 */          }else{
/* 418 */              $('#sub_partner').slideUp(1000);
/* 419 */              $('.icon_partner').removeClass("fa-angle-down").addClass("fa-angle-left");
/* 420 */          }
/* 421 */     });
/* 422 */ 
/* 423 */     if($('#sub_partner').find('.active').length > 0){
/* 424 */         $('.icon_partner').removeClass("fa-angle-left").addClass("fa-angle-down");
/* 425 */         $('#sub_partner').parent().addClass('active');
/* 426 */         $('#sub_partner').css('display','block');
/* 427 */     }
/* 428 */ 
/* 429 */ 
/* 430 */ });

;
/* social-login.js */

/* 1  */ /**
/* 2  *|  * Created by me664 on 12/19/14.
/* 3  *|  */
/* 4  */ window.fbAsyncInit = function() {
/* 5  */     FB.init({
/* 6  */         appId      : '{your-app-id}',
/* 7  */         cookie     : true,  // enable cookies to allow the server to access
/* 8  */                             // the session
/* 9  */         xfbml      : true,  // parse social plugins on this page
/* 10 */         version    : 'v2.1' // use version 2.1
/* 11 */     });
/* 12 */ 
/* 13 */     // Now that we've initialized the JavaScript SDK, we call
/* 14 */     // FB.getLoginStatus().  This function gets the state of the
/* 15 */     // person visiting this page and can return one of three states to
/* 16 */     // the callback you provide.  They can be:
/* 17 */     //
/* 18 */     // 1. Logged into your app ('connected')
/* 19 */     // 2. Logged into Facebook, but not your app ('not_authorized')
/* 20 */     // 3. Not logged into Facebook and can't tell if they are logged into
/* 21 */     //    your app or not.
/* 22 */     //
/* 23 */     // These three cases are handled in the callback function.
/* 24 */ 
/* 25 */     FB.getLoginStatus(function(response) {
/* 26 */         statusChangeCallback(response);
/* 27 */     });
/* 28 */ 
/* 29 */ };

;
/* nicescroll.js */

/* 1   */ /* jquery.nicescroll 3.6.0 InuYaksa*2014 MIT http://nicescroll.areaaperta.com */(function(f){"function"===typeof define&&define.amd?define(["jquery"],f):f(jQuery)})(function(f){var y=!1,D=!1,N=0,O=2E3,x=0,H=["webkit","ms","moz","o"],s=window.requestAnimationFrame||!1,t=window.cancelAnimationFrame||!1;if(!s)for(var P in H){var E=H[P];s||(s=window[E+"RequestAnimationFrame"]);t||(t=window[E+"CancelAnimationFrame"]||window[E+"CancelRequestAnimationFrame"])}var v=window.MutationObserver||window.WebKitMutationObserver||!1,I={zindex:"auto",cursoropacitymin:0,cursoropacitymax:1,cursorcolor:"#424242",
/* 2   */     cursorwidth:"5px",cursorborder:"1px solid #fff",cursorborderradius:"5px",scrollspeed:60,mousescrollstep:24,touchbehavior:!1,hwacceleration:!0,usetransition:!0,boxzoom:!1,dblclickzoom:!0,gesturezoom:!0,grabcursorenabled:!0,autohidemode:!0,background:"",iframeautoresize:!0,cursorminheight:32,preservenativescrolling:!0,railoffset:!1,railhoffset:!1,bouncescroll:!0,spacebarenabled:!0,railpadding:{top:0,right:0,left:0,bottom:0},disableoutline:!0,horizrailenabled:!0,railalign:"right",railvalign:"bottom",
/* 3   */     enabletranslate3d:!0,enablemousewheel:!0,enablekeyboard:!0,smoothscroll:!0,sensitiverail:!0,enablemouselockapi:!0,cursorfixedheight:!1,directionlockdeadzone:6,hidecursordelay:400,nativeparentscrolling:!0,enablescrollonselection:!0,overflowx:!0,overflowy:!0,cursordragspeed:.3,rtlmode:"auto",cursordragontouch:!1,oneaxismousemode:"auto",scriptpath:function(){var f=document.getElementsByTagName("script"),f=f[f.length-1].src.split("?")[0];return 0<f.split("/").length?f.split("/").slice(0,-1).join("/")+
/* 4   */     "/":""}(),preventmultitouchscrolling:!0},F=!1,Q=function(){if(F)return F;var f=document.createElement("DIV"),c=f.style,h=navigator.userAgent,m=navigator.platform,d={haspointerlock:"pointerLockElement"in document||"webkitPointerLockElement"in document||"mozPointerLockElement"in document};d.isopera="opera"in window;d.isopera12=d.isopera&&"getUserMedia"in navigator;d.isoperamini="[object OperaMini]"===Object.prototype.toString.call(window.operamini);d.isie="all"in document&&"attachEvent"in f&&!d.isopera;
/* 5   */     d.isieold=d.isie&&!("msInterpolationMode"in c);d.isie7=d.isie&&!d.isieold&&(!("documentMode"in document)||7==document.documentMode);d.isie8=d.isie&&"documentMode"in document&&8==document.documentMode;d.isie9=d.isie&&"performance"in window&&9<=document.documentMode;d.isie10=d.isie&&"performance"in window&&10==document.documentMode;d.isie11="msRequestFullscreen"in f&&11<=document.documentMode;d.isie9mobile=/iemobile.9/i.test(h);d.isie9mobile&&(d.isie9=!1);d.isie7mobile=!d.isie9mobile&&d.isie7&&/iemobile/i.test(h);
/* 6   */     d.ismozilla="MozAppearance"in c;d.iswebkit="WebkitAppearance"in c;d.ischrome="chrome"in window;d.ischrome22=d.ischrome&&d.haspointerlock;d.ischrome26=d.ischrome&&"transition"in c;d.cantouch="ontouchstart"in document.documentElement||"ontouchstart"in window;d.hasmstouch=window.MSPointerEvent||!1;d.hasw3ctouch=window.PointerEvent||!1;d.ismac=/^mac$/i.test(m);d.isios=d.cantouch&&/iphone|ipad|ipod/i.test(m);d.isios4=d.isios&&!("seal"in Object);d.isios7=d.isios&&"webkitHidden"in document;d.isandroid=/android/i.test(h);
/* 7   */     d.haseventlistener="addEventListener"in f;d.trstyle=!1;d.hastransform=!1;d.hastranslate3d=!1;d.transitionstyle=!1;d.hastransition=!1;d.transitionend=!1;m=["transform","msTransform","webkitTransform","MozTransform","OTransform"];for(h=0;h<m.length;h++)if("undefined"!=typeof c[m[h]]){d.trstyle=m[h];break}d.hastransform=!!d.trstyle;d.hastransform&&(c[d.trstyle]="translate3d(1px,2px,3px)",d.hastranslate3d=/translate3d/.test(c[d.trstyle]));d.transitionstyle=!1;d.prefixstyle="";d.transitionend=!1;for(var m=
/* 8   */         "transition webkitTransition msTransition MozTransition OTransition OTransition KhtmlTransition".split(" "),n=" -webkit- -ms- -moz- -o- -o -khtml-".split(" "),p="transitionend webkitTransitionEnd msTransitionEnd transitionend otransitionend oTransitionEnd KhtmlTransitionEnd".split(" "),h=0;h<m.length;h++)if(m[h]in c){d.transitionstyle=m[h];d.prefixstyle=n[h];d.transitionend=p[h];break}d.ischrome26&&(d.prefixstyle=n[1]);d.hastransition=d.transitionstyle;a:{h=["-webkit-grab","-moz-grab","grab"];if(d.ischrome&&
/* 9   */         !d.ischrome22||d.isie)h=[];for(m=0;m<h.length;m++)if(n=h[m],c.cursor=n,c.cursor==n){c=n;break a}c="url(//mail.google.com/mail/images/2/openhand.cur),n-resize"}d.cursorgrabvalue=c;d.hasmousecapture="setCapture"in f;d.hasMutationObserver=!1!==v;return F=d},R=function(k,c){function h(){var b=a.doc.css(e.trstyle);return b&&"matrix"==b.substr(0,6)?b.replace(/^.*\((.*)\)$/g,"$1").replace(/px/g,"").split(/, +/):!1}function m(){var b=a.win;if("zIndex"in b)return b.zIndex();for(;0<b.length&&9!=b[0].nodeType;){var g=
/* 10  */     b.css("zIndex");if(!isNaN(g)&&0!=g)return parseInt(g);b=b.parent()}return!1}function d(b,g,q){g=b.css(g);b=parseFloat(g);return isNaN(b)?(b=w[g]||0,q=3==b?q?a.win.outerHeight()-a.win.innerHeight():a.win.outerWidth()-a.win.innerWidth():1,a.isie8&&b&&(b+=1),q?b:0):b}function n(b,g,q,c){a._bind(b,g,function(a){a=a?a:window.event;var c={original:a,target:a.target||a.srcElement,type:"wheel",deltaMode:"MozMousePixelScroll"==a.type?0:1,deltaX:0,deltaZ:0,preventDefault:function(){a.preventDefault?a.preventDefault():
/* 11  */     a.returnValue=!1;return!1},stopImmediatePropagation:function(){a.stopImmediatePropagation?a.stopImmediatePropagation():a.cancelBubble=!0}};"mousewheel"==g?(c.deltaY=-.025*a.wheelDelta,a.wheelDeltaX&&(c.deltaX=-.025*a.wheelDeltaX)):c.deltaY=a.detail;return q.call(b,c)},c)}function p(b,g,c){var d,e;0==b.deltaMode?(d=-Math.floor(a.opt.mousescrollstep/54*b.deltaX),e=-Math.floor(a.opt.mousescrollstep/54*b.deltaY)):1==b.deltaMode&&(d=-Math.floor(b.deltaX*a.opt.mousescrollstep),e=-Math.floor(b.deltaY*a.opt.mousescrollstep));
/* 12  */     g&&a.opt.oneaxismousemode&&0==d&&e&&(d=e,e=0,c&&(0>d?a.getScrollLeft()>=a.page.maxw:0>=a.getScrollLeft())&&(e=d,d=0));d&&(a.scrollmom&&a.scrollmom.stop(),a.lastdeltax+=d,a.debounced("mousewheelx",function(){var b=a.lastdeltax;a.lastdeltax=0;a.rail.drag||a.doScrollLeftBy(b)},15));if(e){if(a.opt.nativeparentscrolling&&c&&!a.ispage&&!a.zoomactive)if(0>e){if(a.getScrollTop()>=a.page.maxh)return!0}else if(0>=a.getScrollTop())return!0;a.scrollmom&&a.scrollmom.stop();a.lastdeltay+=e;a.debounced("mousewheely",
/* 13  */         function(){var b=a.lastdeltay;a.lastdeltay=0;a.rail.drag||a.doScrollBy(b)},15)}b.stopImmediatePropagation();return b.preventDefault()}var a=this;this.version="3.6.0";this.name="nicescroll";this.me=c;this.opt={doc:f("body"),win:!1};f.extend(this.opt,I);this.opt.snapbackspeed=80;if(k)for(var G in a.opt)"undefined"!=typeof k[G]&&(a.opt[G]=k[G]);this.iddoc=(this.doc=a.opt.doc)&&this.doc[0]?this.doc[0].id||"":"";this.ispage=/^BODY|HTML/.test(a.opt.win?a.opt.win[0].nodeName:this.doc[0].nodeName);this.haswrapper=
/* 14  */     !1!==a.opt.win;this.win=a.opt.win||(this.ispage?f(window):this.doc);this.docscroll=this.ispage&&!this.haswrapper?f(window):this.win;this.body=f("body");this.iframe=this.isfixed=this.viewport=!1;this.isiframe="IFRAME"==this.doc[0].nodeName&&"IFRAME"==this.win[0].nodeName;this.istextarea="TEXTAREA"==this.win[0].nodeName;this.forcescreen=!1;this.canshowonmouseevent="scroll"!=a.opt.autohidemode;this.page=this.view=this.onzoomout=this.onzoomin=this.onscrollcancel=this.onscrollend=this.onscrollstart=this.onclick=
/* 15  */     this.ongesturezoom=this.onkeypress=this.onmousewheel=this.onmousemove=this.onmouseup=this.onmousedown=!1;this.scroll={x:0,y:0};this.scrollratio={x:0,y:0};this.cursorheight=20;this.scrollvaluemax=0;this.isrtlmode="auto"==this.opt.rtlmode?"rtl"==(this.win[0]==window?this.body:this.win).css("direction"):!0===this.opt.rtlmode;this.observerbody=this.observerremover=this.observer=this.scrollmom=this.scrollrunning=!1;do this.id="ascrail"+O++;while(document.getElementById(this.id));this.hasmousefocus=this.hasfocus=
/* 16  */     this.zoomactive=this.zoom=this.selectiondrag=this.cursorfreezed=this.cursor=this.rail=!1;this.visibility=!0;this.hidden=this.locked=this.railslocked=!1;this.cursoractive=!0;this.wheelprevented=!1;this.overflowx=a.opt.overflowx;this.overflowy=a.opt.overflowy;this.nativescrollingarea=!1;this.checkarea=0;this.events=[];this.saved={};this.delaylist={};this.synclist={};this.lastdeltay=this.lastdeltax=0;this.detected=Q();var e=f.extend({},this.detected);this.ishwscroll=(this.canhwscroll=e.hastransform&&
/* 17  */ a.opt.hwacceleration)&&a.haswrapper;this.hasreversehr=this.isrtlmode&&!e.iswebkit;this.istouchcapable=!1;!e.cantouch||e.isios||e.isandroid||!e.iswebkit&&!e.ismozilla||(this.istouchcapable=!0,e.cantouch=!1);a.opt.enablemouselockapi||(e.hasmousecapture=!1,e.haspointerlock=!1);this.debounced=function(b,g,c){var d=a.delaylist[b];a.delaylist[b]=g;d||setTimeout(function(){var g=a.delaylist[b];a.delaylist[b]=!1;g.call(a)},c)};var r=!1;this.synched=function(b,g){a.synclist[b]=g;(function(){r||(s(function(){r=
/* 18  */     !1;for(var b in a.synclist){var g=a.synclist[b];g&&g.call(a);a.synclist[b]=!1}}),r=!0)})();return b};this.unsynched=function(b){a.synclist[b]&&(a.synclist[b]=!1)};this.css=function(b,g){for(var c in g)a.saved.css.push([b,c,b.css(c)]),b.css(c,g[c])};this.scrollTop=function(b){return"undefined"==typeof b?a.getScrollTop():a.setScrollTop(b)};this.scrollLeft=function(b){return"undefined"==typeof b?a.getScrollLeft():a.setScrollLeft(b)};var A=function(a,g,c,d,e,f,h){this.st=a;this.ed=g;this.spd=c;this.p1=
/* 19  */     d||0;this.p2=e||1;this.p3=f||0;this.p4=h||1;this.ts=(new Date).getTime();this.df=this.ed-this.st};A.prototype={B2:function(a){return 3*a*a*(1-a)},B3:function(a){return 3*a*(1-a)*(1-a)},B4:function(a){return(1-a)*(1-a)*(1-a)},getNow:function(){var a=1-((new Date).getTime()-this.ts)/this.spd,g=this.B2(a)+this.B3(a)+this.B4(a);return 0>a?this.ed:this.st+Math.round(this.df*g)},update:function(a,g){this.st=this.getNow();this.ed=a;this.spd=g;this.ts=(new Date).getTime();this.df=this.ed-this.st;return this}};
/* 20  */     if(this.ishwscroll){this.doc.translate={x:0,y:0,tx:"0px",ty:"0px"};e.hastranslate3d&&e.isios&&this.doc.css("-webkit-backface-visibility","hidden");this.getScrollTop=function(b){if(!b){if(b=h())return 16==b.length?-b[13]:-b[5];if(a.timerscroll&&a.timerscroll.bz)return a.timerscroll.bz.getNow()}return a.doc.translate.y};this.getScrollLeft=function(b){if(!b){if(b=h())return 16==b.length?-b[12]:-b[4];if(a.timerscroll&&a.timerscroll.bh)return a.timerscroll.bh.getNow()}return a.doc.translate.x};this.notifyScrollEvent=
/* 21  */         function(a){var g=document.createEvent("UIEvents");g.initUIEvent("scroll",!1,!0,window,1);g.niceevent=!0;a.dispatchEvent(g)};var K=this.isrtlmode?1:-1;e.hastranslate3d&&a.opt.enabletranslate3d?(this.setScrollTop=function(b,g){a.doc.translate.y=b;a.doc.translate.ty=-1*b+"px";a.doc.css(e.trstyle,"translate3d("+a.doc.translate.tx+","+a.doc.translate.ty+",0px)");g||a.notifyScrollEvent(a.win[0])},this.setScrollLeft=function(b,g){a.doc.translate.x=b;a.doc.translate.tx=b*K+"px";a.doc.css(e.trstyle,"translate3d("+
/* 22  */     a.doc.translate.tx+","+a.doc.translate.ty+",0px)");g||a.notifyScrollEvent(a.win[0])}):(this.setScrollTop=function(b,g){a.doc.translate.y=b;a.doc.translate.ty=-1*b+"px";a.doc.css(e.trstyle,"translate("+a.doc.translate.tx+","+a.doc.translate.ty+")");g||a.notifyScrollEvent(a.win[0])},this.setScrollLeft=function(b,g){a.doc.translate.x=b;a.doc.translate.tx=b*K+"px";a.doc.css(e.trstyle,"translate("+a.doc.translate.tx+","+a.doc.translate.ty+")");g||a.notifyScrollEvent(a.win[0])})}else this.getScrollTop=
/* 23  */         function(){return a.docscroll.scrollTop()},this.setScrollTop=function(b){return a.docscroll.scrollTop(b)},this.getScrollLeft=function(){return a.detected.ismozilla&&a.isrtlmode?Math.abs(a.docscroll.scrollLeft()):a.docscroll.scrollLeft()},this.setScrollLeft=function(b){return a.docscroll.scrollLeft(a.detected.ismozilla&&a.isrtlmode?-b:b)};this.getTarget=function(a){return a?a.target?a.target:a.srcElement?a.srcElement:!1:!1};this.hasParent=function(a,g){if(!a)return!1;for(var c=a.target||a.srcElement||
/* 24  */         a||!1;c&&c.id!=g;)c=c.parentNode||!1;return!1!==c};var w={thin:1,medium:3,thick:5};this.getDocumentScrollOffset=function(){return{top:window.pageYOffset||document.documentElement.scrollTop,left:window.pageXOffset||document.documentElement.scrollLeft}};this.getOffset=function(){if(a.isfixed){var b=a.win.offset(),g=a.getDocumentScrollOffset();b.top-=g.top;b.left-=g.left;return b}b=a.win.offset();if(!a.viewport)return b;g=a.viewport.offset();return{top:b.top-g.top,left:b.left-g.left}};this.updateScrollBar=
/* 25  */         function(b){if(a.ishwscroll)a.rail.css({height:a.win.innerHeight()-(a.opt.railpadding.top+a.opt.railpadding.bottom)}),a.railh&&a.railh.css({width:a.win.innerWidth()-(a.opt.railpadding.left+a.opt.railpadding.right)});else{var g=a.getOffset(),c=g.top,e=g.left-(a.opt.railpadding.left+a.opt.railpadding.right),c=c+d(a.win,"border-top-width",!0),e=e+(a.rail.align?a.win.outerWidth()-d(a.win,"border-right-width")-a.rail.width:d(a.win,"border-left-width")),f=a.opt.railoffset;f&&(f.top&&(c+=f.top),a.rail.align&&
/* 26  */         f.left&&(e+=f.left));a.railslocked||a.rail.css({top:c,left:e,height:(b?b.h:a.win.innerHeight())-(a.opt.railpadding.top+a.opt.railpadding.bottom)});a.zoom&&a.zoom.css({top:c+1,left:1==a.rail.align?e-20:e+a.rail.width+4});if(a.railh&&!a.railslocked){c=g.top;e=g.left;if(f=a.opt.railhoffset)f.top&&(c+=f.top),f.left&&(e+=f.left);b=a.railh.align?c+d(a.win,"border-top-width",!0)+a.win.innerHeight()-a.railh.height:c+d(a.win,"border-top-width",!0);e+=d(a.win,"border-left-width");a.railh.css({top:b-(a.opt.railpadding.top+
/* 27  */         a.opt.railpadding.bottom),left:e,width:a.railh.width})}}};this.doRailClick=function(b,g,c){var e;a.railslocked||(a.cancelEvent(b),g?(g=c?a.doScrollLeft:a.doScrollTop,e=c?(b.pageX-a.railh.offset().left-a.cursorwidth/2)*a.scrollratio.x:(b.pageY-a.rail.offset().top-a.cursorheight/2)*a.scrollratio.y,g(e)):(g=c?a.doScrollLeftBy:a.doScrollBy,e=c?a.scroll.x:a.scroll.y,b=c?b.pageX-a.railh.offset().left:b.pageY-a.rail.offset().top,c=c?a.view.w:a.view.h,g(e>=b?c:-c)))};a.hasanimationframe=s;a.hascancelanimationframe=
/* 28  */         t;a.hasanimationframe?a.hascancelanimationframe||(t=function(){a.cancelAnimationFrame=!0}):(s=function(a){return setTimeout(a,15-Math.floor(+new Date/1E3)%16)},t=clearInterval);this.init=function(){a.saved.css=[];if(e.isie7mobile||e.isoperamini)return!0;e.hasmstouch&&a.css(a.ispage?f("html"):a.win,{"-ms-touch-action":"none"});a.zindex="auto";a.zindex=a.ispage||"auto"!=a.opt.zindex?a.opt.zindex:m()||"auto";!a.ispage&&"auto"!=a.zindex&&a.zindex>x&&(x=a.zindex);a.isie&&0==a.zindex&&"auto"==a.opt.zindex&&
/* 29  */     (a.zindex="auto");if(!a.ispage||!e.cantouch&&!e.isieold&&!e.isie9mobile){var b=a.docscroll;a.ispage&&(b=a.haswrapper?a.win:a.doc);e.isie9mobile||a.css(b,{"overflow-y":"hidden"});a.ispage&&e.isie7&&("BODY"==a.doc[0].nodeName?a.css(f("html"),{"overflow-y":"hidden"}):"HTML"==a.doc[0].nodeName&&a.css(f("body"),{"overflow-y":"hidden"}));!e.isios||a.ispage||a.haswrapper||a.css(f("body"),{"-webkit-overflow-scrolling":"touch"});var g=f(document.createElement("div"));g.css({position:"relative",top:0,"float":"right",
/* 30  */         width:a.opt.cursorwidth,height:"0px","background-color":a.opt.cursorcolor,border:a.opt.cursorborder,"background-clip":"padding-box","-webkit-border-radius":a.opt.cursorborderradius,"-moz-border-radius":a.opt.cursorborderradius,"border-radius":a.opt.cursorborderradius});g.hborder=parseFloat(g.outerHeight()-g.innerHeight());g.addClass("nicescroll-cursors");a.cursor=g;var c=f(document.createElement("div"));c.attr("id",a.id);c.addClass("nicescroll-rails nicescroll-rails-vr");var d,h,k=["left","right",
/* 31  */         "top","bottom"],J;for(J in k)h=k[J],(d=a.opt.railpadding[h])?c.css("padding-"+h,d+"px"):a.opt.railpadding[h]=0;c.append(g);c.width=Math.max(parseFloat(a.opt.cursorwidth),g.outerWidth());c.css({width:c.width+"px",zIndex:a.zindex,background:a.opt.background,cursor:"default"});c.visibility=!0;c.scrollable=!0;c.align="left"==a.opt.railalign?0:1;a.rail=c;g=a.rail.drag=!1;!a.opt.boxzoom||a.ispage||e.isieold||(g=document.createElement("div"),a.bind(g,"click",a.doZoom),a.bind(g,"mouseenter",function(){a.zoom.css("opacity",
/* 32  */         a.opt.cursoropacitymax)}),a.bind(g,"mouseleave",function(){a.zoom.css("opacity",a.opt.cursoropacitymin)}),a.zoom=f(g),a.zoom.css({cursor:"pointer","z-index":a.zindex,backgroundImage:"url("+a.opt.scriptpath+"zoomico.png)",height:18,width:18,backgroundPosition:"0px 0px"}),a.opt.dblclickzoom&&a.bind(a.win,"dblclick",a.doZoom),e.cantouch&&a.opt.gesturezoom&&(a.ongesturezoom=function(b){1.5<b.scale&&a.doZoomIn(b);.8>b.scale&&a.doZoomOut(b);return a.cancelEvent(b)},a.bind(a.win,"gestureend",a.ongesturezoom)));
/* 33  */         a.railh=!1;var l;a.opt.horizrailenabled&&(a.css(b,{"overflow-x":"hidden"}),g=f(document.createElement("div")),g.css({position:"absolute",top:0,height:a.opt.cursorwidth,width:"0px","background-color":a.opt.cursorcolor,border:a.opt.cursorborder,"background-clip":"padding-box","-webkit-border-radius":a.opt.cursorborderradius,"-moz-border-radius":a.opt.cursorborderradius,"border-radius":a.opt.cursorborderradius}),e.isieold&&g.css({overflow:"hidden"}),g.wborder=parseFloat(g.outerWidth()-g.innerWidth()),
/* 34  */             g.addClass("nicescroll-cursors"),a.cursorh=g,l=f(document.createElement("div")),l.attr("id",a.id+"-hr"),l.addClass("nicescroll-rails nicescroll-rails-hr"),l.height=Math.max(parseFloat(a.opt.cursorwidth),g.outerHeight()),l.css({height:l.height+"px",zIndex:a.zindex,background:a.opt.background}),l.append(g),l.visibility=!0,l.scrollable=!0,l.align="top"==a.opt.railvalign?0:1,a.railh=l,a.railh.drag=!1);a.ispage?(c.css({position:"fixed",top:"0px",height:"100%"}),c.align?c.css({right:"0px"}):c.css({left:"0px"}),
/* 35  */             a.body.append(c),a.railh&&(l.css({position:"fixed",left:"0px",width:"100%"}),l.align?l.css({bottom:"0px"}):l.css({top:"0px"}),a.body.append(l))):(a.ishwscroll?("static"==a.win.css("position")&&a.css(a.win,{position:"relative"}),b="HTML"==a.win[0].nodeName?a.body:a.win,f(b).scrollTop(0).scrollLeft(0),a.zoom&&(a.zoom.css({position:"absolute",top:1,right:0,"margin-right":c.width+4}),b.append(a.zoom)),c.css({position:"absolute",top:0}),c.align?c.css({right:0}):c.css({left:0}),b.append(c),l&&(l.css({position:"absolute",
/* 36  */             left:0,bottom:0}),l.align?l.css({bottom:0}):l.css({top:0}),b.append(l))):(a.isfixed="fixed"==a.win.css("position"),b=a.isfixed?"fixed":"absolute",a.isfixed||(a.viewport=a.getViewport(a.win[0])),a.viewport&&(a.body=a.viewport,0==/fixed|absolute/.test(a.viewport.css("position"))&&a.css(a.viewport,{position:"relative"})),c.css({position:b}),a.zoom&&a.zoom.css({position:b}),a.updateScrollBar(),a.body.append(c),a.zoom&&a.body.append(a.zoom),a.railh&&(l.css({position:b}),a.body.append(l))),e.isios&&a.css(a.win,
/* 37  */             {"-webkit-tap-highlight-color":"rgba(0,0,0,0)","-webkit-touch-callout":"none"}),e.isie&&a.opt.disableoutline&&a.win.attr("hideFocus","true"),e.iswebkit&&a.opt.disableoutline&&a.win.css({outline:"none"}));!1===a.opt.autohidemode?(a.autohidedom=!1,a.rail.css({opacity:a.opt.cursoropacitymax}),a.railh&&a.railh.css({opacity:a.opt.cursoropacitymax})):!0===a.opt.autohidemode||"leave"===a.opt.autohidemode?(a.autohidedom=f().add(a.rail),e.isie8&&(a.autohidedom=a.autohidedom.add(a.cursor)),a.railh&&(a.autohidedom=
/* 38  */             a.autohidedom.add(a.railh)),a.railh&&e.isie8&&(a.autohidedom=a.autohidedom.add(a.cursorh))):"scroll"==a.opt.autohidemode?(a.autohidedom=f().add(a.rail),a.railh&&(a.autohidedom=a.autohidedom.add(a.railh))):"cursor"==a.opt.autohidemode?(a.autohidedom=f().add(a.cursor),a.railh&&(a.autohidedom=a.autohidedom.add(a.cursorh))):"hidden"==a.opt.autohidemode&&(a.autohidedom=!1,a.hide(),a.railslocked=!1);if(e.isie9mobile)a.scrollmom=new L(a),a.onmangotouch=function(){var b=a.getScrollTop(),c=a.getScrollLeft();
/* 39  */             if(b==a.scrollmom.lastscrolly&&c==a.scrollmom.lastscrollx)return!0;var g=b-a.mangotouch.sy,e=c-a.mangotouch.sx;if(0!=Math.round(Math.sqrt(Math.pow(e,2)+Math.pow(g,2)))){var d=0>g?-1:1,f=0>e?-1:1,q=+new Date;a.mangotouch.lazy&&clearTimeout(a.mangotouch.lazy);80<q-a.mangotouch.tm||a.mangotouch.dry!=d||a.mangotouch.drx!=f?(a.scrollmom.stop(),a.scrollmom.reset(c,b),a.mangotouch.sy=b,a.mangotouch.ly=b,a.mangotouch.sx=c,a.mangotouch.lx=c,a.mangotouch.dry=d,a.mangotouch.drx=f,a.mangotouch.tm=q):(a.scrollmom.stop(),
/* 40  */                 a.scrollmom.update(a.mangotouch.sx-e,a.mangotouch.sy-g),a.mangotouch.tm=q,g=Math.max(Math.abs(a.mangotouch.ly-b),Math.abs(a.mangotouch.lx-c)),a.mangotouch.ly=b,a.mangotouch.lx=c,2<g&&(a.mangotouch.lazy=setTimeout(function(){a.mangotouch.lazy=!1;a.mangotouch.dry=0;a.mangotouch.drx=0;a.mangotouch.tm=0;a.scrollmom.doMomentum(30)},100)))}},c=a.getScrollTop(),l=a.getScrollLeft(),a.mangotouch={sy:c,ly:c,dry:0,sx:l,lx:l,drx:0,lazy:!1,tm:0},a.bind(a.docscroll,"scroll",a.onmangotouch);else{if(e.cantouch||
/* 41  */             a.istouchcapable||a.opt.touchbehavior||e.hasmstouch){a.scrollmom=new L(a);a.ontouchstart=function(b){if(b.pointerType&&2!=b.pointerType&&"touch"!=b.pointerType)return!1;a.hasmoving=!1;if(!a.railslocked){var c;if(e.hasmstouch)for(c=b.target?b.target:!1;c;){var g=f(c).getNiceScroll();if(0<g.length&&g[0].me==a.me)break;if(0<g.length)return!1;if("DIV"==c.nodeName&&c.id==a.id)break;c=c.parentNode?c.parentNode:!1}a.cancelScroll();if((c=a.getTarget(b))&&/INPUT/i.test(c.nodeName)&&/range/i.test(c.type))return a.stopPropagation(b);
/* 42  */             !("clientX"in b)&&"changedTouches"in b&&(b.clientX=b.changedTouches[0].clientX,b.clientY=b.changedTouches[0].clientY);a.forcescreen&&(g=b,b={original:b.original?b.original:b},b.clientX=g.screenX,b.clientY=g.screenY);a.rail.drag={x:b.clientX,y:b.clientY,sx:a.scroll.x,sy:a.scroll.y,st:a.getScrollTop(),sl:a.getScrollLeft(),pt:2,dl:!1};if(a.ispage||!a.opt.directionlockdeadzone)a.rail.drag.dl="f";else{var g=f(window).width(),d=f(window).height(),q=Math.max(document.body.scrollWidth,document.documentElement.scrollWidth),
/* 43  */                 h=Math.max(document.body.scrollHeight,document.documentElement.scrollHeight),d=Math.max(0,h-d),g=Math.max(0,q-g);a.rail.drag.ck=!a.rail.scrollable&&a.railh.scrollable?0<d?"v":!1:a.rail.scrollable&&!a.railh.scrollable?0<g?"h":!1:!1;a.rail.drag.ck||(a.rail.drag.dl="f")}a.opt.touchbehavior&&a.isiframe&&e.isie&&(g=a.win.position(),a.rail.drag.x+=g.left,a.rail.drag.y+=g.top);a.hasmoving=!1;a.lastmouseup=!1;a.scrollmom.reset(b.clientX,b.clientY);if(!e.cantouch&&!this.istouchcapable&&!b.pointerType){if(!c||
/* 44  */                 !/INPUT|SELECT|TEXTAREA/i.test(c.nodeName))return!a.ispage&&e.hasmousecapture&&c.setCapture(),a.opt.touchbehavior?(c.onclick&&!c._onclick&&(c._onclick=c.onclick,c.onclick=function(b){if(a.hasmoving)return!1;c._onclick.call(this,b)}),a.cancelEvent(b)):a.stopPropagation(b);/SUBMIT|CANCEL|BUTTON/i.test(f(c).attr("type"))&&(pc={tg:c,click:!1},a.preventclick=pc)}}};a.ontouchend=function(b){if(!a.rail.drag)return!0;if(2==a.rail.drag.pt){if(b.pointerType&&2!=b.pointerType&&"touch"!=b.pointerType)return!1;
/* 45  */             a.scrollmom.doMomentum();a.rail.drag=!1;if(a.hasmoving&&(a.lastmouseup=!0,a.hideCursor(),e.hasmousecapture&&document.releaseCapture(),!e.cantouch))return a.cancelEvent(b)}else if(1==a.rail.drag.pt)return a.onmouseup(b)};var n=a.opt.touchbehavior&&a.isiframe&&!e.hasmousecapture;a.ontouchmove=function(b,c){if(!a.rail.drag||b.targetTouches&&a.opt.preventmultitouchscrolling&&1<b.targetTouches.length||b.pointerType&&2!=b.pointerType&&"touch"!=b.pointerType)return!1;if(2==a.rail.drag.pt){if(e.cantouch&&
/* 46  */             e.isios&&"undefined"==typeof b.original)return!0;a.hasmoving=!0;a.preventclick&&!a.preventclick.click&&(a.preventclick.click=a.preventclick.tg.onclick||!1,a.preventclick.tg.onclick=a.onpreventclick);b=f.extend({original:b},b);"changedTouches"in b&&(b.clientX=b.changedTouches[0].clientX,b.clientY=b.changedTouches[0].clientY);if(a.forcescreen){var g=b;b={original:b.original?b.original:b};b.clientX=g.screenX;b.clientY=g.screenY}var d,g=d=0;n&&!c&&(d=a.win.position(),g=-d.left,d=-d.top);var q=b.clientY+
/* 47  */             d;d=q-a.rail.drag.y;var h=b.clientX+g,u=h-a.rail.drag.x,k=a.rail.drag.st-d;a.ishwscroll&&a.opt.bouncescroll?0>k?k=Math.round(k/2):k>a.page.maxh&&(k=a.page.maxh+Math.round((k-a.page.maxh)/2)):(0>k&&(q=k=0),k>a.page.maxh&&(k=a.page.maxh,q=0));var l;a.railh&&a.railh.scrollable&&(l=a.isrtlmode?u-a.rail.drag.sl:a.rail.drag.sl-u,a.ishwscroll&&a.opt.bouncescroll?0>l?l=Math.round(l/2):l>a.page.maxw&&(l=a.page.maxw+Math.round((l-a.page.maxw)/2)):(0>l&&(h=l=0),l>a.page.maxw&&(l=a.page.maxw,h=0)));g=!1;if(a.rail.drag.dl)g=
/* 48  */             !0,"v"==a.rail.drag.dl?l=a.rail.drag.sl:"h"==a.rail.drag.dl&&(k=a.rail.drag.st);else{d=Math.abs(d);var u=Math.abs(u),z=a.opt.directionlockdeadzone;if("v"==a.rail.drag.ck){if(d>z&&u<=.3*d)return a.rail.drag=!1,!0;u>z&&(a.rail.drag.dl="f",f("body").scrollTop(f("body").scrollTop()))}else if("h"==a.rail.drag.ck){if(u>z&&d<=.3*u)return a.rail.drag=!1,!0;d>z&&(a.rail.drag.dl="f",f("body").scrollLeft(f("body").scrollLeft()))}}a.synched("touchmove",function(){a.rail.drag&&2==a.rail.drag.pt&&(a.prepareTransition&&
/* 49  */         a.prepareTransition(0),a.rail.scrollable&&a.setScrollTop(k),a.scrollmom.update(h,q),a.railh&&a.railh.scrollable?(a.setScrollLeft(l),a.showCursor(k,l)):a.showCursor(k),e.isie10&&document.selection.clear())});e.ischrome&&a.istouchcapable&&(g=!1);if(g)return a.cancelEvent(b)}else if(1==a.rail.drag.pt)return a.onmousemove(b)}}a.onmousedown=function(b,c){if(!a.rail.drag||1==a.rail.drag.pt){if(a.railslocked)return a.cancelEvent(b);a.cancelScroll();a.rail.drag={x:b.clientX,y:b.clientY,sx:a.scroll.x,sy:a.scroll.y,
/* 50  */             pt:1,hr:!!c};var g=a.getTarget(b);!a.ispage&&e.hasmousecapture&&g.setCapture();a.isiframe&&!e.hasmousecapture&&(a.saved.csspointerevents=a.doc.css("pointer-events"),a.css(a.doc,{"pointer-events":"none"}));a.hasmoving=!1;return a.cancelEvent(b)}};a.onmouseup=function(b){if(a.rail.drag){if(1!=a.rail.drag.pt)return!0;e.hasmousecapture&&document.releaseCapture();a.isiframe&&!e.hasmousecapture&&a.doc.css("pointer-events",a.saved.csspointerevents);a.rail.drag=!1;a.hasmoving&&a.triggerScrollEnd();return a.cancelEvent(b)}};

/* nicescroll.js */

/* 51  */             a.onmousemove=function(b){if(a.rail.drag&&1==a.rail.drag.pt){if(e.ischrome&&0==b.which)return a.onmouseup(b);a.cursorfreezed=!0;a.hasmoving=!0;if(a.rail.drag.hr){a.scroll.x=a.rail.drag.sx+(b.clientX-a.rail.drag.x);0>a.scroll.x&&(a.scroll.x=0);var c=a.scrollvaluemaxw;a.scroll.x>c&&(a.scroll.x=c)}else a.scroll.y=a.rail.drag.sy+(b.clientY-a.rail.drag.y),0>a.scroll.y&&(a.scroll.y=0),c=a.scrollvaluemax,a.scroll.y>c&&(a.scroll.y=c);a.synched("mousemove",function(){a.rail.drag&&1==a.rail.drag.pt&&(a.showCursor(),
/* 52  */                 a.rail.drag.hr?a.hasreversehr?a.doScrollLeft(a.scrollvaluemaxw-Math.round(a.scroll.x*a.scrollratio.x),a.opt.cursordragspeed):a.doScrollLeft(Math.round(a.scroll.x*a.scrollratio.x),a.opt.cursordragspeed):a.doScrollTop(Math.round(a.scroll.y*a.scrollratio.y),a.opt.cursordragspeed))});return a.cancelEvent(b)}};if(e.cantouch||a.opt.touchbehavior)a.onpreventclick=function(b){if(a.preventclick)return a.preventclick.tg.onclick=a.preventclick.click,a.preventclick=!1,a.cancelEvent(b)},a.bind(a.win,"mousedown",
/* 53  */                 a.ontouchstart),a.onclick=e.isios?!1:function(b){return a.lastmouseup?(a.lastmouseup=!1,a.cancelEvent(b)):!0},a.opt.grabcursorenabled&&e.cursorgrabvalue&&(a.css(a.ispage?a.doc:a.win,{cursor:e.cursorgrabvalue}),a.css(a.rail,{cursor:e.cursorgrabvalue}));else{var p=function(b){if(a.selectiondrag){if(b){var c=a.win.outerHeight();b=b.pageY-a.selectiondrag.top;0<b&&b<c&&(b=0);b>=c&&(b-=c);a.selectiondrag.df=b}0!=a.selectiondrag.df&&(a.doScrollBy(2*-Math.floor(a.selectiondrag.df/6)),a.debounced("doselectionscroll",
/* 54  */                 function(){p()},50))}};a.hasTextSelected="getSelection"in document?function(){return 0<document.getSelection().rangeCount}:"selection"in document?function(){return"None"!=document.selection.type}:function(){return!1};a.onselectionstart=function(b){a.ispage||(a.selectiondrag=a.win.offset())};a.onselectionend=function(b){a.selectiondrag=!1};a.onselectiondrag=function(b){a.selectiondrag&&a.hasTextSelected()&&a.debounced("selectionscroll",function(){p(b)},250)}}e.hasw3ctouch?(a.css(a.rail,{"touch-action":"none"}),
/* 55  */                 a.css(a.cursor,{"touch-action":"none"}),a.bind(a.win,"pointerdown",a.ontouchstart),a.bind(document,"pointerup",a.ontouchend),a.bind(document,"pointermove",a.ontouchmove)):e.hasmstouch?(a.css(a.rail,{"-ms-touch-action":"none"}),a.css(a.cursor,{"-ms-touch-action":"none"}),a.bind(a.win,"MSPointerDown",a.ontouchstart),a.bind(document,"MSPointerUp",a.ontouchend),a.bind(document,"MSPointerMove",a.ontouchmove),a.bind(a.cursor,"MSGestureHold",function(a){a.preventDefault()}),a.bind(a.cursor,"contextmenu",
/* 56  */                 function(a){a.preventDefault()})):this.istouchcapable&&(a.bind(a.win,"touchstart",a.ontouchstart),a.bind(document,"touchend",a.ontouchend),a.bind(document,"touchcancel",a.ontouchend),a.bind(document,"touchmove",a.ontouchmove));if(a.opt.cursordragontouch||!e.cantouch&&!a.opt.touchbehavior)a.rail.css({cursor:"default"}),a.railh&&a.railh.css({cursor:"default"}),a.jqbind(a.rail,"mouseenter",function(){if(!a.ispage&&!a.win.is(":visible"))return!1;a.canshowonmouseevent&&a.showCursor();a.rail.active=!0}),
/* 57  */                 a.jqbind(a.rail,"mouseleave",function(){a.rail.active=!1;a.rail.drag||a.hideCursor()}),a.opt.sensitiverail&&(a.bind(a.rail,"click",function(b){a.doRailClick(b,!1,!1)}),a.bind(a.rail,"dblclick",function(b){a.doRailClick(b,!0,!1)}),a.bind(a.cursor,"click",function(b){a.cancelEvent(b)}),a.bind(a.cursor,"dblclick",function(b){a.cancelEvent(b)})),a.railh&&(a.jqbind(a.railh,"mouseenter",function(){if(!a.ispage&&!a.win.is(":visible"))return!1;a.canshowonmouseevent&&a.showCursor();a.rail.active=!0}),a.jqbind(a.railh,
/* 58  */                 "mouseleave",function(){a.rail.active=!1;a.rail.drag||a.hideCursor()}),a.opt.sensitiverail&&(a.bind(a.railh,"click",function(b){a.doRailClick(b,!1,!0)}),a.bind(a.railh,"dblclick",function(b){a.doRailClick(b,!0,!0)}),a.bind(a.cursorh,"click",function(b){a.cancelEvent(b)}),a.bind(a.cursorh,"dblclick",function(b){a.cancelEvent(b)})));e.cantouch||a.opt.touchbehavior?(a.bind(e.hasmousecapture?a.win:document,"mouseup",a.ontouchend),a.bind(document,"mousemove",a.ontouchmove),a.onclick&&a.bind(document,"click",
/* 59  */                 a.onclick),a.opt.cursordragontouch&&(a.bind(a.cursor,"mousedown",a.onmousedown),a.bind(a.cursor,"mouseup",a.onmouseup),a.cursorh&&a.bind(a.cursorh,"mousedown",function(b){a.onmousedown(b,!0)}),a.cursorh&&a.bind(a.cursorh,"mouseup",a.onmouseup))):(a.bind(e.hasmousecapture?a.win:document,"mouseup",a.onmouseup),a.bind(document,"mousemove",a.onmousemove),a.onclick&&a.bind(document,"click",a.onclick),a.bind(a.cursor,"mousedown",a.onmousedown),a.bind(a.cursor,"mouseup",a.onmouseup),a.railh&&(a.bind(a.cursorh,
/* 60  */                 "mousedown",function(b){a.onmousedown(b,!0)}),a.bind(a.cursorh,"mouseup",a.onmouseup)),!a.ispage&&a.opt.enablescrollonselection&&(a.bind(a.win[0],"mousedown",a.onselectionstart),a.bind(document,"mouseup",a.onselectionend),a.bind(a.cursor,"mouseup",a.onselectionend),a.cursorh&&a.bind(a.cursorh,"mouseup",a.onselectionend),a.bind(document,"mousemove",a.onselectiondrag)),a.zoom&&(a.jqbind(a.zoom,"mouseenter",function(){a.canshowonmouseevent&&a.showCursor();a.rail.active=!0}),a.jqbind(a.zoom,"mouseleave",
/* 61  */                 function(){a.rail.active=!1;a.rail.drag||a.hideCursor()})));a.opt.enablemousewheel&&(a.isiframe||a.bind(e.isie&&a.ispage?document:a.win,"mousewheel",a.onmousewheel),a.bind(a.rail,"mousewheel",a.onmousewheel),a.railh&&a.bind(a.railh,"mousewheel",a.onmousewheelhr));a.ispage||e.cantouch||/HTML|^BODY/.test(a.win[0].nodeName)||(a.win.attr("tabindex")||a.win.attr({tabindex:N++}),a.jqbind(a.win,"focus",function(b){y=a.getTarget(b).id||!0;a.hasfocus=!0;a.canshowonmouseevent&&a.noticeCursor()}),a.jqbind(a.win,
/* 62  */                 "blur",function(b){y=!1;a.hasfocus=!1}),a.jqbind(a.win,"mouseenter",function(b){D=a.getTarget(b).id||!0;a.hasmousefocus=!0;a.canshowonmouseevent&&a.noticeCursor()}),a.jqbind(a.win,"mouseleave",function(){D=!1;a.hasmousefocus=!1;a.rail.drag||a.hideCursor()}))}a.onkeypress=function(b){if(a.railslocked&&0==a.page.maxh)return!0;b=b?b:window.e;var c=a.getTarget(b);if(c&&/INPUT|TEXTAREA|SELECT|OPTION/.test(c.nodeName)&&(!c.getAttribute("type")&&!c.type||!/submit|button|cancel/i.tp)||f(c).attr("contenteditable"))return!0;
/* 63  */             if(a.hasfocus||a.hasmousefocus&&!y||a.ispage&&!y&&!D){c=b.keyCode;if(a.railslocked&&27!=c)return a.cancelEvent(b);var g=b.ctrlKey||!1,d=b.shiftKey||!1,e=!1;switch(c){case 38:case 63233:a.doScrollBy(72);e=!0;break;case 40:case 63235:a.doScrollBy(-72);e=!0;break;case 37:case 63232:a.railh&&(g?a.doScrollLeft(0):a.doScrollLeftBy(72),e=!0);break;case 39:case 63234:a.railh&&(g?a.doScrollLeft(a.page.maxw):a.doScrollLeftBy(-72),e=!0);break;case 33:case 63276:a.doScrollBy(a.view.h);e=!0;break;case 34:case 63277:a.doScrollBy(-a.view.h);
/* 64  */                 e=!0;break;case 36:case 63273:a.railh&&g?a.doScrollPos(0,0):a.doScrollTo(0);e=!0;break;case 35:case 63275:a.railh&&g?a.doScrollPos(a.page.maxw,a.page.maxh):a.doScrollTo(a.page.maxh);e=!0;break;case 32:a.opt.spacebarenabled&&(d?a.doScrollBy(a.view.h):a.doScrollBy(-a.view.h),e=!0);break;case 27:a.zoomactive&&(a.doZoom(),e=!0)}if(e)return a.cancelEvent(b)}};a.opt.enablekeyboard&&a.bind(document,e.isopera&&!e.isopera12?"keypress":"keydown",a.onkeypress);a.bind(document,"keydown",function(b){b.ctrlKey&&
/* 65  */         (a.wheelprevented=!0)});a.bind(document,"keyup",function(b){b.ctrlKey||(a.wheelprevented=!1)});a.bind(window,"blur",function(b){a.wheelprevented=!1});a.bind(window,"resize",a.lazyResize);a.bind(window,"orientationchange",a.lazyResize);a.bind(window,"load",a.lazyResize);if(e.ischrome&&!a.ispage&&!a.haswrapper){var r=a.win.attr("style"),c=parseFloat(a.win.css("width"))+1;a.win.css("width",c);a.synched("chromefix",function(){a.win.attr("style",r)})}a.onAttributeChange=function(b){a.lazyResize(a.isieold?
/* 66  */             250:30)};!1!==v&&(a.observerbody=new v(function(b){b.forEach(function(b){if("attributes"==b.type)return f("body").hasClass("modal-open")?a.hide():a.show()});if(document.body.scrollHeight!=a.page.maxh)return a.lazyResize(30)}),a.observerbody.observe(document.body,{childList:!0,subtree:!0,characterData:!1,attributes:!0,attributeFilter:["class"]}));a.ispage||a.haswrapper||(!1!==v?(a.observer=new v(function(b){b.forEach(a.onAttributeChange)}),a.observer.observe(a.win[0],{childList:!0,characterData:!1,
/* 67  */             attributes:!0,subtree:!1}),a.observerremover=new v(function(b){b.forEach(function(b){if(0<b.removedNodes.length)for(var c in b.removedNodes)if(a&&b.removedNodes[c]==a.win[0])return a.remove()})}),a.observerremover.observe(a.win[0].parentNode,{childList:!0,characterData:!1,attributes:!1,subtree:!1})):(a.bind(a.win,e.isie&&!e.isie9?"propertychange":"DOMAttrModified",a.onAttributeChange),e.isie9&&a.win[0].attachEvent("onpropertychange",a.onAttributeChange),a.bind(a.win,"DOMNodeRemoved",function(b){b.target==
/* 68  */         a.win[0]&&a.remove()})));!a.ispage&&a.opt.boxzoom&&a.bind(window,"resize",a.resizeZoom);a.istextarea&&a.bind(a.win,"mouseup",a.lazyResize);a.lazyResize(30)}if("IFRAME"==this.doc[0].nodeName){var M=function(){a.iframexd=!1;var b;try{b="contentDocument"in this?this.contentDocument:this.contentWindow.document}catch(c){a.iframexd=!0,b=!1}if(a.iframexd)return"console"in window&&console.log("NiceScroll error: policy restriced iframe"),!0;a.forcescreen=!0;a.isiframe&&(a.iframe={doc:f(b),html:a.doc.contents().find("html")[0],
/* 69  */         body:a.doc.contents().find("body")[0]},a.getContentSize=function(){return{w:Math.max(a.iframe.html.scrollWidth,a.iframe.body.scrollWidth),h:Math.max(a.iframe.html.scrollHeight,a.iframe.body.scrollHeight)}},a.docscroll=f(a.iframe.body));if(!e.isios&&a.opt.iframeautoresize&&!a.isiframe){a.win.scrollTop(0);a.doc.height("");var g=Math.max(b.getElementsByTagName("html")[0].scrollHeight,b.body.scrollHeight);a.doc.height(g)}a.lazyResize(30);e.isie7&&a.css(f(a.iframe.html),{"overflow-y":"hidden"});a.css(f(a.iframe.body),
/* 70  */         {"overflow-y":"hidden"});e.isios&&a.haswrapper&&a.css(f(b.body),{"-webkit-transform":"translate3d(0,0,0)"});"contentWindow"in this?a.bind(this.contentWindow,"scroll",a.onscroll):a.bind(b,"scroll",a.onscroll);a.opt.enablemousewheel&&a.bind(b,"mousewheel",a.onmousewheel);a.opt.enablekeyboard&&a.bind(b,e.isopera?"keypress":"keydown",a.onkeypress);if(e.cantouch||a.opt.touchbehavior)a.bind(b,"mousedown",a.ontouchstart),a.bind(b,"mousemove",function(b){return a.ontouchmove(b,!0)}),a.opt.grabcursorenabled&&
/* 71  */     e.cursorgrabvalue&&a.css(f(b.body),{cursor:e.cursorgrabvalue});a.bind(b,"mouseup",a.ontouchend);a.zoom&&(a.opt.dblclickzoom&&a.bind(b,"dblclick",a.doZoom),a.ongesturezoom&&a.bind(b,"gestureend",a.ongesturezoom))};this.doc[0].readyState&&"complete"==this.doc[0].readyState&&setTimeout(function(){M.call(a.doc[0],!1)},500);a.bind(this.doc,"load",M)}};this.showCursor=function(b,c){a.cursortimeout&&(clearTimeout(a.cursortimeout),a.cursortimeout=0);if(a.rail){a.autohidedom&&(a.autohidedom.stop().css({opacity:a.opt.cursoropacitymax}),
/* 72  */         a.cursoractive=!0);a.rail.drag&&1==a.rail.drag.pt||("undefined"!=typeof b&&!1!==b&&(a.scroll.y=Math.round(1*b/a.scrollratio.y)),"undefined"!=typeof c&&(a.scroll.x=Math.round(1*c/a.scrollratio.x)));a.cursor.css({height:a.cursorheight,top:a.scroll.y});if(a.cursorh){var d=a.hasreversehr?a.scrollvaluemaxw-a.scroll.x:a.scroll.x;!a.rail.align&&a.rail.visibility?a.cursorh.css({width:a.cursorwidth,left:d+a.rail.width}):a.cursorh.css({width:a.cursorwidth,left:d});a.cursoractive=!0}a.zoom&&a.zoom.stop().css({opacity:a.opt.cursoropacitymax})}};
/* 73  */     this.hideCursor=function(b){a.cursortimeout||!a.rail||!a.autohidedom||a.hasmousefocus&&"leave"==a.opt.autohidemode||(a.cursortimeout=setTimeout(function(){a.rail.active&&a.showonmouseevent||(a.autohidedom.stop().animate({opacity:a.opt.cursoropacitymin}),a.zoom&&a.zoom.stop().animate({opacity:a.opt.cursoropacitymin}),a.cursoractive=!1);a.cursortimeout=0},b||a.opt.hidecursordelay))};this.noticeCursor=function(b,c,d){a.showCursor(c,d);a.rail.active||a.hideCursor(b)};this.getContentSize=a.ispage?function(){return{w:Math.max(document.body.scrollWidth,
/* 74  */         document.documentElement.scrollWidth),h:Math.max(document.body.scrollHeight,document.documentElement.scrollHeight)}}:a.haswrapper?function(){return{w:a.doc.outerWidth()+parseInt(a.win.css("paddingLeft"))+parseInt(a.win.css("paddingRight")),h:a.doc.outerHeight()+parseInt(a.win.css("paddingTop"))+parseInt(a.win.css("paddingBottom"))}}:function(){return{w:a.docscroll[0].scrollWidth,h:a.docscroll[0].scrollHeight}};this.onResize=function(b,c){if(!a||!a.win)return!1;if(!a.haswrapper&&!a.ispage){if("none"==
/* 75  */         a.win.css("display"))return a.visibility&&a.hideRail().hideRailHr(),!1;a.hidden||a.visibility||a.showRail().showRailHr()}var d=a.page.maxh,e=a.page.maxw,f=a.view.h,h=a.view.w;a.view={w:a.ispage?a.win.width():parseInt(a.win[0].clientWidth),h:a.ispage?a.win.height():parseInt(a.win[0].clientHeight)};a.page=c?c:a.getContentSize();a.page.maxh=Math.max(0,a.page.h-a.view.h);a.page.maxw=Math.max(0,a.page.w-a.view.w);if(a.page.maxh==d&&a.page.maxw==e&&a.view.w==h&&a.view.h==f){if(a.ispage)return a;d=a.win.offset();
/* 76  */         if(a.lastposition&&(e=a.lastposition,e.top==d.top&&e.left==d.left))return a;a.lastposition=d}0==a.page.maxh?(a.hideRail(),a.scrollvaluemax=0,a.scroll.y=0,a.scrollratio.y=0,a.cursorheight=0,a.setScrollTop(0),a.rail.scrollable=!1):(a.page.maxh-=a.opt.railpadding.top+a.opt.railpadding.bottom,a.rail.scrollable=!0);0==a.page.maxw?(a.hideRailHr(),a.scrollvaluemaxw=0,a.scroll.x=0,a.scrollratio.x=0,a.cursorwidth=0,a.setScrollLeft(0),a.railh.scrollable=!1):(a.page.maxw-=a.opt.railpadding.left+a.opt.railpadding.right,
/* 77  */         a.railh.scrollable=!0);a.railslocked=a.locked||0==a.page.maxh&&0==a.page.maxw;if(a.railslocked)return a.ispage||a.updateScrollBar(a.view),!1;a.hidden||a.visibility?a.hidden||a.railh.visibility||a.showRailHr():a.showRail().showRailHr();a.istextarea&&a.win.css("resize")&&"none"!=a.win.css("resize")&&(a.view.h-=20);a.cursorheight=Math.min(a.view.h,Math.round(a.view.h/a.page.h*a.view.h));a.cursorheight=a.opt.cursorfixedheight?a.opt.cursorfixedheight:Math.max(a.opt.cursorminheight,a.cursorheight);a.cursorwidth=
/* 78  */         Math.min(a.view.w,Math.round(a.view.w/a.page.w*a.view.w));a.cursorwidth=a.opt.cursorfixedheight?a.opt.cursorfixedheight:Math.max(a.opt.cursorminheight,a.cursorwidth);a.scrollvaluemax=a.view.h-a.cursorheight-a.cursor.hborder-(a.opt.railpadding.top+a.opt.railpadding.bottom);a.railh&&(a.railh.width=0<a.page.maxh?a.view.w-a.rail.width:a.view.w,a.scrollvaluemaxw=a.railh.width-a.cursorwidth-a.cursorh.wborder-(a.opt.railpadding.left+a.opt.railpadding.right));a.ispage||a.updateScrollBar(a.view);a.scrollratio=
/* 79  */     {x:a.page.maxw/a.scrollvaluemaxw,y:a.page.maxh/a.scrollvaluemax};a.getScrollTop()>a.page.maxh?a.doScrollTop(a.page.maxh):(a.scroll.y=Math.round(a.getScrollTop()*(1/a.scrollratio.y)),a.scroll.x=Math.round(a.getScrollLeft()*(1/a.scrollratio.x)),a.cursoractive&&a.noticeCursor());a.scroll.y&&0==a.getScrollTop()&&a.doScrollTo(Math.floor(a.scroll.y*a.scrollratio.y));return a};this.resize=a.onResize;this.lazyResize=function(b){b=isNaN(b)?30:b;a.debounced("resize",a.resize,b);return a};this.jqbind=function(b,
/* 80  */                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             c,d){a.events.push({e:b,n:c,f:d,q:!0});f(b).bind(c,d)};this.bind=function(b,c,d,f){var h="jquery"in b?b[0]:b;"mousewheel"==c?window.addEventListener||"onwheel"in document?a._bind(h,"wheel",d,f||!1):(b="undefined"!=typeof document.onmousewheel?"mousewheel":"DOMMouseScroll",n(h,b,d,f||!1),"DOMMouseScroll"==b&&n(h,"MozMousePixelScroll",d,f||!1)):h.addEventListener?(e.cantouch&&/mouseup|mousedown|mousemove/.test(c)&&a._bind(h,"mousedown"==c?"touchstart":"mouseup"==c?"touchend":"touchmove",function(a){if(a.touches){if(2>
/* 81  */         a.touches.length){var b=a.touches.length?a.touches[0]:a;b.original=a;d.call(this,b)}}else a.changedTouches&&(b=a.changedTouches[0],b.original=a,d.call(this,b))},f||!1),a._bind(h,c,d,f||!1),e.cantouch&&"mouseup"==c&&a._bind(h,"touchcancel",d,f||!1)):a._bind(h,c,function(b){(b=b||window.event||!1)&&b.srcElement&&(b.target=b.srcElement);"pageY"in b||(b.pageX=b.clientX+document.documentElement.scrollLeft,b.pageY=b.clientY+document.documentElement.scrollTop);return!1===d.call(h,b)||!1===f?a.cancelEvent(b):
/* 82  */         !0})};e.haseventlistener?(this._bind=function(b,c,d,e){a.events.push({e:b,n:c,f:d,b:e,q:!1});b.addEventListener(c,d,e||!1)},this.cancelEvent=function(a){if(!a)return!1;a=a.original?a.original:a;a.preventDefault();a.stopPropagation();a.preventManipulation&&a.preventManipulation();return!1},this.stopPropagation=function(a){if(!a)return!1;a=a.original?a.original:a;a.stopPropagation();return!1},this._unbind=function(a,c,d,e){a.removeEventListener(c,d,e)}):(this._bind=function(b,c,d,e){a.events.push({e:b,
/* 83  */         n:c,f:d,b:e,q:!1});b.attachEvent?b.attachEvent("on"+c,d):b["on"+c]=d},this.cancelEvent=function(a){a=window.event||!1;if(!a)return!1;a.cancelBubble=!0;a.cancel=!0;return a.returnValue=!1},this.stopPropagation=function(a){a=window.event||!1;if(!a)return!1;a.cancelBubble=!0;return!1},this._unbind=function(a,c,d,e){a.detachEvent?a.detachEvent("on"+c,d):a["on"+c]=!1});this.unbindAll=function(){for(var b=0;b<a.events.length;b++){var c=a.events[b];c.q?c.e.unbind(c.n,c.f):a._unbind(c.e,c.n,c.f,c.b)}};this.showRail=
/* 84  */         function(){0==a.page.maxh||!a.ispage&&"none"==a.win.css("display")||(a.visibility=!0,a.rail.visibility=!0,a.rail.css("display","block"));return a};this.showRailHr=function(){if(!a.railh)return a;0==a.page.maxw||!a.ispage&&"none"==a.win.css("display")||(a.railh.visibility=!0,a.railh.css("display","block"));return a};this.hideRail=function(){a.visibility=!1;a.rail.visibility=!1;a.rail.css("display","none");return a};this.hideRailHr=function(){if(!a.railh)return a;a.railh.visibility=!1;a.railh.css("display",
/* 85  */         "none");return a};this.show=function(){a.hidden=!1;a.railslocked=!1;return a.showRail().showRailHr()};this.hide=function(){a.hidden=!0;a.railslocked=!0;return a.hideRail().hideRailHr()};this.toggle=function(){return a.hidden?a.show():a.hide()};this.remove=function(){a.stop();a.cursortimeout&&clearTimeout(a.cursortimeout);a.doZoomOut();a.unbindAll();e.isie9&&a.win[0].detachEvent("onpropertychange",a.onAttributeChange);!1!==a.observer&&a.observer.disconnect();!1!==a.observerremover&&a.observerremover.disconnect();
/* 86  */         !1!==a.observerbody&&a.observerbody.disconnect();a.events=null;a.cursor&&a.cursor.remove();a.cursorh&&a.cursorh.remove();a.rail&&a.rail.remove();a.railh&&a.railh.remove();a.zoom&&a.zoom.remove();for(var b=0;b<a.saved.css.length;b++){var c=a.saved.css[b];c[0].css(c[1],"undefined"==typeof c[2]?"":c[2])}a.saved=!1;a.me.data("__nicescroll","");var d=f.nicescroll;d.each(function(b){if(this&&this.id===a.id){delete d[b];for(var c=++b;c<d.length;c++,b++)d[b]=d[c];d.length--;d.length&&delete d[d.length]}});
/* 87  */         for(var h in a)a[h]=null,delete a[h];a=null};this.scrollstart=function(b){this.onscrollstart=b;return a};this.scrollend=function(b){this.onscrollend=b;return a};this.scrollcancel=function(b){this.onscrollcancel=b;return a};this.zoomin=function(b){this.onzoomin=b;return a};this.zoomout=function(b){this.onzoomout=b;return a};this.isScrollable=function(a){a=a.target?a.target:a;if("OPTION"==a.nodeName)return!0;for(;a&&1==a.nodeType&&!/^BODY|HTML/.test(a.nodeName);){var c=f(a),c=c.css("overflowY")||c.css("overflowX")||
/* 88  */         c.css("overflow")||"";if(/scroll|auto/.test(c))return a.clientHeight!=a.scrollHeight;a=a.parentNode?a.parentNode:!1}return!1};this.getViewport=function(a){for(a=a&&a.parentNode?a.parentNode:!1;a&&1==a.nodeType&&!/^BODY|HTML/.test(a.nodeName);){var c=f(a);if(/fixed|absolute/.test(c.css("position")))return c;var d=c.css("overflowY")||c.css("overflowX")||c.css("overflow")||"";if(/scroll|auto/.test(d)&&a.clientHeight!=a.scrollHeight||0<c.getNiceScroll().length)return c;a=a.parentNode?a.parentNode:!1}return!1};
/* 89  */     this.triggerScrollEnd=function(){if(a.onscrollend){var b=a.getScrollLeft(),c=a.getScrollTop();a.onscrollend.call(a,{type:"scrollend",current:{x:b,y:c},end:{x:b,y:c}})}};this.onmousewheel=function(b){if(!a.wheelprevented){if(a.railslocked)return a.debounced("checkunlock",a.resize,250),!0;if(a.rail.drag)return a.cancelEvent(b);"auto"==a.opt.oneaxismousemode&&0!=b.deltaX&&(a.opt.oneaxismousemode=!1);if(a.opt.oneaxismousemode&&0==b.deltaX&&!a.rail.scrollable)return a.railh&&a.railh.scrollable?a.onmousewheelhr(b):
/* 90  */         !0;var c=+new Date,d=!1;a.opt.preservenativescrolling&&a.checkarea+600<c&&(a.nativescrollingarea=a.isScrollable(b),d=!0);a.checkarea=c;if(a.nativescrollingarea)return!0;if(b=p(b,!1,d))a.checkarea=0;return b}};this.onmousewheelhr=function(b){if(!a.wheelprevented){if(a.railslocked||!a.railh.scrollable)return!0;if(a.rail.drag)return a.cancelEvent(b);var c=+new Date,d=!1;a.opt.preservenativescrolling&&a.checkarea+600<c&&(a.nativescrollingarea=a.isScrollable(b),d=!0);a.checkarea=c;return a.nativescrollingarea?
/* 91  */         !0:a.railslocked?a.cancelEvent(b):p(b,!0,d)}};this.stop=function(){a.cancelScroll();a.scrollmon&&a.scrollmon.stop();a.cursorfreezed=!1;a.scroll.y=Math.round(a.getScrollTop()*(1/a.scrollratio.y));a.noticeCursor();return a};this.getTransitionSpeed=function(b){var c=Math.round(10*a.opt.scrollspeed);b=Math.min(c,Math.round(b/20*a.opt.scrollspeed));return 20<b?b:0};a.opt.smoothscroll?a.ishwscroll&&e.hastransition&&a.opt.usetransition&&a.opt.smoothscroll?(this.prepareTransition=function(b,c){var d=c?20<
/* 92  */     b?b:0:a.getTransitionSpeed(b),f=d?e.prefixstyle+"transform "+d+"ms ease-out":"";a.lasttransitionstyle&&a.lasttransitionstyle==f||(a.lasttransitionstyle=f,a.doc.css(e.transitionstyle,f));return d},this.doScrollLeft=function(b,c){var d=a.scrollrunning?a.newscrolly:a.getScrollTop();a.doScrollPos(b,d,c)},this.doScrollTop=function(b,c){var d=a.scrollrunning?a.newscrollx:a.getScrollLeft();a.doScrollPos(d,b,c)},this.doScrollPos=function(b,c,d){var f=a.getScrollTop(),h=a.getScrollLeft();(0>(a.newscrolly-
/* 93  */     f)*(c-f)||0>(a.newscrollx-h)*(b-h))&&a.cancelScroll();0==a.opt.bouncescroll&&(0>c?c=0:c>a.page.maxh&&(c=a.page.maxh),0>b?b=0:b>a.page.maxw&&(b=a.page.maxw));if(a.scrollrunning&&b==a.newscrollx&&c==a.newscrolly)return!1;a.newscrolly=c;a.newscrollx=b;a.newscrollspeed=d||!1;if(a.timer)return!1;a.timer=setTimeout(function(){var d=a.getScrollTop(),f=a.getScrollLeft(),h,k;h=b-f;k=c-d;h=Math.round(Math.sqrt(Math.pow(h,2)+Math.pow(k,2)));h=a.newscrollspeed&&1<a.newscrollspeed?a.newscrollspeed:a.getTransitionSpeed(h);
/* 94  */         a.newscrollspeed&&1>=a.newscrollspeed&&(h*=a.newscrollspeed);a.prepareTransition(h,!0);a.timerscroll&&a.timerscroll.tm&&clearInterval(a.timerscroll.tm);0<h&&(!a.scrollrunning&&a.onscrollstart&&a.onscrollstart.call(a,{type:"scrollstart",current:{x:f,y:d},request:{x:b,y:c},end:{x:a.newscrollx,y:a.newscrolly},speed:h}),e.transitionend?a.scrollendtrapped||(a.scrollendtrapped=!0,a.bind(a.doc,e.transitionend,a.onScrollTransitionEnd,!1)):(a.scrollendtrapped&&clearTimeout(a.scrollendtrapped),a.scrollendtrapped=
/* 95  */             setTimeout(a.onScrollTransitionEnd,h)),a.timerscroll={bz:new A(d,a.newscrolly,h,0,0,.58,1),bh:new A(f,a.newscrollx,h,0,0,.58,1)},a.cursorfreezed||(a.timerscroll.tm=setInterval(function(){a.showCursor(a.getScrollTop(),a.getScrollLeft())},60)));a.synched("doScroll-set",function(){a.timer=0;a.scrollendtrapped&&(a.scrollrunning=!0);a.setScrollTop(a.newscrolly);a.setScrollLeft(a.newscrollx);if(!a.scrollendtrapped)a.onScrollTransitionEnd()})},50)},this.cancelScroll=function(){if(!a.scrollendtrapped)return!0;
/* 96  */         var b=a.getScrollTop(),c=a.getScrollLeft();a.scrollrunning=!1;e.transitionend||clearTimeout(e.transitionend);a.scrollendtrapped=!1;a._unbind(a.doc[0],e.transitionend,a.onScrollTransitionEnd);a.prepareTransition(0);a.setScrollTop(b);a.railh&&a.setScrollLeft(c);a.timerscroll&&a.timerscroll.tm&&clearInterval(a.timerscroll.tm);a.timerscroll=!1;a.cursorfreezed=!1;a.showCursor(b,c);return a},this.onScrollTransitionEnd=function(){a.scrollendtrapped&&a._unbind(a.doc[0],e.transitionend,a.onScrollTransitionEnd);
/* 97  */         a.scrollendtrapped=!1;a.prepareTransition(0);a.timerscroll&&a.timerscroll.tm&&clearInterval(a.timerscroll.tm);a.timerscroll=!1;var b=a.getScrollTop(),c=a.getScrollLeft();a.setScrollTop(b);a.railh&&a.setScrollLeft(c);a.noticeCursor(!1,b,c);a.cursorfreezed=!1;0>b?b=0:b>a.page.maxh&&(b=a.page.maxh);0>c?c=0:c>a.page.maxw&&(c=a.page.maxw);if(b!=a.newscrolly||c!=a.newscrollx)return a.doScrollPos(c,b,a.opt.snapbackspeed);a.onscrollend&&a.scrollrunning&&a.triggerScrollEnd();a.scrollrunning=!1}):(this.doScrollLeft=
/* 98  */         function(b,c){var d=a.scrollrunning?a.newscrolly:a.getScrollTop();a.doScrollPos(b,d,c)},this.doScrollTop=function(b,c){var d=a.scrollrunning?a.newscrollx:a.getScrollLeft();a.doScrollPos(d,b,c)},this.doScrollPos=function(b,c,d){function e(){if(a.cancelAnimationFrame)return!0;a.scrollrunning=!0;if(n=1-n)return a.timer=s(e)||1;var b=0,c,d,g=d=a.getScrollTop();if(a.dst.ay){g=a.bzscroll?a.dst.py+a.bzscroll.getNow()*a.dst.ay:a.newscrolly;c=g-d;if(0>c&&g<a.newscrolly||0<c&&g>a.newscrolly)g=a.newscrolly;
/* 99  */         a.setScrollTop(g);g==a.newscrolly&&(b=1)}else b=1;d=c=a.getScrollLeft();if(a.dst.ax){d=a.bzscroll?a.dst.px+a.bzscroll.getNow()*a.dst.ax:a.newscrollx;c=d-c;if(0>c&&d<a.newscrollx||0<c&&d>a.newscrollx)d=a.newscrollx;a.setScrollLeft(d);d==a.newscrollx&&(b+=1)}else b+=1;2==b?(a.timer=0,a.cursorfreezed=!1,a.bzscroll=!1,a.scrollrunning=!1,0>g?g=0:g>a.page.maxh&&(g=a.page.maxh),0>d?d=0:d>a.page.maxw&&(d=a.page.maxw),d!=a.newscrollx||g!=a.newscrolly?a.doScrollPos(d,g):a.onscrollend&&a.triggerScrollEnd()):
/* 100 */         a.timer=s(e)||1}c="undefined"==typeof c||!1===c?a.getScrollTop(!0):c;if(a.timer&&a.newscrolly==c&&a.newscrollx==b)return!0;a.timer&&t(a.timer);a.timer=0;var f=a.getScrollTop(),h=a.getScrollLeft();(0>(a.newscrolly-f)*(c-f)||0>(a.newscrollx-h)*(b-h))&&a.cancelScroll();a.newscrolly=c;a.newscrollx=b;a.bouncescroll&&a.rail.visibility||(0>a.newscrolly?a.newscrolly=0:a.newscrolly>a.page.maxh&&(a.newscrolly=a.page.maxh));a.bouncescroll&&a.railh.visibility||(0>a.newscrollx?a.newscrollx=0:a.newscrollx>a.page.maxw&&

/* nicescroll.js */

/* 101 */     (a.newscrollx=a.page.maxw));a.dst={};a.dst.x=b-h;a.dst.y=c-f;a.dst.px=h;a.dst.py=f;var k=Math.round(Math.sqrt(Math.pow(a.dst.x,2)+Math.pow(a.dst.y,2)));a.dst.ax=a.dst.x/k;a.dst.ay=a.dst.y/k;var l=0,m=k;0==a.dst.x?(l=f,m=c,a.dst.ay=1,a.dst.py=0):0==a.dst.y&&(l=h,m=b,a.dst.ax=1,a.dst.px=0);k=a.getTransitionSpeed(k);d&&1>=d&&(k*=d);a.bzscroll=0<k?a.bzscroll?a.bzscroll.update(m,k):new A(l,m,k,0,1,0,1):!1;if(!a.timer){(f==a.page.maxh&&c>=a.page.maxh||h==a.page.maxw&&b>=a.page.maxw)&&a.checkContentSize();
/* 102 */         var n=1;a.cancelAnimationFrame=!1;a.timer=1;a.onscrollstart&&!a.scrollrunning&&a.onscrollstart.call(a,{type:"scrollstart",current:{x:h,y:f},request:{x:b,y:c},end:{x:a.newscrollx,y:a.newscrolly},speed:k});e();(f==a.page.maxh&&c>=f||h==a.page.maxw&&b>=h)&&a.checkContentSize();a.noticeCursor()}},this.cancelScroll=function(){a.timer&&t(a.timer);a.timer=0;a.bzscroll=!1;a.scrollrunning=!1;return a}):(this.doScrollLeft=function(b,c){var d=a.getScrollTop();a.doScrollPos(b,d,c)},this.doScrollTop=function(b,
/* 103 */                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              c){var d=a.getScrollLeft();a.doScrollPos(d,b,c)},this.doScrollPos=function(b,c,d){var e=b>a.page.maxw?a.page.maxw:b;0>e&&(e=0);var f=c>a.page.maxh?a.page.maxh:c;0>f&&(f=0);a.synched("scroll",function(){a.setScrollTop(f);a.setScrollLeft(e)})},this.cancelScroll=function(){});this.doScrollBy=function(b,c){var d=0,d=c?Math.floor((a.scroll.y-b)*a.scrollratio.y):(a.timer?a.newscrolly:a.getScrollTop(!0))-b;if(a.bouncescroll){var e=Math.round(a.view.h/2);d<-e?d=-e:d>a.page.maxh+e&&(d=a.page.maxh+e)}a.cursorfreezed=
/* 104 */         !1;e=a.getScrollTop(!0);if(0>d&&0>=e)return a.noticeCursor();if(d>a.page.maxh&&e>=a.page.maxh)return a.checkContentSize(),a.noticeCursor();a.doScrollTop(d)};this.doScrollLeftBy=function(b,c){var d=0,d=c?Math.floor((a.scroll.x-b)*a.scrollratio.x):(a.timer?a.newscrollx:a.getScrollLeft(!0))-b;if(a.bouncescroll){var e=Math.round(a.view.w/2);d<-e?d=-e:d>a.page.maxw+e&&(d=a.page.maxw+e)}a.cursorfreezed=!1;e=a.getScrollLeft(!0);if(0>d&&0>=e||d>a.page.maxw&&e>=a.page.maxw)return a.noticeCursor();a.doScrollLeft(d)};
/* 105 */     this.doScrollTo=function(b,c){c&&Math.round(b*a.scrollratio.y);a.cursorfreezed=!1;a.doScrollTop(b)};this.checkContentSize=function(){var b=a.getContentSize();b.h==a.page.h&&b.w==a.page.w||a.resize(!1,b)};a.onscroll=function(b){a.rail.drag||a.cursorfreezed||a.synched("scroll",function(){a.scroll.y=Math.round(a.getScrollTop()*(1/a.scrollratio.y));a.railh&&(a.scroll.x=Math.round(a.getScrollLeft()*(1/a.scrollratio.x)));a.noticeCursor()})};a.bind(a.docscroll,"scroll",a.onscroll);this.doZoomIn=function(b){if(!a.zoomactive){a.zoomactive=
/* 106 */         !0;a.zoomrestore={style:{}};var c="position top left zIndex backgroundColor marginTop marginBottom marginLeft marginRight".split(" "),d=a.win[0].style,h;for(h in c){var k=c[h];a.zoomrestore.style[k]="undefined"!=typeof d[k]?d[k]:""}a.zoomrestore.style.width=a.win.css("width");a.zoomrestore.style.height=a.win.css("height");a.zoomrestore.padding={w:a.win.outerWidth()-a.win.width(),h:a.win.outerHeight()-a.win.height()};e.isios4&&(a.zoomrestore.scrollTop=f(window).scrollTop(),f(window).scrollTop(0));
/* 107 */         a.win.css({position:e.isios4?"absolute":"fixed",top:0,left:0,"z-index":x+100,margin:"0px"});c=a.win.css("backgroundColor");(""==c||/transparent|rgba\(0, 0, 0, 0\)|rgba\(0,0,0,0\)/.test(c))&&a.win.css("backgroundColor","#fff");a.rail.css({"z-index":x+101});a.zoom.css({"z-index":x+102});a.zoom.css("backgroundPosition","0px -18px");a.resizeZoom();a.onzoomin&&a.onzoomin.call(a);return a.cancelEvent(b)}};this.doZoomOut=function(b){if(a.zoomactive)return a.zoomactive=!1,a.win.css("margin",""),a.win.css(a.zoomrestore.style),
/* 108 */     e.isios4&&f(window).scrollTop(a.zoomrestore.scrollTop),a.rail.css({"z-index":a.zindex}),a.zoom.css({"z-index":a.zindex}),a.zoomrestore=!1,a.zoom.css("backgroundPosition","0px 0px"),a.onResize(),a.onzoomout&&a.onzoomout.call(a),a.cancelEvent(b)};this.doZoom=function(b){return a.zoomactive?a.doZoomOut(b):a.doZoomIn(b)};this.resizeZoom=function(){if(a.zoomactive){var b=a.getScrollTop();a.win.css({width:f(window).width()-a.zoomrestore.padding.w+"px",height:f(window).height()-a.zoomrestore.padding.h+"px"});
/* 109 */         a.onResize();a.setScrollTop(Math.min(a.page.maxh,b))}};this.init();f.nicescroll.push(this)},L=function(f){var c=this;this.nc=f;this.steptime=this.lasttime=this.speedy=this.speedx=this.lasty=this.lastx=0;this.snapy=this.snapx=!1;this.demuly=this.demulx=0;this.lastscrolly=this.lastscrollx=-1;this.timer=this.chky=this.chkx=0;this.time=function(){return+new Date};this.reset=function(f,k){c.stop();var d=c.time();c.steptime=0;c.lasttime=d;c.speedx=0;c.speedy=0;c.lastx=f;c.lasty=k;c.lastscrollx=-1;c.lastscrolly=
/* 110 */     -1};this.update=function(f,k){var d=c.time();c.steptime=d-c.lasttime;c.lasttime=d;var d=k-c.lasty,n=f-c.lastx,p=c.nc.getScrollTop(),a=c.nc.getScrollLeft(),p=p+d,a=a+n;c.snapx=0>a||a>c.nc.page.maxw;c.snapy=0>p||p>c.nc.page.maxh;c.speedx=n;c.speedy=d;c.lastx=f;c.lasty=k};this.stop=function(){c.nc.unsynched("domomentum2d");c.timer&&clearTimeout(c.timer);c.timer=0;c.lastscrollx=-1;c.lastscrolly=-1};this.doSnapy=function(f,k){var d=!1;0>k?(k=0,d=!0):k>c.nc.page.maxh&&(k=c.nc.page.maxh,d=!0);0>f?(f=0,d=
/* 111 */     !0):f>c.nc.page.maxw&&(f=c.nc.page.maxw,d=!0);d?c.nc.doScrollPos(f,k,c.nc.opt.snapbackspeed):c.nc.triggerScrollEnd()};this.doMomentum=function(f){var k=c.time(),d=f?k+f:c.lasttime;f=c.nc.getScrollLeft();var n=c.nc.getScrollTop(),p=c.nc.page.maxh,a=c.nc.page.maxw;c.speedx=0<a?Math.min(60,c.speedx):0;c.speedy=0<p?Math.min(60,c.speedy):0;d=d&&60>=k-d;if(0>n||n>p||0>f||f>a)d=!1;f=c.speedx&&d?c.speedx:!1;if(c.speedy&&d&&c.speedy||f){var s=Math.max(16,c.steptime);50<s&&(f=s/50,c.speedx*=f,c.speedy*=f,s=
/* 112 */     50);c.demulxy=0;c.lastscrollx=c.nc.getScrollLeft();c.chkx=c.lastscrollx;c.lastscrolly=c.nc.getScrollTop();c.chky=c.lastscrolly;var e=c.lastscrollx,r=c.lastscrolly,t=function(){var d=600<c.time()-k?.04:.02;c.speedx&&(e=Math.floor(c.lastscrollx-c.speedx*(1-c.demulxy)),c.lastscrollx=e,0>e||e>a)&&(d=.1);c.speedy&&(r=Math.floor(c.lastscrolly-c.speedy*(1-c.demulxy)),c.lastscrolly=r,0>r||r>p)&&(d=.1);c.demulxy=Math.min(1,c.demulxy+d);c.nc.synched("domomentum2d",function(){c.speedx&&(c.nc.getScrollLeft()!=
/* 113 */ c.chkx&&c.stop(),c.chkx=e,c.nc.setScrollLeft(e));c.speedy&&(c.nc.getScrollTop()!=c.chky&&c.stop(),c.chky=r,c.nc.setScrollTop(r));c.timer||(c.nc.hideCursor(),c.doSnapy(e,r))});1>c.demulxy?c.timer=setTimeout(t,s):(c.stop(),c.nc.hideCursor(),c.doSnapy(e,r))};t()}else c.doSnapy(c.nc.getScrollLeft(),c.nc.getScrollTop())}},w=f.fn.scrollTop;f.cssHooks.pageYOffset={get:function(k,c,h){return(c=f.data(k,"__nicescroll")||!1)&&c.ishwscroll?c.getScrollTop():w.call(k)},set:function(k,c){var h=f.data(k,"__nicescroll")||
/* 114 */     !1;h&&h.ishwscroll?h.setScrollTop(parseInt(c)):w.call(k,c);return this}};f.fn.scrollTop=function(k){if("undefined"==typeof k){var c=this[0]?f.data(this[0],"__nicescroll")||!1:!1;return c&&c.ishwscroll?c.getScrollTop():w.call(this)}return this.each(function(){var c=f.data(this,"__nicescroll")||!1;c&&c.ishwscroll?c.setScrollTop(parseInt(k)):w.call(f(this),k)})};var B=f.fn.scrollLeft;f.cssHooks.pageXOffset={get:function(k,c,h){return(c=f.data(k,"__nicescroll")||!1)&&c.ishwscroll?c.getScrollLeft():B.call(k)},
/* 115 */     set:function(k,c){var h=f.data(k,"__nicescroll")||!1;h&&h.ishwscroll?h.setScrollLeft(parseInt(c)):B.call(k,c);return this}};f.fn.scrollLeft=function(k){if("undefined"==typeof k){var c=this[0]?f.data(this[0],"__nicescroll")||!1:!1;return c&&c.ishwscroll?c.getScrollLeft():B.call(this)}return this.each(function(){var c=f.data(this,"__nicescroll")||!1;c&&c.ishwscroll?c.setScrollLeft(parseInt(k)):B.call(f(this),k)})};var C=function(k){var c=this;this.length=0;this.name="nicescrollarray";this.each=function(d){for(var f=
/* 116 */     0,h=0;f<c.length;f++)d.call(c[f],h++);return c};this.push=function(d){c[c.length]=d;c.length++};this.eq=function(d){return c[d]};if(k)for(var h=0;h<k.length;h++){var m=f.data(k[h],"__nicescroll")||!1;m&&(this[this.length]=m,this.length++)}return this};(function(f,c,h){for(var m=0;m<c.length;m++)h(f,c[m])})(C.prototype,"show hide toggle onResize resize remove stop doScrollPos".split(" "),function(f,c){f[c]=function(){var f=arguments;return this.each(function(){this[c].apply(this,f)})}});f.fn.getNiceScroll=
/* 117 */     function(k){return"undefined"==typeof k?new C(this):this[k]&&f.data(this[k],"__nicescroll")||!1};f.extend(f.expr[":"],{nicescroll:function(k){return f.data(k,"__nicescroll")?!0:!1}});f.fn.niceScroll=function(k,c){"undefined"!=typeof c||"object"!=typeof k||"jquery"in k||(c=k,k=!1);c=f.extend({},c);var h=new C;"undefined"==typeof c&&(c={});k&&(c.doc=f(k),c.win=f(this));var m=!("doc"in c);m||"win"in c||(c.win=f(this));this.each(function(){var d=f(this).data("__nicescroll")||!1;d||(c.doc=m?f(this):c.doc,
/* 118 */     d=new R(c,f(this)),f(this).data("__nicescroll",d));h.push(d)});return 1==h.length?h[0]:h};window.NiceScroll={getjQuery:function(){return f}};f.nicescroll||(f.nicescroll=new C,f.nicescroll.options=I)});
/* 119 */ 
/* 120 */ 
/* 121 */ jQuery(document).ready(function($){
/* 122 */     $('html').niceScroll({
/* 123 */         cursorcolor: "#000",
/* 124 */         cursorborder: "0px solid #fff",
/* 125 */         railpadding: {
/* 126 */             top: 0,
/* 127 */             right: 0,
/* 128 */             left: 0,
/* 129 */             bottom: 0
/* 130 */         },
/* 131 */         cursorwidth: "10px",
/* 132 */         cursorborderradius: "0px",
/* 133 */         cursoropacitymin: 0.2,
/* 134 */         cursoropacitymax: 0.8,
/* 135 */         boxzoom: true,
/* 136 */         horizrailenabled: false,
/* 137 */         zindex: 9999
/* 138 */     });
/* 139 */ });
