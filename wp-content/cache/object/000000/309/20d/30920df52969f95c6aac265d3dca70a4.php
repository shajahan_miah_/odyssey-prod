|w�W<?php exit; ?>a:1:{s:7:"content";a:1:{i:0;O:8:"stdClass":4:{s:7:"meta_id";s:5:"19842";s:7:"post_id";s:4:"6272";s:8:"meta_key";s:13:"tours_program";s:10:"meta_value";s:5874:"a:6:{i:0;a:2:{s:5:"title";s:22:"Day 1:  Arrive Nairobi";s:4:"desc";s:488:"Following your flight Odyssey Safari staff will greet you at the airport and load your bags for transport to the hotel. Once you are settled in you may select an optional activity or simply relax at the camp for the trip ahead. Some of the possible optional activities include: Bomas of Kenya, Sheldrick Elephant Orphanage, Karen Blixen Museum, Tour of Downtown Nairobi, National Museum, or Langata Giraffe Center.
Overnight at Safari Park hotel & Casino 
Meal Plan:  Bed & Breakfast 
";}i:1;a:2:{s:5:"title";s:35:"Day 2-3:  Amboseli National Reserve";s:4:"desc";s:1765:"After breakfast our adventures in the bush begin with spectacular Amboseli National Park. Romantically situated in the foothills of Mount Kilimanjaro, Amboseli is famous for its tremendous variety of big game. 

Depart Nairobi in the morning and travel to the Amboseli National reserve at the foot of snow capped Mt. Kilimanjaro, Africa's highest peak. Arrive in time for lunch. Afternoon game drive follows. Amboseli is an excellent place to view a multitude of wildlife, including everything from Lions, Cheetah to Wildebeest to Giraffes to Baboons. 

Overnight at Amboseli Sopa Lodge 
Meal Plan:  Breakfast, Lunch & Dinner

Day 4 starts with breakfast followed by a morning game drive in the park. Amboseli offers opportunities such as viewing large herds of elephants, nearly every vista within the park is framed by majestic Mount Kilimanjaro in the distance.  View the "Big Five" while visiting the Observation Hill which allows an overall view of the whole park especially the swamps and elephants, buffaloes, hippos and a variety of water fowls like pelican and the Egyptian goose.  Visit contemporary Masai culture and indigenous lifestyles in the nearby village. 
After lunch a game-drive follows. Optional visit to a Masai village is available as this is the home of the Masai people. Here you learn about their tribal social systems, customs and the place of the Masai in today’s Africa.  The residents live in their Manyatta, or village, much as they have for centuries and a visit gives you the opportunity to see the interior of these mud-hut homes, spotless and unadorned, contrasted with the Maasai themselves, bedecked with brilliantly colored beads and cloths.

Overnight at Amboseli Sopa Lodge.
Meal Plan:  Breakfast, Lunch & Dinner";}i:2;a:2:{s:5:"title";s:33:"Day 4:  Amboseli– Lake Naivasha";s:4:"desc";s:645:"Amboseli– Lake Naivasha
Our destination is Lake Naivsaha Sopa Lodge and we arrive in time for lunch at our lodge by the lake.  In the early afternoon you can take a boat ride on the lake or a visit and nature walk at Crescent Island which is an excellent way to see wild animals while on foot.  The island is a private game sanctuary home to zebra, wildebeest, gazelle, Vervet monkeys, hares, genet cats, waterbuck and giraffe.   You could also simply lounge by the pool admiring the view of zebras, Giraffes and waterbuck just outside the fence of the lodge.

Overnight at Lake Naivasha Sopa Lodge 
Meal Plan:  Breakfast, Lunch & Dinner
";}i:3;a:2:{s:5:"title";s:33:"Day 5:  Lake Nakuru National Park";s:4:"desc";s:835:"Today we travel to the exotic Lake Nakuru National Park located in the Great Rift Valley.   After a 30 minute drive an exciting game run is planned viewing rhino, leopard and the thousands of flamingos in this park.  The game in this park is plentiful and the magnificent bird watching will make a devotee out of even the most reluctant. The Park is a home to stunning flocks of lesser and Greater Flamingos, which literally turn the lake shores in to a magnificent pink stretch. The bird life here is world renown and over 400 bird species exist here, White Pelicans, Plovers, Egrets and Marabou Stork. It is also one of the very few places in Africa to see the white Rhino and rare Rothschild Giraffe.

Evening game drive – 3.00 p.m. to 6.15 p.m. 
Meal Plan:  Breakfast, Lunch & Dinner 
Overnight at Lake Naivasha Sopa Lodge 
";}i:4;a:2:{s:5:"title";s:38:"Day 6-7:  Maasai Mara National Reserve";s:4:"desc";s:1146:"This morning we drive through the dramatic Great Rift Valley to the Masai Mara National Reserve. This enormous reserve is actually part of the vast Serengeti plains famously known for its spectacular great wildebeest’s migration and is home of the Big Five: Lion, Elephant, Leopard, Buffalo and Rhino. Lunch at our Lodge and relax before departing for the afternoon game run. The Mara Game Reserve- one of the greatest wildernesses of the world. Large mammals are varied, and easy to see. Residents among the Park's are: Masai Giraffe, Buffalo, Eland and thousand of plain game including Impala, Zebra, Topi, both Thomson's and grants Gazelles.
Overnight at Ashnil Mara Tented camp
Evening game drive – 3.30 p.m. to 6.45 p.m. 
Meal Plan:  Breakfast, Lunch & Dinner 

Discover Masai Mara
The search for great predators and perhaps even cubs continues today during extensive game drives. On the plains are enormous herds of grazing animals plus the elusive cheetah and leopard hiding amidst acacia boughs.
Overnight at Ashnil Mara Tented camp
Full day game drive: 8.00 a.m. to 6.00 p.m. 
Meal Plan:  Breakfast, Picnic Lunch & Dinner 
";}i:5;a:2:{s:5:"title";s:39:"Day 8, Saturday:  Maasai Mara - Nairobi";s:4:"desc";s:487:"Enjoy early Breakfast at the Camp. Checkout of the Camp and conduct a game drive en-route with an optional opportunity to visit a village of the Maasai people to witness the singing and dancing that are part of their daily lives and sacred rituals. A glimpse into their homes and social structure is a poignant experience. Drive to Nairobi and thereafter to the airport for outbound flight.

Overnight at Safari Park hotel & Casino Meal Plan 
Meal Plan:  Breakfast and Picnic lunch 
";}}";}}}