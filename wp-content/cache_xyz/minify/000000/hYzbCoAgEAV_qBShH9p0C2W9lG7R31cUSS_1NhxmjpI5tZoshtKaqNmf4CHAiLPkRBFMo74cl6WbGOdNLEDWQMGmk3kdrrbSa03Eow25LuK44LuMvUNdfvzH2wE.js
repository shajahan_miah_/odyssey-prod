
/* upload.js */

/* 1   */ if(!(window.console && console.log)) {
/* 2   */   console = {
/* 3   */     log: function(){},
/* 4   */     debug: function(){},
/* 5   */     info: function(){},
/* 6   */     warn: function(){},
/* 7   */     error: function(){}
/* 8   */   };
/* 9   */ }
/* 10  */ 	
/* 11  */ 	
/* 12  */ 	
/* 13  */ jQuery( document ).ready(function($) {
/* 14  */ 	
/* 15  */ 	// edit a folder
/* 16  */ 	$( document ).on( "click", ".sp-cdm-save-category", function() {
/* 17  */ 			
/* 18  */ 			var id = jQuery(this).attr('data-id');
/* 19  */ 					
/* 20  */ 			if(jQuery("#edit_project_name_" + id).val() == ""){
/* 21  */ 			alert("Please enter a project name");
/* 22  */ 	
/* 23  */ 			}else{
/* 24  */ 			
/* 25  */ 			
/* 26  */ 				jQuery.post(sp_vars.ajax_url, {action: "cdm_save_category", name: jQuery("#edit_project_name_" + id).val(), id: id}, function(response){
/* 27  */ 										cdm_ajax_search();
/* 28  */ 		
/* 29  */ 					jQuery("#edit_category").dialog("close");
/* 30  */ 					alert(response);	
/* 31  */ 				})
/* 32  */ 			
/* 33  */ 			}
/* 34  */ 			
/* 35  */ 			return false;
/* 36  */ 			
/* 37  */ 		});
/* 38  */ 		
/* 39  */ 		
/* 40  */ 		// Delete a folder
/* 41  */ 		$( document ).on( "click", ".sp-cdm-delete-category", function() {
/* 42  */ 			var id = jQuery(this).attr('data-id');
/* 43  */ 			
/* 44  */ 			jQuery( "#delete_category_" + id ).dialog({
/* 45  */ 
/* 46  */ 			resizable: false,
/* 47  */ 
/* 48  */ 			height:240,
/* 49  */ 
/* 50  */ 			width:440,

/* upload.js */

/* 51  */ 
/* 52  */ 			modal: true,
/* 53  */ 
/* 54  */ 			buttons: {
/* 55  */ 
/* 56  */ 				"Delete all items": function() {
/* 57  */ 
/* 58  */ 							jQuery.post(sp_vars.ajax_url, {action: "cdm_remove_category", id: id}, function(response){
/* 59  */ 								jQuery.cookie("pid", 0, { expires: 7 , path:"/" }); 
/* 60  */ 								cdm_ajax_search();
/* 61  */ 							})
/* 62  */ 
/* 63  */ 					 
/* 64  */ 
/* 65  */ 					jQuery( this ).dialog( "close" );	
/* 66  */ 
/* 67  */ 						
/* 68  */ 
/* 69  */ 				},
/* 70  */ 
/* 71  */ 				Cancel: function() {
/* 72  */ 
/* 73  */ 					jQuery( this ).dialog( "close" );
/* 74  */ 
/* 75  */ 				}
/* 76  */ 
/* 77  */ 			}
/* 78  */ 
/* 79  */ 		});
/* 80  */ 
/* 81  */ 			
/* 82  */ 		return false;	
/* 83  */ 			
/* 84  */ 		});
/* 85  */ 		
/* 86  */ 		
/* 87  */ });
/* 88  */ 				
/* 89  */ 
/* 90  */ 
/* 91  */ 	
/* 92  */ 	function cdm_download_file(id){
/* 93  */ 		jQuery.get(sp_vars.ajax_url, {action: "cdm_download_file", fid:id}, function(response){
/* 94  */ 			
/* 95  */ 		});
/* 96  */ 		return false;
/* 97  */ 	}
/* 98  */ 	function cdm_download_project(id,nonce){
/* 99  */ 		
/* 100 */ 			window.location.href = sp_vars.ajax_url +"?action=cdm_download_project&nonce=" + nonce + "&id="+ id;

/* upload.js */

/* 101 */ 		
/* 102 */ 			
/* 103 */ 				return false;
/* 104 */ 	}
/* 105 */ 
/* 106 */ 	function cdmOpenModal(name){
/* 107 */ 				jQuery(".cdm-modal").remodal({ hashTracking: false});	
/* 108 */ 			  var inst = jQuery.remodal.lookup[jQuery("[data-remodal-id=" + name + "]").data("remodal")];
/* 109 */ 				inst.open();	
/* 110 */ 				
/* 111 */ 			}
/* 112 */ 			function cdmCloseModal(name){
/* 113 */ 				jQuery(".cdm-modal").remodal({ hashTracking: false});	
/* 114 */ 			  var inst = jQuery.remodal.lookup[jQuery("[data-remodal-id=" + name + "]").data("remodal")];
/* 115 */ 			
/* 116 */ 				inst.close();	
/* 117 */ 				
/* 118 */ 			}
/* 119 */ 			
/* 120 */ 			function cdmRefreshFile(file){
/* 121 */ 				
/* 122 */ 					jQuery(".view-file-content").empty();
/* 123 */ 				
/* 124 */ 				jQuery.get(sp_vars.ajax_url, {action: "cdm_view_file", id: file}, function(response){
/* 125 */ 						jQuery(".view-file-content").html(response);
/* 126 */ 				})		
/* 127 */ 				
/* 128 */ 			}
/* 129 */ 			function cdmViewFile(file){
/* 130 */ 				
/* 131 */ 				jQuery(".view-file-content").empty();
/* 132 */ 				
/* 133 */ 				jQuery.get(sp_vars.ajax_url, {action: "cdm_view_file", id: file}, function(response){
/* 134 */ 						jQuery(".view-file-content").html(response);
/* 135 */ 						 jQuery(".cdm-modal").remodal({ hashTracking: false});	
/* 136 */ 						 var inst = jQuery.remodal.lookup[jQuery("[data-remodal-id=file]").data("remodal")];
/* 137 */ 						 inst.open();
/* 138 */ 				})
/* 139 */ 			 
/* 140 */ 						 		
/* 141 */ 		
/* 142 */ 		}
/* 143 */ 		
/* 144 */ 		
/* 145 */ jQuery(document).on('closed', ".cdm-modal", function (e) {
/* 146 */ 
/* 147 */ jQuery.cookie("viewfile_tab", 0, { expires: 7 , path:"/" }); 	
/* 148 */  
/* 149 */ });
/* 150 */ 

/* upload.js */

/* 151 */ 		function sp_cu_add_project(){
/* 152 */ 		
/* 153 */ 			
/* 154 */ 				jQuery.post(sp_vars.ajax_url, {action: "cdm_save_category", name: jQuery("#sub_category_name").val(), uid: jQuery("#sub_category_uid").val(),parent: jQuery(".cdm_premium_pid_field").val()}, function(response){
/* 155 */ 						 cdmCloseModal("folder");
/* 156 */ 						 cdm_ajax_search();
/* 157 */ 						 jQuery(".pid_select").append(jQuery("<option>", { 
/* 158 */ 		
/* 159 */ 								value: response,
/* 160 */ 								text : jQuery("#sub_category_name").val(),
/* 161 */ 								selected : "selected"							
/* 162 */ 								 }
/* 163 */ 							
/* 164 */ 						 ));
/* 165 */ 		
/* 166 */ 				})				
/* 167 */ 		
/* 168 */ 			
/* 169 */ 		
/* 170 */ 		}
/* 171 */ 
/* 172 */ 
/* 173 */ 
/* 174 */ 
/* 175 */ function cdm_refresh_file_view(fid){
/* 176 */ 	jQuery(".view-file-content").empty();
/* 177 */ 	
/* 178 */ 	 jQuery.get(sp_vars.ajax_url, {action: "cdm_view_file", id: fid}, function(response){
/* 179 */ 						jQuery(".view-file-content").html(response);
/* 180 */ 	})	
/* 181 */ 	 
/* 182 */ 	
/* 183 */ }
/* 184 */ 
/* 185 */ function cdm_check_file_perms(pid){
/* 186 */ 			
/* 187 */ 		
/* 188 */ 		jQuery.post(sp_vars.ajax_url, {action: "cdm_file_permissions", pid:pid}, function(msg){
/* 189 */ 					
/* 190 */ 					if(msg == 1){
/* 191 */ 					jQuery('.hide_add_file_permission').show();	
/* 192 */ 					}else{
/* 193 */ 					jQuery('.hide_add_file_permission').hide();		
/* 194 */ 					}	
/* 195 */ 		});
/* 196 */ 				
/* 197 */ 	
/* 198 */ }
/* 199 */ 
/* 200 */ function cdm_check_folder_perms(pid){

/* upload.js */

/* 201 */ 			
/* 202 */ 			
/* 203 */ 		jQuery.post(sp_vars.ajax_url, {action: "cdm_folder_permissions", pid:pid}, function(msg){
/* 204 */ 					
/* 205 */ 					if(msg == 1){
/* 206 */ 					jQuery('.hide_add_folder_permission').show();	
/* 207 */ 					}else{
/* 208 */ 					jQuery('.hide_add_folder_permission').hide();		
/* 209 */ 					}	
/* 210 */ 		});
/* 211 */ 		
/* 212 */ 
/* 213 */ 				
/* 214 */ 	
/* 215 */ }
/* 216 */ 
/* 217 */ 
/* 218 */ function sp_cu_reload_all_projects(context_folder_pid){
/* 219 */ 	
/* 220 */ 
/* 221 */ 		
/* 222 */ 		jQuery.post(sp_vars.ajax_url, {action: "cdm_project_dropdown", pid:ontext_folder_pid}, function(msg){
/* 223 */ 					
/* 224 */ 					jQuery('.pid_select').html(msg);		
/* 225 */ 		});
/* 226 */ 		
/* 227 */ 
/* 228 */ 
/* 229 */ 		
/* 230 */ 	
/* 231 */ }
/* 232 */ 
/* 233 */ function sp_cu_confirm_delete(div,h,file_id){
/* 234 */ 
/* 235 */ 		
/* 236 */ if(jQuery(window).width()*0.9< 768){
/* 237 */ 	var width = jQuery(window).width()*0.9;	
/* 238 */ 	}else{
/* 239 */ 		var width = 320;
/* 240 */ 	}
/* 241 */ 
/* 242 */ 	var NewDialog = jQuery('<div id="sp_cu_confirm_delete"> ' + div + '</div>');
/* 243 */ 
/* 244 */ 	
/* 245 */ 
/* 246 */ 	jQuery(  NewDialog ).dialog({
/* 247 */ 
/* 248 */ 			width:width,
/* 249 */ 
/* 250 */ 			height:'auto',

/* upload.js */

/* 251 */ 
/* 252 */ 			modal: true,
/* 253 */ 
/* 254 */ 			buttons: {
/* 255 */ 
/* 256 */ 				"Yes": function() {
/* 257 */ 
/* 258 */ 				
/* 259 */ 					jQuery.post(sp_vars.ajax_url, {action: "cdm_delete_file", file_id:file_id}, function(response){
/* 260 */ 						
/* 261 */ 				cdmCloseModal('file');
/* 262 */ 				jQuery( NewDialog ).remove();
/* 263 */ 				 cdm_ajax_search();
/* 264 */ 				});
/* 265 */ 				
/* 266 */ 
/* 267 */ 				
/* 268 */ 
/* 269 */ 				},
/* 270 */ 
/* 271 */ 				Cancel: function() {
/* 272 */ 
/* 273 */ 					jQuery( NewDialog ).remove();
/* 274 */ 
/* 275 */ 				}
/* 276 */ 
/* 277 */ 			}
/* 278 */ 
/* 279 */ 		});
/* 280 */ 
/* 281 */ 	
/* 282 */ 
/* 283 */ }
/* 284 */ 
/* 285 */ 
/* 286 */ 
/* 287 */ 
/* 288 */ 
/* 289 */ 
/* 290 */ 
/* 291 */ function sp_cu_confirm(div,h,url){
/* 292 */ 
/* 293 */ 	
/* 294 */ if(jQuery(window).width()*0.9< 768){
/* 295 */ 	var width = jQuery(window).width()*0.9;	
/* 296 */ 	}else{
/* 297 */ 		var width = 320;
/* 298 */ 	}
/* 299 */ 	jQuery(  div ).dialog({
/* 300 */ 

/* upload.js */

/* 301 */ 			width:width,
/* 302 */ 
/* 303 */ 			height:'auto',
/* 304 */ 
/* 305 */ 			modal: true,
/* 306 */ 
/* 307 */ 			buttons: {
/* 308 */ 
/* 309 */ 				"Yes": function() {
/* 310 */ 
/* 311 */ 					window.location = url;
/* 312 */ 
/* 313 */ 				},
/* 314 */ 
/* 315 */ 				Cancel: function() {
/* 316 */ 
/* 317 */ 					jQuery( this ).dialog( "close" );
/* 318 */ 
/* 319 */ 				}
/* 320 */ 
/* 321 */ 			}
/* 322 */ 
/* 323 */ 		});
/* 324 */ 
/* 325 */ 	
/* 326 */ 
/* 327 */ }
/* 328 */ 
/* 329 */ 
/* 330 */ 
/* 331 */ function sp_cu_dialog(div,w,h){
/* 332 */ 
/* 333 */ 	
/* 334 */ 	
/* 335 */ 	
/* 336 */ 	if(jQuery(window).width()*0.9< 768){
/* 337 */ 	var width = jQuery(window).width()*0.9;	
/* 338 */ 	}else{
/* 339 */ 		var width = w;
/* 340 */ 	}
/* 341 */ 	var dialogBox = jQuery(div);
/* 342 */      var usableDialog = dialogBox[0];
/* 343 */       //jQuery("div.ui-dialog").remove();
/* 344 */             
/* 345 */ 	
/* 346 */ 	
/* 347 */ 	jQuery(usableDialog).dialog({
/* 348 */ 
/* 349 */ 			height:'auto',
/* 350 */ 

/* upload.js */

/* 351 */ 			width:width
/* 352 */ 
/* 353 */ 	});
/* 354 */ 
/* 355 */ }
/* 356 */ 
/* 357 */ /*
/* 358 *| jQuery(document).ready(function() {
/* 359 *| //  jQuery("#cdm_upload_table tr:first").css("display", "none");
/* 360 *| jQuery("#cdm_og").attr("checked","checked");
/* 361 *|    
/* 362 *| setInterval(function(){cdmPremiumReValidate();},1000);
/* 363 *| 
/* 364 *| });
/* 365 *| */
/* 366 */ 
/* 367 */ function cdm_disable_uploads(){
/* 368 */ 	
/* 369 */ 	jQuery(".sp_cdm_add_file").hide();
/* 370 */ 	jQuery(".sp_cdm_add_folder").hide();
/* 371 */ 	
/* 372 */ }
/* 373 */ 
/* 374 */ function cdm_enable_uploads(){
/* 375 */ 	
/* 376 */ 	jQuery(".sp_cdm_add_file").hide();
/* 377 */ 	jQuery(".sp_cdm_add_folder").hide();
/* 378 */ 	
/* 379 */ }
/* 380 */ jQuery(document).ready(function() {
/* 381 */ 
/* 382 */ 	
/* 383 */ 	
/* 384 */ 	
/* 385 */ 	jQuery( document ).on( "click", ".cdm_refresh_file_view", function() {
/* 386 */ 		cdm_refresh_file_view(jQuery(this).attr('data-id'));
/* 387 */ 		
/* 388 */ 		return false;
/* 389 */ 	});
/* 390 */ 	
/* 391 */ 	
/* 392 */ 
/* 393 */ jQuery( ".cdm_button" ).button();
/* 394 */ 
/* 395 */ 
/* 396 */ jQuery.ajaxSetup({ cache: false });
/* 397 */ 
/* 398 */ 
/* 399 */ 
/* 400 */ //add another file input when one is selected

/* upload.js */

/* 401 */ 
/* 402 */ var max = 20;
/* 403 */ 
/* 404 */ var replaceMe = function(){
/* 405 */ 
/* 406 */ 	var obj = jQuery(this);
/* 407 */ 
/* 408 */ 	if(jQuery("#cdm_upload_fields input[type='file']").length > max)
/* 409 */ 
/* 410 */ 	{
/* 411 */ 
/* 412 */ 		alert('fail');
/* 413 */ 
/* 414 */ 		obj.val("");
/* 415 */ 
/* 416 */ 		return false;
/* 417 */ 
/* 418 */ 	}
/* 419 */ 
/* 420 */ 	jQuery(obj).css({'position':'absolute','left':'-9999px','display':'none'}).parent().prepend('<input type="file" name="'+obj.attr('name')+'"/>')
/* 421 */ 
/* 422 */ 	jQuery('#upload_list').append('<div class="sp_upload_div"><span class="sp_upload_name">'+obj.val()+'</span><input type="button" value="cancel"/><div>');
/* 423 */ 
/* 424 */ 	jQuery("#cdm_upload_fields input[type='file']").change(replaceMe);
/* 425 */ 
/* 426 */ 	jQuery("#cdm_upload_fields input[type='button']").click(function(){
/* 427 */ 
/* 428 */ 		jQuery(this).parent().remove();
/* 429 */ 
/* 430 */ 		jQuery(obj).remove();
/* 431 */ 
/* 432 */ 		return false;
/* 433 */ 
/* 434 */ 		
/* 435 */ 
/* 436 */ 		
/* 437 */ 
/* 438 */ 	});
/* 439 */ 
/* 440 */ }
/* 441 */ 
/* 442 */ jQuery("#cdm_upload_fields input[type='file']").change(replaceMe);
/* 443 */ 
/* 444 */ 
/* 445 */ 
/* 446 */ 
/* 447 */ 
/* 448 */ 
/* 449 */ 
/* 450 */ 

/* upload.js */

/* 451 */ 
/* 452 */ 
/* 453 */ 
/* 454 */ 
/* 455 */ 
/* 456 */ 
/* 457 */ 
/* 458 */         jQuery('a.su_ajax').click(function() {
/* 459 */ 
/* 460 */             var url = this.href;
/* 461 */ 
/* 462 */             // show a spinner or something via css
/* 463 */ 
/* 464 */             var dialog = jQuery('<div style="display:none" class="loading"></div>').appendTo('body');
/* 465 */ 
/* 466 */             // open the dialog
/* 467 */ 
/* 468 */             dialog.dialog({
/* 469 */ 
/* 470 */                 // add a close listener to prevent adding multiple divs to the document
/* 471 */ 
/* 472 */                 close: function(event, ui) {
/* 473 */ 
/* 474 */                     // remove div with all data and events
/* 475 */ 
/* 476 */                     dialog.remove();
/* 477 */ 
/* 478 */                 },
/* 479 */ 
/* 480 */                 modal: true,
/* 481 */ 
/* 482 */ 				title: jQuery(this).attr('title'),
/* 483 */ 
/* 484 */ 				height:'auto',
/* 485 */ 
/* 486 */ 				width:700
/* 487 */ 
/* 488 */             });
/* 489 */ 
/* 490 */             // load remote content
/* 491 */ 
/* 492 */             dialog.load(
/* 493 */ 
/* 494 */                 url, 
/* 495 */ 
/* 496 */                 {}, // omit this param object to issue a GET request instead a POST request, otherwise you may provide post parameters within the object
/* 497 */ 
/* 498 */                 function (responseText, textStatus, XMLHttpRequest) {
/* 499 */ 
/* 500 */                     // remove the loading class

/* upload.js */

/* 501 */ 
/* 502 */                     dialog.removeClass('loading');
/* 503 */ 
/* 504 */                 }
/* 505 */ 
/* 506 */             );
/* 507 */ 
/* 508 */             //prevent the browser to follow the link
/* 509 */ 
/* 510 */             return false;
/* 511 */ 
/* 512 */         });
/* 513 */ 
/* 514 */ 
/* 515 */ 
/* 516 */ });
/* 517 */ 
/* 518 */ 
/* 519 */ 
/* 520 */ 
/* 521 */ 
/* 522 */ 
/* 523 */ 
/* 524 */ 
/* 525 */ 
/* 526 */ 
/* 527 */ 

;
/* jquery.validate.js */

/* 1   */ /*
/* 2   *| 
/* 3   *|  * Simple jQuery Form Validation Plugin
/* 4   *| 
/* 5   *|  * http://github.com/davist11/jQuery-Simple-Validate
/* 6   *| 
/* 7   *|  *
/* 8   *| 
/* 9   *|  * Copyright (c) 2010 Trevor Davis (http://trevordavis.net)
/* 10  *| 
/* 11  *|  * Dual licensed under the MIT and GPL licenses.
/* 12  *| 
/* 13  *|  * Uses the same license as jQuery, see:
/* 14  *| 
/* 15  *|  * http://jquery.org/license
/* 16  *| 
/* 17  *|  *
/* 18  *| 
/* 19  *|  * @version 0.3
/* 20  *| 
/* 21  *|  *
/* 22  *| 
/* 23  *|  * Example usage:
/* 24  *| 
/* 25  *|  * $('form.required-form').simpleValidate({
/* 26  *| 
/* 27  *|  *	 errorClass: 'error',
/* 28  *| 
/* 29  *|  *	 errorText: '{label} is a required field.',
/* 30  *| 
/* 31  *|  *	 emailErrorText: 'Please enter a valid {label}',
/* 32  *| 
/* 33  *|  *	 errorElement: 'strong',
/* 34  *| 
/* 35  *|  *	 removeLabelChar: '*',
/* 36  *| 
/* 37  *|  *	 inputErrorClass: '',
/* 38  *| 
/* 39  *|  *	 completeCallback: '',
/* 40  *| 
/* 41  *|  *	 ajaxRequest: false
/* 42  *| 
/* 43  *|  * });
/* 44  *| 
/* 45  *|  */
/* 46  */ 
/* 47  */ ;(function($, window, document, undefined){
/* 48  */ 
/* 49  */ 
/* 50  */ 

/* jquery.validate.js */

/* 51  */ 	// our plugin constructor
/* 52  */ 
/* 53  */ 	var SimpleValidate = function(elem, options) {
/* 54  */ 
/* 55  */ 		this.elem = elem;
/* 56  */ 
/* 57  */ 		this.$elem = $(elem);
/* 58  */ 
/* 59  */ 		this.options = options;
/* 60  */ 
/* 61  */ 		this.metadata = this.$elem.data('plugin-options');
/* 62  */ 
/* 63  */ 		this.$requiredInputs = this.$elem.find(':input.required');
/* 64  */ 
/* 65  */ 	};
/* 66  */ 
/* 67  */ 
/* 68  */ 
/* 69  */ 	// the plugin prototype
/* 70  */ 
/* 71  */ 	SimpleValidate.prototype = {
/* 72  */ 
/* 73  */ 		defaults: {
/* 74  */ 
/* 75  */ 			errorClass: 'error',
/* 76  */ 
/* 77  */ 			errorText: '{label} is a required field.',
/* 78  */ 
/* 79  */ 			emailErrorText: 'Please enter a valid {label}',
/* 80  */ 
/* 81  */ 			errorElement: 'strong',
/* 82  */ 
/* 83  */ 			removeLabelChar: '*',
/* 84  */ 
/* 85  */ 			inputErrorClass: 'input-error',
/* 86  */ 
/* 87  */ 			completeCallback: '',
/* 88  */ 
/* 89  */ 			ajaxRequest: false
/* 90  */ 
/* 91  */ 		},
/* 92  */ 
/* 93  */ 
/* 94  */ 
/* 95  */ 		init: function() {
/* 96  */ 
/* 97  */ 			var self = this;
/* 98  */ 
/* 99  */ 
/* 100 */ 

/* jquery.validate.js */

/* 101 */ 			// Introduce defaults that can be extended either
/* 102 */ 
/* 103 */ 			// globally or using an object literal.
/* 104 */ 
/* 105 */ 			self.config = $.extend({}, self.defaults, self.options, self.metadata);
/* 106 */ 
/* 107 */ 			
/* 108 */ 
/* 109 */ 			// What type of error message is it
/* 110 */ 
/* 111 */ 			self.errorMsgType = self.config.errorText.search(/{label}/);
/* 112 */ 
/* 113 */ 			self.emailErrorMsgType = self.config.emailErrorText.search(/{label}/);
/* 114 */ 
/* 115 */ 			
/* 116 */ 
/* 117 */ 			self.$elem.on('submit.simpleValidate', $.proxy(self.handleSubmit, self));
/* 118 */ 
/* 119 */ 
/* 120 */ 
/* 121 */ 			return this;
/* 122 */ 
/* 123 */ 		},
/* 124 */ 
/* 125 */ 		
/* 126 */ 
/* 127 */ 		checkField: function(index) {
/* 128 */ 
/* 129 */ 			var self = this;
/* 130 */ 
/* 131 */ 			var $field = self.$requiredInputs.eq(index);
/* 132 */ 
/* 133 */ 			var fieldValue = $.trim($field.val());
/* 134 */ 
/* 135 */ 			var labelText = $field.siblings('label').text().replace(self.config.removeLabelChar, '');
/* 136 */ 
/* 137 */ 			var errorMsg = '';
/* 138 */ 
/* 139 */ 			
/* 140 */ 
/* 141 */ 			//Check if it's empty or an invalid email and format the error message
/* 142 */ 
/* 143 */ 			if(fieldValue === '') {
/* 144 */ 
/* 145 */ 				errorMsg = self.formatErrorMsg(self.config.errorText, labelText, self.errorMsgType);
/* 146 */ 
/* 147 */ 				self.hasError = true;
/* 148 */ 
/* 149 */ 			} else if($field.hasClass('email')) {
/* 150 */ 

/* jquery.validate.js */

/* 151 */ 				if(!(/^([_a-z0-9-]+)(\.[_a-z0-9-]+)*@([a-z0-9-]+)(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/.test(fieldValue.toLowerCase()))) {
/* 152 */ 
/* 153 */ 					errorMsg = self.formatErrorMsg(self.config.emailErrorText, labelText, self.emailErrorMsgType);
/* 154 */ 
/* 155 */ 					self.hasError = true;
/* 156 */ 
/* 157 */ 				}
/* 158 */ 
/* 159 */ 			}
/* 160 */ 
/* 161 */ 			
/* 162 */ 
/* 163 */ 			//If there is an error, display it
/* 164 */ 
/* 165 */ 			if(errorMsg !== '') {
/* 166 */ 
/* 167 */ 			  $field.addClass(self.config.inputErrorClass).after('<' + self.config.errorElement + ' class="' + self.config.errorClass + '">' + errorMsg + '</' + self.config.errorElement + '>');
/* 168 */ 
/* 169 */ 			}
/* 170 */ 
/* 171 */ 		},
/* 172 */ 
/* 173 */ 		
/* 174 */ 
/* 175 */ 		formatErrorMsg: function(errorText, labelText, errorMsgType) {
/* 176 */ 
/* 177 */ 			return (errorMsgType > -1 ) ? errorText.replace('{label}', labelText) : errorText;
/* 178 */ 
/* 179 */ 		},
/* 180 */ 
/* 181 */ 		
/* 182 */ 
/* 183 */ 		handleSubmit: function(e) {
/* 184 */ 
/* 185 */ 			var self = this;
/* 186 */ 
/* 187 */ 			
/* 188 */ 
/* 189 */ 			// We are just starting, so there are no errors yet
/* 190 */ 
/* 191 */ 			this.hasError = false;
/* 192 */ 
/* 193 */ 			
/* 194 */ 
/* 195 */ 			// Reset existing displayed errors
/* 196 */ 
/* 197 */ 			self.$elem.find(self.config.errorElement + '.' + self.config.errorClass).remove();
/* 198 */ 
/* 199 */ 			self.$elem.find(':input.' + self.config.inputErrorClass).removeClass(self.config.inputErrorClass);
/* 200 */ 

/* jquery.validate.js */

/* 201 */ 			
/* 202 */ 
/* 203 */ 			// Check each field
/* 204 */ 
/* 205 */ 			self.$requiredInputs.each($.proxy(self.checkField, self));
/* 206 */ 
/* 207 */ 			
/* 208 */ 
/* 209 */ 			// Don't submit the form if there are errors
/* 210 */ 
/* 211 */ 			if(self.hasError) { 
/* 212 */ 
/* 213 */ 				e.preventDefault();
/* 214 */ 
/* 215 */ 			} else if(self.config.completeCallback !== '') { // If there is a callback
/* 216 */ 
/* 217 */ 				self.config.completeCallback(self.$elem);
/* 218 */ 
/* 219 */ 				
/* 220 */ 
/* 221 */ 				// If AJAX request
/* 222 */ 
/* 223 */ 				if(self.config.ajaxRequest) {
/* 224 */ 
/* 225 */ 					e.preventDefault();
/* 226 */ 
/* 227 */ 				}
/* 228 */ 
/* 229 */ 			}
/* 230 */ 
/* 231 */ 		}
/* 232 */ 
/* 233 */ 	};
/* 234 */ 
/* 235 */ 
/* 236 */ 
/* 237 */ 	SimpleValidate.defaults = SimpleValidate.prototype.defaults;
/* 238 */ 
/* 239 */ 
/* 240 */ 
/* 241 */ 	$.fn.simpleValidate = function(options) {
/* 242 */ 
/* 243 */ 		return this.each(function() {
/* 244 */ 
/* 245 */ 			new SimpleValidate(this, options).init();
/* 246 */ 
/* 247 */ 		});
/* 248 */ 
/* 249 */ 	};
/* 250 */ 

/* jquery.validate.js */

/* 251 */ 
/* 252 */ 
/* 253 */ })( jQuery, window , document );

;
/* swfupload.js */

/* 1   */ /**
/* 2   *|  * SWFUpload: http://www.swfupload.org, http://swfupload.googlecode.com
/* 3   *|  *
/* 4   *|  * mmSWFUpload 1.0: Flash upload dialog - http://profandesign.se/swfupload/,  http://www.vinterwebb.se/
/* 5   *|  *
/* 6   *|  * SWFUpload is (c) 2006-2007 Lars Huring, Olov Nilz�n and Mammon Media and is released under the MIT License:
/* 7   *|  * http://www.opensource.org/licenses/mit-license.php
/* 8   *|  *
/* 9   *|  * SWFUpload 2 is (c) 2007-2008 Jake Roberts and is released under the MIT License:
/* 10  *|  * http://www.opensource.org/licenses/mit-license.php
/* 11  *|  *
/* 12  *|  */
/* 13  */ 
/* 14  */ 
/* 15  */ /* ******************* */
/* 16  */ /* Constructor & Init  */
/* 17  */ /* ******************* */
/* 18  */ var SWFUpload;
/* 19  */ 
/* 20  */ if (SWFUpload == undefined) {
/* 21  */ 	SWFUpload = function (settings) {
/* 22  */ 		this.initSWFUpload(settings);
/* 23  */ 	};
/* 24  */ }
/* 25  */ 
/* 26  */ SWFUpload.prototype.initSWFUpload = function (settings) {
/* 27  */ 	try {
/* 28  */ 		this.customSettings = {};	// A container where developers can place their own settings associated with this instance.
/* 29  */ 		this.settings = settings;
/* 30  */ 		this.eventQueue = [];
/* 31  */ 		this.movieName = "SWFUpload_" + SWFUpload.movieCount++;
/* 32  */ 		this.movieElement = null;
/* 33  */ 
/* 34  */ 
/* 35  */ 		// Setup global control tracking
/* 36  */ 		SWFUpload.instances[this.movieName] = this;
/* 37  */ 
/* 38  */ 		// Load the settings.  Load the Flash movie.
/* 39  */ 		this.initSettings();
/* 40  */ 		this.loadFlash();
/* 41  */ 		this.displayDebugInfo();
/* 42  */ 	} catch (ex) {
/* 43  */ 		delete SWFUpload.instances[this.movieName];
/* 44  */ 		throw ex;
/* 45  */ 	}
/* 46  */ };
/* 47  */ 
/* 48  */ /* *************** */
/* 49  */ /* Static Members  */
/* 50  */ /* *************** */

/* swfupload.js */

/* 51  */ SWFUpload.instances = {};
/* 52  */ SWFUpload.movieCount = 0;
/* 53  */ SWFUpload.version = "2.2.0 2009-03-25";
/* 54  */ SWFUpload.QUEUE_ERROR = {
/* 55  */ 	QUEUE_LIMIT_EXCEEDED	  		: -100,
/* 56  */ 	FILE_EXCEEDS_SIZE_LIMIT  		: -110,
/* 57  */ 	ZERO_BYTE_FILE			  		: -120,
/* 58  */ 	INVALID_FILETYPE		  		: -130
/* 59  */ };
/* 60  */ SWFUpload.UPLOAD_ERROR = {
/* 61  */ 	HTTP_ERROR				  		: -200,
/* 62  */ 	MISSING_UPLOAD_URL	      		: -210,
/* 63  */ 	IO_ERROR				  		: -220,
/* 64  */ 	SECURITY_ERROR			  		: -230,
/* 65  */ 	UPLOAD_LIMIT_EXCEEDED	  		: -240,
/* 66  */ 	UPLOAD_FAILED			  		: -250,
/* 67  */ 	SPECIFIED_FILE_ID_NOT_FOUND		: -260,
/* 68  */ 	FILE_VALIDATION_FAILED	  		: -270,
/* 69  */ 	FILE_CANCELLED			  		: -280,
/* 70  */ 	UPLOAD_STOPPED					: -290
/* 71  */ };
/* 72  */ SWFUpload.FILE_STATUS = {
/* 73  */ 	QUEUED		 : -1,
/* 74  */ 	IN_PROGRESS	 : -2,
/* 75  */ 	ERROR		 : -3,
/* 76  */ 	COMPLETE	 : -4,
/* 77  */ 	CANCELLED	 : -5
/* 78  */ };
/* 79  */ SWFUpload.BUTTON_ACTION = {
/* 80  */ 	SELECT_FILE  : -100,
/* 81  */ 	SELECT_FILES : -110,
/* 82  */ 	START_UPLOAD : -120
/* 83  */ };
/* 84  */ SWFUpload.CURSOR = {
/* 85  */ 	ARROW : -1,
/* 86  */ 	HAND : -2
/* 87  */ };
/* 88  */ SWFUpload.WINDOW_MODE = {
/* 89  */ 	WINDOW : "window",
/* 90  */ 	TRANSPARENT : "transparent",
/* 91  */ 	OPAQUE : "opaque"
/* 92  */ };
/* 93  */ 
/* 94  */ // Private: takes a URL, determines if it is relative and converts to an absolute URL
/* 95  */ // using the current site. Only processes the URL if it can, otherwise returns the URL untouched
/* 96  */ SWFUpload.completeURL = function(url) {
/* 97  */ 	if (typeof(url) !== "string" || url.match(/^https?:\/\//i) || url.match(/^\//)) {
/* 98  */ 		return url;
/* 99  */ 	}
/* 100 */ 	

/* swfupload.js */

/* 101 */ 	var currentURL = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ":" + window.location.port : "");
/* 102 */ 	
/* 103 */ 	var indexSlash = window.location.pathname.lastIndexOf("/");
/* 104 */ 	if (indexSlash <= 0) {
/* 105 */ 		path = "/";
/* 106 */ 	} else {
/* 107 */ 		path = window.location.pathname.substr(0, indexSlash) + "/";
/* 108 */ 	}
/* 109 */ 	
/* 110 */ 	return /*currentURL +*/ path + url;
/* 111 */ 	
/* 112 */ };
/* 113 */ 
/* 114 */ 
/* 115 */ /* ******************** */
/* 116 */ /* Instance Members  */
/* 117 */ /* ******************** */
/* 118 */ 
/* 119 */ // Private: initSettings ensures that all the
/* 120 */ // settings are set, getting a default value if one was not assigned.
/* 121 */ SWFUpload.prototype.initSettings = function () {
/* 122 */ 	this.ensureDefault = function (settingName, defaultValue) {
/* 123 */ 		this.settings[settingName] = (this.settings[settingName] == undefined) ? defaultValue : this.settings[settingName];
/* 124 */ 	};
/* 125 */ 	
/* 126 */ 	// Upload backend settings
/* 127 */ 	this.ensureDefault("upload_url", "");
/* 128 */ 	this.ensureDefault("preserve_relative_urls", false);
/* 129 */ 	this.ensureDefault("file_post_name", "Filedata");
/* 130 */ 	this.ensureDefault("post_params", {});
/* 131 */ 	this.ensureDefault("use_query_string", false);
/* 132 */ 	this.ensureDefault("requeue_on_error", false);
/* 133 */ 	this.ensureDefault("http_success", []);
/* 134 */ 	this.ensureDefault("assume_success_timeout", 0);
/* 135 */ 	
/* 136 */ 	// File Settings
/* 137 */ 	this.ensureDefault("file_types", "*.*");
/* 138 */ 	this.ensureDefault("file_types_description", "All Files");
/* 139 */ 	this.ensureDefault("file_size_limit", 0);	// Default zero means "unlimited"
/* 140 */ 	this.ensureDefault("file_upload_limit", 0);
/* 141 */ 	this.ensureDefault("file_queue_limit", 0);
/* 142 */ 
/* 143 */ 	// Flash Settings
/* 144 */ 	this.ensureDefault("flash_url", "swfupload.swf");
/* 145 */ 	this.ensureDefault("prevent_swf_caching", true);
/* 146 */ 	
/* 147 */ 	// Button Settings
/* 148 */ 	this.ensureDefault("button_image_url", "");
/* 149 */ 	this.ensureDefault("button_width", 1);
/* 150 */ 	this.ensureDefault("button_height", 1);

/* swfupload.js */

/* 151 */ 	this.ensureDefault("button_text", "");
/* 152 */ 	this.ensureDefault("button_text_style", "color: #000000; font-size: 16pt;");
/* 153 */ 	this.ensureDefault("button_text_top_padding", 0);
/* 154 */ 	this.ensureDefault("button_text_left_padding", 0);
/* 155 */ 	this.ensureDefault("button_action", SWFUpload.BUTTON_ACTION.SELECT_FILES);
/* 156 */ 	this.ensureDefault("button_disabled", false);
/* 157 */ 	this.ensureDefault("button_placeholder_id", "");
/* 158 */ 	this.ensureDefault("button_placeholder", null);
/* 159 */ 	this.ensureDefault("button_cursor", SWFUpload.CURSOR.ARROW);
/* 160 */ 	this.ensureDefault("button_window_mode", SWFUpload.WINDOW_MODE.WINDOW);
/* 161 */ 	
/* 162 */ 	// Debug Settings
/* 163 */ 	this.ensureDefault("debug", false);
/* 164 */ 	this.settings.debug_enabled = this.settings.debug;	// Here to maintain v2 API
/* 165 */ 	
/* 166 */ 	// Event Handlers
/* 167 */ 	this.settings.return_upload_start_handler = this.returnUploadStart;
/* 168 */ 	this.ensureDefault("swfupload_loaded_handler", null);
/* 169 */ 	this.ensureDefault("file_dialog_start_handler", null);
/* 170 */ 	this.ensureDefault("file_queued_handler", null);
/* 171 */ 	this.ensureDefault("file_queue_error_handler", null);
/* 172 */ 	this.ensureDefault("file_dialog_complete_handler", null);
/* 173 */ 	
/* 174 */ 	this.ensureDefault("upload_start_handler", null);
/* 175 */ 	this.ensureDefault("upload_progress_handler", null);
/* 176 */ 	this.ensureDefault("upload_error_handler", null);
/* 177 */ 	this.ensureDefault("upload_success_handler", null);
/* 178 */ 	this.ensureDefault("upload_complete_handler", null);
/* 179 */ 	
/* 180 */ 	this.ensureDefault("debug_handler", this.debugMessage);
/* 181 */ 
/* 182 */ 	this.ensureDefault("custom_settings", {});
/* 183 */ 
/* 184 */ 	// Other settings
/* 185 */ 	this.customSettings = this.settings.custom_settings;
/* 186 */ 	
/* 187 */ 	// Update the flash url if needed
/* 188 */ 	if (!!this.settings.prevent_swf_caching) {
/* 189 */ 		this.settings.flash_url = this.settings.flash_url + (this.settings.flash_url.indexOf("?") < 0 ? "?" : "&") + "preventswfcaching=" + new Date().getTime();
/* 190 */ 	}
/* 191 */ 	
/* 192 */ 	if (!this.settings.preserve_relative_urls) {
/* 193 */ 		//this.settings.flash_url = SWFUpload.completeURL(this.settings.flash_url);	// Don't need to do this one since flash doesn't look at it
/* 194 */ 		this.settings.upload_url = SWFUpload.completeURL(this.settings.upload_url);
/* 195 */ 		this.settings.button_image_url = SWFUpload.completeURL(this.settings.button_image_url);
/* 196 */ 	}
/* 197 */ 	
/* 198 */ 	delete this.ensureDefault;
/* 199 */ };
/* 200 */ 

/* swfupload.js */

/* 201 */ // Private: loadFlash replaces the button_placeholder element with the flash movie.
/* 202 */ SWFUpload.prototype.loadFlash = function () {
/* 203 */ 	var targetElement, tempParent;
/* 204 */ 
/* 205 */ 	// Make sure an element with the ID we are going to use doesn't already exist
/* 206 */ 	if (document.getElementById(this.movieName) !== null) {
/* 207 */ 		throw "ID " + this.movieName + " is already in use. The Flash Object could not be added";
/* 208 */ 	}
/* 209 */ 
/* 210 */ 	// Get the element where we will be placing the flash movie
/* 211 */ 	targetElement = document.getElementById(this.settings.button_placeholder_id) || this.settings.button_placeholder;
/* 212 */ 
/* 213 */ 	if (targetElement == undefined) {
/* 214 */ 		throw "Could not find the placeholder element: " + this.settings.button_placeholder_id;
/* 215 */ 	}
/* 216 */ 
/* 217 */ 	// Append the container and load the flash
/* 218 */ 	tempParent = document.createElement("div");
/* 219 */ 	tempParent.innerHTML = this.getFlashHTML();	// Using innerHTML is non-standard but the only sensible way to dynamically add Flash in IE (and maybe other browsers)
/* 220 */ 	targetElement.parentNode.replaceChild(tempParent.firstChild, targetElement);
/* 221 */ 
/* 222 */ 	// Fix IE Flash/Form bug
/* 223 */ 	if (window[this.movieName] == undefined) {
/* 224 */ 		window[this.movieName] = this.getMovieElement();
/* 225 */ 	}
/* 226 */ 	
/* 227 */ };
/* 228 */ 
/* 229 */ // Private: getFlashHTML generates the object tag needed to embed the flash in to the document
/* 230 */ SWFUpload.prototype.getFlashHTML = function () {
/* 231 */ 	// Flash Satay object syntax: http://www.alistapart.com/articles/flashsatay
/* 232 */ 	return ['<object id="', this.movieName, '" type="application/x-shockwave-flash" data="', this.settings.flash_url, '" width="', this.settings.button_width, '" height="', this.settings.button_height, '" class="swfupload">',
/* 233 */ 				'<param name="wmode" value="', this.settings.button_window_mode, '" />',
/* 234 */ 				'<param name="movie" value="', this.settings.flash_url, '" />',
/* 235 */ 				'<param name="quality" value="high" />',
/* 236 */ 				'<param name="menu" value="false" />',
/* 237 */ 				'<param name="allowScriptAccess" value="always" />',
/* 238 */ 				'<param name="flashvars" value="' + this.getFlashVars() + '" />',
/* 239 */ 				'</object>'].join("");
/* 240 */ };
/* 241 */ 
/* 242 */ // Private: getFlashVars builds the parameter string that will be passed
/* 243 */ // to flash in the flashvars param.
/* 244 */ SWFUpload.prototype.getFlashVars = function () {
/* 245 */ 	// Build a string from the post param object
/* 246 */ 	var paramString = this.buildParamString();
/* 247 */ 	var httpSuccessString = this.settings.http_success.join(",");
/* 248 */ 	
/* 249 */ 	// Build the parameter string
/* 250 */ 	return ["movieName=", encodeURIComponent(this.movieName),

/* swfupload.js */

/* 251 */ 			"&amp;uploadURL=", encodeURIComponent(this.settings.upload_url),
/* 252 */ 			"&amp;useQueryString=", encodeURIComponent(this.settings.use_query_string),
/* 253 */ 			"&amp;requeueOnError=", encodeURIComponent(this.settings.requeue_on_error),
/* 254 */ 			"&amp;httpSuccess=", encodeURIComponent(httpSuccessString),
/* 255 */ 			"&amp;assumeSuccessTimeout=", encodeURIComponent(this.settings.assume_success_timeout),
/* 256 */ 			"&amp;params=", encodeURIComponent(paramString),
/* 257 */ 			"&amp;filePostName=", encodeURIComponent(this.settings.file_post_name),
/* 258 */ 			"&amp;fileTypes=", encodeURIComponent(this.settings.file_types),
/* 259 */ 			"&amp;fileTypesDescription=", encodeURIComponent(this.settings.file_types_description),
/* 260 */ 			"&amp;fileSizeLimit=", encodeURIComponent(this.settings.file_size_limit),
/* 261 */ 			"&amp;fileUploadLimit=", encodeURIComponent(this.settings.file_upload_limit),
/* 262 */ 			"&amp;fileQueueLimit=", encodeURIComponent(this.settings.file_queue_limit),
/* 263 */ 			"&amp;debugEnabled=", encodeURIComponent(this.settings.debug_enabled),
/* 264 */ 			"&amp;buttonImageURL=", encodeURIComponent(this.settings.button_image_url),
/* 265 */ 			"&amp;buttonWidth=", encodeURIComponent(this.settings.button_width),
/* 266 */ 			"&amp;buttonHeight=", encodeURIComponent(this.settings.button_height),
/* 267 */ 			"&amp;buttonText=", encodeURIComponent(this.settings.button_text),
/* 268 */ 			"&amp;buttonTextTopPadding=", encodeURIComponent(this.settings.button_text_top_padding),
/* 269 */ 			"&amp;buttonTextLeftPadding=", encodeURIComponent(this.settings.button_text_left_padding),
/* 270 */ 			"&amp;buttonTextStyle=", encodeURIComponent(this.settings.button_text_style),
/* 271 */ 			"&amp;buttonAction=", encodeURIComponent(this.settings.button_action),
/* 272 */ 			"&amp;buttonDisabled=", encodeURIComponent(this.settings.button_disabled),
/* 273 */ 			"&amp;buttonCursor=", encodeURIComponent(this.settings.button_cursor)
/* 274 */ 		].join("");
/* 275 */ };
/* 276 */ 
/* 277 */ // Public: getMovieElement retrieves the DOM reference to the Flash element added by SWFUpload
/* 278 */ // The element is cached after the first lookup
/* 279 */ SWFUpload.prototype.getMovieElement = function () {
/* 280 */ 	if (this.movieElement == undefined) {
/* 281 */ 		this.movieElement = document.getElementById(this.movieName);
/* 282 */ 	}
/* 283 */ 
/* 284 */ 	if (this.movieElement === null) {
/* 285 */ 		throw "Could not find Flash element";
/* 286 */ 	}
/* 287 */ 	
/* 288 */ 	return this.movieElement;
/* 289 */ };
/* 290 */ 
/* 291 */ // Private: buildParamString takes the name/value pairs in the post_params setting object
/* 292 */ // and joins them up in to a string formatted "name=value&amp;name=value"
/* 293 */ SWFUpload.prototype.buildParamString = function () {
/* 294 */ 	var postParams = this.settings.post_params; 
/* 295 */ 	var paramStringPairs = [];
/* 296 */ 
/* 297 */ 	if (typeof(postParams) === "object") {
/* 298 */ 		for (var name in postParams) {
/* 299 */ 			if (postParams.hasOwnProperty(name)) {
/* 300 */ 				paramStringPairs.push(encodeURIComponent(name.toString()) + "=" + encodeURIComponent(postParams[name].toString()));

/* swfupload.js */

/* 301 */ 			}
/* 302 */ 		}
/* 303 */ 	}
/* 304 */ 
/* 305 */ 	return paramStringPairs.join("&amp;");
/* 306 */ };
/* 307 */ 
/* 308 */ // Public: Used to remove a SWFUpload instance from the page. This method strives to remove
/* 309 */ // all references to the SWF, and other objects so memory is properly freed.
/* 310 */ // Returns true if everything was destroyed. Returns a false if a failure occurs leaving SWFUpload in an inconsistant state.
/* 311 */ // Credits: Major improvements provided by steffen
/* 312 */ SWFUpload.prototype.destroy = function () {
/* 313 */ 	try {
/* 314 */ 		// Make sure Flash is done before we try to remove it
/* 315 */ 		this.cancelUpload(null, false);
/* 316 */ 		
/* 317 */ 
/* 318 */ 		// Remove the SWFUpload DOM nodes
/* 319 */ 		var movieElement = null;
/* 320 */ 		movieElement = this.getMovieElement();
/* 321 */ 		
/* 322 */ 		if (movieElement && typeof(movieElement.CallFunction) === "unknown") { // We only want to do this in IE
/* 323 */ 			// Loop through all the movie's properties and remove all function references (DOM/JS IE 6/7 memory leak workaround)
/* 324 */ 			for (var i in movieElement) {
/* 325 */ 				try {
/* 326 */ 					if (typeof(movieElement[i]) === "function") {
/* 327 */ 						movieElement[i] = null;
/* 328 */ 					}
/* 329 */ 				} catch (ex1) {}
/* 330 */ 			}
/* 331 */ 
/* 332 */ 			// Remove the Movie Element from the page
/* 333 */ 			try {
/* 334 */ 				movieElement.parentNode.removeChild(movieElement);
/* 335 */ 			} catch (ex) {}
/* 336 */ 		}
/* 337 */ 		
/* 338 */ 		// Remove IE form fix reference
/* 339 */ 		window[this.movieName] = null;
/* 340 */ 
/* 341 */ 		// Destroy other references
/* 342 */ 		SWFUpload.instances[this.movieName] = null;
/* 343 */ 		delete SWFUpload.instances[this.movieName];
/* 344 */ 
/* 345 */ 		this.movieElement = null;
/* 346 */ 		this.settings = null;
/* 347 */ 		this.customSettings = null;
/* 348 */ 		this.eventQueue = null;
/* 349 */ 		this.movieName = null;
/* 350 */ 		

/* swfupload.js */

/* 351 */ 		
/* 352 */ 		return true;
/* 353 */ 	} catch (ex2) {
/* 354 */ 		return false;
/* 355 */ 	}
/* 356 */ };
/* 357 */ 
/* 358 */ 
/* 359 */ // Public: displayDebugInfo prints out settings and configuration
/* 360 */ // information about this SWFUpload instance.
/* 361 */ // This function (and any references to it) can be deleted when placing
/* 362 */ // SWFUpload in production.
/* 363 */ SWFUpload.prototype.displayDebugInfo = function () {
/* 364 */ 	this.debug(
/* 365 */ 		[
/* 366 */ 			"---SWFUpload Instance Info---\n",
/* 367 */ 			"Version: ", SWFUpload.version, "\n",
/* 368 */ 			"Movie Name: ", this.movieName, "\n",
/* 369 */ 			"Settings:\n",
/* 370 */ 			"\t", "upload_url:               ", this.settings.upload_url, "\n",
/* 371 */ 			"\t", "flash_url:                ", this.settings.flash_url, "\n",
/* 372 */ 			"\t", "use_query_string:         ", this.settings.use_query_string.toString(), "\n",
/* 373 */ 			"\t", "requeue_on_error:         ", this.settings.requeue_on_error.toString(), "\n",
/* 374 */ 			"\t", "http_success:             ", this.settings.http_success.join(", "), "\n",
/* 375 */ 			"\t", "assume_success_timeout:   ", this.settings.assume_success_timeout, "\n",
/* 376 */ 			"\t", "file_post_name:           ", this.settings.file_post_name, "\n",
/* 377 */ 			"\t", "post_params:              ", this.settings.post_params.toString(), "\n",
/* 378 */ 			"\t", "file_types:               ", this.settings.file_types, "\n",
/* 379 */ 			"\t", "file_types_description:   ", this.settings.file_types_description, "\n",
/* 380 */ 			"\t", "file_size_limit:          ", this.settings.file_size_limit, "\n",
/* 381 */ 			"\t", "file_upload_limit:        ", this.settings.file_upload_limit, "\n",
/* 382 */ 			"\t", "file_queue_limit:         ", this.settings.file_queue_limit, "\n",
/* 383 */ 			"\t", "debug:                    ", this.settings.debug.toString(), "\n",
/* 384 */ 
/* 385 */ 			"\t", "prevent_swf_caching:      ", this.settings.prevent_swf_caching.toString(), "\n",
/* 386 */ 
/* 387 */ 			"\t", "button_placeholder_id:    ", this.settings.button_placeholder_id.toString(), "\n",
/* 388 */ 			"\t", "button_placeholder:       ", (this.settings.button_placeholder ? "Set" : "Not Set"), "\n",
/* 389 */ 			"\t", "button_image_url:         ", this.settings.button_image_url.toString(), "\n",
/* 390 */ 			"\t", "button_width:             ", this.settings.button_width.toString(), "\n",
/* 391 */ 			"\t", "button_height:            ", this.settings.button_height.toString(), "\n",
/* 392 */ 			"\t", "button_text:              ", this.settings.button_text.toString(), "\n",
/* 393 */ 			"\t", "button_text_style:        ", this.settings.button_text_style.toString(), "\n",
/* 394 */ 			"\t", "button_text_top_padding:  ", this.settings.button_text_top_padding.toString(), "\n",
/* 395 */ 			"\t", "button_text_left_padding: ", this.settings.button_text_left_padding.toString(), "\n",
/* 396 */ 			"\t", "button_action:            ", this.settings.button_action.toString(), "\n",
/* 397 */ 			"\t", "button_disabled:          ", this.settings.button_disabled.toString(), "\n",
/* 398 */ 
/* 399 */ 			"\t", "custom_settings:          ", this.settings.custom_settings.toString(), "\n",
/* 400 */ 			"Event Handlers:\n",

/* swfupload.js */

/* 401 */ 			"\t", "swfupload_loaded_handler assigned:  ", (typeof this.settings.swfupload_loaded_handler === "function").toString(), "\n",
/* 402 */ 			"\t", "file_dialog_start_handler assigned: ", (typeof this.settings.file_dialog_start_handler === "function").toString(), "\n",
/* 403 */ 			"\t", "file_queued_handler assigned:       ", (typeof this.settings.file_queued_handler === "function").toString(), "\n",
/* 404 */ 			"\t", "file_queue_error_handler assigned:  ", (typeof this.settings.file_queue_error_handler === "function").toString(), "\n",
/* 405 */ 			"\t", "upload_start_handler assigned:      ", (typeof this.settings.upload_start_handler === "function").toString(), "\n",
/* 406 */ 			"\t", "upload_progress_handler assigned:   ", (typeof this.settings.upload_progress_handler === "function").toString(), "\n",
/* 407 */ 			"\t", "upload_error_handler assigned:      ", (typeof this.settings.upload_error_handler === "function").toString(), "\n",
/* 408 */ 			"\t", "upload_success_handler assigned:    ", (typeof this.settings.upload_success_handler === "function").toString(), "\n",
/* 409 */ 			"\t", "upload_complete_handler assigned:   ", (typeof this.settings.upload_complete_handler === "function").toString(), "\n",
/* 410 */ 			"\t", "debug_handler assigned:             ", (typeof this.settings.debug_handler === "function").toString(), "\n"
/* 411 */ 		].join("")
/* 412 */ 	);
/* 413 */ };
/* 414 */ 
/* 415 */ /* Note: addSetting and getSetting are no longer used by SWFUpload but are included
/* 416 *| 	the maintain v2 API compatibility
/* 417 *| */
/* 418 */ // Public: (Deprecated) addSetting adds a setting value. If the value given is undefined or null then the default_value is used.
/* 419 */ SWFUpload.prototype.addSetting = function (name, value, default_value) {
/* 420 */     if (value == undefined) {
/* 421 */         return (this.settings[name] = default_value);
/* 422 */     } else {
/* 423 */         return (this.settings[name] = value);
/* 424 */ 	}
/* 425 */ };
/* 426 */ 
/* 427 */ // Public: (Deprecated) getSetting gets a setting. Returns an empty string if the setting was not found.
/* 428 */ SWFUpload.prototype.getSetting = function (name) {
/* 429 */     if (this.settings[name] != undefined) {
/* 430 */         return this.settings[name];
/* 431 */ 	}
/* 432 */ 
/* 433 */     return "";
/* 434 */ };
/* 435 */ 
/* 436 */ 
/* 437 */ 
/* 438 */ // Private: callFlash handles function calls made to the Flash element.
/* 439 */ // Calls are made with a setTimeout for some functions to work around
/* 440 */ // bugs in the ExternalInterface library.
/* 441 */ SWFUpload.prototype.callFlash = function (functionName, argumentArray) {
/* 442 */ 	argumentArray = argumentArray || [];
/* 443 */ 	
/* 444 */ 	var movieElement = this.getMovieElement();
/* 445 */ 	var returnValue, returnString;
/* 446 */ 
/* 447 */ 	// Flash's method if calling ExternalInterface methods (code adapted from MooTools).
/* 448 */ 	try {
/* 449 */ 		returnString = movieElement.CallFunction('<invoke name="' + functionName + '" returntype="javascript">' + __flash__argumentsToXML(argumentArray, 0) + '</invoke>');
/* 450 */ 		returnValue = eval(returnString);

/* swfupload.js */

/* 451 */ 	} catch (ex) {
/* 452 */ 		throw "Call to " + functionName + " failed";
/* 453 */ 	}
/* 454 */ 	
/* 455 */ 	// Unescape file post param values
/* 456 */ 	if (returnValue != undefined && typeof returnValue.post === "object") {
/* 457 */ 		returnValue = this.unescapeFilePostParams(returnValue);
/* 458 */ 	}
/* 459 */ 
/* 460 */ 	return returnValue;
/* 461 */ };
/* 462 */ 
/* 463 */ /* *****************************
/* 464 *| 	-- Flash control methods --
/* 465 *| 	Your UI should use these
/* 466 *| 	to operate SWFUpload
/* 467 *|    ***************************** */
/* 468 */ 
/* 469 */ // WARNING: this function does not work in Flash Player 10
/* 470 */ // Public: selectFile causes a File Selection Dialog window to appear.  This
/* 471 */ // dialog only allows 1 file to be selected.
/* 472 */ SWFUpload.prototype.selectFile = function () {
/* 473 */ 	this.callFlash("SelectFile");
/* 474 */ };
/* 475 */ 
/* 476 */ // WARNING: this function does not work in Flash Player 10
/* 477 */ // Public: selectFiles causes a File Selection Dialog window to appear/ This
/* 478 */ // dialog allows the user to select any number of files
/* 479 */ // Flash Bug Warning: Flash limits the number of selectable files based on the combined length of the file names.
/* 480 */ // If the selection name length is too long the dialog will fail in an unpredictable manner.  There is no work-around
/* 481 */ // for this bug.
/* 482 */ SWFUpload.prototype.selectFiles = function () {
/* 483 */ 	this.callFlash("SelectFiles");
/* 484 */ };
/* 485 */ 
/* 486 */ 
/* 487 */ // Public: startUpload starts uploading the first file in the queue unless
/* 488 */ // the optional parameter 'fileID' specifies the ID 
/* 489 */ SWFUpload.prototype.startUpload = function (fileID) {
/* 490 */ 	this.callFlash("StartUpload", [fileID]);
/* 491 */ };
/* 492 */ 
/* 493 */ // Public: cancelUpload cancels any queued file.  The fileID parameter may be the file ID or index.
/* 494 */ // If you do not specify a fileID the current uploading file or first file in the queue is cancelled.
/* 495 */ // If you do not want the uploadError event to trigger you can specify false for the triggerErrorEvent parameter.
/* 496 */ SWFUpload.prototype.cancelUpload = function (fileID, triggerErrorEvent) {
/* 497 */ 	if (triggerErrorEvent !== false) {
/* 498 */ 		triggerErrorEvent = true;
/* 499 */ 	}
/* 500 */ 	this.callFlash("CancelUpload", [fileID, triggerErrorEvent]);

/* swfupload.js */

/* 501 */ };
/* 502 */ 
/* 503 */ // Public: stopUpload stops the current upload and requeues the file at the beginning of the queue.
/* 504 */ // If nothing is currently uploading then nothing happens.
/* 505 */ SWFUpload.prototype.stopUpload = function () {
/* 506 */ 	this.callFlash("StopUpload");
/* 507 */ };
/* 508 */ 
/* 509 */ /* ************************
/* 510 *|  * Settings methods
/* 511 *|  *   These methods change the SWFUpload settings.
/* 512 *|  *   SWFUpload settings should not be changed directly on the settings object
/* 513 *|  *   since many of the settings need to be passed to Flash in order to take
/* 514 *|  *   effect.
/* 515 *|  * *********************** */
/* 516 */ 
/* 517 */ // Public: getStats gets the file statistics object.
/* 518 */ SWFUpload.prototype.getStats = function () {
/* 519 */ 	return this.callFlash("GetStats");
/* 520 */ };
/* 521 */ 
/* 522 */ // Public: setStats changes the SWFUpload statistics.  You shouldn't need to 
/* 523 */ // change the statistics but you can.  Changing the statistics does not
/* 524 */ // affect SWFUpload accept for the successful_uploads count which is used
/* 525 */ // by the upload_limit setting to determine how many files the user may upload.
/* 526 */ SWFUpload.prototype.setStats = function (statsObject) {
/* 527 */ 	this.callFlash("SetStats", [statsObject]);
/* 528 */ };
/* 529 */ 
/* 530 */ // Public: getFile retrieves a File object by ID or Index.  If the file is
/* 531 */ // not found then 'null' is returned.
/* 532 */ SWFUpload.prototype.getFile = function (fileID) {
/* 533 */ 	if (typeof(fileID) === "number") {
/* 534 */ 		return this.callFlash("GetFileByIndex", [fileID]);
/* 535 */ 	} else {
/* 536 */ 		return this.callFlash("GetFile", [fileID]);
/* 537 */ 	}
/* 538 */ };
/* 539 */ 
/* 540 */ // Public: addFileParam sets a name/value pair that will be posted with the
/* 541 */ // file specified by the Files ID.  If the name already exists then the
/* 542 */ // exiting value will be overwritten.
/* 543 */ SWFUpload.prototype.addFileParam = function (fileID, name, value) {
/* 544 */ 	return this.callFlash("AddFileParam", [fileID, name, value]);
/* 545 */ };
/* 546 */ 
/* 547 */ // Public: removeFileParam removes a previously set (by addFileParam) name/value
/* 548 */ // pair from the specified file.
/* 549 */ SWFUpload.prototype.removeFileParam = function (fileID, name) {
/* 550 */ 	this.callFlash("RemoveFileParam", [fileID, name]);

/* swfupload.js */

/* 551 */ };
/* 552 */ 
/* 553 */ // Public: setUploadUrl changes the upload_url setting.
/* 554 */ SWFUpload.prototype.setUploadURL = function (url) {
/* 555 */ 	this.settings.upload_url = url.toString();
/* 556 */ 	this.callFlash("SetUploadURL", [url]);
/* 557 */ };
/* 558 */ 
/* 559 */ // Public: setPostParams changes the post_params setting
/* 560 */ SWFUpload.prototype.setPostParams = function (paramsObject) {
/* 561 */ 	this.settings.post_params = paramsObject;
/* 562 */ 	this.callFlash("SetPostParams", [paramsObject]);
/* 563 */ };
/* 564 */ 
/* 565 */ // Public: addPostParam adds post name/value pair.  Each name can have only one value.
/* 566 */ SWFUpload.prototype.addPostParam = function (name, value) {
/* 567 */ 	this.settings.post_params[name] = value;
/* 568 */ 	this.callFlash("SetPostParams", [this.settings.post_params]);
/* 569 */ };
/* 570 */ 
/* 571 */ // Public: removePostParam deletes post name/value pair.
/* 572 */ SWFUpload.prototype.removePostParam = function (name) {
/* 573 */ 	delete this.settings.post_params[name];
/* 574 */ 	this.callFlash("SetPostParams", [this.settings.post_params]);
/* 575 */ };
/* 576 */ 
/* 577 */ // Public: setFileTypes changes the file_types setting and the file_types_description setting
/* 578 */ SWFUpload.prototype.setFileTypes = function (types, description) {
/* 579 */ 	this.settings.file_types = types;
/* 580 */ 	this.settings.file_types_description = description;
/* 581 */ 	this.callFlash("SetFileTypes", [types, description]);
/* 582 */ };
/* 583 */ 
/* 584 */ // Public: setFileSizeLimit changes the file_size_limit setting
/* 585 */ SWFUpload.prototype.setFileSizeLimit = function (fileSizeLimit) {
/* 586 */ 	this.settings.file_size_limit = fileSizeLimit;
/* 587 */ 	this.callFlash("SetFileSizeLimit", [fileSizeLimit]);
/* 588 */ };
/* 589 */ 
/* 590 */ // Public: setFileUploadLimit changes the file_upload_limit setting
/* 591 */ SWFUpload.prototype.setFileUploadLimit = function (fileUploadLimit) {
/* 592 */ 	this.settings.file_upload_limit = fileUploadLimit;
/* 593 */ 	this.callFlash("SetFileUploadLimit", [fileUploadLimit]);
/* 594 */ };
/* 595 */ 
/* 596 */ // Public: setFileQueueLimit changes the file_queue_limit setting
/* 597 */ SWFUpload.prototype.setFileQueueLimit = function (fileQueueLimit) {
/* 598 */ 	this.settings.file_queue_limit = fileQueueLimit;
/* 599 */ 	this.callFlash("SetFileQueueLimit", [fileQueueLimit]);
/* 600 */ };

/* swfupload.js */

/* 601 */ 
/* 602 */ // Public: setFilePostName changes the file_post_name setting
/* 603 */ SWFUpload.prototype.setFilePostName = function (filePostName) {
/* 604 */ 	this.settings.file_post_name = filePostName;
/* 605 */ 	this.callFlash("SetFilePostName", [filePostName]);
/* 606 */ };
/* 607 */ 
/* 608 */ // Public: setUseQueryString changes the use_query_string setting
/* 609 */ SWFUpload.prototype.setUseQueryString = function (useQueryString) {
/* 610 */ 	this.settings.use_query_string = useQueryString;
/* 611 */ 	this.callFlash("SetUseQueryString", [useQueryString]);
/* 612 */ };
/* 613 */ 
/* 614 */ // Public: setRequeueOnError changes the requeue_on_error setting
/* 615 */ SWFUpload.prototype.setRequeueOnError = function (requeueOnError) {
/* 616 */ 	this.settings.requeue_on_error = requeueOnError;
/* 617 */ 	this.callFlash("SetRequeueOnError", [requeueOnError]);
/* 618 */ };
/* 619 */ 
/* 620 */ // Public: setHTTPSuccess changes the http_success setting
/* 621 */ SWFUpload.prototype.setHTTPSuccess = function (http_status_codes) {
/* 622 */ 	if (typeof http_status_codes === "string") {
/* 623 */ 		http_status_codes = http_status_codes.replace(" ", "").split(",");
/* 624 */ 	}
/* 625 */ 	
/* 626 */ 	this.settings.http_success = http_status_codes;
/* 627 */ 	this.callFlash("SetHTTPSuccess", [http_status_codes]);
/* 628 */ };
/* 629 */ 
/* 630 */ // Public: setHTTPSuccess changes the http_success setting
/* 631 */ SWFUpload.prototype.setAssumeSuccessTimeout = function (timeout_seconds) {
/* 632 */ 	this.settings.assume_success_timeout = timeout_seconds;
/* 633 */ 	this.callFlash("SetAssumeSuccessTimeout", [timeout_seconds]);
/* 634 */ };
/* 635 */ 
/* 636 */ // Public: setDebugEnabled changes the debug_enabled setting
/* 637 */ SWFUpload.prototype.setDebugEnabled = function (debugEnabled) {
/* 638 */ 	this.settings.debug_enabled = debugEnabled;
/* 639 */ 	this.callFlash("SetDebugEnabled", [debugEnabled]);
/* 640 */ };
/* 641 */ 
/* 642 */ // Public: setButtonImageURL loads a button image sprite
/* 643 */ SWFUpload.prototype.setButtonImageURL = function (buttonImageURL) {
/* 644 */ 	if (buttonImageURL == undefined) {
/* 645 */ 		buttonImageURL = "";
/* 646 */ 	}
/* 647 */ 	
/* 648 */ 	this.settings.button_image_url = buttonImageURL;
/* 649 */ 	this.callFlash("SetButtonImageURL", [buttonImageURL]);
/* 650 */ };

/* swfupload.js */

/* 651 */ 
/* 652 */ // Public: setButtonDimensions resizes the Flash Movie and button
/* 653 */ SWFUpload.prototype.setButtonDimensions = function (width, height) {
/* 654 */ 	this.settings.button_width = width;
/* 655 */ 	this.settings.button_height = height;
/* 656 */ 	
/* 657 */ 	var movie = this.getMovieElement();
/* 658 */ 	if (movie != undefined) {
/* 659 */ 		movie.style.width = width + "px";
/* 660 */ 		movie.style.height = height + "px";
/* 661 */ 	}
/* 662 */ 	
/* 663 */ 	this.callFlash("SetButtonDimensions", [width, height]);
/* 664 */ };
/* 665 */ // Public: setButtonText Changes the text overlaid on the button
/* 666 */ SWFUpload.prototype.setButtonText = function (html) {
/* 667 */ 	this.settings.button_text = html;
/* 668 */ 	this.callFlash("SetButtonText", [html]);
/* 669 */ };
/* 670 */ // Public: setButtonTextPadding changes the top and left padding of the text overlay
/* 671 */ SWFUpload.prototype.setButtonTextPadding = function (left, top) {
/* 672 */ 	this.settings.button_text_top_padding = top;
/* 673 */ 	this.settings.button_text_left_padding = left;
/* 674 */ 	this.callFlash("SetButtonTextPadding", [left, top]);
/* 675 */ };
/* 676 */ 
/* 677 */ // Public: setButtonTextStyle changes the CSS used to style the HTML/Text overlaid on the button
/* 678 */ SWFUpload.prototype.setButtonTextStyle = function (css) {
/* 679 */ 	this.settings.button_text_style = css;
/* 680 */ 	this.callFlash("SetButtonTextStyle", [css]);
/* 681 */ };
/* 682 */ // Public: setButtonDisabled disables/enables the button
/* 683 */ SWFUpload.prototype.setButtonDisabled = function (isDisabled) {
/* 684 */ 	this.settings.button_disabled = isDisabled;
/* 685 */ 	this.callFlash("SetButtonDisabled", [isDisabled]);
/* 686 */ };
/* 687 */ // Public: setButtonAction sets the action that occurs when the button is clicked
/* 688 */ SWFUpload.prototype.setButtonAction = function (buttonAction) {
/* 689 */ 	this.settings.button_action = buttonAction;
/* 690 */ 	this.callFlash("SetButtonAction", [buttonAction]);
/* 691 */ };
/* 692 */ 
/* 693 */ // Public: setButtonCursor changes the mouse cursor displayed when hovering over the button
/* 694 */ SWFUpload.prototype.setButtonCursor = function (cursor) {
/* 695 */ 	this.settings.button_cursor = cursor;
/* 696 */ 	this.callFlash("SetButtonCursor", [cursor]);
/* 697 */ };
/* 698 */ 
/* 699 */ /* *******************************
/* 700 *| 	Flash Event Interfaces

/* swfupload.js */

/* 701 *| 	These functions are used by Flash to trigger the various
/* 702 *| 	events.
/* 703 *| 	
/* 704 *| 	All these functions a Private.
/* 705 *| 	
/* 706 *| 	Because the ExternalInterface library is buggy the event calls
/* 707 *| 	are added to a queue and the queue then executed by a setTimeout.
/* 708 *| 	This ensures that events are executed in a determinate order and that
/* 709 *| 	the ExternalInterface bugs are avoided.
/* 710 *| ******************************* */
/* 711 */ 
/* 712 */ SWFUpload.prototype.queueEvent = function (handlerName, argumentArray) {
/* 713 */ 	// Warning: Don't call this.debug inside here or you'll create an infinite loop
/* 714 */ 	
/* 715 */ 	if (argumentArray == undefined) {
/* 716 */ 		argumentArray = [];
/* 717 */ 	} else if (!(argumentArray instanceof Array)) {
/* 718 */ 		argumentArray = [argumentArray];
/* 719 */ 	}
/* 720 */ 	
/* 721 */ 	var self = this;
/* 722 */ 	if (typeof this.settings[handlerName] === "function") {
/* 723 */ 		// Queue the event
/* 724 */ 		this.eventQueue.push(function () {
/* 725 */ 			this.settings[handlerName].apply(this, argumentArray);
/* 726 */ 		});
/* 727 */ 		
/* 728 */ 		// Execute the next queued event
/* 729 */ 		setTimeout(function () {
/* 730 */ 			self.executeNextEvent();
/* 731 */ 		}, 0);
/* 732 */ 		
/* 733 */ 	} else if (this.settings[handlerName] !== null) {
/* 734 */ 		throw "Event handler " + handlerName + " is unknown or is not a function";
/* 735 */ 	}
/* 736 */ };
/* 737 */ 
/* 738 */ // Private: Causes the next event in the queue to be executed.  Since events are queued using a setTimeout
/* 739 */ // we must queue them in order to garentee that they are executed in order.
/* 740 */ SWFUpload.prototype.executeNextEvent = function () {
/* 741 */ 	// Warning: Don't call this.debug inside here or you'll create an infinite loop
/* 742 */ 
/* 743 */ 	var  f = this.eventQueue ? this.eventQueue.shift() : null;
/* 744 */ 	if (typeof(f) === "function") {
/* 745 */ 		f.apply(this);
/* 746 */ 	}
/* 747 */ };
/* 748 */ 
/* 749 */ // Private: unescapeFileParams is part of a workaround for a flash bug where objects passed through ExternalInterface cannot have
/* 750 */ // properties that contain characters that are not valid for JavaScript identifiers. To work around this

/* swfupload.js */

/* 751 */ // the Flash Component escapes the parameter names and we must unescape again before passing them along.
/* 752 */ SWFUpload.prototype.unescapeFilePostParams = function (file) {
/* 753 */ 	var reg = /[$]([0-9a-f]{4})/i;
/* 754 */ 	var unescapedPost = {};
/* 755 */ 	var uk;
/* 756 */ 
/* 757 */ 	if (file != undefined) {
/* 758 */ 		for (var k in file.post) {
/* 759 */ 			if (file.post.hasOwnProperty(k)) {
/* 760 */ 				uk = k;
/* 761 */ 				var match;
/* 762 */ 				while ((match = reg.exec(uk)) !== null) {
/* 763 */ 					uk = uk.replace(match[0], String.fromCharCode(parseInt("0x" + match[1], 16)));
/* 764 */ 				}
/* 765 */ 				unescapedPost[uk] = file.post[k];
/* 766 */ 			}
/* 767 */ 		}
/* 768 */ 
/* 769 */ 		file.post = unescapedPost;
/* 770 */ 	}
/* 771 */ 
/* 772 */ 	return file;
/* 773 */ };
/* 774 */ 
/* 775 */ // Private: Called by Flash to see if JS can call in to Flash (test if External Interface is working)
/* 776 */ SWFUpload.prototype.testExternalInterface = function () {
/* 777 */ 	try {
/* 778 */ 		return this.callFlash("TestExternalInterface");
/* 779 */ 	} catch (ex) {
/* 780 */ 		return false;
/* 781 */ 	}
/* 782 */ };
/* 783 */ 
/* 784 */ // Private: This event is called by Flash when it has finished loading. Don't modify this.
/* 785 */ // Use the swfupload_loaded_handler event setting to execute custom code when SWFUpload has loaded.
/* 786 */ SWFUpload.prototype.flashReady = function () {
/* 787 */ 	// Check that the movie element is loaded correctly with its ExternalInterface methods defined
/* 788 */ 	var movieElement = this.getMovieElement();
/* 789 */ 
/* 790 */ 	if (!movieElement) {
/* 791 */ 		this.debug("Flash called back ready but the flash movie can't be found.");
/* 792 */ 		return;
/* 793 */ 	}
/* 794 */ 
/* 795 */ 	this.cleanUp(movieElement);
/* 796 */ 	
/* 797 */ 	this.queueEvent("swfupload_loaded_handler");
/* 798 */ };
/* 799 */ 
/* 800 */ // Private: removes Flash added fuctions to the DOM node to prevent memory leaks in IE.

/* swfupload.js */

/* 801 */ // This function is called by Flash each time the ExternalInterface functions are created.
/* 802 */ SWFUpload.prototype.cleanUp = function (movieElement) {
/* 803 */ 	// Pro-actively unhook all the Flash functions
/* 804 */ 	try {
/* 805 */ 		if (this.movieElement && typeof(movieElement.CallFunction) === "unknown") { // We only want to do this in IE
/* 806 */ 			this.debug("Removing Flash functions hooks (this should only run in IE and should prevent memory leaks)");
/* 807 */ 			for (var key in movieElement) {
/* 808 */ 				try {
/* 809 */ 					if (typeof(movieElement[key]) === "function") {
/* 810 */ 						movieElement[key] = null;
/* 811 */ 					}
/* 812 */ 				} catch (ex) {
/* 813 */ 				}
/* 814 */ 			}
/* 815 */ 		}
/* 816 */ 	} catch (ex1) {
/* 817 */ 	
/* 818 */ 	}
/* 819 */ 
/* 820 */ 	// Fix Flashes own cleanup code so if the SWFMovie was removed from the page
/* 821 */ 	// it doesn't display errors.
/* 822 */ 	window["__flash__removeCallback"] = function (instance, name) {
/* 823 */ 		try {
/* 824 */ 			if (instance) {
/* 825 */ 				instance[name] = null;
/* 826 */ 			}
/* 827 */ 		} catch (flashEx) {
/* 828 */ 		
/* 829 */ 		}
/* 830 */ 	};
/* 831 */ 
/* 832 */ };
/* 833 */ 
/* 834 */ 
/* 835 */ /* This is a chance to do something before the browse window opens */
/* 836 */ SWFUpload.prototype.fileDialogStart = function () {
/* 837 */ 	this.queueEvent("file_dialog_start_handler");
/* 838 */ };
/* 839 */ 
/* 840 */ 
/* 841 */ /* Called when a file is successfully added to the queue. */
/* 842 */ SWFUpload.prototype.fileQueued = function (file) {
/* 843 */ 	file = this.unescapeFilePostParams(file);
/* 844 */ 	this.queueEvent("file_queued_handler", file);
/* 845 */ };
/* 846 */ 
/* 847 */ 
/* 848 */ /* Handle errors that occur when an attempt to queue a file fails. */
/* 849 */ SWFUpload.prototype.fileQueueError = function (file, errorCode, message) {
/* 850 */ 	file = this.unescapeFilePostParams(file);

/* swfupload.js */

/* 851 */ 	this.queueEvent("file_queue_error_handler", [file, errorCode, message]);
/* 852 */ };
/* 853 */ 
/* 854 */ /* Called after the file dialog has closed and the selected files have been queued.
/* 855 *| 	You could call startUpload here if you want the queued files to begin uploading immediately. */
/* 856 */ SWFUpload.prototype.fileDialogComplete = function (numFilesSelected, numFilesQueued, numFilesInQueue) {
/* 857 */ 	this.queueEvent("file_dialog_complete_handler", [numFilesSelected, numFilesQueued, numFilesInQueue]);
/* 858 */ };
/* 859 */ 
/* 860 */ SWFUpload.prototype.uploadStart = function (file) {
/* 861 */ 	file = this.unescapeFilePostParams(file);
/* 862 */ 	this.queueEvent("return_upload_start_handler", file);
/* 863 */ };
/* 864 */ 
/* 865 */ SWFUpload.prototype.returnUploadStart = function (file) {
/* 866 */ 	var returnValue;
/* 867 */ 	if (typeof this.settings.upload_start_handler === "function") {
/* 868 */ 		file = this.unescapeFilePostParams(file);
/* 869 */ 		returnValue = this.settings.upload_start_handler.call(this, file);
/* 870 */ 	} else if (this.settings.upload_start_handler != undefined) {
/* 871 */ 		throw "upload_start_handler must be a function";
/* 872 */ 	}
/* 873 */ 
/* 874 */ 	// Convert undefined to true so if nothing is returned from the upload_start_handler it is
/* 875 */ 	// interpretted as 'true'.
/* 876 */ 	if (returnValue === undefined) {
/* 877 */ 		returnValue = true;
/* 878 */ 	}
/* 879 */ 	
/* 880 */ 	returnValue = !!returnValue;
/* 881 */ 	
/* 882 */ 	this.callFlash("ReturnUploadStart", [returnValue]);
/* 883 */ };
/* 884 */ 
/* 885 */ 
/* 886 */ 
/* 887 */ SWFUpload.prototype.uploadProgress = function (file, bytesComplete, bytesTotal) {
/* 888 */ 	file = this.unescapeFilePostParams(file);
/* 889 */ 	this.queueEvent("upload_progress_handler", [file, bytesComplete, bytesTotal]);
/* 890 */ };
/* 891 */ 
/* 892 */ SWFUpload.prototype.uploadError = function (file, errorCode, message) {
/* 893 */ 	file = this.unescapeFilePostParams(file);
/* 894 */ 	this.queueEvent("upload_error_handler", [file, errorCode, message]);
/* 895 */ };
/* 896 */ 
/* 897 */ SWFUpload.prototype.uploadSuccess = function (file, serverData, responseReceived) {
/* 898 */ 	file = this.unescapeFilePostParams(file);
/* 899 */ 	this.queueEvent("upload_success_handler", [file, serverData, responseReceived]);
/* 900 */ };

/* swfupload.js */

/* 901 */ 
/* 902 */ SWFUpload.prototype.uploadComplete = function (file) {
/* 903 */ 	file = this.unescapeFilePostParams(file);
/* 904 */ 	this.queueEvent("upload_complete_handler", file);
/* 905 */ };
/* 906 */ 
/* 907 */ /* Called by SWFUpload JavaScript and Flash functions when debug is enabled. By default it writes messages to the
/* 908 *|    internal debug console.  You can override this event and have messages written where you want. */
/* 909 */ SWFUpload.prototype.debug = function (message) {
/* 910 */ 	this.queueEvent("debug_handler", message);
/* 911 */ };
/* 912 */ 
/* 913 */ 
/* 914 */ /* **********************************
/* 915 *| 	Debug Console
/* 916 *| 	The debug console is a self contained, in page location
/* 917 *| 	for debug message to be sent.  The Debug Console adds
/* 918 *| 	itself to the body if necessary.
/* 919 *| 
/* 920 *| 	The console is automatically scrolled as messages appear.
/* 921 *| 	
/* 922 *| 	If you are using your own debug handler or when you deploy to production and
/* 923 *| 	have debug disabled you can remove these functions to reduce the file size
/* 924 *| 	and complexity.
/* 925 *| ********************************** */
/* 926 */    
/* 927 */ // Private: debugMessage is the default debug_handler.  If you want to print debug messages
/* 928 */ // call the debug() function.  When overriding the function your own function should
/* 929 */ // check to see if the debug setting is true before outputting debug information.
/* 930 */ SWFUpload.prototype.debugMessage = function (message) {
/* 931 */ 	if (this.settings.debug) {
/* 932 */ 		var exceptionMessage, exceptionValues = [];
/* 933 */ 
/* 934 */ 		// Check for an exception object and print it nicely
/* 935 */ 		if (typeof message === "object" && typeof message.name === "string" && typeof message.message === "string") {
/* 936 */ 			for (var key in message) {
/* 937 */ 				if (message.hasOwnProperty(key)) {
/* 938 */ 					exceptionValues.push(key + ": " + message[key]);
/* 939 */ 				}
/* 940 */ 			}
/* 941 */ 			exceptionMessage = exceptionValues.join("\n") || "";
/* 942 */ 			exceptionValues = exceptionMessage.split("\n");
/* 943 */ 			exceptionMessage = "EXCEPTION: " + exceptionValues.join("\nEXCEPTION: ");
/* 944 */ 			SWFUpload.Console.writeLine(exceptionMessage);
/* 945 */ 		} else {
/* 946 */ 			SWFUpload.Console.writeLine(message);
/* 947 */ 		}
/* 948 */ 	}
/* 949 */ };
/* 950 */ 

/* swfupload.js */

/* 951 */ SWFUpload.Console = {};
/* 952 */ SWFUpload.Console.writeLine = function (message) {
/* 953 */ 	var console, documentForm;
/* 954 */ 
/* 955 */ 	try {
/* 956 */ 		console = document.getElementById("SWFUpload_Console");
/* 957 */ 
/* 958 */ 		if (!console) {
/* 959 */ 			documentForm = document.createElement("form");
/* 960 */ 			document.getElementsByTagName("body")[0].appendChild(documentForm);
/* 961 */ 
/* 962 */ 			console = document.createElement("textarea");
/* 963 */ 			console.id = "SWFUpload_Console";
/* 964 */ 			console.style.fontFamily = "monospace";
/* 965 */ 			console.setAttribute("wrap", "off");
/* 966 */ 			console.wrap = "off";
/* 967 */ 			console.style.overflow = "auto";
/* 968 */ 			console.style.width = "700px";
/* 969 */ 			console.style.height = "350px";
/* 970 */ 			console.style.margin = "5px";
/* 971 */ 			documentForm.appendChild(console);
/* 972 */ 		}
/* 973 */ 
/* 974 */ 		console.value += message + "\n";
/* 975 */ 
/* 976 */ 		console.scrollTop = console.scrollHeight - console.clientHeight;
/* 977 */ 	} catch (ex) {
/* 978 */ 		alert("Exception: " + ex.name + " Message: " + ex.message);
/* 979 */ 	}
/* 980 */ };
/* 981 */ 

;
/* swfupload.queue.js */

/* 1  */ /*
/* 2  *| 	Queue Plug-in
/* 3  *| 	
/* 4  *| 	Features:
/* 5  *| 		*Adds a cancelQueue() method for cancelling the entire queue.
/* 6  *| 		*All queued files are uploaded when startUpload() is called.
/* 7  *| 		*If false is returned from uploadComplete then the queue upload is stopped.
/* 8  *| 		 If false is not returned (strict comparison) then the queue upload is continued.
/* 9  *| 		*Adds a QueueComplete event that is fired when all the queued files have finished uploading.
/* 10 *| 		 Set the event handler with the queue_complete_handler setting.
/* 11 *| 		
/* 12 *| 	*/
/* 13 */ 
/* 14 */ var SWFUpload;
/* 15 */ if (typeof(SWFUpload) === "function") {
/* 16 */ 	SWFUpload.queue = {};
/* 17 */ 	
/* 18 */ 	SWFUpload.prototype.initSettings = (function (oldInitSettings) {
/* 19 */ 		return function () {
/* 20 */ 			if (typeof(oldInitSettings) === "function") {
/* 21 */ 				oldInitSettings.call(this);
/* 22 */ 			}
/* 23 */ 			
/* 24 */ 			this.queueSettings = {};
/* 25 */ 			
/* 26 */ 			this.queueSettings.queue_cancelled_flag = false;
/* 27 */ 			this.queueSettings.queue_upload_count = 0;
/* 28 */ 			
/* 29 */ 			this.queueSettings.user_upload_complete_handler = this.settings.upload_complete_handler;
/* 30 */ 			this.queueSettings.user_upload_start_handler = this.settings.upload_start_handler;
/* 31 */ 			this.settings.upload_complete_handler = SWFUpload.queue.uploadCompleteHandler;
/* 32 */ 			this.settings.upload_start_handler = SWFUpload.queue.uploadStartHandler;
/* 33 */ 			
/* 34 */ 			this.settings.queue_complete_handler = this.settings.queue_complete_handler || null;
/* 35 */ 		};
/* 36 */ 	})(SWFUpload.prototype.initSettings);
/* 37 */ 
/* 38 */ 	SWFUpload.prototype.startUpload = function (fileID) {
/* 39 */ 		this.queueSettings.queue_cancelled_flag = false;
/* 40 */ 		this.callFlash("StartUpload", [fileID]);
/* 41 */ 	};
/* 42 */ 
/* 43 */ 	SWFUpload.prototype.cancelQueue = function () {
/* 44 */ 		this.queueSettings.queue_cancelled_flag = true;
/* 45 */ 		this.stopUpload();
/* 46 */ 		
/* 47 */ 		var stats = this.getStats();
/* 48 */ 		while (stats.files_queued > 0) {
/* 49 */ 			this.cancelUpload();
/* 50 */ 			stats = this.getStats();

/* swfupload.queue.js */

/* 51 */ 		}
/* 52 */ 	};
/* 53 */ 	
/* 54 */ 	SWFUpload.queue.uploadStartHandler = function (file) {
/* 55 */ 		var returnValue;
/* 56 */ 		if (typeof(this.queueSettings.user_upload_start_handler) === "function") {
/* 57 */ 			returnValue = this.queueSettings.user_upload_start_handler.call(this, file);
/* 58 */ 		}
/* 59 */ 		
/* 60 */ 		// To prevent upload a real "FALSE" value must be returned, otherwise default to a real "TRUE" value.
/* 61 */ 		returnValue = (returnValue === false) ? false : true;
/* 62 */ 		
/* 63 */ 		this.queueSettings.queue_cancelled_flag = !returnValue;
/* 64 */ 
/* 65 */ 		return returnValue;
/* 66 */ 	};
/* 67 */ 	
/* 68 */ 	SWFUpload.queue.uploadCompleteHandler = function (file) {
/* 69 */ 		var user_upload_complete_handler = this.queueSettings.user_upload_complete_handler;
/* 70 */ 		var continueUpload;
/* 71 */ 		
/* 72 */ 		if (file.filestatus === SWFUpload.FILE_STATUS.COMPLETE) {
/* 73 */ 			this.queueSettings.queue_upload_count++;
/* 74 */ 		}
/* 75 */ 
/* 76 */ 		if (typeof(user_upload_complete_handler) === "function") {
/* 77 */ 			continueUpload = (user_upload_complete_handler.call(this, file) === false) ? false : true;
/* 78 */ 		} else if (file.filestatus === SWFUpload.FILE_STATUS.QUEUED) {
/* 79 */ 			// If the file was stopped and re-queued don't restart the upload
/* 80 */ 			continueUpload = false;
/* 81 */ 		} else {
/* 82 */ 			continueUpload = true;
/* 83 */ 		}
/* 84 */ 		
/* 85 */ 		if (continueUpload) {
/* 86 */ 			var stats = this.getStats();
/* 87 */ 			if (stats.files_queued > 0 && this.queueSettings.queue_cancelled_flag === false) {
/* 88 */ 				this.startUpload();
/* 89 */ 			} else if (this.queueSettings.queue_cancelled_flag === false) {
/* 90 */ 				this.queueEvent("queue_complete_handler", [this.queueSettings.queue_upload_count]);
/* 91 */ 				this.queueSettings.queue_upload_count = 0;
/* 92 */ 			} else {
/* 93 */ 				this.queueSettings.queue_cancelled_flag = false;
/* 94 */ 				this.queueSettings.queue_upload_count = 0;
/* 95 */ 			}
/* 96 */ 		}
/* 97 */ 	};
/* 98 */ }
/* 99 */ 

;
/* swfobject.js */

/* 1 */ /*	SWFObject v2.2 <http://code.google.com/p/swfobject/> 
/* 2 *| 	is released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
/* 3 *| */
/* 4 */ var swfobject=function(){var D="undefined",r="object",S="Shockwave Flash",W="ShockwaveFlash.ShockwaveFlash",q="application/x-shockwave-flash",R="SWFObjectExprInst",x="onreadystatechange",O=window,j=document,t=navigator,T=false,U=[h],o=[],N=[],I=[],l,Q,E,B,J=false,a=false,n,G,m=true,M=function(){var aa=typeof j.getElementById!=D&&typeof j.getElementsByTagName!=D&&typeof j.createElement!=D,ah=t.userAgent.toLowerCase(),Y=t.platform.toLowerCase(),ae=Y?/win/.test(Y):/win/.test(ah),ac=Y?/mac/.test(Y):/mac/.test(ah),af=/webkit/.test(ah)?parseFloat(ah.replace(/^.*webkit\/(\d+(\.\d+)?).*$/,"$1")):false,X=!+"\v1",ag=[0,0,0],ab=null;if(typeof t.plugins!=D&&typeof t.plugins[S]==r){ab=t.plugins[S].description;if(ab&&!(typeof t.mimeTypes!=D&&t.mimeTypes[q]&&!t.mimeTypes[q].enabledPlugin)){T=true;X=false;ab=ab.replace(/^.*\s+(\S+\s+\S+$)/,"$1");ag[0]=parseInt(ab.replace(/^(.*)\..*$/,"$1"),10);ag[1]=parseInt(ab.replace(/^.*\.(.*)\s.*$/,"$1"),10);ag[2]=/[a-zA-Z]/.test(ab)?parseInt(ab.replace(/^.*[a-zA-Z]+(.*)$/,"$1"),10):0}}else{if(typeof O.ActiveXObject!=D){try{var ad=new ActiveXObject(W);if(ad){ab=ad.GetVariable("$version");if(ab){X=true;ab=ab.split(" ")[1].split(",");ag=[parseInt(ab[0],10),parseInt(ab[1],10),parseInt(ab[2],10)]}}}catch(Z){}}}return{w3:aa,pv:ag,wk:af,ie:X,win:ae,mac:ac}}(),k=function(){if(!M.w3){return}if((typeof j.readyState!=D&&j.readyState=="complete")||(typeof j.readyState==D&&(j.getElementsByTagName("body")[0]||j.body))){f()}if(!J){if(typeof j.addEventListener!=D){j.addEventListener("DOMContentLoaded",f,false)}if(M.ie&&M.win){j.attachEvent(x,function(){if(j.readyState=="complete"){j.detachEvent(x,arguments.callee);f()}});if(O==top){(function(){if(J){return}try{j.documentElement.doScroll("left")}catch(X){setTimeout(arguments.callee,0);return}f()})()}}if(M.wk){(function(){if(J){return}if(!/loaded|complete/.test(j.readyState)){setTimeout(arguments.callee,0);return}f()})()}s(f)}}();function f(){if(J){return}try{var Z=j.getElementsByTagName("body")[0].appendChild(C("span"));Z.parentNode.removeChild(Z)}catch(aa){return}J=true;var X=U.length;for(var Y=0;Y<X;Y++){U[Y]()}}function K(X){if(J){X()}else{U[U.length]=X}}function s(Y){if(typeof O.addEventListener!=D){O.addEventListener("load",Y,false)}else{if(typeof j.addEventListener!=D){j.addEventListener("load",Y,false)}else{if(typeof O.attachEvent!=D){i(O,"onload",Y)}else{if(typeof O.onload=="function"){var X=O.onload;O.onload=function(){X();Y()}}else{O.onload=Y}}}}}function h(){if(T){V()}else{H()}}function V(){var X=j.getElementsByTagName("body")[0];var aa=C(r);aa.setAttribute("type",q);var Z=X.appendChild(aa);if(Z){var Y=0;(function(){if(typeof Z.GetVariable!=D){var ab=Z.GetVariable("$version");if(ab){ab=ab.split(" ")[1].split(",");M.pv=[parseInt(ab[0],10),parseInt(ab[1],10),parseInt(ab[2],10)]}}else{if(Y<10){Y++;setTimeout(arguments.callee,10);return}}X.removeChild(aa);Z=null;H()})()}else{H()}}function H(){var ag=o.length;if(ag>0){for(var af=0;af<ag;af++){var Y=o[af].id;var ab=o[af].callbackFn;var aa={success:false,id:Y};if(M.pv[0]>0){var ae=c(Y);if(ae){if(F(o[af].swfVersion)&&!(M.wk&&M.wk<312)){w(Y,true);if(ab){aa.success=true;aa.ref=z(Y);ab(aa)}}else{if(o[af].expressInstall&&A()){var ai={};ai.data=o[af].expressInstall;ai.width=ae.getAttribute("width")||"0";ai.height=ae.getAttribute("height")||"0";if(ae.getAttribute("class")){ai.styleclass=ae.getAttribute("class")}if(ae.getAttribute("align")){ai.align=ae.getAttribute("align")}var ah={};var X=ae.getElementsByTagName("param");var ac=X.length;for(var ad=0;ad<ac;ad++){if(X[ad].getAttribute("name").toLowerCase()!="movie"){ah[X[ad].getAttribute("name")]=X[ad].getAttribute("value")}}P(ai,ah,Y,ab)}else{p(ae);if(ab){ab(aa)}}}}}else{w(Y,true);if(ab){var Z=z(Y);if(Z&&typeof Z.SetVariable!=D){aa.success=true;aa.ref=Z}ab(aa)}}}}}function z(aa){var X=null;var Y=c(aa);if(Y&&Y.nodeName=="OBJECT"){if(typeof Y.SetVariable!=D){X=Y}else{var Z=Y.getElementsByTagName(r)[0];if(Z){X=Z}}}return X}function A(){return !a&&F("6.0.65")&&(M.win||M.mac)&&!(M.wk&&M.wk<312)}function P(aa,ab,X,Z){a=true;E=Z||null;B={success:false,id:X};var ae=c(X);if(ae){if(ae.nodeName=="OBJECT"){l=g(ae);Q=null}else{l=ae;Q=X}aa.id=R;if(typeof aa.width==D||(!/%$/.test(aa.width)&&parseInt(aa.width,10)<310)){aa.width="310"}if(typeof aa.height==D||(!/%$/.test(aa.height)&&parseInt(aa.height,10)<137)){aa.height="137"}j.title=j.title.slice(0,47)+" - Flash Player Installation";var ad=M.ie&&M.win?"ActiveX":"PlugIn",ac="MMredirectURL="+encodeURI(O.location).toString().replace(/&/g,"%26")+"&MMplayerType="+ad+"&MMdoctitle="+j.title;if(typeof ab.flashvars!=D){ab.flashvars+="&"+ac}else{ab.flashvars=ac}if(M.ie&&M.win&&ae.readyState!=4){var Y=C("div");X+="SWFObjectNew";Y.setAttribute("id",X);ae.parentNode.insertBefore(Y,ae);ae.style.display="none";(function(){if(ae.readyState==4){ae.parentNode.removeChild(ae)}else{setTimeout(arguments.callee,10)}})()}u(aa,ab,X)}}function p(Y){if(M.ie&&M.win&&Y.readyState!=4){var X=C("div");Y.parentNode.insertBefore(X,Y);X.parentNode.replaceChild(g(Y),X);Y.style.display="none";(function(){if(Y.readyState==4){Y.parentNode.removeChild(Y)}else{setTimeout(arguments.callee,10)}})()}else{Y.parentNode.replaceChild(g(Y),Y)}}function g(ab){var aa=C("div");if(M.win&&M.ie){aa.innerHTML=ab.innerHTML}else{var Y=ab.getElementsByTagName(r)[0];if(Y){var ad=Y.childNodes;if(ad){var X=ad.length;for(var Z=0;Z<X;Z++){if(!(ad[Z].nodeType==1&&ad[Z].nodeName=="PARAM")&&!(ad[Z].nodeType==8)){aa.appendChild(ad[Z].cloneNode(true))}}}}}return aa}function u(ai,ag,Y){var X,aa=c(Y);if(M.wk&&M.wk<312){return X}if(aa){if(typeof ai.id==D){ai.id=Y}if(M.ie&&M.win){var ah="";for(var ae in ai){if(ai[ae]!=Object.prototype[ae]){if(ae.toLowerCase()=="data"){ag.movie=ai[ae]}else{if(ae.toLowerCase()=="styleclass"){ah+=' class="'+ai[ae]+'"'}else{if(ae.toLowerCase()!="classid"){ah+=" "+ae+'="'+ai[ae]+'"'}}}}}var af="";for(var ad in ag){if(ag[ad]!=Object.prototype[ad]){af+='<param name="'+ad+'" value="'+ag[ad]+'" />'}}aa.outerHTML='<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"'+ah+">"+af+"</object>";N[N.length]=ai.id;X=c(ai.id)}else{var Z=C(r);Z.setAttribute("type",q);for(var ac in ai){if(ai[ac]!=Object.prototype[ac]){if(ac.toLowerCase()=="styleclass"){Z.setAttribute("class",ai[ac])}else{if(ac.toLowerCase()!="classid"){Z.setAttribute(ac,ai[ac])}}}}for(var ab in ag){if(ag[ab]!=Object.prototype[ab]&&ab.toLowerCase()!="movie"){e(Z,ab,ag[ab])}}aa.parentNode.replaceChild(Z,aa);X=Z}}return X}function e(Z,X,Y){var aa=C("param");aa.setAttribute("name",X);aa.setAttribute("value",Y);Z.appendChild(aa)}function y(Y){var X=c(Y);if(X&&X.nodeName=="OBJECT"){if(M.ie&&M.win){X.style.display="none";(function(){if(X.readyState==4){b(Y)}else{setTimeout(arguments.callee,10)}})()}else{X.parentNode.removeChild(X)}}}function b(Z){var Y=c(Z);if(Y){for(var X in Y){if(typeof Y[X]=="function"){Y[X]=null}}Y.parentNode.removeChild(Y)}}function c(Z){var X=null;try{X=j.getElementById(Z)}catch(Y){}return X}function C(X){return j.createElement(X)}function i(Z,X,Y){Z.attachEvent(X,Y);I[I.length]=[Z,X,Y]}function F(Z){var Y=M.pv,X=Z.split(".");X[0]=parseInt(X[0],10);X[1]=parseInt(X[1],10)||0;X[2]=parseInt(X[2],10)||0;return(Y[0]>X[0]||(Y[0]==X[0]&&Y[1]>X[1])||(Y[0]==X[0]&&Y[1]==X[1]&&Y[2]>=X[2]))?true:false}function v(ac,Y,ad,ab){if(M.ie&&M.mac){return}var aa=j.getElementsByTagName("head")[0];if(!aa){return}var X=(ad&&typeof ad=="string")?ad:"screen";if(ab){n=null;G=null}if(!n||G!=X){var Z=C("style");Z.setAttribute("type","text/css");Z.setAttribute("media",X);n=aa.appendChild(Z);if(M.ie&&M.win&&typeof j.styleSheets!=D&&j.styleSheets.length>0){n=j.styleSheets[j.styleSheets.length-1]}G=X}if(M.ie&&M.win){if(n&&typeof n.addRule==r){n.addRule(ac,Y)}}else{if(n&&typeof j.createTextNode!=D){n.appendChild(j.createTextNode(ac+" {"+Y+"}"))}}}function w(Z,X){if(!m){return}var Y=X?"visible":"hidden";if(J&&c(Z)){c(Z).style.visibility=Y}else{v("#"+Z,"visibility:"+Y)}}function L(Y){var Z=/[\\\"<>\.;]/;var X=Z.exec(Y)!=null;return X&&typeof encodeURIComponent!=D?encodeURIComponent(Y):Y}var d=function(){if(M.ie&&M.win){window.attachEvent("onunload",function(){var ac=I.length;for(var ab=0;ab<ac;ab++){I[ab][0].detachEvent(I[ab][1],I[ab][2])}var Z=N.length;for(var aa=0;aa<Z;aa++){y(N[aa])}for(var Y in M){M[Y]=null}M=null;for(var X in swfobject){swfobject[X]=null}swfobject=null})}}();return{registerObject:function(ab,X,aa,Z){if(M.w3&&ab&&X){var Y={};Y.id=ab;Y.swfVersion=X;Y.expressInstall=aa;Y.callbackFn=Z;o[o.length]=Y;w(ab,false)}else{if(Z){Z({success:false,id:ab})}}},getObjectById:function(X){if(M.w3){return z(X)}},embedSWF:function(ab,ah,ae,ag,Y,aa,Z,ad,af,ac){var X={success:false,id:ah};if(M.w3&&!(M.wk&&M.wk<312)&&ab&&ah&&ae&&ag&&Y){w(ah,false);K(function(){ae+="";ag+="";var aj={};if(af&&typeof af===r){for(var al in af){aj[al]=af[al]}}aj.data=ab;aj.width=ae;aj.height=ag;var am={};if(ad&&typeof ad===r){for(var ak in ad){am[ak]=ad[ak]}}if(Z&&typeof Z===r){for(var ai in Z){if(typeof am.flashvars!=D){am.flashvars+="&"+ai+"="+Z[ai]}else{am.flashvars=ai+"="+Z[ai]}}}if(F(Y)){var an=u(aj,am,ah);if(aj.id==ah){w(ah,true)}X.success=true;X.ref=an}else{if(aa&&A()){aj.data=aa;P(aj,am,ah,ac);return}else{w(ah,true)}}if(ac){ac(X)}})}else{if(ac){ac(X)}}},switchOffAutoHideShow:function(){m=false},ua:M,getFlashPlayerVersion:function(){return{major:M.pv[0],minor:M.pv[1],release:M.pv[2]}},hasFlashPlayerVersion:F,createSWF:function(Z,Y,X){if(M.w3){return u(Z,Y,X)}else{return undefined}},showExpressInstall:function(Z,aa,X,Y){if(M.w3&&A()){P(Z,aa,X,Y)}},removeSWF:function(X){if(M.w3){y(X)}},createCSS:function(aa,Z,Y,X){if(M.w3){v(aa,Z,Y,X)}},addDomLoadEvent:K,addLoadEvent:s,getQueryParamValue:function(aa){var Z=j.location.search||j.location.hash;if(Z){if(/\?/.test(Z)){Z=Z.split("?")[1]}if(aa==null){return L(Z)}var Y=Z.split("&");for(var X=0;X<Y.length;X++){if(Y[X].substring(0,Y[X].indexOf("="))==aa){return L(Y[X].substring((Y[X].indexOf("=")+1)))}}}return""},expressInstallCallback:function(){if(a){var X=c(R);if(X&&l){X.parentNode.replaceChild(l,X);if(Q){w(Q,true);if(M.ie&&M.win){l.style.display="block"}}if(E){E(B)}}a=false}}}}();

;
/* swfupload.swfobject.js */

/* 1   */ /*
/* 2   *| 	SWFUpload.SWFObject Plugin
/* 3   *| 
/* 4   *| 	Summary:
/* 5   *| 		This plugin uses SWFObject to embed SWFUpload dynamically in the page.  SWFObject provides accurate Flash Player detection and DOM Ready loading.
/* 6   *| 		This plugin replaces the Graceful Degradation plugin.
/* 7   *| 
/* 8   *| 	Features:
/* 9   *| 		* swfupload_load_failed_hander event
/* 10  *| 		* swfupload_pre_load_handler event
/* 11  *| 		* minimum_flash_version setting (default: "9.0.28")
/* 12  *| 		* SWFUpload.onload event for early loading
/* 13  *| 
/* 14  *| 	Usage:
/* 15  *| 		Provide handlers and settings as needed.  When using the SWFUpload.SWFObject plugin you should initialize SWFUploading
/* 16  *| 		in SWFUpload.onload rather than in window.onload.  When initialized this way SWFUpload can load earlier preventing the UI flicker
/* 17  *| 		that was seen using the Graceful Degradation plugin.
/* 18  *| 
/* 19  *| 		<script type="text/javascript">
/* 20  *| 			var swfu;
/* 21  *| 			SWFUpload.onload = function () {
/* 22  *| 				swfu = new SWFUpload({
/* 23  *| 					minimum_flash_version: "9.0.28",
/* 24  *| 					swfupload_pre_load_handler: swfuploadPreLoad,
/* 25  *| 					swfupload_load_failed_handler: swfuploadLoadFailed
/* 26  *| 				});
/* 27  *| 			};
/* 28  *| 		</script>
/* 29  *| 		
/* 30  *| 	Notes:
/* 31  *| 		You must provide set minimum_flash_version setting to "8" if you are using SWFUpload for Flash Player 8.
/* 32  *| 		The swfuploadLoadFailed event is only fired if the minimum version of Flash Player is not met.  Other issues such as missing SWF files, browser bugs
/* 33  *| 		 or corrupt Flash Player installations will not trigger this event.
/* 34  *| 		The swfuploadPreLoad event is fired as soon as the minimum version of Flash Player is found.  It does not wait for SWFUpload to load and can
/* 35  *| 		 be used to prepare the SWFUploadUI and hide alternate content.
/* 36  *| 		swfobject's onDomReady event is cross-browser safe but will default to the window.onload event when DOMReady is not supported by the browser.
/* 37  *| 		 Early DOM Loading is supported in major modern browsers but cannot be guaranteed for every browser ever made.
/* 38  *| */
/* 39  */ 
/* 40  */ 
/* 41  */ // SWFObject v2.1 must be loaded
/* 42  */ 	
/* 43  */ var SWFUpload;
/* 44  */ if (typeof(SWFUpload) === "function") {
/* 45  */ 	SWFUpload.onload = function () {};
/* 46  */ 
/* 47  */ 	swfobject.addDomLoadEvent(function () {
/* 48  */ 		if (typeof(SWFUpload.onload) === "function") {
/* 49  */ 			setTimeout(function(){SWFUpload.onload.call(window);}, 200);
/* 50  */ 		}

/* swfupload.swfobject.js */

/* 51  */ 	});
/* 52  */ 
/* 53  */ 	SWFUpload.prototype.initSettings = (function (oldInitSettings) {
/* 54  */ 		return function () {
/* 55  */ 			if (typeof(oldInitSettings) === "function") {
/* 56  */ 				oldInitSettings.call(this);
/* 57  */ 			}
/* 58  */ 
/* 59  */ 			this.ensureDefault = function (settingName, defaultValue) {
/* 60  */ 				this.settings[settingName] = (this.settings[settingName] == undefined) ? defaultValue : this.settings[settingName];
/* 61  */ 			};
/* 62  */ 
/* 63  */ 			this.ensureDefault("minimum_flash_version", "9.0.28");
/* 64  */ 			this.ensureDefault("swfupload_pre_load_handler", null);
/* 65  */ 			this.ensureDefault("swfupload_load_failed_handler", null);
/* 66  */ 
/* 67  */ 			delete this.ensureDefault;
/* 68  */ 
/* 69  */ 		};
/* 70  */ 	})(SWFUpload.prototype.initSettings);
/* 71  */ 
/* 72  */ 
/* 73  */ 	SWFUpload.prototype.loadFlash = function (oldLoadFlash) {
/* 74  */ 		return function () {
/* 75  */ 			var hasFlash = swfobject.hasFlashPlayerVersion(this.settings.minimum_flash_version);
/* 76  */ 			
/* 77  */ 			if (hasFlash) {
/* 78  */ 				this.queueEvent("swfupload_pre_load_handler");
/* 79  */ 				if (typeof(oldLoadFlash) === "function") {
/* 80  */ 					oldLoadFlash.call(this);
/* 81  */ 				}
/* 82  */ 			} else {
/* 83  */ 				this.queueEvent("swfupload_load_failed_handler");
/* 84  */ 			}
/* 85  */ 		};
/* 86  */ 		
/* 87  */ 	}(SWFUpload.prototype.loadFlash);
/* 88  */ 			
/* 89  */ 	SWFUpload.prototype.displayDebugInfo = function (oldDisplayDebugInfo) {
/* 90  */ 		return function () {
/* 91  */ 			if (typeof(oldDisplayDebugInfo) === "function") {
/* 92  */ 				oldDisplayDebugInfo.call(this);
/* 93  */ 			}
/* 94  */ 			
/* 95  */ 			this.debug(
/* 96  */ 				[
/* 97  */ 					"SWFUpload.SWFObject Plugin settings:", "\n",
/* 98  */ 					"\t", "minimum_flash_version:                      ", this.settings.minimum_flash_version, "\n",
/* 99  */ 					"\t", "swfupload_pre_load_handler assigned:     ", (typeof(this.settings.swfupload_pre_load_handler) === "function").toString(), "\n",
/* 100 */ 					"\t", "swfupload_load_failed_handler assigned:     ", (typeof(this.settings.swfupload_load_failed_handler) === "function").toString(), "\n",

/* swfupload.swfobject.js */

/* 101 */ 				].join("")
/* 102 */ 			);
/* 103 */ 		};	
/* 104 */ 	}(SWFUpload.prototype.displayDebugInfo);
/* 105 */ }
/* 106 */ 
