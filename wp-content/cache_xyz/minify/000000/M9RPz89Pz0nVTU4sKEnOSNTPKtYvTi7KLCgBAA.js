
/* script.js */

/* 1   */ (function( $, gglcptch ) {
/* 2   */ 	gglcptch = gglcptch || {};
/* 3   */ 
/* 4   */ 	gglcptch.prepare = function() {
/* 5   */ 		/*
/* 6   *| 		 * display reCaptcha for plugin`s block
/* 7   *| 		 */
/* 8   */ 		$( '.gglcptch_v1, .gglcptch_v2' ).each( function() {
/* 9   */ 			var container = $( this ).find( '.gglcptch_recaptcha' ).attr( 'id' );
/* 10  */ 			if ( $( this ).is( ':visible' ) )
/* 11  */ 				gglcptch.display( container );
/* 12  */ 		});
/* 13  */ 
/* 14  */ 		/*
/* 15  *| 		 * display reCaptcha for others blocks
/* 16  *| 		 * this part is neccessary because
/* 17  *| 		 * we have disabled the connection to Google reCaptcha API from other plugins
/* 18  *| 		 * via plugin`s php-functionality
/* 19  *| 		 */
/* 20  */ 		if ( gglcptch.options.version == 'v2' ) {
/* 21  */ 			$( '.g-recaptcha' ).each( function() {
/* 22  */ 				/* reCAPTCHA will be generated into the empty block only */
/* 23  */ 				if ( $( this ).html() === '' && $( this ).text() === '' ) {
/* 24  */ 
/* 25  */ 					/* get element`s ID */
/* 26  */ 					var container = $( this ).attr( 'id' );
/* 27  */ 
/* 28  */ 					if ( typeof container == 'undefined' ) {
/* 29  */ 						container = get_id();
/* 30  */ 						$( this ).attr( 'id', container );
/* 31  */ 					}
/* 32  */ 
/* 33  */ 					/* get reCapatcha parameters */
/* 34  */ 					var sitekey  = $( this ).attr( 'data-sitekey' ),
/* 35  */ 						theme    = $( this ).attr( 'data-theme' ),
/* 36  */ 						lang     = $( this ).attr( 'data-lang' ),
/* 37  */ 						size     = $( this ).attr( 'data-size' ),
/* 38  */ 						type     = $( this ).attr( 'data-type' ),
/* 39  */ 						tabindex = $( this ).attr( 'data-tabindex' ),
/* 40  */ 						callback = $( this ).attr( 'data-callback' ),
/* 41  */ 						ex_call  = $( this ).attr( 'data-expired-callback' ),
/* 42  */ 						stoken   = $( this ).attr( 'data-stoken' ),
/* 43  */ 						params   = [];
/* 44  */ 
/* 45  */ 					params['sitekey'] = sitekey ? sitekey : gglcptch.options.sitekey;
/* 46  */ 					if ( !! theme )
/* 47  */ 						params['theme'] = theme;
/* 48  */ 					if ( !! lang )
/* 49  */ 						params['lang'] = lang;
/* 50  */ 					if ( !! size )

/* script.js */

/* 51  */ 						params['size'] = size;
/* 52  */ 					if ( !! type )
/* 53  */ 						params['type'] = type;
/* 54  */ 					if ( !! tabindex )
/* 55  */ 						params['tabindex'] = tabindex;
/* 56  */ 					if ( !! callback )
/* 57  */ 						params['callback'] = callback;
/* 58  */ 					if ( !! ex_call )
/* 59  */ 						params['expired-callback'] = ex_call;
/* 60  */ 					if ( !! stoken )
/* 61  */ 						params['stoken'] = stoken;
/* 62  */ 
/* 63  */ 					gglcptch.display( container, false, params );
/* 64  */ 				}
/* 65  */ 			});
/* 66  */ 
/* 67  */ 			/*
/* 68  *| 			 * count the number of reCAPTCHA blocks in the form
/* 69  *| 			 */
/* 70  */ 			$( 'form' ).each( function() {
/* 71  */ 				if ( $( this ).contents().find( 'iframe[title="recaptcha widget"]' ).length > 1 && ! $( this ).children( '.gglcptch_dublicate_error' ).length ) {
/* 72  */ 					$( this ).prepend( '<div class="gglcptch_dublicate_error error" style="color: red;">'+ gglcptch.options.error + '</div><br />\n' );
/* 73  */ 				}
/* 74  */ 			});
/* 75  */ 		}
/* 76  */ 	};
/* 77  */ 
/* 78  */ 	gglcptch.display = function( container, v1_add_to_last_element, params ) {
/* 79  */ 		if ( typeof( container ) == 'undefined' || container == '' || typeof( gglcptch.options ) == 'undefined' ) {
/* 80  */ 			return;
/* 81  */ 		}
/* 82  */ 
/* 83  */ 		var gglcptch_version = gglcptch.options.version;
/* 84  */ 		v1_add_to_last_element = v1_add_to_last_element || false;
/* 85  */ 
/* 86  */ 		if ( gglcptch_version == 'v1' ) {
/* 87  */ 			if ( Recaptcha.widget == null || v1_add_to_last_element == true ) {
/* 88  */ 				Recaptcha.create( gglcptch.options.sitekey, container, { 'theme' : gglcptch.options.theme } );
/* 89  */ 			}
/* 90  */ 		}
/* 91  */ 		if ( gglcptch_version == 'v2' ) {
/* 92  */ 			var parameters = params ? params : { 'sitekey' : gglcptch.options.sitekey, 'theme' : gglcptch.options.theme },
/* 93  */ 				gglcptch_index = grecaptcha.render( container, parameters );
/* 94  */ 			$( '#' + container ).data( 'gglcptch_index', gglcptch_index );
/* 95  */ 		}
/* 96  */ 	};
/* 97  */ 
/* 98  */ 	window.onload = gglcptch.prepare;
/* 99  */ 
/* 100 */ 	$( document ).ready(function() {

/* script.js */

/* 101 */ 
/* 102 */ 		$( '#recaptcha_widget_div' ).on( 'input paste change', '#recaptcha_response_field', cleanError );
/* 103 */ 
/* 104 */ 		$( 'form' ).not( '[name="loginform"], [name="registerform"], [name="lostpasswordform"], #setupform, .cntctfrmpr_contact_form, .cntctfrm_contact_form, #commentform, #gglcptch_admin_settings_page' ).submit( function( e ) {
/* 105 */ 			var $form = $( this ),
/* 106 */ 				$gglcptch = $form.find( '.gglcptch' ),
/* 107 */ 				$captcha = $gglcptch.filter( '.gglcptch_v1' ).find( '.gglcptch_recaptcha:visible' ),
/* 108 */ 				$captcha_v2 = $gglcptch.filter( '.gglcptch_v2' ).find( '.gglcptch_recaptcha:visible' );
/* 109 */ 			if ( $captcha.length ) {
/* 110 */ 				if ( $gglcptch.find( 'input[name="gglcptch_test_enable_js_field"]:hidden' ).length == 0 ) {
/* 111 */ 					$gglcptch.append( '<input type="hidden" value="' + gglcptch.vars.nonce + '" name="gglcptch_test_enable_js_field" />' );
/* 112 */ 				}
/* 113 */ 				$.ajax({
/* 114 */ 					async   : false,
/* 115 */ 					cache   : false,
/* 116 */ 					type    : 'POST',
/* 117 */ 					url     : gglcptch.vars.ajaxurl,
/* 118 */ 					headers : {
/* 119 */ 						'Content-Type' : 'application/x-www-form-urlencoded'
/* 120 */ 					},
/* 121 */ 					data    : {
/* 122 */ 						action: 'gglcptch_captcha_check',
/* 123 */ 						recaptcha_challenge_field : $( '#recaptcha_challenge_field' ).val(),
/* 124 */ 						recaptcha_response_field  : $( '#recaptcha_response_field' ).val()
/* 125 */ 					},
/* 126 */ 					success: function( data ) {
/* 127 */ 						if ( data == 'error' ) {
/* 128 */ 							if ( $captcha.next( '#gglcptch_error' ).length == 0 ) {
/* 129 */ 								$captcha.after( '<label id="gglcptch_error">' + gglcptch.vars.error_msg + '</label>' );
/* 130 */ 							}
/* 131 */ 							$( '#recaptcha_reload' ).trigger( 'click' );
/* 132 */ 							e.preventDefault ? e.preventDefault() : (e.returnValue = false);
/* 133 */ 							return false;
/* 134 */ 						}
/* 135 */ 					},
/* 136 */ 					error: function( request, status, error ) {
/* 137 */ 						if ( $captcha.next( '#gglcptch_error' ).length == 0 ) {
/* 138 */ 							$captcha.after( '<label id="gglcptch_error">' + request.status + ' ' + error + '</label>' );
/* 139 */ 						}
/* 140 */ 						$( '#recaptcha_reload' ).trigger( 'click' );
/* 141 */ 						e.preventDefault ? e.preventDefault() : (e.returnValue = false);
/* 142 */ 						return false;
/* 143 */ 					}
/* 144 */ 				});
/* 145 */ 				$( '#recaptcha_reload' ).trigger( 'click' );
/* 146 */ 			} else if ( $captcha_v2.length ) {
/* 147 */ 				if ( $gglcptch.find( 'input[name="gglcptch_test_enable_js_field"]:hidden' ).length == 0 ) {
/* 148 */ 					$gglcptch.append( '<input type="hidden" value="' + gglcptch.vars.nonce + '" name="gglcptch_test_enable_js_field" />' );
/* 149 */ 				}
/* 150 */ 				$.ajax({

/* script.js */

/* 151 */ 					async   : false,
/* 152 */ 					cache   : false,
/* 153 */ 					type    : 'POST',
/* 154 */ 					url     : gglcptch.vars.ajaxurl,
/* 155 */ 					headers : {
/* 156 */ 						'Content-Type' : 'application/x-www-form-urlencoded'
/* 157 */ 					},
/* 158 */ 					data    : {
/* 159 */ 						action: 'gglcptch_captcha_check',
/* 160 */ 						'g-recaptcha-response'  : $form.find( '.g-recaptcha-response' ).val()
/* 161 */ 					},
/* 162 */ 					success: function( data ) {
/* 163 */ 						if ( data == 'error' ) {
/* 164 */ 							if ( $captcha_v2.next( '#gglcptch_error' ).length == 0 ) {
/* 165 */ 								$captcha_v2.after( '<label id="gglcptch_error">' + gglcptch.vars.error_msg + '</label>' );
/* 166 */ 								$( "#gglcptch_error" ).fadeOut( 4000, function() {
/* 167 */ 									$( "#gglcptch_error" ).remove();
/* 168 */ 								});
/* 169 */ 								$( 'html, body' ).animate({ scrollTop: $captcha_v2.offset().top - 50 }, 500);
/* 170 */ 							}
/* 171 */ 							e.preventDefault ? e.preventDefault() : (e.returnValue = false);
/* 172 */ 							return false;
/* 173 */ 						}
/* 174 */ 					},
/* 175 */ 					error: function( request, status, error ) {
/* 176 */ 						if ( $captcha_v2.next( '#gglcptch_error' ).length == 0 ) {
/* 177 */ 							$captcha_v2.after( '<label id="gglcptch_error">' + request.status + ' ' + error + '</label>' );
/* 178 */ 						}
/* 179 */ 						e.preventDefault ? e.preventDefault() : (e.returnValue = false);
/* 180 */ 						return false;
/* 181 */ 					}
/* 182 */ 				});
/* 183 */ 			}
/* 184 */ 		});
/* 185 */ 	});
/* 186 */ 
/* 187 */ 	function cleanError() {
/* 188 */ 		$error = $( this ).parents( '#recaptcha_widget_div' ).next( '#gglcptch_error' );
/* 189 */ 		if ( $error.length ) {
/* 190 */ 			$error.remove();
/* 191 */ 		}
/* 192 */ 	}
/* 193 */ 
/* 194 */ 	function get_id() {
/* 195 */ 		var id = 'gglcptch_recaptcha_' + Math.floor( Math.random() * 1000 );
/* 196 */ 		if ( $( '#' + id ).length )
/* 197 */ 			id = get_id();
/* 198 */ 		else
/* 199 */ 			return id;
/* 200 */ 	}

/* script.js */

/* 201 */ 
/* 202 */ })(jQuery, gglcptch);
