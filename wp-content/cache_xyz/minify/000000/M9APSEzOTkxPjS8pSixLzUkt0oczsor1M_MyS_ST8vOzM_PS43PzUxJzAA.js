
/* booking_modal.js */

/* 1   */ /**
/* 2   *|  * Created by me664 on 12/29/14.
/* 3   *|  */
/* 4   */ jQuery(document).ready(function($){
/* 5   */ 
/* 6   */     $(document).on('click','.payment_gateway_item .st_payment_gatewaw_submit',function(){
/* 7   */         var me=$(this).parents('.booking_modal_form');
/* 8   */ 
/* 9   */         $(this).next('.icon_loading').remove();
/* 10  */         $(this).after('<span class="icon_loading"><i class="fa fa-spin fa-refresh"></i></span>');
/* 11  */ 
/* 12  */         submit_form(me,$(this));
/* 13  */     });
/* 14  */ 
/* 15  */     $(document).on('click','.btn_hotel_booking',function(){
/* 16  */         var form=$(this).closest('form');
/* 17  */         if(!checkRequiredBooking(form))
/* 18  */         {
/* 19  */             return false;
/* 20  */         }
/* 21  */ 
/* 22  */         var tar_get=$(this).data('target');
/* 23  */ 
/* 24  */         $.magnificPopup.open({
/* 25  */             items: {
/* 26  */                 type: 'inline',
/* 27  */                 src: tar_get
/* 28  */             }
/* 29  */ 
/* 30  */         });
/* 31  */ 
/* 32  */     });
/* 33  */     function do_scrollTo(el)
/* 34  */     {
/* 35  */         if(el.length){
/* 36  */             var top=el.offset().top;
/* 37  */             if($('#wpadminbar').length && $('#wpadminbar').css('position')=='fixed')
/* 38  */             {
/* 39  */                 top-=32;
/* 40  */             }
/* 41  */             top-=50;
/* 42  */             $('html,body').animate({
/* 43  */                 'scrollTop':top
/* 44  */             },500);
/* 45  */         }
/* 46  */     }
/* 47  */ 
/* 48  */     function setMessage(holder,message,type)
/* 49  */     {
/* 50  */         if(typeof  type=='undefined'){

/* booking_modal.js */

/* 51  */             type='infomation';
/* 52  */         }
/* 53  */         var html='<div class="alert alert-'+type+'">'+message+'</div>';
/* 54  */         if(!holder.length) return;
/* 55  */         holder.html('');
/* 56  */         holder.html(html);
/* 57  */         do_scrollTo(holder);
/* 58  */     }
/* 59  */ 
/* 60  */ 
/* 61  */     function checkRequiredBooking(searchbox)
/* 62  */     {
/* 63  */         var searchform=$('.booking-item-dates-change');
/* 64  */         if(typeof searchbox!="undefined")
/* 65  */         {
/* 66  */             var data=searchbox.find('input,select,textarea').serializeArray();
/* 67  */         }
/* 68  */ 
/* 69  */         var dataobj = {};
/* 70  */         for (var i = 0; i < data.length; ++i)
/* 71  */             dataobj[data[i].name] = data[i].value;
/* 72  */ 
/* 73  */         var holder=$('.search_room_alert');
/* 74  */ 
/* 75  */         holder.html('');
/* 76  */         if(dataobj.room_num_search=="1"){
/* 77  */             if(dataobj.adult_num=="" || dataobj.child_num=='' ||typeof dataobj.adult_num=='undefined' || typeof dataobj.child_num=='undefined'){
/* 78  */ 
/* 79  */                 setMessage(holder,st_hotel_localize.booking_required_adult_children,'danger');
/* 80  */                 return false;
/* 81  */             }
/* 82  */ 
/* 83  */         }
/* 84  */         if(dataobj.check_in=="" || dataobj.check_out=='')
/* 85  */         {
/* 86  */             if(dataobj.check_in==""){
/* 87  */                 searchform.find('[name=start]').addClass('error');
/* 88  */             }
/* 89  */             if(dataobj.check_out==""){
/* 90  */                 searchform.find('[name=end]').addClass('error');
/* 91  */             }
/* 92  */             setMessage(holder,st_hotel_localize.is_not_select_date,'danger');
/* 93  */             return false;
/* 94  */         }
/* 95  */ 
/* 96  */         return true;
/* 97  */ 
/* 98  */     }
/* 99  */ 
/* 100 */     function submit_form(me,clicked)

/* booking_modal.js */

/* 101 */     {
/* 102 */         var data=me.find("select,textarea, input").serializeArray();
/* 103 */ 
/* 104 */         me.find('.form-control').removeClass('error');
/* 105 */ 
/* 106 */ 
/* 107 */         data.push(
/* 108 */             {
/* 109 */                 name:'action',
/* 110 */                 value:'booking_form_submit'
/* 111 */             }
/* 112 */         );
/* 113 */ 
/* 114 */ 
/* 115 */         me.find('.form_alert').addClass('hidden');
/* 116 */         var dataobj = {};
/* 117 */ 
/* 118 */         var form_validate=true;
/* 119 */ 
/* 120 */ 
/* 121 */         for (var i = 0; i < data.length; ++i)
/* 122 */         {
/* 123 */             dataobj[data[i].name] = data[i].value;
/* 124 */             var name=data[i].name;
/* 125 */ 
/* 126 */         }
/* 127 */         me.find('input.required,select.required,textarea.required').removeClass('error');
/* 128 */         me.find('input.required,select.required,textarea.required').each(function(){
/* 129 */             if(!$(this).val())
/* 130 */             {
/* 131 */                 $(this).addClass('error');
/* 132 */                 form_validate=false;
/* 133 */ 
/* 134 */             }
/* 135 */         });
/* 136 */ 
/* 137 */ 
/* 138 */ 
/* 139 */         dataobj[clicked.attr('name')]=clicked.attr('value');
/* 140 */ 
/* 141 */         if(form_validate==false){
/* 142 */             me.find('.form_alert').addClass('alert-danger').removeClass('hidden');
/* 143 */             me.find('.form_alert').html(st_checkout_text.validate_form);
/* 144 */             me.find('.icon_loading').remove();
/* 145 */             return false;
/* 146 */         }
/* 147 */ 
/* 148 */         //term_condition
/* 149 */         if(!dataobj.term_condition){
/* 150 */             me.find('.form_alert').addClass('alert-danger').removeClass('hidden');

/* booking_modal.js */

/* 151 */             me.find('.form_alert').html(st_checkout_text.error_accept_term);
/* 152 */ 
/* 153 */             me.find('.icon_loading').remove();
/* 154 */             return false;
/* 155 */         }
/* 156 */         //console.log(dataobj);
/* 157 */ 
/* 158 */ 
/* 159 */         me.addClass('loading');
/* 160 */         $.ajax({
/* 161 */             'type':'post',
/* 162 */             'dataType':'json',
/* 163 */             'url':st_params.ajax_url,
/* 164 */             'data':dataobj,
/* 165 */             'success':function(data){
/* 166 */                 me.removeClass('loading');
/* 167 */ 
/* 168 */                 if(data.message){
/* 169 */                     me.find('.form_alert').addClass('alert-danger').removeClass('hidden');
/* 170 */                     me.find('.form_alert').html(data.message);
/* 171 */                 }
/* 172 */ 
/* 173 */                 if(data.redirect){
/* 174 */                     window.location.href=data.redirect;
/* 175 */                 }
/* 176 */ 
/* 177 */                 me.find('.icon_loading').remove();
/* 178 */ 
/* 179 */                 var widget_id='st_recaptchar_'+dataobj.item_id;
/* 180 */ 
/* 181 */                 get_new_captcha(me);
/* 182 */             },
/* 183 */             error:function(data){
/* 184 */ 
/* 185 */                 me.removeClass('loading');
/* 186 */                 alert('Ajax Fail');
/* 187 */ 
/* 188 */                 me.find('.icon_loading').remove();
/* 189 */ 
/* 190 */                 var widget_id='st_recaptchar_'+dataobj.item_id;
/* 191 */ 
/* 192 */                 get_new_captcha(me);
/* 193 */ 
/* 194 */             }
/* 195 */         });
/* 196 */ 
/* 197 */         function get_new_captcha(me)
/* 198 */         {
/* 199 */             var captcha_box=me.find('.captcha_box');
/* 200 */             url=captcha_box.find('.captcha_img').attr('src');

/* booking_modal.js */

/* 201 */             captcha_box.find('.captcha_img').attr('src',url);
/* 202 */         }
/* 203 */     }
/* 204 */ });
