
/* comment-reply.min.js */

/* 1 */ var addComment={moveForm:function(a,b,c,d){var e,f,g,h,i=this,j=i.I(a),k=i.I(c),l=i.I("cancel-comment-reply-link"),m=i.I("comment_parent"),n=i.I("comment_post_ID"),o=k.getElementsByTagName("form")[0];if(j&&k&&l&&m&&o){i.respondId=c,d=d||!1,i.I("wp-temp-form-div")||(e=document.createElement("div"),e.id="wp-temp-form-div",e.style.display="none",k.parentNode.insertBefore(e,k)),j.parentNode.insertBefore(k,j.nextSibling),n&&d&&(n.value=d),m.value=b,l.style.display="",l.onclick=function(){var a=addComment,b=a.I("wp-temp-form-div"),c=a.I(a.respondId);if(b&&c)return a.I("comment_parent").value="0",b.parentNode.insertBefore(c,b),b.parentNode.removeChild(b),this.style.display="none",this.onclick=null,!1};try{for(var p=0;p<o.elements.length;p++)if(f=o.elements[p],h=!1,"getComputedStyle"in window?g=window.getComputedStyle(f):document.documentElement.currentStyle&&(g=f.currentStyle),(f.offsetWidth<=0&&f.offsetHeight<=0||"hidden"===g.visibility)&&(h=!0),"hidden"!==f.type&&!f.disabled&&!h){f.focus();break}}catch(q){}return!1}},I:function(a){return document.getElementById(a)}};

;
/* review_form.js */

/* 1  */ /**
/* 2  *|  * Created by me664 on 11/18/14.
/* 3  *|  */
/* 4  */ jQuery(document).ready(function($){
/* 5  */ 
/* 6  */    $('.comment-form .add_rating li').hover(function(){
/* 7  */         var index=$(this).index();
/* 8  */         var sibling=$(this).siblings();
/* 9  */             sibling.removeClass('active');
/* 10 */ 
/* 11 */         try
/* 12 */         {
/* 13 */ 
/* 14 */             index=parseInt(index);
/* 15 */ 
/* 16 */             for(i=0;i<=index;i++)
/* 17 */             {
/* 18 */                 $(this).parent().find('li:eq('+i+')').addClass('active');
/* 19 */             }
/* 20 */ 
/* 21 */             $(this).parents('.form-group').find('.comment_rate').val(index+1);
/* 22 */ 
/* 23 */         }catch(ex)
/* 24 */         {
/* 25 */             console.log(ex);
/* 26 */         }
/* 27 */    });
/* 28 */    
/* 29 */   jQuery('#tab-1420425776515-2-4').removeClass('wpb_ui-tabs-hide');
/* 30 */   jQuery("#tab-1420425776515-2-4").removeAttr("style");
/* 31 */ 
/* 32 */ });

;
/* bootstrap.js */

/* 1    */ /* ========================================================================
/* 2    *|  * Bootstrap: alert.js v3.1.1
/* 3    *|  * http://getbootstrap.com/javascript/#alerts
/* 4    *|  * ========================================================================
/* 5    *|  * Copyright 2011-2014 Twitter, Inc.
/* 6    *|  * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
/* 7    *|  * ======================================================================== */
/* 8    */ 
/* 9    */ 
/* 10   */ +function ($) {
/* 11   */   'use strict';
/* 12   */ 
/* 13   */   // ALERT CLASS DEFINITION
/* 14   */   // ======================
/* 15   */ 
/* 16   */   var dismiss = '[data-dismiss="alert"]'
/* 17   */   var Alert   = function (el) {
/* 18   */     $(el).on('click', dismiss, this.close)
/* 19   */   }
/* 20   */ 
/* 21   */   Alert.prototype.close = function (e) {
/* 22   */     var $this    = $(this)
/* 23   */     var selector = $this.attr('data-target')
/* 24   */ 
/* 25   */     if (!selector) {
/* 26   */       selector = $this.attr('href')
/* 27   */       selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
/* 28   */     }
/* 29   */ 
/* 30   */     var $parent = $(selector)
/* 31   */ 
/* 32   */     if (e) e.preventDefault()
/* 33   */ 
/* 34   */     if (!$parent.length) {
/* 35   */       $parent = $this.hasClass('alert') ? $this : $this.parent()
/* 36   */     }
/* 37   */ 
/* 38   */     $parent.trigger(e = $.Event('close.bs.alert'))
/* 39   */ 
/* 40   */     if (e.isDefaultPrevented()) return
/* 41   */ 
/* 42   */     $parent.removeClass('in')
/* 43   */ 
/* 44   */     function removeElement() {
/* 45   */       $parent.trigger('closed.bs.alert').remove()
/* 46   */     }
/* 47   */ 
/* 48   */     $.support.transition && $parent.hasClass('fade') ?
/* 49   */       $parent
/* 50   */         .one($.support.transition.end, removeElement)

/* bootstrap.js */

/* 51   */         .emulateTransitionEnd(150) :
/* 52   */       removeElement()
/* 53   */   }
/* 54   */ 
/* 55   */ 
/* 56   */   // ALERT PLUGIN DEFINITION
/* 57   */   // =======================
/* 58   */ 
/* 59   */   var old = $.fn.alert
/* 60   */ 
/* 61   */   $.fn.alert = function (option) {
/* 62   */     return this.each(function () {
/* 63   */       var $this = $(this)
/* 64   */       var data  = $this.data('bs.alert')
/* 65   */ 
/* 66   */       if (!data) $this.data('bs.alert', (data = new Alert(this)))
/* 67   */       if (typeof option == 'string') data[option].call($this)
/* 68   */     })
/* 69   */   }
/* 70   */ 
/* 71   */   $.fn.alert.Constructor = Alert
/* 72   */ 
/* 73   */ 
/* 74   */   // ALERT NO CONFLICT
/* 75   */   // =================
/* 76   */ 
/* 77   */   $.fn.alert.noConflict = function () {
/* 78   */     $.fn.alert = old
/* 79   */     return this
/* 80   */   }
/* 81   */ 
/* 82   */ 
/* 83   */   // ALERT DATA-API
/* 84   */   // ==============
/* 85   */ 
/* 86   */   $(document).on('click.bs.alert.data-api', dismiss, Alert.prototype.close)
/* 87   */ 
/* 88   */ }(jQuery);
/* 89   */ 
/* 90   */ /* ========================================================================
/* 91   *|  * Bootstrap: button.js v3.1.1
/* 92   *|  * http://getbootstrap.com/javascript/#buttons
/* 93   *|  * ========================================================================
/* 94   *|  * Copyright 2011-2014 Twitter, Inc.
/* 95   *|  * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
/* 96   *|  * ======================================================================== */
/* 97   */ 
/* 98   */ 
/* 99   */ +function ($) {
/* 100  */   'use strict';

/* bootstrap.js */

/* 101  */ 
/* 102  */   // BUTTON PUBLIC CLASS DEFINITION
/* 103  */   // ==============================
/* 104  */ 
/* 105  */   var Button = function (element, options) {
/* 106  */     this.$element  = $(element)
/* 107  */     this.options   = $.extend({}, Button.DEFAULTS, options)
/* 108  */     this.isLoading = false
/* 109  */   }
/* 110  */ 
/* 111  */   Button.DEFAULTS = {
/* 112  */     loadingText: 'loading...'
/* 113  */   }
/* 114  */ 
/* 115  */   Button.prototype.setState = function (state) {
/* 116  */     var d    = 'disabled'
/* 117  */     var $el  = this.$element
/* 118  */     var val  = $el.is('input') ? 'val' : 'html'
/* 119  */     var data = $el.data()
/* 120  */ 
/* 121  */     state = state + 'Text'
/* 122  */ 
/* 123  */     if (!data.resetText) $el.data('resetText', $el[val]())
/* 124  */ 
/* 125  */     $el[val](data[state] || this.options[state])
/* 126  */ 
/* 127  */     // push to event loop to allow forms to submit
/* 128  */     setTimeout($.proxy(function () {
/* 129  */       if (state == 'loadingText') {
/* 130  */         this.isLoading = true
/* 131  */         $el.addClass(d).attr(d, d)
/* 132  */       } else if (this.isLoading) {
/* 133  */         this.isLoading = false
/* 134  */         $el.removeClass(d).removeAttr(d)
/* 135  */       }
/* 136  */     }, this), 0)
/* 137  */   }
/* 138  */ 
/* 139  */   Button.prototype.toggle = function () {
/* 140  */     var changed = true
/* 141  */     var $parent = this.$element.closest('[data-toggle="buttons"]')
/* 142  */ 
/* 143  */     if ($parent.length) {
/* 144  */       var $input = this.$element.find('input')
/* 145  */       if ($input.prop('type') == 'radio') {
/* 146  */         if ($input.prop('checked') && this.$element.hasClass('active')) changed = false
/* 147  */         else $parent.find('.active').removeClass('active')
/* 148  */       }
/* 149  */       if (changed) $input.prop('checked', !this.$element.hasClass('active')).trigger('change')
/* 150  */     }

/* bootstrap.js */

/* 151  */ 
/* 152  */     if (changed) this.$element.toggleClass('active')
/* 153  */   }
/* 154  */ 
/* 155  */ 
/* 156  */   // BUTTON PLUGIN DEFINITION
/* 157  */   // ========================
/* 158  */ 
/* 159  */   var old = $.fn.button
/* 160  */ 
/* 161  */   $.fn.button = function (option) {
/* 162  */     return this.each(function () {
/* 163  */       var $this   = $(this)
/* 164  */       var data    = $this.data('bs.button')
/* 165  */       var options = typeof option == 'object' && option
/* 166  */ 
/* 167  */       if (!data) $this.data('bs.button', (data = new Button(this, options)))
/* 168  */ 
/* 169  */       if (option == 'toggle') data.toggle()
/* 170  */       else if (option) data.setState(option)
/* 171  */     })
/* 172  */   }
/* 173  */ 
/* 174  */   $.fn.button.Constructor = Button
/* 175  */ 
/* 176  */ 
/* 177  */   // BUTTON NO CONFLICT
/* 178  */   // ==================
/* 179  */ 
/* 180  */   $.fn.button.noConflict = function () {
/* 181  */     $.fn.button = old
/* 182  */     return this
/* 183  */   }
/* 184  */ 
/* 185  */ 
/* 186  */   // BUTTON DATA-API
/* 187  */   // ===============
/* 188  */ 
/* 189  */   $(document).on('click.bs.button.data-api', '[data-toggle^=button]', function (e) {
/* 190  */     var $btn = $(e.target)
/* 191  */     if (!$btn.hasClass('btn')) $btn = $btn.closest('.btn')
/* 192  */     $btn.button('toggle')
/* 193  */     e.preventDefault()
/* 194  */   })
/* 195  */ 
/* 196  */ }(jQuery);
/* 197  */ 
/* 198  */ /* ========================================================================
/* 199  *|  * Bootstrap: carousel.js v3.1.1
/* 200  *|  * http://getbootstrap.com/javascript/#carousel

/* bootstrap.js */

/* 201  *|  * ========================================================================
/* 202  *|  * Copyright 2011-2014 Twitter, Inc.
/* 203  *|  * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
/* 204  *|  * ======================================================================== */
/* 205  */ 
/* 206  */ 
/* 207  */ +function ($) {
/* 208  */   'use strict';
/* 209  */ 
/* 210  */   // CAROUSEL CLASS DEFINITION
/* 211  */   // =========================
/* 212  */ 
/* 213  */   var Carousel = function (element, options) {
/* 214  */     this.$element    = $(element)
/* 215  */     this.$indicators = this.$element.find('.carousel-indicators')
/* 216  */     this.options     = options
/* 217  */     this.paused      =
/* 218  */     this.sliding     =
/* 219  */     this.interval    =
/* 220  */     this.$active     =
/* 221  */     this.$items      = null
/* 222  */ 
/* 223  */     this.options.pause == 'hover' && this.$element
/* 224  */       .on('mouseenter', $.proxy(this.pause, this))
/* 225  */       .on('mouseleave', $.proxy(this.cycle, this))
/* 226  */   }
/* 227  */ 
/* 228  */   Carousel.DEFAULTS = {
/* 229  */     interval: 5000,
/* 230  */     pause: 'hover',
/* 231  */     wrap: true
/* 232  */   }
/* 233  */ 
/* 234  */   Carousel.prototype.cycle =  function (e) {
/* 235  */     e || (this.paused = false)
/* 236  */ 
/* 237  */     this.interval && clearInterval(this.interval)
/* 238  */ 
/* 239  */     this.options.interval
/* 240  */       && !this.paused
/* 241  */       && (this.interval = setInterval($.proxy(this.next, this), this.options.interval))
/* 242  */ 
/* 243  */     return this
/* 244  */   }
/* 245  */ 
/* 246  */   Carousel.prototype.getActiveIndex = function () {
/* 247  */     this.$active = this.$element.find('.item.active')
/* 248  */     this.$items  = this.$active.parent().children()
/* 249  */ 
/* 250  */     return this.$items.index(this.$active)

/* bootstrap.js */

/* 251  */   }
/* 252  */ 
/* 253  */   Carousel.prototype.to = function (pos) {
/* 254  */     var that        = this
/* 255  */     var activeIndex = this.getActiveIndex()
/* 256  */ 
/* 257  */     if (pos > (this.$items.length - 1) || pos < 0) return
/* 258  */ 
/* 259  */     if (this.sliding)       return this.$element.one('slid.bs.carousel', function () { that.to(pos) })
/* 260  */     if (activeIndex == pos) return this.pause().cycle()
/* 261  */ 
/* 262  */     return this.slide(pos > activeIndex ? 'next' : 'prev', $(this.$items[pos]))
/* 263  */   }
/* 264  */ 
/* 265  */   Carousel.prototype.pause = function (e) {
/* 266  */     e || (this.paused = true)
/* 267  */ 
/* 268  */     if (this.$element.find('.next, .prev').length && $.support.transition) {
/* 269  */       this.$element.trigger($.support.transition.end)
/* 270  */       this.cycle(true)
/* 271  */     }
/* 272  */ 
/* 273  */     this.interval = clearInterval(this.interval)
/* 274  */ 
/* 275  */     return this
/* 276  */   }
/* 277  */ 
/* 278  */   Carousel.prototype.next = function () {
/* 279  */     if (this.sliding) return
/* 280  */     return this.slide('next')
/* 281  */   }
/* 282  */ 
/* 283  */   Carousel.prototype.prev = function () {
/* 284  */     if (this.sliding) return
/* 285  */     return this.slide('prev')
/* 286  */   }
/* 287  */ 
/* 288  */   Carousel.prototype.slide = function (type, next) {
/* 289  */     var $active   = this.$element.find('.item.active')
/* 290  */     var $next     = next || $active[type]()
/* 291  */     var isCycling = this.interval
/* 292  */     var direction = type == 'next' ? 'left' : 'right'
/* 293  */     var fallback  = type == 'next' ? 'first' : 'last'
/* 294  */     var that      = this
/* 295  */ 
/* 296  */     if (!$next.length) {
/* 297  */       if (!this.options.wrap) return
/* 298  */       $next = this.$element.find('.item')[fallback]()
/* 299  */     }
/* 300  */ 

/* bootstrap.js */

/* 301  */     if ($next.hasClass('active')) return this.sliding = false
/* 302  */ 
/* 303  */     var e = $.Event('slide.bs.carousel', { relatedTarget: $next[0], direction: direction })
/* 304  */     this.$element.trigger(e)
/* 305  */     if (e.isDefaultPrevented()) return
/* 306  */ 
/* 307  */     this.sliding = true
/* 308  */ 
/* 309  */     isCycling && this.pause()
/* 310  */ 
/* 311  */     if (this.$indicators.length) {
/* 312  */       this.$indicators.find('.active').removeClass('active')
/* 313  */       this.$element.one('slid.bs.carousel', function () {
/* 314  */         var $nextIndicator = $(that.$indicators.children()[that.getActiveIndex()])
/* 315  */         $nextIndicator && $nextIndicator.addClass('active')
/* 316  */       })
/* 317  */     }
/* 318  */ 
/* 319  */     if ($.support.transition && this.$element.hasClass('slide')) {
/* 320  */       $next.addClass(type)
/* 321  */       $next[0].offsetWidth // force reflow
/* 322  */       $active.addClass(direction)
/* 323  */       $next.addClass(direction)
/* 324  */       $active
/* 325  */         .one($.support.transition.end, function () {
/* 326  */           $next.removeClass([type, direction].join(' ')).addClass('active')
/* 327  */           $active.removeClass(['active', direction].join(' '))
/* 328  */           that.sliding = false
/* 329  */           setTimeout(function () { that.$element.trigger('slid.bs.carousel') }, 0)
/* 330  */         })
/* 331  */         .emulateTransitionEnd($active.css('transition-duration').slice(0, -1) * 1000)
/* 332  */     } else {
/* 333  */       $active.removeClass('active')
/* 334  */       $next.addClass('active')
/* 335  */       this.sliding = false
/* 336  */       this.$element.trigger('slid.bs.carousel')
/* 337  */     }
/* 338  */ 
/* 339  */     isCycling && this.cycle()
/* 340  */ 
/* 341  */     return this
/* 342  */   }
/* 343  */ 
/* 344  */ 
/* 345  */   // CAROUSEL PLUGIN DEFINITION
/* 346  */   // ==========================
/* 347  */ 
/* 348  */   var old = $.fn.carousel
/* 349  */ 
/* 350  */   $.fn.carousel = function (option) {

/* bootstrap.js */

/* 351  */     return this.each(function () {
/* 352  */       var $this   = $(this)
/* 353  */       var data    = $this.data('bs.carousel')
/* 354  */       var options = $.extend({}, Carousel.DEFAULTS, $this.data(), typeof option == 'object' && option)
/* 355  */       var action  = typeof option == 'string' ? option : options.slide
/* 356  */ 
/* 357  */       if (!data) $this.data('bs.carousel', (data = new Carousel(this, options)))
/* 358  */       if (typeof option == 'number') data.to(option)
/* 359  */       else if (action) data[action]()
/* 360  */       else if (options.interval) data.pause().cycle()
/* 361  */     })
/* 362  */   }
/* 363  */ 
/* 364  */   $.fn.carousel.Constructor = Carousel
/* 365  */ 
/* 366  */ 
/* 367  */   // CAROUSEL NO CONFLICT
/* 368  */   // ====================
/* 369  */ 
/* 370  */   $.fn.carousel.noConflict = function () {
/* 371  */     $.fn.carousel = old
/* 372  */     return this
/* 373  */   }
/* 374  */ 
/* 375  */ 
/* 376  */   // CAROUSEL DATA-API
/* 377  */   // =================
/* 378  */ 
/* 379  */   $(document).on('click.bs.carousel.data-api', '[data-slide], [data-slide-to]', function (e) {
/* 380  */     var $this   = $(this), href
/* 381  */     var $target = $($this.attr('data-target') || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')) //strip for ie7
/* 382  */     var options = $.extend({}, $target.data(), $this.data())
/* 383  */     var slideIndex = $this.attr('data-slide-to')
/* 384  */     if (slideIndex) options.interval = false
/* 385  */ 
/* 386  */     $target.carousel(options)
/* 387  */ 
/* 388  */     if (slideIndex = $this.attr('data-slide-to')) {
/* 389  */       $target.data('bs.carousel').to(slideIndex)
/* 390  */     }
/* 391  */ 
/* 392  */     e.preventDefault()
/* 393  */   })
/* 394  */ 
/* 395  */   $(window).on('load', function () {
/* 396  */     $('[data-ride="carousel"]').each(function () {
/* 397  */       var $carousel = $(this)
/* 398  */       $carousel.carousel($carousel.data())
/* 399  */     })
/* 400  */   })

/* bootstrap.js */

/* 401  */ 
/* 402  */ }(jQuery);
/* 403  */ 
/* 404  */ /* ========================================================================
/* 405  *|  * Bootstrap: dropdown.js v3.1.1
/* 406  *|  * http://getbootstrap.com/javascript/#dropdowns
/* 407  *|  * ========================================================================
/* 408  *|  * Copyright 2011-2014 Twitter, Inc.
/* 409  *|  * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
/* 410  *|  * ======================================================================== */
/* 411  */ 
/* 412  */ 
/* 413  */ +function ($) {
/* 414  */   'use strict';
/* 415  */ 
/* 416  */   // DROPDOWN CLASS DEFINITION
/* 417  */   // =========================
/* 418  */ 
/* 419  */   var backdrop = '.dropdown-backdrop'
/* 420  */   var toggle   = '[data-toggle=dropdown]'
/* 421  */   var Dropdown = function (element) {
/* 422  */     $(element).on('click.bs.dropdown', this.toggle)
/* 423  */   }
/* 424  */ 
/* 425  */   Dropdown.prototype.toggle = function (e) {
/* 426  */     var $this = $(this)
/* 427  */ 
/* 428  */     if ($this.is('.disabled, :disabled')) return
/* 429  */ 
/* 430  */     var $parent  = getParent($this)
/* 431  */     var isActive = $parent.hasClass('open')
/* 432  */ 
/* 433  */     clearMenus()
/* 434  */ 
/* 435  */     if (!isActive) {
/* 436  */       if ('ontouchstart' in document.documentElement && !$parent.closest('.navbar-nav').length) {
/* 437  */         // if mobile we use a backdrop because click events don't delegate
/* 438  */         $('<div class="dropdown-backdrop"/>').insertAfter($(this)).on('click', clearMenus)
/* 439  */       }
/* 440  */ 
/* 441  */       var relatedTarget = { relatedTarget: this }
/* 442  */       $parent.trigger(e = $.Event('show.bs.dropdown', relatedTarget))
/* 443  */ 
/* 444  */       if (e.isDefaultPrevented()) return
/* 445  */ 
/* 446  */       $parent
/* 447  */         .toggleClass('open')
/* 448  */         .trigger('shown.bs.dropdown', relatedTarget)
/* 449  */ 
/* 450  */       $this.focus()

/* bootstrap.js */

/* 451  */     }
/* 452  */ 
/* 453  */     return false
/* 454  */   }
/* 455  */ 
/* 456  */   Dropdown.prototype.keydown = function (e) {
/* 457  */     if (!/(38|40|27)/.test(e.keyCode)) return
/* 458  */ 
/* 459  */     var $this = $(this)
/* 460  */ 
/* 461  */     e.preventDefault()
/* 462  */     e.stopPropagation()
/* 463  */ 
/* 464  */     if ($this.is('.disabled, :disabled')) return
/* 465  */ 
/* 466  */     var $parent  = getParent($this)
/* 467  */     var isActive = $parent.hasClass('open')
/* 468  */ 
/* 469  */     if (!isActive || (isActive && e.keyCode == 27)) {
/* 470  */       if (e.which == 27) $parent.find(toggle).focus()
/* 471  */       return $this.click()
/* 472  */     }
/* 473  */ 
/* 474  */     var desc = ' li:not(.divider):visible a'
/* 475  */     var $items = $parent.find('[role=menu]' + desc + ', [role=listbox]' + desc)
/* 476  */ 
/* 477  */     if (!$items.length) return
/* 478  */ 
/* 479  */     var index = $items.index($items.filter(':focus'))
/* 480  */ 
/* 481  */     if (e.keyCode == 38 && index > 0)                 index--                        // up
/* 482  */     if (e.keyCode == 40 && index < $items.length - 1) index++                        // down
/* 483  */     if (!~index)                                      index = 0
/* 484  */ 
/* 485  */     $items.eq(index).focus()
/* 486  */   }
/* 487  */ 
/* 488  */   function clearMenus(e) {
/* 489  */     $(backdrop).remove()
/* 490  */     $(toggle).each(function () {
/* 491  */       var $parent = getParent($(this))
/* 492  */       var relatedTarget = { relatedTarget: this }
/* 493  */       if (!$parent.hasClass('open')) return
/* 494  */       $parent.trigger(e = $.Event('hide.bs.dropdown', relatedTarget))
/* 495  */       if (e.isDefaultPrevented()) return
/* 496  */       $parent.removeClass('open').trigger('hidden.bs.dropdown', relatedTarget)
/* 497  */     })
/* 498  */   }
/* 499  */ 
/* 500  */   function getParent($this) {

/* bootstrap.js */

/* 501  */     var selector = $this.attr('data-target')
/* 502  */ 
/* 503  */     if (!selector) {
/* 504  */       selector = $this.attr('href')
/* 505  */       selector = selector && /#[A-Za-z]/.test(selector) && selector.replace(/.*(?=#[^\s]*$)/, '') //strip for ie7
/* 506  */     }
/* 507  */ 
/* 508  */     var $parent = selector && $(selector)
/* 509  */ 
/* 510  */     return $parent && $parent.length ? $parent : $this.parent()
/* 511  */   }
/* 512  */ 
/* 513  */ 
/* 514  */   // DROPDOWN PLUGIN DEFINITION
/* 515  */   // ==========================
/* 516  */ 
/* 517  */   var old = $.fn.dropdown
/* 518  */ 
/* 519  */   $.fn.dropdown = function (option) {
/* 520  */     return this.each(function () {
/* 521  */       var $this = $(this)
/* 522  */       var data  = $this.data('bs.dropdown')
/* 523  */ 
/* 524  */       if (!data) $this.data('bs.dropdown', (data = new Dropdown(this)))
/* 525  */       if (typeof option == 'string') data[option].call($this)
/* 526  */     })
/* 527  */   }
/* 528  */ 
/* 529  */   $.fn.dropdown.Constructor = Dropdown
/* 530  */ 
/* 531  */ 
/* 532  */   // DROPDOWN NO CONFLICT
/* 533  */   // ====================
/* 534  */ 
/* 535  */   $.fn.dropdown.noConflict = function () {
/* 536  */     $.fn.dropdown = old
/* 537  */     return this
/* 538  */   }
/* 539  */ 
/* 540  */ 
/* 541  */   // APPLY TO STANDARD DROPDOWN ELEMENTS
/* 542  */   // ===================================
/* 543  */ 
/* 544  */   $(document)
/* 545  */     .on('click.bs.dropdown.data-api', clearMenus)
/* 546  */     .on('click.bs.dropdown.data-api', '.dropdown form', function (e) { e.stopPropagation() })
/* 547  */     .on('click.bs.dropdown.data-api', toggle, Dropdown.prototype.toggle)
/* 548  */     .on('keydown.bs.dropdown.data-api', toggle + ', [role=menu], [role=listbox]', Dropdown.prototype.keydown)
/* 549  */ 
/* 550  */ }(jQuery);

/* bootstrap.js */

/* 551  */ 
/* 552  */ /* ========================================================================
/* 553  *|  * Bootstrap: modal.js v3.1.1
/* 554  *|  * http://getbootstrap.com/javascript/#modals
/* 555  *|  * ========================================================================
/* 556  *|  * Copyright 2011-2014 Twitter, Inc.
/* 557  *|  * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
/* 558  *|  * ======================================================================== */
/* 559  */ 
/* 560  */ 
/* 561  */ +function ($) {
/* 562  */   'use strict';
/* 563  */ 
/* 564  */   // MODAL CLASS DEFINITION
/* 565  */   // ======================
/* 566  */ 
/* 567  */   var Modal = function (element, options) {
/* 568  */     this.options   = options
/* 569  */     this.$element  = $(element)
/* 570  */     this.$backdrop =
/* 571  */     this.isShown   = null
/* 572  */ 
/* 573  */     if (this.options.remote) {
/* 574  */       this.$element
/* 575  */         .find('.modal-content')
/* 576  */         .load(this.options.remote, $.proxy(function () {
/* 577  */           this.$element.trigger('loaded.bs.modal')
/* 578  */         }, this))
/* 579  */     }
/* 580  */   }
/* 581  */ 
/* 582  */   Modal.DEFAULTS = {
/* 583  */     backdrop: true,
/* 584  */     keyboard: true,
/* 585  */     show: true
/* 586  */   }
/* 587  */ 
/* 588  */   Modal.prototype.toggle = function (_relatedTarget) {
/* 589  */     return this[!this.isShown ? 'show' : 'hide'](_relatedTarget)
/* 590  */   }
/* 591  */ 
/* 592  */   Modal.prototype.show = function (_relatedTarget) {
/* 593  */     var that = this
/* 594  */     var e    = $.Event('show.bs.modal', { relatedTarget: _relatedTarget })
/* 595  */ 
/* 596  */     this.$element.trigger(e)
/* 597  */ 
/* 598  */     if (this.isShown || e.isDefaultPrevented()) return
/* 599  */ 
/* 600  */     this.isShown = true

/* bootstrap.js */

/* 601  */ 
/* 602  */     this.escape()
/* 603  */ 
/* 604  */     this.$element.on('click.dismiss.bs.modal', '[data-dismiss="modal"]', $.proxy(this.hide, this))
/* 605  */ 
/* 606  */     this.backdrop(function () {
/* 607  */       var transition = $.support.transition && that.$element.hasClass('fade')
/* 608  */ 
/* 609  */       if (!that.$element.parent().length) {
/* 610  */         that.$element.appendTo(document.body) // don't move modals dom position
/* 611  */       }
/* 612  */ 
/* 613  */       that.$element
/* 614  */         .show()
/* 615  */         .scrollTop(0)
/* 616  */ 
/* 617  */       if (transition) {
/* 618  */         that.$element[0].offsetWidth // force reflow
/* 619  */       }
/* 620  */ 
/* 621  */       that.$element
/* 622  */         .addClass('in')
/* 623  */         .attr('aria-hidden', false)
/* 624  */ 
/* 625  */       that.enforceFocus()
/* 626  */ 
/* 627  */       var e = $.Event('shown.bs.modal', { relatedTarget: _relatedTarget })
/* 628  */ 
/* 629  */       transition ?
/* 630  */         that.$element.find('.modal-dialog') // wait for modal to slide in
/* 631  */           .one($.support.transition.end, function () {
/* 632  */             that.$element.focus().trigger(e)
/* 633  */           })
/* 634  */           .emulateTransitionEnd(300) :
/* 635  */         that.$element.focus().trigger(e)
/* 636  */     })
/* 637  */   }
/* 638  */ 
/* 639  */   Modal.prototype.hide = function (e) {
/* 640  */     if (e) e.preventDefault()
/* 641  */ 
/* 642  */     e = $.Event('hide.bs.modal')
/* 643  */ 
/* 644  */     this.$element.trigger(e)
/* 645  */ 
/* 646  */     if (!this.isShown || e.isDefaultPrevented()) return
/* 647  */ 
/* 648  */     this.isShown = false
/* 649  */ 
/* 650  */     this.escape()

/* bootstrap.js */

/* 651  */ 
/* 652  */     $(document).off('focusin.bs.modal')
/* 653  */ 
/* 654  */     this.$element
/* 655  */       .removeClass('in')
/* 656  */       .attr('aria-hidden', true)
/* 657  */       .off('click.dismiss.bs.modal')
/* 658  */ 
/* 659  */     $.support.transition && this.$element.hasClass('fade') ?
/* 660  */       this.$element
/* 661  */         .one($.support.transition.end, $.proxy(this.hideModal, this))
/* 662  */         .emulateTransitionEnd(300) :
/* 663  */       this.hideModal()
/* 664  */   }
/* 665  */ 
/* 666  */   Modal.prototype.enforceFocus = function () {
/* 667  */     $(document)
/* 668  */       .off('focusin.bs.modal') // guard against infinite focus loop
/* 669  */       .on('focusin.bs.modal', $.proxy(function (e) {
/* 670  */         if (this.$element[0] !== e.target && !this.$element.has(e.target).length) {
/* 671  */           this.$element.focus()
/* 672  */         }
/* 673  */       }, this))
/* 674  */   }
/* 675  */ 
/* 676  */   Modal.prototype.escape = function () {
/* 677  */     if (this.isShown && this.options.keyboard) {
/* 678  */       this.$element.on('keyup.dismiss.bs.modal', $.proxy(function (e) {
/* 679  */         e.which == 27 && this.hide()
/* 680  */       }, this))
/* 681  */     } else if (!this.isShown) {
/* 682  */       this.$element.off('keyup.dismiss.bs.modal')
/* 683  */     }
/* 684  */   }
/* 685  */ 
/* 686  */   Modal.prototype.hideModal = function () {
/* 687  */     var that = this
/* 688  */     this.$element.hide()
/* 689  */     this.backdrop(function () {
/* 690  */       that.removeBackdrop()
/* 691  */       that.$element.trigger('hidden.bs.modal')
/* 692  */     })
/* 693  */   }
/* 694  */ 
/* 695  */   Modal.prototype.removeBackdrop = function () {
/* 696  */     this.$backdrop && this.$backdrop.remove()
/* 697  */     this.$backdrop = null
/* 698  */   }
/* 699  */ 
/* 700  */   Modal.prototype.backdrop = function (callback) {

/* bootstrap.js */

/* 701  */     var animate = this.$element.hasClass('fade') ? 'fade' : ''
/* 702  */ 
/* 703  */     if (this.isShown && this.options.backdrop) {
/* 704  */       var doAnimate = $.support.transition && animate
/* 705  */ 
/* 706  */       this.$backdrop = $('<div class="modal-backdrop ' + animate + '" />')
/* 707  */         .appendTo(document.body)
/* 708  */ 
/* 709  */       this.$element.on('click.dismiss.bs.modal', $.proxy(function (e) {
/* 710  */         if (e.target !== e.currentTarget) return
/* 711  */         this.options.backdrop == 'static'
/* 712  */           ? this.$element[0].focus.call(this.$element[0])
/* 713  */           : this.hide.call(this)
/* 714  */       }, this))
/* 715  */ 
/* 716  */       if (doAnimate) this.$backdrop[0].offsetWidth // force reflow
/* 717  */ 
/* 718  */       this.$backdrop.addClass('in')
/* 719  */ 
/* 720  */       if (!callback) return
/* 721  */ 
/* 722  */       doAnimate ?
/* 723  */         this.$backdrop
/* 724  */           .one($.support.transition.end, callback)
/* 725  */           .emulateTransitionEnd(150) :
/* 726  */         callback()
/* 727  */ 
/* 728  */     } else if (!this.isShown && this.$backdrop) {
/* 729  */       this.$backdrop.removeClass('in')
/* 730  */ 
/* 731  */       $.support.transition && this.$element.hasClass('fade') ?
/* 732  */         this.$backdrop
/* 733  */           .one($.support.transition.end, callback)
/* 734  */           .emulateTransitionEnd(150) :
/* 735  */         callback()
/* 736  */ 
/* 737  */     } else if (callback) {
/* 738  */       callback()
/* 739  */     }
/* 740  */   }
/* 741  */ 
/* 742  */ 
/* 743  */   // MODAL PLUGIN DEFINITION
/* 744  */   // =======================
/* 745  */ 
/* 746  */   var old = $.fn.modal
/* 747  */ 
/* 748  */   $.fn.modal = function (option, _relatedTarget) {
/* 749  */     return this.each(function () {
/* 750  */       var $this   = $(this)

/* bootstrap.js */

/* 751  */       var data    = $this.data('bs.modal')
/* 752  */       var options = $.extend({}, Modal.DEFAULTS, $this.data(), typeof option == 'object' && option)
/* 753  */ 
/* 754  */       if (!data) $this.data('bs.modal', (data = new Modal(this, options)))
/* 755  */       if (typeof option == 'string') data[option](_relatedTarget)
/* 756  */       else if (options.show) data.show(_relatedTarget)
/* 757  */     })
/* 758  */   }
/* 759  */ 
/* 760  */   $.fn.modal.Constructor = Modal
/* 761  */ 
/* 762  */ 
/* 763  */   // MODAL NO CONFLICT
/* 764  */   // =================
/* 765  */ 
/* 766  */   $.fn.modal.noConflict = function () {
/* 767  */     $.fn.modal = old
/* 768  */     return this
/* 769  */   }
/* 770  */ 
/* 771  */ 
/* 772  */   // MODAL DATA-API
/* 773  */   // ==============
/* 774  */ 
/* 775  */   $(document).on('click.bs.modal.data-api', '[data-toggle="modal"]', function (e) {
/* 776  */     var $this   = $(this)
/* 777  */     var href    = $this.attr('href')
/* 778  */     var $target = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, ''))) //strip for ie7
/* 779  */     var option  = $target.data('bs.modal') ? 'toggle' : $.extend({ remote: !/#/.test(href) && href }, $target.data(), $this.data())
/* 780  */ 
/* 781  */     if ($this.is('a')) e.preventDefault()
/* 782  */ 
/* 783  */     $target
/* 784  */       .modal(option, this)
/* 785  */       .one('hide', function () {
/* 786  */         $this.is(':visible') && $this.focus()
/* 787  */       })
/* 788  */   })
/* 789  */ 
/* 790  */   $(document)
/* 791  */     .on('show.bs.modal', '.modal', function () { $(document.body).addClass('modal-open') })
/* 792  */     .on('hidden.bs.modal', '.modal', function () { $(document.body).removeClass('modal-open') })
/* 793  */ 
/* 794  */ }(jQuery);
/* 795  */ 
/* 796  */ /* ========================================================================
/* 797  *|  * Bootstrap: tooltip.js v3.1.1
/* 798  *|  * http://getbootstrap.com/javascript/#tooltip
/* 799  *|  * Inspired by the original jQuery.tipsy by Jason Frame
/* 800  *|  * ========================================================================

/* bootstrap.js */

/* 801  *|  * Copyright 2011-2014 Twitter, Inc.
/* 802  *|  * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
/* 803  *|  * ======================================================================== */
/* 804  */ 
/* 805  */ 
/* 806  */ +function ($) {
/* 807  */   'use strict';
/* 808  */ 
/* 809  */   // TOOLTIP PUBLIC CLASS DEFINITION
/* 810  */   // ===============================
/* 811  */ 
/* 812  */   var Tooltip = function (element, options) {
/* 813  */     this.type       =
/* 814  */     this.options    =
/* 815  */     this.enabled    =
/* 816  */     this.timeout    =
/* 817  */     this.hoverState =
/* 818  */     this.$element   = null
/* 819  */ 
/* 820  */     this.init('tooltip', element, options)
/* 821  */   }
/* 822  */ 
/* 823  */   Tooltip.DEFAULTS = {
/* 824  */     animation: true,
/* 825  */     placement: 'top',
/* 826  */     selector: false,
/* 827  */     template: '<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
/* 828  */     trigger: 'hover focus',
/* 829  */     title: '',
/* 830  */     delay: 0,
/* 831  */     html: false,
/* 832  */     container: false
/* 833  */   }
/* 834  */ 
/* 835  */   Tooltip.prototype.init = function (type, element, options) {
/* 836  */     this.enabled  = true
/* 837  */     this.type     = type
/* 838  */     this.$element = $(element)
/* 839  */     this.options  = this.getOptions(options)
/* 840  */ 
/* 841  */     var triggers = this.options.trigger.split(' ')
/* 842  */ 
/* 843  */     for (var i = triggers.length; i--;) {
/* 844  */       var trigger = triggers[i]
/* 845  */ 
/* 846  */       if (trigger == 'click') {
/* 847  */         this.$element.on('click.' + this.type, this.options.selector, $.proxy(this.toggle, this))
/* 848  */       } else if (trigger != 'manual') {
/* 849  */         var eventIn  = trigger == 'hover' ? 'mouseenter' : 'focusin'
/* 850  */         var eventOut = trigger == 'hover' ? 'mouseleave' : 'focusout'

/* bootstrap.js */

/* 851  */ 
/* 852  */         this.$element.on(eventIn  + '.' + this.type, this.options.selector, $.proxy(this.enter, this))
/* 853  */         this.$element.on(eventOut + '.' + this.type, this.options.selector, $.proxy(this.leave, this))
/* 854  */       }
/* 855  */     }
/* 856  */ 
/* 857  */     this.options.selector ?
/* 858  */       (this._options = $.extend({}, this.options, { trigger: 'manual', selector: '' })) :
/* 859  */       this.fixTitle()
/* 860  */   }
/* 861  */ 
/* 862  */   Tooltip.prototype.getDefaults = function () {
/* 863  */     return Tooltip.DEFAULTS
/* 864  */   }
/* 865  */ 
/* 866  */   Tooltip.prototype.getOptions = function (options) {
/* 867  */     options = $.extend({}, this.getDefaults(), this.$element.data(), options)
/* 868  */ 
/* 869  */     if (options.delay && typeof options.delay == 'number') {
/* 870  */       options.delay = {
/* 871  */         show: options.delay,
/* 872  */         hide: options.delay
/* 873  */       }
/* 874  */     }
/* 875  */ 
/* 876  */     return options
/* 877  */   }
/* 878  */ 
/* 879  */   Tooltip.prototype.getDelegateOptions = function () {
/* 880  */     var options  = {}
/* 881  */     var defaults = this.getDefaults()
/* 882  */ 
/* 883  */     this._options && $.each(this._options, function (key, value) {
/* 884  */       if (defaults[key] != value) options[key] = value
/* 885  */     })
/* 886  */ 
/* 887  */     return options
/* 888  */   }
/* 889  */ 
/* 890  */   Tooltip.prototype.enter = function (obj) {
/* 891  */     var self = obj instanceof this.constructor ?
/* 892  */       obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type)
/* 893  */ 
/* 894  */     clearTimeout(self.timeout)
/* 895  */ 
/* 896  */     self.hoverState = 'in'
/* 897  */ 
/* 898  */     if (!self.options.delay || !self.options.delay.show) return self.show()
/* 899  */ 
/* 900  */     self.timeout = setTimeout(function () {

/* bootstrap.js */

/* 901  */       if (self.hoverState == 'in') self.show()
/* 902  */     }, self.options.delay.show)
/* 903  */   }
/* 904  */ 
/* 905  */   Tooltip.prototype.leave = function (obj) {
/* 906  */     var self = obj instanceof this.constructor ?
/* 907  */       obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type)
/* 908  */ 
/* 909  */     clearTimeout(self.timeout)
/* 910  */ 
/* 911  */     self.hoverState = 'out'
/* 912  */ 
/* 913  */     if (!self.options.delay || !self.options.delay.hide) return self.hide()
/* 914  */ 
/* 915  */     self.timeout = setTimeout(function () {
/* 916  */       if (self.hoverState == 'out') self.hide()
/* 917  */     }, self.options.delay.hide)
/* 918  */   }
/* 919  */ 
/* 920  */   Tooltip.prototype.show = function () {
/* 921  */     var e = $.Event('show.bs.' + this.type)
/* 922  */ 
/* 923  */     if (this.hasContent() && this.enabled) {
/* 924  */       this.$element.trigger(e)
/* 925  */ 
/* 926  */       if (e.isDefaultPrevented()) return
/* 927  */       var that = this;
/* 928  */ 
/* 929  */       var $tip = this.tip()
/* 930  */ 
/* 931  */       this.setContent()
/* 932  */ 
/* 933  */       if (this.options.animation) $tip.addClass('fade')
/* 934  */ 
/* 935  */       var placement = typeof this.options.placement == 'function' ?
/* 936  */         this.options.placement.call(this, $tip[0], this.$element[0]) :
/* 937  */         this.options.placement
/* 938  */ 
/* 939  */       var autoToken = /\s?auto?\s?/i
/* 940  */       var autoPlace = autoToken.test(placement)
/* 941  */       if (autoPlace) placement = placement.replace(autoToken, '') || 'top'
/* 942  */ 
/* 943  */       $tip
/* 944  */         .detach()
/* 945  */         .css({ top: 0, left: 0, display: 'block' })
/* 946  */         .addClass(placement)
/* 947  */ 
/* 948  */       this.options.container ? $tip.appendTo(this.options.container) : $tip.insertAfter(this.$element)
/* 949  */ 
/* 950  */       var pos          = this.getPosition()

/* bootstrap.js */

/* 951  */       var actualWidth  = $tip[0].offsetWidth
/* 952  */       var actualHeight = $tip[0].offsetHeight
/* 953  */ 
/* 954  */       if (autoPlace) {
/* 955  */         var $parent = this.$element.parent()
/* 956  */ 
/* 957  */         var orgPlacement = placement
/* 958  */         var docScroll    = document.documentElement.scrollTop || document.body.scrollTop
/* 959  */         var parentWidth  = this.options.container == 'body' ? window.innerWidth  : $parent.outerWidth()
/* 960  */         var parentHeight = this.options.container == 'body' ? window.innerHeight : $parent.outerHeight()
/* 961  */         var parentLeft   = this.options.container == 'body' ? 0 : $parent.offset().left
/* 962  */ 
/* 963  */         placement = placement == 'bottom' && pos.top   + pos.height  + actualHeight - docScroll > parentHeight  ? 'top'    :
/* 964  */                     placement == 'top'    && pos.top   - docScroll   - actualHeight < 0                         ? 'bottom' :
/* 965  */                     placement == 'right'  && pos.right + actualWidth > parentWidth                              ? 'left'   :
/* 966  */                     placement == 'left'   && pos.left  - actualWidth < parentLeft                               ? 'right'  :
/* 967  */                     placement
/* 968  */ 
/* 969  */         $tip
/* 970  */           .removeClass(orgPlacement)
/* 971  */           .addClass(placement)
/* 972  */       }
/* 973  */ 
/* 974  */       var calculatedOffset = this.getCalculatedOffset(placement, pos, actualWidth, actualHeight)
/* 975  */ 
/* 976  */       this.applyPlacement(calculatedOffset, placement)
/* 977  */       this.hoverState = null
/* 978  */ 
/* 979  */       var complete = function() {
/* 980  */         that.$element.trigger('shown.bs.' + that.type)
/* 981  */       }
/* 982  */ 
/* 983  */       $.support.transition && this.$tip.hasClass('fade') ?
/* 984  */         $tip
/* 985  */           .one($.support.transition.end, complete)
/* 986  */           .emulateTransitionEnd(150) :
/* 987  */         complete()
/* 988  */     }
/* 989  */   }
/* 990  */ 
/* 991  */   Tooltip.prototype.applyPlacement = function (offset, placement) {
/* 992  */     var replace
/* 993  */     var $tip   = this.tip()
/* 994  */     var width  = $tip[0].offsetWidth
/* 995  */     var height = $tip[0].offsetHeight
/* 996  */ 
/* 997  */     // manually read margins because getBoundingClientRect includes difference
/* 998  */     var marginTop = parseInt($tip.css('margin-top'), 10)
/* 999  */     var marginLeft = parseInt($tip.css('margin-left'), 10)
/* 1000 */ 

/* bootstrap.js */

/* 1001 */     // we must check for NaN for ie 8/9
/* 1002 */     if (isNaN(marginTop))  marginTop  = 0
/* 1003 */     if (isNaN(marginLeft)) marginLeft = 0
/* 1004 */ 
/* 1005 */     offset.top  = offset.top  + marginTop
/* 1006 */     offset.left = offset.left + marginLeft
/* 1007 */ 
/* 1008 */     // $.fn.offset doesn't round pixel values
/* 1009 */     // so we use setOffset directly with our own function B-0
/* 1010 */     $.offset.setOffset($tip[0], $.extend({
/* 1011 */       using: function (props) {
/* 1012 */         $tip.css({
/* 1013 */           top: Math.round(props.top),
/* 1014 */           left: Math.round(props.left)
/* 1015 */         })
/* 1016 */       }
/* 1017 */     }, offset), 0)
/* 1018 */ 
/* 1019 */     $tip.addClass('in')
/* 1020 */ 
/* 1021 */     // check to see if placing tip in new offset caused the tip to resize itself
/* 1022 */     var actualWidth  = $tip[0].offsetWidth
/* 1023 */     var actualHeight = $tip[0].offsetHeight
/* 1024 */ 
/* 1025 */     if (placement == 'top' && actualHeight != height) {
/* 1026 */       replace = true
/* 1027 */       offset.top = offset.top + height - actualHeight
/* 1028 */     }
/* 1029 */ 
/* 1030 */     if (/bottom|top/.test(placement)) {
/* 1031 */       var delta = 0
/* 1032 */ 
/* 1033 */       if (offset.left < 0) {
/* 1034 */         delta       = offset.left * -2
/* 1035 */         offset.left = 0
/* 1036 */ 
/* 1037 */         $tip.offset(offset)
/* 1038 */ 
/* 1039 */         actualWidth  = $tip[0].offsetWidth
/* 1040 */         actualHeight = $tip[0].offsetHeight
/* 1041 */       }
/* 1042 */ 
/* 1043 */       this.replaceArrow(delta - width + actualWidth, actualWidth, 'left')
/* 1044 */     } else {
/* 1045 */       this.replaceArrow(actualHeight - height, actualHeight, 'top')
/* 1046 */     }
/* 1047 */ 
/* 1048 */     if (replace) $tip.offset(offset)
/* 1049 */   }
/* 1050 */ 

/* bootstrap.js */

/* 1051 */   Tooltip.prototype.replaceArrow = function (delta, dimension, position) {
/* 1052 */     this.arrow().css(position, delta ? (50 * (1 - delta / dimension) + '%') : '')
/* 1053 */   }
/* 1054 */ 
/* 1055 */   Tooltip.prototype.setContent = function () {
/* 1056 */     var $tip  = this.tip()
/* 1057 */     var title = this.getTitle()
/* 1058 */ 
/* 1059 */     $tip.find('.tooltip-inner')[this.options.html ? 'html' : 'text'](title)
/* 1060 */     $tip.removeClass('fade in top bottom left right')
/* 1061 */   }
/* 1062 */ 
/* 1063 */   Tooltip.prototype.hide = function () {
/* 1064 */     var that = this
/* 1065 */     var $tip = this.tip()
/* 1066 */     var e    = $.Event('hide.bs.' + this.type)
/* 1067 */ 
/* 1068 */     function complete() {
/* 1069 */       if (that.hoverState != 'in') $tip.detach()
/* 1070 */       that.$element.trigger('hidden.bs.' + that.type)
/* 1071 */     }
/* 1072 */ 
/* 1073 */     this.$element.trigger(e)
/* 1074 */ 
/* 1075 */     if (e.isDefaultPrevented()) return
/* 1076 */ 
/* 1077 */     $tip.removeClass('in')
/* 1078 */ 
/* 1079 */     $.support.transition && this.$tip.hasClass('fade') ?
/* 1080 */       $tip
/* 1081 */         .one($.support.transition.end, complete)
/* 1082 */         .emulateTransitionEnd(150) :
/* 1083 */       complete()
/* 1084 */ 
/* 1085 */     this.hoverState = null
/* 1086 */ 
/* 1087 */     return this
/* 1088 */   }
/* 1089 */ 
/* 1090 */   Tooltip.prototype.fixTitle = function () {
/* 1091 */     var $e = this.$element
/* 1092 */     if ($e.attr('title') || typeof($e.attr('data-original-title')) != 'string') {
/* 1093 */       $e.attr('data-original-title', $e.attr('title') || '').attr('title', '')
/* 1094 */     }
/* 1095 */   }
/* 1096 */ 
/* 1097 */   Tooltip.prototype.hasContent = function () {
/* 1098 */     return this.getTitle()
/* 1099 */   }
/* 1100 */ 

/* bootstrap.js */

/* 1101 */   Tooltip.prototype.getPosition = function () {
/* 1102 */     var el = this.$element[0]
/* 1103 */     return $.extend({}, (typeof el.getBoundingClientRect == 'function') ? el.getBoundingClientRect() : {
/* 1104 */       width: el.offsetWidth,
/* 1105 */       height: el.offsetHeight
/* 1106 */     }, this.$element.offset())
/* 1107 */   }
/* 1108 */ 
/* 1109 */   Tooltip.prototype.getCalculatedOffset = function (placement, pos, actualWidth, actualHeight) {
/* 1110 */     return placement == 'bottom' ? { top: pos.top + pos.height,   left: pos.left + pos.width / 2 - actualWidth / 2  } :
/* 1111 */            placement == 'top'    ? { top: pos.top - actualHeight, left: pos.left + pos.width / 2 - actualWidth / 2  } :
/* 1112 */            placement == 'left'   ? { top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left - actualWidth } :
/* 1113 */         /* placement == 'right' */ { top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left + pos.width   }
/* 1114 */   }
/* 1115 */ 
/* 1116 */   Tooltip.prototype.getTitle = function () {
/* 1117 */     var title
/* 1118 */     var $e = this.$element
/* 1119 */     var o  = this.options
/* 1120 */ 
/* 1121 */     title = $e.attr('data-original-title')
/* 1122 */       || (typeof o.title == 'function' ? o.title.call($e[0]) :  o.title)
/* 1123 */ 
/* 1124 */     return title
/* 1125 */   }
/* 1126 */ 
/* 1127 */   Tooltip.prototype.tip = function () {
/* 1128 */     return this.$tip = this.$tip || $(this.options.template)
/* 1129 */   }
/* 1130 */ 
/* 1131 */   Tooltip.prototype.arrow = function () {
/* 1132 */     return this.$arrow = this.$arrow || this.tip().find('.tooltip-arrow')
/* 1133 */   }
/* 1134 */ 
/* 1135 */   Tooltip.prototype.validate = function () {
/* 1136 */     if (!this.$element[0].parentNode) {
/* 1137 */       this.hide()
/* 1138 */       this.$element = null
/* 1139 */       this.options  = null
/* 1140 */     }
/* 1141 */   }
/* 1142 */ 
/* 1143 */   Tooltip.prototype.enable = function () {
/* 1144 */     this.enabled = true
/* 1145 */   }
/* 1146 */ 
/* 1147 */   Tooltip.prototype.disable = function () {
/* 1148 */     this.enabled = false
/* 1149 */   }
/* 1150 */ 

/* bootstrap.js */

/* 1151 */   Tooltip.prototype.toggleEnabled = function () {
/* 1152 */     this.enabled = !this.enabled
/* 1153 */   }
/* 1154 */ 
/* 1155 */   Tooltip.prototype.toggle = function (e) {
/* 1156 */     var self = e ? $(e.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type) : this
/* 1157 */     self.tip().hasClass('in') ? self.leave(self) : self.enter(self)
/* 1158 */   }
/* 1159 */ 
/* 1160 */   Tooltip.prototype.destroy = function () {
/* 1161 */     clearTimeout(this.timeout)
/* 1162 */     this.hide().$element.off('.' + this.type).removeData('bs.' + this.type)
/* 1163 */   }
/* 1164 */ 
/* 1165 */ 
/* 1166 */   // TOOLTIP PLUGIN DEFINITION
/* 1167 */   // =========================
/* 1168 */ 
/* 1169 */   var old = $.fn.tooltip
/* 1170 */ 
/* 1171 */   $.fn.tooltip = function (option) {
/* 1172 */     return this.each(function () {
/* 1173 */       var $this   = $(this)
/* 1174 */       var data    = $this.data('bs.tooltip')
/* 1175 */       var options = typeof option == 'object' && option
/* 1176 */ 
/* 1177 */       if (!data && option == 'destroy') return
/* 1178 */       if (!data) $this.data('bs.tooltip', (data = new Tooltip(this, options)))
/* 1179 */       if (typeof option == 'string') data[option]()
/* 1180 */     })
/* 1181 */   }
/* 1182 */ 
/* 1183 */   $.fn.tooltip.Constructor = Tooltip
/* 1184 */ 
/* 1185 */ 
/* 1186 */   // TOOLTIP NO CONFLICT
/* 1187 */   // ===================
/* 1188 */ 
/* 1189 */   $.fn.tooltip.noConflict = function () {
/* 1190 */     $.fn.tooltip = old
/* 1191 */     return this
/* 1192 */   }
/* 1193 */ 
/* 1194 */ }(jQuery);
/* 1195 */ 
/* 1196 */ /* ========================================================================
/* 1197 *|  * Bootstrap: popover.js v3.1.1
/* 1198 *|  * http://getbootstrap.com/javascript/#popovers
/* 1199 *|  * ========================================================================
/* 1200 *|  * Copyright 2011-2014 Twitter, Inc.

/* bootstrap.js */

/* 1201 *|  * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
/* 1202 *|  * ======================================================================== */
/* 1203 */ 
/* 1204 */ 
/* 1205 */ +function ($) {
/* 1206 */   'use strict';
/* 1207 */ 
/* 1208 */   // POPOVER PUBLIC CLASS DEFINITION
/* 1209 */   // ===============================
/* 1210 */ 
/* 1211 */   var Popover = function (element, options) {
/* 1212 */     this.init('popover', element, options)
/* 1213 */   }
/* 1214 */ 
/* 1215 */   if (!$.fn.tooltip) throw new Error('Popover requires tooltip.js')
/* 1216 */ 
/* 1217 */   Popover.DEFAULTS = $.extend({}, $.fn.tooltip.Constructor.DEFAULTS, {
/* 1218 */     placement: 'right',
/* 1219 */     trigger: 'click',
/* 1220 */     content: '',
/* 1221 */     template: '<div class="popover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
/* 1222 */   })
/* 1223 */ 
/* 1224 */ 
/* 1225 */   // NOTE: POPOVER EXTENDS tooltip.js
/* 1226 */   // ================================
/* 1227 */ 
/* 1228 */   Popover.prototype = $.extend({}, $.fn.tooltip.Constructor.prototype)
/* 1229 */ 
/* 1230 */   Popover.prototype.constructor = Popover
/* 1231 */ 
/* 1232 */   Popover.prototype.getDefaults = function () {
/* 1233 */     return Popover.DEFAULTS
/* 1234 */   }
/* 1235 */ 
/* 1236 */   Popover.prototype.setContent = function () {
/* 1237 */     var $tip    = this.tip()
/* 1238 */     var title   = this.getTitle()
/* 1239 */     var content = this.getContent()
/* 1240 */ 
/* 1241 */     $tip.find('.popover-title')[this.options.html ? 'html' : 'text'](title)
/* 1242 */     $tip.find('.popover-content')[ // we use append for html objects to maintain js events
/* 1243 */       this.options.html ? (typeof content == 'string' ? 'html' : 'append') : 'text'
/* 1244 */     ](content)
/* 1245 */ 
/* 1246 */     $tip.removeClass('fade top bottom left right in')
/* 1247 */ 
/* 1248 */     // IE8 doesn't accept hiding via the `:empty` pseudo selector, we have to do
/* 1249 */     // this manually by checking the contents.
/* 1250 */     if (!$tip.find('.popover-title').html()) $tip.find('.popover-title').hide()

/* bootstrap.js */

/* 1251 */   }
/* 1252 */ 
/* 1253 */   Popover.prototype.hasContent = function () {
/* 1254 */     return this.getTitle() || this.getContent()
/* 1255 */   }
/* 1256 */ 
/* 1257 */   Popover.prototype.getContent = function () {
/* 1258 */     var $e = this.$element
/* 1259 */     var o  = this.options
/* 1260 */ 
/* 1261 */     return $e.attr('data-content')
/* 1262 */       || (typeof o.content == 'function' ?
/* 1263 */             o.content.call($e[0]) :
/* 1264 */             o.content)
/* 1265 */   }
/* 1266 */ 
/* 1267 */   Popover.prototype.arrow = function () {
/* 1268 */     return this.$arrow = this.$arrow || this.tip().find('.arrow')
/* 1269 */   }
/* 1270 */ 
/* 1271 */   Popover.prototype.tip = function () {
/* 1272 */     if (!this.$tip) this.$tip = $(this.options.template)
/* 1273 */     return this.$tip
/* 1274 */   }
/* 1275 */ 
/* 1276 */ 
/* 1277 */   // POPOVER PLUGIN DEFINITION
/* 1278 */   // =========================
/* 1279 */ 
/* 1280 */   var old = $.fn.popover
/* 1281 */ 
/* 1282 */   $.fn.popover = function (option) {
/* 1283 */     return this.each(function () {
/* 1284 */       var $this   = $(this)
/* 1285 */       var data    = $this.data('bs.popover')
/* 1286 */       var options = typeof option == 'object' && option
/* 1287 */ 
/* 1288 */       if (!data && option == 'destroy') return
/* 1289 */       if (!data) $this.data('bs.popover', (data = new Popover(this, options)))
/* 1290 */       if (typeof option == 'string') data[option]()
/* 1291 */     })
/* 1292 */   }
/* 1293 */ 
/* 1294 */   $.fn.popover.Constructor = Popover
/* 1295 */ 
/* 1296 */ 
/* 1297 */   // POPOVER NO CONFLICT
/* 1298 */   // ===================
/* 1299 */ 
/* 1300 */   $.fn.popover.noConflict = function () {

/* bootstrap.js */

/* 1301 */     $.fn.popover = old
/* 1302 */     return this
/* 1303 */   }
/* 1304 */ 
/* 1305 */ }(jQuery);
/* 1306 */ 
/* 1307 */ /* ========================================================================
/* 1308 *|  * Bootstrap: tab.js v3.1.1
/* 1309 *|  * http://getbootstrap.com/javascript/#tabs
/* 1310 *|  * ========================================================================
/* 1311 *|  * Copyright 2011-2014 Twitter, Inc.
/* 1312 *|  * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
/* 1313 *|  * ======================================================================== */
/* 1314 */ 
/* 1315 */ 
/* 1316 */ +function ($) {
/* 1317 */   'use strict';
/* 1318 */ 
/* 1319 */   // TAB CLASS DEFINITION
/* 1320 */   // ====================
/* 1321 */ 
/* 1322 */   var Tab = function (element) {
/* 1323 */     this.element = $(element)
/* 1324 */   }
/* 1325 */ 
/* 1326 */   Tab.prototype.show = function () {
/* 1327 */     var $this    = this.element
/* 1328 */     var $ul      = $this.closest('ul:not(.dropdown-menu)')
/* 1329 */     var selector = $this.data('target')
/* 1330 */ 
/* 1331 */     if (!selector) {
/* 1332 */       selector = $this.attr('href')
/* 1333 */       selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') //strip for ie7
/* 1334 */     }
/* 1335 */ 
/* 1336 */     if ($this.parent('li').hasClass('active')) return
/* 1337 */ 
/* 1338 */     var previous = $ul.find('.active:last a')[0]
/* 1339 */     var e        = $.Event('show.bs.tab', {
/* 1340 */       relatedTarget: previous
/* 1341 */     })
/* 1342 */ 
/* 1343 */     $this.trigger(e)
/* 1344 */ 
/* 1345 */     if (e.isDefaultPrevented()) return
/* 1346 */ 
/* 1347 */     var $target = $(selector)
/* 1348 */ 
/* 1349 */     this.activate($this.parent('li'), $ul)
/* 1350 */     this.activate($target, $target.parent(), function () {

/* bootstrap.js */

/* 1351 */       $this.trigger({
/* 1352 */         type: 'shown.bs.tab',
/* 1353 */         relatedTarget: previous
/* 1354 */       })
/* 1355 */     })
/* 1356 */   }
/* 1357 */ 
/* 1358 */   Tab.prototype.activate = function (element, container, callback) {
/* 1359 */     var $active    = container.find('> .active')
/* 1360 */     var transition = callback
/* 1361 */       && $.support.transition
/* 1362 */       && $active.hasClass('fade')
/* 1363 */ 
/* 1364 */     function next() {
/* 1365 */       $active
/* 1366 */         .removeClass('active')
/* 1367 */         .find('> .dropdown-menu > .active')
/* 1368 */         .removeClass('active')
/* 1369 */ 
/* 1370 */       element.addClass('active')
/* 1371 */ 
/* 1372 */       if (transition) {
/* 1373 */         element[0].offsetWidth // reflow for transition
/* 1374 */         element.addClass('in')
/* 1375 */       } else {
/* 1376 */         element.removeClass('fade')
/* 1377 */       }
/* 1378 */ 
/* 1379 */       if (element.parent('.dropdown-menu')) {
/* 1380 */         element.closest('li.dropdown').addClass('active')
/* 1381 */       }
/* 1382 */ 
/* 1383 */       callback && callback()
/* 1384 */     }
/* 1385 */ 
/* 1386 */     transition ?
/* 1387 */       $active
/* 1388 */         .one($.support.transition.end, next)
/* 1389 */         .emulateTransitionEnd(150) :
/* 1390 */       next()
/* 1391 */ 
/* 1392 */     $active.removeClass('in')
/* 1393 */   }
/* 1394 */ 
/* 1395 */ 
/* 1396 */   // TAB PLUGIN DEFINITION
/* 1397 */   // =====================
/* 1398 */ 
/* 1399 */   var old = $.fn.tab
/* 1400 */ 

/* bootstrap.js */

/* 1401 */   $.fn.tab = function ( option ) {
/* 1402 */     return this.each(function () {
/* 1403 */       var $this = $(this)
/* 1404 */       var data  = $this.data('bs.tab')
/* 1405 */ 
/* 1406 */       if (!data) $this.data('bs.tab', (data = new Tab(this)))
/* 1407 */       if (typeof option == 'string') data[option]()
/* 1408 */     })
/* 1409 */   }
/* 1410 */ 
/* 1411 */   $.fn.tab.Constructor = Tab
/* 1412 */ 
/* 1413 */ 
/* 1414 */   // TAB NO CONFLICT
/* 1415 */   // ===============
/* 1416 */ 
/* 1417 */   $.fn.tab.noConflict = function () {
/* 1418 */     $.fn.tab = old
/* 1419 */     return this
/* 1420 */   }
/* 1421 */ 
/* 1422 */ 
/* 1423 */   // TAB DATA-API
/* 1424 */   // ============
/* 1425 */ 
/* 1426 */   $(document).on('click.bs.tab.data-api', '[data-toggle="tab"], [data-toggle="pill"]', function (e) {
/* 1427 */     e.preventDefault()
/* 1428 */     $(this).tab('show')
/* 1429 */   })
/* 1430 */ 
/* 1431 */ }(jQuery);
/* 1432 */ 
/* 1433 */ /* ========================================================================
/* 1434 *|  * Bootstrap: affix.js v3.1.1
/* 1435 *|  * http://getbootstrap.com/javascript/#affix
/* 1436 *|  * ========================================================================
/* 1437 *|  * Copyright 2011-2014 Twitter, Inc.
/* 1438 *|  * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
/* 1439 *|  * ======================================================================== */
/* 1440 */ 
/* 1441 */ 
/* 1442 */ +function ($) {
/* 1443 */   'use strict';
/* 1444 */ 
/* 1445 */   // AFFIX CLASS DEFINITION
/* 1446 */   // ======================
/* 1447 */ 
/* 1448 */   var Affix = function (element, options) {
/* 1449 */     this.options = $.extend({}, Affix.DEFAULTS, options)
/* 1450 */     this.$window = $(window)

/* bootstrap.js */

/* 1451 */       .on('scroll.bs.affix.data-api', $.proxy(this.checkPosition, this))
/* 1452 */       .on('click.bs.affix.data-api',  $.proxy(this.checkPositionWithEventLoop, this))
/* 1453 */ 
/* 1454 */     this.$element     = $(element)
/* 1455 */     this.affixed      =
/* 1456 */     this.unpin        =
/* 1457 */     this.pinnedOffset = null
/* 1458 */ 
/* 1459 */     this.checkPosition()
/* 1460 */   }
/* 1461 */ 
/* 1462 */   Affix.RESET = 'affix affix-top affix-bottom'
/* 1463 */ 
/* 1464 */   Affix.DEFAULTS = {
/* 1465 */     offset: 0
/* 1466 */   }
/* 1467 */ 
/* 1468 */   Affix.prototype.getPinnedOffset = function () {
/* 1469 */     if (this.pinnedOffset) return this.pinnedOffset
/* 1470 */     this.$element.removeClass(Affix.RESET).addClass('affix')
/* 1471 */     var scrollTop = this.$window.scrollTop()
/* 1472 */     var position  = this.$element.offset()
/* 1473 */     return (this.pinnedOffset = position.top - scrollTop)
/* 1474 */   }
/* 1475 */ 
/* 1476 */   Affix.prototype.checkPositionWithEventLoop = function () {
/* 1477 */     setTimeout($.proxy(this.checkPosition, this), 1)
/* 1478 */   }
/* 1479 */ 
/* 1480 */   Affix.prototype.checkPosition = function () {
/* 1481 */     if (!this.$element.is(':visible')) return
/* 1482 */ 
/* 1483 */     var scrollHeight = $(document).height()
/* 1484 */     var scrollTop    = this.$window.scrollTop()
/* 1485 */     var position     = this.$element.offset()
/* 1486 */     var offset       = this.options.offset
/* 1487 */     var offsetTop    = offset.top
/* 1488 */     var offsetBottom = offset.bottom
/* 1489 */ 
/* 1490 */     if (this.affixed == 'top') position.top += scrollTop
/* 1491 */ 
/* 1492 */     if (typeof offset != 'object')         offsetBottom = offsetTop = offset
/* 1493 */     if (typeof offsetTop == 'function')    offsetTop    = offset.top(this.$element)
/* 1494 */     if (typeof offsetBottom == 'function') offsetBottom = offset.bottom(this.$element)
/* 1495 */ 
/* 1496 */     var affix = this.unpin   != null && (scrollTop + this.unpin <= position.top) ? false :
/* 1497 */                 offsetBottom != null && (position.top + this.$element.height() >= scrollHeight - offsetBottom) ? 'bottom' :
/* 1498 */                 offsetTop    != null && (scrollTop <= offsetTop) ? 'top' : false
/* 1499 */ 
/* 1500 */     if (this.affixed === affix) return

/* bootstrap.js */

/* 1501 */     if (this.unpin) this.$element.css('top', '')
/* 1502 */ 
/* 1503 */     var affixType = 'affix' + (affix ? '-' + affix : '')
/* 1504 */     var e         = $.Event(affixType + '.bs.affix')
/* 1505 */ 
/* 1506 */     this.$element.trigger(e)
/* 1507 */ 
/* 1508 */     if (e.isDefaultPrevented()) return
/* 1509 */ 
/* 1510 */     this.affixed = affix
/* 1511 */     this.unpin = affix == 'bottom' ? this.getPinnedOffset() : null
/* 1512 */ 
/* 1513 */     this.$element
/* 1514 */       .removeClass(Affix.RESET)
/* 1515 */       .addClass(affixType)
/* 1516 */       .trigger($.Event(affixType.replace('affix', 'affixed')))
/* 1517 */ 
/* 1518 */     if (affix == 'bottom') {
/* 1519 */       this.$element.offset({ top: scrollHeight - offsetBottom - this.$element.height() })
/* 1520 */     }
/* 1521 */   }
/* 1522 */ 
/* 1523 */ 
/* 1524 */   // AFFIX PLUGIN DEFINITION
/* 1525 */   // =======================
/* 1526 */ 
/* 1527 */   var old = $.fn.affix
/* 1528 */ 
/* 1529 */   $.fn.affix = function (option) {
/* 1530 */     return this.each(function () {
/* 1531 */       var $this   = $(this)
/* 1532 */       var data    = $this.data('bs.affix')
/* 1533 */       var options = typeof option == 'object' && option
/* 1534 */ 
/* 1535 */       if (!data) $this.data('bs.affix', (data = new Affix(this, options)))
/* 1536 */       if (typeof option == 'string') data[option]()
/* 1537 */     })
/* 1538 */   }
/* 1539 */ 
/* 1540 */   $.fn.affix.Constructor = Affix
/* 1541 */ 
/* 1542 */ 
/* 1543 */   // AFFIX NO CONFLICT
/* 1544 */   // =================
/* 1545 */ 
/* 1546 */   $.fn.affix.noConflict = function () {
/* 1547 */     $.fn.affix = old
/* 1548 */     return this
/* 1549 */   }
/* 1550 */ 

/* bootstrap.js */

/* 1551 */ 
/* 1552 */   // AFFIX DATA-API
/* 1553 */   // ==============
/* 1554 */ 
/* 1555 */   $(window).on('load', function () {
/* 1556 */     $('[data-spy="affix"]').each(function () {
/* 1557 */       var $spy = $(this)
/* 1558 */       var data = $spy.data()
/* 1559 */ 
/* 1560 */       data.offset = data.offset || {}
/* 1561 */ 
/* 1562 */       if (data.offsetBottom) data.offset.bottom = data.offsetBottom
/* 1563 */       if (data.offsetTop)    data.offset.top    = data.offsetTop
/* 1564 */ 
/* 1565 */       $spy.affix(data)
/* 1566 */     })
/* 1567 */   })
/* 1568 */ 
/* 1569 */ }(jQuery);
/* 1570 */ 
/* 1571 */ /* ========================================================================
/* 1572 *|  * Bootstrap: collapse.js v3.1.1
/* 1573 *|  * http://getbootstrap.com/javascript/#collapse
/* 1574 *|  * ========================================================================
/* 1575 *|  * Copyright 2011-2014 Twitter, Inc.
/* 1576 *|  * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
/* 1577 *|  * ======================================================================== */
/* 1578 */ 
/* 1579 */ 
/* 1580 */ +function ($) {
/* 1581 */   'use strict';
/* 1582 */ 
/* 1583 */   // COLLAPSE PUBLIC CLASS DEFINITION
/* 1584 */   // ================================
/* 1585 */ 
/* 1586 */   var Collapse = function (element, options) {
/* 1587 */     this.$element      = $(element)
/* 1588 */     this.options       = $.extend({}, Collapse.DEFAULTS, options)
/* 1589 */     this.transitioning = null
/* 1590 */ 
/* 1591 */     if (this.options.parent) this.$parent = $(this.options.parent)
/* 1592 */     if (this.options.toggle) this.toggle()
/* 1593 */   }
/* 1594 */ 
/* 1595 */   Collapse.DEFAULTS = {
/* 1596 */     toggle: true
/* 1597 */   }
/* 1598 */ 
/* 1599 */   Collapse.prototype.dimension = function () {
/* 1600 */     var hasWidth = this.$element.hasClass('width')

/* bootstrap.js */

/* 1601 */     return hasWidth ? 'width' : 'height'
/* 1602 */   }
/* 1603 */ 
/* 1604 */   Collapse.prototype.show = function () {
/* 1605 */     if (this.transitioning || this.$element.hasClass('in')) return
/* 1606 */ 
/* 1607 */     var startEvent = $.Event('show.bs.collapse')
/* 1608 */     this.$element.trigger(startEvent)
/* 1609 */     if (startEvent.isDefaultPrevented()) return
/* 1610 */ 
/* 1611 */     var actives = this.$parent && this.$parent.find('> .panel > .in')
/* 1612 */ 
/* 1613 */     if (actives && actives.length) {
/* 1614 */       var hasData = actives.data('bs.collapse')
/* 1615 */       if (hasData && hasData.transitioning) return
/* 1616 */       actives.collapse('hide')
/* 1617 */       hasData || actives.data('bs.collapse', null)
/* 1618 */     }
/* 1619 */ 
/* 1620 */     var dimension = this.dimension()
/* 1621 */ 
/* 1622 */     this.$element
/* 1623 */       .removeClass('collapse')
/* 1624 */       .addClass('collapsing')
/* 1625 */       [dimension](0)
/* 1626 */ 
/* 1627 */     this.transitioning = 1
/* 1628 */ 
/* 1629 */     var complete = function () {
/* 1630 */       this.$element
/* 1631 */         .removeClass('collapsing')
/* 1632 */         .addClass('collapse in')
/* 1633 */         [dimension]('auto')
/* 1634 */       this.transitioning = 0
/* 1635 */       this.$element.trigger('shown.bs.collapse')
/* 1636 */     }
/* 1637 */ 
/* 1638 */     if (!$.support.transition) return complete.call(this)
/* 1639 */ 
/* 1640 */     var scrollSize = $.camelCase(['scroll', dimension].join('-'))
/* 1641 */ 
/* 1642 */     this.$element
/* 1643 */       .one($.support.transition.end, $.proxy(complete, this))
/* 1644 */       .emulateTransitionEnd(350)
/* 1645 */       [dimension](this.$element[0][scrollSize])
/* 1646 */   }
/* 1647 */ 
/* 1648 */   Collapse.prototype.hide = function () {
/* 1649 */     if (this.transitioning || !this.$element.hasClass('in')) return
/* 1650 */ 

/* bootstrap.js */

/* 1651 */     var startEvent = $.Event('hide.bs.collapse')
/* 1652 */     this.$element.trigger(startEvent)
/* 1653 */     if (startEvent.isDefaultPrevented()) return
/* 1654 */ 
/* 1655 */     var dimension = this.dimension()
/* 1656 */ 
/* 1657 */     this.$element
/* 1658 */       [dimension](this.$element[dimension]())
/* 1659 */       [0].offsetHeight
/* 1660 */ 
/* 1661 */     this.$element
/* 1662 */       .addClass('collapsing')
/* 1663 */       .removeClass('collapse')
/* 1664 */       .removeClass('in')
/* 1665 */ 
/* 1666 */     this.transitioning = 1
/* 1667 */ 
/* 1668 */     var complete = function () {
/* 1669 */       this.transitioning = 0
/* 1670 */       this.$element
/* 1671 */         .trigger('hidden.bs.collapse')
/* 1672 */         .removeClass('collapsing')
/* 1673 */         .addClass('collapse')
/* 1674 */     }
/* 1675 */ 
/* 1676 */     if (!$.support.transition) return complete.call(this)
/* 1677 */ 
/* 1678 */     this.$element
/* 1679 */       [dimension](0)
/* 1680 */       .one($.support.transition.end, $.proxy(complete, this))
/* 1681 */       .emulateTransitionEnd(350)
/* 1682 */   }
/* 1683 */ 
/* 1684 */   Collapse.prototype.toggle = function () {
/* 1685 */     this[this.$element.hasClass('in') ? 'hide' : 'show']()
/* 1686 */   }
/* 1687 */ 
/* 1688 */ 
/* 1689 */   // COLLAPSE PLUGIN DEFINITION
/* 1690 */   // ==========================
/* 1691 */ 
/* 1692 */   var old = $.fn.collapse
/* 1693 */ 
/* 1694 */   $.fn.collapse = function (option) {
/* 1695 */     return this.each(function () {
/* 1696 */       var $this   = $(this)
/* 1697 */       var data    = $this.data('bs.collapse')
/* 1698 */       var options = $.extend({}, Collapse.DEFAULTS, $this.data(), typeof option == 'object' && option)
/* 1699 */ 
/* 1700 */       if (!data && options.toggle && option == 'show') option = !option

/* bootstrap.js */

/* 1701 */       if (!data) $this.data('bs.collapse', (data = new Collapse(this, options)))
/* 1702 */       if (typeof option == 'string') data[option]()
/* 1703 */     })
/* 1704 */   }
/* 1705 */ 
/* 1706 */   $.fn.collapse.Constructor = Collapse
/* 1707 */ 
/* 1708 */ 
/* 1709 */   // COLLAPSE NO CONFLICT
/* 1710 */   // ====================
/* 1711 */ 
/* 1712 */   $.fn.collapse.noConflict = function () {
/* 1713 */     $.fn.collapse = old
/* 1714 */     return this
/* 1715 */   }
/* 1716 */ 
/* 1717 */ 
/* 1718 */   // COLLAPSE DATA-API
/* 1719 */   // =================
/* 1720 */ 
/* 1721 */   $(document).on('click.bs.collapse.data-api', '[data-toggle=collapse]', function (e) {
/* 1722 */     var $this   = $(this), href
/* 1723 */     var target  = $this.attr('data-target')
/* 1724 */         || e.preventDefault()
/* 1725 */         || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '') //strip for ie7
/* 1726 */     var $target = $(target)
/* 1727 */     var data    = $target.data('bs.collapse')
/* 1728 */     var option  = data ? 'toggle' : $this.data()
/* 1729 */     var parent  = $this.attr('data-parent')
/* 1730 */     var $parent = parent && $(parent)
/* 1731 */ 
/* 1732 */     if (!data || !data.transitioning) {
/* 1733 */       if ($parent) $parent.find('[data-toggle=collapse][data-parent="' + parent + '"]').not($this).addClass('collapsed')
/* 1734 */       $this[$target.hasClass('in') ? 'addClass' : 'removeClass']('collapsed')
/* 1735 */     }
/* 1736 */ 
/* 1737 */     $target.collapse(option)
/* 1738 */   })
/* 1739 */ 
/* 1740 */ }(jQuery);
/* 1741 */ 
/* 1742 */ /* ========================================================================
/* 1743 *|  * Bootstrap: scrollspy.js v3.1.1
/* 1744 *|  * http://getbootstrap.com/javascript/#scrollspy
/* 1745 *|  * ========================================================================
/* 1746 *|  * Copyright 2011-2014 Twitter, Inc.
/* 1747 *|  * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
/* 1748 *|  * ======================================================================== */
/* 1749 */ 
/* 1750 */ 

/* bootstrap.js */

/* 1751 */ +function ($) {
/* 1752 */   'use strict';
/* 1753 */ 
/* 1754 */   // SCROLLSPY CLASS DEFINITION
/* 1755 */   // ==========================
/* 1756 */ 
/* 1757 */   function ScrollSpy(element, options) {
/* 1758 */     var href
/* 1759 */     var process  = $.proxy(this.process, this)
/* 1760 */ 
/* 1761 */     this.$element       = $(element).is('body') ? $(window) : $(element)
/* 1762 */     this.$body          = $('body')
/* 1763 */     this.$scrollElement = this.$element.on('scroll.bs.scroll-spy.data-api', process)
/* 1764 */     this.options        = $.extend({}, ScrollSpy.DEFAULTS, options)
/* 1765 */     this.selector       = (this.options.target
/* 1766 */       || ((href = $(element).attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')) //strip for ie7
/* 1767 */       || '') + ' .nav li > a'
/* 1768 */     this.offsets        = $([])
/* 1769 */     this.targets        = $([])
/* 1770 */     this.activeTarget   = null
/* 1771 */ 
/* 1772 */     this.refresh()
/* 1773 */     this.process()
/* 1774 */   }
/* 1775 */ 
/* 1776 */   ScrollSpy.DEFAULTS = {
/* 1777 */     offset: 10
/* 1778 */   }
/* 1779 */ 
/* 1780 */   ScrollSpy.prototype.refresh = function () {
/* 1781 */     var offsetMethod = this.$element[0] == window ? 'offset' : 'position'
/* 1782 */ 
/* 1783 */     this.offsets = $([])
/* 1784 */     this.targets = $([])
/* 1785 */ 
/* 1786 */     var self     = this
/* 1787 */     var $targets = this.$body
/* 1788 */       .find(this.selector)
/* 1789 */       .map(function () {
/* 1790 */         var $el   = $(this)
/* 1791 */         var href  = $el.data('target') || $el.attr('href')
/* 1792 */         var $href = /^#./.test(href) && $(href)
/* 1793 */ 
/* 1794 */         return ($href
/* 1795 */           && $href.length
/* 1796 */           && $href.is(':visible')
/* 1797 */           && [[ $href[offsetMethod]().top + (!$.isWindow(self.$scrollElement.get(0)) && self.$scrollElement.scrollTop()), href ]]) || null
/* 1798 */       })
/* 1799 */       .sort(function (a, b) { return a[0] - b[0] })
/* 1800 */       .each(function () {

/* bootstrap.js */

/* 1801 */         self.offsets.push(this[0])
/* 1802 */         self.targets.push(this[1])
/* 1803 */       })
/* 1804 */   }
/* 1805 */ 
/* 1806 */   ScrollSpy.prototype.process = function () {
/* 1807 */     var scrollTop    = this.$scrollElement.scrollTop() + this.options.offset
/* 1808 */     var scrollHeight = this.$scrollElement[0].scrollHeight || this.$body[0].scrollHeight
/* 1809 */     var maxScroll    = scrollHeight - this.$scrollElement.height()
/* 1810 */     var offsets      = this.offsets
/* 1811 */     var targets      = this.targets
/* 1812 */     var activeTarget = this.activeTarget
/* 1813 */     var i
/* 1814 */ 
/* 1815 */     if (scrollTop >= maxScroll) {
/* 1816 */       return activeTarget != (i = targets.last()[0]) && this.activate(i)
/* 1817 */     }
/* 1818 */ 
/* 1819 */     if (activeTarget && scrollTop <= offsets[0]) {
/* 1820 */       return activeTarget != (i = targets[0]) && this.activate(i)
/* 1821 */     }
/* 1822 */ 
/* 1823 */     for (i = offsets.length; i--;) {
/* 1824 */       activeTarget != targets[i]
/* 1825 */         && scrollTop >= offsets[i]
/* 1826 */         && (!offsets[i + 1] || scrollTop <= offsets[i + 1])
/* 1827 */         && this.activate( targets[i] )
/* 1828 */     }
/* 1829 */   }
/* 1830 */ 
/* 1831 */   ScrollSpy.prototype.activate = function (target) {
/* 1832 */     this.activeTarget = target
/* 1833 */ 
/* 1834 */     $(this.selector)
/* 1835 */       .parentsUntil(this.options.target, '.active')
/* 1836 */       .removeClass('active')
/* 1837 */ 
/* 1838 */     var selector = this.selector +
/* 1839 */         '[data-target="' + target + '"],' +
/* 1840 */         this.selector + '[href="' + target + '"]'
/* 1841 */ 
/* 1842 */     var active = $(selector)
/* 1843 */       .parents('li')
/* 1844 */       .addClass('active')
/* 1845 */ 
/* 1846 */     if (active.parent('.dropdown-menu').length) {
/* 1847 */       active = active
/* 1848 */         .closest('li.dropdown')
/* 1849 */         .addClass('active')
/* 1850 */     }

/* bootstrap.js */

/* 1851 */ 
/* 1852 */     active.trigger('activate.bs.scrollspy')
/* 1853 */   }
/* 1854 */ 
/* 1855 */ 
/* 1856 */   // SCROLLSPY PLUGIN DEFINITION
/* 1857 */   // ===========================
/* 1858 */ 
/* 1859 */   var old = $.fn.scrollspy
/* 1860 */ 
/* 1861 */   $.fn.scrollspy = function (option) {
/* 1862 */     return this.each(function () {
/* 1863 */       var $this   = $(this)
/* 1864 */       var data    = $this.data('bs.scrollspy')
/* 1865 */       var options = typeof option == 'object' && option
/* 1866 */ 
/* 1867 */       if (!data) $this.data('bs.scrollspy', (data = new ScrollSpy(this, options)))
/* 1868 */       if (typeof option == 'string') data[option]()
/* 1869 */     })
/* 1870 */   }
/* 1871 */ 
/* 1872 */   $.fn.scrollspy.Constructor = ScrollSpy
/* 1873 */ 
/* 1874 */ 
/* 1875 */   // SCROLLSPY NO CONFLICT
/* 1876 */   // =====================
/* 1877 */ 
/* 1878 */   $.fn.scrollspy.noConflict = function () {
/* 1879 */     $.fn.scrollspy = old
/* 1880 */     return this
/* 1881 */   }
/* 1882 */ 
/* 1883 */ 
/* 1884 */   // SCROLLSPY DATA-API
/* 1885 */   // ==================
/* 1886 */ 
/* 1887 */   $(window).on('load', function () {
/* 1888 */     $('[data-spy="scroll"]').each(function () {
/* 1889 */       var $spy = $(this)
/* 1890 */       $spy.scrollspy($spy.data())
/* 1891 */     })
/* 1892 */   })
/* 1893 */ 
/* 1894 */ }(jQuery);
/* 1895 */ 
/* 1896 */ /* ========================================================================
/* 1897 *|  * Bootstrap: transition.js v3.1.1
/* 1898 *|  * http://getbootstrap.com/javascript/#transitions
/* 1899 *|  * ========================================================================
/* 1900 *|  * Copyright 2011-2014 Twitter, Inc.

/* bootstrap.js */

/* 1901 *|  * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
/* 1902 *|  * ======================================================================== */
/* 1903 */ 
/* 1904 */ 
/* 1905 */ +function ($) {
/* 1906 */   'use strict';
/* 1907 */ 
/* 1908 */   // CSS TRANSITION SUPPORT (Shoutout: http://www.modernizr.com/)
/* 1909 */   // ============================================================
/* 1910 */ 
/* 1911 */   function transitionEnd() {
/* 1912 */     var el = document.createElement('bootstrap')
/* 1913 */ 
/* 1914 */     var transEndEventNames = {
/* 1915 */       'WebkitTransition' : 'webkitTransitionEnd',
/* 1916 */       'MozTransition'    : 'transitionend',
/* 1917 */       'OTransition'      : 'oTransitionEnd otransitionend',
/* 1918 */       'transition'       : 'transitionend'
/* 1919 */     }
/* 1920 */ 
/* 1921 */     for (var name in transEndEventNames) {
/* 1922 */       if (el.style[name] !== undefined) {
/* 1923 */         return { end: transEndEventNames[name] }
/* 1924 */       }
/* 1925 */     }
/* 1926 */ 
/* 1927 */     return false // explicit for ie8 (  ._.)
/* 1928 */   }
/* 1929 */ 
/* 1930 */   // http://blog.alexmaccaw.com/css-transitions
/* 1931 */   $.fn.emulateTransitionEnd = function (duration) {
/* 1932 */     var called = false, $el = this
/* 1933 */     $(this).one($.support.transition.end, function () { called = true })
/* 1934 */     var callback = function () { if (!called) $($el).trigger($.support.transition.end) }
/* 1935 */     setTimeout(callback, duration)
/* 1936 */     return this
/* 1937 */   }
/* 1938 */ 
/* 1939 */   $(function () {
/* 1940 */     $.support.transition = transitionEnd()
/* 1941 */   })
/* 1942 */ 
/* 1943 */ }(jQuery);
/* 1944 */ 

;
/* slimmenu.js */

/* 1   */ /**
/* 2   *|  * jquery.slimmenu.js
/* 3   *|  * http://adnantopal.github.io/slimmenu/
/* 4   *|  * Author: @adnantopal
/* 5   *|  * Copyright 2013, Adnan Topal (atopal.com)
/* 6   *|  * Licensed under the MIT license.
/* 7   *|  */
/* 8   */ ;(function ( $, window, document, undefined )
/* 9   */ {
/* 10  */     var pluginName = "slimmenu",
/* 11  */         defaults =
/* 12  */         {
/* 13  */             resizeWidth: '768',
/* 14  */             collapserTitle: 'Main Menu',
/* 15  */             animSpeed: 'medium',
/* 16  */             easingEffect: null,
/* 17  */             indentChildren: false,
/* 18  */             childrenIndenter: '&nbsp;&nbsp;'
/* 19  */         };
/* 20  */ 
/* 21  */     function Plugin( element, options )
/* 22  */     {
/* 23  */         this.element = element;
/* 24  */         this.$elem = $(this.element);
/* 25  */         this.options = $.extend( {}, defaults, options );
/* 26  */         this.init();
/* 27  */     }
/* 28  */ 
/* 29  */     Plugin.prototype = {
/* 30  */ 
/* 31  */         init: function()
/* 32  */         {
/* 33  */             var $options = this.options,
/* 34  */                 $menu = this.$elem,
/* 35  */                 $collapser = '<div class="slimmenu-menu-collapser">'+$options.collapserTitle+'<div class="slimmenu-collapse-button"><span class="slimmenu-icon-bar"></span><span class="slimmenu-icon-bar"></span><span class="slimmenu-icon-bar"></span></div></div>',
/* 36  */                 $menu_collapser;
/* 37  */ 
/* 38  */             $menu.before($collapser);
/* 39  */             $menu_collapser = $menu.prev('.slimmenu-menu-collapser');
/* 40  */ 
/* 41  */             $menu.on('click', '.slimmenu-sub-collapser', function(e)
/* 42  */             {
/* 43  */                 e.preventDefault();
/* 44  */                 e.stopPropagation();
/* 45  */ 
/* 46  */                 var $parent_li = $(this).closest('li');
/* 47  */ 
/* 48  */                 if ($(this).hasClass('expanded'))
/* 49  */                 {
/* 50  */                     $(this).removeClass('expanded');

/* slimmenu.js */

/* 51  */                     $(this).find('i').addClass('fa fa-angle-down');
/* 52  */                     $parent_li.find('>ul').slideUp($options.animSpeed, $options.easingEffect);
/* 53  */                 }
/* 54  */                 else
/* 55  */                 {
/* 56  */                     $(this).addClass('expanded');
/* 57  */                     $(this).find('i').removeClass('fa-angle-down').addClass('fa fa-angle-up');
/* 58  */                     $parent_li.find('>ul').slideDown($options.animSpeed, $options.easingEffect);
/* 59  */                 }
/* 60  */             });
/* 61  */ 
/* 62  */             $menu_collapser.on('click', '.slimmenu-collapse-button', function(e)
/* 63  */             {
/* 64  */                 e.preventDefault();
/* 65  */                 $menu.slideToggle($options.animSpeed, $options.easingEffect);
/* 66  */             });
/* 67  */ 
/* 68  */             this.resizeMenu({ data: { el: this.element, options: this.options } });
/* 69  */             $(window).on('resize', { el: this.element, options: this.options }, this.resizeMenu);
/* 70  */         },
/* 71  */ 
/* 72  */         resizeMenu: function(event)
/* 73  */         {
/* 74  */             var $window = $(window),
/* 75  */                 $options = event.data.options,
/* 76  */                 $menu = $(event.data.el),
/* 77  */                 $menu_collapser = $('body').find('.slimmenu-menu-collapser');
/* 78  */ 
/* 79  */             $menu.find('li').each(function()
/* 80  */             {
/* 81  */                 if ($(this).has('ul').length)
/* 82  */                 {
/* 83  */                     $(this).addClass('slimmenu-sub-menu');
/* 84  */                     if ($(this).has('.slimmenu-sub-collapser').length)
/* 85  */                     {
/* 86  */                         $(this).children('.slimmenu-sub-collapser i').addClass('fa fa-angle-down');
/* 87  */                     }
/* 88  */                     else
/* 89  */                     {
/* 90  */                         $(this).append('<span class="slimmenu-sub-collapser"><i class="fa fa-angle-down"></i></span>');
/* 91  */                     }
/* 92  */                 }
/* 93  */ 
/* 94  */                 $(this).children('ul').hide();
/* 95  */                 $(this).find('.slimmenu-sub-collapser').removeClass('expanded').children('i').addClass('fa fa-angle-down');
/* 96  */             });
/* 97  */ 
/* 98  */             if ($options.resizeWidth >= $window.width() || $('body').hasClass('touch'))
/* 99  */             {
/* 100 */                 if ($options.indentChildren)

/* slimmenu.js */

/* 101 */                 {
/* 102 */                     $menu.find('ul').each(function()
/* 103 */                     {
/* 104 */                         var $depth = $(this).parents('ul').length;
/* 105 */                         if (!$(this).children('li').children('a').has('i').length)
/* 106 */                         {
/* 107 */                             $(this).children('li').children('a').prepend(Plugin.prototype.indent($depth, $options));
/* 108 */                         }
/* 109 */                     });
/* 110 */                 }
/* 111 */ 
/* 112 */                 $menu.find('li').has('ul').off('mouseenter mouseleave');
/* 113 */                 $menu.addClass('slimmenu-collapsed').hide();
/* 114 */                 $menu_collapser.show();
/* 115 */             }
/* 116 */             else
/* 117 */             {
/* 118 */                 $menu.find('li').has('ul').on('mouseenter', function()
/* 119 */                 {
/* 120 */                     $(this).find('>ul').stop().slideDown($options.animSpeed, $options.easingEffect);
/* 121 */                 })
/* 122 */                 .on('mouseleave', function()
/* 123 */                 {
/* 124 */                     $(this).find('>ul').stop().slideUp($options.animSpeed, $options.easingEffect);
/* 125 */                 });
/* 126 */ 
/* 127 */                 $menu.find('li > a > i').remove();
/* 128 */                 $menu.removeClass('slimmenu-collapsed').show();
/* 129 */                 $menu_collapser.hide();
/* 130 */             }
/* 131 */         },
/* 132 */ 
/* 133 */         indent: function(num, options)
/* 134 */         {
/* 135 */             var $indent = '';
/* 136 */             for (var i=0; i < num; i++)
/* 137 */             {
/* 138 */                 $indent += options.childrenIndenter;
/* 139 */             }
/* 140 */             return '<i>'+$indent+'</i>';
/* 141 */         }
/* 142 */     };
/* 143 */ 
/* 144 */     $.fn[pluginName] = function ( options )
/* 145 */     {
/* 146 */         return this.each(function ()
/* 147 */         {
/* 148 */             if (!$.data(this, "plugin_" + pluginName))
/* 149 */             {
/* 150 */                 $.data(this, "plugin_" + pluginName,

/* slimmenu.js */

/* 151 */                 new Plugin( this, options ));
/* 152 */             }
/* 153 */         });
/* 154 */     };
/* 155 */ 
/* 156 */ })( jQuery, window, document );

;
/* bootstrap-datepicker.js */

/* 1    */ /* =========================================================
/* 2    *|  * bootstrap-datepicker.js
/* 3    *|  * Repo: https://github.com/eternicode/bootstrap-datepicker/
/* 4    *|  * Demo: http://eternicode.github.io/bootstrap-datepicker/
/* 5    *|  * Docs: http://bootstrap-datepicker.readthedocs.org/
/* 6    *|  * Forked from http://www.eyecon.ro/bootstrap-datepicker
/* 7    *|  * =========================================================
/* 8    *|  * Started by Stefan Petre; improvements by Andrew Rowls + contributors
/* 9    *|  *
/* 10   *|  * Licensed under the Apache License, Version 2.0 (the "License");
/* 11   *|  * you may not use this file except in compliance with the License.
/* 12   *|  * You may obtain a copy of the License at
/* 13   *|  *
/* 14   *|  * http://www.apache.org/licenses/LICENSE-2.0
/* 15   *|  *
/* 16   *|  * Unless required by applicable law or agreed to in writing, software
/* 17   *|  * distributed under the License is distributed on an "AS IS" BASIS,
/* 18   *|  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
/* 19   *|  * See the License for the specific language governing permissions and
/* 20   *|  * limitations under the License.
/* 21   *|  * ========================================================= */
/* 22   */ 
/* 23   */ (function($, undefined) {
/* 24   */ 
/* 25   */     var $window = $(window);
/* 26   */ 
/* 27   */     function UTCDate() {
/* 28   */         return new Date(Date.UTC.apply(Date, arguments));
/* 29   */     }
/* 30   */ 
/* 31   */     function UTCToday() {
/* 32   */         var today = new Date();
/* 33   */         return UTCDate(today.getFullYear(), today.getMonth(), today.getDate());
/* 34   */     }
/* 35   */ 
/* 36   */     function alias(method) {
/* 37   */         return function() {
/* 38   */             return this[method].apply(this, arguments);
/* 39   */         };
/* 40   */     }
/* 41   */ 
/* 42   */     var DateArray = (function() {
/* 43   */         var extras = {
/* 44   */             get: function(i) {
/* 45   */                 return this.slice(i)[0];
/* 46   */             },
/* 47   */             contains: function(d) {
/* 48   */                 // Array.indexOf is not cross-browser;
/* 49   */                 // $.inArray doesn't work with Dates
/* 50   */                 var val = d && d.valueOf();

/* bootstrap-datepicker.js */

/* 51   */                 for (var i = 0, l = this.length; i < l; i++)
/* 52   */                     if (this[i].valueOf() === val)
/* 53   */                         return i;
/* 54   */                 return -1;
/* 55   */             },
/* 56   */             remove: function(i) {
/* 57   */                 this.splice(i, 1);
/* 58   */             },
/* 59   */             replace: function(new_array) {
/* 60   */                 if (!new_array)
/* 61   */                     return;
/* 62   */                 if (!$.isArray(new_array))
/* 63   */                     new_array = [new_array];
/* 64   */                 this.clear();
/* 65   */                 this.push.apply(this, new_array);
/* 66   */             },
/* 67   */             clear: function() {
/* 68   */                 this.length = 0;
/* 69   */             },
/* 70   */             copy: function() {
/* 71   */                 var a = new DateArray();
/* 72   */                 a.replace(this);
/* 73   */                 return a;
/* 74   */             }
/* 75   */         };
/* 76   */ 
/* 77   */         return function() {
/* 78   */             var a = [];
/* 79   */             a.push.apply(a, arguments);
/* 80   */             $.extend(a, extras);
/* 81   */             return a;
/* 82   */         };
/* 83   */     })();
/* 84   */ 
/* 85   */ 
/* 86   */     // Picker object
/* 87   */ 
/* 88   */     var Datepicker = function(element, options) {
/* 89   */         this.dates = new DateArray();
/* 90   */         this.viewDate = UTCToday();
/* 91   */         this.focusDate = null;
/* 92   */ 
/* 93   */         this._process_options(options);
/* 94   */ 
/* 95   */         this.element = $(element);
/* 96   */         this.isInline = false;
/* 97   */         this.isInput = this.element.is('input');
/* 98   */         this.component = this.element.is('.date') ? this.element.find('.add-on, .input-group-addon, .btn') : false;
/* 99   */         this.hasInput = this.component && this.element.find('input').length;
/* 100  */         if (this.component && this.component.length === 0)

/* bootstrap-datepicker.js */

/* 101  */             this.component = false;
/* 102  */ 
/* 103  */         this.picker = $(DPGlobal.template);
/* 104  */         this._buildEvents();
/* 105  */         this._attachEvents();
/* 106  */ 
/* 107  */         if (this.isInline) {
/* 108  */             this.picker.addClass('datepicker-inline').appendTo(this.element);
/* 109  */         } else {
/* 110  */             this.picker.addClass('datepicker-dropdown dropdown-menu');
/* 111  */         }
/* 112  */ 
/* 113  */         if (this.o.rtl) {
/* 114  */             this.picker.addClass('datepicker-rtl');
/* 115  */         }
/* 116  */ 
/* 117  */         this.viewMode = this.o.startView;
/* 118  */ 
/* 119  */         if (this.o.calendarWeeks)
/* 120  */             this.picker.find('tfoot th.today')
/* 121  */                 .attr('colspan', function(i, val) {
/* 122  */                     return parseInt(val) + 1;
/* 123  */                 });
/* 124  */ 
/* 125  */         this._allow_update = false;
/* 126  */ 
/* 127  */         this.setStartDate(this._o.startDate);
/* 128  */         this.setEndDate(this._o.endDate);
/* 129  */         this.setDaysOfWeekDisabled(this.o.daysOfWeekDisabled);
/* 130  */ 
/* 131  */         this.fillDow();
/* 132  */         this.fillMonths();
/* 133  */ 
/* 134  */         this._allow_update = true;
/* 135  */ 
/* 136  */         this.update();
/* 137  */         this.showMode();
/* 138  */ 
/* 139  */         if (this.isInline) {
/* 140  */             this.show();
/* 141  */         }
/* 142  */     };
/* 143  */ 
/* 144  */     Datepicker.prototype = {
/* 145  */         constructor: Datepicker,
/* 146  */ 
/* 147  */         _process_options: function(opts) {
/* 148  */             // Store raw options for reference
/* 149  */             this._o = $.extend({}, this._o, opts);
/* 150  */             // Processed options

/* bootstrap-datepicker.js */

/* 151  */             var o = this.o = $.extend({}, this._o);
/* 152  */ 
/* 153  */             // Check if "de-DE" style date is available, if not language should
/* 154  */             // fallback to 2 letter code eg "de"
/* 155  */             var lang = o.language;
/* 156  */             if (!dates[lang]) {
/* 157  */                 lang = lang.split('-')[0];
/* 158  */                 if (!dates[lang])
/* 159  */                     lang = defaults.language;
/* 160  */             }
/* 161  */             o.language = lang;
/* 162  */ 
/* 163  */             switch (o.startView) {
/* 164  */                 case 2:
/* 165  */                 case 'decade':
/* 166  */                     o.startView = 2;
/* 167  */                     break;
/* 168  */                 case 1:
/* 169  */                 case 'year':
/* 170  */                     o.startView = 1;
/* 171  */                     break;
/* 172  */                 default:
/* 173  */                     o.startView = 0;
/* 174  */             }
/* 175  */ 
/* 176  */             switch (o.minViewMode) {
/* 177  */                 case 1:
/* 178  */                 case 'months':
/* 179  */                     o.minViewMode = 1;
/* 180  */                     break;
/* 181  */                 case 2:
/* 182  */                 case 'years':
/* 183  */                     o.minViewMode = 2;
/* 184  */                     break;
/* 185  */                 default:
/* 186  */                     o.minViewMode = 0;
/* 187  */             }
/* 188  */ 
/* 189  */             o.startView = Math.max(o.startView, o.minViewMode);
/* 190  */ 
/* 191  */             // true, false, or Number > 0
/* 192  */             if (o.multidate !== true) {
/* 193  */                 o.multidate = Number(o.multidate) || false;
/* 194  */                 if (o.multidate !== false)
/* 195  */                     o.multidate = Math.max(0, o.multidate);
/* 196  */                 else
/* 197  */                     o.multidate = 1;
/* 198  */             }
/* 199  */             o.multidateSeparator = String(o.multidateSeparator);
/* 200  */ 

/* bootstrap-datepicker.js */

/* 201  */             o.weekStart %= 7;
/* 202  */             o.weekEnd = ((o.weekStart + 6) % 7);
/* 203  */ 
/* 204  */             var format = DPGlobal.parseFormat(o.format);
/* 205  */             if (o.startDate !== -Infinity) {
/* 206  */                 if ( !! o.startDate) {
/* 207  */                     if (o.startDate instanceof Date)
/* 208  */                         o.startDate = this._local_to_utc(this._zero_time(o.startDate));
/* 209  */                     else
/* 210  */                         o.startDate = DPGlobal.parseDate(o.startDate, format, o.language);
/* 211  */                 } else {
/* 212  */                     o.startDate = -Infinity;
/* 213  */                 }
/* 214  */             }
/* 215  */             if (o.endDate !== Infinity) {
/* 216  */                 if ( !! o.endDate) {
/* 217  */                     if (o.endDate instanceof Date)
/* 218  */                         o.endDate = this._local_to_utc(this._zero_time(o.endDate));
/* 219  */                     else
/* 220  */                         o.endDate = DPGlobal.parseDate(o.endDate, format, o.language);
/* 221  */                 } else {
/* 222  */                     o.endDate = Infinity;
/* 223  */                 }
/* 224  */             }
/* 225  */ 
/* 226  */             o.daysOfWeekDisabled = o.daysOfWeekDisabled || [];
/* 227  */             if (!$.isArray(o.daysOfWeekDisabled))
/* 228  */                 o.daysOfWeekDisabled = o.daysOfWeekDisabled.split(/[,\s]*/);
/* 229  */             o.daysOfWeekDisabled = $.map(o.daysOfWeekDisabled, function(d) {
/* 230  */                 return parseInt(d, 10);
/* 231  */             });
/* 232  */ 
/* 233  */             var plc = String(o.orientation).toLowerCase().split(/\s+/g),
/* 234  */                 _plc = o.orientation.toLowerCase();
/* 235  */             plc = $.grep(plc, function(word) {
/* 236  */                 return (/^auto|left|right|top|bottom$/).test(word);
/* 237  */             });
/* 238  */             o.orientation = {
/* 239  */                 x: 'auto',
/* 240  */                 y: 'auto'
/* 241  */             };
/* 242  */             if (!_plc || _plc === 'auto')
/* 243  */             ; // no action
/* 244  */             else if (plc.length === 1) {
/* 245  */                 switch (plc[0]) {
/* 246  */                     case 'top':
/* 247  */                     case 'bottom':
/* 248  */                         o.orientation.y = plc[0];
/* 249  */                         break;
/* 250  */                     case 'left':

/* bootstrap-datepicker.js */

/* 251  */                     case 'right':
/* 252  */                         o.orientation.x = plc[0];
/* 253  */                         break;
/* 254  */                 }
/* 255  */             } else {
/* 256  */                 _plc = $.grep(plc, function(word) {
/* 257  */                     return (/^left|right$/).test(word);
/* 258  */                 });
/* 259  */                 o.orientation.x = _plc[0] || 'auto';
/* 260  */ 
/* 261  */                 _plc = $.grep(plc, function(word) {
/* 262  */                     return (/^top|bottom$/).test(word);
/* 263  */                 });
/* 264  */                 o.orientation.y = _plc[0] || 'auto';
/* 265  */             }
/* 266  */         },
/* 267  */         _events: [],
/* 268  */         _secondaryEvents: [],
/* 269  */         _applyEvents: function(evs) {
/* 270  */             for (var i = 0, el, ch, ev; i < evs.length; i++) {
/* 271  */                 el = evs[i][0];
/* 272  */                 if (evs[i].length === 2) {
/* 273  */                     ch = undefined;
/* 274  */                     ev = evs[i][1];
/* 275  */                 } else if (evs[i].length === 3) {
/* 276  */                     ch = evs[i][1];
/* 277  */                     ev = evs[i][2];
/* 278  */                 }
/* 279  */                 el.on(ev, ch);
/* 280  */             }
/* 281  */         },
/* 282  */         _unapplyEvents: function(evs) {
/* 283  */             for (var i = 0, el, ev, ch; i < evs.length; i++) {
/* 284  */                 el = evs[i][0];
/* 285  */                 if (evs[i].length === 2) {
/* 286  */                     ch = undefined;
/* 287  */                     ev = evs[i][1];
/* 288  */                 } else if (evs[i].length === 3) {
/* 289  */                     ch = evs[i][1];
/* 290  */                     ev = evs[i][2];
/* 291  */                 }
/* 292  */                 el.off(ev, ch);
/* 293  */             }
/* 294  */         },
/* 295  */         _buildEvents: function() {
/* 296  */             if (this.isInput) { // single input
/* 297  */                 this._events = [
/* 298  */                     [this.element, {
/* 299  */                         focus: $.proxy(this.show, this),
/* 300  */                         keyup: $.proxy(function(e) {

/* bootstrap-datepicker.js */

/* 301  */                             if ($.inArray(e.keyCode, [27, 37, 39, 38, 40, 32, 13, 9]) === -1)
/* 302  */                                 this.update();
/* 303  */                         }, this),
/* 304  */                         keydown: $.proxy(this.keydown, this)
/* 305  */                     }]
/* 306  */                 ];
/* 307  */             } else if (this.component && this.hasInput) { // component: input + button
/* 308  */                 this._events = [
/* 309  */                     // For components that are not readonly, allow keyboard nav
/* 310  */                     [this.element.find('input'), {
/* 311  */                         focus: $.proxy(this.show, this),
/* 312  */                         keyup: $.proxy(function(e) {
/* 313  */                             if ($.inArray(e.keyCode, [27, 37, 39, 38, 40, 32, 13, 9]) === -1)
/* 314  */                                 this.update();
/* 315  */                         }, this),
/* 316  */                         keydown: $.proxy(this.keydown, this)
/* 317  */                     }],
/* 318  */                     [this.component, {
/* 319  */                         click: $.proxy(this.show, this)
/* 320  */                     }]
/* 321  */                 ];
/* 322  */             } else if (this.element.is('div')) { // inline datepicker
/* 323  */                 this.isInline = true;
/* 324  */             } else {
/* 325  */                 this._events = [
/* 326  */                     [this.element, {
/* 327  */                         click: $.proxy(this.show, this)
/* 328  */                     }]
/* 329  */                 ];
/* 330  */             }
/* 331  */             this._events.push(
/* 332  */                 // Component: listen for blur on element descendants
/* 333  */                 [this.element, '*', {
/* 334  */                     blur: $.proxy(function(e) {
/* 335  */                         this._focused_from = e.target;
/* 336  */                     }, this)
/* 337  */                 }],
/* 338  */                 // Input: listen for blur on element
/* 339  */                 [this.element, {
/* 340  */                     blur: $.proxy(function(e) {
/* 341  */                         this._focused_from = e.target;
/* 342  */                     }, this)
/* 343  */                 }]
/* 344  */             );
/* 345  */ 
/* 346  */             this._secondaryEvents = [
/* 347  */                 [this.picker, {
/* 348  */                     click: $.proxy(this.click, this)
/* 349  */                 }],
/* 350  */                 [$(window), {

/* bootstrap-datepicker.js */

/* 351  */                     resize: $.proxy(this.place, this)
/* 352  */                 }],
/* 353  */                 [$(document), {
/* 354  */                     'mousedown touchstart': $.proxy(function(e) {
/* 355  */                         // Clicked outside the datepicker, hide it
/* 356  */                         if (!(
/* 357  */                             this.element.is(e.target) ||
/* 358  */                             this.element.find(e.target).length ||
/* 359  */                             this.picker.is(e.target) ||
/* 360  */                             this.picker.find(e.target).length
/* 361  */                         )) {
/* 362  */                             this.hide();
/* 363  */                         }
/* 364  */                     }, this)
/* 365  */                 }]
/* 366  */             ];
/* 367  */         },
/* 368  */         _attachEvents: function() {
/* 369  */             this._detachEvents();
/* 370  */             this._applyEvents(this._events);
/* 371  */         },
/* 372  */         _detachEvents: function() {
/* 373  */             this._unapplyEvents(this._events);
/* 374  */         },
/* 375  */         _attachSecondaryEvents: function() {
/* 376  */             this._detachSecondaryEvents();
/* 377  */             this._applyEvents(this._secondaryEvents);
/* 378  */         },
/* 379  */         _detachSecondaryEvents: function() {
/* 380  */             this._unapplyEvents(this._secondaryEvents);
/* 381  */         },
/* 382  */         _trigger: function(event, altdate) {
/* 383  */             var date = altdate || this.dates.get(-1),
/* 384  */                 local_date = this._utc_to_local(date);
/* 385  */ 
/* 386  */             this.element.trigger({
/* 387  */                 type: event,
/* 388  */                 date: local_date,
/* 389  */                 dates: $.map(this.dates, this._utc_to_local),
/* 390  */                 format: $.proxy(function(ix, format) {
/* 391  */                     if (arguments.length === 0) {
/* 392  */                         ix = this.dates.length - 1;
/* 393  */                         format = this.o.format;
/* 394  */                     } else if (typeof ix === 'string') {
/* 395  */                         format = ix;
/* 396  */                         ix = this.dates.length - 1;
/* 397  */                     }
/* 398  */                     format = format || this.o.format;
/* 399  */                     var date = this.dates.get(ix);
/* 400  */                     return DPGlobal.formatDate(date, format, this.o.language);

/* bootstrap-datepicker.js */

/* 401  */                 }, this)
/* 402  */             });
/* 403  */         },
/* 404  */ 
/* 405  */         show: function() {
/* 406  */             if (!this.isInline)
/* 407  */                 this.picker.appendTo('body');
/* 408  */             this.picker.show();
/* 409  */             this.place();
/* 410  */             this._attachSecondaryEvents();
/* 411  */             this._trigger('show');
/* 412  */         },
/* 413  */ 
/* 414  */         hide: function() {
/* 415  */             if (this.isInline)
/* 416  */                 return;
/* 417  */             if (!this.picker.is(':visible'))
/* 418  */                 return;
/* 419  */             this.focusDate = null;
/* 420  */             this.picker.hide().detach();
/* 421  */             this._detachSecondaryEvents();
/* 422  */             this.viewMode = this.o.startView;
/* 423  */             this.showMode();
/* 424  */ 
/* 425  */             if (
/* 426  */                 this.o.forceParse &&
/* 427  */                 (
/* 428  */                     this.isInput && this.element.val() ||
/* 429  */                     this.hasInput && this.element.find('input').val()
/* 430  */                 )
/* 431  */             )
/* 432  */                 this.setValue();
/* 433  */             this._trigger('hide');
/* 434  */         },
/* 435  */ 
/* 436  */         remove: function() {
/* 437  */             this.hide();
/* 438  */             this._detachEvents();
/* 439  */             this._detachSecondaryEvents();
/* 440  */             this.picker.remove();
/* 441  */             delete this.element.data().datepicker;
/* 442  */             if (!this.isInput) {
/* 443  */                 delete this.element.data().date;
/* 444  */             }
/* 445  */         },
/* 446  */ 
/* 447  */         _utc_to_local: function(utc) {
/* 448  */             return utc && new Date(utc.getTime() + (utc.getTimezoneOffset() * 60000));
/* 449  */         },
/* 450  */         _local_to_utc: function(local) {

/* bootstrap-datepicker.js */

/* 451  */             return local && new Date(local.getTime() - (local.getTimezoneOffset() * 60000));
/* 452  */         },
/* 453  */         _zero_time: function(local) {
/* 454  */             return local && new Date(local.getFullYear(), local.getMonth(), local.getDate());
/* 455  */         },
/* 456  */         _zero_utc_time: function(utc) {
/* 457  */             return utc && new Date(Date.UTC(utc.getUTCFullYear(), utc.getUTCMonth(), utc.getUTCDate()));
/* 458  */         },
/* 459  */ 
/* 460  */         getDates: function() {
/* 461  */             return $.map(this.dates, this._utc_to_local);
/* 462  */         },
/* 463  */ 
/* 464  */         getUTCDates: function() {
/* 465  */             return $.map(this.dates, function(d) {
/* 466  */                 return new Date(d);
/* 467  */             });
/* 468  */         },
/* 469  */ 
/* 470  */         getDate: function() {
/* 471  */             return this._utc_to_local(this.getUTCDate());
/* 472  */         },
/* 473  */ 
/* 474  */         getUTCDate: function() {
/* 475  */             return new Date(this.dates.get(-1));
/* 476  */         },
/* 477  */ 
/* 478  */         setDates: function() {
/* 479  */             var args = $.isArray(arguments[0]) ? arguments[0] : arguments;
/* 480  */             this.update.apply(this, args);
/* 481  */             this._trigger('changeDate');
/* 482  */             this.setValue();
/* 483  */         },
/* 484  */ 
/* 485  */         setUTCDates: function() {
/* 486  */             var args = $.isArray(arguments[0]) ? arguments[0] : arguments;
/* 487  */             this.update.apply(this, $.map(args, this._utc_to_local));
/* 488  */             this._trigger('changeDate');
/* 489  */             this.setValue();
/* 490  */         },
/* 491  */ 
/* 492  */         setDate: alias('setDates'),
/* 493  */         setUTCDate: alias('setUTCDates'),
/* 494  */ 
/* 495  */         setValue: function() {
/* 496  */             var formatted = this.getFormattedDate();
/* 497  */             if (!this.isInput) {
/* 498  */                 if (this.component) {
/* 499  */                     this.element.find('input').val(formatted).change();
/* 500  */                 }

/* bootstrap-datepicker.js */

/* 501  */             } else {
/* 502  */                 this.element.val(formatted).change();
/* 503  */             }
/* 504  */         },
/* 505  */ 
/* 506  */         getFormattedDate: function(format) {
/* 507  */             if (format === undefined)
/* 508  */                 format = this.o.format;
/* 509  */ 
/* 510  */             var lang = this.o.language;
/* 511  */             return $.map(this.dates, function(d) {
/* 512  */                 return DPGlobal.formatDate(d, format, lang);
/* 513  */             }).join(this.o.multidateSeparator);
/* 514  */         },
/* 515  */ 
/* 516  */         setStartDate: function(startDate) {
/* 517  */             this._process_options({
/* 518  */                 startDate: startDate
/* 519  */             });
/* 520  */             this.update();
/* 521  */             this.updateNavArrows();
/* 522  */         },
/* 523  */ 
/* 524  */         setEndDate: function(endDate) {
/* 525  */             this._process_options({
/* 526  */                 endDate: endDate
/* 527  */             });
/* 528  */             this.update();
/* 529  */             this.updateNavArrows();
/* 530  */         },
/* 531  */ 
/* 532  */         setDaysOfWeekDisabled: function(daysOfWeekDisabled) {
/* 533  */             this._process_options({
/* 534  */                 daysOfWeekDisabled: daysOfWeekDisabled
/* 535  */             });
/* 536  */             this.update();
/* 537  */             this.updateNavArrows();
/* 538  */         },
/* 539  */ 
/* 540  */         place: function() {
/* 541  */             if (this.isInline)
/* 542  */                 return;
/* 543  */             var calendarWidth = this.picker.outerWidth(),
/* 544  */                 calendarHeight = this.picker.outerHeight(),
/* 545  */                 visualPadding = 10,
/* 546  */                 windowWidth = $window.width(),
/* 547  */                 windowHeight = $window.height(),
/* 548  */                 scrollTop = $window.scrollTop();
/* 549  */ 
/* 550  */             var zIndex = parseInt(this.element.parents().filter(function() {

/* bootstrap-datepicker.js */

/* 551  */                 return $(this).css('z-index') !== 'auto';
/* 552  */             }).first().css('z-index')) + 9999;
/* 553  */             var offset = this.component ? this.component.parent().offset() : this.element.offset();
/* 554  */             var height = this.component ? this.component.outerHeight(true) : this.element.outerHeight(false);
/* 555  */             var width = this.component ? this.component.outerWidth(true) : this.element.outerWidth(false);
/* 556  */             var left = offset.left,
/* 557  */                 top = offset.top;
/* 558  */ 
/* 559  */             this.picker.removeClass(
/* 560  */                 'datepicker-orient-top datepicker-orient-bottom ' +
/* 561  */                 'datepicker-orient-right datepicker-orient-left'
/* 562  */             );
/* 563  */ 
/* 564  */             if (this.o.orientation.x !== 'auto') {
/* 565  */                 this.picker.addClass('datepicker-orient-' + this.o.orientation.x);
/* 566  */                 if (this.o.orientation.x === 'right')
/* 567  */                     left -= calendarWidth - width;
/* 568  */             }
/* 569  */             // auto x orientation is best-placement: if it crosses a window
/* 570  */             // edge, fudge it sideways
/* 571  */             else {
/* 572  */                 // Default to left
/* 573  */                 this.picker.addClass('datepicker-orient-left');
/* 574  */                 if (offset.left < 0)
/* 575  */                     left -= offset.left - visualPadding;
/* 576  */                 else if (offset.left + calendarWidth > windowWidth)
/* 577  */                     left = windowWidth - calendarWidth - visualPadding;
/* 578  */             }
/* 579  */ 
/* 580  */             // auto y orientation is best-situation: top or bottom, no fudging,
/* 581  */             // decision based on which shows more of the calendar
/* 582  */             var yorient = this.o.orientation.y,
/* 583  */                 top_overflow, bottom_overflow;
/* 584  */             if (yorient === 'auto') {
/* 585  */                 top_overflow = -scrollTop + offset.top - calendarHeight;
/* 586  */                 bottom_overflow = scrollTop + windowHeight - (offset.top + height + calendarHeight);
/* 587  */                 if (Math.max(top_overflow, bottom_overflow) === bottom_overflow)
/* 588  */                     yorient = 'top';
/* 589  */                 else
/* 590  */                     yorient = 'bottom';
/* 591  */             }
/* 592  */             this.picker.addClass('datepicker-orient-' + yorient);
/* 593  */             if (yorient === 'top')
/* 594  */                 top += height;
/* 595  */             else
/* 596  */                 top -= calendarHeight + parseInt(this.picker.css('padding-top'));
/* 597  */ 
/* 598  */             this.picker.css({
/* 599  */                 top: top,
/* 600  */                 left: left,

/* bootstrap-datepicker.js */

/* 601  */                 zIndex: zIndex
/* 602  */             });
/* 603  */         },
/* 604  */ 
/* 605  */         _allow_update: true,
/* 606  */         update: function() {
/* 607  */             if (!this._allow_update)
/* 608  */                 return;
/* 609  */ 
/* 610  */             var oldDates = this.dates.copy(),
/* 611  */                 dates = [],
/* 612  */                 fromArgs = false;
/* 613  */             if (arguments.length) {
/* 614  */                 $.each(arguments, $.proxy(function(i, date) {
/* 615  */                     if (date instanceof Date)
/* 616  */                         date = this._local_to_utc(date);
/* 617  */                     dates.push(date);
/* 618  */                 }, this));
/* 619  */                 fromArgs = true;
/* 620  */             } else {
/* 621  */                 dates = this.isInput ? this.element.val() : this.element.data('date') || this.element.find('input').val();
/* 622  */                 if (dates && this.o.multidate)
/* 623  */                     dates = dates.split(this.o.multidateSeparator);
/* 624  */                 else
/* 625  */                     dates = [dates];
/* 626  */                 delete this.element.data().date;
/* 627  */             }
/* 628  */ 
/* 629  */             dates = $.map(dates, $.proxy(function(date) {
/* 630  */                 return DPGlobal.parseDate(date, this.o.format, this.o.language);
/* 631  */             }, this));
/* 632  */             dates = $.grep(dates, $.proxy(function(date) {
/* 633  */                 return (
/* 634  */                     date < this.o.startDate ||
/* 635  */                     date > this.o.endDate || !date
/* 636  */                 );
/* 637  */             }, this), true);
/* 638  */             this.dates.replace(dates);
/* 639  */ 
/* 640  */             if (this.dates.length)
/* 641  */                 this.viewDate = new Date(this.dates.get(-1));
/* 642  */             else if (this.viewDate < this.o.startDate)
/* 643  */                 this.viewDate = new Date(this.o.startDate);
/* 644  */             else if (this.viewDate > this.o.endDate)
/* 645  */                 this.viewDate = new Date(this.o.endDate);
/* 646  */ 
/* 647  */             if (fromArgs) {
/* 648  */                 // setting date by clicking
/* 649  */                 this.setValue();
/* 650  */             } else if (dates.length) {

/* bootstrap-datepicker.js */

/* 651  */                 // setting date by typing
/* 652  */                 if (String(oldDates) !== String(this.dates))
/* 653  */                     this._trigger('changeDate');
/* 654  */             }
/* 655  */             if (!this.dates.length && oldDates.length)
/* 656  */                 this._trigger('clearDate');
/* 657  */ 
/* 658  */             this.fill();
/* 659  */         },
/* 660  */ 
/* 661  */         fillDow: function() {
/* 662  */             var dowCnt = this.o.weekStart,
/* 663  */                 html = '<tr>';
/* 664  */             if (this.o.calendarWeeks) {
/* 665  */                 var cell = '<th class="cw">&nbsp;</th>';
/* 666  */                 html += cell;
/* 667  */                 this.picker.find('.datepicker-days thead tr:first-child').prepend(cell);
/* 668  */             }
/* 669  */             while (dowCnt < this.o.weekStart + 7) {
/* 670  */                 html += '<th class="dow">' + dates[this.o.language].daysMin[(dowCnt++) % 7] + '</th>';
/* 671  */             }
/* 672  */             html += '</tr>';
/* 673  */             this.picker.find('.datepicker-days thead').append(html);
/* 674  */         },
/* 675  */ 
/* 676  */         fillMonths: function() {
/* 677  */             var html = '',
/* 678  */                 i = 0;
/* 679  */             while (i < 12) {
/* 680  */                 html += '<span class="month">' + dates[this.o.language].monthsShort[i++] + '</span>';
/* 681  */             }
/* 682  */             this.picker.find('.datepicker-months td').html(html);
/* 683  */         },
/* 684  */ 
/* 685  */         setRange: function(range) {
/* 686  */             if (!range || !range.length)
/* 687  */                 delete this.range;
/* 688  */             else
/* 689  */                 this.range = $.map(range, function(d) {
/* 690  */                     return d.valueOf();
/* 691  */                 });
/* 692  */             this.fill();
/* 693  */         },
/* 694  */ 
/* 695  */         getClassNames: function(date) {
/* 696  */             var cls = [],
/* 697  */                 year = this.viewDate.getUTCFullYear(),
/* 698  */                 month = this.viewDate.getUTCMonth(),
/* 699  */                 today = new Date();
/* 700  */             if (date.getUTCFullYear() < year || (date.getUTCFullYear() === year && date.getUTCMonth() < month)) {

/* bootstrap-datepicker.js */

/* 701  */                 cls.push('old');
/* 702  */             } else if (date.getUTCFullYear() > year || (date.getUTCFullYear() === year && date.getUTCMonth() > month)) {
/* 703  */                 cls.push('new');
/* 704  */             }
/* 705  */             if (this.focusDate && date.valueOf() === this.focusDate.valueOf())
/* 706  */                 cls.push('focused');
/* 707  */             // Compare internal UTC date with local today, not UTC today
/* 708  */             if (this.o.todayHighlight &&
/* 709  */                 date.getUTCFullYear() === today.getFullYear() &&
/* 710  */                 date.getUTCMonth() === today.getMonth() &&
/* 711  */                 date.getUTCDate() === today.getDate()) {
/* 712  */                 cls.push('today');
/* 713  */             }
/* 714  */             if (this.dates.contains(date) !== -1)
/* 715  */                 cls.push('active');
/* 716  */             if (date.valueOf() < this.o.startDate || date.valueOf() > this.o.endDate ||
/* 717  */                 $.inArray(date.getUTCDay(), this.o.daysOfWeekDisabled) !== -1) {
/* 718  */                 cls.push('disabled');
/* 719  */             }
/* 720  */             if (this.range) {
/* 721  */                 if (date > this.range[0] && date < this.range[this.range.length - 1]) {
/* 722  */                     cls.push('range');
/* 723  */                 }
/* 724  */                 if ($.inArray(date.valueOf(), this.range) !== -1) {
/* 725  */                     cls.push('selected');
/* 726  */                 }
/* 727  */             }
/* 728  */             return cls;
/* 729  */         },
/* 730  */ 
/* 731  */         fill: function() {
/* 732  */             var d = new Date(this.viewDate),
/* 733  */                 year = d.getUTCFullYear(),
/* 734  */                 month = d.getUTCMonth(),
/* 735  */                 startYear = this.o.startDate !== -Infinity ? this.o.startDate.getUTCFullYear() : -Infinity,
/* 736  */                 startMonth = this.o.startDate !== -Infinity ? this.o.startDate.getUTCMonth() : -Infinity,
/* 737  */                 endYear = this.o.endDate !== Infinity ? this.o.endDate.getUTCFullYear() : Infinity,
/* 738  */                 endMonth = this.o.endDate !== Infinity ? this.o.endDate.getUTCMonth() : Infinity,
/* 739  */                 todaytxt = dates[this.o.language].today || dates['en'].today || '',
/* 740  */                 cleartxt = dates[this.o.language].clear || dates['en'].clear || '',
/* 741  */                 tooltip;
/* 742  */             this.picker.find('.datepicker-days thead th.datepicker-switch')
/* 743  */                 .text(dates[this.o.language].months[month] + ' ' + year);
/* 744  */             this.picker.find('tfoot th.today')
/* 745  */                 .text(todaytxt)
/* 746  */                 .toggle(this.o.todayBtn !== false);
/* 747  */             this.picker.find('tfoot th.clear')
/* 748  */                 .text(cleartxt)
/* 749  */                 .toggle(this.o.clearBtn !== false);
/* 750  */             this.updateNavArrows();

/* bootstrap-datepicker.js */

/* 751  */             this.fillMonths();
/* 752  */             var prevMonth = UTCDate(year, month - 1, 28),
/* 753  */                 day = DPGlobal.getDaysInMonth(prevMonth.getUTCFullYear(), prevMonth.getUTCMonth());
/* 754  */             prevMonth.setUTCDate(day);
/* 755  */             prevMonth.setUTCDate(day - (prevMonth.getUTCDay() - this.o.weekStart + 7) % 7);
/* 756  */             var nextMonth = new Date(prevMonth);
/* 757  */             nextMonth.setUTCDate(nextMonth.getUTCDate() + 42);
/* 758  */             nextMonth = nextMonth.valueOf();
/* 759  */             var html = [];
/* 760  */             var clsName;
/* 761  */             while (prevMonth.valueOf() < nextMonth) {
/* 762  */                 if (prevMonth.getUTCDay() === this.o.weekStart) {
/* 763  */                     html.push('<tr>');
/* 764  */                     if (this.o.calendarWeeks) {
/* 765  */                         // ISO 8601: First week contains first thursday.
/* 766  */                         // ISO also states week starts on Monday, but we can be more abstract here.
/* 767  */                         var
/* 768  */                         // Start of current week: based on weekstart/current date
/* 769  */                         ws = new Date(+prevMonth + (this.o.weekStart - prevMonth.getUTCDay() - 7) % 7 * 864e5),
/* 770  */                             // Thursday of this week
/* 771  */                             th = new Date(Number(ws) + (7 + 4 - ws.getUTCDay()) % 7 * 864e5),
/* 772  */                             // First Thursday of year, year from thursday
/* 773  */                             yth = new Date(Number(yth = UTCDate(th.getUTCFullYear(), 0, 1)) + (7 + 4 - yth.getUTCDay()) % 7 * 864e5),
/* 774  */                             // Calendar week: ms between thursdays, div ms per day, div 7 days
/* 775  */                             calWeek = (th - yth) / 864e5 / 7 + 1;
/* 776  */                         html.push('<td class="cw">' + calWeek + '</td>');
/* 777  */ 
/* 778  */                     }
/* 779  */                 }
/* 780  */                 clsName = this.getClassNames(prevMonth);
/* 781  */                 clsName.push('day');
/* 782  */ 
/* 783  */                 if (this.o.beforeShowDay !== $.noop) {
/* 784  */                     var before = this.o.beforeShowDay(this._utc_to_local(prevMonth));
/* 785  */                     if (before === undefined)
/* 786  */                         before = {};
/* 787  */                     else if (typeof(before) === 'boolean')
/* 788  */                         before = {
/* 789  */                             enabled: before
/* 790  */                         };
/* 791  */                     else if (typeof(before) === 'string')
/* 792  */                         before = {
/* 793  */                             classes: before
/* 794  */                         };
/* 795  */                     if (before.enabled === false)
/* 796  */                         clsName.push('disabled');
/* 797  */                     if (before.classes)
/* 798  */                         clsName = clsName.concat(before.classes.split(/\s+/));
/* 799  */                     if (before.tooltip)
/* 800  */                         tooltip = before.tooltip;

/* bootstrap-datepicker.js */

/* 801  */                 }
/* 802  */ 
/* 803  */                 clsName = $.unique(clsName);
/* 804  */                 html.push('<td class="' + clsName.join(' ') + '"' + (tooltip ? ' title="' + tooltip + '"' : '') + '>' + prevMonth.getUTCDate() + '</td>');
/* 805  */                 if (prevMonth.getUTCDay() === this.o.weekEnd) {
/* 806  */                     html.push('</tr>');
/* 807  */                 }
/* 808  */                 prevMonth.setUTCDate(prevMonth.getUTCDate() + 1);
/* 809  */             }
/* 810  */             this.picker.find('.datepicker-days tbody').empty().append(html.join(''));
/* 811  */ 
/* 812  */             var months = this.picker.find('.datepicker-months')
/* 813  */                 .find('th:eq(1)')
/* 814  */                 .text(year)
/* 815  */                 .end()
/* 816  */                 .find('span').removeClass('active');
/* 817  */ 
/* 818  */             $.each(this.dates, function(i, d) {
/* 819  */                 if (d.getUTCFullYear() === year)
/* 820  */                     months.eq(d.getUTCMonth()).addClass('active');
/* 821  */             });
/* 822  */ 
/* 823  */             if (year < startYear || year > endYear) {
/* 824  */                 months.addClass('disabled');
/* 825  */             }
/* 826  */             if (year === startYear) {
/* 827  */                 months.slice(0, startMonth).addClass('disabled');
/* 828  */             }
/* 829  */             if (year === endYear) {
/* 830  */                 months.slice(endMonth + 1).addClass('disabled');
/* 831  */             }
/* 832  */ 
/* 833  */             html = '';
/* 834  */             year = parseInt(year / 10, 10) * 10;
/* 835  */             var yearCont = this.picker.find('.datepicker-years')
/* 836  */                 .find('th:eq(1)')
/* 837  */                 .text(year + '-' + (year + 9))
/* 838  */                 .end()
/* 839  */                 .find('td');
/* 840  */             year -= 1;
/* 841  */             var years = $.map(this.dates, function(d) {
/* 842  */                 return d.getUTCFullYear();
/* 843  */             }),
/* 844  */                 classes;
/* 845  */             for (var i = -1; i < 11; i++) {
/* 846  */                 classes = ['year'];
/* 847  */                 if (i === -1)
/* 848  */                     classes.push('old');
/* 849  */                 else if (i === 10)
/* 850  */                     classes.push('new');

/* bootstrap-datepicker.js */

/* 851  */                 if ($.inArray(year, years) !== -1)
/* 852  */                     classes.push('active');
/* 853  */                 if (year < startYear || year > endYear)
/* 854  */                     classes.push('disabled');
/* 855  */                 html += '<span class="' + classes.join(' ') + '">' + year + '</span>';
/* 856  */                 year += 1;
/* 857  */             }
/* 858  */             yearCont.html(html);
/* 859  */         },
/* 860  */ 
/* 861  */         updateNavArrows: function() {
/* 862  */             if (!this._allow_update)
/* 863  */                 return;
/* 864  */ 
/* 865  */             var d = new Date(this.viewDate),
/* 866  */                 year = d.getUTCFullYear(),
/* 867  */                 month = d.getUTCMonth();
/* 868  */             switch (this.viewMode) {
/* 869  */                 case 0:
/* 870  */                     if (this.o.startDate !== -Infinity && year <= this.o.startDate.getUTCFullYear() && month <= this.o.startDate.getUTCMonth()) {
/* 871  */                         this.picker.find('.prev').css({
/* 872  */                             visibility: 'hidden'
/* 873  */                         });
/* 874  */                     } else {
/* 875  */                         this.picker.find('.prev').css({
/* 876  */                             visibility: 'visible'
/* 877  */                         });
/* 878  */                     }
/* 879  */                     if (this.o.endDate !== Infinity && year >= this.o.endDate.getUTCFullYear() && month >= this.o.endDate.getUTCMonth()) {
/* 880  */                         this.picker.find('.next').css({
/* 881  */                             visibility: 'hidden'
/* 882  */                         });
/* 883  */                     } else {
/* 884  */                         this.picker.find('.next').css({
/* 885  */                             visibility: 'visible'
/* 886  */                         });
/* 887  */                     }
/* 888  */                     break;
/* 889  */                 case 1:
/* 890  */                 case 2:
/* 891  */                     if (this.o.startDate !== -Infinity && year <= this.o.startDate.getUTCFullYear()) {
/* 892  */                         this.picker.find('.prev').css({
/* 893  */                             visibility: 'hidden'
/* 894  */                         });
/* 895  */                     } else {
/* 896  */                         this.picker.find('.prev').css({
/* 897  */                             visibility: 'visible'
/* 898  */                         });
/* 899  */                     }
/* 900  */                     if (this.o.endDate !== Infinity && year >= this.o.endDate.getUTCFullYear()) {

/* bootstrap-datepicker.js */

/* 901  */                         this.picker.find('.next').css({
/* 902  */                             visibility: 'hidden'
/* 903  */                         });
/* 904  */                     } else {
/* 905  */                         this.picker.find('.next').css({
/* 906  */                             visibility: 'visible'
/* 907  */                         });
/* 908  */                     }
/* 909  */                     break;
/* 910  */             }
/* 911  */         },
/* 912  */ 
/* 913  */         click: function(e) {
/* 914  */             e.preventDefault();
/* 915  */             var target = $(e.target).closest('span, td, th'),
/* 916  */                 year, month, day;
/* 917  */             if (target.length === 1) {
/* 918  */                 switch (target[0].nodeName.toLowerCase()) {
/* 919  */                     case 'th':
/* 920  */                         switch (target[0].className) {
/* 921  */                             case 'datepicker-switch':
/* 922  */                                 this.showMode(1);
/* 923  */                                 break;
/* 924  */                             case 'prev':
/* 925  */                             case 'next':
/* 926  */                                 var dir = DPGlobal.modes[this.viewMode].navStep * (target[0].className === 'prev' ? -1 : 1);
/* 927  */                                 switch (this.viewMode) {
/* 928  */                                     case 0:
/* 929  */                                         this.viewDate = this.moveMonth(this.viewDate, dir);
/* 930  */                                         this._trigger('changeMonth', this.viewDate);
/* 931  */                                         break;
/* 932  */                                     case 1:
/* 933  */                                     case 2:
/* 934  */                                         this.viewDate = this.moveYear(this.viewDate, dir);
/* 935  */                                         if (this.viewMode === 1)
/* 936  */                                             this._trigger('changeYear', this.viewDate);
/* 937  */                                         break;
/* 938  */                                 }
/* 939  */                                 this.fill();
/* 940  */                                 break;
/* 941  */                             case 'today':
/* 942  */                                 var date = new Date();
/* 943  */                                 date = UTCDate(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0);
/* 944  */ 
/* 945  */                                 this.showMode(-2);
/* 946  */                                 var which = this.o.todayBtn === 'linked' ? null : 'view';
/* 947  */                                 this._setDate(date, which);
/* 948  */                                 break;
/* 949  */                             case 'clear':
/* 950  */                                 var element;

/* bootstrap-datepicker.js */

/* 951  */                                 if (this.isInput)
/* 952  */                                     element = this.element;
/* 953  */                                 else if (this.component)
/* 954  */                                     element = this.element.find('input');
/* 955  */                                 if (element)
/* 956  */                                     element.val("").change();
/* 957  */                                 this.update();
/* 958  */                                 this._trigger('changeDate');
/* 959  */                                 if (this.o.autoclose)
/* 960  */                                     this.hide();
/* 961  */                                 break;
/* 962  */                         }
/* 963  */                         break;
/* 964  */                     case 'span':
/* 965  */                         if (!target.is('.disabled')) {
/* 966  */                             this.viewDate.setUTCDate(1);
/* 967  */                             if (target.is('.month')) {
/* 968  */                                 day = 1;
/* 969  */                                 month = target.parent().find('span').index(target);
/* 970  */                                 year = this.viewDate.getUTCFullYear();
/* 971  */                                 this.viewDate.setUTCMonth(month);
/* 972  */                                 this._trigger('changeMonth', this.viewDate);
/* 973  */                                 if (this.o.minViewMode === 1) {
/* 974  */                                     this._setDate(UTCDate(year, month, day));
/* 975  */                                 }
/* 976  */                             } else {
/* 977  */                                 day = 1;
/* 978  */                                 month = 0;
/* 979  */                                 year = parseInt(target.text(), 10) || 0;
/* 980  */                                 this.viewDate.setUTCFullYear(year);
/* 981  */                                 this._trigger('changeYear', this.viewDate);
/* 982  */                                 if (this.o.minViewMode === 2) {
/* 983  */                                     this._setDate(UTCDate(year, month, day));
/* 984  */                                 }
/* 985  */                             }
/* 986  */                             this.showMode(-1);
/* 987  */                             this.fill();
/* 988  */                         }
/* 989  */                         break;
/* 990  */                     case 'td':
/* 991  */                         if (target.is('.day') && !target.is('.disabled')) {
/* 992  */                             day = parseInt(target.text(), 10) || 1;
/* 993  */                             year = this.viewDate.getUTCFullYear();
/* 994  */                             month = this.viewDate.getUTCMonth();
/* 995  */                             if (target.is('.old')) {
/* 996  */                                 if (month === 0) {
/* 997  */                                     month = 11;
/* 998  */                                     year -= 1;
/* 999  */                                 } else {
/* 1000 */                                     month -= 1;

/* bootstrap-datepicker.js */

/* 1001 */                                 }
/* 1002 */                             } else if (target.is('.new')) {
/* 1003 */                                 if (month === 11) {
/* 1004 */                                     month = 0;
/* 1005 */                                     year += 1;
/* 1006 */                                 } else {
/* 1007 */                                     month += 1;
/* 1008 */                                 }
/* 1009 */                             }
/* 1010 */                             this._setDate(UTCDate(year, month, day));
/* 1011 */                         }
/* 1012 */                         break;
/* 1013 */                 }
/* 1014 */             }
/* 1015 */             if (this.picker.is(':visible') && this._focused_from) {
/* 1016 */                 $(this._focused_from).focus();
/* 1017 */             }
/* 1018 */             delete this._focused_from;
/* 1019 */         },
/* 1020 */ 
/* 1021 */         _toggle_multidate: function(date) {
/* 1022 */             var ix = this.dates.contains(date);
/* 1023 */             if (!date) {
/* 1024 */                 this.dates.clear();
/* 1025 */             } else if (ix !== -1) {
/* 1026 */                 this.dates.remove(ix);
/* 1027 */             } else {
/* 1028 */                 this.dates.push(date);
/* 1029 */             }
/* 1030 */             if (typeof this.o.multidate === 'number')
/* 1031 */                 while (this.dates.length > this.o.multidate)
/* 1032 */                     this.dates.remove(0);
/* 1033 */         },
/* 1034 */ 
/* 1035 */         _setDate: function(date, which) {
/* 1036 */             if (!which || which === 'date')
/* 1037 */                 this._toggle_multidate(date && new Date(date));
/* 1038 */             if (!which || which === 'view')
/* 1039 */                 this.viewDate = date && new Date(date);
/* 1040 */ 
/* 1041 */             this.fill();
/* 1042 */             this.setValue();
/* 1043 */             this._trigger('changeDate');
/* 1044 */             var element;
/* 1045 */             if (this.isInput) {
/* 1046 */                 element = this.element;
/* 1047 */             } else if (this.component) {
/* 1048 */                 element = this.element.find('input');
/* 1049 */             }
/* 1050 */             if (element) {

/* bootstrap-datepicker.js */

/* 1051 */                 element.change();
/* 1052 */             }
/* 1053 */             if (this.o.autoclose && (!which || which === 'date')) {
/* 1054 */                 this.hide();
/* 1055 */             }
/* 1056 */         },
/* 1057 */ 
/* 1058 */         moveMonth: function(date, dir) {
/* 1059 */             if (!date)
/* 1060 */                 return undefined;
/* 1061 */             if (!dir)
/* 1062 */                 return date;
/* 1063 */             var new_date = new Date(date.valueOf()),
/* 1064 */                 day = new_date.getUTCDate(),
/* 1065 */                 month = new_date.getUTCMonth(),
/* 1066 */                 mag = Math.abs(dir),
/* 1067 */                 new_month, test;
/* 1068 */             dir = dir > 0 ? 1 : -1;
/* 1069 */             if (mag === 1) {
/* 1070 */                 test = dir === -1
/* 1071 */                 // If going back one month, make sure month is not current month
/* 1072 */                 // (eg, Mar 31 -> Feb 31 == Feb 28, not Mar 02)
/* 1073 */                 ? function() {
/* 1074 */                     return new_date.getUTCMonth() === month;
/* 1075 */                 }
/* 1076 */                 // If going forward one month, make sure month is as expected
/* 1077 */                 // (eg, Jan 31 -> Feb 31 == Feb 28, not Mar 02)
/* 1078 */                 : function() {
/* 1079 */                     return new_date.getUTCMonth() !== new_month;
/* 1080 */                 };
/* 1081 */                 new_month = month + dir;
/* 1082 */                 new_date.setUTCMonth(new_month);
/* 1083 */                 // Dec -> Jan (12) or Jan -> Dec (-1) -- limit expected date to 0-11
/* 1084 */                 if (new_month < 0 || new_month > 11)
/* 1085 */                     new_month = (new_month + 12) % 12;
/* 1086 */             } else {
/* 1087 */                 // For magnitudes >1, move one month at a time...
/* 1088 */                 for (var i = 0; i < mag; i++)
/* 1089 */                 // ...which might decrease the day (eg, Jan 31 to Feb 28, etc)...
/* 1090 */                     new_date = this.moveMonth(new_date, dir);
/* 1091 */                 // ...then reset the day, keeping it in the new month
/* 1092 */                 new_month = new_date.getUTCMonth();
/* 1093 */                 new_date.setUTCDate(day);
/* 1094 */                 test = function() {
/* 1095 */                     return new_month !== new_date.getUTCMonth();
/* 1096 */                 };
/* 1097 */             }
/* 1098 */             // Common date-resetting loop -- if date is beyond end of month, make it
/* 1099 */             // end of month
/* 1100 */             while (test()) {

/* bootstrap-datepicker.js */

/* 1101 */                 new_date.setUTCDate(--day);
/* 1102 */                 new_date.setUTCMonth(new_month);
/* 1103 */             }
/* 1104 */             return new_date;
/* 1105 */         },
/* 1106 */ 
/* 1107 */         moveYear: function(date, dir) {
/* 1108 */             return this.moveMonth(date, dir * 12);
/* 1109 */         },
/* 1110 */ 
/* 1111 */         dateWithinRange: function(date) {
/* 1112 */             return date >= this.o.startDate && date <= this.o.endDate;
/* 1113 */         },
/* 1114 */ 
/* 1115 */         keydown: function(e) {
/* 1116 */             if (this.picker.is(':not(:visible)')) {
/* 1117 */                 if (e.keyCode === 27) // allow escape to hide and re-show picker
/* 1118 */                     this.show();
/* 1119 */                 return;
/* 1120 */             }
/* 1121 */             var dateChanged = false,
/* 1122 */                 dir, newDate, newViewDate,
/* 1123 */                 focusDate = this.focusDate || this.viewDate;
/* 1124 */             switch (e.keyCode) {
/* 1125 */                 case 27: // escape
/* 1126 */                     if (this.focusDate) {
/* 1127 */                         this.focusDate = null;
/* 1128 */                         this.viewDate = this.dates.get(-1) || this.viewDate;
/* 1129 */                         this.fill();
/* 1130 */                     } else
/* 1131 */                         this.hide();
/* 1132 */                     e.preventDefault();
/* 1133 */                     break;
/* 1134 */                 case 37: // left
/* 1135 */                 case 39: // right
/* 1136 */                     if (!this.o.keyboardNavigation)
/* 1137 */                         break;
/* 1138 */                     dir = e.keyCode === 37 ? -1 : 1;
/* 1139 */                     if (e.ctrlKey) {
/* 1140 */                         newDate = this.moveYear(this.dates.get(-1) || UTCToday(), dir);
/* 1141 */                         newViewDate = this.moveYear(focusDate, dir);
/* 1142 */                         this._trigger('changeYear', this.viewDate);
/* 1143 */                     } else if (e.shiftKey) {
/* 1144 */                         newDate = this.moveMonth(this.dates.get(-1) || UTCToday(), dir);
/* 1145 */                         newViewDate = this.moveMonth(focusDate, dir);
/* 1146 */                         this._trigger('changeMonth', this.viewDate);
/* 1147 */                     } else {
/* 1148 */                         newDate = new Date(this.dates.get(-1) || UTCToday());
/* 1149 */                         newDate.setUTCDate(newDate.getUTCDate() + dir);
/* 1150 */                         newViewDate = new Date(focusDate);

/* bootstrap-datepicker.js */

/* 1151 */                         newViewDate.setUTCDate(focusDate.getUTCDate() + dir);
/* 1152 */                     }
/* 1153 */                     if (this.dateWithinRange(newDate)) {
/* 1154 */                         this.focusDate = this.viewDate = newViewDate;
/* 1155 */                         this.setValue();
/* 1156 */                         this.fill();
/* 1157 */                         e.preventDefault();
/* 1158 */                     }
/* 1159 */                     break;
/* 1160 */                 case 38: // up
/* 1161 */                 case 40: // down
/* 1162 */                     if (!this.o.keyboardNavigation)
/* 1163 */                         break;
/* 1164 */                     dir = e.keyCode === 38 ? -1 : 1;
/* 1165 */                     if (e.ctrlKey) {
/* 1166 */                         newDate = this.moveYear(this.dates.get(-1) || UTCToday(), dir);
/* 1167 */                         newViewDate = this.moveYear(focusDate, dir);
/* 1168 */                         this._trigger('changeYear', this.viewDate);
/* 1169 */                     } else if (e.shiftKey) {
/* 1170 */                         newDate = this.moveMonth(this.dates.get(-1) || UTCToday(), dir);
/* 1171 */                         newViewDate = this.moveMonth(focusDate, dir);
/* 1172 */                         this._trigger('changeMonth', this.viewDate);
/* 1173 */                     } else {
/* 1174 */                         newDate = new Date(this.dates.get(-1) || UTCToday());
/* 1175 */                         newDate.setUTCDate(newDate.getUTCDate() + dir * 7);
/* 1176 */                         newViewDate = new Date(focusDate);
/* 1177 */                         newViewDate.setUTCDate(focusDate.getUTCDate() + dir * 7);
/* 1178 */                     }
/* 1179 */                     if (this.dateWithinRange(newDate)) {
/* 1180 */                         this.focusDate = this.viewDate = newViewDate;
/* 1181 */                         this.setValue();
/* 1182 */                         this.fill();
/* 1183 */                         e.preventDefault();
/* 1184 */                     }
/* 1185 */                     break;
/* 1186 */                 case 32: // spacebar
/* 1187 */                     // Spacebar is used in manually typing dates in some formats.
/* 1188 */                     // As such, its behavior should not be hijacked.
/* 1189 */                     break;
/* 1190 */                 case 13: // enter
/* 1191 */                     focusDate = this.focusDate || this.dates.get(-1) || this.viewDate;
/* 1192 */                     this._toggle_multidate(focusDate);
/* 1193 */                     dateChanged = true;
/* 1194 */                     this.focusDate = null;
/* 1195 */                     this.viewDate = this.dates.get(-1) || this.viewDate;
/* 1196 */                     this.setValue();
/* 1197 */                     this.fill();
/* 1198 */                     if (this.picker.is(':visible')) {
/* 1199 */                         e.preventDefault();
/* 1200 */                         if (this.o.autoclose)

/* bootstrap-datepicker.js */

/* 1201 */                             this.hide();
/* 1202 */                     }
/* 1203 */                     break;
/* 1204 */                 case 9: // tab
/* 1205 */                     this.focusDate = null;
/* 1206 */                     this.viewDate = this.dates.get(-1) || this.viewDate;
/* 1207 */                     this.fill();
/* 1208 */                     this.hide();
/* 1209 */                     break;
/* 1210 */             }
/* 1211 */             if (dateChanged) {
/* 1212 */                 if (this.dates.length)
/* 1213 */                     this._trigger('changeDate');
/* 1214 */                 else
/* 1215 */                     this._trigger('clearDate');
/* 1216 */                 var element;
/* 1217 */                 if (this.isInput) {
/* 1218 */                     element = this.element;
/* 1219 */                 } else if (this.component) {
/* 1220 */                     element = this.element.find('input');
/* 1221 */                 }
/* 1222 */                 if (element) {
/* 1223 */                     element.change();
/* 1224 */                 }
/* 1225 */             }
/* 1226 */         },
/* 1227 */ 
/* 1228 */         showMode: function(dir) {
/* 1229 */             if (dir) {
/* 1230 */                 this.viewMode = Math.max(this.o.minViewMode, Math.min(2, this.viewMode + dir));
/* 1231 */             }
/* 1232 */             this.picker
/* 1233 */                 .find('>div')
/* 1234 */                 .hide()
/* 1235 */                 .filter('.datepicker-' + DPGlobal.modes[this.viewMode].clsName)
/* 1236 */                 .css('display', 'block');
/* 1237 */             this.updateNavArrows();
/* 1238 */         }
/* 1239 */     };
/* 1240 */ 
/* 1241 */     var DateRangePicker = function(element, options) {
/* 1242 */         this.element = $(element);
/* 1243 */         this.inputs = $.map(options.inputs, function(i) {
/* 1244 */             return i.jquery ? i[0] : i;
/* 1245 */         });
/* 1246 */         delete options.inputs;
/* 1247 */ 
/* 1248 */         $(this.inputs)
/* 1249 */             .datepicker(options)
/* 1250 */             .bind('changeDate', $.proxy(this.dateUpdated, this));

/* bootstrap-datepicker.js */

/* 1251 */ 
/* 1252 */         this.pickers = $.map(this.inputs, function(i) {
/* 1253 */             return $(i).data('datepicker');
/* 1254 */         });
/* 1255 */         this.updateDates();
/* 1256 */     };
/* 1257 */     DateRangePicker.prototype = {
/* 1258 */         updateDates: function() {
/* 1259 */             this.dates = $.map(this.pickers, function(i) {
/* 1260 */                 return i.getUTCDate();
/* 1261 */             });
/* 1262 */             this.updateRanges();
/* 1263 */         },
/* 1264 */         updateRanges: function() {
/* 1265 */             var range = $.map(this.dates, function(d) {
/* 1266 */                 return d.valueOf();
/* 1267 */             });
/* 1268 */             $.each(this.pickers, function(i, p) {
/* 1269 */                 p.setRange(range);
/* 1270 */             });
/* 1271 */         },
/* 1272 */         dateUpdated: function(e) {
/* 1273 */             // `this.updating` is a workaround for preventing infinite recursion
/* 1274 */             // between `changeDate` triggering and `setUTCDate` calling.  Until
/* 1275 */             // there is a better mechanism.
/* 1276 */             if (this.updating)
/* 1277 */                 return;
/* 1278 */             this.updating = true;
/* 1279 */ 
/* 1280 */             var dp = $(e.target).data('datepicker'),
/* 1281 */                 new_date = dp.getUTCDate(),
/* 1282 */                 i = $.inArray(e.target, this.inputs),
/* 1283 */                 l = this.inputs.length;
/* 1284 */             if (i === -1)
/* 1285 */                 return;
/* 1286 */ 
/* 1287 */             $.each(this.pickers, function(i, p) {
/* 1288 */                 if (!p.getUTCDate())
/* 1289 */                     p.setUTCDate(new_date);
/* 1290 */             });
/* 1291 */ 
/* 1292 */             if (new_date < this.dates[i]) {
/* 1293 */                 // Date being moved earlier/left
/* 1294 */                 while (i >= 0 && new_date < this.dates[i]) {
/* 1295 */                     this.pickers[i--].setUTCDate(new_date);
/* 1296 */                 }
/* 1297 */             } else if (new_date > this.dates[i]) {
/* 1298 */                 // Date being moved later/right
/* 1299 */                 while (i < l && new_date > this.dates[i]) {
/* 1300 */                     this.pickers[i++].setUTCDate(new_date);

/* bootstrap-datepicker.js */

/* 1301 */                 }
/* 1302 */             }
/* 1303 */             this.updateDates();
/* 1304 */ 
/* 1305 */             delete this.updating;
/* 1306 */         },
/* 1307 */         remove: function() {
/* 1308 */             $.map(this.pickers, function(p) {
/* 1309 */                 p.remove();
/* 1310 */             });
/* 1311 */             delete this.element.data().datepicker;
/* 1312 */         }
/* 1313 */     };
/* 1314 */ 
/* 1315 */     function opts_from_el(el, prefix) {
/* 1316 */         // Derive options from element data-attrs
/* 1317 */         var data = $(el).data(),
/* 1318 */             out = {}, inkey,
/* 1319 */             replace = new RegExp('^' + prefix.toLowerCase() + '([A-Z])');
/* 1320 */         prefix = new RegExp('^' + prefix.toLowerCase());
/* 1321 */ 
/* 1322 */         function re_lower(_, a) {
/* 1323 */             return a.toLowerCase();
/* 1324 */         }
/* 1325 */         for (var key in data)
/* 1326 */             if (prefix.test(key)) {
/* 1327 */                 inkey = key.replace(replace, re_lower);
/* 1328 */                 out[inkey] = data[key];
/* 1329 */             }
/* 1330 */         return out;
/* 1331 */     }
/* 1332 */ 
/* 1333 */     function opts_from_locale(lang) {
/* 1334 */         // Derive options from locale plugins
/* 1335 */         var out = {};
/* 1336 */         // Check if "de-DE" style date is available, if not language should
/* 1337 */         // fallback to 2 letter code eg "de"
/* 1338 */         if (!dates[lang]) {
/* 1339 */             lang = lang.split('-')[0];
/* 1340 */             if (!dates[lang])
/* 1341 */                 return;
/* 1342 */         }
/* 1343 */         var d = dates[lang];
/* 1344 */         $.each(locale_opts, function(i, k) {
/* 1345 */             if (k in d)
/* 1346 */                 out[k] = d[k];
/* 1347 */         });
/* 1348 */         return out;
/* 1349 */     }
/* 1350 */ 

/* bootstrap-datepicker.js */

/* 1351 */     var old = $.fn.datepicker;
/* 1352 */     $.fn.datepicker = function(option) {
/* 1353 */         var args = Array.apply(null, arguments);
/* 1354 */         args.shift();
/* 1355 */         var internal_return;
/* 1356 */         this.each(function() {
/* 1357 */             var $this = $(this),
/* 1358 */                 data = $this.data('datepicker'),
/* 1359 */                 options = typeof option === 'object' && option;
/* 1360 */             if (!data) {
/* 1361 */                 var elopts = opts_from_el(this, 'date'),
/* 1362 */                     // Preliminary otions
/* 1363 */                     xopts = $.extend({}, defaults, elopts, options),
/* 1364 */                     locopts = opts_from_locale(xopts.language),
/* 1365 */                     // Options priority: js args, data-attrs, locales, defaults
/* 1366 */                     opts = $.extend({}, defaults, locopts, elopts, options);
/* 1367 */                 if ($this.is('.input-daterange') || opts.inputs) {
/* 1368 */                     var ropts = {
/* 1369 */                         inputs: opts.inputs || $this.find('input[name="start"], input[name="end"]').toArray()
/* 1370 */                     };
/* 1371 */                     $this.data('datepicker', (data = new DateRangePicker(this, $.extend(opts, ropts))));
/* 1372 */                 } else {
/* 1373 */                     $this.data('datepicker', (data = new Datepicker(this, opts)));
/* 1374 */                 }
/* 1375 */             }
/* 1376 */             if (typeof option === 'string' && typeof data[option] === 'function') {
/* 1377 */                 internal_return = data[option].apply(data, args);
/* 1378 */                 if (internal_return !== undefined)
/* 1379 */                     return false;
/* 1380 */             }
/* 1381 */         });
/* 1382 */         if (internal_return !== undefined)
/* 1383 */             return internal_return;
/* 1384 */         else
/* 1385 */             return this;
/* 1386 */     };
/* 1387 */ 
/* 1388 */     var defaults = $.fn.datepicker.defaults = {
/* 1389 */         autoclose: false,
/* 1390 */         beforeShowDay: $.noop,
/* 1391 */         calendarWeeks: false,
/* 1392 */         clearBtn: false,
/* 1393 */         daysOfWeekDisabled: [],
/* 1394 */         endDate: Infinity,
/* 1395 */         forceParse: true,
/* 1396 */         format: 'mm/dd/yyyy',
/* 1397 */         keyboardNavigation: true,
/* 1398 */         language: 'en',
/* 1399 */         minViewMode: 0,
/* 1400 */         multidate: false,

/* bootstrap-datepicker.js */

/* 1401 */         multidateSeparator: ',',
/* 1402 */         orientation: "auto",
/* 1403 */         rtl: false,
/* 1404 */         startDate: -Infinity,
/* 1405 */         startView: 0,
/* 1406 */         todayBtn: false,
/* 1407 */         todayHighlight: false,
/* 1408 */         weekStart: 0
/* 1409 */     };
/* 1410 */     var locale_opts = $.fn.datepicker.locale_opts = [
/* 1411 */         'format',
/* 1412 */         'rtl',
/* 1413 */         'weekStart'
/* 1414 */     ];
/* 1415 */     $.fn.datepicker.Constructor = Datepicker;
/* 1416 */     var dates = $.fn.datepicker.dates = {
/* 1417 */         en: {
/* 1418 */             days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
/* 1419 */             daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
/* 1420 */             daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"],
/* 1421 */             months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
/* 1422 */             monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
/* 1423 */             today: "Today",
/* 1424 */             clear: "Clear"
/* 1425 */         }
/* 1426 */     };
/* 1427 */ 
/* 1428 */     var DPGlobal = {
/* 1429 */         modes: [{
/* 1430 */             clsName: 'days',
/* 1431 */             navFnc: 'Month',
/* 1432 */             navStep: 1
/* 1433 */         }, {
/* 1434 */             clsName: 'months',
/* 1435 */             navFnc: 'FullYear',
/* 1436 */             navStep: 1
/* 1437 */         }, {
/* 1438 */             clsName: 'years',
/* 1439 */             navFnc: 'FullYear',
/* 1440 */             navStep: 10
/* 1441 */         }],
/* 1442 */         isLeapYear: function(year) {
/* 1443 */             return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0));
/* 1444 */         },
/* 1445 */         getDaysInMonth: function(year, month) {
/* 1446 */             return [31, (DPGlobal.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
/* 1447 */         },
/* 1448 */         validParts: /dd?|DD?|mm?|MM?|yy(?:yy)?/g,
/* 1449 */         nonpunctuation: /[^ -\/:-@\[\u3400-\u9fff-`{-~\t\n\r]+/g,
/* 1450 */         parseFormat: function(format) {

/* bootstrap-datepicker.js */

/* 1451 */             // IE treats \0 as a string end in inputs (truncating the value),
/* 1452 */             // so it's a bad format delimiter, anyway
/* 1453 */             var separators = format.replace(this.validParts, '\0').split('\0'),
/* 1454 */                 parts = format.match(this.validParts);
/* 1455 */             if (!separators || !separators.length || !parts || parts.length === 0) {
/* 1456 */                 throw new Error("Invalid date format.");
/* 1457 */             }
/* 1458 */             return {
/* 1459 */                 separators: separators,
/* 1460 */                 parts: parts
/* 1461 */             };
/* 1462 */         },
/* 1463 */         parseDate: function(date, format, language) {
/* 1464 */             if (!date)
/* 1465 */                 return undefined;
/* 1466 */             if (date instanceof Date)
/* 1467 */                 return date;
/* 1468 */             if (typeof format === 'string')
/* 1469 */                 format = DPGlobal.parseFormat(format);
/* 1470 */             var part_re = /([\-+]\d+)([dmwy])/,
/* 1471 */                 parts = date.match(/([\-+]\d+)([dmwy])/g),
/* 1472 */                 part, dir, i;
/* 1473 */             if (/^[\-+]\d+[dmwy]([\s,]+[\-+]\d+[dmwy])*$/.test(date)) {
/* 1474 */                 date = new Date();
/* 1475 */                 for (i = 0; i < parts.length; i++) {
/* 1476 */                     part = part_re.exec(parts[i]);
/* 1477 */                     dir = parseInt(part[1]);
/* 1478 */                     switch (part[2]) {
/* 1479 */                         case 'd':
/* 1480 */                             date.setUTCDate(date.getUTCDate() + dir);
/* 1481 */                             break;
/* 1482 */                         case 'm':
/* 1483 */                             date = Datepicker.prototype.moveMonth.call(Datepicker.prototype, date, dir);
/* 1484 */                             break;
/* 1485 */                         case 'w':
/* 1486 */                             date.setUTCDate(date.getUTCDate() + dir * 7);
/* 1487 */                             break;
/* 1488 */                         case 'y':
/* 1489 */                             date = Datepicker.prototype.moveYear.call(Datepicker.prototype, date, dir);
/* 1490 */                             break;
/* 1491 */                     }
/* 1492 */                 }
/* 1493 */                 return UTCDate(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), 0, 0, 0);
/* 1494 */             }
/* 1495 */             parts = date && date.match(this.nonpunctuation) || [];
/* 1496 */             date = new Date();
/* 1497 */             var parsed = {},
/* 1498 */                 setters_order = ['yyyy', 'yy', 'M', 'MM', 'm', 'mm', 'd', 'dd'],
/* 1499 */                 setters_map = {
/* 1500 */                     yyyy: function(d, v) {

/* bootstrap-datepicker.js */

/* 1501 */                         return d.setUTCFullYear(v);
/* 1502 */                     },
/* 1503 */                     yy: function(d, v) {
/* 1504 */                         return d.setUTCFullYear(2000 + v);
/* 1505 */                     },
/* 1506 */                     m: function(d, v) {
/* 1507 */                         if (isNaN(d))
/* 1508 */                             return d;
/* 1509 */                         v -= 1;
/* 1510 */                         while (v < 0) v += 12;
/* 1511 */                         v %= 12;
/* 1512 */                         d.setUTCMonth(v);
/* 1513 */                         while (d.getUTCMonth() !== v)
/* 1514 */                             d.setUTCDate(d.getUTCDate() - 1);
/* 1515 */                         return d;
/* 1516 */                     },
/* 1517 */                     d: function(d, v) {
/* 1518 */                         return d.setUTCDate(v);
/* 1519 */                     }
/* 1520 */                 },
/* 1521 */                 val, filtered;
/* 1522 */             setters_map['M'] = setters_map['MM'] = setters_map['mm'] = setters_map['m'];
/* 1523 */             setters_map['dd'] = setters_map['d'];
/* 1524 */             date = UTCDate(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0);
/* 1525 */             var fparts = format.parts.slice();
/* 1526 */             // Remove noop parts
/* 1527 */             if (parts.length !== fparts.length) {
/* 1528 */                 fparts = $(fparts).filter(function(i, p) {
/* 1529 */                     return $.inArray(p, setters_order) !== -1;
/* 1530 */                 }).toArray();
/* 1531 */             }
/* 1532 */             // Process remainder
/* 1533 */             function match_part() {
/* 1534 */                 var m = this.slice(0, parts[i].length),
/* 1535 */                     p = parts[i].slice(0, m.length);
/* 1536 */                 return m === p;
/* 1537 */             }
/* 1538 */             if (parts.length === fparts.length) {
/* 1539 */                 var cnt;
/* 1540 */                 for (i = 0, cnt = fparts.length; i < cnt; i++) {
/* 1541 */                     val = parseInt(parts[i], 10);
/* 1542 */                     part = fparts[i];
/* 1543 */                     if (isNaN(val)) {
/* 1544 */                         switch (part) {
/* 1545 */                             case 'MM':
/* 1546 */                                 filtered = $(dates[language].months).filter(match_part);
/* 1547 */                                 val = $.inArray(filtered[0], dates[language].months) + 1;
/* 1548 */                                 break;
/* 1549 */                             case 'M':
/* 1550 */                                 filtered = $(dates[language].monthsShort).filter(match_part);

/* bootstrap-datepicker.js */

/* 1551 */                                 val = $.inArray(filtered[0], dates[language].monthsShort) + 1;
/* 1552 */                                 break;
/* 1553 */                         }
/* 1554 */                     }
/* 1555 */                     parsed[part] = val;
/* 1556 */                 }
/* 1557 */                 var _date, s;
/* 1558 */                 for (i = 0; i < setters_order.length; i++) {
/* 1559 */                     s = setters_order[i];
/* 1560 */                     if (s in parsed && !isNaN(parsed[s])) {
/* 1561 */                         _date = new Date(date);
/* 1562 */                         setters_map[s](_date, parsed[s]);
/* 1563 */                         if (!isNaN(_date))
/* 1564 */                             date = _date;
/* 1565 */                     }
/* 1566 */                 }
/* 1567 */             }
/* 1568 */             return date;
/* 1569 */         },
/* 1570 */         formatDate: function(date, format, language) {
/* 1571 */             if (!date)
/* 1572 */                 return '';
/* 1573 */             if (typeof format === 'string')
/* 1574 */                 format = DPGlobal.parseFormat(format);
/* 1575 */             var val = {
/* 1576 */                 d: date.getUTCDate(),
/* 1577 */                 D: dates[language].daysShort[date.getUTCDay()],
/* 1578 */                 DD: dates[language].days[date.getUTCDay()],
/* 1579 */                 m: date.getUTCMonth() + 1,
/* 1580 */                 M: dates[language].monthsShort[date.getUTCMonth()],
/* 1581 */                 MM: dates[language].months[date.getUTCMonth()],
/* 1582 */                 yy: date.getUTCFullYear().toString().substring(2),
/* 1583 */                 yyyy: date.getUTCFullYear()
/* 1584 */             };
/* 1585 */             val.dd = (val.d < 10 ? '0' : '') + val.d;
/* 1586 */             val.mm = (val.m < 10 ? '0' : '') + val.m;
/* 1587 */             date = [];
/* 1588 */             var seps = $.extend([], format.separators);
/* 1589 */             for (var i = 0, cnt = format.parts.length; i <= cnt; i++) {
/* 1590 */                 if (seps.length)
/* 1591 */                     date.push(seps.shift());
/* 1592 */                 date.push(val[format.parts[i]]);
/* 1593 */             }
/* 1594 */             return date.join('');
/* 1595 */         },
/* 1596 */         headTemplate: '<thead>' +
/* 1597 */             '<tr>' +
/* 1598 */             '<th class="prev"></th>' +
/* 1599 */             '<th colspan="5" class="datepicker-switch"></th>' +
/* 1600 */             '<th class="next"></th>' +

/* bootstrap-datepicker.js */

/* 1601 */             '</tr>' +
/* 1602 */             '</thead>',
/* 1603 */         contTemplate: '<tbody><tr><td colspan="7"></td></tr></tbody>',
/* 1604 */         footTemplate: '<tfoot>' +
/* 1605 */             '<tr>' +
/* 1606 */             '<th colspan="7" class="today"></th>' +
/* 1607 */             '</tr>' +
/* 1608 */             '<tr>' +
/* 1609 */             '<th colspan="7" class="clear"></th>' +
/* 1610 */             '</tr>' +
/* 1611 */             '</tfoot>'
/* 1612 */     };
/* 1613 */     DPGlobal.template = '<div class="datepicker">' +
/* 1614 */         '<div class="datepicker-days">' +
/* 1615 */         '<table class=" table-condensed">' +
/* 1616 */         DPGlobal.headTemplate +
/* 1617 */         '<tbody></tbody>' +
/* 1618 */         DPGlobal.footTemplate +
/* 1619 */         '</table>' +
/* 1620 */         '</div>' +
/* 1621 */         '<div class="datepicker-months">' +
/* 1622 */         '<table class="table-condensed">' +
/* 1623 */         DPGlobal.headTemplate +
/* 1624 */         DPGlobal.contTemplate +
/* 1625 */         DPGlobal.footTemplate +
/* 1626 */         '</table>' +
/* 1627 */         '</div>' +
/* 1628 */         '<div class="datepicker-years">' +
/* 1629 */         '<table class="table-condensed">' +
/* 1630 */         DPGlobal.headTemplate +
/* 1631 */         DPGlobal.contTemplate +
/* 1632 */         DPGlobal.footTemplate +
/* 1633 */         '</table>' +
/* 1634 */         '</div>' +
/* 1635 */         '</div>';
/* 1636 */ 
/* 1637 */     $.fn.datepicker.DPGlobal = DPGlobal;
/* 1638 */ 
/* 1639 */ 
/* 1640 */     /* DATEPICKER NO CONFLICT
/* 1641 *|      * =================== */
/* 1642 */ 
/* 1643 */     $.fn.datepicker.noConflict = function() {
/* 1644 */         $.fn.datepicker = old;
/* 1645 */         return this;
/* 1646 */     };
/* 1647 */ 
/* 1648 */ 
/* 1649 */     /* DATEPICKER DATA-API
/* 1650 *|      * ================== */

/* bootstrap-datepicker.js */

/* 1651 */ 
/* 1652 */     $(document).on(
/* 1653 */         'focus.datepicker.data-api click.datepicker.data-api',
/* 1654 */         '[data-provide="datepicker"]',
/* 1655 */         function(e) {
/* 1656 */             var $this = $(this);
/* 1657 */             if ($this.data('datepicker'))
/* 1658 */                 return;
/* 1659 */             e.preventDefault();
/* 1660 */             // component click requires us to explicitly show it
/* 1661 */             $this.datepicker('show');
/* 1662 */         }
/* 1663 */     );
/* 1664 */     $(function() {
/* 1665 */         $('[data-provide="datepicker-inline"]').datepicker();
/* 1666 */     });
/* 1667 */ 
/* 1668 */ }(window.jQuery));

;
/* bootstrap-timepicker.js */

/* 1    */ /*!
/* 2    *|  * Timepicker Component for Twitter Bootstrap
/* 3    *|  *
/* 4    *|  * Copyright 2013 Joris de Wit
/* 5    *|  *
/* 6    *|  * Contributors https://github.com/jdewit/bootstrap-timepicker/graphs/contributors
/* 7    *|  *
/* 8    *|  * For the full copyright and license information, please view the LICENSE
/* 9    *|  * file that was distributed with this source code.
/* 10   *|  */
/* 11   */ (function($, window, document, undefined) {
/* 12   */     'use strict';
/* 13   */ 
/* 14   */     // TIMEPICKER PUBLIC CLASS DEFINITION
/* 15   */     var Timepicker = function(element, options) {
/* 16   */         this.widget = '';
/* 17   */         this.$element = $(element);
/* 18   */         this.defaultTime = options.defaultTime;
/* 19   */         this.disableFocus = options.disableFocus;
/* 20   */         this.disableMousewheel = options.disableMousewheel;
/* 21   */         this.isOpen = options.isOpen;
/* 22   */         this.minuteStep = options.minuteStep;
/* 23   */         this.modalBackdrop = options.modalBackdrop;
/* 24   */         this.orientation = options.orientation;
/* 25   */         this.secondStep = options.secondStep;
/* 26   */         this.showInputs = options.showInputs;
/* 27   */         this.showMeridian = options.showMeridian;
/* 28   */         this.showSeconds = options.showSeconds;
/* 29   */         this.template = options.template;
/* 30   */         this.appendWidgetTo = options.appendWidgetTo;
/* 31   */         this.showWidgetOnAddonClick = options.showWidgetOnAddonClick;
/* 32   */ 
/* 33   */         this._init();
/* 34   */     };
/* 35   */ 
/* 36   */     Timepicker.prototype = {
/* 37   */ 
/* 38   */         constructor: Timepicker,
/* 39   */         _init: function() {
/* 40   */             var self = this;
/* 41   */ 
/* 42   */             if (this.showWidgetOnAddonClick && (this.$element.parent().hasClass('input-append') || this.$element.parent().hasClass('input-prepend'))) {
/* 43   */                 this.$element.parent('.input-append, .input-prepend').find('.add-on').on({
/* 44   */                     'click.timepicker': $.proxy(this.showWidget, this)
/* 45   */                 });
/* 46   */                 this.$element.on({
/* 47   */                     'focus.timepicker': $.proxy(this.highlightUnit, this),
/* 48   */                     'click.timepicker': $.proxy(this.highlightUnit, this),
/* 49   */                     'keydown.timepicker': $.proxy(this.elementKeydown, this),
/* 50   */                     'blur.timepicker': $.proxy(this.blurElement, this),

/* bootstrap-timepicker.js */

/* 51   */                     'mousewheel.timepicker DOMMouseScroll.timepicker': $.proxy(this.mousewheel, this)
/* 52   */                 });
/* 53   */             } else {
/* 54   */                 if (this.template) {
/* 55   */                     this.$element.on({
/* 56   */                         'focus.timepicker': $.proxy(this.showWidget, this),
/* 57   */                         'click.timepicker': $.proxy(this.showWidget, this),
/* 58   */                         'blur.timepicker': $.proxy(this.blurElement, this),
/* 59   */                         'mousewheel.timepicker DOMMouseScroll.timepicker': $.proxy(this.mousewheel, this)
/* 60   */                     });
/* 61   */                 } else {
/* 62   */                     this.$element.on({
/* 63   */                         'focus.timepicker': $.proxy(this.highlightUnit, this),
/* 64   */                         'click.timepicker': $.proxy(this.highlightUnit, this),
/* 65   */                         'keydown.timepicker': $.proxy(this.elementKeydown, this),
/* 66   */                         'blur.timepicker': $.proxy(this.blurElement, this),
/* 67   */                         'mousewheel.timepicker DOMMouseScroll.timepicker': $.proxy(this.mousewheel, this)
/* 68   */                     });
/* 69   */                 }
/* 70   */             }
/* 71   */ 
/* 72   */             if (this.template !== false) {
/* 73   */                 this.$widget = $(this.getTemplate()).on('click', $.proxy(this.widgetClick, this));
/* 74   */             } else {
/* 75   */                 this.$widget = false;
/* 76   */             }
/* 77   */ 
/* 78   */             if (this.showInputs && this.$widget !== false) {
/* 79   */                 this.$widget.find('input').each(function() {
/* 80   */                     $(this).on({
/* 81   */                         'click.timepicker': function() {
/* 82   */                             $(this).select();
/* 83   */                         },
/* 84   */                         'keydown.timepicker': $.proxy(self.widgetKeydown, self),
/* 85   */                         'keyup.timepicker': $.proxy(self.widgetKeyup, self)
/* 86   */                     });
/* 87   */                 });
/* 88   */             }
/* 89   */ 
/* 90   */             this.setDefaultTime(this.defaultTime);
/* 91   */         },
/* 92   */ 
/* 93   */         blurElement: function() {
/* 94   */             this.highlightedUnit = null;
/* 95   */             this.updateFromElementVal();
/* 96   */         },
/* 97   */ 
/* 98   */         clear: function() {
/* 99   */             this.hour = '';
/* 100  */             this.minute = '';

/* bootstrap-timepicker.js */

/* 101  */             this.second = '';
/* 102  */             this.meridian = '';
/* 103  */ 
/* 104  */             this.$element.val('');
/* 105  */         },
/* 106  */ 
/* 107  */         decrementHour: function() {
/* 108  */             if (this.showMeridian) {
/* 109  */                 if (this.hour === 1) {
/* 110  */                     this.hour = 12;
/* 111  */                 } else if (this.hour === 12) {
/* 112  */                     this.hour--;
/* 113  */ 
/* 114  */                     return this.toggleMeridian();
/* 115  */                 } else if (this.hour === 0) {
/* 116  */                     this.hour = 11;
/* 117  */ 
/* 118  */                     return this.toggleMeridian();
/* 119  */                 } else {
/* 120  */                     this.hour--;
/* 121  */                 }
/* 122  */             } else {
/* 123  */                 if (this.hour <= 0) {
/* 124  */                     this.hour = 23;
/* 125  */                 } else {
/* 126  */                     this.hour--;
/* 127  */                 }
/* 128  */             }
/* 129  */         },
/* 130  */ 
/* 131  */         decrementMinute: function(step) {
/* 132  */             var newVal;
/* 133  */ 
/* 134  */             if (step) {
/* 135  */                 newVal = this.minute - step;
/* 136  */             } else {
/* 137  */                 newVal = this.minute - this.minuteStep;
/* 138  */             }
/* 139  */ 
/* 140  */             if (newVal < 0) {
/* 141  */                 this.decrementHour();
/* 142  */                 this.minute = newVal + 60;
/* 143  */             } else {
/* 144  */                 this.minute = newVal;
/* 145  */             }
/* 146  */         },
/* 147  */ 
/* 148  */         decrementSecond: function() {
/* 149  */             var newVal = this.second - this.secondStep;
/* 150  */ 

/* bootstrap-timepicker.js */

/* 151  */             if (newVal < 0) {
/* 152  */                 this.decrementMinute(true);
/* 153  */                 this.second = newVal + 60;
/* 154  */             } else {
/* 155  */                 this.second = newVal;
/* 156  */             }
/* 157  */         },
/* 158  */ 
/* 159  */         elementKeydown: function(e) {
/* 160  */             switch (e.keyCode) {
/* 161  */                 case 9: //tab
/* 162  */                 case 27: // escape
/* 163  */                     this.updateFromElementVal();
/* 164  */                     break;
/* 165  */                 case 37: // left arrow
/* 166  */                     e.preventDefault();
/* 167  */                     this.highlightPrevUnit();
/* 168  */                     break;
/* 169  */                 case 38: // up arrow
/* 170  */                     e.preventDefault();
/* 171  */                     switch (this.highlightedUnit) {
/* 172  */                         case 'hour':
/* 173  */                             this.incrementHour();
/* 174  */                             this.highlightHour();
/* 175  */                             break;
/* 176  */                         case 'minute':
/* 177  */                             this.incrementMinute();
/* 178  */                             this.highlightMinute();
/* 179  */                             break;
/* 180  */                         case 'second':
/* 181  */                             this.incrementSecond();
/* 182  */                             this.highlightSecond();
/* 183  */                             break;
/* 184  */                         case 'meridian':
/* 185  */                             this.toggleMeridian();
/* 186  */                             this.highlightMeridian();
/* 187  */                             break;
/* 188  */                     }
/* 189  */                     this.update();
/* 190  */                     break;
/* 191  */                 case 39: // right arrow
/* 192  */                     e.preventDefault();
/* 193  */                     this.highlightNextUnit();
/* 194  */                     break;
/* 195  */                 case 40: // down arrow
/* 196  */                     e.preventDefault();
/* 197  */                     switch (this.highlightedUnit) {
/* 198  */                         case 'hour':
/* 199  */                             this.decrementHour();
/* 200  */                             this.highlightHour();

/* bootstrap-timepicker.js */

/* 201  */                             break;
/* 202  */                         case 'minute':
/* 203  */                             this.decrementMinute();
/* 204  */                             this.highlightMinute();
/* 205  */                             break;
/* 206  */                         case 'second':
/* 207  */                             this.decrementSecond();
/* 208  */                             this.highlightSecond();
/* 209  */                             break;
/* 210  */                         case 'meridian':
/* 211  */                             this.toggleMeridian();
/* 212  */                             this.highlightMeridian();
/* 213  */                             break;
/* 214  */                     }
/* 215  */ 
/* 216  */                     this.update();
/* 217  */                     break;
/* 218  */             }
/* 219  */         },
/* 220  */ 
/* 221  */         getCursorPosition: function() {
/* 222  */             var input = this.$element.get(0);
/* 223  */ 
/* 224  */             if ('selectionStart' in input) { // Standard-compliant browsers
/* 225  */ 
/* 226  */                 return input.selectionStart;
/* 227  */             } else if (document.selection) { // IE fix
/* 228  */                 input.focus();
/* 229  */                 var sel = document.selection.createRange(),
/* 230  */                     selLen = document.selection.createRange().text.length;
/* 231  */ 
/* 232  */                 sel.moveStart('character', -input.value.length);
/* 233  */ 
/* 234  */                 return sel.text.length - selLen;
/* 235  */             }
/* 236  */         },
/* 237  */ 
/* 238  */         getTemplate: function() {
/* 239  */             var template,
/* 240  */                 hourTemplate,
/* 241  */                 minuteTemplate,
/* 242  */                 secondTemplate,
/* 243  */                 meridianTemplate,
/* 244  */                 templateContent;
/* 245  */ 
/* 246  */             if (this.showInputs) {
/* 247  */                 hourTemplate = '<input type="text" class="bootstrap-timepicker-hour" maxlength="2"/>';
/* 248  */                 minuteTemplate = '<input type="text" class="bootstrap-timepicker-minute" maxlength="2"/>';
/* 249  */                 secondTemplate = '<input type="text" class="bootstrap-timepicker-second" maxlength="2"/>';
/* 250  */                 meridianTemplate = '<input type="text" class="bootstrap-timepicker-meridian" maxlength="2"/>';

/* bootstrap-timepicker.js */

/* 251  */             } else {
/* 252  */                 hourTemplate = '<span class="bootstrap-timepicker-hour"></span>';
/* 253  */                 minuteTemplate = '<span class="bootstrap-timepicker-minute"></span>';
/* 254  */                 secondTemplate = '<span class="bootstrap-timepicker-second"></span>';
/* 255  */                 meridianTemplate = '<span class="bootstrap-timepicker-meridian"></span>';
/* 256  */             }
/* 257  */ 
/* 258  */             templateContent = '<table>' +
/* 259  */                 '<tr>' +
/* 260  */                 '<td><a href="#" data-action="incrementHour"><i class="fa fa-angle-up"></i></a></td>' +
/* 261  */                 '<td class="separator">&nbsp;</td>' +
/* 262  */                 '<td><a href="#" data-action="incrementMinute"><i class="fa fa-angle-up"></i></a></td>' +
/* 263  */                 (this.showSeconds ?
/* 264  */                 '<td class="separator">&nbsp;</td>' +
/* 265  */                 '<td><a href="#" data-action="incrementSecond"><i class="fa fa-angle-up"></i></a></td>' : '') +
/* 266  */                 (this.showMeridian ?
/* 267  */                 '<td class="separator">&nbsp;</td>' +
/* 268  */                 '<td class="meridian-column"><a href="#" data-action="toggleMeridian"><i class="fa fa-angle-up"></i></a></td>' : '') +
/* 269  */                 '</tr>' +
/* 270  */                 '<tr>' +
/* 271  */                 '<td>' + hourTemplate + '</td> ' +
/* 272  */                 '<td class="separator">:</td>' +
/* 273  */                 '<td>' + minuteTemplate + '</td> ' +
/* 274  */                 (this.showSeconds ?
/* 275  */                 '<td class="separator">:</td>' +
/* 276  */                 '<td>' + secondTemplate + '</td>' : '') +
/* 277  */                 (this.showMeridian ?
/* 278  */                 '<td class="separator">&nbsp;</td>' +
/* 279  */                 '<td>' + meridianTemplate + '</td>' : '') +
/* 280  */                 '</tr>' +
/* 281  */                 '<tr>' +
/* 282  */                 '<td><a href="#" data-action="decrementHour"><i class="fa fa-angle-down"></i></a></td>' +
/* 283  */                 '<td class="separator"></td>' +
/* 284  */                 '<td><a href="#" data-action="decrementMinute"><i class="fa fa-angle-down"></i></a></td>' +
/* 285  */                 (this.showSeconds ?
/* 286  */                 '<td class="separator">&nbsp;</td>' +
/* 287  */                 '<td><a href="#" data-action="decrementSecond"><i class="fa fa-angle-down"></i></a></td>' : '') +
/* 288  */                 (this.showMeridian ?
/* 289  */                 '<td class="separator">&nbsp;</td>' +
/* 290  */                 '<td><a href="#" data-action="toggleMeridian"><i class="fa fa-angle-down"></i></a></td>' : '') +
/* 291  */                 '</tr>' +
/* 292  */                 '</table>';
/* 293  */ 
/* 294  */             switch (this.template) {
/* 295  */                 case 'modal':
/* 296  */                     template = '<div class="bootstrap-timepicker-widget modal hide fade in" data-backdrop="' + (this.modalBackdrop ? 'true' : 'false') + '">' +
/* 297  */                         '<div class="modal-header">' +
/* 298  */                         '<a href="#" class="close" data-dismiss="modal">×</a>' +
/* 299  */                         '<h3>Pick a Time</h3>' +
/* 300  */                         '</div>' +

/* bootstrap-timepicker.js */

/* 301  */                         '<div class="modal-content">' +
/* 302  */                         templateContent +
/* 303  */                         '</div>' +
/* 304  */                         '<div class="modal-footer">' +
/* 305  */                         '<a href="#" class="btn btn-primary" data-dismiss="modal">OK</a>' +
/* 306  */                         '</div>' +
/* 307  */                         '</div>';
/* 308  */                     break;
/* 309  */                 case 'dropdown':
/* 310  */                     template = '<div class="bootstrap-timepicker-widget dropdown-menu">' + templateContent + '</div>';
/* 311  */                     break;
/* 312  */             }
/* 313  */ 
/* 314  */             return template;
/* 315  */         },
/* 316  */ 
/* 317  */         getTime: function() {
/* 318  */             if (this.hour === '') {
/* 319  */                 return '';
/* 320  */             }
/* 321  */ 
/* 322  */             return this.hour + ':' + (this.minute.toString().length === 1 ? '0' + this.minute : this.minute) + (this.showSeconds ? ':' + (this.second.toString().length === 1 ? '0' + this.second : this.second) : '') + (this.showMeridian ? ' ' + this.meridian : '');
/* 323  */         },
/* 324  */ 
/* 325  */         hideWidget: function() {
/* 326  */             if (this.isOpen === false) {
/* 327  */                 return;
/* 328  */             }
/* 329  */ 
/* 330  */             this.$element.trigger({
/* 331  */                 'type': 'hide.timepicker',
/* 332  */                 'time': {
/* 333  */                     'value': this.getTime(),
/* 334  */                     'hours': this.hour,
/* 335  */                     'minutes': this.minute,
/* 336  */                     'seconds': this.second,
/* 337  */                     'meridian': this.meridian
/* 338  */                 }
/* 339  */             });
/* 340  */ 
/* 341  */             if (this.template === 'modal' && this.$widget.modal) {
/* 342  */                 this.$widget.modal('hide');
/* 343  */             } else {
/* 344  */                 this.$widget.removeClass('open');
/* 345  */             }
/* 346  */ 
/* 347  */             $(document).off('mousedown.timepicker, touchend.timepicker');
/* 348  */ 
/* 349  */             this.isOpen = false;
/* 350  */             // show/hide approach taken by datepicker

/* bootstrap-timepicker.js */

/* 351  */             this.$widget.detach();
/* 352  */         },
/* 353  */ 
/* 354  */         highlightUnit: function() {
/* 355  */             this.position = this.getCursorPosition();
/* 356  */             if (this.position >= 0 && this.position <= 2) {
/* 357  */                 this.highlightHour();
/* 358  */             } else if (this.position >= 3 && this.position <= 5) {
/* 359  */                 this.highlightMinute();
/* 360  */             } else if (this.position >= 6 && this.position <= 8) {
/* 361  */                 if (this.showSeconds) {
/* 362  */                     this.highlightSecond();
/* 363  */                 } else {
/* 364  */                     this.highlightMeridian();
/* 365  */                 }
/* 366  */             } else if (this.position >= 9 && this.position <= 11) {
/* 367  */                 this.highlightMeridian();
/* 368  */             }
/* 369  */         },
/* 370  */ 
/* 371  */         highlightNextUnit: function() {
/* 372  */             switch (this.highlightedUnit) {
/* 373  */                 case 'hour':
/* 374  */                     this.highlightMinute();
/* 375  */                     break;
/* 376  */                 case 'minute':
/* 377  */                     if (this.showSeconds) {
/* 378  */                         this.highlightSecond();
/* 379  */                     } else if (this.showMeridian) {
/* 380  */                         this.highlightMeridian();
/* 381  */                     } else {
/* 382  */                         this.highlightHour();
/* 383  */                     }
/* 384  */                     break;
/* 385  */                 case 'second':
/* 386  */                     if (this.showMeridian) {
/* 387  */                         this.highlightMeridian();
/* 388  */                     } else {
/* 389  */                         this.highlightHour();
/* 390  */                     }
/* 391  */                     break;
/* 392  */                 case 'meridian':
/* 393  */                     this.highlightHour();
/* 394  */                     break;
/* 395  */             }
/* 396  */         },
/* 397  */ 
/* 398  */         highlightPrevUnit: function() {
/* 399  */             switch (this.highlightedUnit) {
/* 400  */                 case 'hour':

/* bootstrap-timepicker.js */

/* 401  */                     if (this.showMeridian) {
/* 402  */                         this.highlightMeridian();
/* 403  */                     } else if (this.showSeconds) {
/* 404  */                         this.highlightSecond();
/* 405  */                     } else {
/* 406  */                         this.highlightMinute();
/* 407  */                     }
/* 408  */                     break;
/* 409  */                 case 'minute':
/* 410  */                     this.highlightHour();
/* 411  */                     break;
/* 412  */                 case 'second':
/* 413  */                     this.highlightMinute();
/* 414  */                     break;
/* 415  */                 case 'meridian':
/* 416  */                     if (this.showSeconds) {
/* 417  */                         this.highlightSecond();
/* 418  */                     } else {
/* 419  */                         this.highlightMinute();
/* 420  */                     }
/* 421  */                     break;
/* 422  */             }
/* 423  */         },
/* 424  */ 
/* 425  */         highlightHour: function() {
/* 426  */             var $element = this.$element.get(0),
/* 427  */                 self = this;
/* 428  */ 
/* 429  */             this.highlightedUnit = 'hour';
/* 430  */ 
/* 431  */             if ($element.setSelectionRange) {
/* 432  */                 setTimeout(function() {
/* 433  */                     if (self.hour < 10) {
/* 434  */                         $element.setSelectionRange(0, 1);
/* 435  */                     } else {
/* 436  */                         $element.setSelectionRange(0, 2);
/* 437  */                     }
/* 438  */                 }, 0);
/* 439  */             }
/* 440  */         },
/* 441  */ 
/* 442  */         highlightMinute: function() {
/* 443  */             var $element = this.$element.get(0),
/* 444  */                 self = this;
/* 445  */ 
/* 446  */             this.highlightedUnit = 'minute';
/* 447  */ 
/* 448  */             if ($element.setSelectionRange) {
/* 449  */                 setTimeout(function() {
/* 450  */                     if (self.hour < 10) {

/* bootstrap-timepicker.js */

/* 451  */                         $element.setSelectionRange(2, 4);
/* 452  */                     } else {
/* 453  */                         $element.setSelectionRange(3, 5);
/* 454  */                     }
/* 455  */                 }, 0);
/* 456  */             }
/* 457  */         },
/* 458  */ 
/* 459  */         highlightSecond: function() {
/* 460  */             var $element = this.$element.get(0),
/* 461  */                 self = this;
/* 462  */ 
/* 463  */             this.highlightedUnit = 'second';
/* 464  */ 
/* 465  */             if ($element.setSelectionRange) {
/* 466  */                 setTimeout(function() {
/* 467  */                     if (self.hour < 10) {
/* 468  */                         $element.setSelectionRange(5, 7);
/* 469  */                     } else {
/* 470  */                         $element.setSelectionRange(6, 8);
/* 471  */                     }
/* 472  */                 }, 0);
/* 473  */             }
/* 474  */         },
/* 475  */ 
/* 476  */         highlightMeridian: function() {
/* 477  */             var $element = this.$element.get(0),
/* 478  */                 self = this;
/* 479  */ 
/* 480  */             this.highlightedUnit = 'meridian';
/* 481  */ 
/* 482  */             if ($element.setSelectionRange) {
/* 483  */                 if (this.showSeconds) {
/* 484  */                     setTimeout(function() {
/* 485  */                         if (self.hour < 10) {
/* 486  */                             $element.setSelectionRange(8, 10);
/* 487  */                         } else {
/* 488  */                             $element.setSelectionRange(9, 11);
/* 489  */                         }
/* 490  */                     }, 0);
/* 491  */                 } else {
/* 492  */                     setTimeout(function() {
/* 493  */                         if (self.hour < 10) {
/* 494  */                             $element.setSelectionRange(5, 7);
/* 495  */                         } else {
/* 496  */                             $element.setSelectionRange(6, 8);
/* 497  */                         }
/* 498  */                     }, 0);
/* 499  */                 }
/* 500  */             }

/* bootstrap-timepicker.js */

/* 501  */         },
/* 502  */ 
/* 503  */         incrementHour: function() {
/* 504  */             if (this.showMeridian) {
/* 505  */                 if (this.hour === 11) {
/* 506  */                     this.hour++;
/* 507  */                     return this.toggleMeridian();
/* 508  */                 } else if (this.hour === 12) {
/* 509  */                     this.hour = 0;
/* 510  */                 }
/* 511  */             }
/* 512  */             if (this.hour === 23) {
/* 513  */                 this.hour = 0;
/* 514  */ 
/* 515  */                 return;
/* 516  */             }
/* 517  */             this.hour++;
/* 518  */         },
/* 519  */ 
/* 520  */         incrementMinute: function(step) {
/* 521  */             var newVal;
/* 522  */ 
/* 523  */             if (step) {
/* 524  */                 newVal = this.minute + step;
/* 525  */             } else {
/* 526  */                 newVal = this.minute + this.minuteStep - (this.minute % this.minuteStep);
/* 527  */             }
/* 528  */ 
/* 529  */             if (newVal > 59) {
/* 530  */                 this.incrementHour();
/* 531  */                 this.minute = newVal - 60;
/* 532  */             } else {
/* 533  */                 this.minute = newVal;
/* 534  */             }
/* 535  */         },
/* 536  */ 
/* 537  */         incrementSecond: function() {
/* 538  */             var newVal = this.second + this.secondStep - (this.second % this.secondStep);
/* 539  */ 
/* 540  */             if (newVal > 59) {
/* 541  */                 this.incrementMinute(true);
/* 542  */                 this.second = newVal - 60;
/* 543  */             } else {
/* 544  */                 this.second = newVal;
/* 545  */             }
/* 546  */         },
/* 547  */ 
/* 548  */         mousewheel: function(e) {
/* 549  */             if (this.disableMousewheel) {
/* 550  */                 return;

/* bootstrap-timepicker.js */

/* 551  */             }
/* 552  */ 
/* 553  */             e.preventDefault();
/* 554  */             e.stopPropagation();
/* 555  */ 
/* 556  */             var delta = e.originalEvent.wheelDelta || -e.originalEvent.detail,
/* 557  */                 scrollTo = null;
/* 558  */ 
/* 559  */             if (e.type === 'mousewheel') {
/* 560  */                 scrollTo = (e.originalEvent.wheelDelta * -1);
/* 561  */             } else if (e.type === 'DOMMouseScroll') {
/* 562  */                 scrollTo = 40 * e.originalEvent.detail;
/* 563  */             }
/* 564  */ 
/* 565  */             if (scrollTo) {
/* 566  */                 e.preventDefault();
/* 567  */                 $(this).scrollTop(scrollTo + $(this).scrollTop());
/* 568  */             }
/* 569  */ 
/* 570  */             switch (this.highlightedUnit) {
/* 571  */                 case 'minute':
/* 572  */                     if (delta > 0) {
/* 573  */                         this.incrementMinute();
/* 574  */                     } else {
/* 575  */                         this.decrementMinute();
/* 576  */                     }
/* 577  */                     this.highlightMinute();
/* 578  */                     break;
/* 579  */                 case 'second':
/* 580  */                     if (delta > 0) {
/* 581  */                         this.incrementSecond();
/* 582  */                     } else {
/* 583  */                         this.decrementSecond();
/* 584  */                     }
/* 585  */                     this.highlightSecond();
/* 586  */                     break;
/* 587  */                 case 'meridian':
/* 588  */                     this.toggleMeridian();
/* 589  */                     this.highlightMeridian();
/* 590  */                     break;
/* 591  */                 default:
/* 592  */                     if (delta > 0) {
/* 593  */                         this.incrementHour();
/* 594  */                     } else {
/* 595  */                         this.decrementHour();
/* 596  */                     }
/* 597  */                     this.highlightHour();
/* 598  */                     break;
/* 599  */             }
/* 600  */ 

/* bootstrap-timepicker.js */

/* 601  */             return false;
/* 602  */         },
/* 603  */ 
/* 604  */         // This method was adapted from bootstrap-datepicker.
/* 605  */         place: function() {
/* 606  */             if (this.isInline) {
/* 607  */                 return;
/* 608  */             }
/* 609  */             var widgetWidth = this.$widget.outerWidth(),
/* 610  */                 widgetHeight = this.$widget.outerHeight(),
/* 611  */                 visualPadding = 10,
/* 612  */                 windowWidth =
/* 613  */                     $(window).width(),
/* 614  */                 windowHeight = $(window).height(),
/* 615  */                 scrollTop = $(window).scrollTop();
/* 616  */ 
/* 617  */             var zIndex = parseInt(this.$element.parents().filter(function() {}).first().css('z-index'), 10) + 10;
/* 618  */             var offset = this.component ? this.component.parent().offset() : this.$element.offset();
/* 619  */             var height = this.component ? this.component.outerHeight(true) : this.$element.outerHeight(false);
/* 620  */             var width = this.component ? this.component.outerWidth(true) : this.$element.outerWidth(false);
/* 621  */             var left = offset.left,
/* 622  */                 top = offset.top;
/* 623  */ 
/* 624  */             this.$widget.removeClass('timepicker-orient-top timepicker-orient-bottom timepicker-orient-right timepicker-orient-left');
/* 625  */ 
/* 626  */             if (this.orientation.x !== 'auto') {
/* 627  */                 this.picker.addClass('datepicker-orient-' + this.orientation.x);
/* 628  */                 if (this.orientation.x === 'right') {
/* 629  */                     left -= widgetWidth - width;
/* 630  */                 }
/* 631  */             } else {
/* 632  */                 // auto x orientation is best-placement: if it crosses a window edge, fudge it sideways
/* 633  */                 // Default to left
/* 634  */                 this.$widget.addClass('timepicker-orient-left');
/* 635  */                 if (offset.left < 0) {
/* 636  */                     left -= offset.left - visualPadding;
/* 637  */                 } else if (offset.left + widgetWidth > windowWidth) {
/* 638  */                     left = windowWidth - widgetWidth - visualPadding;
/* 639  */                 }
/* 640  */             }
/* 641  */             // auto y orientation is best-situation: top or bottom, no fudging, decision based on which shows more of the widget
/* 642  */             var yorient = this.orientation.y,
/* 643  */                 topOverflow, bottomOverflow;
/* 644  */             if (yorient === 'auto') {
/* 645  */                 topOverflow = -scrollTop + offset.top - widgetHeight;
/* 646  */                 bottomOverflow = scrollTop + windowHeight - (offset.top + height + widgetHeight);
/* 647  */                 if (Math.max(topOverflow, bottomOverflow) === bottomOverflow) {
/* 648  */                     yorient = 'top';
/* 649  */                 } else {
/* 650  */                     yorient = 'bottom';

/* bootstrap-timepicker.js */

/* 651  */                 }
/* 652  */             }
/* 653  */             this.$widget.addClass('timepicker-orient-' + yorient);
/* 654  */             if (yorient === 'top') {
/* 655  */                 top += height;
/* 656  */             } else {
/* 657  */                 top -= widgetHeight + parseInt(this.$widget.css('padding-top'), 10);
/* 658  */             }
/* 659  */ 
/* 660  */             this.$widget.css({
/* 661  */                 top: top,
/* 662  */                 left: left,
/* 663  */                 zIndex: zIndex
/* 664  */             });
/* 665  */         },
/* 666  */ 
/* 667  */         remove: function() {
/* 668  */             $('document').off('.timepicker');
/* 669  */             if (this.$widget) {
/* 670  */                 this.$widget.remove();
/* 671  */             }
/* 672  */             delete this.$element.data().timepicker;
/* 673  */         },
/* 674  */ 
/* 675  */         setDefaultTime: function(defaultTime) {
/* 676  */             if (!this.$element.val()) {
/* 677  */                 if (defaultTime === 'current') {
/* 678  */                     var dTime = new Date(),
/* 679  */                         hours = dTime.getHours(),
/* 680  */                         minutes = dTime.getMinutes(),
/* 681  */                         seconds = dTime.getSeconds(),
/* 682  */                         meridian = 'AM';
/* 683  */ 
/* 684  */                     if (seconds !== 0) {
/* 685  */                         seconds = Math.ceil(dTime.getSeconds() / this.secondStep) * this.secondStep;
/* 686  */                         if (seconds === 60) {
/* 687  */                             minutes += 1;
/* 688  */                             seconds = 0;
/* 689  */                         }
/* 690  */                     }
/* 691  */ 
/* 692  */                     if (minutes !== 0) {
/* 693  */                         minutes = Math.ceil(dTime.getMinutes() / this.minuteStep) * this.minuteStep;
/* 694  */                         if (minutes === 60) {
/* 695  */                             hours += 1;
/* 696  */                             minutes = 0;
/* 697  */                         }
/* 698  */                     }
/* 699  */ 
/* 700  */                     if (this.showMeridian) {

/* bootstrap-timepicker.js */

/* 701  */                         if (hours === 0) {
/* 702  */                             hours = 12;
/* 703  */                         } else if (hours >= 12) {
/* 704  */                             if (hours > 12) {
/* 705  */                                 hours = hours - 12;
/* 706  */                             }
/* 707  */                             meridian = 'PM';
/* 708  */                         } else {
/* 709  */                             meridian = 'AM';
/* 710  */                         }
/* 711  */                     }
/* 712  */ 
/* 713  */                     this.hour = hours;
/* 714  */                     this.minute = minutes;
/* 715  */                     this.second = seconds;
/* 716  */                     this.meridian = meridian;
/* 717  */ 
/* 718  */                     this.update();
/* 719  */ 
/* 720  */                 } else if (defaultTime === false) {
/* 721  */                     this.hour = 0;
/* 722  */                     this.minute = 0;
/* 723  */                     this.second = 0;
/* 724  */                     this.meridian = 'AM';
/* 725  */                 } else {
/* 726  */                     this.setTime(defaultTime);
/* 727  */                 }
/* 728  */             } else {
/* 729  */                 this.updateFromElementVal();
/* 730  */             }
/* 731  */         },
/* 732  */ 
/* 733  */         setTime: function(time, ignoreWidget) {
/* 734  */             if (!time) {
/* 735  */                 this.clear();
/* 736  */                 return;
/* 737  */             }
/* 738  */ 
/* 739  */             var timeArray,
/* 740  */                 hour,
/* 741  */                 minute,
/* 742  */                 second,
/* 743  */                 meridian;
/* 744  */ 
/* 745  */             if (typeof time === 'object' && time.getMonth) {
/* 746  */                 // this is a date object
/* 747  */                 hour = time.getHours();
/* 748  */                 minute = time.getMinutes();
/* 749  */                 second = time.getSeconds();
/* 750  */ 

/* bootstrap-timepicker.js */

/* 751  */                 if (this.showMeridian) {
/* 752  */                     meridian = 'AM';
/* 753  */                     if (hour > 12) {
/* 754  */                         meridian = 'PM';
/* 755  */                         hour = hour % 12;
/* 756  */                     }
/* 757  */ 
/* 758  */                     if (hour === 12) {
/* 759  */                         meridian = 'PM';
/* 760  */                     }
/* 761  */                 }
/* 762  */             } else {
/* 763  */                 if (time.match(/p/i) !== null) {
/* 764  */                     meridian = 'PM';
/* 765  */                 } else {
/* 766  */                     meridian = 'AM';
/* 767  */                 }
/* 768  */ 
/* 769  */                 time = time.replace(/[^0-9\:]/g, '');
/* 770  */ 
/* 771  */                 timeArray = time.split(':');
/* 772  */ 
/* 773  */                 hour = timeArray[0] ? timeArray[0].toString() : timeArray.toString();
/* 774  */                 minute = timeArray[1] ? timeArray[1].toString() : '';
/* 775  */                 second = timeArray[2] ? timeArray[2].toString() : '';
/* 776  */ 
/* 777  */                 // idiot proofing
/* 778  */                 if (hour.length > 4) {
/* 779  */                     second = hour.substr(4, 2);
/* 780  */                 }
/* 781  */                 if (hour.length > 2) {
/* 782  */                     minute = hour.substr(2, 2);
/* 783  */                     hour = hour.substr(0, 2);
/* 784  */                 }
/* 785  */                 if (minute.length > 2) {
/* 786  */                     second = minute.substr(2, 2);
/* 787  */                     minute = minute.substr(0, 2);
/* 788  */                 }
/* 789  */                 if (second.length > 2) {
/* 790  */                     second = second.substr(2, 2);
/* 791  */                 }
/* 792  */ 
/* 793  */                 hour = parseInt(hour, 10);
/* 794  */                 minute = parseInt(minute, 10);
/* 795  */                 second = parseInt(second, 10);
/* 796  */ 
/* 797  */                 if (isNaN(hour)) {
/* 798  */                     hour = 0;
/* 799  */                 }
/* 800  */                 if (isNaN(minute)) {

/* bootstrap-timepicker.js */

/* 801  */                     minute = 0;
/* 802  */                 }
/* 803  */                 if (isNaN(second)) {
/* 804  */                     second = 0;
/* 805  */                 }
/* 806  */ 
/* 807  */                 if (this.showMeridian) {
/* 808  */                     if (hour < 1) {
/* 809  */                         hour = 1;
/* 810  */                     } else if (hour > 12) {
/* 811  */                         hour = 12;
/* 812  */                     }
/* 813  */                 } else {
/* 814  */                     if (hour >= 24) {
/* 815  */                         hour = 23;
/* 816  */                     } else if (hour < 0) {
/* 817  */                         hour = 0;
/* 818  */                     }
/* 819  */                     if (hour < 13 && meridian === 'PM') {
/* 820  */                         hour = hour + 12;
/* 821  */                     }
/* 822  */                 }
/* 823  */ 
/* 824  */                 if (minute < 0) {
/* 825  */                     minute = 0;
/* 826  */                 } else if (minute >= 60) {
/* 827  */                     minute = 59;
/* 828  */                 }
/* 829  */ 
/* 830  */                 if (this.showSeconds) {
/* 831  */                     if (isNaN(second)) {
/* 832  */                         second = 0;
/* 833  */                     } else if (second < 0) {
/* 834  */                         second = 0;
/* 835  */                     } else if (second >= 60) {
/* 836  */                         second = 59;
/* 837  */                     }
/* 838  */                 }
/* 839  */             }
/* 840  */ 
/* 841  */             this.hour = hour;
/* 842  */             this.minute = minute;
/* 843  */             this.second = second;
/* 844  */             this.meridian = meridian;
/* 845  */ 
/* 846  */             this.update(ignoreWidget);
/* 847  */         },
/* 848  */ 
/* 849  */         showWidget: function() {
/* 850  */             if (this.isOpen) {

/* bootstrap-timepicker.js */

/* 851  */                 return;
/* 852  */             }
/* 853  */ 
/* 854  */             if (this.$element.is(':disabled')) {
/* 855  */                 return;
/* 856  */             }
/* 857  */ 
/* 858  */             // show/hide approach taken by datepicker
/* 859  */             this.$widget.appendTo(this.appendWidgetTo);
/* 860  */             var self = this;
/* 861  */             $(document).on('mousedown.timepicker, touchend.timepicker', function(e) {
/* 862  */                 // This condition was inspired by bootstrap-datepicker.
/* 863  */                 // The element the timepicker is invoked on is the input but it has a sibling for addon/button.
/* 864  */                 if (!(self.$element.parent().find(e.target).length ||
/* 865  */                     self.$widget.is(e.target) ||
/* 866  */                     self.$widget.find(e.target).length)) {
/* 867  */                     self.hideWidget();
/* 868  */                 }
/* 869  */             });
/* 870  */ 
/* 871  */             this.$element.trigger({
/* 872  */                 'type': 'show.timepicker',
/* 873  */                 'time': {
/* 874  */                     'value': this.getTime(),
/* 875  */                     'hours': this.hour,
/* 876  */                     'minutes': this.minute,
/* 877  */                     'seconds': this.second,
/* 878  */                     'meridian': this.meridian
/* 879  */                 }
/* 880  */             });
/* 881  */ 
/* 882  */             this.place();
/* 883  */             if (this.disableFocus) {
/* 884  */                 this.$element.blur();
/* 885  */             }
/* 886  */ 
/* 887  */             // widget shouldn't be empty on open
/* 888  */             if (this.hour === '') {
/* 889  */                 if (this.defaultTime) {
/* 890  */                     this.setDefaultTime(this.defaultTime);
/* 891  */                 } else {
/* 892  */                     this.setTime('0:0:0');
/* 893  */                 }
/* 894  */             }
/* 895  */ 
/* 896  */             if (this.template === 'modal' && this.$widget.modal) {
/* 897  */                 this.$widget.modal('show').on('hidden', $.proxy(this.hideWidget, this));
/* 898  */             } else {
/* 899  */                 if (this.isOpen === false) {
/* 900  */                     this.$widget.addClass('open');

/* bootstrap-timepicker.js */

/* 901  */                 }
/* 902  */             }
/* 903  */ 
/* 904  */             this.isOpen = true;
/* 905  */         },
/* 906  */ 
/* 907  */         toggleMeridian: function() {
/* 908  */             this.meridian = this.meridian === 'AM' ? 'PM' : 'AM';
/* 909  */         },
/* 910  */ 
/* 911  */         update: function(ignoreWidget) {
/* 912  */             this.updateElement();
/* 913  */             if (!ignoreWidget) {
/* 914  */                 this.updateWidget();
/* 915  */             }
/* 916  */ 
/* 917  */             this.$element.trigger({
/* 918  */                 'type': 'changeTime.timepicker',
/* 919  */                 'time': {
/* 920  */                     'value': this.getTime(),
/* 921  */                     'hours': this.hour,
/* 922  */                     'minutes': this.minute,
/* 923  */                     'seconds': this.second,
/* 924  */                     'meridian': this.meridian
/* 925  */                 }
/* 926  */             });
/* 927  */         },
/* 928  */ 
/* 929  */         updateElement: function() {
/* 930  */             this.$element.val(this.getTime()).change();
/* 931  */         },
/* 932  */ 
/* 933  */         updateFromElementVal: function() {
/* 934  */             this.setTime(this.$element.val());
/* 935  */         },
/* 936  */ 
/* 937  */         updateWidget: function() {
/* 938  */             if (this.$widget === false) {
/* 939  */                 return;
/* 940  */             }
/* 941  */ 
/* 942  */             var hour = this.hour,
/* 943  */                 minute = this.minute.toString().length === 1 ? '0' + this.minute : this.minute,
/* 944  */                 second = this.second.toString().length === 1 ? '0' + this.second : this.second;
/* 945  */ 
/* 946  */             if (this.showInputs) {
/* 947  */                 this.$widget.find('input.bootstrap-timepicker-hour').val(hour);
/* 948  */                 this.$widget.find('input.bootstrap-timepicker-minute').val(minute);
/* 949  */ 
/* 950  */                 if (this.showSeconds) {

/* bootstrap-timepicker.js */

/* 951  */                     this.$widget.find('input.bootstrap-timepicker-second').val(second);
/* 952  */                 }
/* 953  */                 if (this.showMeridian) {
/* 954  */                     this.$widget.find('input.bootstrap-timepicker-meridian').val(this.meridian);
/* 955  */                 }
/* 956  */             } else {
/* 957  */                 this.$widget.find('span.bootstrap-timepicker-hour').text(hour);
/* 958  */                 this.$widget.find('span.bootstrap-timepicker-minute').text(minute);
/* 959  */ 
/* 960  */                 if (this.showSeconds) {
/* 961  */                     this.$widget.find('span.bootstrap-timepicker-second').text(second);
/* 962  */                 }
/* 963  */                 if (this.showMeridian) {
/* 964  */                     this.$widget.find('span.bootstrap-timepicker-meridian').text(this.meridian);
/* 965  */                 }
/* 966  */             }
/* 967  */         },
/* 968  */ 
/* 969  */         updateFromWidgetInputs: function() {
/* 970  */             if (this.$widget === false) {
/* 971  */                 return;
/* 972  */             }
/* 973  */ 
/* 974  */             var t = this.$widget.find('input.bootstrap-timepicker-hour').val() + ':' +
/* 975  */                 this.$widget.find('input.bootstrap-timepicker-minute').val() +
/* 976  */                 (this.showSeconds ? ':' + this.$widget.find('input.bootstrap-timepicker-second').val() : '') +
/* 977  */                 (this.showMeridian ? this.$widget.find('input.bootstrap-timepicker-meridian').val() : '');
/* 978  */ 
/* 979  */             this.setTime(t, true);
/* 980  */         },
/* 981  */ 
/* 982  */         widgetClick: function(e) {
/* 983  */             e.stopPropagation();
/* 984  */             e.preventDefault();
/* 985  */ 
/* 986  */             var $input = $(e.target),
/* 987  */                 action = $input.closest('a').data('action');
/* 988  */ 
/* 989  */             if (action) {
/* 990  */                 this[action]();
/* 991  */             }
/* 992  */             this.update();
/* 993  */ 
/* 994  */             if ($input.is('input')) {
/* 995  */                 $input.get(0).setSelectionRange(0, 2);
/* 996  */             }
/* 997  */         },
/* 998  */ 
/* 999  */         widgetKeydown: function(e) {
/* 1000 */             var $input = $(e.target),

/* bootstrap-timepicker.js */

/* 1001 */                 name = $input.attr('class').replace('bootstrap-timepicker-', '');
/* 1002 */ 
/* 1003 */             switch (e.keyCode) {
/* 1004 */                 case 9: //tab
/* 1005 */                     if ((this.showMeridian && name === 'meridian') || (this.showSeconds && name === 'second') || (!this.showMeridian && !this.showSeconds && name === 'minute')) {
/* 1006 */                         return this.hideWidget();
/* 1007 */                     }
/* 1008 */                     break;
/* 1009 */                 case 27: // escape
/* 1010 */                     this.hideWidget();
/* 1011 */                     break;
/* 1012 */                 case 38: // up arrow
/* 1013 */                     e.preventDefault();
/* 1014 */                     switch (name) {
/* 1015 */                         case 'hour':
/* 1016 */                             this.incrementHour();
/* 1017 */                             break;
/* 1018 */                         case 'minute':
/* 1019 */                             this.incrementMinute();
/* 1020 */                             break;
/* 1021 */                         case 'second':
/* 1022 */                             this.incrementSecond();
/* 1023 */                             break;
/* 1024 */                         case 'meridian':
/* 1025 */                             this.toggleMeridian();
/* 1026 */                             break;
/* 1027 */                     }
/* 1028 */                     this.setTime(this.getTime());
/* 1029 */                     $input.get(0).setSelectionRange(0, 2);
/* 1030 */                     break;
/* 1031 */                 case 40: // down arrow
/* 1032 */                     e.preventDefault();
/* 1033 */                     switch (name) {
/* 1034 */                         case 'hour':
/* 1035 */                             this.decrementHour();
/* 1036 */                             break;
/* 1037 */                         case 'minute':
/* 1038 */                             this.decrementMinute();
/* 1039 */                             break;
/* 1040 */                         case 'second':
/* 1041 */                             this.decrementSecond();
/* 1042 */                             break;
/* 1043 */                         case 'meridian':
/* 1044 */                             this.toggleMeridian();
/* 1045 */                             break;
/* 1046 */                     }
/* 1047 */                     this.setTime(this.getTime());
/* 1048 */                     $input.get(0).setSelectionRange(0, 2);
/* 1049 */                     break;
/* 1050 */             }

/* bootstrap-timepicker.js */

/* 1051 */         },
/* 1052 */ 
/* 1053 */         widgetKeyup: function(e) {
/* 1054 */             if ((e.keyCode === 65) || (e.keyCode === 77) || (e.keyCode === 80) || (e.keyCode === 46) || (e.keyCode === 8) || (e.keyCode >= 46 && e.keyCode <= 57)) {
/* 1055 */                 this.updateFromWidgetInputs();
/* 1056 */             }
/* 1057 */         }
/* 1058 */     };
/* 1059 */ 
/* 1060 */     //TIMEPICKER PLUGIN DEFINITION
/* 1061 */     $.fn.timepicker = function(option) {
/* 1062 */         var args = Array.apply(null, arguments);
/* 1063 */         args.shift();
/* 1064 */         return this.each(function() {
/* 1065 */             var $this = $(this),
/* 1066 */                 data = $this.data('timepicker'),
/* 1067 */                 options = typeof option === 'object' && option;
/* 1068 */ 
/* 1069 */             if (!data) {
/* 1070 */                 $this.data('timepicker', (data = new Timepicker(this, $.extend({}, $.fn.timepicker.defaults, options, $(this).data()))));
/* 1071 */             }
/* 1072 */ 
/* 1073 */             if (typeof option === 'string') {
/* 1074 */                 data[option].apply(data, args);
/* 1075 */             }
/* 1076 */         });
/* 1077 */     };
/* 1078 */ 
/* 1079 */     $.fn.timepicker.defaults = {
/* 1080 */         defaultTime: 'current',
/* 1081 */         disableFocus: false,
/* 1082 */         disableMousewheel: false,
/* 1083 */         isOpen: false,
/* 1084 */         minuteStep: 15,
/* 1085 */         modalBackdrop: false,
/* 1086 */         orientation: {
/* 1087 */             x: 'auto',
/* 1088 */             y: 'auto'
/* 1089 */         },
/* 1090 */         secondStep: 15,
/* 1091 */         showSeconds: false,
/* 1092 */         showInputs: true,
/* 1093 */         showMeridian: true,
/* 1094 */         template: 'dropdown',
/* 1095 */         appendWidgetTo: 'body',
/* 1096 */         showWidgetOnAddonClick: true
/* 1097 */     };
/* 1098 */ 
/* 1099 */     $.fn.timepicker.Constructor = Timepicker;
/* 1100 */ 

/* bootstrap-timepicker.js */

/* 1101 */ })(jQuery, window, document);

;
/* ionrangeslider.js */

/* 1  */ // Ion.RangeSlider | version 2.0.2 | https://github.com/IonDen/ion.rangeSlider
/* 2  */ (function(e,s,g,q,u){var t=0,p=function(){var a=q.userAgent,b=/msie\s\d+/i;return 0<a.search(b)&&(a=b.exec(a).toString(),a=a.split(" ")[1],9>a)?(e("html").addClass("lt-ie9"),!0):!1}(),l="ontouchstart"in g||0<q.msMaxTouchPoints;Function.prototype.bind||(Function.prototype.bind=function(a){var b=this,c=[].slice;if("function"!=typeof b)throw new TypeError;var d=c.call(arguments,1),h=function(){if(this instanceof h){var f=function(){};f.prototype=b.prototype;var f=new f,k=b.apply(f,d.concat(c.call(arguments)));
/* 3  */     return Object(k)===k?k:f}return b.apply(a,d.concat(c.call(arguments)))};return h});var r=function(a,b,c){this.VERSION="2.0.2";this.input=a;this.plugin_count=c;this.old_to=this.old_from=this.calc_count=this.current_plugin=0;this.raf_id=null;this.is_update=this.is_key=this.force_redraw=this.dragging=!1;this.is_start=!0;this.is_click=this.is_resize=this.is_active=!1;this.$cache={win:e(g),body:e(s.body),input:e(a),cont:null,rs:null,min:null,max:null,from:null,to:null,single:null,bar:null,line:null,s_single:null,
/* 4  */     s_from:null,s_to:null,shad_single:null,shad_from:null,shad_to:null,grid:null,grid_labels:[]};a=this.$cache.input;a={type:a.data("type"),min:a.data("min"),max:a.data("max"),from:a.data("from"),to:a.data("to"),step:a.data("step"),min_interval:a.data("minInterval"),max_interval:a.data("maxInterval"),drag_interval:a.data("dragInterval"),values:a.data("values"),from_fixed:a.data("fromFixed"),from_min:a.data("fromMin"),from_max:a.data("fromMax"),from_shadow:a.data("fromShadow"),to_fixed:a.data("toFixed"),
/* 5  */     to_min:a.data("toMin"),to_max:a.data("toMax"),to_shadow:a.data("toShadow"),prettify_enabled:a.data("prettifyEnabled"),prettify_separator:a.data("prettifySeparator"),force_edges:a.data("forceEdges"),keyboard:a.data("keyboard"),keyboard_step:a.data("keyboardStep"),grid:a.data("grid"),grid_margin:a.data("gridMargin"),grid_num:a.data("gridNum"),grid_snap:a.data("gridSnap"),hide_min_max:a.data("hideMinMax"),hide_from_to:a.data("hideFromTo"),prefix:a.data("prefix"),postfix:a.data("postfix"),max_postfix:a.data("maxPostfix"),
/* 6  */     decorate_both:a.data("decorateBoth"),values_separator:a.data("valuesSeparator"),disable:a.data("disable")};a.values=a.values&&a.values.split(",");b=e.extend(a,b);this.options=e.extend({type:"single",min:10,max:100,from:null,to:null,step:1,min_interval:0,max_interval:0,drag_interval:!1,values:[],p_values:[],from_fixed:!1,from_min:null,from_max:null,from_shadow:!1,to_fixed:!1,to_min:null,to_max:null,to_shadow:!1,prettify_enabled:!0,prettify_separator:" ",prettify:null,force_edges:!1,keyboard:!1,keyboard_step:5,
/* 7  */     grid:!1,grid_margin:!0,grid_num:4,grid_snap:!1,hide_min_max:!1,hide_from_to:!1,prefix:"",postfix:"",max_postfix:"",decorate_both:!0,values_separator:" \u2014 ",disable:!1,onStart:null,onChange:null,onFinish:null,onUpdate:null},b);this.validate();this.result={input:this.$cache.input,slider:null,min:this.options.min,max:this.options.max,from:this.options.from,from_percent:0,from_value:null,to:this.options.to,to_percent:0,to_value:null};this.coords={x_gap:0,x_pointer:0,w_rs:0,w_rs_old:0,w_handle:0,p_gap:0,
/* 8  */     p_gap_left:0,p_gap_right:0,p_step:0,p_pointer:0,p_handle:0,p_single:0,p_single_real:0,p_from:0,p_from_real:0,p_to:0,p_to_real:0,p_bar_x:0,p_bar_w:0,grid_gap:0,big_num:0,big:[],big_w:[],big_p:[],big_x:[]};this.labels={w_min:0,w_max:0,w_from:0,w_to:0,w_single:0,p_min:0,p_max:0,p_from:0,p_from_left:0,p_to:0,p_to_left:0,p_single:0,p_single_left:0};this.init()};r.prototype={init:function(a){this.coords.p_step=this.options.step/((this.options.max-this.options.min)/100);this.target="base";this.toggleInput();
/* 9  */     this.append();this.setMinMax();if(a){if(this.force_redraw=!0,this.calc(!0),this.options.onUpdate&&"function"===typeof this.options.onUpdate)this.options.onUpdate(this.result)}else if(this.force_redraw=!0,this.calc(!0),this.options.onStart&&"function"===typeof this.options.onStart)this.options.onStart(this.result);this.updateScene();this.raf_id=requestAnimationFrame(this.updateScene.bind(this))},append:function(){this.$cache.input.before('<span class="irs js-irs-'+this.plugin_count+'"></span>');this.$cache.input.prop("readonly",
/* 10 */     !0);this.$cache.cont=this.$cache.input.prev();this.result.slider=this.$cache.cont;this.$cache.cont.html('<span class="irs"><span class="irs-line" tabindex="-1"><span class="irs-line-left"></span><span class="irs-line-mid"></span><span class="irs-line-right"></span></span><span class="irs-min">0</span><span class="irs-max">1</span><span class="irs-from">0</span><span class="irs-to">0</span><span class="irs-single">0</span></span><span class="irs-grid"></span><span class="irs-bar"></span>');this.$cache.rs=
/* 11 */     this.$cache.cont.find(".irs");this.$cache.min=this.$cache.cont.find(".irs-min");this.$cache.max=this.$cache.cont.find(".irs-max");this.$cache.from=this.$cache.cont.find(".irs-from");this.$cache.to=this.$cache.cont.find(".irs-to");this.$cache.single=this.$cache.cont.find(".irs-single");this.$cache.bar=this.$cache.cont.find(".irs-bar");this.$cache.line=this.$cache.cont.find(".irs-line");this.$cache.grid=this.$cache.cont.find(".irs-grid");"single"===this.options.type?(this.$cache.cont.append('<span class="irs-bar-edge"></span><span class="irs-shadow shadow-single"></span><span class="irs-slider single"></span>'),
/* 12 */     this.$cache.s_single=this.$cache.cont.find(".single"),this.$cache.from[0].style.visibility="hidden",this.$cache.to[0].style.visibility="hidden",this.$cache.shad_single=this.$cache.cont.find(".shadow-single")):(this.$cache.cont.append('<span class="irs-shadow shadow-from"></span><span class="irs-shadow shadow-to"></span><span class="irs-slider from"></span><span class="irs-slider to"></span>'),this.$cache.s_from=this.$cache.cont.find(".from"),this.$cache.s_to=this.$cache.cont.find(".to"),this.$cache.shad_from=
/* 13 */     this.$cache.cont.find(".shadow-from"),this.$cache.shad_to=this.$cache.cont.find(".shadow-to"));this.options.hide_from_to&&(this.$cache.from[0].style.display="none",this.$cache.to[0].style.display="none",this.$cache.single[0].style.display="none");this.appendGrid();this.options.disable?this.appendDisableMask():(this.$cache.cont.removeClass("irs-disabled"),this.bindEvents())},appendDisableMask:function(){this.$cache.cont.append('<span class="irs-disable-mask"></span>');this.$cache.cont.addClass("irs-disabled")},
/* 14 */     remove:function(){this.$cache.cont.remove();this.$cache.cont=null;this.$cache.line.off("keydown.irs_"+this.plugin_count);l?(this.$cache.body.off("touchmove.irs_"+this.plugin_count),this.$cache.win.off("touchend.irs_"+this.plugin_count)):(this.$cache.body.off("mousemove.irs_"+this.plugin_count),this.$cache.win.off("mouseup.irs_"+this.plugin_count),p&&(this.$cache.body.off("mouseup.irs_"+this.plugin_count),this.$cache.body.off("mouseleave.irs_"+this.plugin_count)));this.$cache.grid_labels=[];this.coords.big=
/* 15 */         [];this.coords.big_w=[];this.coords.big_p=[];this.coords.big_x=[];cancelAnimationFrame(this.raf_id)},bindEvents:function(){if(l){this.$cache.body.on("touchmove.irs_"+this.plugin_count,this.pointerMove.bind(this));this.$cache.win.on("touchend.irs_"+this.plugin_count,this.pointerUp.bind(this));this.$cache.line.on("touchstart.irs_"+this.plugin_count,this.pointerClick.bind(this,"click"));if(this.options.drag_interval&&"double"===this.options.type)this.$cache.bar.on("touchstart.irs_"+this.plugin_count,
/* 16 */         this.pointerDown.bind(this,"both"));else this.$cache.bar.on("touchstart.irs_"+this.plugin_count,this.pointerClick.bind(this,"click"));"single"===this.options.type?(this.$cache.s_single.on("touchstart.irs_"+this.plugin_count,this.pointerDown.bind(this,"single")),this.$cache.shad_single.on("touchstart.irs_"+this.plugin_count,this.pointerClick.bind(this,"click"))):(this.$cache.s_from.on("touchstart.irs_"+this.plugin_count,this.pointerDown.bind(this,"from")),this.$cache.s_to.on("touchstart.irs_"+this.plugin_count,
/* 17 */         this.pointerDown.bind(this,"to")),this.$cache.shad_from.on("touchstart.irs_"+this.plugin_count,this.pointerClick.bind(this,"click")),this.$cache.shad_to.on("touchstart.irs_"+this.plugin_count,this.pointerClick.bind(this,"click")))}else{if(this.options.keyboard)this.$cache.line.on("keydown.irs_"+this.plugin_count,this.key.bind(this,"keyboard"));this.$cache.body.on("mousemove.irs_"+this.plugin_count,this.pointerMove.bind(this));this.$cache.win.on("mouseup.irs_"+this.plugin_count,this.pointerUp.bind(this));
/* 18 */         p&&(this.$cache.body.on("mouseup.irs_"+this.plugin_count,this.pointerUp.bind(this)),this.$cache.body.on("mouseleave.irs_"+this.plugin_count,this.pointerUp.bind(this)));this.$cache.line.on("mousedown.irs_"+this.plugin_count,this.pointerClick.bind(this,"click"));if(this.options.drag_interval&&"double"===this.options.type)this.$cache.bar.on("mousedown.irs_"+this.plugin_count,this.pointerDown.bind(this,"both"));else this.$cache.bar.on("mousedown.irs_"+this.plugin_count,this.pointerClick.bind(this,"click"));
/* 19 */         "single"===this.options.type?(this.$cache.s_single.on("mousedown.irs_"+this.plugin_count,this.pointerDown.bind(this,"single")),this.$cache.shad_single.on("mousedown.irs_"+this.plugin_count,this.pointerClick.bind(this,"click"))):(this.$cache.s_from.on("mousedown.irs_"+this.plugin_count,this.pointerDown.bind(this,"from")),this.$cache.s_to.on("mousedown.irs_"+this.plugin_count,this.pointerDown.bind(this,"to")),this.$cache.shad_from.on("mousedown.irs_"+this.plugin_count,this.pointerClick.bind(this,"click")),
/* 20 */             this.$cache.shad_to.on("mousedown.irs_"+this.plugin_count,this.pointerClick.bind(this,"click")))}},pointerMove:function(a){this.dragging&&(this.coords.x_pointer=(l?a.originalEvent.touches[0]:a).pageX-this.coords.x_gap,this.calc())},pointerUp:function(a){if(this.current_plugin===this.plugin_count&&this.is_active){this.is_active=!1;var b=this.options.onFinish&&"function"===typeof this.options.onFinish;a=e.contains(this.$cache.cont[0],a.target)||this.dragging;if(b&&a)this.options.onFinish(this.result);
/* 21 */         this.force_redraw=!0;this.dragging=!1;p&&e("*").prop("unselectable",!1)}},pointerDown:function(a,b){b.preventDefault();var c=l?b.originalEvent.touches[0]:b;if(2!==b.button){this.current_plugin=this.plugin_count;this.target=a;this.dragging=this.is_active=!0;this.coords.x_gap=this.$cache.rs.offset().left;this.coords.x_pointer=c.pageX-this.coords.x_gap;this.calcPointer();switch(a){case "single":this.coords.p_gap=this.toFixed(this.coords.p_pointer-this.coords.p_single);break;case "from":this.coords.p_gap=
/* 22 */         this.toFixed(this.coords.p_pointer-this.coords.p_from);this.$cache.s_from.addClass("type_last");this.$cache.s_to.removeClass("type_last");break;case "to":this.coords.p_gap=this.toFixed(this.coords.p_pointer-this.coords.p_to);this.$cache.s_to.addClass("type_last");this.$cache.s_from.removeClass("type_last");break;case "both":this.coords.p_gap_left=this.toFixed(this.coords.p_pointer-this.coords.p_from),this.coords.p_gap_right=this.toFixed(this.coords.p_to-this.coords.p_pointer),this.$cache.s_to.removeClass("type_last"),
/* 23 */         this.$cache.s_from.removeClass("type_last")}p&&e("*").prop("unselectable",!0);this.$cache.line.trigger("focus")}},pointerClick:function(a,b){b.preventDefault();var c=l?b.originalEvent.touches[0]:b;2!==b.button&&(this.current_plugin=this.plugin_count,this.target=a,this.is_click=!0,this.coords.x_gap=this.$cache.rs.offset().left,this.coords.x_pointer=+(c.pageX-this.coords.x_gap).toFixed(),this.force_redraw=!0,this.calc(),this.$cache.line.trigger("focus"))},key:function(a,b){if(!(this.current_plugin!==
/* 24 */         this.plugin_count||b.altKey||b.ctrlKey||b.shiftKey||b.metaKey)){switch(b.which){case 83:case 65:case 40:case 37:b.preventDefault();this.moveByKey(!1);break;case 87:case 68:case 38:case 39:b.preventDefault(),this.moveByKey(!0)}return!0}},moveByKey:function(a){var b=this.coords.p_pointer,b=a?b+this.options.keyboard_step:b-this.options.keyboard_step;this.coords.x_pointer=this.toFixed(this.coords.w_rs/100*b);this.is_key=!0;this.calc()},setMinMax:function(){this.options.hide_min_max?(this.$cache.min[0].style.display=
/* 25 */         "none",this.$cache.max[0].style.display="none"):(this.options.values.length?(this.$cache.min.html(this.decorate(this.options.p_values[this.options.min])),this.$cache.max.html(this.decorate(this.options.p_values[this.options.max]))):(this.$cache.min.html(this.decorate(this._prettify(this.options.min),this.options.min)),this.$cache.max.html(this.decorate(this._prettify(this.options.max),this.options.max))),this.labels.w_min=this.$cache.min.outerWidth(!1),this.labels.w_max=this.$cache.max.outerWidth(!1))},
/* 26 */     calc:function(a){this.calc_count++;if(10===this.calc_count||a)this.calc_count=0,this.coords.w_rs=this.$cache.rs.outerWidth(!1),this.coords.w_handle="single"===this.options.type?this.$cache.s_single.outerWidth(!1):this.$cache.s_from.outerWidth(!1);if(this.coords.w_rs){this.calcPointer();this.coords.p_handle=this.toFixed(this.coords.w_handle/this.coords.w_rs*100);a=100-this.coords.p_handle;var b=this.toFixed(this.coords.p_pointer-this.coords.p_gap);"click"===this.target&&(b=this.toFixed(this.coords.p_pointer-
/* 27 */     this.coords.p_handle/2),this.target=this.chooseHandle(b));0>b?b=0:b>a&&(b=a);switch(this.target){case "base":b=(this.options.max-this.options.min)/100;a=(this.result.from-this.options.min)/b;b=(this.result.to-this.options.min)/b;this.coords.p_single_real=this.toFixed(a);this.coords.p_from_real=this.toFixed(a);this.coords.p_to_real=this.toFixed(b);this.coords.p_single_real=this.checkDiapason(this.coords.p_single_real,this.options.from_min,this.options.from_max);this.coords.p_from_real=this.checkDiapason(this.coords.p_from_real,
/* 28 */         this.options.from_min,this.options.from_max);this.coords.p_to_real=this.checkDiapason(this.coords.p_to_real,this.options.to_min,this.options.to_max);this.coords.p_single=this.toFixed(a-this.coords.p_handle/100*a);this.coords.p_from=this.toFixed(a-this.coords.p_handle/100*a);this.coords.p_to=this.toFixed(b-this.coords.p_handle/100*b);this.target=null;break;case "single":if(this.options.from_fixed)break;this.coords.p_single_real=this.calcWithStep(b/a*100);this.coords.p_single_real=this.checkDiapason(this.coords.p_single_real,
/* 29 */         this.options.from_min,this.options.from_max);this.coords.p_single=this.toFixed(this.coords.p_single_real/100*a);break;case "from":if(this.options.from_fixed)break;this.coords.p_from_real=this.calcWithStep(b/a*100);this.coords.p_from_real>this.coords.p_to_real&&(this.coords.p_from_real=this.coords.p_to_real);this.coords.p_from_real=this.checkDiapason(this.coords.p_from_real,this.options.from_min,this.options.from_max);this.coords.p_from_real=this.checkMinInterval(this.coords.p_from_real,this.coords.p_to_real,
/* 30 */         "from");this.coords.p_from_real=this.checkMaxInterval(this.coords.p_from_real,this.coords.p_to_real,"from");this.coords.p_from=this.toFixed(this.coords.p_from_real/100*a);break;case "to":if(this.options.to_fixed)break;this.coords.p_to_real=this.calcWithStep(b/a*100);this.coords.p_to_real<this.coords.p_from_real&&(this.coords.p_to_real=this.coords.p_from_real);this.coords.p_to_real=this.checkDiapason(this.coords.p_to_real,this.options.to_min,this.options.to_max);this.coords.p_to_real=this.checkMinInterval(this.coords.p_to_real,
/* 31 */         this.coords.p_from_real,"to");this.coords.p_to_real=this.checkMaxInterval(this.coords.p_to_real,this.coords.p_from_real,"to");this.coords.p_to=this.toFixed(this.coords.p_to_real/100*a);break;case "both":b=this.toFixed(b+.1*this.coords.p_handle),this.coords.p_from_real=this.calcWithStep((b-this.coords.p_gap_left)/a*100),this.coords.p_from_real=this.checkDiapason(this.coords.p_from_real,this.options.from_min,this.options.from_max),this.coords.p_from_real=this.checkMinInterval(this.coords.p_from_real,
/* 32 */         this.coords.p_to_real,"from"),this.coords.p_from=this.toFixed(this.coords.p_from_real/100*a),this.coords.p_to_real=this.calcWithStep((b+this.coords.p_gap_right)/a*100),this.coords.p_to_real=this.checkDiapason(this.coords.p_to_real,this.options.to_min,this.options.to_max),this.coords.p_to_real=this.checkMinInterval(this.coords.p_to_real,this.coords.p_from_real,"to"),this.coords.p_to=this.toFixed(this.coords.p_to_real/100*a)}"single"===this.options.type?(this.coords.p_bar_x=this.coords.p_handle/2,this.coords.p_bar_w=
/* 33 */         this.coords.p_single,this.result.from_percent=this.coords.p_single_real,this.result.from=this.calcReal(this.coords.p_single_real),this.options.values.length&&(this.result.from_value=this.options.values[this.result.from])):(this.coords.p_bar_x=this.toFixed(this.coords.p_from+this.coords.p_handle/2),this.coords.p_bar_w=this.toFixed(this.coords.p_to-this.coords.p_from),this.result.from_percent=this.coords.p_from_real,this.result.from=this.calcReal(this.coords.p_from_real),this.result.to_percent=this.coords.p_to_real,
/* 34 */         this.result.to=this.calcReal(this.coords.p_to_real),this.options.values.length&&(this.result.from_value=this.options.values[this.result.from],this.result.to_value=this.options.values[this.result.to]));this.calcMinMax();this.calcLabels()}},calcPointer:function(){this.coords.w_rs?(0>this.coords.x_pointer?this.coords.x_pointer=0:this.coords.x_pointer>this.coords.w_rs&&(this.coords.x_pointer=this.coords.w_rs),this.coords.p_pointer=this.toFixed(this.coords.x_pointer/this.coords.w_rs*100)):this.coords.p_pointer=
/* 35 */         0},chooseHandle:function(a){return"single"===this.options.type?"single":a>=this.coords.p_from_real+(this.coords.p_to_real-this.coords.p_from_real)/2?"to":"from"},calcMinMax:function(){this.coords.w_rs&&(this.labels.p_min=this.labels.w_min/this.coords.w_rs*100,this.labels.p_max=this.labels.w_max/this.coords.w_rs*100)},calcLabels:function(){this.coords.w_rs&&!this.options.hide_from_to&&("single"===this.options.type?(this.labels.w_single=this.$cache.single.outerWidth(!1),this.labels.p_single=this.labels.w_single/
/* 36 */     this.coords.w_rs*100,this.labels.p_single_left=this.coords.p_single+this.coords.p_handle/2-this.labels.p_single/2):(this.labels.w_from=this.$cache.from.outerWidth(!1),this.labels.p_from=this.labels.w_from/this.coords.w_rs*100,this.labels.p_from_left=this.coords.p_from+this.coords.p_handle/2-this.labels.p_from/2,this.labels.p_from_left=this.toFixed(this.labels.p_from_left),this.labels.p_from_left=this.checkEdges(this.labels.p_from_left,this.labels.p_from),this.labels.w_to=this.$cache.to.outerWidth(!1),
/* 37 */         this.labels.p_to=this.labels.w_to/this.coords.w_rs*100,this.labels.p_to_left=this.coords.p_to+this.coords.p_handle/2-this.labels.p_to/2,this.labels.p_to_left=this.toFixed(this.labels.p_to_left),this.labels.p_to_left=this.checkEdges(this.labels.p_to_left,this.labels.p_to),this.labels.w_single=this.$cache.single.outerWidth(!1),this.labels.p_single=this.labels.w_single/this.coords.w_rs*100,this.labels.p_single_left=(this.labels.p_from_left+this.labels.p_to_left+this.labels.p_to)/2-this.labels.p_single/
/* 38 */     2,this.labels.p_single_left=this.toFixed(this.labels.p_single_left)),this.labels.p_single_left=this.checkEdges(this.labels.p_single_left,this.labels.p_single))},updateScene:function(){this.drawHandles();this.raf_id=requestAnimationFrame(this.updateScene.bind(this))},drawHandles:function(){this.coords.w_rs=this.$cache.rs.outerWidth(!1);this.coords.w_rs!==this.coords.w_rs_old&&(this.target="base",this.is_resize=!0);if(this.coords.w_rs!==this.coords.w_rs_old||this.force_redraw)this.setMinMax(),this.calc(!0),
/* 39 */         this.drawLabels(),this.options.grid&&(this.calcGridMargin(),this.calcGridLabels()),this.force_redraw=!0,this.coords.w_rs_old=this.coords.w_rs,this.drawShadow();if(this.coords.w_rs&&(this.dragging||this.force_redraw||this.is_key)){if(this.old_from!==this.result.from||this.old_to!==this.result.to||this.force_redraw||this.is_key){this.drawLabels();this.$cache.bar[0].style.left=this.coords.p_bar_x+"%";this.$cache.bar[0].style.width=this.coords.p_bar_w+"%";if("single"===this.options.type)this.$cache.s_single[0].style.left=
/* 40 */         this.coords.p_single+"%",this.$cache.single[0].style.left=this.labels.p_single_left+"%",this.options.values.length?(this.$cache.input.prop("value",this.result.from_value),this.$cache.input.data("from",this.result.from_value)):(this.$cache.input.prop("value",this.result.from),this.$cache.input.data("from",this.result.from));else{this.$cache.s_from[0].style.left=this.coords.p_from+"%";this.$cache.s_to[0].style.left=this.coords.p_to+"%";if(this.old_from!==this.result.from||this.force_redraw)this.$cache.from[0].style.left=
/* 41 */         this.labels.p_from_left+"%";if(this.old_to!==this.result.to||this.force_redraw)this.$cache.to[0].style.left=this.labels.p_to_left+"%";this.$cache.single[0].style.left=this.labels.p_single_left+"%";this.options.values.length?(this.$cache.input.prop("value",this.result.from_value+";"+this.result.to_value),this.$cache.input.data("from",this.result.from_value),this.$cache.input.data("to",this.result.to_value)):(this.$cache.input.prop("value",this.result.from+";"+this.result.to),this.$cache.input.data("from",
/* 42 */         this.result.from),this.$cache.input.data("to",this.result.to))}this.old_from===this.result.from&&this.old_to===this.result.to||this.is_start||this.$cache.input.trigger("change");this.old_from=this.result.from;this.old_to=this.result.to;if(this.options.onChange&&"function"===typeof this.options.onChange&&!this.is_resize&&!this.is_update&&!this.is_start)this.options.onChange(this.result);if(this.options.onFinish&&"function"===typeof this.options.onFinish&&(this.is_key||this.is_click))this.options.onFinish(this.result);
/* 43 */         this.is_resize=this.is_update=!1}this.force_redraw=this.is_click=this.is_key=this.is_start=!1}},drawLabels:function(){var a=this.options.values.length,b=this.options.p_values,c;if(!this.options.hide_from_to)if("single"===this.options.type)a=a?this.decorate(b[this.result.from]):this.decorate(this._prettify(this.result.from),this.result.from),this.$cache.single.html(a),this.calcLabels(),this.$cache.min[0].style.visibility=this.labels.p_single_left<this.labels.p_min+1?"hidden":"visible",this.$cache.max[0].style.visibility=
/* 44 */         this.labels.p_single_left+this.labels.p_single>100-this.labels.p_max-1?"hidden":"visible";else{a?(this.options.decorate_both?(a=this.decorate(b[this.result.from]),a+=this.options.values_separator,a+=this.decorate(b[this.result.to])):a=this.decorate(b[this.result.from]+this.options.values_separator+b[this.result.to]),c=this.decorate(b[this.result.from]),b=this.decorate(b[this.result.to])):(this.options.decorate_both?(a=this.decorate(this._prettify(this.result.from)),a+=this.options.values_separator,
/* 45 */         a+=this.decorate(this._prettify(this.result.to))):a=this.decorate(this._prettify(this.result.from)+this.options.values_separator+this._prettify(this.result.to),this.result.from),c=this.decorate(this._prettify(this.result.from),this.result.from),b=this.decorate(this._prettify(this.result.to),this.result.to));this.$cache.single.html(a);this.$cache.from.html(c);this.$cache.to.html(b);this.calcLabels();b=Math.min(this.labels.p_single_left,this.labels.p_from_left);a=this.labels.p_single_left+this.labels.p_single;
/* 46 */         c=this.labels.p_to_left+this.labels.p_to;var d=Math.max(a,c);this.labels.p_from_left+this.labels.p_from>=this.labels.p_to_left?(this.$cache.from[0].style.visibility="hidden",this.$cache.to[0].style.visibility="hidden",this.$cache.single[0].style.visibility="visible",this.result.from===this.result.to?(this.$cache.from[0].style.visibility="visible",this.$cache.single[0].style.visibility="hidden",d=c):(this.$cache.from[0].style.visibility="hidden",this.$cache.single[0].style.visibility="visible",d=Math.max(a,
/* 47 */             c))):(this.$cache.from[0].style.visibility="visible",this.$cache.to[0].style.visibility="visible",this.$cache.single[0].style.visibility="hidden");this.$cache.min[0].style.visibility=b<this.labels.p_min+1?"hidden":"visible";this.$cache.max[0].style.visibility=d>100-this.labels.p_max-1?"hidden":"visible"}},drawShadow:function(){var a=this.options,b=this.$cache,c,d;"single"===a.type?a.from_shadow&&(a.from_min||a.from_max)?(c=this.calcPercent(a.from_min||a.min),d=this.calcPercent(a.from_max||a.max)-
/* 48 */     c,c=this.toFixed(c-this.coords.p_handle/100*c),d=this.toFixed(d-this.coords.p_handle/100*d),c+=this.coords.p_handle/2,b.shad_single[0].style.display="block",b.shad_single[0].style.left=c+"%",b.shad_single[0].style.width=d+"%"):b.shad_single[0].style.display="none":(a.from_shadow&&(a.from_min||a.from_max)?(c=this.calcPercent(a.from_min||a.min),d=this.calcPercent(a.from_max||a.max)-c,c=this.toFixed(c-this.coords.p_handle/100*c),d=this.toFixed(d-this.coords.p_handle/100*d),c+=this.coords.p_handle/2,
/* 49 */         b.shad_from[0].style.display="block",b.shad_from[0].style.left=c+"%",b.shad_from[0].style.width=d+"%"):b.shad_from[0].style.display="none",a.to_shadow&&(a.to_min||a.to_max)?(c=this.calcPercent(a.to_min||a.min),a=this.calcPercent(a.to_max||a.max)-c,c=this.toFixed(c-this.coords.p_handle/100*c),a=this.toFixed(a-this.coords.p_handle/100*a),c+=this.coords.p_handle/2,b.shad_to[0].style.display="block",b.shad_to[0].style.left=c+"%",b.shad_to[0].style.width=a+"%"):b.shad_to[0].style.display="none")},toggleInput:function(){this.$cache.input.toggleClass("irs-hidden-input")},
/* 50 */     calcPercent:function(a){return this.toFixed((a-this.options.min)/((this.options.max-this.options.min)/100))},calcReal:function(a){var b=this.options.min,c=this.options.max,d=0;0>b&&(d=Math.abs(b),b+=d,c+=d);a=(c-b)/100*a+b;(b=this.options.step.toString().split(".")[1])?a=+a.toFixed(b.length):(a/=this.options.step,a*=this.options.step,a=+a.toFixed(0));d&&(a-=d);a<this.options.min?a=this.options.min:a>this.options.max&&(a=this.options.max);return b?+a.toFixed(b.length):this.toFixed(a)},calcWithStep:function(a){var b=

/* ionrangeslider.js */

/* 51 */         Math.round(a/this.coords.p_step)*this.coords.p_step;100<b&&(b=100);100===a&&(b=100);return this.toFixed(b)},checkMinInterval:function(a,b,c){var d=this.options;if(!d.min_interval)return a;a=this.calcReal(a);b=this.calcReal(b);"from"===c?b-a<d.min_interval&&(a=b-d.min_interval):a-b<d.min_interval&&(a=b+d.min_interval);return this.calcPercent(a)},checkMaxInterval:function(a,b,c){var d=this.options;if(!d.max_interval)return a;a=this.calcReal(a);b=this.calcReal(b);"from"===c?b-a>d.max_interval&&(a=b-
/* 52 */     d.max_interval):a-b>d.max_interval&&(a=b+d.max_interval);return this.calcPercent(a)},checkDiapason:function(a,b,c){a=this.calcReal(a);var d=this.options;b&&"number"===typeof b||(b=d.min);c&&"number"===typeof c||(c=d.max);a<b&&(a=b);a>c&&(a=c);return this.calcPercent(a)},toFixed:function(a){a=a.toFixed(5);return+a},_prettify:function(a){return this.options.prettify_enabled?this.options.prettify&&"function"===typeof this.options.prettify?this.options.prettify(a):this.prettify(a):a},prettify:function(a){return a.toString().replace(/(\d{1,3}(?=(?:\d\d\d)+(?!\d)))/g,
/* 53 */         "$1"+this.options.prettify_separator)},checkEdges:function(a,b){if(!this.options.force_edges)return this.toFixed(a);0>a?a=0:a>100-b&&(a=100-b);return this.toFixed(a)},validate:function(){var a=this.options,b=this.result,c=a.values,d=c.length,h,f;"string"===typeof a.min&&(a.min=+a.min);"string"===typeof a.max&&(a.max=+a.max);"string"===typeof a.from&&(a.from=+a.from);"string"===typeof a.to&&(a.to=+a.to);"string"===typeof a.step&&(a.step=+a.step);"string"===typeof a.from_min&&(a.from_min=+a.from_min);
/* 54 */         "string"===typeof a.from_max&&(a.from_max=+a.from_max);"string"===typeof a.to_min&&(a.to_min=+a.to_min);"string"===typeof a.to_max&&(a.to_max=+a.to_max);"string"===typeof a.keyboard_step&&(a.keyboard_step=+a.keyboard_step);"string"===typeof a.grid_num&&(a.grid_num=+a.grid_num);a.max<=a.min&&(a.max=a.min?2*a.min:a.min+1,a.step=1);if(d)for(a.p_values=[],a.min=0,a.max=d-1,a.step=1,a.grid_num=a.max,a.grid_snap=!0,f=0;f<d;f++)h=+c[f],isNaN(h)?h=c[f]:(c[f]=h,h=this._prettify(h)),a.p_values.push(h);if("number"!==
/* 55 */             typeof a.from||isNaN(a.from))a.from=a.min;if("number"!==typeof a.to||isNaN(a.from))a.to=a.max;if(a.from<a.min||a.from>a.max)a.from=a.min;if(a.to>a.max||a.to<a.min)a.to=a.max;"double"===a.type&&a.from>a.to&&(a.from=a.to);if("number"!==typeof a.step||isNaN(a.step)||!a.step||0>a.step)a.step=1;if("number"!==typeof a.keyboard_step||isNaN(a.keyboard_step)||!a.keyboard_step||0>a.keyboard_step)a.keyboard_step=5;a.from_min&&a.from<a.from_min&&(a.from=a.from_min);a.from_max&&a.from>a.from_max&&(a.from=a.from_max);
/* 56 */         a.to_min&&a.to<a.to_min&&(a.to=a.to_min);a.to_max&&a.from>a.to_max&&(a.to=a.to_max);if(b){b.min!==a.min&&(b.min=a.min);b.max!==a.max&&(b.max=a.max);if(b.from<b.min||b.from>b.max)b.from=a.from;if(b.to<b.min||b.to>b.max)b.to=a.to}if("number"!==typeof a.min_interval||isNaN(a.min_interval)||!a.min_interval||0>a.min_interval)a.min_interval=0;if("number"!==typeof a.max_interval||isNaN(a.max_interval)||!a.max_interval||0>a.max_interval)a.max_interval=0;a.min_interval&&a.min_interval>a.max-a.min&&(a.min_interval=
/* 57 */             a.max-a.min);a.max_interval&&a.max_interval>a.max-a.min&&(a.max_interval=a.max-a.min)},decorate:function(a,b){var c="",d=this.options;d.prefix&&(c+=d.prefix);c+=a;d.max_postfix&&(d.values.length&&a===d.p_values[d.max]?(c+=d.max_postfix,d.postfix&&(c+=" ")):b===d.max&&(c+=d.max_postfix,d.postfix&&(c+=" ")));d.postfix&&(c+=d.postfix);return c},updateFrom:function(){this.result.from=this.options.from;this.result.from_percent=this.calcPercent(this.result.from);this.options.values&&(this.result.from_value=
/* 58 */         this.options.values[this.result.from])},updateTo:function(){this.result.to=this.options.to;this.result.to_percent=this.calcPercent(this.result.to);this.options.values&&(this.result.to_value=this.options.values[this.result.to])},updateResult:function(){this.result.min=this.options.min;this.result.max=this.options.max;this.updateFrom();this.updateTo()},appendGrid:function(){if(this.options.grid){var a=this.options,b,c;b=a.max-a.min;var d=a.grid_num,h=0,f=0,k=4,e,g,m=0,n="";this.calcGridMargin();a.grid_snap?
/* 59 */         (d=b/a.step,h=this.toFixed(a.step/(b/100))):h=this.toFixed(100/d);4<d&&(k=3);7<d&&(k=2);14<d&&(k=1);28<d&&(k=0);for(b=0;b<d+1;b++){e=k;f=this.toFixed(h*b);100<f&&(f=100,e-=2,0>e&&(e=0));this.coords.big[b]=f;g=(f-h*(b-1))/(e+1);for(c=1;c<=e&&0!==f;c++)m=this.toFixed(f-g*c),n+='<span class="irs-grid-pol small" style="left: '+m+'%"></span>';n+='<span class="irs-grid-pol" style="left: '+f+'%"></span>';m=this.calcReal(f);m=a.values.length?a.p_values[m]:this._prettify(m);n+='<span class="irs-grid-text js-grid-text-'+
/* 60 */     b+'" style="left: '+f+'%">'+m+"</span>"}this.coords.big_num=Math.ceil(d+1);this.$cache.cont.addClass("irs-with-grid");this.$cache.grid.html(n);this.cacheGridLabels()}},cacheGridLabels:function(){var a,b,c=this.coords.big_num;for(b=0;b<c;b++)a=this.$cache.grid.find(".js-grid-text-"+b),this.$cache.grid_labels.push(a);this.calcGridLabels()},calcGridLabels:function(){var a,b;b=[];var c=[],d=this.coords.big_num;for(a=0;a<d;a++)this.coords.big_w[a]=this.$cache.grid_labels[a].outerWidth(!1),this.coords.big_p[a]=
/* 61 */         this.toFixed(this.coords.big_w[a]/this.coords.w_rs*100),this.coords.big_x[a]=this.toFixed(this.coords.big_p[a]/2),b[a]=this.toFixed(this.coords.big[a]-this.coords.big_x[a]),c[a]=this.toFixed(b[a]+this.coords.big_p[a]);this.options.force_edges&&(b[0]<this.coords.grid_gap&&(b[0]=this.coords.grid_gap,c[0]=this.toFixed(b[0]+this.coords.big_p[0]),this.coords.big_x[0]=this.coords.grid_gap),c[d-1]>100-this.coords.grid_gap&&(c[d-1]=100-this.coords.grid_gap,b[d-1]=this.toFixed(c[d-1]-this.coords.big_p[d-1]),
/* 62 */         this.coords.big_x[d-1]=this.toFixed(this.coords.big_p[d-1]-this.coords.grid_gap)));this.calcGridCollision(2,b,c);this.calcGridCollision(4,b,c);for(a=0;a<d;a++)b=this.$cache.grid_labels[a][0],b.style.marginLeft=-this.coords.big_x[a]+"%"},calcGridCollision:function(a,b,c){var d,e,f,g=this.coords.big_num;for(d=0;d<g;d+=a){e=d+a/2;if(e>=g)break;f=this.$cache.grid_labels[e][0];f.style.visibility=c[d]<=b[e]?"visible":"hidden"}},calcGridMargin:function(){this.options.grid_margin&&(this.coords.w_rs=this.$cache.rs.outerWidth(!1),
/* 63 */     this.coords.w_rs&&(this.coords.w_handle="single"===this.options.type?this.$cache.s_single.outerWidth(!1):this.$cache.s_from.outerWidth(!1),this.coords.p_handle=this.toFixed(this.coords.w_handle/this.coords.w_rs*100),this.coords.grid_gap=this.toFixed(this.coords.p_handle/2-.1),this.$cache.grid[0].style.width=this.toFixed(100-this.coords.p_handle)+"%",this.$cache.grid[0].style.left=this.coords.grid_gap+"%"))},update:function(a){this.is_update=!0;this.options=e.extend(this.options,a);this.validate();
/* 64 */         this.updateResult(a);this.toggleInput();this.remove();this.init(!0)},reset:function(){this.updateResult();this.update()},destroy:function(){this.toggleInput();this.$cache.input.prop("readonly",!1);e.data(this.input,"ionRangeSlider",null);this.remove();this.options=this.input=null}};e.fn.ionRangeSlider=function(a){return this.each(function(){e.data(this,"ionRangeSlider")||e.data(this,"ionRangeSlider",new r(this,a,t++))})};(function(){for(var a=0,b=["ms","moz","webkit","o"],c=0;c<b.length&&!g.requestAnimationFrame;++c)g.requestAnimationFrame=
/* 65 */     g[b[c]+"RequestAnimationFrame"],g.cancelAnimationFrame=g[b[c]+"CancelAnimationFrame"]||g[b[c]+"CancelRequestAnimationFrame"];g.requestAnimationFrame||(g.requestAnimationFrame=function(b,c){var f=(new Date).getTime(),e=Math.max(0,16-(f-a)),l=g.setTimeout(function(){b(f+e)},e);a=f+e;return l});g.cancelAnimationFrame||(g.cancelAnimationFrame=function(a){clearTimeout(a)})})()})(jQuery,document,window,navigator);

;
/* icheck.js */

/* 1   */ /*!
/* 2   *|  * iCheck v1.0.2, http://git.io/arlzeA
/* 3   *|  * ===================================
/* 4   *|  * Powerful jQuery and Zepto plugin for checkboxes and radio buttons customization
/* 5   *|  *
/* 6   *|  * (c) 2013 Damir Sultanov, http://fronteed.com
/* 7   *|  * MIT Licensed
/* 8   *|  */
/* 9   */ 
/* 10  */ (function($) {
/* 11  */ 
/* 12  */     // Cached vars
/* 13  */     var _iCheck = 'iCheck',
/* 14  */         _iCheckHelper = _iCheck + '-helper',
/* 15  */         _checkbox = 'checkbox',
/* 16  */         _radio = 'radio',
/* 17  */         _checked = 'checked',
/* 18  */         _unchecked = 'un' + _checked,
/* 19  */         _disabled = 'disabled',
/* 20  */         a
/* 21  */         _determinate = 'determinate',
/* 22  */         _indeterminate = 'in' + _determinate,
/* 23  */         _update = 'update',
/* 24  */         _type = 'type',
/* 25  */         _click = 'click',
/* 26  */         _touch = 'touchbegin.i touchend.i',
/* 27  */         _add = 'addClass',
/* 28  */         _remove = 'removeClass',
/* 29  */         _callback = 'trigger',
/* 30  */         _label = 'label',
/* 31  */         _cursor = 'cursor',
/* 32  */         _mobile = /ipad|iphone|ipod|android|blackberry|windows phone|opera mini|silk/i.test(navigator.userAgent);
/* 33  */ 
/* 34  */     // Plugin init
/* 35  */     $.fn[_iCheck] = function(options, fire) {
/* 36  */ 
/* 37  */         // Walker
/* 38  */         var handle = 'input[type="' + _checkbox + '"], input[type="' + _radio + '"]',
/* 39  */             stack = $(),
/* 40  */             walker = function(object) {
/* 41  */                 object.each(function() {
/* 42  */                     var self = $(this);
/* 43  */ 
/* 44  */                     if (self.is(handle)) {
/* 45  */                         stack = stack.add(self);
/* 46  */                     } else {
/* 47  */                         stack = stack.add(self.find(handle));
/* 48  */                     }
/* 49  */                 });
/* 50  */             };

/* icheck.js */

/* 51  */ 
/* 52  */         // Check if we should operate with some method
/* 53  */         if (/^(check|uncheck|toggle|indeterminate|determinate|disable|enable|update|destroy)$/i.test(options)) {
/* 54  */ 
/* 55  */             // Normalize method's name
/* 56  */             options = options.toLowerCase();
/* 57  */ 
/* 58  */             // Find checkboxes and radio buttons
/* 59  */             walker(this);
/* 60  */ 
/* 61  */             return stack.each(function() {
/* 62  */                 var self = $(this);
/* 63  */ 
/* 64  */                 if (options == 'destroy') {
/* 65  */                     tidy(self, 'ifDestroyed');
/* 66  */                 } else {
/* 67  */                     operate(self, true, options);
/* 68  */                 }
/* 69  */ 
/* 70  */                 // Fire method's callback
/* 71  */                 if ($.isFunction(fire)) {
/* 72  */                     fire();
/* 73  */                 }
/* 74  */             });
/* 75  */ 
/* 76  */             // Customization
/* 77  */         } else if (typeof options == 'object' || !options) {
/* 78  */ 
/* 79  */             // Check if any options were passed
/* 80  */             var settings = $.extend({
/* 81  */                 checkedClass: _checked,
/* 82  */                 disabledClass: _disabled,
/* 83  */                 indeterminateClass: _indeterminate,
/* 84  */                 labelHover: true
/* 85  */             }, options),
/* 86  */ 
/* 87  */                 selector = settings.handle,
/* 88  */                 hoverClass = settings.hoverClass || 'hover',
/* 89  */                 focusClass = settings.focusClass || 'focus',
/* 90  */                 activeClass = settings.activeClass || 'active',
/* 91  */                 labelHover = !! settings.labelHover,
/* 92  */                 labelHoverClass = settings.labelHoverClass || 'hover',
/* 93  */ 
/* 94  */                 // Setup clickable area
/* 95  */                 area = ('' + settings.increaseArea).replace('%', '') | 0;
/* 96  */ 
/* 97  */             // Selector limit
/* 98  */             if (selector == _checkbox || selector == _radio) {
/* 99  */                 handle = 'input[type="' + selector + '"]';
/* 100 */             }

/* icheck.js */

/* 101 */ 
/* 102 */             // Clickable area limit
/* 103 */             if (area < -50) {
/* 104 */                 area = -50;
/* 105 */             }
/* 106 */ 
/* 107 */             // Walk around the selector
/* 108 */             walker(this);
/* 109 */ 
/* 110 */             return stack.each(function() {
/* 111 */                 var self = $(this);
/* 112 */ 
/* 113 */                 // If already customized
/* 114 */                 tidy(self);
/* 115 */ 
/* 116 */                 var node = this,
/* 117 */                     id = node.id,
/* 118 */ 
/* 119 */                     // Layer styles
/* 120 */                     offset = -area + '%',
/* 121 */                     size = 100 + (area * 2) + '%',
/* 122 */                     layer = {
/* 123 */                         position: 'absolute',
/* 124 */                         top: offset,
/* 125 */                         left: offset,
/* 126 */                         display: 'block',
/* 127 */                         width: size,
/* 128 */                         height: size,
/* 129 */                         margin: 0,
/* 130 */                         padding: 0,
/* 131 */                         background: '#fff',
/* 132 */                         border: 0,
/* 133 */                         opacity: 0
/* 134 */                     },
/* 135 */ 
/* 136 */                     // Choose how to hide input
/* 137 */                     hide = _mobile ? {
/* 138 */                         position: 'absolute',
/* 139 */                         visibility: 'hidden'
/* 140 */                     } : area ? layer : {
/* 141 */                         position: 'absolute',
/* 142 */                         opacity: 0
/* 143 */                     },
/* 144 */ 
/* 145 */                     // Get proper class
/* 146 */                     className = node[_type] == _checkbox ? settings.checkboxClass || 'i' + _checkbox : settings.radioClass || 'i' + _radio,
/* 147 */ 
/* 148 */                     // Find assigned labels
/* 149 */                     label = $(_label + '[for="' + id + '"]').add(self.closest(_label)),
/* 150 */ 

/* icheck.js */

/* 151 */                     // Check ARIA option
/* 152 */                     aria = !! settings.aria,
/* 153 */ 
/* 154 */                     // Set ARIA placeholder
/* 155 */                     ariaID = _iCheck + '-' + Math.random().toString(36).substr(2, 6),
/* 156 */ 
/* 157 */                     // Parent & helper
/* 158 */                     parent = '<div class="' + className + '" ' + (aria ? 'role="' + node[_type] + '" ' : ''),
/* 159 */                     helper;
/* 160 */ 
/* 161 */                 // Set ARIA "labelledby"
/* 162 */                 if (aria) {
/* 163 */                     label.each(function() {
/* 164 */                         parent += 'aria-labelledby="';
/* 165 */ 
/* 166 */                         if (this.id) {
/* 167 */                             parent += this.id;
/* 168 */                         } else {
/* 169 */                             this.id = ariaID;
/* 170 */                             parent += ariaID;
/* 171 */                         }
/* 172 */ 
/* 173 */                         parent += '"';
/* 174 */                     });
/* 175 */                 }
/* 176 */ 
/* 177 */                 // Wrap input
/* 178 */                 parent = self.wrap(parent + '/>')[_callback]('ifCreated').parent().append(settings.insert);
/* 179 */ 
/* 180 */                 // Layer addition
/* 181 */                 helper = $('<ins class="' + _iCheckHelper + '"/>').css(layer).appendTo(parent);
/* 182 */ 
/* 183 */                 // Finalize customization
/* 184 */                 self.data(_iCheck, {
/* 185 */                     o: settings,
/* 186 */                     s: self.attr('style')
/* 187 */                 }).css(hide); !! settings.inheritClass && parent[_add](node.className || ''); !! settings.inheritID && id && parent.attr('id', _iCheck + '-' + id);
/* 188 */                 parent.css('position') == 'static' && parent.css('position', 'relative');
/* 189 */                 operate(self, true, _update);
/* 190 */ 
/* 191 */                 // Label events
/* 192 */                 if (label.length) {
/* 193 */                     label.on(_click + '.i mouseover.i mouseout.i ' + _touch, function(event) {
/* 194 */                         var type = event[_type],
/* 195 */                             item = $(this);
/* 196 */ 
/* 197 */                         // Do nothing if input is disabled
/* 198 */                         if (!node[_disabled]) {
/* 199 */ 
/* 200 */                             // Click

/* icheck.js */

/* 201 */                             if (type == _click) {
/* 202 */                                 if ($(event.target).is('a')) {
/* 203 */                                     return;
/* 204 */                                 }
/* 205 */                                 operate(self, false, true);
/* 206 */ 
/* 207 */                                 // Hover state
/* 208 */                             } else if (labelHover) {
/* 209 */ 
/* 210 */                                 // mouseout|touchend
/* 211 */                                 if (/ut|nd/.test(type)) {
/* 212 */                                     parent[_remove](hoverClass);
/* 213 */                                     item[_remove](labelHoverClass);
/* 214 */                                 } else {
/* 215 */                                     parent[_add](hoverClass);
/* 216 */                                     item[_add](labelHoverClass);
/* 217 */                                 }
/* 218 */                             }
/* 219 */ 
/* 220 */                             if (_mobile) {
/* 221 */                                 event.stopPropagation();
/* 222 */                             } else {
/* 223 */                                 return false;
/* 224 */                             }
/* 225 */                         }
/* 226 */                     });
/* 227 */                 }
/* 228 */ 
/* 229 */                 // Input events
/* 230 */                 self.on(_click + '.i focus.i blur.i keyup.i keydown.i keypress.i', function(event) {
/* 231 */                     var type = event[_type],
/* 232 */                         key = event.keyCode;
/* 233 */ 
/* 234 */                     // Click
/* 235 */                     if (type == _click) {
/* 236 */                         return false;
/* 237 */ 
/* 238 */                         // Keydown
/* 239 */                     } else if (type == 'keydown' && key == 32) {
/* 240 */                         if (!(node[_type] == _radio && node[_checked])) {
/* 241 */                             if (node[_checked]) {
/* 242 */                                 off(self, _checked);
/* 243 */                             } else {
/* 244 */                                 on(self, _checked);
/* 245 */                             }
/* 246 */                         }
/* 247 */ 
/* 248 */                         return false;
/* 249 */ 
/* 250 */                         // Keyup

/* icheck.js */

/* 251 */                     } else if (type == 'keyup' && node[_type] == _radio) {
/* 252 */                         !node[_checked] && on(self, _checked);
/* 253 */ 
/* 254 */                         // Focus/blur
/* 255 */                     } else if (/us|ur/.test(type)) {
/* 256 */                         parent[type == 'blur' ? _remove : _add](focusClass);
/* 257 */                     }
/* 258 */                 });
/* 259 */ 
/* 260 */                 // Helper events
/* 261 */                 helper.on(_click + ' mousedown mouseup mouseover mouseout ' + _touch, function(event) {
/* 262 */                     var type = event[_type],
/* 263 */ 
/* 264 */                         // mousedown|mouseup
/* 265 */                         toggle = /wn|up/.test(type) ? activeClass : hoverClass;
/* 266 */ 
/* 267 */                     // Do nothing if input is disabled
/* 268 */                     if (!node[_disabled]) {
/* 269 */ 
/* 270 */                         // Click
/* 271 */                         if (type == _click) {
/* 272 */                             operate(self, false, true);
/* 273 */ 
/* 274 */                             // Active and hover states
/* 275 */                         } else {
/* 276 */ 
/* 277 */                             // State is on
/* 278 */                             if (/wn|er|in/.test(type)) {
/* 279 */ 
/* 280 */                                 // mousedown|mouseover|touchbegin
/* 281 */                                 parent[_add](toggle);
/* 282 */ 
/* 283 */                                 // State is off
/* 284 */                             } else {
/* 285 */                                 parent[_remove](toggle + ' ' + activeClass);
/* 286 */                             }
/* 287 */ 
/* 288 */                             // Label hover
/* 289 */                             if (label.length && labelHover && toggle == hoverClass) {
/* 290 */ 
/* 291 */                                 // mouseout|touchend
/* 292 */                                 label[/ut|nd/.test(type) ? _remove : _add](labelHoverClass);
/* 293 */                             }
/* 294 */                         }
/* 295 */ 
/* 296 */                         if (_mobile) {
/* 297 */                             event.stopPropagation();
/* 298 */                         } else {
/* 299 */                             return false;
/* 300 */                         }

/* icheck.js */

/* 301 */                     }
/* 302 */                 });
/* 303 */             });
/* 304 */         } else {
/* 305 */             return this;
/* 306 */         }
/* 307 */     };
/* 308 */ 
/* 309 */     // Do something with inputs
/* 310 */     function operate(input, direct, method) {
/* 311 */         var node = input[0],
/* 312 */             state = /er/.test(method) ? _indeterminate : /bl/.test(method) ? _disabled : _checked,
/* 313 */             active = method == _update ? {
/* 314 */                 checked: node[_checked],
/* 315 */                 disabled: node[_disabled],
/* 316 */                 indeterminate: input.attr(_indeterminate) == 'true' || input.attr(_determinate) == 'false'
/* 317 */             } : node[state];
/* 318 */ 
/* 319 */         // Check, disable or indeterminate
/* 320 */         if (/^(ch|di|in)/.test(method) && !active) {
/* 321 */             on(input, state);
/* 322 */ 
/* 323 */             // Uncheck, enable or determinate
/* 324 */         } else if (/^(un|en|de)/.test(method) && active) {
/* 325 */             off(input, state);
/* 326 */ 
/* 327 */             // Update
/* 328 */         } else if (method == _update) {
/* 329 */ 
/* 330 */             // Handle states
/* 331 */             for (var each in active) {
/* 332 */                 if (active[each]) {
/* 333 */                     on(input, each, true);
/* 334 */                 } else {
/* 335 */                     off(input, each, true);
/* 336 */                 }
/* 337 */             }
/* 338 */ 
/* 339 */         } else if (!direct || method == 'toggle') {
/* 340 */ 
/* 341 */             // Helper or label was clicked
/* 342 */             if (!direct) {
/* 343 */                 input[_callback]('ifClicked');
/* 344 */             }
/* 345 */ 
/* 346 */             // Toggle checked state
/* 347 */             if (active) {
/* 348 */                 if (node[_type] !== _radio) {
/* 349 */                     off(input, state);
/* 350 */                 }

/* icheck.js */

/* 351 */             } else {
/* 352 */                 on(input, state);
/* 353 */             }
/* 354 */         }
/* 355 */     }
/* 356 */ 
/* 357 */     // Add checked, disabled or indeterminate state
/* 358 */     function on(input, state, keep) {
/* 359 */         var node = input[0],
/* 360 */             parent = input.parent(),
/* 361 */             checked = state == _checked,
/* 362 */             indeterminate = state == _indeterminate,
/* 363 */             disabled = state == _disabled,
/* 364 */             callback = indeterminate ? _determinate : checked ? _unchecked : 'enabled',
/* 365 */             regular = option(input, callback + capitalize(node[_type])),
/* 366 */             specific = option(input, state + capitalize(node[_type]));
/* 367 */ 
/* 368 */         // Prevent unnecessary actions
/* 369 */         if (node[state] !== true) {
/* 370 */ 
/* 371 */             // Toggle assigned radio buttons
/* 372 */             if (!keep && state == _checked && node[_type] == _radio && node.name) {
/* 373 */                 var form = input.closest('form'),
/* 374 */                     inputs = 'input[name="' + node.name + '"]';
/* 375 */ 
/* 376 */                 inputs = form.length ? form.find(inputs) : $(inputs);
/* 377 */ 
/* 378 */                 inputs.each(function() {
/* 379 */                     if (this !== node && $(this).data(_iCheck)) {
/* 380 */                         off($(this), state);
/* 381 */                     }
/* 382 */                 });
/* 383 */             }
/* 384 */ 
/* 385 */             // Indeterminate state
/* 386 */             if (indeterminate) {
/* 387 */ 
/* 388 */                 // Add indeterminate state
/* 389 */                 node[state] = true;
/* 390 */ 
/* 391 */                 // Remove checked state
/* 392 */                 if (node[_checked]) {
/* 393 */                     off(input, _checked, 'force');
/* 394 */                 }
/* 395 */ 
/* 396 */                 // Checked or disabled state
/* 397 */             } else {
/* 398 */ 
/* 399 */                 // Add checked or disabled state
/* 400 */                 if (!keep) {

/* icheck.js */

/* 401 */                     node[state] = true;
/* 402 */                 }
/* 403 */ 
/* 404 */                 // Remove indeterminate state
/* 405 */                 if (checked && node[_indeterminate]) {
/* 406 */                     off(input, _indeterminate, false);
/* 407 */                 }
/* 408 */             }
/* 409 */ 
/* 410 */             // Trigger callbacks
/* 411 */             callbacks(input, checked, state, keep);
/* 412 */         }
/* 413 */ 
/* 414 */         // Add proper cursor
/* 415 */         if (node[_disabled] && !! option(input, _cursor, true)) {
/* 416 */             parent.find('.' + _iCheckHelper).css(_cursor, 'default');
/* 417 */         }
/* 418 */ 
/* 419 */         // Add state class
/* 420 */         parent[_add](specific || option(input, state) || '');
/* 421 */ 
/* 422 */         // Set ARIA attribute
/* 423 */         if ( !! parent.attr('role') && !indeterminate) {
/* 424 */             parent.attr('aria-' + (disabled ? _disabled : _checked), 'true');
/* 425 */         }
/* 426 */ 
/* 427 */         // Remove regular state class
/* 428 */         parent[_remove](regular || option(input, callback) || '');
/* 429 */     }
/* 430 */ 
/* 431 */     // Remove checked, disabled or indeterminate state
/* 432 */     function off(input, state, keep) {
/* 433 */         var node = input[0],
/* 434 */             parent = input.parent(),
/* 435 */             checked = state == _checked,
/* 436 */             indeterminate = state == _indeterminate,
/* 437 */             disabled = state == _disabled,
/* 438 */             callback = indeterminate ? _determinate : checked ? _unchecked : 'enabled',
/* 439 */             regular = option(input, callback + capitalize(node[_type])),
/* 440 */             specific = option(input, state + capitalize(node[_type]));
/* 441 */ 
/* 442 */         // Prevent unnecessary actions
/* 443 */         if (node[state] !== false) {
/* 444 */ 
/* 445 */             // Toggle state
/* 446 */             if (indeterminate || !keep || keep == 'force') {
/* 447 */                 node[state] = false;
/* 448 */             }
/* 449 */ 
/* 450 */             // Trigger callbacks

/* icheck.js */

/* 451 */             callbacks(input, checked, callback, keep);
/* 452 */         }
/* 453 */ 
/* 454 */         // Add proper cursor
/* 455 */         if (!node[_disabled] && !! option(input, _cursor, true)) {
/* 456 */             parent.find('.' + _iCheckHelper).css(_cursor, 'pointer');
/* 457 */         }
/* 458 */ 
/* 459 */         // Remove state class
/* 460 */         parent[_remove](specific || option(input, state) || '');
/* 461 */ 
/* 462 */         // Set ARIA attribute
/* 463 */         if ( !! parent.attr('role') && !indeterminate) {
/* 464 */             parent.attr('aria-' + (disabled ? _disabled : _checked), 'false');
/* 465 */         }
/* 466 */ 
/* 467 */         // Add regular state class
/* 468 */         parent[_add](regular || option(input, callback) || '');
/* 469 */     }
/* 470 */ 
/* 471 */     // Remove all traces
/* 472 */     function tidy(input, callback) {
/* 473 */         if (input.data(_iCheck)) {
/* 474 */ 
/* 475 */             // Remove everything except input
/* 476 */             input.parent().html(input.attr('style', input.data(_iCheck).s || ''));
/* 477 */ 
/* 478 */             // Callback
/* 479 */             if (callback) {
/* 480 */                 input[_callback](callback);
/* 481 */             }
/* 482 */ 
/* 483 */             // Unbind events
/* 484 */             input.off('.i').unwrap();
/* 485 */             $(_label + '[for="' + input[0].id + '"]').add(input.closest(_label)).off('.i');
/* 486 */         }
/* 487 */     }
/* 488 */ 
/* 489 */     // Get some option
/* 490 */     function option(input, state, regular) {
/* 491 */         if (input.data(_iCheck)) {
/* 492 */             return input.data(_iCheck).o[state + (regular ? '' : 'Class')];
/* 493 */         }
/* 494 */     }
/* 495 */ 
/* 496 */     // Capitalize some string
/* 497 */     function capitalize(string) {
/* 498 */         return string.charAt(0).toUpperCase() + string.slice(1);
/* 499 */     }
/* 500 */ 

/* icheck.js */

/* 501 */     // Executable handlers
/* 502 */     function callbacks(input, checked, callback, keep) {
/* 503 */         if (!keep) {
/* 504 */             if (checked) {
/* 505 */                 input[_callback]('ifToggled');
/* 506 */             }
/* 507 */ 
/* 508 */             input[_callback]('ifChanged')[_callback]('if' + capitalize(callback));
/* 509 */         }
/* 510 */     }
/* 511 */ })(window.jQuery || window.Zepto);

;
/* fotorama.js */

/* 1    */ /*!
/* 2    *|  * Fotorama 4.5.1 | http://fotorama.io/license/
/* 3    *|  */
/* 4    */ ! function(a, b, c, d, e) {
/* 5    */     "use strict";
/* 6    */ 
/* 7    */     function f(a) {
/* 8    */         var b = "bez_" + d.makeArray(arguments).join("_").replace(".", "p");
/* 9    */         if ("function" != typeof d.easing[b]) {
/* 10   */             var c = function(a, b) {
/* 11   */                 var c = [null, null],
/* 12   */                     d = [null, null],
/* 13   */                     e = [null, null],
/* 14   */                     f = function(f, g) {
/* 15   */                         return e[g] = 3 * a[g], d[g] = 3 * (b[g] - a[g]) - e[g], c[g] = 1 - e[g] - d[g], f * (e[g] + f * (d[g] + f * c[g]))
/* 16   */                     }, g = function(a) {
/* 17   */                         return e[0] + a * (2 * d[0] + 3 * c[0] * a)
/* 18   */                     }, h = function(a) {
/* 19   */                         for (var b, c = a, d = 0; ++d < 14 && (b = f(c, 0) - a, !(Math.abs(b) < .001));) c -= b / g(c);
/* 20   */                         return c
/* 21   */                     };
/* 22   */                 return function(a) {
/* 23   */                     return f(h(a), 1)
/* 24   */                 }
/* 25   */             };
/* 26   */             d.easing[b] = function(b, d, e, f, g) {
/* 27   */                 return f * c([a[0], a[1]], [a[2], a[3]])(d / g) + e
/* 28   */             }
/* 29   */         }
/* 30   */         return b
/* 31   */     }
/* 32   */ 
/* 33   */     function g() {}
/* 34   */ 
/* 35   */     function h(a, b, c) {
/* 36   */         return Math.max(isNaN(b) ? -1 / 0 : b, Math.min(isNaN(c) ? 1 / 0 : c, a))
/* 37   */     }
/* 38   */ 
/* 39   */     function i(a) {
/* 40   */         return a.match(/ma/) && a.match(/-?\d+(?!d)/g)[a.match(/3d/) ? 12 : 4]
/* 41   */     }
/* 42   */ 
/* 43   */     function j(a) {
/* 44   */         return Ec ? +i(a.css("transform")) : +a.css("left").replace("px", "")
/* 45   */     }
/* 46   */ 
/* 47   */     function k(a, b) {
/* 48   */         var c = {};
/* 49   */         return Ec ? c.transform = "translate3d(" + (a + (b ? .001 : 0)) + "px,0,0)" : c.left = a, c
/* 50   */     }

/* fotorama.js */

/* 51   */ 
/* 52   */     function l(a) {
/* 53   */         return {
/* 54   */             "transition-duration": a + "ms"
/* 55   */         }
/* 56   */     }
/* 57   */ 
/* 58   */     function m(a, b) {
/* 59   */         return +String(a).replace(b || "px", "") || e
/* 60   */     }
/* 61   */ 
/* 62   */     function n(a) {
/* 63   */         return /%$/.test(a) && m(a, "%")
/* 64   */     }
/* 65   */ 
/* 66   */     function o(a, b) {
/* 67   */         return n(a) / 100 * b || m(a)
/* 68   */     }
/* 69   */ 
/* 70   */     function p(a) {
/* 71   */         return ( !! m(a) || !! m(a, "%")) && a
/* 72   */     }
/* 73   */ 
/* 74   */     function q(a, b, c, d) {
/* 75   */         return (a - (d || 0)) * (b + (c || 0))
/* 76   */     }
/* 77   */ 
/* 78   */     function r(a, b, c, d) {
/* 79   */         return -Math.round(a / (b + (c || 0)) - (d || 0))
/* 80   */     }
/* 81   */ 
/* 82   */     function s(a) {
/* 83   */         var b = a.data();
/* 84   */         if (!b.tEnd) {
/* 85   */             var c = a[0],
/* 86   */                 d = {
/* 87   */                     WebkitTransition: "webkitTransitionEnd",
/* 88   */                     MozTransition: "transitionend",
/* 89   */                     OTransition: "oTransitionEnd otransitionend",
/* 90   */                     msTransition: "MSTransitionEnd",
/* 91   */                     transition: "transitionend"
/* 92   */                 };
/* 93   */             c.addEventListener(d[mc.prefixed("transition")], function(a) {
/* 94   */                 b.tProp && a.propertyName.match(b.tProp) && b.onEndFn()
/* 95   */             }, !1), b.tEnd = !0
/* 96   */         }
/* 97   */     }
/* 98   */ 
/* 99   */     function t(a, b, c, d) {
/* 100  */         var e, f = a.data();

/* fotorama.js */

/* 101  */         f && (f.onEndFn = function() {
/* 102  */             e || (e = !0, clearTimeout(f.tT), c())
/* 103  */         }, f.tProp = b, clearTimeout(f.tT), f.tT = setTimeout(function() {
/* 104  */             f.onEndFn()
/* 105  */         }, 1.5 * d), s(a))
/* 106  */     }
/* 107  */ 
/* 108  */     function u(a, b, c) {
/* 109  */         if (a.length) {
/* 110  */             var d = a.data();
/* 111  */             Ec ? (a.css(l(0)), d.onEndFn = g, clearTimeout(d.tT)) : a.stop();
/* 112  */             var e = v(b, function() {
/* 113  */                 return j(a)
/* 114  */             });
/* 115  */             return a.css(k(e, c)), e
/* 116  */         }
/* 117  */     }
/* 118  */ 
/* 119  */     function v() {
/* 120  */         for (var a, b = 0, c = arguments.length; c > b && (a = b ? arguments[b]() : arguments[b], "number" != typeof a); b++);
/* 121  */         return a
/* 122  */     }
/* 123  */ 
/* 124  */     function w(a, b) {
/* 125  */         return Math.round(a + (b - a) / 1.5)
/* 126  */     }
/* 127  */ 
/* 128  */     function x() {
/* 129  */         return x.p = x.p || ("https:" === c.protocol ? "https://" : "http://"), x.p
/* 130  */     }
/* 131  */ 
/* 132  */     function y(a) {
/* 133  */         var c = b.createElement("a");
/* 134  */         return c.href = a, c
/* 135  */     }
/* 136  */ 
/* 137  */     function z(a, b) {
/* 138  */         if ("string" != typeof a) return a;
/* 139  */         a = y(a);
/* 140  */         var c, d;
/* 141  */         if (a.host.match(/youtube\.com/) && a.search) {
/* 142  */             if (c = a.search.split("v=")[1]) {
/* 143  */                 var e = c.indexOf("&"); - 1 !== e && (c = c.substring(0, e)), d = "youtube"
/* 144  */             }
/* 145  */         } else a.host.match(/youtube\.com|youtu\.be/) ? (c = a.pathname.replace(/^\/(embed\/|v\/)?/, "").replace(/\/.*/, ""), d = "youtube") : a.host.match(/vimeo\.com/) && (d = "vimeo", c = a.pathname.replace(/^\/(video\/)?/, "").replace(/\/.*/, ""));
/* 146  */         return c && d || !b || (c = a.href, d = "custom"), c ? {
/* 147  */             id: c,
/* 148  */             type: d,
/* 149  */             s: a.search.replace(/^\?/, "")
/* 150  */         } : !1

/* fotorama.js */

/* 151  */     }
/* 152  */ 
/* 153  */     function A(a, b, c) {
/* 154  */         var e, f, g = a.video;
/* 155  */         return "youtube" === g.type ? (f = x() + "img.youtube.com/vi/" + g.id + "/default.jpg", e = f.replace(/\/default.jpg$/, "/hqdefault.jpg"), a.thumbsReady = !0) : "vimeo" === g.type ? d.ajax({
/* 156  */             url: x() + "vimeo.com/api/v2/video/" + g.id + ".json",
/* 157  */             dataType: "jsonp",
/* 158  */             success: function(d) {
/* 159  */                 a.thumbsReady = !0, B(b, {
/* 160  */                     img: d[0].thumbnail_large,
/* 161  */                     thumb: d[0].thumbnail_small
/* 162  */                 }, a.i, c)
/* 163  */             }
/* 164  */         }) : a.thumbsReady = !0, {
/* 165  */             img: e,
/* 166  */             thumb: f
/* 167  */         }
/* 168  */     }
/* 169  */ 
/* 170  */     function B(a, b, c, e) {
/* 171  */         for (var f = 0, g = a.length; g > f; f++) {
/* 172  */             var h = a[f];
/* 173  */             if (h.i === c && h.thumbsReady) {
/* 174  */                 var i = {
/* 175  */                     videoReady: !0
/* 176  */                 };
/* 177  */                 i[Uc] = i[Wc] = i[Vc] = !1, e.splice(f, 1, d.extend({}, h, i, b));
/* 178  */                 break
/* 179  */             }
/* 180  */         }
/* 181  */     }
/* 182  */ 
/* 183  */     function C(a) {
/* 184  */         function b(a, b, e) {
/* 185  */             var f = a.children("img").eq(0),
/* 186  */                 g = a.attr("href"),
/* 187  */                 h = a.attr("src"),
/* 188  */                 i = f.attr("src"),
/* 189  */                 j = b.video,
/* 190  */                 k = e ? z(g, j === !0) : !1;
/* 191  */             k ? g = !1 : k = j, c(a, f, d.extend(b, {
/* 192  */                 video: k,
/* 193  */                 img: b.img || g || h || i,
/* 194  */                 thumb: b.thumb || i || h || g
/* 195  */             }))
/* 196  */         }
/* 197  */ 
/* 198  */         function c(a, b, c) {
/* 199  */             var e = c.thumb && c.img !== c.thumb,
/* 200  */                 f = m(c.width || a.attr("width")),

/* fotorama.js */

/* 201  */                 g = m(c.height || a.attr("height"));
/* 202  */             d.extend(c, {
/* 203  */                 width: f,
/* 204  */                 height: g,
/* 205  */                 thumbratio: R(c.thumbratio || m(c.thumbwidth || b && b.attr("width") || e || f) / m(c.thumbheight || b && b.attr("height") || e || g))
/* 206  */             })
/* 207  */         }
/* 208  */         var e = [];
/* 209  */         return a.children().each(function() {
/* 210  */             var a = d(this),
/* 211  */                 f = Q(d.extend(a.data(), {
/* 212  */                     id: a.attr("id")
/* 213  */                 }));
/* 214  */             if (a.is("a, img")) b(a, f, !0);
/* 215  */             else {
/* 216  */                 if (a.is(":empty")) return;
/* 217  */                 c(a, null, d.extend(f, {
/* 218  */                     html: this,
/* 219  */                     _html: a.html()
/* 220  */                 }))
/* 221  */             }
/* 222  */             e.push(f)
/* 223  */         }), e
/* 224  */     }
/* 225  */ 
/* 226  */     function D(a) {
/* 227  */         return 0 === a.offsetWidth && 0 === a.offsetHeight
/* 228  */     }
/* 229  */ 
/* 230  */     function E(a) {
/* 231  */         return !d.contains(b.documentElement, a)
/* 232  */     }
/* 233  */ 
/* 234  */     function F(a, b, c) {
/* 235  */         a() ? b() : setTimeout(function() {
/* 236  */             F(a, b)
/* 237  */         }, c || 100)
/* 238  */     }
/* 239  */ 
/* 240  */     function G(a) {
/* 241  */         c.replace(c.protocol + "//" + c.host + c.pathname.replace(/^\/?/, "/") + c.search + "#" + a)
/* 242  */     }
/* 243  */ 
/* 244  */     function H(a, b, c) {
/* 245  */         var d = a.data(),
/* 246  */             e = d.measures;
/* 247  */         if (e && (!d.l || d.l.W !== e.width || d.l.H !== e.height || d.l.r !== e.ratio || d.l.w !== b.w || d.l.h !== b.h || d.l.m !== c)) {
/* 248  */             var f = e.width,
/* 249  */                 g = e.height,
/* 250  */                 i = b.w / b.h,

/* fotorama.js */

/* 251  */                 j = e.ratio >= i,
/* 252  */                 k = "scaledown" === c,
/* 253  */                 l = "contain" === c,
/* 254  */                 m = "cover" === c;
/* 255  */             j && (k || l) || !j && m ? (f = h(b.w, 0, k ? f : 1 / 0), g = f / e.ratio) : (j && m || !j && (k || l)) && (g = h(b.h, 0, k ? g : 1 / 0), f = g * e.ratio), a.css({
/* 256  */                 width: Math.ceil(f),
/* 257  */                 height: Math.ceil(g),
/* 258  */                 marginLeft: Math.floor(-f / 2),
/* 259  */                 marginTop: Math.floor(-g / 2)
/* 260  */             }), d.l = {
/* 261  */                 W: e.width,
/* 262  */                 H: e.height,
/* 263  */                 r: e.ratio,
/* 264  */                 w: b.w,
/* 265  */                 h: b.h,
/* 266  */                 m: c
/* 267  */             }
/* 268  */         }
/* 269  */         return !0
/* 270  */     }
/* 271  */ 
/* 272  */     function I(a, b) {
/* 273  */         var c = a[0];
/* 274  */         c.styleSheet ? c.styleSheet.cssText = b : a.html(b)
/* 275  */     }
/* 276  */ 
/* 277  */     function J(a, b, c) {
/* 278  */         return b === c ? !1 : b >= a ? "left" : a >= c ? "right" : "left right"
/* 279  */     }
/* 280  */ 
/* 281  */     function K(a, b, c, d) {
/* 282  */         if (!c) return !1;
/* 283  */         if (!isNaN(a)) return a - (d ? 0 : 1);
/* 284  */         for (var e, f = 0, g = b.length; g > f; f++) {
/* 285  */             var h = b[f];
/* 286  */             if (h.id === a) {
/* 287  */                 e = f;
/* 288  */                 break
/* 289  */             }
/* 290  */         }
/* 291  */         return e
/* 292  */     }
/* 293  */ 
/* 294  */     function L(a, b, c) {
/* 295  */         c = c || {}, a.each(function() {
/* 296  */             var a, e = d(this),
/* 297  */                 f = e.data();
/* 298  */             f.clickOn || (f.clickOn = !0, d.extend(X(e, {
/* 299  */                 onStart: function(b) {
/* 300  */                     a = b, (c.onStart || g).call(this, b)

/* fotorama.js */

/* 301  */                 },
/* 302  */                 onMove: c.onMove || g,
/* 303  */                 onTouchEnd: c.onTouchEnd || g,
/* 304  */                 onEnd: function(c) {
/* 305  */                     c.moved || b.call(this, a)
/* 306  */                 }
/* 307  */             }), {
/* 308  */                 noMove: !0
/* 309  */             }))
/* 310  */         })
/* 311  */     }
/* 312  */ 
/* 313  */     function M(a, b) {
/* 314  */         return '<div class="' + a + '">' + (b || "") + "</div>"
/* 315  */     }
/* 316  */ 
/* 317  */     function N(a) {
/* 318  */         for (var b = a.length; b;) {
/* 319  */             var c = Math.floor(Math.random() * b--),
/* 320  */                 d = a[b];
/* 321  */             a[b] = a[c], a[c] = d
/* 322  */         }
/* 323  */         return a
/* 324  */     }
/* 325  */ 
/* 326  */     function O(a) {
/* 327  */         return "[object Array]" == Object.prototype.toString.call(a) && d.map(a, function(a) {
/* 328  */             return d.extend({}, a)
/* 329  */         })
/* 330  */     }
/* 331  */ 
/* 332  */     function P(a, b) {
/* 333  */         Ac.scrollLeft(a).scrollTop(b)
/* 334  */     }
/* 335  */ 
/* 336  */     function Q(a) {
/* 337  */         if (a) {
/* 338  */             var b = {};
/* 339  */             return d.each(a, function(a, c) {
/* 340  */                 b[a.toLowerCase()] = c
/* 341  */             }), b
/* 342  */         }
/* 343  */     }
/* 344  */ 
/* 345  */     function R(a) {
/* 346  */         if (a) {
/* 347  */             var b = +a;
/* 348  */             return isNaN(b) ? (b = a.split("/"), +b[0] / +b[1] || e) : b
/* 349  */         }
/* 350  */     }

/* fotorama.js */

/* 351  */ 
/* 352  */     function S(a, b) {
/* 353  */         a.preventDefault(), b && a.stopPropagation()
/* 354  */     }
/* 355  */ 
/* 356  */     function T(a) {
/* 357  */         return a ? ">" : "<"
/* 358  */     }
/* 359  */ 
/* 360  */     function U(a, b) {
/* 361  */         var c = a.data(),
/* 362  */             e = Math.round(b.pos),
/* 363  */             f = function() {
/* 364  */                 c.sliding = !1, (b.onEnd || g)()
/* 365  */             };
/* 366  */         "undefined" != typeof b.overPos && b.overPos !== b.pos && (e = b.overPos, f = function() {
/* 367  */             U(a, d.extend({}, b, {
/* 368  */                 overPos: b.pos,
/* 369  */                 time: Math.max(Nc, b.time / 2)
/* 370  */             }))
/* 371  */         });
/* 372  */         var h = d.extend(k(e, b._001), b.width && {
/* 373  */             width: b.width
/* 374  */         });
/* 375  */         c.sliding = !0, Ec ? (a.css(d.extend(l(b.time), h)), b.time > 10 ? t(a, "transform", f, b.time) : f()) : a.stop().animate(h, b.time, Xc, f)
/* 376  */     }
/* 377  */ 
/* 378  */     function V(a, b, c, e, f, h) {
/* 379  */         var i = "undefined" != typeof h;
/* 380  */         if (i || (f.push(arguments), Array.prototype.push.call(arguments, f.length), !(f.length > 1))) {
/* 381  */             a = a || d(a), b = b || d(b);
/* 382  */             var j = a[0],
/* 383  */                 k = b[0],
/* 384  */                 l = "crossfade" === e.method,
/* 385  */                 m = function() {
/* 386  */                     if (!m.done) {
/* 387  */                         m.done = !0;
/* 388  */                         var a = (i || f.shift()) && f.shift();
/* 389  */                         a && V.apply(this, a), (e.onEnd || g)( !! a)
/* 390  */                     }
/* 391  */                 }, n = e.time / (h || 1);
/* 392  */             c.removeClass(Kb + " " + Jb), a.stop().addClass(Kb), b.stop().addClass(Jb), l && k && a.fadeTo(0, 0), a.fadeTo(l ? n : 0, 1, l && m), b.fadeTo(n, 0, m), j && l || k || m()
/* 393  */         }
/* 394  */     }
/* 395  */ 
/* 396  */     function W(a) {
/* 397  */         var b = (a.touches || [])[0] || a;
/* 398  */         a._x = b.pageX, a._y = b.clientY, a._now = d.now()
/* 399  */     }
/* 400  */ 

/* fotorama.js */

/* 401  */     function X(c, e) {
/* 402  */         function f(a) {
/* 403  */             return n = d(a.target), v.checked = q = r = t = !1, l || v.flow || a.touches && a.touches.length > 1 || a.which > 1 || wc && wc.type !== a.type && yc || (q = e.select && n.is(e.select, u)) ? q : (p = "touchstart" === a.type, r = n.is("a, a *", u), o = v.control, s = v.noMove || v.noSwipe || o ? 16 : v.snap ? 0 : 4, W(a), m = wc = a, xc = a.type.replace(/down|start/, "move").replace(/Down/, "Move"), (e.onStart || g).call(u, a, {
/* 404  */                 control: o,
/* 405  */                 $target: n
/* 406  */             }), l = v.flow = !0, void((!p || v.go) && S(a)))
/* 407  */         }
/* 408  */ 
/* 409  */         function h(a) {
/* 410  */             if (a.touches && a.touches.length > 1 || Kc && !a.isPrimary || xc !== a.type || !l) return l && i(), void(e.onTouchEnd || g)();
/* 411  */             W(a);
/* 412  */             var b = Math.abs(a._x - m._x),
/* 413  */                 c = Math.abs(a._y - m._y),
/* 414  */                 d = b - c,
/* 415  */                 f = (v.go || v.x || d >= 0) && !v.noSwipe,
/* 416  */                 h = 0 > d;
/* 417  */             p && !v.checked ? (l = f) && S(a) : (S(a), (e.onMove || g).call(u, a, {
/* 418  */                 touch: p
/* 419  */             })), !t && Math.sqrt(Math.pow(b, 2) + Math.pow(c, 2)) > s && (t = !0), v.checked = v.checked || f || h
/* 420  */         }
/* 421  */ 
/* 422  */         function i(a) {
/* 423  */             (e.onTouchEnd || g)();
/* 424  */             var b = l;
/* 425  */             v.control = l = !1, b && (v.flow = !1), !b || r && !v.checked || (a && S(a), yc = !0, clearTimeout(zc), zc = setTimeout(function() {
/* 426  */                 yc = !1
/* 427  */             }, 1e3), (e.onEnd || g).call(u, {
/* 428  */                 moved: t,
/* 429  */                 $target: n,
/* 430  */                 control: o,
/* 431  */                 touch: p,
/* 432  */                 startEvent: m,
/* 433  */                 aborted: !a || "MSPointerCancel" === a.type
/* 434  */             }))
/* 435  */         }
/* 436  */ 
/* 437  */         function j() {
/* 438  */             v.flow || setTimeout(function() {
/* 439  */                 v.flow = !0
/* 440  */             }, 10)
/* 441  */         }
/* 442  */ 
/* 443  */         function k() {
/* 444  */             v.flow && setTimeout(function() {
/* 445  */                 v.flow = !1
/* 446  */             }, Mc)
/* 447  */         }
/* 448  */         var l, m, n, o, p, q, r, s, t, u = c[0],
/* 449  */             v = {};
/* 450  */         return Kc ? (u[Jc]("MSPointerDown", f, !1), b[Jc]("MSPointerMove", h, !1), b[Jc]("MSPointerCancel", i, !1), b[Jc]("MSPointerUp", i, !1)) : (u[Jc] && (u[Jc]("touchstart", f, !1), u[Jc]("touchmove", h, !1), u[Jc]("touchend", i, !1), b[Jc]("touchstart", j, !1), b[Jc]("touchend", k, !1), b[Jc]("touchcancel", k, !1), a[Jc]("scroll", k, !1)), c.on("mousedown", f), Bc.on("mousemove", h).on("mouseup", i)), c.on("click", "a", function(a) {

/* fotorama.js */

/* 451  */             v.checked && S(a)
/* 452  */         }), v
/* 453  */     }
/* 454  */ 
/* 455  */     function Y(a, b) {
/* 456  */         function c(c, d) {
/* 457  */             A = !0, j = l = c._x, q = c._now, p = [
/* 458  */                 [q, j]
/* 459  */             ], m = n = D.noMove || d ? 0 : u(a, (b.getPos || g)(), b._001), (b.onStart || g).call(B, c)
/* 460  */         }
/* 461  */ 
/* 462  */         function e(a, b) {
/* 463  */             s = D.min, t = D.max, v = D.snap, x = a.altKey, A = z = !1, y = b.control, y || C.sliding || c(a)
/* 464  */         }
/* 465  */ 
/* 466  */         function f(d, e) {
/* 467  */             D.noSwipe || (A || c(d), l = d._x, p.push([d._now, l]), n = m - (j - l), o = J(n, s, t), s >= n ? n = w(n, s) : n >= t && (n = w(n, t)), D.noMove || (a.css(k(n, b._001)), z || (z = !0, e.touch || Kc || a.addClass(Zb)), (b.onMove || g).call(B, d, {
/* 468  */                 pos: n,
/* 469  */                 edge: o
/* 470  */             })))
/* 471  */         }
/* 472  */ 
/* 473  */         function i(e) {
/* 474  */             if (!D.noSwipe || !e.moved) {
/* 475  */                 A || c(e.startEvent, !0), e.touch || Kc || a.removeClass(Zb), r = d.now();
/* 476  */                 for (var f, i, j, k, o, q, u, w, y, z = r - Mc, C = null, E = Nc, F = b.friction, G = p.length - 1; G >= 0; G--) {
/* 477  */                     if (f = p[G][0], i = Math.abs(f - z), null === C || j > i) C = f, k = p[G][1];
/* 478  */                     else if (C === z || i > j) break;
/* 479  */                     j = i
/* 480  */                 }
/* 481  */                 u = h(n, s, t);
/* 482  */                 var H = k - l,
/* 483  */                     I = H >= 0,
/* 484  */                     J = r - C,
/* 485  */                     K = J > Mc,
/* 486  */                     L = !K && n !== m && u === n;
/* 487  */                 v && (u = h(Math[L ? I ? "floor" : "ceil" : "round"](n / v) * v, s, t), s = t = u), L && (v || u === n) && (y = -(H / J), E *= h(Math.abs(y), b.timeLow, b.timeHigh), o = Math.round(n + y * E / F), v || (u = o), (!I && o > t || I && s > o) && (q = I ? s : t, w = o - q, v || (u = q), w = h(u + .03 * w, q - 50, q + 50), E = Math.abs((n - w) / (y / F)))), E *= x ? 10 : 1, (b.onEnd || g).call(B, d.extend(e, {
/* 488  */                     moved: e.moved || K && v,
/* 489  */                     pos: n,
/* 490  */                     newPos: u,
/* 491  */                     overPos: w,
/* 492  */                     time: E
/* 493  */                 }))
/* 494  */             }
/* 495  */         }
/* 496  */         var j, l, m, n, o, p, q, r, s, t, v, x, y, z, A, B = a[0],
/* 497  */             C = a.data(),
/* 498  */             D = {};
/* 499  */         return D = d.extend(X(b.$wrap, {
/* 500  */             onStart: e,

/* fotorama.js */

/* 501  */             onMove: f,
/* 502  */             onTouchEnd: b.onTouchEnd,
/* 503  */             onEnd: i,
/* 504  */             select: b.select
/* 505  */         }), D)
/* 506  */     }
/* 507  */ 
/* 508  */     function Z(a, b) {
/* 509  */         var c, e, f, h = a[0],
/* 510  */             i = {
/* 511  */                 prevent: {}
/* 512  */             };
/* 513  */         return h[Jc] && h[Jc](Lc, function(a) {
/* 514  */             var h = a.wheelDeltaY || -1 * a.deltaY || 0,
/* 515  */                 j = a.wheelDeltaX || -1 * a.deltaX || 0,
/* 516  */                 k = Math.abs(j) > Math.abs(h),
/* 517  */                 l = T(0 > j),
/* 518  */                 m = e === l,
/* 519  */                 n = d.now(),
/* 520  */                 o = Mc > n - f;
/* 521  */             e = l, f = n, k && i.ok && (!i.prevent[l] || c) && (S(a, !0), c && m && o || (b.shift && (c = !0, clearTimeout(i.t), i.t = setTimeout(function() {
/* 522  */                 c = !1
/* 523  */             }, Oc)), (b.onEnd || g)(a, b.shift ? l : j)))
/* 524  */         }, !1), i
/* 525  */     }
/* 526  */ 
/* 527  */     function $() {
/* 528  */         d.each(d.Fotorama.instances, function(a, b) {
/* 529  */             b.index = a
/* 530  */         })
/* 531  */     }
/* 532  */ 
/* 533  */     function _(a) {
/* 534  */         d.Fotorama.instances.push(a), $()
/* 535  */     }
/* 536  */ 
/* 537  */     function ab(a) {
/* 538  */         d.Fotorama.instances.splice(a.index, 1), $()
/* 539  */     }
/* 540  */     var bb = "fotorama",
/* 541  */         cb = "fullscreen",
/* 542  */         db = bb + "__wrap",
/* 543  */         eb = db + "--css2",
/* 544  */         fb = db + "--css3",
/* 545  */         gb = db + "--video",
/* 546  */         hb = db + "--fade",
/* 547  */         ib = db + "--slide",
/* 548  */         jb = db + "--no-controls",
/* 549  */         kb = db + "--no-shadows",
/* 550  */         lb = db + "--pan-y",

/* fotorama.js */

/* 551  */         mb = db + "--rtl",
/* 552  */         nb = db + "--only-active",
/* 553  */         ob = db + "--no-captions",
/* 554  */         pb = db + "--toggle-arrows",
/* 555  */         qb = bb + "__stage",
/* 556  */         rb = qb + "__frame",
/* 557  */         sb = rb + "--video",
/* 558  */         tb = qb + "__shaft",
/* 559  */         ub = bb + "__grab",
/* 560  */         vb = bb + "__pointer",
/* 561  */         wb = bb + "__arr",
/* 562  */         xb = wb + "--disabled",
/* 563  */         yb = wb + "--prev",
/* 564  */         zb = wb + "--next",
/* 565  */         Ab = bb + "__nav",
/* 566  */         Bb = Ab + "-wrap",
/* 567  */         Cb = Ab + "__shaft",
/* 568  */         Db = Ab + "--dots",
/* 569  */         Eb = Ab + "--thumbs",
/* 570  */         Fb = Ab + "__frame",
/* 571  */         Gb = Fb + "--dot",
/* 572  */         Hb = Fb + "--thumb",
/* 573  */         Ib = bb + "__fade",
/* 574  */         Jb = Ib + "-front",
/* 575  */         Kb = Ib + "-rear",
/* 576  */         Lb = bb + "__shadow",
/* 577  */         Mb = Lb + "s",
/* 578  */         Nb = Mb + "--left",
/* 579  */         Ob = Mb + "--right",
/* 580  */         Pb = bb + "__active",
/* 581  */         Qb = bb + "__select",
/* 582  */         Rb = bb + "--hidden",
/* 583  */         Sb = bb + "--fullscreen",
/* 584  */         Tb = bb + "__fullscreen-icon",
/* 585  */         Ub = bb + "__error",
/* 586  */         Vb = bb + "__loading",
/* 587  */         Wb = bb + "__loaded",
/* 588  */         Xb = Wb + "--full",
/* 589  */         Yb = Wb + "--img",
/* 590  */         Zb = bb + "__grabbing",
/* 591  */         $b = bb + "__img",
/* 592  */         _b = $b + "--full",
/* 593  */         ac = bb + "__dot",
/* 594  */         bc = bb + "__thumb",
/* 595  */         cc = bc + "-border",
/* 596  */         dc = bb + "__html",
/* 597  */         ec = bb + "__video",
/* 598  */         fc = ec + "-play",
/* 599  */         gc = ec + "-close",
/* 600  */         hc = bb + "__caption",

/* fotorama.js */

/* 601  */         ic = bb + "__caption__wrap",
/* 602  */         jc = bb + "__spinner",
/* 603  */         kc = d && d.fn.jquery.split(".");
/* 604  */     if (!kc || kc[0] < 1 || 1 == kc[0] && kc[1] < 8) throw "Fotorama requires jQuery 1.8 or later and will not run without it.";
/* 605  */     var lc = {}, mc = function(a, b, c) {
/* 606  */             function d(a) {
/* 607  */                 r.cssText = a
/* 608  */             }
/* 609  */ 
/* 610  */             function e(a, b) {
/* 611  */                 return typeof a === b
/* 612  */             }
/* 613  */ 
/* 614  */             function f(a, b) {
/* 615  */                 return !!~("" + a).indexOf(b)
/* 616  */             }
/* 617  */ 
/* 618  */             function g(a, b) {
/* 619  */                 for (var d in a) {
/* 620  */                     var e = a[d];
/* 621  */                     if (!f(e, "-") && r[e] !== c) return "pfx" == b ? e : !0
/* 622  */                 }
/* 623  */                 return !1
/* 624  */             }
/* 625  */ 
/* 626  */             function h(a, b, d) {
/* 627  */                 for (var f in a) {
/* 628  */                     var g = b[a[f]];
/* 629  */                     if (g !== c) return d === !1 ? a[f] : e(g, "function") ? g.bind(d || b) : g
/* 630  */                 }
/* 631  */                 return !1
/* 632  */             }
/* 633  */ 
/* 634  */             function i(a, b, c) {
/* 635  */                 var d = a.charAt(0).toUpperCase() + a.slice(1),
/* 636  */                     f = (a + " " + u.join(d + " ") + d).split(" ");
/* 637  */                 return e(b, "string") || e(b, "undefined") ? g(f, b) : (f = (a + " " + v.join(d + " ") + d).split(" "), h(f, b, c))
/* 638  */             }
/* 639  */             var j, k, l, m = "2.6.2",
/* 640  */                 n = {}, o = b.documentElement,
/* 641  */                 p = "modernizr",
/* 642  */                 q = b.createElement(p),
/* 643  */                 r = q.style,
/* 644  */                 s = ({}.toString, " -webkit- -moz- -o- -ms- ".split(" ")),
/* 645  */                 t = "Webkit Moz O ms",
/* 646  */                 u = t.split(" "),
/* 647  */                 v = t.toLowerCase().split(" "),
/* 648  */                 w = {}, x = [],
/* 649  */                 y = x.slice,
/* 650  */                 z = function(a, c, d, e) {

/* fotorama.js */

/* 651  */                     var f, g, h, i, j = b.createElement("div"),
/* 652  */                         k = b.body,
/* 653  */                         l = k || b.createElement("body");
/* 654  */                     if (parseInt(d, 10))
/* 655  */                         for (; d--;) h = b.createElement("div"), h.id = e ? e[d] : p + (d + 1), j.appendChild(h);
/* 656  */                     return f = ["&#173;", '<style id="s', p, '">', a, "</style>"].join(""), j.id = p, (k ? j : l).innerHTML += f, l.appendChild(j), k || (l.style.background = "", l.style.overflow = "hidden", i = o.style.overflow, o.style.overflow = "hidden", o.appendChild(l)), g = c(j, a), k ? j.parentNode.removeChild(j) : (l.parentNode.removeChild(l), o.style.overflow = i), !! g
/* 657  */                 }, A = {}.hasOwnProperty;
/* 658  */             l = e(A, "undefined") || e(A.call, "undefined") ? function(a, b) {
/* 659  */                 return b in a && e(a.constructor.prototype[b], "undefined")
/* 660  */             } : function(a, b) {
/* 661  */                 return A.call(a, b)
/* 662  */             }, Function.prototype.bind || (Function.prototype.bind = function(a) {
/* 663  */                 var b = this;
/* 664  */                 if ("function" != typeof b) throw new TypeError;
/* 665  */                 var c = y.call(arguments, 1),
/* 666  */                     d = function() {
/* 667  */                         if (this instanceof d) {
/* 668  */                             var e = function() {};
/* 669  */                             e.prototype = b.prototype;
/* 670  */                             var f = new e,
/* 671  */                                 g = b.apply(f, c.concat(y.call(arguments)));
/* 672  */                             return Object(g) === g ? g : f
/* 673  */                         }
/* 674  */                         return b.apply(a, c.concat(y.call(arguments)))
/* 675  */                     };
/* 676  */                 return d
/* 677  */             }), w.csstransforms3d = function() {
/* 678  */                 var a = !! i("perspective");
/* 679  */                 return a
/* 680  */             };
/* 681  */             for (var B in w) l(w, B) && (k = B.toLowerCase(), n[k] = w[B](), x.push((n[k] ? "" : "no-") + k));
/* 682  */             return n.addTest = function(a, b) {
/* 683  */                 if ("object" == typeof a)
/* 684  */                     for (var d in a) l(a, d) && n.addTest(d, a[d]);
/* 685  */                 else {
/* 686  */                     if (a = a.toLowerCase(), n[a] !== c) return n;
/* 687  */                     b = "function" == typeof b ? b() : b, "undefined" != typeof enableClasses && enableClasses && (o.className += " " + (b ? "" : "no-") + a), n[a] = b
/* 688  */                 }
/* 689  */                 return n
/* 690  */             }, d(""), q = j = null, n._version = m, n._prefixes = s, n._domPrefixes = v, n._cssomPrefixes = u, n.testProp = function(a) {
/* 691  */                 return g([a])
/* 692  */             }, n.testAllProps = i, n.testStyles = z, n.prefixed = function(a, b, c) {
/* 693  */                 return b ? i(a, b, c) : i(a, "pfx")
/* 694  */             }, n
/* 695  */         }(a, b),
/* 696  */         nc = {
/* 697  */             ok: !1,
/* 698  */             is: function() {
/* 699  */                 return !1
/* 700  */             },

/* fotorama.js */

/* 701  */             request: function() {},
/* 702  */             cancel: function() {},
/* 703  */             event: "",
/* 704  */             prefix: ""
/* 705  */         }, oc = "webkit moz o ms khtml".split(" ");
/* 706  */     if ("undefined" != typeof b.cancelFullScreen) nc.ok = !0;
/* 707  */     else
/* 708  */         for (var pc = 0, qc = oc.length; qc > pc; pc++)
/* 709  */             if (nc.prefix = oc[pc], "undefined" != typeof b[nc.prefix + "CancelFullScreen"]) {
/* 710  */                 nc.ok = !0;
/* 711  */                 break
/* 712  */             } nc.ok && (nc.event = nc.prefix + "fullscreenchange", nc.is = function() {
/* 713  */         switch (this.prefix) {
/* 714  */             case "":
/* 715  */                 return b.fullScreen;
/* 716  */             case "webkit":
/* 717  */                 return b.webkitIsFullScreen;
/* 718  */             default:
/* 719  */                 return b[this.prefix + "FullScreen"]
/* 720  */         }
/* 721  */     }, nc.request = function(a) {
/* 722  */         return "" === this.prefix ? a.requestFullScreen() : a[this.prefix + "RequestFullScreen"]()
/* 723  */     }, nc.cancel = function() {
/* 724  */         return "" === this.prefix ? b.cancelFullScreen() : b[this.prefix + "CancelFullScreen"]()
/* 725  */     });
/* 726  */     var rc, sc = {
/* 727  */             lines: 12,
/* 728  */             length: 5,
/* 729  */             width: 2,
/* 730  */             radius: 7,
/* 731  */             corners: 1,
/* 732  */             rotate: 15,
/* 733  */             color: "rgba(128, 128, 128, .75)",
/* 734  */             hwaccel: !0
/* 735  */         }, tc = {
/* 736  */             top: "auto",
/* 737  */             left: "auto",
/* 738  */             className: ""
/* 739  */         };
/* 740  */     ! function(a, b) {
/* 741  */         rc = b()
/* 742  */     }(this, function() {
/* 743  */         function a(a, c) {
/* 744  */             var d, e = b.createElement(a || "div");
/* 745  */             for (d in c) e[d] = c[d];
/* 746  */             return e
/* 747  */         }
/* 748  */ 
/* 749  */         function c(a) {
/* 750  */             for (var b = 1, c = arguments.length; c > b; b++) a.appendChild(arguments[b]);

/* fotorama.js */

/* 751  */             return a
/* 752  */         }
/* 753  */ 
/* 754  */         function d(a, b, c, d) {
/* 755  */             var e = ["opacity", b, ~~ (100 * a), c, d].join("-"),
/* 756  */                 f = .01 + c / d * 100,
/* 757  */                 g = Math.max(1 - (1 - a) / b * (100 - f), a),
/* 758  */                 h = m.substring(0, m.indexOf("Animation")).toLowerCase(),
/* 759  */                 i = h && "-" + h + "-" || "";
/* 760  */             return o[e] || (p.insertRule("@" + i + "keyframes " + e + "{0%{opacity:" + g + "}" + f + "%{opacity:" + a + "}" + (f + .01) + "%{opacity:1}" + (f + b) % 100 + "%{opacity:" + a + "}100%{opacity:" + g + "}}", p.cssRules.length), o[e] = 1), e
/* 761  */         }
/* 762  */ 
/* 763  */         function f(a, b) {
/* 764  */             var c, d, f = a.style;
/* 765  */             for (b = b.charAt(0).toUpperCase() + b.slice(1), d = 0; d < n.length; d++)
/* 766  */                 if (c = n[d] + b, f[c] !== e) return c;
/* 767  */             return f[b] !== e ? b : void 0
/* 768  */         }
/* 769  */ 
/* 770  */         function g(a, b) {
/* 771  */             for (var c in b) a.style[f(a, c) || c] = b[c];
/* 772  */             return a
/* 773  */         }
/* 774  */ 
/* 775  */         function h(a) {
/* 776  */             for (var b = 1; b < arguments.length; b++) {
/* 777  */                 var c = arguments[b];
/* 778  */                 for (var d in c) a[d] === e && (a[d] = c[d])
/* 779  */             }
/* 780  */             return a
/* 781  */         }
/* 782  */ 
/* 783  */         function i(a) {
/* 784  */             for (var b = {
/* 785  */                 x: a.offsetLeft,
/* 786  */                 y: a.offsetTop
/* 787  */             }; a = a.offsetParent;) b.x += a.offsetLeft, b.y += a.offsetTop;
/* 788  */             return b
/* 789  */         }
/* 790  */ 
/* 791  */         function j(a, b) {
/* 792  */             return "string" == typeof a ? a : a[b % a.length]
/* 793  */         }
/* 794  */ 
/* 795  */         function k(a) {
/* 796  */             return "undefined" == typeof this ? new k(a) : void(this.opts = h(a || {}, k.defaults, q))
/* 797  */         }
/* 798  */ 
/* 799  */         function l() {
/* 800  */             function b(b, c) {

/* fotorama.js */

/* 801  */                 return a("<" + b + ' xmlns="urn:schemas-microsoft.com:vml" class="spin-vml">', c)
/* 802  */             }
/* 803  */             p.addRule(".spin-vml", "behavior:url(#default#VML)"), k.prototype.lines = function(a, d) {
/* 804  */                 function e() {
/* 805  */                     return g(b("group", {
/* 806  */                         coordsize: k + " " + k,
/* 807  */                         coordorigin: -i + " " + -i
/* 808  */                     }), {
/* 809  */                         width: k,
/* 810  */                         height: k
/* 811  */                     })
/* 812  */                 }
/* 813  */ 
/* 814  */                 function f(a, f, h) {
/* 815  */                     c(m, c(g(e(), {
/* 816  */                         rotation: 360 / d.lines * a + "deg",
/* 817  */                         left: ~~f
/* 818  */                     }), c(g(b("roundrect", {
/* 819  */                         arcsize: d.corners
/* 820  */                     }), {
/* 821  */                         width: i,
/* 822  */                         height: d.width,
/* 823  */                         left: d.radius,
/* 824  */                         top: -d.width >> 1,
/* 825  */                         filter: h
/* 826  */                     }), b("fill", {
/* 827  */                         color: j(d.color, a),
/* 828  */                         opacity: d.opacity
/* 829  */                     }), b("stroke", {
/* 830  */                         opacity: 0
/* 831  */                     }))))
/* 832  */                 }
/* 833  */                 var h, i = d.length + d.width,
/* 834  */                     k = 2 * i,
/* 835  */                     l = 2 * -(d.width + d.length) + "px",
/* 836  */                     m = g(e(), {
/* 837  */                         position: "absolute",
/* 838  */                         top: l,
/* 839  */                         left: l
/* 840  */                     });
/* 841  */                 if (d.shadow)
/* 842  */                     for (h = 1; h <= d.lines; h++) f(h, -2, "progid:DXImageTransform.Microsoft.Blur(pixelradius=2,makeshadow=1,shadowopacity=.3)");
/* 843  */                 for (h = 1; h <= d.lines; h++) f(h);
/* 844  */                 return c(a, m)
/* 845  */             }, k.prototype.opacity = function(a, b, c, d) {
/* 846  */                 var e = a.firstChild;
/* 847  */                 d = d.shadow && d.lines || 0, e && b + d < e.childNodes.length && (e = e.childNodes[b + d], e = e && e.firstChild, e = e && e.firstChild, e && (e.opacity = c))
/* 848  */             }
/* 849  */         }
/* 850  */         var m, n = ["webkit", "Moz", "ms", "O"],

/* fotorama.js */

/* 851  */             o = {}, p = function() {
/* 852  */                 var d = a("style", {
/* 853  */                     type: "text/css"
/* 854  */                 });
/* 855  */                 return c(b.getElementsByTagName("head")[0], d), d.sheet || d.styleSheet
/* 856  */             }(),
/* 857  */             q = {
/* 858  */                 lines: 12,
/* 859  */                 length: 7,
/* 860  */                 width: 5,
/* 861  */                 radius: 10,
/* 862  */                 rotate: 0,
/* 863  */                 corners: 1,
/* 864  */                 color: "#000",
/* 865  */                 direction: 1,
/* 866  */                 speed: 1,
/* 867  */                 trail: 100,
/* 868  */                 opacity: .25,
/* 869  */                 fps: 20,
/* 870  */                 zIndex: 2e9,
/* 871  */                 className: "spinner",
/* 872  */                 top: "auto",
/* 873  */                 left: "auto",
/* 874  */                 position: "relative"
/* 875  */             };
/* 876  */         k.defaults = {}, h(k.prototype, {
/* 877  */             spin: function(b) {
/* 878  */                 this.stop();
/* 879  */                 var c, d, e = this,
/* 880  */                     f = e.opts,
/* 881  */                     h = e.el = g(a(0, {
/* 882  */                         className: f.className
/* 883  */                     }), {
/* 884  */                         position: f.position,
/* 885  */                         width: 0,
/* 886  */                         zIndex: f.zIndex
/* 887  */                     }),
/* 888  */                     j = f.radius + f.length + f.width;
/* 889  */                 if (b && (b.insertBefore(h, b.firstChild || null), d = i(b), c = i(h), g(h, {
/* 890  */                     left: ("auto" == f.left ? d.x - c.x + (b.offsetWidth >> 1) : parseInt(f.left, 10) + j) + "px",
/* 891  */                     top: ("auto" == f.top ? d.y - c.y + (b.offsetHeight >> 1) : parseInt(f.top, 10) + j) + "px"
/* 892  */                 })), h.setAttribute("role", "progressbar"), e.lines(h, e.opts), !m) {
/* 893  */                     var k, l = 0,
/* 894  */                         n = (f.lines - 1) * (1 - f.direction) / 2,
/* 895  */                         o = f.fps,
/* 896  */                         p = o / f.speed,
/* 897  */                         q = (1 - f.opacity) / (p * f.trail / 100),
/* 898  */                         r = p / f.lines;
/* 899  */                     ! function s() {
/* 900  */                         l++;

/* fotorama.js */

/* 901  */                         for (var a = 0; a < f.lines; a++) k = Math.max(1 - (l + (f.lines - a) * r) % p * q, f.opacity), e.opacity(h, a * f.direction + n, k, f);
/* 902  */                         e.timeout = e.el && setTimeout(s, ~~ (1e3 / o))
/* 903  */                     }()
/* 904  */                 }
/* 905  */                 return e
/* 906  */             },
/* 907  */             stop: function() {
/* 908  */                 var a = this.el;
/* 909  */                 return a && (clearTimeout(this.timeout), a.parentNode && a.parentNode.removeChild(a), this.el = e), this
/* 910  */             },
/* 911  */             lines: function(b, e) {
/* 912  */                 function f(b, c) {
/* 913  */                     return g(a(), {
/* 914  */                         position: "absolute",
/* 915  */                         width: e.length + e.width + "px",
/* 916  */                         height: e.width + "px",
/* 917  */                         background: b,
/* 918  */                         boxShadow: c,
/* 919  */                         transformOrigin: "left",
/* 920  */                         transform: "rotate(" + ~~(360 / e.lines * i + e.rotate) + "deg) translate(" + e.radius + "px,0)",
/* 921  */                         borderRadius: (e.corners * e.width >> 1) + "px"
/* 922  */                     })
/* 923  */                 }
/* 924  */                 for (var h, i = 0, k = (e.lines - 1) * (1 - e.direction) / 2; i < e.lines; i++) h = g(a(), {
/* 925  */                     position: "absolute",
/* 926  */                     top: 1 + ~(e.width / 2) + "px",
/* 927  */                     transform: e.hwaccel ? "translate3d(0,0,0)" : "",
/* 928  */                     opacity: e.opacity,
/* 929  */                     animation: m && d(e.opacity, e.trail, k + i * e.direction, e.lines) + " " + 1 / e.speed + "s linear infinite"
/* 930  */                 }), e.shadow && c(h, g(f("#000", "0 0 4px #000"), {
/* 931  */                     top: "2px"
/* 932  */                 })), c(b, c(h, f(j(e.color, i), "0 0 1px rgba(0,0,0,.1)")));
/* 933  */                 return b
/* 934  */             },
/* 935  */             opacity: function(a, b, c) {
/* 936  */                 b < a.childNodes.length && (a.childNodes[b].style.opacity = c)
/* 937  */             }
/* 938  */         });
/* 939  */         var r = g(a("group"), {
/* 940  */             behavior: "url(#default#VML)"
/* 941  */         });
/* 942  */         return !f(r, "transform") && r.adj ? l() : m = f(r, "animation"), k
/* 943  */     });
/* 944  */     var uc, vc, wc, xc, yc, zc, Ac = d(a),
/* 945  */         Bc = d(b),
/* 946  */         Cc = "quirks" === c.hash.replace("#", ""),
/* 947  */         Dc = mc.csstransforms3d,
/* 948  */         Ec = Dc && !Cc,
/* 949  */         Fc = Dc || "CSS1Compat" === b.compatMode,
/* 950  */         Gc = nc.ok,

/* fotorama.js */

/* 951  */         Hc = navigator.userAgent.match(/Android|webOS|iPhone|iPad|iPod|BlackBerry|Windows Phone/i),
/* 952  */         Ic = !Ec || Hc,
/* 953  */         Jc = "addEventListener",
/* 954  */         Kc = navigator.msPointerEnabled,
/* 955  */         Lc = "onwheel" in b.createElement("div") ? "wheel" : b.onmousewheel !== e ? "mousewheel" : "DOMMouseScroll",
/* 956  */         Mc = 250,
/* 957  */         Nc = 300,
/* 958  */         Oc = 1400,
/* 959  */         Pc = 5e3,
/* 960  */         Qc = 2,
/* 961  */         Rc = 64,
/* 962  */         Sc = 500,
/* 963  */         Tc = 333,
/* 964  */         Uc = "$stageFrame",
/* 965  */         Vc = "$navDotFrame",
/* 966  */         Wc = "$navThumbFrame",
/* 967  */         Xc = f([.1, 0, .25, 1]),
/* 968  */         Yc = 99999,
/* 969  */         Zc = {
/* 970  */             width: null,
/* 971  */             minwidth: null,
/* 972  */             maxwidth: "100%",
/* 973  */             height: null,
/* 974  */             minheight: null,
/* 975  */             maxheight: null,
/* 976  */             ratio: null,
/* 977  */             margin: Qc,
/* 978  */             glimpse: 0,
/* 979  */             nav: "dots",
/* 980  */             navposition: "bottom",
/* 981  */             navwidth: null,
/* 982  */             thumbwidth: Rc,
/* 983  */             thumbheight: Rc,
/* 984  */             thumbmargin: Qc,
/* 985  */             thumbborderwidth: Qc,
/* 986  */             allowfullscreen: !1,
/* 987  */             fit: "contain",
/* 988  */             transition: "slide",
/* 989  */             clicktransition: null,
/* 990  */             transitionduration: Nc,
/* 991  */             captions: !0,
/* 992  */             hash: !1,
/* 993  */             startindex: 0,
/* 994  */             loop: !1,
/* 995  */             autoplay: !1,
/* 996  */             stopautoplayontouch: !0,
/* 997  */             keyboard: !1,
/* 998  */             arrows: !0,
/* 999  */             click: !0,
/* 1000 */             swipe: !0,

/* fotorama.js */

/* 1001 */             trackpad: !0,
/* 1002 */             shuffle: !1,
/* 1003 */             direction: "ltr",
/* 1004 */             shadows: !0,
/* 1005 */             spinner: null
/* 1006 */         }, $c = {
/* 1007 */             left: !0,
/* 1008 */             right: !0,
/* 1009 */             down: !1,
/* 1010 */             up: !1,
/* 1011 */             space: !1,
/* 1012 */             home: !1,
/* 1013 */             end: !1
/* 1014 */         };
/* 1015 */     jQuery.Fotorama = function(a, e) {
/* 1016 */         function f() {
/* 1017 */             d.each(pd, function(a, b) {
/* 1018 */                 if (!b.i) {
/* 1019 */                     b.i = ce++;
/* 1020 */                     var c = z(b.video, !0);
/* 1021 */                     if (c) {
/* 1022 */                         var d = {};
/* 1023 */                         b.video = c, b.img || b.thumb ? b.thumbsReady = !0 : d = A(b, pd, $d), B(pd, {
/* 1024 */                             img: d.img,
/* 1025 */                             thumb: d.thumb
/* 1026 */                         }, b.i, $d)
/* 1027 */                     }
/* 1028 */                 }
/* 1029 */             })
/* 1030 */         }
/* 1031 */ 
/* 1032 */         function g(a) {
/* 1033 */             return Pd[a] || $d.fullScreen
/* 1034 */         }
/* 1035 */ 
/* 1036 */         function i(a) {
/* 1037 */             var b = "keydown." + bb,
/* 1038 */                 c = "keydown." + bb + _d,
/* 1039 */                 d = "resize." + bb + _d;
/* 1040 */             a ? (Bc.on(c, function(a) {
/* 1041 */                 var b, c;
/* 1042 */                 td && 27 === a.keyCode ? (b = !0, fd(td, !0, !0)) : ($d.fullScreen || e.keyboard && !$d.index) && (27 === a.keyCode ? (b = !0, $d.cancelFullScreen()) : a.shiftKey && 32 === a.keyCode && g("space") || 37 === a.keyCode && g("left") || 38 === a.keyCode && g("up") ? c = "<" : 32 === a.keyCode && g("space") || 39 === a.keyCode && g("right") || 40 === a.keyCode && g("down") ? c = ">" : 36 === a.keyCode && g("home") ? c = "<<" : 35 === a.keyCode && g("end") && (c = ">>")), (b || c) && S(a), c && $d.show({
/* 1043 */                     index: c,
/* 1044 */                     slow: a.altKey,
/* 1045 */                     user: !0
/* 1046 */                 })
/* 1047 */             }), $d.index || Bc.off(b).on(b, "textarea, input, select", function(a) {
/* 1048 */                 !vc.hasClass(cb) && a.stopPropagation()
/* 1049 */             }), Ac.on(d, $d.resize)) : (Bc.off(c), Ac.off(d))
/* 1050 */         }

/* fotorama.js */

/* 1051 */ 
/* 1052 */         function j(b) {
/* 1053 */             b !== j.f && (b ? (a.html("").addClass(bb + " " + ae).append(ge).before(ee).before(fe), _($d)) : (ge.detach(), ee.detach(), fe.detach(), a.html(de.urtext).removeClass(ae), ab($d)), i(b), j.f = b)
/* 1054 */         }
/* 1055 */ 
/* 1056 */         function n() {
/* 1057 */             pd = $d.data = pd || O(e.data) || C(a), qd = $d.size = pd.length, !od.ok && e.shuffle && N(pd), f(), ze = y(ze), qd && j(!0)
/* 1058 */         }
/* 1059 */ 
/* 1060 */         function s() {
/* 1061 */             var a = 2 > qd || td;
/* 1062 */             Ce.noMove = a || Id, Ce.noSwipe = a || !e.swipe, !Md && ie.toggleClass(ub, !Ce.noMove && !Ce.noSwipe), Kc && ge.toggleClass(lb, !Ce.noSwipe)
/* 1063 */         }
/* 1064 */ 
/* 1065 */         function t(a) {
/* 1066 */             a === !0 && (a = ""), e.autoplay = Math.max(+a || Pc, 1.5 * Ld)
/* 1067 */         }
/* 1068 */ 
/* 1069 */         function w() {
/* 1070 */             function a(a, c) {
/* 1071 */                 b[a ? "add" : "remove"].push(c)
/* 1072 */             }
/* 1073 */             $d.options = e = Q(e), Id = "crossfade" === e.transition || "dissolve" === e.transition, Cd = e.loop && (qd > 2 || Id) && (!Md || "slide" !== Md), Ld = +e.transitionduration || Nc, Od = "rtl" === e.direction, Pd = d.extend({}, e.keyboard && $c, e.keyboard);
/* 1074 */             var b = {
/* 1075 */                 add: [],
/* 1076 */                 remove: []
/* 1077 */             };
/* 1078 */             qd > 1 ? (Dd = e.nav, Fd = "top" === e.navposition, b.remove.push(Qb), me.toggle( !! e.arrows)) : (Dd = !1, me.hide()), ec(), sd = new rc(d.extend(sc, e.spinner, tc, {
/* 1079 */                 direction: Od ? -1 : 1
/* 1080 */             })), yc(), zc(), e.autoplay && t(e.autoplay), Jd = m(e.thumbwidth) || Rc, Kd = m(e.thumbheight) || Rc, De.ok = Fe.ok = e.trackpad && !Ic, s(), Xc(e, [Be]), Ed = "thumbs" === Dd, Ed ? (lc(qd, "navThumb"), rd = re, Zd = Wc, I(ee, d.Fotorama.jst.style({
/* 1081 */                 w: Jd,
/* 1082 */                 h: Kd,
/* 1083 */                 b: e.thumbborderwidth,
/* 1084 */                 m: e.thumbmargin,
/* 1085 */                 s: _d,
/* 1086 */                 q: !Fc
/* 1087 */             })), oe.addClass(Eb).removeClass(Db)) : "dots" === Dd ? (lc(qd, "navDot"), rd = qe, Zd = Vc, oe.addClass(Db).removeClass(Eb)) : (Dd = !1, oe.removeClass(Eb + " " + Db)), Dd && (Fd ? ne.insertBefore(he) : ne.insertAfter(he), qc.nav = !1, qc(rd, pe, "nav")), Gd = e.allowfullscreen, Gd ? (te.appendTo(he), Hd = Gc && "native" === Gd) : (te.detach(), Hd = !1), a(Id, hb), a(!Id, ib), a(!e.captions, ob), a(Od, mb), a("always" !== e.arrows, pb), Nd = e.shadows && !Ic, a(!Nd, kb), ge.addClass(b.add.join(" ")).removeClass(b.remove.join(" ")), Ae = d.extend({}, e)
/* 1088 */         }
/* 1089 */ 
/* 1090 */         function x(a) {
/* 1091 */             return 0 > a ? (qd + a % qd) % qd : a >= qd ? a % qd : a
/* 1092 */         }
/* 1093 */ 
/* 1094 */         function y(a) {
/* 1095 */             return h(a, 0, qd - 1)
/* 1096 */         }
/* 1097 */ 
/* 1098 */         function D(a) {
/* 1099 */             return Cd ? x(a) : y(a)
/* 1100 */         }

/* fotorama.js */

/* 1101 */ 
/* 1102 */         function W(a) {
/* 1103 */             return a > 0 || Cd ? a - 1 : !1
/* 1104 */         }
/* 1105 */ 
/* 1106 */         function X(a) {
/* 1107 */             return qd - 1 > a || Cd ? a + 1 : !1
/* 1108 */         }
/* 1109 */ 
/* 1110 */         function $() {
/* 1111 */             Ce.min = Cd ? -1 / 0 : -q(qd - 1, Be.w, e.margin, wd), Ce.max = Cd ? 1 / 0 : -q(0, Be.w, e.margin, wd), Ce.snap = Be.w + e.margin
/* 1112 */         }
/* 1113 */ 
/* 1114 */         function Ib() {
/* 1115 */             Ee.min = Math.min(0, Be.nw - pe.width()), Ee.max = 0, pe.toggleClass(ub, !(Ee.noMove = Ee.min === Ee.max))
/* 1116 */         }
/* 1117 */ 
/* 1118 */         function Jb(a, b, c) {
/* 1119 */             if ("number" == typeof a) {
/* 1120 */                 a = new Array(a);
/* 1121 */                 var e = !0
/* 1122 */             }
/* 1123 */             return d.each(a, function(a, d) {
/* 1124 */                 if (e && (d = a), "number" == typeof d) {
/* 1125 */                     var f = pd[x(d)];
/* 1126 */                     if (f) {
/* 1127 */                         var g = "$" + b + "Frame",
/* 1128 */                             h = f[g];
/* 1129 */                         c.call(this, a, d, f, h, g, h && h.data())
/* 1130 */                     }
/* 1131 */                 }
/* 1132 */             })
/* 1133 */         }
/* 1134 */ 
/* 1135 */         function Kb(a, b, c, d) {
/* 1136 */             (!Qd || "*" === Qd && d === Bd) && (a = p(e.width) || p(a) || Sc, b = p(e.height) || p(b) || Tc, $d.resize({
/* 1137 */                 width: a,
/* 1138 */                 ratio: e.ratio || c || a / b
/* 1139 */             }, 0, d === Bd ? !0 : "*"))
/* 1140 */         }
/* 1141 */ 
/* 1142 */         function Lb(a, b, c, f, g) {
/* 1143 */             Jb(a, b, function(a, h, i, j, k, l) {
/* 1144 */                 function m(a) {
/* 1145 */                     var b = x(h);
/* 1146 */                     Zc(a, {
/* 1147 */                         index: b,
/* 1148 */                         src: v,
/* 1149 */                         frame: pd[b]
/* 1150 */                     })

/* fotorama.js */

/* 1151 */                 }
/* 1152 */ 
/* 1153 */                 function n() {
/* 1154 */                     s.remove(), d.Fotorama.cache[v] = "error", i.html && "stage" === b || !w || w === v ? (!v || i.html || q ? "stage" === b && (j.trigger("f:load").removeClass(Vb + " " + Ub).addClass(Wb), m("load"), Kb()) : (j.trigger("f:error").removeClass(Vb).addClass(Ub), m("error")), l.state = "error", !(qd > 1 && pd[h] === i) || i.html || i.deleted || i.video || q || (i.deleted = !0, $d.splice(h, 1))) : (i[u] = v = w, Lb([h], b, c, f, !0))
/* 1155 */                 }
/* 1156 */ 
/* 1157 */                 function o() {
/* 1158 */                     d.Fotorama.measures[v] = t.measures = d.Fotorama.measures[v] || {
/* 1159 */                         width: r.width,
/* 1160 */                         height: r.height,
/* 1161 */                         ratio: r.width / r.height
/* 1162 */                     }, Kb(t.measures.width, t.measures.height, t.measures.ratio, h), s.off("load error").addClass($b + (q ? " " + _b : "")).prependTo(j), H(s, c || Be, f || i.fit || e.fit), d.Fotorama.cache[v] = l.state = "loaded", setTimeout(function() {
/* 1163 */                         j.trigger("f:load").removeClass(Vb + " " + Ub).addClass(Wb + " " + (q ? Xb : Yb)), "stage" === b && m("load")
/* 1164 */                     }, 5)
/* 1165 */                 }
/* 1166 */ 
/* 1167 */                 function p() {
/* 1168 */                     var a = 10;
/* 1169 */                     F(function() {
/* 1170 */                         return !Xd || !a-- && !Ic
/* 1171 */                     }, function() {
/* 1172 */                         o()
/* 1173 */                     })
/* 1174 */                 }
/* 1175 */                 if (j) {
/* 1176 */                     var q = $d.fullScreen && i.full && i.full !== i.img && !l.$full && "stage" === b;
/* 1177 */                     if (!l.$img || g || q) {
/* 1178 */                         var r = new Image,
/* 1179 */                             s = d(r),
/* 1180 */                             t = s.data();
/* 1181 */                         l[q ? "$full" : "$img"] = s;
/* 1182 */                         var u = "stage" === b ? q ? "full" : "img" : "thumb",
/* 1183 */                             v = i[u],
/* 1184 */                             w = q ? null : i["stage" === b ? "thumb" : "img"];
/* 1185 */                         if ("navThumb" === b && (j = l.$wrap), !v) return void n();
/* 1186 */                         d.Fotorama.cache[v] ? ! function y() {
/* 1187 */                             "error" === d.Fotorama.cache[v] ? n() : "loaded" === d.Fotorama.cache[v] ? setTimeout(p, 0) : setTimeout(y, 100)
/* 1188 */                         }() : (d.Fotorama.cache[v] = "*", s.on("load", p).on("error", n)), l.state = "", r.src = v
/* 1189 */                     }
/* 1190 */                 }
/* 1191 */             })
/* 1192 */         }
/* 1193 */ 
/* 1194 */         function Zb(a) {
/* 1195 */             ye.append(sd.spin().el).appendTo(a)
/* 1196 */         }
/* 1197 */ 
/* 1198 */         function ec() {
/* 1199 */             ye.detach(), sd && sd.stop()
/* 1200 */         }

/* fotorama.js */

/* 1201 */ 
/* 1202 */         function kc() {
/* 1203 */             var a = $d.activeFrame[Uc];
/* 1204 */             a && !a.data().state && (Zb(a), a.on("f:load f:error", function() {
/* 1205 */                 a.off("f:load f:error"), ec()
/* 1206 */             }))
/* 1207 */         }
/* 1208 */ 
/* 1209 */         function lc(a, b) {
/* 1210 */             Jb(a, b, function(a, c, f, g, h, i) {
/* 1211 */                 g || (g = f[h] = ge[h].clone(), i = g.data(), i.data = f, "stage" === b ? (f.html && d('<div class="' + dc + '"></div>').append(f._html ? d(f.html).removeAttr("id").html(f._html) : f.html).appendTo(g), e.captions && f.caption && d(M(hc, M(ic, f.caption))).appendTo(g), f.video && g.addClass(sb).append(ve.clone()), je = je.add(g)) : "navDot" === b ? qe = qe.add(g) : "navThumb" === b && (i.$wrap = g.children(":first"), re = re.add(g), f.video && g.append(ve.clone())))
/* 1212 */             })
/* 1213 */         }
/* 1214 */ 
/* 1215 */         function mc(a, b, c) {
/* 1216 */             return a && a.length && H(a, b, c)
/* 1217 */         }
/* 1218 */ 
/* 1219 */         function oc(a) {
/* 1220 */             Jb(a, "stage", function(a, b, c, f, g, h) {
/* 1221 */                 if (f) {
/* 1222 */                     He[Uc][x(b)] = f.css(d.extend({
/* 1223 */                         left: Id ? 0 : q(b, Be.w, e.margin, wd)
/* 1224 */                     }, Id && l(0))), E(f[0]) && (f.appendTo(ie), fd(c.$video));
/* 1225 */                     var i = c.fit || e.fit;
/* 1226 */                     mc(h.$img, Be, i), mc(h.$full, Be, i)
/* 1227 */                 }
/* 1228 */             })
/* 1229 */         }
/* 1230 */ 
/* 1231 */         function pc(a, b) {
/* 1232 */             if ("thumbs" === Dd && !isNaN(a)) {
/* 1233 */                 var c = -a,
/* 1234 */                     e = -a + Be.nw;
/* 1235 */                 re.each(function() {
/* 1236 */                     var a = d(this),
/* 1237 */                         f = a.data(),
/* 1238 */                         g = f.eq,
/* 1239 */                         h = {
/* 1240 */                             h: Kd
/* 1241 */                         }, i = "cover";
/* 1242 */                     h.w = f.w, f.l + f.w < c || f.l > e || mc(f.$img, h, i) || b && Lb([g], "navThumb", h, i)
/* 1243 */                 })
/* 1244 */             }
/* 1245 */         }
/* 1246 */ 
/* 1247 */         function qc(a, b, c) {
/* 1248 */             if (!qc[c]) {
/* 1249 */                 var f = "nav" === c && Ed,
/* 1250 */                     g = 0;

/* fotorama.js */

/* 1251 */                 b.append(a.filter(function() {
/* 1252 */                     for (var a, b = d(this), c = b.data(), e = 0, f = pd.length; f > e; e++)
/* 1253 */                         if (c.data === pd[e]) {
/* 1254 */                             a = !0, c.eq = e;
/* 1255 */                             break
/* 1256 */                         }
/* 1257 */                     return a || b.remove() && !1
/* 1258 */                 }).sort(function(a, b) {
/* 1259 */                     return d(a).data().eq - d(b).data().eq
/* 1260 */                 }).each(function() {
/* 1261 */                     if (f) {
/* 1262 */                         var a = d(this),
/* 1263 */                             b = a.data(),
/* 1264 */                             c = Math.round(Kd * b.data.thumbratio) || Jd;
/* 1265 */                         b.l = g, b.w = c, a.css({
/* 1266 */                             width: c
/* 1267 */                         }), g += c + e.thumbmargin
/* 1268 */                     }
/* 1269 */                 })), qc[c] = !0
/* 1270 */             }
/* 1271 */         }
/* 1272 */ 
/* 1273 */         function wc(a) {
/* 1274 */             return a - Ie > Be.w / 3
/* 1275 */         }
/* 1276 */ 
/* 1277 */         function xc(a) {
/* 1278 */             return !(Cd || ze + a && ze - qd + a || td)
/* 1279 */         }
/* 1280 */ 
/* 1281 */         function yc() {
/* 1282 */             ke.toggleClass(xb, xc(0)), le.toggleClass(xb, xc(1))
/* 1283 */         }
/* 1284 */ 
/* 1285 */         function zc() {
/* 1286 */             De.ok && (De.prevent = {
/* 1287 */                 "<": xc(0),
/* 1288 */                 ">": xc(1)
/* 1289 */             })
/* 1290 */         }
/* 1291 */ 
/* 1292 */         function Cc(a) {
/* 1293 */             var b, c, d = a.data();
/* 1294 */             return Ed ? (b = d.l, c = d.w) : (b = a.position().left, c = a.width()), {
/* 1295 */                 c: b + c / 2,
/* 1296 */                 min: -b + 10 * e.thumbmargin,
/* 1297 */                 max: -b + Be.w - c - 10 * e.thumbmargin
/* 1298 */             }
/* 1299 */         }
/* 1300 */ 

/* fotorama.js */

/* 1301 */         function Dc(a) {
/* 1302 */             var b = $d.activeFrame[Zd].data();
/* 1303 */             U(se, {
/* 1304 */                 time: .9 * a,
/* 1305 */                 pos: b.l,
/* 1306 */                 width: b.w - 2 * e.thumbborderwidth
/* 1307 */             })
/* 1308 */         }
/* 1309 */ 
/* 1310 */         function Hc(a) {
/* 1311 */             var b = pd[a.guessIndex][Zd];
/* 1312 */             if (b) {
/* 1313 */                 var c = Ee.min !== Ee.max,
/* 1314 */                     d = c && Cc($d.activeFrame[Zd]),
/* 1315 */                     e = c && (a.keep && Hc.l ? Hc.l : h((a.coo || Be.nw / 2) - Cc(b).c, d.min, d.max)),
/* 1316 */                     f = c && h(e, Ee.min, Ee.max),
/* 1317 */                     g = .9 * a.time;
/* 1318 */                 U(pe, {
/* 1319 */                     time: g,
/* 1320 */                     pos: f || 0,
/* 1321 */                     onEnd: function() {
/* 1322 */                         pc(f, !0)
/* 1323 */                     }
/* 1324 */                 }), ed(oe, J(f, Ee.min, Ee.max)), Hc.l = e
/* 1325 */             }
/* 1326 */         }
/* 1327 */ 
/* 1328 */         function Jc() {
/* 1329 */             Lc(Zd), Ge[Zd].push($d.activeFrame[Zd].addClass(Pb))
/* 1330 */         }
/* 1331 */ 
/* 1332 */         function Lc(a) {
/* 1333 */             for (var b = Ge[a]; b.length;) b.shift().removeClass(Pb)
/* 1334 */         }
/* 1335 */ 
/* 1336 */         function Oc(a) {
/* 1337 */             var b = He[a];
/* 1338 */             d.each(vd, function(a, c) {
/* 1339 */                 delete b[x(c)]
/* 1340 */             }), d.each(b, function(a, c) {
/* 1341 */                 delete b[a], c.detach()
/* 1342 */             })
/* 1343 */         }
/* 1344 */ 
/* 1345 */         function Qc(a) {
/* 1346 */             wd = xd = ze;
/* 1347 */             var b = $d.activeFrame,
/* 1348 */                 c = b[Uc];
/* 1349 */             c && (Lc(Uc), Ge[Uc].push(c.addClass(Pb)), a || $d.show.onEnd(!0), u(ie, 0, !0), Oc(Uc), oc(vd), $(), Ib())
/* 1350 */         }

/* fotorama.js */

/* 1351 */ 
/* 1352 */         function Xc(a, b) {
/* 1353 */             a && d.each(b, function(b, c) {
/* 1354 */                 c && d.extend(c, {
/* 1355 */                     width: a.width || c.width,
/* 1356 */                     height: a.height,
/* 1357 */                     minwidth: a.minwidth,
/* 1358 */                     maxwidth: a.maxwidth,
/* 1359 */                     minheight: a.minheight,
/* 1360 */                     maxheight: a.maxheight,
/* 1361 */                     ratio: R(a.ratio)
/* 1362 */                 })
/* 1363 */             })
/* 1364 */         }
/* 1365 */ 
/* 1366 */         function Zc(b, c) {
/* 1367 */             a.trigger(bb + ":" + b, [$d, c])
/* 1368 */         }
/* 1369 */ 
/* 1370 */         function _c() {
/* 1371 */             clearTimeout(ad.t), Xd = 1, e.stopautoplayontouch ? $d.stopAutoplay() : Ud = !0
/* 1372 */         }
/* 1373 */ 
/* 1374 */         function ad() {
/* 1375 */             e.stopautoplayontouch || (bd(), cd()), ad.t = setTimeout(function() {
/* 1376 */                 Xd = 0
/* 1377 */             }, Nc + Mc)
/* 1378 */         }
/* 1379 */ 
/* 1380 */         function bd() {
/* 1381 */             Ud = !(!td && !Vd)
/* 1382 */         }
/* 1383 */ 
/* 1384 */         function cd() {
/* 1385 */             if (clearTimeout(cd.t), !e.autoplay || Ud) return void($d.autoplay && ($d.autoplay = !1, Zc("stopautoplay")));
/* 1386 */             $d.autoplay || ($d.autoplay = !0, Zc("startautoplay"));
/* 1387 */             var a = ze,
/* 1388 */                 b = $d.activeFrame[Uc].data();
/* 1389 */             F(function() {
/* 1390 */                 return b.state || a !== ze
/* 1391 */             }, function() {
/* 1392 */                 cd.t = setTimeout(function() {
/* 1393 */                     Ud || a !== ze || $d.show(Cd ? T(!Od) : x(ze + (Od ? -1 : 1)))
/* 1394 */                 }, e.autoplay)
/* 1395 */             })
/* 1396 */         }
/* 1397 */ 
/* 1398 */         function dd() {
/* 1399 */             $d.fullScreen && ($d.fullScreen = !1, Gc && nc.cancel(be), vc.removeClass(cb), uc.removeClass(cb), a.removeClass(Sb).insertAfter(fe), Be = d.extend({}, Wd), fd(td, !0, !0), kd("x", !1), $d.resize(), Lb(vd, "stage"), P(Sd, Rd), Zc("fullscreenexit"))
/* 1400 */         }

/* fotorama.js */

/* 1401 */ 
/* 1402 */         function ed(a, b) {
/* 1403 */             Nd && (a.removeClass(Nb + " " + Ob), b && !td && a.addClass(b.replace(/^|\s/g, " " + Mb + "--")))
/* 1404 */         }
/* 1405 */ 
/* 1406 */         function fd(a, b, c) {
/* 1407 */             b && (ge.removeClass(gb), td = !1, s()), a && a !== td && (a.remove(), Zc("unloadvideo")), c && (bd(), cd())
/* 1408 */         }
/* 1409 */ 
/* 1410 */         function gd(a) {
/* 1411 */             ge.toggleClass(jb, a)
/* 1412 */         }
/* 1413 */ 
/* 1414 */         function hd(a) {
/* 1415 */             if (!Ce.flow) {
/* 1416 */                 var b = a ? a.pageX : hd.x,
/* 1417 */                     c = b && !xc(wc(b)) && e.click;
/* 1418 */                 hd.p === c || !Id && e.swipe || !he.toggleClass(vb, c) || (hd.p = c, hd.x = b)
/* 1419 */             }
/* 1420 */         }
/* 1421 */ 
/* 1422 */         function id(a) {
/* 1423 */             clearTimeout(id.t), e.clicktransition && e.clicktransition !== e.transition ? (Md = e.transition, $d.setOptions({
/* 1424 */                 transition: e.clicktransition
/* 1425 */             }), id.t = setTimeout(function() {
/* 1426 */                 $d.show(a)
/* 1427 */             }, 10)) : $d.show(a)
/* 1428 */         }
/* 1429 */ 
/* 1430 */         function jd(a, b) {
/* 1431 */             var c = a.target,
/* 1432 */                 f = d(c);
/* 1433 */             f.hasClass(fc) ? $d.playVideo() : c === ue ? $d[($d.fullScreen ? "cancel" : "request") + "FullScreen"]() : td ? c === xe && fd(td, !0, !0) : b ? gd() : e.click && id({
/* 1434 */                 index: a.shiftKey || T(wc(a._x)),
/* 1435 */                 slow: a.altKey,
/* 1436 */                 user: !0
/* 1437 */             })
/* 1438 */         }
/* 1439 */ 
/* 1440 */         function kd(a, b) {
/* 1441 */             Ce[a] = Ee[a] = b
/* 1442 */         }
/* 1443 */ 
/* 1444 */         function ld(a, b) {
/* 1445 */             var c = d(this).data().eq;
/* 1446 */             id({
/* 1447 */                 index: c,
/* 1448 */                 slow: a.altKey,
/* 1449 */                 user: !0,
/* 1450 */                 coo: a._x - oe.offset().left,

/* fotorama.js */

/* 1451 */                 time: b
/* 1452 */             })
/* 1453 */         }
/* 1454 */ 
/* 1455 */         function md() {
/* 1456 */             if (n(), w(), !md.i) {
/* 1457 */                 md.i = !0;
/* 1458 */                 var a = e.startindex;
/* 1459 */                 (a || e.hash && c.hash) && (Bd = K(a || c.hash.replace(/^#/, ""), pd, 0 === $d.index || a, a)), ze = wd = xd = yd = Bd = D(Bd) || 0
/* 1460 */             }
/* 1461 */             if (qd) {
/* 1462 */                 if (nd()) return;
/* 1463 */                 td && fd(td, !0), vd = [], Oc(Uc), $d.show({
/* 1464 */                     index: ze,
/* 1465 */                     time: 0,
/* 1466 */                     reset: md.ok
/* 1467 */                 }), $d.resize()
/* 1468 */             } else $d.destroy();
/* 1469 */             md.ok = !0
/* 1470 */         }
/* 1471 */ 
/* 1472 */         function nd() {
/* 1473 */             return !nd.f === Od ? (nd.f = Od, ze = qd - 1 - ze, $d.reverse(), !0) : void 0
/* 1474 */         }
/* 1475 */ 
/* 1476 */         function od() {
/* 1477 */             od.ok || (od.ok = !0, Zc("ready"))
/* 1478 */         }
/* 1479 */         uc = uc || d("html"), vc = vc || d("body");
/* 1480 */         var pd, qd, rd, sd, td, ud, vd, wd, xd, yd, zd, Ad, Bd, Cd, Dd, Ed, Fd, Gd, Hd, Id, Jd, Kd, Ld, Md, Nd, Od, Pd, Qd, Rd, Sd, Td, Ud, Vd, Wd, Xd, Yd, Zd, $d = this,
/* 1481 */             _d = d.now(),
/* 1482 */             ae = bb + _d,
/* 1483 */             be = a[0],
/* 1484 */             ce = 1,
/* 1485 */             de = a.data(),
/* 1486 */             ee = d("<style></style>"),
/* 1487 */             fe = d(M(Rb)),
/* 1488 */             ge = d(M(db)),
/* 1489 */             he = d(M(qb)).appendTo(ge),
/* 1490 */             ie = (he[0], d(M(tb)).appendTo(he)),
/* 1491 */             je = d(),
/* 1492 */             ke = d(M(wb + " " + yb)),
/* 1493 */             le = d(M(wb + " " + zb)),
/* 1494 */             me = ke.add(le).appendTo(he),
/* 1495 */             ne = d(M(Bb)),
/* 1496 */             oe = d(M(Ab)).appendTo(ne),
/* 1497 */             pe = d(M(Cb)).appendTo(oe),
/* 1498 */             qe = d(),
/* 1499 */             re = d(),
/* 1500 */             se = (ie.data(), pe.data(), d(M(cc)).appendTo(pe)),

/* fotorama.js */

/* 1501 */             te = d(M(Tb)),
/* 1502 */             ue = te[0],
/* 1503 */             ve = d(M(fc)),
/* 1504 */             we = d(M(gc)).appendTo(he),
/* 1505 */             xe = we[0],
/* 1506 */             ye = d(M(jc)),
/* 1507 */             ze = !1,
/* 1508 */             Ae = {}, Be = {}, Ce = {}, De = {}, Ee = {}, Fe = {}, Ge = {}, He = {}, Ie = 0,
/* 1509 */             Je = [];
/* 1510 */         ge[Uc] = d(M(rb)), ge[Wc] = d(M(Fb + " " + Hb, M(bc))), ge[Vc] = d(M(Fb + " " + Gb, M(ac))), Ge[Uc] = [], Ge[Wc] = [], Ge[Vc] = [], He[Uc] = {}, ge.addClass(Ec ? fb : eb), de.fotorama = this, $d.startAutoplay = function(a) {
/* 1511 */             return $d.autoplay ? this : (Ud = Vd = !1, t(a || e.autoplay), cd(), this)
/* 1512 */         }, $d.stopAutoplay = function() {
/* 1513 */             return $d.autoplay && (Ud = Vd = !0, cd()), this
/* 1514 */         }, $d.show = function(a) {
/* 1515 */             var b;
/* 1516 */             "object" != typeof a ? (b = a, a = {}) : b = a.index, b = ">" === b ? xd + 1 : "<" === b ? xd - 1 : "<<" === b ? 0 : ">>" === b ? qd - 1 : b, b = isNaN(b) ? K(b, pd, !0) : b, b = "undefined" == typeof b ? ze || 0 : b, $d.activeIndex = ze = D(b), zd = W(ze), Ad = X(ze), vd = [ze, zd, Ad], xd = Cd ? b : ze;
/* 1517 */             var c = Math.abs(yd - xd),
/* 1518 */                 d = v(a.time, function() {
/* 1519 */                     return Math.min(Ld * (1 + (c - 1) / 12), 2 * Ld)
/* 1520 */                 }),
/* 1521 */                 f = a.overPos;
/* 1522 */             a.slow && (d *= 10), $d.activeFrame = ud = pd[ze], fd(td, ud.i !== pd[x(wd)].i), lc(vd, "stage"), oc(Ic ? [xd] : [xd, W(xd), X(xd)]), kd("go", !0), a.reset || Zc("show", {
/* 1523 */                 user: a.user,
/* 1524 */                 time: d
/* 1525 */             }), Ud = !0;
/* 1526 */             var g = $d.show.onEnd = function(b) {
/* 1527 */                 if (!g.ok) {
/* 1528 */                     if (g.ok = !0, b || Qc(!0), !a.reset && (Zc("showend", {
/* 1529 */                         user: a.user
/* 1530 */                     }), !b && Md && Md !== e.transition)) return $d.setOptions({
/* 1531 */                         transition: Md
/* 1532 */                     }), void(Md = !1);
/* 1533 */                     kc(), Lb(vd, "stage"), kd("go", !1), zc(), hd(), bd(), cd()
/* 1534 */                 }
/* 1535 */             };
/* 1536 */             if (Id) {
/* 1537 */                 var i = ud[Uc],
/* 1538 */                     j = ze !== yd ? pd[yd][Uc] : null;
/* 1539 */                 V(i, j, je, {
/* 1540 */                     time: d,
/* 1541 */                     method: e.transition,
/* 1542 */                     onEnd: g
/* 1543 */                 }, Je)
/* 1544 */             } else U(ie, {
/* 1545 */                 pos: -q(xd, Be.w, e.margin, wd),
/* 1546 */                 overPos: f,
/* 1547 */                 time: d,
/* 1548 */                 onEnd: g,
/* 1549 */                 _001: !0
/* 1550 */             }); if (yc(), Dd) {

/* fotorama.js */

/* 1551 */                 Jc();
/* 1552 */                 var k = y(ze + h(xd - yd, -1, 1));
/* 1553 */                 Hc({
/* 1554 */                     time: d,
/* 1555 */                     coo: k !== ze && a.coo,
/* 1556 */                     guessIndex: "undefined" != typeof a.coo ? k : ze,
/* 1557 */                     keep: a.reset
/* 1558 */                 }), Ed && Dc(d)
/* 1559 */             }
/* 1560 */             return Td = "undefined" != typeof yd && yd !== ze, yd = ze, e.hash && Td && !$d.eq && G(ud.id || ze + 1), this
/* 1561 */         }, $d.requestFullScreen = function() {
/* 1562 */             return Gd && !$d.fullScreen && (Rd = Ac.scrollTop(), Sd = Ac.scrollLeft(), P(0, 0), kd("x", !0), Wd = d.extend({}, Be), a.addClass(Sb).appendTo(vc.addClass(cb)), uc.addClass(cb), fd(td, !0, !0), $d.fullScreen = !0, Hd && nc.request(be), $d.resize(), Lb(vd, "stage"), kc(), Zc("fullscreenenter")), this
/* 1563 */         }, $d.cancelFullScreen = function() {
/* 1564 */             return Hd && nc.is() ? nc.cancel(b) : dd(), this
/* 1565 */         }, b.addEventListener && b.addEventListener(nc.event, function() {
/* 1566 */             !pd || nc.is() || td || dd()
/* 1567 */         }, !1), $d.resize = function(a) {
/* 1568 */             if (!pd) return this;
/* 1569 */             Xc($d.fullScreen ? {
/* 1570 */                 width: "100%",
/* 1571 */                 maxwidth: null,
/* 1572 */                 minwidth: null,
/* 1573 */                 height: "100%",
/* 1574 */                 maxheight: null,
/* 1575 */                 minheight: null
/* 1576 */             } : Q(a), [Be, $d.fullScreen || e]);
/* 1577 */             var b = arguments[1] || 0,
/* 1578 */                 c = arguments[2],
/* 1579 */                 d = Be.width,
/* 1580 */                 f = Be.height,
/* 1581 */                 g = Be.ratio,
/* 1582 */                 i = Ac.height() - (Dd ? oe.height() : 0);
/* 1583 */             return p(d) && (ge.addClass(nb).css({
/* 1584 */                 width: d,
/* 1585 */                 minWidth: Be.minwidth || 0,
/* 1586 */                 maxWidth: Be.maxwidth || Yc
/* 1587 */             }), d = Be.W = Be.w = ge.width(), Be.nw = Dd && o(e.navwidth, d) || d, e.glimpse && (Be.w -= Math.round(2 * (o(e.glimpse, d) || 0))), ie.css({
/* 1588 */                 width: Be.w,
/* 1589 */                 marginLeft: (Be.W - Be.w) / 2
/* 1590 */             }), f = o(f, i), f = f || g && d / g, f && (d = Math.round(d), f = Be.h = Math.round(h(f, o(Be.minheight, i), o(Be.maxheight, i))), he.stop().animate({
/* 1591 */                 width: d,
/* 1592 */                 height: f
/* 1593 */             }, b, function() {
/* 1594 */                 ge.removeClass(nb)
/* 1595 */             }), Qc(), Dd && (oe.stop().animate({
/* 1596 */                 width: Be.nw
/* 1597 */             }, b), Hc({
/* 1598 */                 guessIndex: ze,
/* 1599 */                 time: b,
/* 1600 */                 keep: !0

/* fotorama.js */

/* 1601 */             }), Ed && qc.nav && Dc(b)), Qd = c || !0, od())), Ie = he.offset().left, this
/* 1602 */         }, $d.setOptions = function(a) {
/* 1603 */             return d.extend(e, a), md(), this
/* 1604 */         }, $d.shuffle = function() {
/* 1605 */             return pd && N(pd) && md(), this
/* 1606 */         }, $d.destroy = function() {
/* 1607 */             return $d.cancelFullScreen(), $d.stopAutoplay(), pd = $d.data = null, j(), vd = [], Oc(Uc), this
/* 1608 */         }, $d.playVideo = function() {
/* 1609 */             var a = $d.activeFrame,
/* 1610 */                 b = a.video,
/* 1611 */                 c = ze;
/* 1612 */             return "object" == typeof b && a.videoReady && (Hd && $d.fullScreen && $d.cancelFullScreen(), F(function() {
/* 1613 */                 return !nc.is() || c !== ze
/* 1614 */             }, function() {
/* 1615 */                 c === ze && (a.$video = a.$video || d(d.Fotorama.jst.video(b)), a.$video.appendTo(a[Uc]), ge.addClass(gb), td = a.$video, s(), Zc("loadvideo"))
/* 1616 */             })), this
/* 1617 */         }, $d.stopVideo = function() {
/* 1618 */             return fd(td, !0, !0), this
/* 1619 */         }, he.on("mousemove", hd), Ce = Y(ie, {
/* 1620 */             onStart: _c,
/* 1621 */             onMove: function(a, b) {
/* 1622 */                 ed(he, b.edge)
/* 1623 */             },
/* 1624 */             onTouchEnd: ad,
/* 1625 */             onEnd: function(a) {
/* 1626 */                 ed(he);
/* 1627 */                 var b = (Kc && !Yd || a.touch) && e.arrows && "always" !== e.arrows;
/* 1628 */                 if (a.moved || b && a.pos !== a.newPos && !a.control) {
/* 1629 */                     var c = r(a.newPos, Be.w, e.margin, wd);
/* 1630 */                     $d.show({
/* 1631 */                         index: c,
/* 1632 */                         time: Id ? Ld : a.time,
/* 1633 */                         overPos: a.overPos,
/* 1634 */                         user: !0
/* 1635 */                     })
/* 1636 */                 } else a.aborted || a.control || jd(a.startEvent, b)
/* 1637 */             },
/* 1638 */             _001: !0,
/* 1639 */             timeLow: 1,
/* 1640 */             timeHigh: 1,
/* 1641 */             friction: 2,
/* 1642 */             select: "." + Qb + ", ." + Qb + " *",
/* 1643 */             $wrap: he
/* 1644 */         }), Ee = Y(pe, {
/* 1645 */             onStart: _c,
/* 1646 */             onMove: function(a, b) {
/* 1647 */                 ed(oe, b.edge)
/* 1648 */             },
/* 1649 */             onTouchEnd: ad,
/* 1650 */             onEnd: function(a) {

/* fotorama.js */

/* 1651 */                 function b() {
/* 1652 */                     Hc.l = a.newPos, bd(), cd(), pc(a.newPos, !0)
/* 1653 */                 }
/* 1654 */                 if (a.moved) a.pos !== a.newPos ? (Ud = !0, U(pe, {
/* 1655 */                     time: a.time,
/* 1656 */                     pos: a.newPos,
/* 1657 */                     overPos: a.overPos,
/* 1658 */                     onEnd: b
/* 1659 */                 }), pc(a.newPos), Nd && ed(oe, J(a.newPos, Ee.min, Ee.max))) : b();
/* 1660 */                 else {
/* 1661 */                     var c = a.$target.closest("." + Fb, pe)[0];
/* 1662 */                     c && ld.call(c, a.startEvent)
/* 1663 */                 }
/* 1664 */             },
/* 1665 */             timeLow: .5,
/* 1666 */             timeHigh: 2,
/* 1667 */             friction: 5,
/* 1668 */             $wrap: oe
/* 1669 */         }), De = Z(he, {
/* 1670 */             shift: !0,
/* 1671 */             onEnd: function(a, b) {
/* 1672 */                 _c(), ad(), $d.show({
/* 1673 */                     index: b,
/* 1674 */                     slow: a.altKey
/* 1675 */                 })
/* 1676 */             }
/* 1677 */         }), Fe = Z(oe, {
/* 1678 */             onEnd: function(a, b) {
/* 1679 */                 _c(), ad();
/* 1680 */                 var c = u(pe) + .25 * b;
/* 1681 */                 pe.css(k(h(c, Ee.min, Ee.max))), Nd && ed(oe, J(c, Ee.min, Ee.max)), Fe.prevent = {
/* 1682 */                     "<": c >= Ee.max,
/* 1683 */                     ">": c <= Ee.min
/* 1684 */                 }, clearTimeout(Fe.t), Fe.t = setTimeout(function() {
/* 1685 */                     pc(c, !0)
/* 1686 */                 }, Mc), pc(c)
/* 1687 */             }
/* 1688 */         }), ge.hover(function() {
/* 1689 */             setTimeout(function() {
/* 1690 */                 Xd || (Yd = !0, gd(!Yd))
/* 1691 */             }, 0)
/* 1692 */         }, function() {
/* 1693 */             Yd && (Yd = !1, gd(!Yd))
/* 1694 */         }), L(me, function(a) {
/* 1695 */             S(a), id({
/* 1696 */                 index: me.index(this) ? ">" : "<",
/* 1697 */                 slow: a.altKey,
/* 1698 */                 user: !0
/* 1699 */             })
/* 1700 */         }, {

/* fotorama.js */

/* 1701 */             onStart: function() {
/* 1702 */                 _c(), Ce.control = !0
/* 1703 */             },
/* 1704 */             onTouchEnd: ad
/* 1705 */         }), d.each("load push pop shift unshift reverse sort splice".split(" "), function(a, b) {
/* 1706 */             $d[b] = function() {
/* 1707 */                 return pd = pd || [], "load" !== b ? Array.prototype[b].apply(pd, arguments) : arguments[0] && "object" == typeof arguments[0] && arguments[0].length && (pd = O(arguments[0])), md(), $d
/* 1708 */             }
/* 1709 */         }), md()
/* 1710 */     }, d.fn.fotorama = function(b) {
/* 1711 */         return this.each(function() {
/* 1712 */             var c = this,
/* 1713 */                 e = d(this),
/* 1714 */                 f = e.data(),
/* 1715 */                 g = f.fotorama;
/* 1716 */             g ? g.setOptions(b) : F(function() {
/* 1717 */                 return !D(c)
/* 1718 */             }, function() {
/* 1719 */                 f.urtext = e.html(), new d.Fotorama(e, d.extend({}, Zc, a.fotoramaDefaults, b, f))
/* 1720 */             })
/* 1721 */         })
/* 1722 */     }, d.Fotorama.instances = [], d.Fotorama.cache = {}, d.Fotorama.measures = {}, d = d || {}, d.Fotorama = d.Fotorama || {}, d.Fotorama.jst = d.Fotorama.jst || {}, d.Fotorama.jst.style = function(a) {
/* 1723 */         {
/* 1724 */             var b, c = "";
/* 1725 */             lc.escape
/* 1726 */         }
/* 1727 */         return c += ".fotorama" + (null == (b = a.s) ? "" : b) + " .fotorama__nav--thumbs .fotorama__nav__frame{\npadding:" + (null == (b = a.m) ? "" : b) + "px;\nheight:" + (null == (b = a.h) ? "" : b) + "px}\n.fotorama" + (null == (b = a.s) ? "" : b) + " .fotorama__thumb-border{\nheight:" + (null == (b = a.h - a.b * (a.q ? 0 : 2)) ? "" : b) + "px;\nborder-width:" + (null == (b = a.b) ? "" : b) + "px;\nmargin-top:" + (null == (b = a.m) ? "" : b) + "px}"
/* 1728 */     }, d.Fotorama.jst.video = function(a) {
/* 1729 */         function b() {
/* 1730 */             c += d.call(arguments, "")
/* 1731 */         }
/* 1732 */         var c = "",
/* 1733 */             d = (lc.escape, Array.prototype.join);
/* 1734 */         return c += '<div class="fotorama__video"><iframe src="', b(("youtube" == a.type ? "http://youtube.com/embed/" + a.id + "?autoplay=1" : "vimeo" == a.type ? "http://player.vimeo.com/video/" + a.id + "?autoplay=1&badge=0" : a.id) + (a.s && "custom" != a.type ? "&" + a.s : "")), c += '" frameborder="0" allowfullscreen></iframe></div>'
/* 1735 */     }, d(function() {
/* 1736 */         d("." + bb + ':not([data-auto="false"])').fotorama()
/* 1737 */     })
/* 1738 */ }(window, document, location, "undefined" != typeof jQuery && jQuery);

;
/* handlebars-v2.0.0.js */

/* 1    */ /*!
/* 2    *| 
/* 3    *|  handlebars v2.0.0
/* 4    *| 
/* 5    *| Copyright (C) 2011-2014 by Yehuda Katz
/* 6    *| 
/* 7    *| Permission is hereby granted, free of charge, to any person obtaining a copy
/* 8    *| of this software and associated documentation files (the "Software"), to deal
/* 9    *| in the Software without restriction, including without limitation the rights
/* 10   *| to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/* 11   *| copies of the Software, and to permit persons to whom the Software is
/* 12   *| furnished to do so, subject to the following conditions:
/* 13   *| 
/* 14   *| The above copyright notice and this permission notice shall be included in
/* 15   *| all copies or substantial portions of the Software.
/* 16   *| 
/* 17   *| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/* 18   *| IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/* 19   *| FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/* 20   *| AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/* 21   *| LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/* 22   *| OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/* 23   *| THE SOFTWARE.
/* 24   *| 
/* 25   *| @license
/* 26   *| */
/* 27   */ /* exported Handlebars */
/* 28   */ (function (root, factory) {
/* 29   */   if (typeof define === 'function' && define.amd) {
/* 30   */     define([], factory);
/* 31   */   } else if (typeof exports === 'object') {
/* 32   */     module.exports = factory();
/* 33   */   } else {
/* 34   */     root.Handlebars = root.Handlebars || factory();
/* 35   */   }
/* 36   */ }(this, function () {
/* 37   */ // handlebars/safe-string.js
/* 38   */ var __module4__ = (function() {
/* 39   */   "use strict";
/* 40   */   var __exports__;
/* 41   */   // Build out our basic SafeString type
/* 42   */   function SafeString(string) {
/* 43   */     this.string = string;
/* 44   */   }
/* 45   */ 
/* 46   */   SafeString.prototype.toString = function() {
/* 47   */     return "" + this.string;
/* 48   */   };
/* 49   */ 
/* 50   */   __exports__ = SafeString;

/* handlebars-v2.0.0.js */

/* 51   */   return __exports__;
/* 52   */ })();
/* 53   */ 
/* 54   */ // handlebars/utils.js
/* 55   */ var __module3__ = (function(__dependency1__) {
/* 56   */   "use strict";
/* 57   */   var __exports__ = {};
/* 58   */   /*jshint -W004 */
/* 59   */   var SafeString = __dependency1__;
/* 60   */ 
/* 61   */   var escape = {
/* 62   */     "&": "&amp;",
/* 63   */     "<": "&lt;",
/* 64   */     ">": "&gt;",
/* 65   */     '"': "&quot;",
/* 66   */     "'": "&#x27;",
/* 67   */     "`": "&#x60;"
/* 68   */   };
/* 69   */ 
/* 70   */   var badChars = /[&<>"'`]/g;
/* 71   */   var possible = /[&<>"'`]/;
/* 72   */ 
/* 73   */   function escapeChar(chr) {
/* 74   */     return escape[chr];
/* 75   */   }
/* 76   */ 
/* 77   */   function extend(obj /* , ...source */) {
/* 78   */     for (var i = 1; i < arguments.length; i++) {
/* 79   */       for (var key in arguments[i]) {
/* 80   */         if (Object.prototype.hasOwnProperty.call(arguments[i], key)) {
/* 81   */           obj[key] = arguments[i][key];
/* 82   */         }
/* 83   */       }
/* 84   */     }
/* 85   */ 
/* 86   */     return obj;
/* 87   */   }
/* 88   */ 
/* 89   */   __exports__.extend = extend;var toString = Object.prototype.toString;
/* 90   */   __exports__.toString = toString;
/* 91   */   // Sourced from lodash
/* 92   */   // https://github.com/bestiejs/lodash/blob/master/LICENSE.txt
/* 93   */   var isFunction = function(value) {
/* 94   */     return typeof value === 'function';
/* 95   */   };
/* 96   */   // fallback for older versions of Chrome and Safari
/* 97   */   /* istanbul ignore next */
/* 98   */   if (isFunction(/x/)) {
/* 99   */     isFunction = function(value) {
/* 100  */       return typeof value === 'function' && toString.call(value) === '[object Function]';

/* handlebars-v2.0.0.js */

/* 101  */     };
/* 102  */   }
/* 103  */   var isFunction;
/* 104  */   __exports__.isFunction = isFunction;
/* 105  */   /* istanbul ignore next */
/* 106  */   var isArray = Array.isArray || function(value) {
/* 107  */     return (value && typeof value === 'object') ? toString.call(value) === '[object Array]' : false;
/* 108  */   };
/* 109  */   __exports__.isArray = isArray;
/* 110  */ 
/* 111  */   function escapeExpression(string) {
/* 112  */     // don't escape SafeStrings, since they're already safe
/* 113  */     if (string instanceof SafeString) {
/* 114  */       return string.toString();
/* 115  */     } else if (string == null) {
/* 116  */       return "";
/* 117  */     } else if (!string) {
/* 118  */       return string + '';
/* 119  */     }
/* 120  */ 
/* 121  */     // Force a string conversion as this will be done by the append regardless and
/* 122  */     // the regex test will do this transparently behind the scenes, causing issues if
/* 123  */     // an object's to string has escaped characters in it.
/* 124  */     string = "" + string;
/* 125  */ 
/* 126  */     if(!possible.test(string)) { return string; }
/* 127  */     return string.replace(badChars, escapeChar);
/* 128  */   }
/* 129  */ 
/* 130  */   __exports__.escapeExpression = escapeExpression;function isEmpty(value) {
/* 131  */     if (!value && value !== 0) {
/* 132  */       return true;
/* 133  */     } else if (isArray(value) && value.length === 0) {
/* 134  */       return true;
/* 135  */     } else {
/* 136  */       return false;
/* 137  */     }
/* 138  */   }
/* 139  */ 
/* 140  */   __exports__.isEmpty = isEmpty;function appendContextPath(contextPath, id) {
/* 141  */     return (contextPath ? contextPath + '.' : '') + id;
/* 142  */   }
/* 143  */ 
/* 144  */   __exports__.appendContextPath = appendContextPath;
/* 145  */   return __exports__;
/* 146  */ })(__module4__);
/* 147  */ 
/* 148  */ // handlebars/exception.js
/* 149  */ var __module5__ = (function() {
/* 150  */   "use strict";

/* handlebars-v2.0.0.js */

/* 151  */   var __exports__;
/* 152  */ 
/* 153  */   var errorProps = ['description', 'fileName', 'lineNumber', 'message', 'name', 'number', 'stack'];
/* 154  */ 
/* 155  */   function Exception(message, node) {
/* 156  */     var line;
/* 157  */     if (node && node.firstLine) {
/* 158  */       line = node.firstLine;
/* 159  */ 
/* 160  */       message += ' - ' + line + ':' + node.firstColumn;
/* 161  */     }
/* 162  */ 
/* 163  */     var tmp = Error.prototype.constructor.call(this, message);
/* 164  */ 
/* 165  */     // Unfortunately errors are not enumerable in Chrome (at least), so `for prop in tmp` doesn't work.
/* 166  */     for (var idx = 0; idx < errorProps.length; idx++) {
/* 167  */       this[errorProps[idx]] = tmp[errorProps[idx]];
/* 168  */     }
/* 169  */ 
/* 170  */     if (line) {
/* 171  */       this.lineNumber = line;
/* 172  */       this.column = node.firstColumn;
/* 173  */     }
/* 174  */   }
/* 175  */ 
/* 176  */   Exception.prototype = new Error();
/* 177  */ 
/* 178  */   __exports__ = Exception;
/* 179  */   return __exports__;
/* 180  */ })();
/* 181  */ 
/* 182  */ // handlebars/base.js
/* 183  */ var __module2__ = (function(__dependency1__, __dependency2__) {
/* 184  */   "use strict";
/* 185  */   var __exports__ = {};
/* 186  */   var Utils = __dependency1__;
/* 187  */   var Exception = __dependency2__;
/* 188  */ 
/* 189  */   var VERSION = "2.0.0";
/* 190  */   __exports__.VERSION = VERSION;var COMPILER_REVISION = 6;
/* 191  */   __exports__.COMPILER_REVISION = COMPILER_REVISION;
/* 192  */   var REVISION_CHANGES = {
/* 193  */     1: '<= 1.0.rc.2', // 1.0.rc.2 is actually rev2 but doesn't report it
/* 194  */     2: '== 1.0.0-rc.3',
/* 195  */     3: '== 1.0.0-rc.4',
/* 196  */     4: '== 1.x.x',
/* 197  */     5: '== 2.0.0-alpha.x',
/* 198  */     6: '>= 2.0.0-beta.1'
/* 199  */   };
/* 200  */   __exports__.REVISION_CHANGES = REVISION_CHANGES;

/* handlebars-v2.0.0.js */

/* 201  */   var isArray = Utils.isArray,
/* 202  */       isFunction = Utils.isFunction,
/* 203  */       toString = Utils.toString,
/* 204  */       objectType = '[object Object]';
/* 205  */ 
/* 206  */   function HandlebarsEnvironment(helpers, partials) {
/* 207  */     this.helpers = helpers || {};
/* 208  */     this.partials = partials || {};
/* 209  */ 
/* 210  */     registerDefaultHelpers(this);
/* 211  */   }
/* 212  */ 
/* 213  */   __exports__.HandlebarsEnvironment = HandlebarsEnvironment;HandlebarsEnvironment.prototype = {
/* 214  */     constructor: HandlebarsEnvironment,
/* 215  */ 
/* 216  */     logger: logger,
/* 217  */     log: log,
/* 218  */ 
/* 219  */     registerHelper: function(name, fn) {
/* 220  */       if (toString.call(name) === objectType) {
/* 221  */         if (fn) { throw new Exception('Arg not supported with multiple helpers'); }
/* 222  */         Utils.extend(this.helpers, name);
/* 223  */       } else {
/* 224  */         this.helpers[name] = fn;
/* 225  */       }
/* 226  */     },
/* 227  */     unregisterHelper: function(name) {
/* 228  */       delete this.helpers[name];
/* 229  */     },
/* 230  */ 
/* 231  */     registerPartial: function(name, partial) {
/* 232  */       if (toString.call(name) === objectType) {
/* 233  */         Utils.extend(this.partials,  name);
/* 234  */       } else {
/* 235  */         this.partials[name] = partial;
/* 236  */       }
/* 237  */     },
/* 238  */     unregisterPartial: function(name) {
/* 239  */       delete this.partials[name];
/* 240  */     }
/* 241  */   };
/* 242  */ 
/* 243  */   function registerDefaultHelpers(instance) {
/* 244  */     instance.registerHelper('helperMissing', function(/* [args, ]options */) {
/* 245  */       if(arguments.length === 1) {
/* 246  */         // A missing field in a {{foo}} constuct.
/* 247  */         return undefined;
/* 248  */       } else {
/* 249  */         // Someone is actually trying to call something, blow up.
/* 250  */         throw new Exception("Missing helper: '" + arguments[arguments.length-1].name + "'");

/* handlebars-v2.0.0.js */

/* 251  */       }
/* 252  */     });
/* 253  */ 
/* 254  */     instance.registerHelper('blockHelperMissing', function(context, options) {
/* 255  */       var inverse = options.inverse,
/* 256  */           fn = options.fn;
/* 257  */ 
/* 258  */       if(context === true) {
/* 259  */         return fn(this);
/* 260  */       } else if(context === false || context == null) {
/* 261  */         return inverse(this);
/* 262  */       } else if (isArray(context)) {
/* 263  */         if(context.length > 0) {
/* 264  */           if (options.ids) {
/* 265  */             options.ids = [options.name];
/* 266  */           }
/* 267  */ 
/* 268  */           return instance.helpers.each(context, options);
/* 269  */         } else {
/* 270  */           return inverse(this);
/* 271  */         }
/* 272  */       } else {
/* 273  */         if (options.data && options.ids) {
/* 274  */           var data = createFrame(options.data);
/* 275  */           data.contextPath = Utils.appendContextPath(options.data.contextPath, options.name);
/* 276  */           options = {data: data};
/* 277  */         }
/* 278  */ 
/* 279  */         return fn(context, options);
/* 280  */       }
/* 281  */     });
/* 282  */ 
/* 283  */     instance.registerHelper('each', function(context, options) {
/* 284  */       if (!options) {
/* 285  */         throw new Exception('Must pass iterator to #each');
/* 286  */       }
/* 287  */ 
/* 288  */       var fn = options.fn, inverse = options.inverse;
/* 289  */       var i = 0, ret = "", data;
/* 290  */ 
/* 291  */       var contextPath;
/* 292  */       if (options.data && options.ids) {
/* 293  */         contextPath = Utils.appendContextPath(options.data.contextPath, options.ids[0]) + '.';
/* 294  */       }
/* 295  */ 
/* 296  */       if (isFunction(context)) { context = context.call(this); }
/* 297  */ 
/* 298  */       if (options.data) {
/* 299  */         data = createFrame(options.data);
/* 300  */       }

/* handlebars-v2.0.0.js */

/* 301  */ 
/* 302  */       if(context && typeof context === 'object') {
/* 303  */         if (isArray(context)) {
/* 304  */           for(var j = context.length; i<j; i++) {
/* 305  */             if (data) {
/* 306  */               data.index = i;
/* 307  */               data.first = (i === 0);
/* 308  */               data.last  = (i === (context.length-1));
/* 309  */ 
/* 310  */               if (contextPath) {
/* 311  */                 data.contextPath = contextPath + i;
/* 312  */               }
/* 313  */             }
/* 314  */             ret = ret + fn(context[i], { data: data });
/* 315  */           }
/* 316  */         } else {
/* 317  */           for(var key in context) {
/* 318  */             if(context.hasOwnProperty(key)) {
/* 319  */               if(data) {
/* 320  */                 data.key = key;
/* 321  */                 data.index = i;
/* 322  */                 data.first = (i === 0);
/* 323  */ 
/* 324  */                 if (contextPath) {
/* 325  */                   data.contextPath = contextPath + key;
/* 326  */                 }
/* 327  */               }
/* 328  */               ret = ret + fn(context[key], {data: data});
/* 329  */               i++;
/* 330  */             }
/* 331  */           }
/* 332  */         }
/* 333  */       }
/* 334  */ 
/* 335  */       if(i === 0){
/* 336  */         ret = inverse(this);
/* 337  */       }
/* 338  */ 
/* 339  */       return ret;
/* 340  */     });
/* 341  */ 
/* 342  */     instance.registerHelper('if', function(conditional, options) {
/* 343  */       if (isFunction(conditional)) { conditional = conditional.call(this); }
/* 344  */ 
/* 345  */       // Default behavior is to render the positive path if the value is truthy and not empty.
/* 346  */       // The `includeZero` option may be set to treat the condtional as purely not empty based on the
/* 347  */       // behavior of isEmpty. Effectively this determines if 0 is handled by the positive path or negative.
/* 348  */       if ((!options.hash.includeZero && !conditional) || Utils.isEmpty(conditional)) {
/* 349  */         return options.inverse(this);
/* 350  */       } else {

/* handlebars-v2.0.0.js */

/* 351  */         return options.fn(this);
/* 352  */       }
/* 353  */     });
/* 354  */ 
/* 355  */     instance.registerHelper('unless', function(conditional, options) {
/* 356  */       return instance.helpers['if'].call(this, conditional, {fn: options.inverse, inverse: options.fn, hash: options.hash});
/* 357  */     });
/* 358  */ 
/* 359  */     instance.registerHelper('with', function(context, options) {
/* 360  */       if (isFunction(context)) { context = context.call(this); }
/* 361  */ 
/* 362  */       var fn = options.fn;
/* 363  */ 
/* 364  */       if (!Utils.isEmpty(context)) {
/* 365  */         if (options.data && options.ids) {
/* 366  */           var data = createFrame(options.data);
/* 367  */           data.contextPath = Utils.appendContextPath(options.data.contextPath, options.ids[0]);
/* 368  */           options = {data:data};
/* 369  */         }
/* 370  */ 
/* 371  */         return fn(context, options);
/* 372  */       } else {
/* 373  */         return options.inverse(this);
/* 374  */       }
/* 375  */     });
/* 376  */ 
/* 377  */     instance.registerHelper('log', function(message, options) {
/* 378  */       var level = options.data && options.data.level != null ? parseInt(options.data.level, 10) : 1;
/* 379  */       instance.log(level, message);
/* 380  */     });
/* 381  */ 
/* 382  */     instance.registerHelper('lookup', function(obj, field) {
/* 383  */       return obj && obj[field];
/* 384  */     });
/* 385  */   }
/* 386  */ 
/* 387  */   var logger = {
/* 388  */     methodMap: { 0: 'debug', 1: 'info', 2: 'warn', 3: 'error' },
/* 389  */ 
/* 390  */     // State enum
/* 391  */     DEBUG: 0,
/* 392  */     INFO: 1,
/* 393  */     WARN: 2,
/* 394  */     ERROR: 3,
/* 395  */     level: 3,
/* 396  */ 
/* 397  */     // can be overridden in the host environment
/* 398  */     log: function(level, message) {
/* 399  */       if (logger.level <= level) {
/* 400  */         var method = logger.methodMap[level];

/* handlebars-v2.0.0.js */

/* 401  */         if (typeof console !== 'undefined' && console[method]) {
/* 402  */           console[method].call(console, message);
/* 403  */         }
/* 404  */       }
/* 405  */     }
/* 406  */   };
/* 407  */   __exports__.logger = logger;
/* 408  */   var log = logger.log;
/* 409  */   __exports__.log = log;
/* 410  */   var createFrame = function(object) {
/* 411  */     var frame = Utils.extend({}, object);
/* 412  */     frame._parent = object;
/* 413  */     return frame;
/* 414  */   };
/* 415  */   __exports__.createFrame = createFrame;
/* 416  */   return __exports__;
/* 417  */ })(__module3__, __module5__);
/* 418  */ 
/* 419  */ // handlebars/runtime.js
/* 420  */ var __module6__ = (function(__dependency1__, __dependency2__, __dependency3__) {
/* 421  */   "use strict";
/* 422  */   var __exports__ = {};
/* 423  */   var Utils = __dependency1__;
/* 424  */   var Exception = __dependency2__;
/* 425  */   var COMPILER_REVISION = __dependency3__.COMPILER_REVISION;
/* 426  */   var REVISION_CHANGES = __dependency3__.REVISION_CHANGES;
/* 427  */   var createFrame = __dependency3__.createFrame;
/* 428  */ 
/* 429  */   function checkRevision(compilerInfo) {
/* 430  */     var compilerRevision = compilerInfo && compilerInfo[0] || 1,
/* 431  */         currentRevision = COMPILER_REVISION;
/* 432  */ 
/* 433  */     if (compilerRevision !== currentRevision) {
/* 434  */       if (compilerRevision < currentRevision) {
/* 435  */         var runtimeVersions = REVISION_CHANGES[currentRevision],
/* 436  */             compilerVersions = REVISION_CHANGES[compilerRevision];
/* 437  */         throw new Exception("Template was precompiled with an older version of Handlebars than the current runtime. "+
/* 438  */               "Please update your precompiler to a newer version ("+runtimeVersions+") or downgrade your runtime to an older version ("+compilerVersions+").");
/* 439  */       } else {
/* 440  */         // Use the embedded version info since the runtime doesn't know about this revision yet
/* 441  */         throw new Exception("Template was precompiled with a newer version of Handlebars than the current runtime. "+
/* 442  */               "Please update your runtime to a newer version ("+compilerInfo[1]+").");
/* 443  */       }
/* 444  */     }
/* 445  */   }
/* 446  */ 
/* 447  */   __exports__.checkRevision = checkRevision;// TODO: Remove this line and break up compilePartial
/* 448  */ 
/* 449  */   function template(templateSpec, env) {
/* 450  */     /* istanbul ignore next */

/* handlebars-v2.0.0.js */

/* 451  */     if (!env) {
/* 452  */       throw new Exception("No environment passed to template");
/* 453  */     }
/* 454  */     if (!templateSpec || !templateSpec.main) {
/* 455  */       throw new Exception('Unknown template object: ' + typeof templateSpec);
/* 456  */     }
/* 457  */ 
/* 458  */     // Note: Using env.VM references rather than local var references throughout this section to allow
/* 459  */     // for external users to override these as psuedo-supported APIs.
/* 460  */     env.VM.checkRevision(templateSpec.compiler);
/* 461  */ 
/* 462  */     var invokePartialWrapper = function(partial, indent, name, context, hash, helpers, partials, data, depths) {
/* 463  */       if (hash) {
/* 464  */         context = Utils.extend({}, context, hash);
/* 465  */       }
/* 466  */ 
/* 467  */       var result = env.VM.invokePartial.call(this, partial, name, context, helpers, partials, data, depths);
/* 468  */ 
/* 469  */       if (result == null && env.compile) {
/* 470  */         var options = { helpers: helpers, partials: partials, data: data, depths: depths };
/* 471  */         partials[name] = env.compile(partial, { data: data !== undefined, compat: templateSpec.compat }, env);
/* 472  */         result = partials[name](context, options);
/* 473  */       }
/* 474  */       if (result != null) {
/* 475  */         if (indent) {
/* 476  */           var lines = result.split('\n');
/* 477  */           for (var i = 0, l = lines.length; i < l; i++) {
/* 478  */             if (!lines[i] && i + 1 === l) {
/* 479  */               break;
/* 480  */             }
/* 481  */ 
/* 482  */             lines[i] = indent + lines[i];
/* 483  */           }
/* 484  */           result = lines.join('\n');
/* 485  */         }
/* 486  */         return result;
/* 487  */       } else {
/* 488  */         throw new Exception("The partial " + name + " could not be compiled when running in runtime-only mode");
/* 489  */       }
/* 490  */     };
/* 491  */ 
/* 492  */     // Just add water
/* 493  */     var container = {
/* 494  */       lookup: function(depths, name) {
/* 495  */         var len = depths.length;
/* 496  */         for (var i = 0; i < len; i++) {
/* 497  */           if (depths[i] && depths[i][name] != null) {
/* 498  */             return depths[i][name];
/* 499  */           }
/* 500  */         }

/* handlebars-v2.0.0.js */

/* 501  */       },
/* 502  */       lambda: function(current, context) {
/* 503  */         return typeof current === 'function' ? current.call(context) : current;
/* 504  */       },
/* 505  */ 
/* 506  */       escapeExpression: Utils.escapeExpression,
/* 507  */       invokePartial: invokePartialWrapper,
/* 508  */ 
/* 509  */       fn: function(i) {
/* 510  */         return templateSpec[i];
/* 511  */       },
/* 512  */ 
/* 513  */       programs: [],
/* 514  */       program: function(i, data, depths) {
/* 515  */         var programWrapper = this.programs[i],
/* 516  */             fn = this.fn(i);
/* 517  */         if (data || depths) {
/* 518  */           programWrapper = program(this, i, fn, data, depths);
/* 519  */         } else if (!programWrapper) {
/* 520  */           programWrapper = this.programs[i] = program(this, i, fn);
/* 521  */         }
/* 522  */         return programWrapper;
/* 523  */       },
/* 524  */ 
/* 525  */       data: function(data, depth) {
/* 526  */         while (data && depth--) {
/* 527  */           data = data._parent;
/* 528  */         }
/* 529  */         return data;
/* 530  */       },
/* 531  */       merge: function(param, common) {
/* 532  */         var ret = param || common;
/* 533  */ 
/* 534  */         if (param && common && (param !== common)) {
/* 535  */           ret = Utils.extend({}, common, param);
/* 536  */         }
/* 537  */ 
/* 538  */         return ret;
/* 539  */       },
/* 540  */ 
/* 541  */       noop: env.VM.noop,
/* 542  */       compilerInfo: templateSpec.compiler
/* 543  */     };
/* 544  */ 
/* 545  */     var ret = function(context, options) {
/* 546  */       options = options || {};
/* 547  */       var data = options.data;
/* 548  */ 
/* 549  */       ret._setup(options);
/* 550  */       if (!options.partial && templateSpec.useData) {

/* handlebars-v2.0.0.js */

/* 551  */         data = initData(context, data);
/* 552  */       }
/* 553  */       var depths;
/* 554  */       if (templateSpec.useDepths) {
/* 555  */         depths = options.depths ? [context].concat(options.depths) : [context];
/* 556  */       }
/* 557  */ 
/* 558  */       return templateSpec.main.call(container, context, container.helpers, container.partials, data, depths);
/* 559  */     };
/* 560  */     ret.isTop = true;
/* 561  */ 
/* 562  */     ret._setup = function(options) {
/* 563  */       if (!options.partial) {
/* 564  */         container.helpers = container.merge(options.helpers, env.helpers);
/* 565  */ 
/* 566  */         if (templateSpec.usePartial) {
/* 567  */           container.partials = container.merge(options.partials, env.partials);
/* 568  */         }
/* 569  */       } else {
/* 570  */         container.helpers = options.helpers;
/* 571  */         container.partials = options.partials;
/* 572  */       }
/* 573  */     };
/* 574  */ 
/* 575  */     ret._child = function(i, data, depths) {
/* 576  */       if (templateSpec.useDepths && !depths) {
/* 577  */         throw new Exception('must pass parent depths');
/* 578  */       }
/* 579  */ 
/* 580  */       return program(container, i, templateSpec[i], data, depths);
/* 581  */     };
/* 582  */     return ret;
/* 583  */   }
/* 584  */ 
/* 585  */   __exports__.template = template;function program(container, i, fn, data, depths) {
/* 586  */     var prog = function(context, options) {
/* 587  */       options = options || {};
/* 588  */ 
/* 589  */       return fn.call(container, context, container.helpers, container.partials, options.data || data, depths && [context].concat(depths));
/* 590  */     };
/* 591  */     prog.program = i;
/* 592  */     prog.depth = depths ? depths.length : 0;
/* 593  */     return prog;
/* 594  */   }
/* 595  */ 
/* 596  */   __exports__.program = program;function invokePartial(partial, name, context, helpers, partials, data, depths) {
/* 597  */     var options = { partial: true, helpers: helpers, partials: partials, data: data, depths: depths };
/* 598  */ 
/* 599  */     if(partial === undefined) {
/* 600  */       throw new Exception("The partial " + name + " could not be found");

/* handlebars-v2.0.0.js */

/* 601  */     } else if(partial instanceof Function) {
/* 602  */       return partial(context, options);
/* 603  */     }
/* 604  */   }
/* 605  */ 
/* 606  */   __exports__.invokePartial = invokePartial;function noop() { return ""; }
/* 607  */ 
/* 608  */   __exports__.noop = noop;function initData(context, data) {
/* 609  */     if (!data || !('root' in data)) {
/* 610  */       data = data ? createFrame(data) : {};
/* 611  */       data.root = context;
/* 612  */     }
/* 613  */     return data;
/* 614  */   }
/* 615  */   return __exports__;
/* 616  */ })(__module3__, __module5__, __module2__);
/* 617  */ 
/* 618  */ // handlebars.runtime.js
/* 619  */ var __module1__ = (function(__dependency1__, __dependency2__, __dependency3__, __dependency4__, __dependency5__) {
/* 620  */   "use strict";
/* 621  */   var __exports__;
/* 622  */   /*globals Handlebars: true */
/* 623  */   var base = __dependency1__;
/* 624  */ 
/* 625  */   // Each of these augment the Handlebars object. No need to setup here.
/* 626  */   // (This is done to easily share code between commonjs and browse envs)
/* 627  */   var SafeString = __dependency2__;
/* 628  */   var Exception = __dependency3__;
/* 629  */   var Utils = __dependency4__;
/* 630  */   var runtime = __dependency5__;
/* 631  */ 
/* 632  */   // For compatibility and usage outside of module systems, make the Handlebars object a namespace
/* 633  */   var create = function() {
/* 634  */     var hb = new base.HandlebarsEnvironment();
/* 635  */ 
/* 636  */     Utils.extend(hb, base);
/* 637  */     hb.SafeString = SafeString;
/* 638  */     hb.Exception = Exception;
/* 639  */     hb.Utils = Utils;
/* 640  */     hb.escapeExpression = Utils.escapeExpression;
/* 641  */ 
/* 642  */     hb.VM = runtime;
/* 643  */     hb.template = function(spec) {
/* 644  */       return runtime.template(spec, hb);
/* 645  */     };
/* 646  */ 
/* 647  */     return hb;
/* 648  */   };
/* 649  */ 
/* 650  */   var Handlebars = create();

/* handlebars-v2.0.0.js */

/* 651  */   Handlebars.create = create;
/* 652  */ 
/* 653  */   Handlebars['default'] = Handlebars;
/* 654  */ 
/* 655  */   __exports__ = Handlebars;
/* 656  */   return __exports__;
/* 657  */ })(__module2__, __module4__, __module5__, __module3__, __module6__);
/* 658  */ 
/* 659  */ // handlebars/compiler/ast.js
/* 660  */ var __module7__ = (function(__dependency1__) {
/* 661  */   "use strict";
/* 662  */   var __exports__;
/* 663  */   var Exception = __dependency1__;
/* 664  */ 
/* 665  */   function LocationInfo(locInfo) {
/* 666  */     locInfo = locInfo || {};
/* 667  */     this.firstLine   = locInfo.first_line;
/* 668  */     this.firstColumn = locInfo.first_column;
/* 669  */     this.lastColumn  = locInfo.last_column;
/* 670  */     this.lastLine    = locInfo.last_line;
/* 671  */   }
/* 672  */ 
/* 673  */   var AST = {
/* 674  */     ProgramNode: function(statements, strip, locInfo) {
/* 675  */       LocationInfo.call(this, locInfo);
/* 676  */       this.type = "program";
/* 677  */       this.statements = statements;
/* 678  */       this.strip = strip;
/* 679  */     },
/* 680  */ 
/* 681  */     MustacheNode: function(rawParams, hash, open, strip, locInfo) {
/* 682  */       LocationInfo.call(this, locInfo);
/* 683  */       this.type = "mustache";
/* 684  */       this.strip = strip;
/* 685  */ 
/* 686  */       // Open may be a string parsed from the parser or a passed boolean flag
/* 687  */       if (open != null && open.charAt) {
/* 688  */         // Must use charAt to support IE pre-10
/* 689  */         var escapeFlag = open.charAt(3) || open.charAt(2);
/* 690  */         this.escaped = escapeFlag !== '{' && escapeFlag !== '&';
/* 691  */       } else {
/* 692  */         this.escaped = !!open;
/* 693  */       }
/* 694  */ 
/* 695  */       if (rawParams instanceof AST.SexprNode) {
/* 696  */         this.sexpr = rawParams;
/* 697  */       } else {
/* 698  */         // Support old AST API
/* 699  */         this.sexpr = new AST.SexprNode(rawParams, hash);
/* 700  */       }

/* handlebars-v2.0.0.js */

/* 701  */ 
/* 702  */       // Support old AST API that stored this info in MustacheNode
/* 703  */       this.id = this.sexpr.id;
/* 704  */       this.params = this.sexpr.params;
/* 705  */       this.hash = this.sexpr.hash;
/* 706  */       this.eligibleHelper = this.sexpr.eligibleHelper;
/* 707  */       this.isHelper = this.sexpr.isHelper;
/* 708  */     },
/* 709  */ 
/* 710  */     SexprNode: function(rawParams, hash, locInfo) {
/* 711  */       LocationInfo.call(this, locInfo);
/* 712  */ 
/* 713  */       this.type = "sexpr";
/* 714  */       this.hash = hash;
/* 715  */ 
/* 716  */       var id = this.id = rawParams[0];
/* 717  */       var params = this.params = rawParams.slice(1);
/* 718  */ 
/* 719  */       // a mustache is definitely a helper if:
/* 720  */       // * it is an eligible helper, and
/* 721  */       // * it has at least one parameter or hash segment
/* 722  */       this.isHelper = !!(params.length || hash);
/* 723  */ 
/* 724  */       // a mustache is an eligible helper if:
/* 725  */       // * its id is simple (a single part, not `this` or `..`)
/* 726  */       this.eligibleHelper = this.isHelper || id.isSimple;
/* 727  */ 
/* 728  */       // if a mustache is an eligible helper but not a definite
/* 729  */       // helper, it is ambiguous, and will be resolved in a later
/* 730  */       // pass or at runtime.
/* 731  */     },
/* 732  */ 
/* 733  */     PartialNode: function(partialName, context, hash, strip, locInfo) {
/* 734  */       LocationInfo.call(this, locInfo);
/* 735  */       this.type         = "partial";
/* 736  */       this.partialName  = partialName;
/* 737  */       this.context      = context;
/* 738  */       this.hash = hash;
/* 739  */       this.strip = strip;
/* 740  */ 
/* 741  */       this.strip.inlineStandalone = true;
/* 742  */     },
/* 743  */ 
/* 744  */     BlockNode: function(mustache, program, inverse, strip, locInfo) {
/* 745  */       LocationInfo.call(this, locInfo);
/* 746  */ 
/* 747  */       this.type = 'block';
/* 748  */       this.mustache = mustache;
/* 749  */       this.program  = program;
/* 750  */       this.inverse  = inverse;

/* handlebars-v2.0.0.js */

/* 751  */       this.strip = strip;
/* 752  */ 
/* 753  */       if (inverse && !program) {
/* 754  */         this.isInverse = true;
/* 755  */       }
/* 756  */     },
/* 757  */ 
/* 758  */     RawBlockNode: function(mustache, content, close, locInfo) {
/* 759  */       LocationInfo.call(this, locInfo);
/* 760  */ 
/* 761  */       if (mustache.sexpr.id.original !== close) {
/* 762  */         throw new Exception(mustache.sexpr.id.original + " doesn't match " + close, this);
/* 763  */       }
/* 764  */ 
/* 765  */       content = new AST.ContentNode(content, locInfo);
/* 766  */ 
/* 767  */       this.type = 'block';
/* 768  */       this.mustache = mustache;
/* 769  */       this.program = new AST.ProgramNode([content], {}, locInfo);
/* 770  */     },
/* 771  */ 
/* 772  */     ContentNode: function(string, locInfo) {
/* 773  */       LocationInfo.call(this, locInfo);
/* 774  */       this.type = "content";
/* 775  */       this.original = this.string = string;
/* 776  */     },
/* 777  */ 
/* 778  */     HashNode: function(pairs, locInfo) {
/* 779  */       LocationInfo.call(this, locInfo);
/* 780  */       this.type = "hash";
/* 781  */       this.pairs = pairs;
/* 782  */     },
/* 783  */ 
/* 784  */     IdNode: function(parts, locInfo) {
/* 785  */       LocationInfo.call(this, locInfo);
/* 786  */       this.type = "ID";
/* 787  */ 
/* 788  */       var original = "",
/* 789  */           dig = [],
/* 790  */           depth = 0,
/* 791  */           depthString = '';
/* 792  */ 
/* 793  */       for(var i=0,l=parts.length; i<l; i++) {
/* 794  */         var part = parts[i].part;
/* 795  */         original += (parts[i].separator || '') + part;
/* 796  */ 
/* 797  */         if (part === ".." || part === "." || part === "this") {
/* 798  */           if (dig.length > 0) {
/* 799  */             throw new Exception("Invalid path: " + original, this);
/* 800  */           } else if (part === "..") {

/* handlebars-v2.0.0.js */

/* 801  */             depth++;
/* 802  */             depthString += '../';
/* 803  */           } else {
/* 804  */             this.isScoped = true;
/* 805  */           }
/* 806  */         } else {
/* 807  */           dig.push(part);
/* 808  */         }
/* 809  */       }
/* 810  */ 
/* 811  */       this.original = original;
/* 812  */       this.parts    = dig;
/* 813  */       this.string   = dig.join('.');
/* 814  */       this.depth    = depth;
/* 815  */       this.idName   = depthString + this.string;
/* 816  */ 
/* 817  */       // an ID is simple if it only has one part, and that part is not
/* 818  */       // `..` or `this`.
/* 819  */       this.isSimple = parts.length === 1 && !this.isScoped && depth === 0;
/* 820  */ 
/* 821  */       this.stringModeValue = this.string;
/* 822  */     },
/* 823  */ 
/* 824  */     PartialNameNode: function(name, locInfo) {
/* 825  */       LocationInfo.call(this, locInfo);
/* 826  */       this.type = "PARTIAL_NAME";
/* 827  */       this.name = name.original;
/* 828  */     },
/* 829  */ 
/* 830  */     DataNode: function(id, locInfo) {
/* 831  */       LocationInfo.call(this, locInfo);
/* 832  */       this.type = "DATA";
/* 833  */       this.id = id;
/* 834  */       this.stringModeValue = id.stringModeValue;
/* 835  */       this.idName = '@' + id.stringModeValue;
/* 836  */     },
/* 837  */ 
/* 838  */     StringNode: function(string, locInfo) {
/* 839  */       LocationInfo.call(this, locInfo);
/* 840  */       this.type = "STRING";
/* 841  */       this.original =
/* 842  */         this.string =
/* 843  */         this.stringModeValue = string;
/* 844  */     },
/* 845  */ 
/* 846  */     NumberNode: function(number, locInfo) {
/* 847  */       LocationInfo.call(this, locInfo);
/* 848  */       this.type = "NUMBER";
/* 849  */       this.original =
/* 850  */         this.number = number;

/* handlebars-v2.0.0.js */

/* 851  */       this.stringModeValue = Number(number);
/* 852  */     },
/* 853  */ 
/* 854  */     BooleanNode: function(bool, locInfo) {
/* 855  */       LocationInfo.call(this, locInfo);
/* 856  */       this.type = "BOOLEAN";
/* 857  */       this.bool = bool;
/* 858  */       this.stringModeValue = bool === "true";
/* 859  */     },
/* 860  */ 
/* 861  */     CommentNode: function(comment, locInfo) {
/* 862  */       LocationInfo.call(this, locInfo);
/* 863  */       this.type = "comment";
/* 864  */       this.comment = comment;
/* 865  */ 
/* 866  */       this.strip = {
/* 867  */         inlineStandalone: true
/* 868  */       };
/* 869  */     }
/* 870  */   };
/* 871  */ 
/* 872  */ 
/* 873  */   // Must be exported as an object rather than the root of the module as the jison lexer
/* 874  */   // most modify the object to operate properly.
/* 875  */   __exports__ = AST;
/* 876  */   return __exports__;
/* 877  */ })(__module5__);
/* 878  */ 
/* 879  */ // handlebars/compiler/parser.js
/* 880  */ var __module9__ = (function() {
/* 881  */   "use strict";
/* 882  */   var __exports__;
/* 883  */   /* jshint ignore:start */
/* 884  */   /* istanbul ignore next */
/* 885  */   /* Jison generated parser */
/* 886  */   var handlebars = (function(){
/* 887  */   var parser = {trace: function trace() { },
/* 888  */   yy: {},
/* 889  */   symbols_: {"error":2,"root":3,"program":4,"EOF":5,"program_repetition0":6,"statement":7,"mustache":8,"block":9,"rawBlock":10,"partial":11,"CONTENT":12,"COMMENT":13,"openRawBlock":14,"END_RAW_BLOCK":15,"OPEN_RAW_BLOCK":16,"sexpr":17,"CLOSE_RAW_BLOCK":18,"openBlock":19,"block_option0":20,"closeBlock":21,"openInverse":22,"block_option1":23,"OPEN_BLOCK":24,"CLOSE":25,"OPEN_INVERSE":26,"inverseAndProgram":27,"INVERSE":28,"OPEN_ENDBLOCK":29,"path":30,"OPEN":31,"OPEN_UNESCAPED":32,"CLOSE_UNESCAPED":33,"OPEN_PARTIAL":34,"partialName":35,"param":36,"partial_option0":37,"partial_option1":38,"sexpr_repetition0":39,"sexpr_option0":40,"dataName":41,"STRING":42,"NUMBER":43,"BOOLEAN":44,"OPEN_SEXPR":45,"CLOSE_SEXPR":46,"hash":47,"hash_repetition_plus0":48,"hashSegment":49,"ID":50,"EQUALS":51,"DATA":52,"pathSegments":53,"SEP":54,"$accept":0,"$end":1},
/* 890  */   terminals_: {2:"error",5:"EOF",12:"CONTENT",13:"COMMENT",15:"END_RAW_BLOCK",16:"OPEN_RAW_BLOCK",18:"CLOSE_RAW_BLOCK",24:"OPEN_BLOCK",25:"CLOSE",26:"OPEN_INVERSE",28:"INVERSE",29:"OPEN_ENDBLOCK",31:"OPEN",32:"OPEN_UNESCAPED",33:"CLOSE_UNESCAPED",34:"OPEN_PARTIAL",42:"STRING",43:"NUMBER",44:"BOOLEAN",45:"OPEN_SEXPR",46:"CLOSE_SEXPR",50:"ID",51:"EQUALS",52:"DATA",54:"SEP"},
/* 891  */   productions_: [0,[3,2],[4,1],[7,1],[7,1],[7,1],[7,1],[7,1],[7,1],[10,3],[14,3],[9,4],[9,4],[19,3],[22,3],[27,2],[21,3],[8,3],[8,3],[11,5],[11,4],[17,3],[17,1],[36,1],[36,1],[36,1],[36,1],[36,1],[36,3],[47,1],[49,3],[35,1],[35,1],[35,1],[41,2],[30,1],[53,3],[53,1],[6,0],[6,2],[20,0],[20,1],[23,0],[23,1],[37,0],[37,1],[38,0],[38,1],[39,0],[39,2],[40,0],[40,1],[48,1],[48,2]],
/* 892  */   performAction: function anonymous(yytext,yyleng,yylineno,yy,yystate,$$,_$) {
/* 893  */ 
/* 894  */   var $0 = $$.length - 1;
/* 895  */   switch (yystate) {
/* 896  */   case 1: yy.prepareProgram($$[$0-1].statements, true); return $$[$0-1]; 
/* 897  */   break;
/* 898  */   case 2:this.$ = new yy.ProgramNode(yy.prepareProgram($$[$0]), {}, this._$);
/* 899  */   break;
/* 900  */   case 3:this.$ = $$[$0];

/* handlebars-v2.0.0.js */

/* 901  */   break;
/* 902  */   case 4:this.$ = $$[$0];
/* 903  */   break;
/* 904  */   case 5:this.$ = $$[$0];
/* 905  */   break;
/* 906  */   case 6:this.$ = $$[$0];
/* 907  */   break;
/* 908  */   case 7:this.$ = new yy.ContentNode($$[$0], this._$);
/* 909  */   break;
/* 910  */   case 8:this.$ = new yy.CommentNode($$[$0], this._$);
/* 911  */   break;
/* 912  */   case 9:this.$ = new yy.RawBlockNode($$[$0-2], $$[$0-1], $$[$0], this._$);
/* 913  */   break;
/* 914  */   case 10:this.$ = new yy.MustacheNode($$[$0-1], null, '', '', this._$);
/* 915  */   break;
/* 916  */   case 11:this.$ = yy.prepareBlock($$[$0-3], $$[$0-2], $$[$0-1], $$[$0], false, this._$);
/* 917  */   break;
/* 918  */   case 12:this.$ = yy.prepareBlock($$[$0-3], $$[$0-2], $$[$0-1], $$[$0], true, this._$);
/* 919  */   break;
/* 920  */   case 13:this.$ = new yy.MustacheNode($$[$0-1], null, $$[$0-2], yy.stripFlags($$[$0-2], $$[$0]), this._$);
/* 921  */   break;
/* 922  */   case 14:this.$ = new yy.MustacheNode($$[$0-1], null, $$[$0-2], yy.stripFlags($$[$0-2], $$[$0]), this._$);
/* 923  */   break;
/* 924  */   case 15:this.$ = { strip: yy.stripFlags($$[$0-1], $$[$0-1]), program: $$[$0] };
/* 925  */   break;
/* 926  */   case 16:this.$ = {path: $$[$0-1], strip: yy.stripFlags($$[$0-2], $$[$0])};
/* 927  */   break;
/* 928  */   case 17:this.$ = new yy.MustacheNode($$[$0-1], null, $$[$0-2], yy.stripFlags($$[$0-2], $$[$0]), this._$);
/* 929  */   break;
/* 930  */   case 18:this.$ = new yy.MustacheNode($$[$0-1], null, $$[$0-2], yy.stripFlags($$[$0-2], $$[$0]), this._$);
/* 931  */   break;
/* 932  */   case 19:this.$ = new yy.PartialNode($$[$0-3], $$[$0-2], $$[$0-1], yy.stripFlags($$[$0-4], $$[$0]), this._$);
/* 933  */   break;
/* 934  */   case 20:this.$ = new yy.PartialNode($$[$0-2], undefined, $$[$0-1], yy.stripFlags($$[$0-3], $$[$0]), this._$);
/* 935  */   break;
/* 936  */   case 21:this.$ = new yy.SexprNode([$$[$0-2]].concat($$[$0-1]), $$[$0], this._$);
/* 937  */   break;
/* 938  */   case 22:this.$ = new yy.SexprNode([$$[$0]], null, this._$);
/* 939  */   break;
/* 940  */   case 23:this.$ = $$[$0];
/* 941  */   break;
/* 942  */   case 24:this.$ = new yy.StringNode($$[$0], this._$);
/* 943  */   break;
/* 944  */   case 25:this.$ = new yy.NumberNode($$[$0], this._$);
/* 945  */   break;
/* 946  */   case 26:this.$ = new yy.BooleanNode($$[$0], this._$);
/* 947  */   break;
/* 948  */   case 27:this.$ = $$[$0];
/* 949  */   break;
/* 950  */   case 28:$$[$0-1].isHelper = true; this.$ = $$[$0-1];

/* handlebars-v2.0.0.js */

/* 951  */   break;
/* 952  */   case 29:this.$ = new yy.HashNode($$[$0], this._$);
/* 953  */   break;
/* 954  */   case 30:this.$ = [$$[$0-2], $$[$0]];
/* 955  */   break;
/* 956  */   case 31:this.$ = new yy.PartialNameNode($$[$0], this._$);
/* 957  */   break;
/* 958  */   case 32:this.$ = new yy.PartialNameNode(new yy.StringNode($$[$0], this._$), this._$);
/* 959  */   break;
/* 960  */   case 33:this.$ = new yy.PartialNameNode(new yy.NumberNode($$[$0], this._$));
/* 961  */   break;
/* 962  */   case 34:this.$ = new yy.DataNode($$[$0], this._$);
/* 963  */   break;
/* 964  */   case 35:this.$ = new yy.IdNode($$[$0], this._$);
/* 965  */   break;
/* 966  */   case 36: $$[$0-2].push({part: $$[$0], separator: $$[$0-1]}); this.$ = $$[$0-2]; 
/* 967  */   break;
/* 968  */   case 37:this.$ = [{part: $$[$0]}];
/* 969  */   break;
/* 970  */   case 38:this.$ = [];
/* 971  */   break;
/* 972  */   case 39:$$[$0-1].push($$[$0]);
/* 973  */   break;
/* 974  */   case 48:this.$ = [];
/* 975  */   break;
/* 976  */   case 49:$$[$0-1].push($$[$0]);
/* 977  */   break;
/* 978  */   case 52:this.$ = [$$[$0]];
/* 979  */   break;
/* 980  */   case 53:$$[$0-1].push($$[$0]);
/* 981  */   break;
/* 982  */   }
/* 983  */   },
/* 984  */   table: [{3:1,4:2,5:[2,38],6:3,12:[2,38],13:[2,38],16:[2,38],24:[2,38],26:[2,38],31:[2,38],32:[2,38],34:[2,38]},{1:[3]},{5:[1,4]},{5:[2,2],7:5,8:6,9:7,10:8,11:9,12:[1,10],13:[1,11],14:16,16:[1,20],19:14,22:15,24:[1,18],26:[1,19],28:[2,2],29:[2,2],31:[1,12],32:[1,13],34:[1,17]},{1:[2,1]},{5:[2,39],12:[2,39],13:[2,39],16:[2,39],24:[2,39],26:[2,39],28:[2,39],29:[2,39],31:[2,39],32:[2,39],34:[2,39]},{5:[2,3],12:[2,3],13:[2,3],16:[2,3],24:[2,3],26:[2,3],28:[2,3],29:[2,3],31:[2,3],32:[2,3],34:[2,3]},{5:[2,4],12:[2,4],13:[2,4],16:[2,4],24:[2,4],26:[2,4],28:[2,4],29:[2,4],31:[2,4],32:[2,4],34:[2,4]},{5:[2,5],12:[2,5],13:[2,5],16:[2,5],24:[2,5],26:[2,5],28:[2,5],29:[2,5],31:[2,5],32:[2,5],34:[2,5]},{5:[2,6],12:[2,6],13:[2,6],16:[2,6],24:[2,6],26:[2,6],28:[2,6],29:[2,6],31:[2,6],32:[2,6],34:[2,6]},{5:[2,7],12:[2,7],13:[2,7],16:[2,7],24:[2,7],26:[2,7],28:[2,7],29:[2,7],31:[2,7],32:[2,7],34:[2,7]},{5:[2,8],12:[2,8],13:[2,8],16:[2,8],24:[2,8],26:[2,8],28:[2,8],29:[2,8],31:[2,8],32:[2,8],34:[2,8]},{17:21,30:22,41:23,50:[1,26],52:[1,25],53:24},{17:27,30:22,41:23,50:[1,26],52:[1,25],53:24},{4:28,6:3,12:[2,38],13:[2,38],16:[2,38],24:[2,38],26:[2,38],28:[2,38],29:[2,38],31:[2,38],32:[2,38],34:[2,38]},{4:29,6:3,12:[2,38],13:[2,38],16:[2,38],24:[2,38],26:[2,38],28:[2,38],29:[2,38],31:[2,38],32:[2,38],34:[2,38]},{12:[1,30]},{30:32,35:31,42:[1,33],43:[1,34],50:[1,26],53:24},{17:35,30:22,41:23,50:[1,26],52:[1,25],53:24},{17:36,30:22,41:23,50:[1,26],52:[1,25],53:24},{17:37,30:22,41:23,50:[1,26],52:[1,25],53:24},{25:[1,38]},{18:[2,48],25:[2,48],33:[2,48],39:39,42:[2,48],43:[2,48],44:[2,48],45:[2,48],46:[2,48],50:[2,48],52:[2,48]},{18:[2,22],25:[2,22],33:[2,22],46:[2,22]},{18:[2,35],25:[2,35],33:[2,35],42:[2,35],43:[2,35],44:[2,35],45:[2,35],46:[2,35],50:[2,35],52:[2,35],54:[1,40]},{30:41,50:[1,26],53:24},{18:[2,37],25:[2,37],33:[2,37],42:[2,37],43:[2,37],44:[2,37],45:[2,37],46:[2,37],50:[2,37],52:[2,37],54:[2,37]},{33:[1,42]},{20:43,27:44,28:[1,45],29:[2,40]},{23:46,27:47,28:[1,45],29:[2,42]},{15:[1,48]},{25:[2,46],30:51,36:49,38:50,41:55,42:[1,52],43:[1,53],44:[1,54],45:[1,56],47:57,48:58,49:60,50:[1,59],52:[1,25],53:24},{25:[2,31],42:[2,31],43:[2,31],44:[2,31],45:[2,31],50:[2,31],52:[2,31]},{25:[2,32],42:[2,32],43:[2,32],44:[2,32],45:[2,32],50:[2,32],52:[2,32]},{25:[2,33],42:[2,33],43:[2,33],44:[2,33],45:[2,33],50:[2,33],52:[2,33]},{25:[1,61]},{25:[1,62]},{18:[1,63]},{5:[2,17],12:[2,17],13:[2,17],16:[2,17],24:[2,17],26:[2,17],28:[2,17],29:[2,17],31:[2,17],32:[2,17],34:[2,17]},{18:[2,50],25:[2,50],30:51,33:[2,50],36:65,40:64,41:55,42:[1,52],43:[1,53],44:[1,54],45:[1,56],46:[2,50],47:66,48:58,49:60,50:[1,59],52:[1,25],53:24},{50:[1,67]},{18:[2,34],25:[2,34],33:[2,34],42:[2,34],43:[2,34],44:[2,34],45:[2,34],46:[2,34],50:[2,34],52:[2,34]},{5:[2,18],12:[2,18],13:[2,18],16:[2,18],24:[2,18],26:[2,18],28:[2,18],29:[2,18],31:[2,18],32:[2,18],34:[2,18]},{21:68,29:[1,69]},{29:[2,41]},{4:70,6:3,12:[2,38],13:[2,38],16:[2,38],24:[2,38],26:[2,38],29:[2,38],31:[2,38],32:[2,38],34:[2,38]},{21:71,29:[1,69]},{29:[2,43]},{5:[2,9],12:[2,9],13:[2,9],16:[2,9],24:[2,9],26:[2,9],28:[2,9],29:[2,9],31:[2,9],32:[2,9],34:[2,9]},{25:[2,44],37:72,47:73,48:58,49:60,50:[1,74]},{25:[1,75]},{18:[2,23],25:[2,23],33:[2,23],42:[2,23],43:[2,23],44:[2,23],45:[2,23],46:[2,23],50:[2,23],52:[2,23]},{18:[2,24],25:[2,24],33:[2,24],42:[2,24],43:[2,24],44:[2,24],45:[2,24],46:[2,24],50:[2,24],52:[2,24]},{18:[2,25],25:[2,25],33:[2,25],42:[2,25],43:[2,25],44:[2,25],45:[2,25],46:[2,25],50:[2,25],52:[2,25]},{18:[2,26],25:[2,26],33:[2,26],42:[2,26],43:[2,26],44:[2,26],45:[2,26],46:[2,26],50:[2,26],52:[2,26]},{18:[2,27],25:[2,27],33:[2,27],42:[2,27],43:[2,27],44:[2,27],45:[2,27],46:[2,27],50:[2,27],52:[2,27]},{17:76,30:22,41:23,50:[1,26],52:[1,25],53:24},{25:[2,47]},{18:[2,29],25:[2,29],33:[2,29],46:[2,29],49:77,50:[1,74]},{18:[2,37],25:[2,37],33:[2,37],42:[2,37],43:[2,37],44:[2,37],45:[2,37],46:[2,37],50:[2,37],51:[1,78],52:[2,37],54:[2,37]},{18:[2,52],25:[2,52],33:[2,52],46:[2,52],50:[2,52]},{12:[2,13],13:[2,13],16:[2,13],24:[2,13],26:[2,13],28:[2,13],29:[2,13],31:[2,13],32:[2,13],34:[2,13]},{12:[2,14],13:[2,14],16:[2,14],24:[2,14],26:[2,14],28:[2,14],29:[2,14],31:[2,14],32:[2,14],34:[2,14]},{12:[2,10]},{18:[2,21],25:[2,21],33:[2,21],46:[2,21]},{18:[2,49],25:[2,49],33:[2,49],42:[2,49],43:[2,49],44:[2,49],45:[2,49],46:[2,49],50:[2,49],52:[2,49]},{18:[2,51],25:[2,51],33:[2,51],46:[2,51]},{18:[2,36],25:[2,36],33:[2,36],42:[2,36],43:[2,36],44:[2,36],45:[2,36],46:[2,36],50:[2,36],52:[2,36],54:[2,36]},{5:[2,11],12:[2,11],13:[2,11],16:[2,11],24:[2,11],26:[2,11],28:[2,11],29:[2,11],31:[2,11],32:[2,11],34:[2,11]},{30:79,50:[1,26],53:24},{29:[2,15]},{5:[2,12],12:[2,12],13:[2,12],16:[2,12],24:[2,12],26:[2,12],28:[2,12],29:[2,12],31:[2,12],32:[2,12],34:[2,12]},{25:[1,80]},{25:[2,45]},{51:[1,78]},{5:[2,20],12:[2,20],13:[2,20],16:[2,20],24:[2,20],26:[2,20],28:[2,20],29:[2,20],31:[2,20],32:[2,20],34:[2,20]},{46:[1,81]},{18:[2,53],25:[2,53],33:[2,53],46:[2,53],50:[2,53]},{30:51,36:82,41:55,42:[1,52],43:[1,53],44:[1,54],45:[1,56],50:[1,26],52:[1,25],53:24},{25:[1,83]},{5:[2,19],12:[2,19],13:[2,19],16:[2,19],24:[2,19],26:[2,19],28:[2,19],29:[2,19],31:[2,19],32:[2,19],34:[2,19]},{18:[2,28],25:[2,28],33:[2,28],42:[2,28],43:[2,28],44:[2,28],45:[2,28],46:[2,28],50:[2,28],52:[2,28]},{18:[2,30],25:[2,30],33:[2,30],46:[2,30],50:[2,30]},{5:[2,16],12:[2,16],13:[2,16],16:[2,16],24:[2,16],26:[2,16],28:[2,16],29:[2,16],31:[2,16],32:[2,16],34:[2,16]}],
/* 985  */   defaultActions: {4:[2,1],44:[2,41],47:[2,43],57:[2,47],63:[2,10],70:[2,15],73:[2,45]},
/* 986  */   parseError: function parseError(str, hash) {
/* 987  */       throw new Error(str);
/* 988  */   },
/* 989  */   parse: function parse(input) {
/* 990  */       var self = this, stack = [0], vstack = [null], lstack = [], table = this.table, yytext = "", yylineno = 0, yyleng = 0, recovering = 0, TERROR = 2, EOF = 1;
/* 991  */       this.lexer.setInput(input);
/* 992  */       this.lexer.yy = this.yy;
/* 993  */       this.yy.lexer = this.lexer;
/* 994  */       this.yy.parser = this;
/* 995  */       if (typeof this.lexer.yylloc == "undefined")
/* 996  */           this.lexer.yylloc = {};
/* 997  */       var yyloc = this.lexer.yylloc;
/* 998  */       lstack.push(yyloc);
/* 999  */       var ranges = this.lexer.options && this.lexer.options.ranges;
/* 1000 */       if (typeof this.yy.parseError === "function")

/* handlebars-v2.0.0.js */

/* 1001 */           this.parseError = this.yy.parseError;
/* 1002 */       function popStack(n) {
/* 1003 */           stack.length = stack.length - 2 * n;
/* 1004 */           vstack.length = vstack.length - n;
/* 1005 */           lstack.length = lstack.length - n;
/* 1006 */       }
/* 1007 */       function lex() {
/* 1008 */           var token;
/* 1009 */           token = self.lexer.lex() || 1;
/* 1010 */           if (typeof token !== "number") {
/* 1011 */               token = self.symbols_[token] || token;
/* 1012 */           }
/* 1013 */           return token;
/* 1014 */       }
/* 1015 */       var symbol, preErrorSymbol, state, action, a, r, yyval = {}, p, len, newState, expected;
/* 1016 */       while (true) {
/* 1017 */           state = stack[stack.length - 1];
/* 1018 */           if (this.defaultActions[state]) {
/* 1019 */               action = this.defaultActions[state];
/* 1020 */           } else {
/* 1021 */               if (symbol === null || typeof symbol == "undefined") {
/* 1022 */                   symbol = lex();
/* 1023 */               }
/* 1024 */               action = table[state] && table[state][symbol];
/* 1025 */           }
/* 1026 */           if (typeof action === "undefined" || !action.length || !action[0]) {
/* 1027 */               var errStr = "";
/* 1028 */               if (!recovering) {
/* 1029 */                   expected = [];
/* 1030 */                   for (p in table[state])
/* 1031 */                       if (this.terminals_[p] && p > 2) {
/* 1032 */                           expected.push("'" + this.terminals_[p] + "'");
/* 1033 */                       }
/* 1034 */                   if (this.lexer.showPosition) {
/* 1035 */                       errStr = "Parse error on line " + (yylineno + 1) + ":\n" + this.lexer.showPosition() + "\nExpecting " + expected.join(", ") + ", got '" + (this.terminals_[symbol] || symbol) + "'";
/* 1036 */                   } else {
/* 1037 */                       errStr = "Parse error on line " + (yylineno + 1) + ": Unexpected " + (symbol == 1?"end of input":"'" + (this.terminals_[symbol] || symbol) + "'");
/* 1038 */                   }
/* 1039 */                   this.parseError(errStr, {text: this.lexer.match, token: this.terminals_[symbol] || symbol, line: this.lexer.yylineno, loc: yyloc, expected: expected});
/* 1040 */               }
/* 1041 */           }
/* 1042 */           if (action[0] instanceof Array && action.length > 1) {
/* 1043 */               throw new Error("Parse Error: multiple actions possible at state: " + state + ", token: " + symbol);
/* 1044 */           }
/* 1045 */           switch (action[0]) {
/* 1046 */           case 1:
/* 1047 */               stack.push(symbol);
/* 1048 */               vstack.push(this.lexer.yytext);
/* 1049 */               lstack.push(this.lexer.yylloc);
/* 1050 */               stack.push(action[1]);

/* handlebars-v2.0.0.js */

/* 1051 */               symbol = null;
/* 1052 */               if (!preErrorSymbol) {
/* 1053 */                   yyleng = this.lexer.yyleng;
/* 1054 */                   yytext = this.lexer.yytext;
/* 1055 */                   yylineno = this.lexer.yylineno;
/* 1056 */                   yyloc = this.lexer.yylloc;
/* 1057 */                   if (recovering > 0)
/* 1058 */                       recovering--;
/* 1059 */               } else {
/* 1060 */                   symbol = preErrorSymbol;
/* 1061 */                   preErrorSymbol = null;
/* 1062 */               }
/* 1063 */               break;
/* 1064 */           case 2:
/* 1065 */               len = this.productions_[action[1]][1];
/* 1066 */               yyval.$ = vstack[vstack.length - len];
/* 1067 */               yyval._$ = {first_line: lstack[lstack.length - (len || 1)].first_line, last_line: lstack[lstack.length - 1].last_line, first_column: lstack[lstack.length - (len || 1)].first_column, last_column: lstack[lstack.length - 1].last_column};
/* 1068 */               if (ranges) {
/* 1069 */                   yyval._$.range = [lstack[lstack.length - (len || 1)].range[0], lstack[lstack.length - 1].range[1]];
/* 1070 */               }
/* 1071 */               r = this.performAction.call(yyval, yytext, yyleng, yylineno, this.yy, action[1], vstack, lstack);
/* 1072 */               if (typeof r !== "undefined") {
/* 1073 */                   return r;
/* 1074 */               }
/* 1075 */               if (len) {
/* 1076 */                   stack = stack.slice(0, -1 * len * 2);
/* 1077 */                   vstack = vstack.slice(0, -1 * len);
/* 1078 */                   lstack = lstack.slice(0, -1 * len);
/* 1079 */               }
/* 1080 */               stack.push(this.productions_[action[1]][0]);
/* 1081 */               vstack.push(yyval.$);
/* 1082 */               lstack.push(yyval._$);
/* 1083 */               newState = table[stack[stack.length - 2]][stack[stack.length - 1]];
/* 1084 */               stack.push(newState);
/* 1085 */               break;
/* 1086 */           case 3:
/* 1087 */               return true;
/* 1088 */           }
/* 1089 */       }
/* 1090 */       return true;
/* 1091 */   }
/* 1092 */   };
/* 1093 */   /* Jison generated lexer */
/* 1094 */   var lexer = (function(){
/* 1095 */   var lexer = ({EOF:1,
/* 1096 */   parseError:function parseError(str, hash) {
/* 1097 */           if (this.yy.parser) {
/* 1098 */               this.yy.parser.parseError(str, hash);
/* 1099 */           } else {
/* 1100 */               throw new Error(str);

/* handlebars-v2.0.0.js */

/* 1101 */           }
/* 1102 */       },
/* 1103 */   setInput:function (input) {
/* 1104 */           this._input = input;
/* 1105 */           this._more = this._less = this.done = false;
/* 1106 */           this.yylineno = this.yyleng = 0;
/* 1107 */           this.yytext = this.matched = this.match = '';
/* 1108 */           this.conditionStack = ['INITIAL'];
/* 1109 */           this.yylloc = {first_line:1,first_column:0,last_line:1,last_column:0};
/* 1110 */           if (this.options.ranges) this.yylloc.range = [0,0];
/* 1111 */           this.offset = 0;
/* 1112 */           return this;
/* 1113 */       },
/* 1114 */   input:function () {
/* 1115 */           var ch = this._input[0];
/* 1116 */           this.yytext += ch;
/* 1117 */           this.yyleng++;
/* 1118 */           this.offset++;
/* 1119 */           this.match += ch;
/* 1120 */           this.matched += ch;
/* 1121 */           var lines = ch.match(/(?:\r\n?|\n).*/g);
/* 1122 */           if (lines) {
/* 1123 */               this.yylineno++;
/* 1124 */               this.yylloc.last_line++;
/* 1125 */           } else {
/* 1126 */               this.yylloc.last_column++;
/* 1127 */           }
/* 1128 */           if (this.options.ranges) this.yylloc.range[1]++;
/* 1129 */ 
/* 1130 */           this._input = this._input.slice(1);
/* 1131 */           return ch;
/* 1132 */       },
/* 1133 */   unput:function (ch) {
/* 1134 */           var len = ch.length;
/* 1135 */           var lines = ch.split(/(?:\r\n?|\n)/g);
/* 1136 */ 
/* 1137 */           this._input = ch + this._input;
/* 1138 */           this.yytext = this.yytext.substr(0, this.yytext.length-len-1);
/* 1139 */           //this.yyleng -= len;
/* 1140 */           this.offset -= len;
/* 1141 */           var oldLines = this.match.split(/(?:\r\n?|\n)/g);
/* 1142 */           this.match = this.match.substr(0, this.match.length-1);
/* 1143 */           this.matched = this.matched.substr(0, this.matched.length-1);
/* 1144 */ 
/* 1145 */           if (lines.length-1) this.yylineno -= lines.length-1;
/* 1146 */           var r = this.yylloc.range;
/* 1147 */ 
/* 1148 */           this.yylloc = {first_line: this.yylloc.first_line,
/* 1149 */             last_line: this.yylineno+1,
/* 1150 */             first_column: this.yylloc.first_column,

/* handlebars-v2.0.0.js */

/* 1151 */             last_column: lines ?
/* 1152 */                 (lines.length === oldLines.length ? this.yylloc.first_column : 0) + oldLines[oldLines.length - lines.length].length - lines[0].length:
/* 1153 */                 this.yylloc.first_column - len
/* 1154 */             };
/* 1155 */ 
/* 1156 */           if (this.options.ranges) {
/* 1157 */               this.yylloc.range = [r[0], r[0] + this.yyleng - len];
/* 1158 */           }
/* 1159 */           return this;
/* 1160 */       },
/* 1161 */   more:function () {
/* 1162 */           this._more = true;
/* 1163 */           return this;
/* 1164 */       },
/* 1165 */   less:function (n) {
/* 1166 */           this.unput(this.match.slice(n));
/* 1167 */       },
/* 1168 */   pastInput:function () {
/* 1169 */           var past = this.matched.substr(0, this.matched.length - this.match.length);
/* 1170 */           return (past.length > 20 ? '...':'') + past.substr(-20).replace(/\n/g, "");
/* 1171 */       },
/* 1172 */   upcomingInput:function () {
/* 1173 */           var next = this.match;
/* 1174 */           if (next.length < 20) {
/* 1175 */               next += this._input.substr(0, 20-next.length);
/* 1176 */           }
/* 1177 */           return (next.substr(0,20)+(next.length > 20 ? '...':'')).replace(/\n/g, "");
/* 1178 */       },
/* 1179 */   showPosition:function () {
/* 1180 */           var pre = this.pastInput();
/* 1181 */           var c = new Array(pre.length + 1).join("-");
/* 1182 */           return pre + this.upcomingInput() + "\n" + c+"^";
/* 1183 */       },
/* 1184 */   next:function () {
/* 1185 */           if (this.done) {
/* 1186 */               return this.EOF;
/* 1187 */           }
/* 1188 */           if (!this._input) this.done = true;
/* 1189 */ 
/* 1190 */           var token,
/* 1191 */               match,
/* 1192 */               tempMatch,
/* 1193 */               index,
/* 1194 */               col,
/* 1195 */               lines;
/* 1196 */           if (!this._more) {
/* 1197 */               this.yytext = '';
/* 1198 */               this.match = '';
/* 1199 */           }
/* 1200 */           var rules = this._currentRules();

/* handlebars-v2.0.0.js */

/* 1201 */           for (var i=0;i < rules.length; i++) {
/* 1202 */               tempMatch = this._input.match(this.rules[rules[i]]);
/* 1203 */               if (tempMatch && (!match || tempMatch[0].length > match[0].length)) {
/* 1204 */                   match = tempMatch;
/* 1205 */                   index = i;
/* 1206 */                   if (!this.options.flex) break;
/* 1207 */               }
/* 1208 */           }
/* 1209 */           if (match) {
/* 1210 */               lines = match[0].match(/(?:\r\n?|\n).*/g);
/* 1211 */               if (lines) this.yylineno += lines.length;
/* 1212 */               this.yylloc = {first_line: this.yylloc.last_line,
/* 1213 */                              last_line: this.yylineno+1,
/* 1214 */                              first_column: this.yylloc.last_column,
/* 1215 */                              last_column: lines ? lines[lines.length-1].length-lines[lines.length-1].match(/\r?\n?/)[0].length : this.yylloc.last_column + match[0].length};
/* 1216 */               this.yytext += match[0];
/* 1217 */               this.match += match[0];
/* 1218 */               this.matches = match;
/* 1219 */               this.yyleng = this.yytext.length;
/* 1220 */               if (this.options.ranges) {
/* 1221 */                   this.yylloc.range = [this.offset, this.offset += this.yyleng];
/* 1222 */               }
/* 1223 */               this._more = false;
/* 1224 */               this._input = this._input.slice(match[0].length);
/* 1225 */               this.matched += match[0];
/* 1226 */               token = this.performAction.call(this, this.yy, this, rules[index],this.conditionStack[this.conditionStack.length-1]);
/* 1227 */               if (this.done && this._input) this.done = false;
/* 1228 */               if (token) return token;
/* 1229 */               else return;
/* 1230 */           }
/* 1231 */           if (this._input === "") {
/* 1232 */               return this.EOF;
/* 1233 */           } else {
/* 1234 */               return this.parseError('Lexical error on line '+(this.yylineno+1)+'. Unrecognized text.\n'+this.showPosition(),
/* 1235 */                       {text: "", token: null, line: this.yylineno});
/* 1236 */           }
/* 1237 */       },
/* 1238 */   lex:function lex() {
/* 1239 */           var r = this.next();
/* 1240 */           if (typeof r !== 'undefined') {
/* 1241 */               return r;
/* 1242 */           } else {
/* 1243 */               return this.lex();
/* 1244 */           }
/* 1245 */       },
/* 1246 */   begin:function begin(condition) {
/* 1247 */           this.conditionStack.push(condition);
/* 1248 */       },
/* 1249 */   popState:function popState() {
/* 1250 */           return this.conditionStack.pop();

/* handlebars-v2.0.0.js */

/* 1251 */       },
/* 1252 */   _currentRules:function _currentRules() {
/* 1253 */           return this.conditions[this.conditionStack[this.conditionStack.length-1]].rules;
/* 1254 */       },
/* 1255 */   topState:function () {
/* 1256 */           return this.conditionStack[this.conditionStack.length-2];
/* 1257 */       },
/* 1258 */   pushState:function begin(condition) {
/* 1259 */           this.begin(condition);
/* 1260 */       }});
/* 1261 */   lexer.options = {};
/* 1262 */   lexer.performAction = function anonymous(yy,yy_,$avoiding_name_collisions,YY_START) {
/* 1263 */ 
/* 1264 */ 
/* 1265 */   function strip(start, end) {
/* 1266 */     return yy_.yytext = yy_.yytext.substr(start, yy_.yyleng-end);
/* 1267 */   }
/* 1268 */ 
/* 1269 */ 
/* 1270 */   var YYSTATE=YY_START
/* 1271 */   switch($avoiding_name_collisions) {
/* 1272 */   case 0:
/* 1273 */                                      if(yy_.yytext.slice(-2) === "\\\\") {
/* 1274 */                                        strip(0,1);
/* 1275 */                                        this.begin("mu");
/* 1276 */                                      } else if(yy_.yytext.slice(-1) === "\\") {
/* 1277 */                                        strip(0,1);
/* 1278 */                                        this.begin("emu");
/* 1279 */                                      } else {
/* 1280 */                                        this.begin("mu");
/* 1281 */                                      }
/* 1282 */                                      if(yy_.yytext) return 12;
/* 1283 */                                    
/* 1284 */   break;
/* 1285 */   case 1:return 12;
/* 1286 */   break;
/* 1287 */   case 2:
/* 1288 */                                      this.popState();
/* 1289 */                                      return 12;
/* 1290 */                                    
/* 1291 */   break;
/* 1292 */   case 3:
/* 1293 */                                     yy_.yytext = yy_.yytext.substr(5, yy_.yyleng-9);
/* 1294 */                                     this.popState();
/* 1295 */                                     return 15;
/* 1296 */                                    
/* 1297 */   break;
/* 1298 */   case 4: return 12; 
/* 1299 */   break;
/* 1300 */   case 5:strip(0,4); this.popState(); return 13;

/* handlebars-v2.0.0.js */

/* 1301 */   break;
/* 1302 */   case 6:return 45;
/* 1303 */   break;
/* 1304 */   case 7:return 46;
/* 1305 */   break;
/* 1306 */   case 8: return 16; 
/* 1307 */   break;
/* 1308 */   case 9:
/* 1309 */                                     this.popState();
/* 1310 */                                     this.begin('raw');
/* 1311 */                                     return 18;
/* 1312 */                                    
/* 1313 */   break;
/* 1314 */   case 10:return 34;
/* 1315 */   break;
/* 1316 */   case 11:return 24;
/* 1317 */   break;
/* 1318 */   case 12:return 29;
/* 1319 */   break;
/* 1320 */   case 13:this.popState(); return 28;
/* 1321 */   break;
/* 1322 */   case 14:this.popState(); return 28;
/* 1323 */   break;
/* 1324 */   case 15:return 26;
/* 1325 */   break;
/* 1326 */   case 16:return 26;
/* 1327 */   break;
/* 1328 */   case 17:return 32;
/* 1329 */   break;
/* 1330 */   case 18:return 31;
/* 1331 */   break;
/* 1332 */   case 19:this.popState(); this.begin('com');
/* 1333 */   break;
/* 1334 */   case 20:strip(3,5); this.popState(); return 13;
/* 1335 */   break;
/* 1336 */   case 21:return 31;
/* 1337 */   break;
/* 1338 */   case 22:return 51;
/* 1339 */   break;
/* 1340 */   case 23:return 50;
/* 1341 */   break;
/* 1342 */   case 24:return 50;
/* 1343 */   break;
/* 1344 */   case 25:return 54;
/* 1345 */   break;
/* 1346 */   case 26:// ignore whitespace
/* 1347 */   break;
/* 1348 */   case 27:this.popState(); return 33;
/* 1349 */   break;
/* 1350 */   case 28:this.popState(); return 25;

/* handlebars-v2.0.0.js */

/* 1351 */   break;
/* 1352 */   case 29:yy_.yytext = strip(1,2).replace(/\\"/g,'"'); return 42;
/* 1353 */   break;
/* 1354 */   case 30:yy_.yytext = strip(1,2).replace(/\\'/g,"'"); return 42;
/* 1355 */   break;
/* 1356 */   case 31:return 52;
/* 1357 */   break;
/* 1358 */   case 32:return 44;
/* 1359 */   break;
/* 1360 */   case 33:return 44;
/* 1361 */   break;
/* 1362 */   case 34:return 43;
/* 1363 */   break;
/* 1364 */   case 35:return 50;
/* 1365 */   break;
/* 1366 */   case 36:yy_.yytext = strip(1,2); return 50;
/* 1367 */   break;
/* 1368 */   case 37:return 'INVALID';
/* 1369 */   break;
/* 1370 */   case 38:return 5;
/* 1371 */   break;
/* 1372 */   }
/* 1373 */   };
/* 1374 */   lexer.rules = [/^(?:[^\x00]*?(?=(\{\{)))/,/^(?:[^\x00]+)/,/^(?:[^\x00]{2,}?(?=(\{\{|\\\{\{|\\\\\{\{|$)))/,/^(?:\{\{\{\{\/[^\s!"#%-,\.\/;->@\[-\^`\{-~]+(?=[=}\s\/.])\}\}\}\})/,/^(?:[^\x00]*?(?=(\{\{\{\{\/)))/,/^(?:[\s\S]*?--\}\})/,/^(?:\()/,/^(?:\))/,/^(?:\{\{\{\{)/,/^(?:\}\}\}\})/,/^(?:\{\{(~)?>)/,/^(?:\{\{(~)?#)/,/^(?:\{\{(~)?\/)/,/^(?:\{\{(~)?\^\s*(~)?\}\})/,/^(?:\{\{(~)?\s*else\s*(~)?\}\})/,/^(?:\{\{(~)?\^)/,/^(?:\{\{(~)?\s*else\b)/,/^(?:\{\{(~)?\{)/,/^(?:\{\{(~)?&)/,/^(?:\{\{!--)/,/^(?:\{\{![\s\S]*?\}\})/,/^(?:\{\{(~)?)/,/^(?:=)/,/^(?:\.\.)/,/^(?:\.(?=([=~}\s\/.)])))/,/^(?:[\/.])/,/^(?:\s+)/,/^(?:\}(~)?\}\})/,/^(?:(~)?\}\})/,/^(?:"(\\["]|[^"])*")/,/^(?:'(\\[']|[^'])*')/,/^(?:@)/,/^(?:true(?=([~}\s)])))/,/^(?:false(?=([~}\s)])))/,/^(?:-?[0-9]+(?:\.[0-9]+)?(?=([~}\s)])))/,/^(?:([^\s!"#%-,\.\/;->@\[-\^`\{-~]+(?=([=~}\s\/.)]))))/,/^(?:\[[^\]]*\])/,/^(?:.)/,/^(?:$)/];
/* 1375 */   lexer.conditions = {"mu":{"rules":[6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38],"inclusive":false},"emu":{"rules":[2],"inclusive":false},"com":{"rules":[5],"inclusive":false},"raw":{"rules":[3,4],"inclusive":false},"INITIAL":{"rules":[0,1,38],"inclusive":true}};
/* 1376 */   return lexer;})()
/* 1377 */   parser.lexer = lexer;
/* 1378 */   function Parser () { this.yy = {}; }Parser.prototype = parser;parser.Parser = Parser;
/* 1379 */   return new Parser;
/* 1380 */   })();__exports__ = handlebars;
/* 1381 */   /* jshint ignore:end */
/* 1382 */   return __exports__;
/* 1383 */ })();
/* 1384 */ 
/* 1385 */ // handlebars/compiler/helpers.js
/* 1386 */ var __module10__ = (function(__dependency1__) {
/* 1387 */   "use strict";
/* 1388 */   var __exports__ = {};
/* 1389 */   var Exception = __dependency1__;
/* 1390 */ 
/* 1391 */   function stripFlags(open, close) {
/* 1392 */     return {
/* 1393 */       left: open.charAt(2) === '~',
/* 1394 */       right: close.charAt(close.length-3) === '~'
/* 1395 */     };
/* 1396 */   }
/* 1397 */ 
/* 1398 */   __exports__.stripFlags = stripFlags;
/* 1399 */   function prepareBlock(mustache, program, inverseAndProgram, close, inverted, locInfo) {
/* 1400 */     /*jshint -W040 */

/* handlebars-v2.0.0.js */

/* 1401 */     if (mustache.sexpr.id.original !== close.path.original) {
/* 1402 */       throw new Exception(mustache.sexpr.id.original + ' doesn\'t match ' + close.path.original, mustache);
/* 1403 */     }
/* 1404 */ 
/* 1405 */     var inverse = inverseAndProgram && inverseAndProgram.program;
/* 1406 */ 
/* 1407 */     var strip = {
/* 1408 */       left: mustache.strip.left,
/* 1409 */       right: close.strip.right,
/* 1410 */ 
/* 1411 */       // Determine the standalone candiacy. Basically flag our content as being possibly standalone
/* 1412 */       // so our parent can determine if we actually are standalone
/* 1413 */       openStandalone: isNextWhitespace(program.statements),
/* 1414 */       closeStandalone: isPrevWhitespace((inverse || program).statements)
/* 1415 */     };
/* 1416 */ 
/* 1417 */     if (mustache.strip.right) {
/* 1418 */       omitRight(program.statements, null, true);
/* 1419 */     }
/* 1420 */ 
/* 1421 */     if (inverse) {
/* 1422 */       var inverseStrip = inverseAndProgram.strip;
/* 1423 */ 
/* 1424 */       if (inverseStrip.left) {
/* 1425 */         omitLeft(program.statements, null, true);
/* 1426 */       }
/* 1427 */       if (inverseStrip.right) {
/* 1428 */         omitRight(inverse.statements, null, true);
/* 1429 */       }
/* 1430 */       if (close.strip.left) {
/* 1431 */         omitLeft(inverse.statements, null, true);
/* 1432 */       }
/* 1433 */ 
/* 1434 */       // Find standalone else statments
/* 1435 */       if (isPrevWhitespace(program.statements)
/* 1436 */           && isNextWhitespace(inverse.statements)) {
/* 1437 */ 
/* 1438 */         omitLeft(program.statements);
/* 1439 */         omitRight(inverse.statements);
/* 1440 */       }
/* 1441 */     } else {
/* 1442 */       if (close.strip.left) {
/* 1443 */         omitLeft(program.statements, null, true);
/* 1444 */       }
/* 1445 */     }
/* 1446 */ 
/* 1447 */     if (inverted) {
/* 1448 */       return new this.BlockNode(mustache, inverse, program, strip, locInfo);
/* 1449 */     } else {
/* 1450 */       return new this.BlockNode(mustache, program, inverse, strip, locInfo);

/* handlebars-v2.0.0.js */

/* 1451 */     }
/* 1452 */   }
/* 1453 */ 
/* 1454 */   __exports__.prepareBlock = prepareBlock;
/* 1455 */   function prepareProgram(statements, isRoot) {
/* 1456 */     for (var i = 0, l = statements.length; i < l; i++) {
/* 1457 */       var current = statements[i],
/* 1458 */           strip = current.strip;
/* 1459 */ 
/* 1460 */       if (!strip) {
/* 1461 */         continue;
/* 1462 */       }
/* 1463 */ 
/* 1464 */       var _isPrevWhitespace = isPrevWhitespace(statements, i, isRoot, current.type === 'partial'),
/* 1465 */           _isNextWhitespace = isNextWhitespace(statements, i, isRoot),
/* 1466 */ 
/* 1467 */           openStandalone = strip.openStandalone && _isPrevWhitespace,
/* 1468 */           closeStandalone = strip.closeStandalone && _isNextWhitespace,
/* 1469 */           inlineStandalone = strip.inlineStandalone && _isPrevWhitespace && _isNextWhitespace;
/* 1470 */ 
/* 1471 */       if (strip.right) {
/* 1472 */         omitRight(statements, i, true);
/* 1473 */       }
/* 1474 */       if (strip.left) {
/* 1475 */         omitLeft(statements, i, true);
/* 1476 */       }
/* 1477 */ 
/* 1478 */       if (inlineStandalone) {
/* 1479 */         omitRight(statements, i);
/* 1480 */ 
/* 1481 */         if (omitLeft(statements, i)) {
/* 1482 */           // If we are on a standalone node, save the indent info for partials
/* 1483 */           if (current.type === 'partial') {
/* 1484 */             current.indent = (/([ \t]+$)/).exec(statements[i-1].original) ? RegExp.$1 : '';
/* 1485 */           }
/* 1486 */         }
/* 1487 */       }
/* 1488 */       if (openStandalone) {
/* 1489 */         omitRight((current.program || current.inverse).statements);
/* 1490 */ 
/* 1491 */         // Strip out the previous content node if it's whitespace only
/* 1492 */         omitLeft(statements, i);
/* 1493 */       }
/* 1494 */       if (closeStandalone) {
/* 1495 */         // Always strip the next node
/* 1496 */         omitRight(statements, i);
/* 1497 */ 
/* 1498 */         omitLeft((current.inverse || current.program).statements);
/* 1499 */       }
/* 1500 */     }

/* handlebars-v2.0.0.js */

/* 1501 */ 
/* 1502 */     return statements;
/* 1503 */   }
/* 1504 */ 
/* 1505 */   __exports__.prepareProgram = prepareProgram;function isPrevWhitespace(statements, i, isRoot) {
/* 1506 */     if (i === undefined) {
/* 1507 */       i = statements.length;
/* 1508 */     }
/* 1509 */ 
/* 1510 */     // Nodes that end with newlines are considered whitespace (but are special
/* 1511 */     // cased for strip operations)
/* 1512 */     var prev = statements[i-1],
/* 1513 */         sibling = statements[i-2];
/* 1514 */     if (!prev) {
/* 1515 */       return isRoot;
/* 1516 */     }
/* 1517 */ 
/* 1518 */     if (prev.type === 'content') {
/* 1519 */       return (sibling || !isRoot ? (/\r?\n\s*?$/) : (/(^|\r?\n)\s*?$/)).test(prev.original);
/* 1520 */     }
/* 1521 */   }
/* 1522 */   function isNextWhitespace(statements, i, isRoot) {
/* 1523 */     if (i === undefined) {
/* 1524 */       i = -1;
/* 1525 */     }
/* 1526 */ 
/* 1527 */     var next = statements[i+1],
/* 1528 */         sibling = statements[i+2];
/* 1529 */     if (!next) {
/* 1530 */       return isRoot;
/* 1531 */     }
/* 1532 */ 
/* 1533 */     if (next.type === 'content') {
/* 1534 */       return (sibling || !isRoot ? (/^\s*?\r?\n/) : (/^\s*?(\r?\n|$)/)).test(next.original);
/* 1535 */     }
/* 1536 */   }
/* 1537 */ 
/* 1538 */   // Marks the node to the right of the position as omitted.
/* 1539 */   // I.e. {{foo}}' ' will mark the ' ' node as omitted.
/* 1540 */   //
/* 1541 */   // If i is undefined, then the first child will be marked as such.
/* 1542 */   //
/* 1543 */   // If mulitple is truthy then all whitespace will be stripped out until non-whitespace
/* 1544 */   // content is met.
/* 1545 */   function omitRight(statements, i, multiple) {
/* 1546 */     var current = statements[i == null ? 0 : i + 1];
/* 1547 */     if (!current || current.type !== 'content' || (!multiple && current.rightStripped)) {
/* 1548 */       return;
/* 1549 */     }
/* 1550 */ 

/* handlebars-v2.0.0.js */

/* 1551 */     var original = current.string;
/* 1552 */     current.string = current.string.replace(multiple ? (/^\s+/) : (/^[ \t]*\r?\n?/), '');
/* 1553 */     current.rightStripped = current.string !== original;
/* 1554 */   }
/* 1555 */ 
/* 1556 */   // Marks the node to the left of the position as omitted.
/* 1557 */   // I.e. ' '{{foo}} will mark the ' ' node as omitted.
/* 1558 */   //
/* 1559 */   // If i is undefined then the last child will be marked as such.
/* 1560 */   //
/* 1561 */   // If mulitple is truthy then all whitespace will be stripped out until non-whitespace
/* 1562 */   // content is met.
/* 1563 */   function omitLeft(statements, i, multiple) {
/* 1564 */     var current = statements[i == null ? statements.length - 1 : i - 1];
/* 1565 */     if (!current || current.type !== 'content' || (!multiple && current.leftStripped)) {
/* 1566 */       return;
/* 1567 */     }
/* 1568 */ 
/* 1569 */     // We omit the last node if it's whitespace only and not preceeded by a non-content node.
/* 1570 */     var original = current.string;
/* 1571 */     current.string = current.string.replace(multiple ? (/\s+$/) : (/[ \t]+$/), '');
/* 1572 */     current.leftStripped = current.string !== original;
/* 1573 */     return current.leftStripped;
/* 1574 */   }
/* 1575 */   return __exports__;
/* 1576 */ })(__module5__);
/* 1577 */ 
/* 1578 */ // handlebars/compiler/base.js
/* 1579 */ var __module8__ = (function(__dependency1__, __dependency2__, __dependency3__, __dependency4__) {
/* 1580 */   "use strict";
/* 1581 */   var __exports__ = {};
/* 1582 */   var parser = __dependency1__;
/* 1583 */   var AST = __dependency2__;
/* 1584 */   var Helpers = __dependency3__;
/* 1585 */   var extend = __dependency4__.extend;
/* 1586 */ 
/* 1587 */   __exports__.parser = parser;
/* 1588 */ 
/* 1589 */   var yy = {};
/* 1590 */   extend(yy, Helpers, AST);
/* 1591 */ 
/* 1592 */   function parse(input) {
/* 1593 */     // Just return if an already-compile AST was passed in.
/* 1594 */     if (input.constructor === AST.ProgramNode) { return input; }
/* 1595 */ 
/* 1596 */     parser.yy = yy;
/* 1597 */ 
/* 1598 */     return parser.parse(input);
/* 1599 */   }
/* 1600 */ 

/* handlebars-v2.0.0.js */

/* 1601 */   __exports__.parse = parse;
/* 1602 */   return __exports__;
/* 1603 */ })(__module9__, __module7__, __module10__, __module3__);
/* 1604 */ 
/* 1605 */ // handlebars/compiler/compiler.js
/* 1606 */ var __module11__ = (function(__dependency1__, __dependency2__) {
/* 1607 */   "use strict";
/* 1608 */   var __exports__ = {};
/* 1609 */   var Exception = __dependency1__;
/* 1610 */   var isArray = __dependency2__.isArray;
/* 1611 */ 
/* 1612 */   var slice = [].slice;
/* 1613 */ 
/* 1614 */   function Compiler() {}
/* 1615 */ 
/* 1616 */   __exports__.Compiler = Compiler;// the foundHelper register will disambiguate helper lookup from finding a
/* 1617 */   // function in a context. This is necessary for mustache compatibility, which
/* 1618 */   // requires that context functions in blocks are evaluated by blockHelperMissing,
/* 1619 */   // and then proceed as if the resulting value was provided to blockHelperMissing.
/* 1620 */ 
/* 1621 */   Compiler.prototype = {
/* 1622 */     compiler: Compiler,
/* 1623 */ 
/* 1624 */     equals: function(other) {
/* 1625 */       var len = this.opcodes.length;
/* 1626 */       if (other.opcodes.length !== len) {
/* 1627 */         return false;
/* 1628 */       }
/* 1629 */ 
/* 1630 */       for (var i = 0; i < len; i++) {
/* 1631 */         var opcode = this.opcodes[i],
/* 1632 */             otherOpcode = other.opcodes[i];
/* 1633 */         if (opcode.opcode !== otherOpcode.opcode || !argEquals(opcode.args, otherOpcode.args)) {
/* 1634 */           return false;
/* 1635 */         }
/* 1636 */       }
/* 1637 */ 
/* 1638 */       // We know that length is the same between the two arrays because they are directly tied
/* 1639 */       // to the opcode behavior above.
/* 1640 */       len = this.children.length;
/* 1641 */       for (i = 0; i < len; i++) {
/* 1642 */         if (!this.children[i].equals(other.children[i])) {
/* 1643 */           return false;
/* 1644 */         }
/* 1645 */       }
/* 1646 */ 
/* 1647 */       return true;
/* 1648 */     },
/* 1649 */ 
/* 1650 */     guid: 0,

/* handlebars-v2.0.0.js */

/* 1651 */ 
/* 1652 */     compile: function(program, options) {
/* 1653 */       this.opcodes = [];
/* 1654 */       this.children = [];
/* 1655 */       this.depths = {list: []};
/* 1656 */       this.options = options;
/* 1657 */       this.stringParams = options.stringParams;
/* 1658 */       this.trackIds = options.trackIds;
/* 1659 */ 
/* 1660 */       // These changes will propagate to the other compiler components
/* 1661 */       var knownHelpers = this.options.knownHelpers;
/* 1662 */       this.options.knownHelpers = {
/* 1663 */         'helperMissing': true,
/* 1664 */         'blockHelperMissing': true,
/* 1665 */         'each': true,
/* 1666 */         'if': true,
/* 1667 */         'unless': true,
/* 1668 */         'with': true,
/* 1669 */         'log': true,
/* 1670 */         'lookup': true
/* 1671 */       };
/* 1672 */       if (knownHelpers) {
/* 1673 */         for (var name in knownHelpers) {
/* 1674 */           this.options.knownHelpers[name] = knownHelpers[name];
/* 1675 */         }
/* 1676 */       }
/* 1677 */ 
/* 1678 */       return this.accept(program);
/* 1679 */     },
/* 1680 */ 
/* 1681 */     accept: function(node) {
/* 1682 */       return this[node.type](node);
/* 1683 */     },
/* 1684 */ 
/* 1685 */     program: function(program) {
/* 1686 */       var statements = program.statements;
/* 1687 */ 
/* 1688 */       for(var i=0, l=statements.length; i<l; i++) {
/* 1689 */         this.accept(statements[i]);
/* 1690 */       }
/* 1691 */       this.isSimple = l === 1;
/* 1692 */ 
/* 1693 */       this.depths.list = this.depths.list.sort(function(a, b) {
/* 1694 */         return a - b;
/* 1695 */       });
/* 1696 */ 
/* 1697 */       return this;
/* 1698 */     },
/* 1699 */ 
/* 1700 */     compileProgram: function(program) {

/* handlebars-v2.0.0.js */

/* 1701 */       var result = new this.compiler().compile(program, this.options);
/* 1702 */       var guid = this.guid++, depth;
/* 1703 */ 
/* 1704 */       this.usePartial = this.usePartial || result.usePartial;
/* 1705 */ 
/* 1706 */       this.children[guid] = result;
/* 1707 */ 
/* 1708 */       for(var i=0, l=result.depths.list.length; i<l; i++) {
/* 1709 */         depth = result.depths.list[i];
/* 1710 */ 
/* 1711 */         if(depth < 2) { continue; }
/* 1712 */         else { this.addDepth(depth - 1); }
/* 1713 */       }
/* 1714 */ 
/* 1715 */       return guid;
/* 1716 */     },
/* 1717 */ 
/* 1718 */     block: function(block) {
/* 1719 */       var mustache = block.mustache,
/* 1720 */           program = block.program,
/* 1721 */           inverse = block.inverse;
/* 1722 */ 
/* 1723 */       if (program) {
/* 1724 */         program = this.compileProgram(program);
/* 1725 */       }
/* 1726 */ 
/* 1727 */       if (inverse) {
/* 1728 */         inverse = this.compileProgram(inverse);
/* 1729 */       }
/* 1730 */ 
/* 1731 */       var sexpr = mustache.sexpr;
/* 1732 */       var type = this.classifySexpr(sexpr);
/* 1733 */ 
/* 1734 */       if (type === "helper") {
/* 1735 */         this.helperSexpr(sexpr, program, inverse);
/* 1736 */       } else if (type === "simple") {
/* 1737 */         this.simpleSexpr(sexpr);
/* 1738 */ 
/* 1739 */         // now that the simple mustache is resolved, we need to
/* 1740 */         // evaluate it by executing `blockHelperMissing`
/* 1741 */         this.opcode('pushProgram', program);
/* 1742 */         this.opcode('pushProgram', inverse);
/* 1743 */         this.opcode('emptyHash');
/* 1744 */         this.opcode('blockValue', sexpr.id.original);
/* 1745 */       } else {
/* 1746 */         this.ambiguousSexpr(sexpr, program, inverse);
/* 1747 */ 
/* 1748 */         // now that the simple mustache is resolved, we need to
/* 1749 */         // evaluate it by executing `blockHelperMissing`
/* 1750 */         this.opcode('pushProgram', program);

/* handlebars-v2.0.0.js */

/* 1751 */         this.opcode('pushProgram', inverse);
/* 1752 */         this.opcode('emptyHash');
/* 1753 */         this.opcode('ambiguousBlockValue');
/* 1754 */       }
/* 1755 */ 
/* 1756 */       this.opcode('append');
/* 1757 */     },
/* 1758 */ 
/* 1759 */     hash: function(hash) {
/* 1760 */       var pairs = hash.pairs, i, l;
/* 1761 */ 
/* 1762 */       this.opcode('pushHash');
/* 1763 */ 
/* 1764 */       for(i=0, l=pairs.length; i<l; i++) {
/* 1765 */         this.pushParam(pairs[i][1]);
/* 1766 */       }
/* 1767 */       while(i--) {
/* 1768 */         this.opcode('assignToHash', pairs[i][0]);
/* 1769 */       }
/* 1770 */       this.opcode('popHash');
/* 1771 */     },
/* 1772 */ 
/* 1773 */     partial: function(partial) {
/* 1774 */       var partialName = partial.partialName;
/* 1775 */       this.usePartial = true;
/* 1776 */ 
/* 1777 */       if (partial.hash) {
/* 1778 */         this.accept(partial.hash);
/* 1779 */       } else {
/* 1780 */         this.opcode('push', 'undefined');
/* 1781 */       }
/* 1782 */ 
/* 1783 */       if (partial.context) {
/* 1784 */         this.accept(partial.context);
/* 1785 */       } else {
/* 1786 */         this.opcode('getContext', 0);
/* 1787 */         this.opcode('pushContext');
/* 1788 */       }
/* 1789 */ 
/* 1790 */       this.opcode('invokePartial', partialName.name, partial.indent || '');
/* 1791 */       this.opcode('append');
/* 1792 */     },
/* 1793 */ 
/* 1794 */     content: function(content) {
/* 1795 */       if (content.string) {
/* 1796 */         this.opcode('appendContent', content.string);
/* 1797 */       }
/* 1798 */     },
/* 1799 */ 
/* 1800 */     mustache: function(mustache) {

/* handlebars-v2.0.0.js */

/* 1801 */       this.sexpr(mustache.sexpr);
/* 1802 */ 
/* 1803 */       if(mustache.escaped && !this.options.noEscape) {
/* 1804 */         this.opcode('appendEscaped');
/* 1805 */       } else {
/* 1806 */         this.opcode('append');
/* 1807 */       }
/* 1808 */     },
/* 1809 */ 
/* 1810 */     ambiguousSexpr: function(sexpr, program, inverse) {
/* 1811 */       var id = sexpr.id,
/* 1812 */           name = id.parts[0],
/* 1813 */           isBlock = program != null || inverse != null;
/* 1814 */ 
/* 1815 */       this.opcode('getContext', id.depth);
/* 1816 */ 
/* 1817 */       this.opcode('pushProgram', program);
/* 1818 */       this.opcode('pushProgram', inverse);
/* 1819 */ 
/* 1820 */       this.ID(id);
/* 1821 */ 
/* 1822 */       this.opcode('invokeAmbiguous', name, isBlock);
/* 1823 */     },
/* 1824 */ 
/* 1825 */     simpleSexpr: function(sexpr) {
/* 1826 */       var id = sexpr.id;
/* 1827 */ 
/* 1828 */       if (id.type === 'DATA') {
/* 1829 */         this.DATA(id);
/* 1830 */       } else if (id.parts.length) {
/* 1831 */         this.ID(id);
/* 1832 */       } else {
/* 1833 */         // Simplified ID for `this`
/* 1834 */         this.addDepth(id.depth);
/* 1835 */         this.opcode('getContext', id.depth);
/* 1836 */         this.opcode('pushContext');
/* 1837 */       }
/* 1838 */ 
/* 1839 */       this.opcode('resolvePossibleLambda');
/* 1840 */     },
/* 1841 */ 
/* 1842 */     helperSexpr: function(sexpr, program, inverse) {
/* 1843 */       var params = this.setupFullMustacheParams(sexpr, program, inverse),
/* 1844 */           id = sexpr.id,
/* 1845 */           name = id.parts[0];
/* 1846 */ 
/* 1847 */       if (this.options.knownHelpers[name]) {
/* 1848 */         this.opcode('invokeKnownHelper', params.length, name);
/* 1849 */       } else if (this.options.knownHelpersOnly) {
/* 1850 */         throw new Exception("You specified knownHelpersOnly, but used the unknown helper " + name, sexpr);

/* handlebars-v2.0.0.js */

/* 1851 */       } else {
/* 1852 */         id.falsy = true;
/* 1853 */ 
/* 1854 */         this.ID(id);
/* 1855 */         this.opcode('invokeHelper', params.length, id.original, id.isSimple);
/* 1856 */       }
/* 1857 */     },
/* 1858 */ 
/* 1859 */     sexpr: function(sexpr) {
/* 1860 */       var type = this.classifySexpr(sexpr);
/* 1861 */ 
/* 1862 */       if (type === "simple") {
/* 1863 */         this.simpleSexpr(sexpr);
/* 1864 */       } else if (type === "helper") {
/* 1865 */         this.helperSexpr(sexpr);
/* 1866 */       } else {
/* 1867 */         this.ambiguousSexpr(sexpr);
/* 1868 */       }
/* 1869 */     },
/* 1870 */ 
/* 1871 */     ID: function(id) {
/* 1872 */       this.addDepth(id.depth);
/* 1873 */       this.opcode('getContext', id.depth);
/* 1874 */ 
/* 1875 */       var name = id.parts[0];
/* 1876 */       if (!name) {
/* 1877 */         // Context reference, i.e. `{{foo .}}` or `{{foo ..}}`
/* 1878 */         this.opcode('pushContext');
/* 1879 */       } else {
/* 1880 */         this.opcode('lookupOnContext', id.parts, id.falsy, id.isScoped);
/* 1881 */       }
/* 1882 */     },
/* 1883 */ 
/* 1884 */     DATA: function(data) {
/* 1885 */       this.options.data = true;
/* 1886 */       this.opcode('lookupData', data.id.depth, data.id.parts);
/* 1887 */     },
/* 1888 */ 
/* 1889 */     STRING: function(string) {
/* 1890 */       this.opcode('pushString', string.string);
/* 1891 */     },
/* 1892 */ 
/* 1893 */     NUMBER: function(number) {
/* 1894 */       this.opcode('pushLiteral', number.number);
/* 1895 */     },
/* 1896 */ 
/* 1897 */     BOOLEAN: function(bool) {
/* 1898 */       this.opcode('pushLiteral', bool.bool);
/* 1899 */     },
/* 1900 */ 

/* handlebars-v2.0.0.js */

/* 1901 */     comment: function() {},
/* 1902 */ 
/* 1903 */     // HELPERS
/* 1904 */     opcode: function(name) {
/* 1905 */       this.opcodes.push({ opcode: name, args: slice.call(arguments, 1) });
/* 1906 */     },
/* 1907 */ 
/* 1908 */     addDepth: function(depth) {
/* 1909 */       if(depth === 0) { return; }
/* 1910 */ 
/* 1911 */       if(!this.depths[depth]) {
/* 1912 */         this.depths[depth] = true;
/* 1913 */         this.depths.list.push(depth);
/* 1914 */       }
/* 1915 */     },
/* 1916 */ 
/* 1917 */     classifySexpr: function(sexpr) {
/* 1918 */       var isHelper   = sexpr.isHelper;
/* 1919 */       var isEligible = sexpr.eligibleHelper;
/* 1920 */       var options    = this.options;
/* 1921 */ 
/* 1922 */       // if ambiguous, we can possibly resolve the ambiguity now
/* 1923 */       // An eligible helper is one that does not have a complex path, i.e. `this.foo`, `../foo` etc.
/* 1924 */       if (isEligible && !isHelper) {
/* 1925 */         var name = sexpr.id.parts[0];
/* 1926 */ 
/* 1927 */         if (options.knownHelpers[name]) {
/* 1928 */           isHelper = true;
/* 1929 */         } else if (options.knownHelpersOnly) {
/* 1930 */           isEligible = false;
/* 1931 */         }
/* 1932 */       }
/* 1933 */ 
/* 1934 */       if (isHelper) { return "helper"; }
/* 1935 */       else if (isEligible) { return "ambiguous"; }
/* 1936 */       else { return "simple"; }
/* 1937 */     },
/* 1938 */ 
/* 1939 */     pushParams: function(params) {
/* 1940 */       for(var i=0, l=params.length; i<l; i++) {
/* 1941 */         this.pushParam(params[i]);
/* 1942 */       }
/* 1943 */     },
/* 1944 */ 
/* 1945 */     pushParam: function(val) {
/* 1946 */       if (this.stringParams) {
/* 1947 */         if(val.depth) {
/* 1948 */           this.addDepth(val.depth);
/* 1949 */         }
/* 1950 */         this.opcode('getContext', val.depth || 0);

/* handlebars-v2.0.0.js */

/* 1951 */         this.opcode('pushStringParam', val.stringModeValue, val.type);
/* 1952 */ 
/* 1953 */         if (val.type === 'sexpr') {
/* 1954 */           // Subexpressions get evaluated and passed in
/* 1955 */           // in string params mode.
/* 1956 */           this.sexpr(val);
/* 1957 */         }
/* 1958 */       } else {
/* 1959 */         if (this.trackIds) {
/* 1960 */           this.opcode('pushId', val.type, val.idName || val.stringModeValue);
/* 1961 */         }
/* 1962 */         this.accept(val);
/* 1963 */       }
/* 1964 */     },
/* 1965 */ 
/* 1966 */     setupFullMustacheParams: function(sexpr, program, inverse) {
/* 1967 */       var params = sexpr.params;
/* 1968 */       this.pushParams(params);
/* 1969 */ 
/* 1970 */       this.opcode('pushProgram', program);
/* 1971 */       this.opcode('pushProgram', inverse);
/* 1972 */ 
/* 1973 */       if (sexpr.hash) {
/* 1974 */         this.hash(sexpr.hash);
/* 1975 */       } else {
/* 1976 */         this.opcode('emptyHash');
/* 1977 */       }
/* 1978 */ 
/* 1979 */       return params;
/* 1980 */     }
/* 1981 */   };
/* 1982 */ 
/* 1983 */   function precompile(input, options, env) {
/* 1984 */     if (input == null || (typeof input !== 'string' && input.constructor !== env.AST.ProgramNode)) {
/* 1985 */       throw new Exception("You must pass a string or Handlebars AST to Handlebars.precompile. You passed " + input);
/* 1986 */     }
/* 1987 */ 
/* 1988 */     options = options || {};
/* 1989 */     if (!('data' in options)) {
/* 1990 */       options.data = true;
/* 1991 */     }
/* 1992 */     if (options.compat) {
/* 1993 */       options.useDepths = true;
/* 1994 */     }
/* 1995 */ 
/* 1996 */     var ast = env.parse(input);
/* 1997 */     var environment = new env.Compiler().compile(ast, options);
/* 1998 */     return new env.JavaScriptCompiler().compile(environment, options);
/* 1999 */   }
/* 2000 */ 

/* handlebars-v2.0.0.js */

/* 2001 */   __exports__.precompile = precompile;function compile(input, options, env) {
/* 2002 */     if (input == null || (typeof input !== 'string' && input.constructor !== env.AST.ProgramNode)) {
/* 2003 */       throw new Exception("You must pass a string or Handlebars AST to Handlebars.compile. You passed " + input);
/* 2004 */     }
/* 2005 */ 
/* 2006 */     options = options || {};
/* 2007 */ 
/* 2008 */     if (!('data' in options)) {
/* 2009 */       options.data = true;
/* 2010 */     }
/* 2011 */     if (options.compat) {
/* 2012 */       options.useDepths = true;
/* 2013 */     }
/* 2014 */ 
/* 2015 */     var compiled;
/* 2016 */ 
/* 2017 */     function compileInput() {
/* 2018 */       var ast = env.parse(input);
/* 2019 */       var environment = new env.Compiler().compile(ast, options);
/* 2020 */       var templateSpec = new env.JavaScriptCompiler().compile(environment, options, undefined, true);
/* 2021 */       return env.template(templateSpec);
/* 2022 */     }
/* 2023 */ 
/* 2024 */     // Template is only compiled on first use and cached after that point.
/* 2025 */     var ret = function(context, options) {
/* 2026 */       if (!compiled) {
/* 2027 */         compiled = compileInput();
/* 2028 */       }
/* 2029 */       return compiled.call(this, context, options);
/* 2030 */     };
/* 2031 */     ret._setup = function(options) {
/* 2032 */       if (!compiled) {
/* 2033 */         compiled = compileInput();
/* 2034 */       }
/* 2035 */       return compiled._setup(options);
/* 2036 */     };
/* 2037 */     ret._child = function(i, data, depths) {
/* 2038 */       if (!compiled) {
/* 2039 */         compiled = compileInput();
/* 2040 */       }
/* 2041 */       return compiled._child(i, data, depths);
/* 2042 */     };
/* 2043 */     return ret;
/* 2044 */   }
/* 2045 */ 
/* 2046 */   __exports__.compile = compile;function argEquals(a, b) {
/* 2047 */     if (a === b) {
/* 2048 */       return true;
/* 2049 */     }
/* 2050 */ 

/* handlebars-v2.0.0.js */

/* 2051 */     if (isArray(a) && isArray(b) && a.length === b.length) {
/* 2052 */       for (var i = 0; i < a.length; i++) {
/* 2053 */         if (!argEquals(a[i], b[i])) {
/* 2054 */           return false;
/* 2055 */         }
/* 2056 */       }
/* 2057 */       return true;
/* 2058 */     }
/* 2059 */   }
/* 2060 */   return __exports__;
/* 2061 */ })(__module5__, __module3__);
/* 2062 */ 
/* 2063 */ // handlebars/compiler/javascript-compiler.js
/* 2064 */ var __module12__ = (function(__dependency1__, __dependency2__) {
/* 2065 */   "use strict";
/* 2066 */   var __exports__;
/* 2067 */   var COMPILER_REVISION = __dependency1__.COMPILER_REVISION;
/* 2068 */   var REVISION_CHANGES = __dependency1__.REVISION_CHANGES;
/* 2069 */   var Exception = __dependency2__;
/* 2070 */ 
/* 2071 */   function Literal(value) {
/* 2072 */     this.value = value;
/* 2073 */   }
/* 2074 */ 
/* 2075 */   function JavaScriptCompiler() {}
/* 2076 */ 
/* 2077 */   JavaScriptCompiler.prototype = {
/* 2078 */     // PUBLIC API: You can override these methods in a subclass to provide
/* 2079 */     // alternative compiled forms for name lookup and buffering semantics
/* 2080 */     nameLookup: function(parent, name /* , type*/) {
/* 2081 */       if (JavaScriptCompiler.isValidJavaScriptVariableName(name)) {
/* 2082 */         return parent + "." + name;
/* 2083 */       } else {
/* 2084 */         return parent + "['" + name + "']";
/* 2085 */       }
/* 2086 */     },
/* 2087 */     depthedLookup: function(name) {
/* 2088 */       this.aliases.lookup = 'this.lookup';
/* 2089 */ 
/* 2090 */       return 'lookup(depths, "' + name + '")';
/* 2091 */     },
/* 2092 */ 
/* 2093 */     compilerInfo: function() {
/* 2094 */       var revision = COMPILER_REVISION,
/* 2095 */           versions = REVISION_CHANGES[revision];
/* 2096 */       return [revision, versions];
/* 2097 */     },
/* 2098 */ 
/* 2099 */     appendToBuffer: function(string) {
/* 2100 */       if (this.environment.isSimple) {

/* handlebars-v2.0.0.js */

/* 2101 */         return "return " + string + ";";
/* 2102 */       } else {
/* 2103 */         return {
/* 2104 */           appendToBuffer: true,
/* 2105 */           content: string,
/* 2106 */           toString: function() { return "buffer += " + string + ";"; }
/* 2107 */         };
/* 2108 */       }
/* 2109 */     },
/* 2110 */ 
/* 2111 */     initializeBuffer: function() {
/* 2112 */       return this.quotedString("");
/* 2113 */     },
/* 2114 */ 
/* 2115 */     namespace: "Handlebars",
/* 2116 */     // END PUBLIC API
/* 2117 */ 
/* 2118 */     compile: function(environment, options, context, asObject) {
/* 2119 */       this.environment = environment;
/* 2120 */       this.options = options;
/* 2121 */       this.stringParams = this.options.stringParams;
/* 2122 */       this.trackIds = this.options.trackIds;
/* 2123 */       this.precompile = !asObject;
/* 2124 */ 
/* 2125 */       this.name = this.environment.name;
/* 2126 */       this.isChild = !!context;
/* 2127 */       this.context = context || {
/* 2128 */         programs: [],
/* 2129 */         environments: []
/* 2130 */       };
/* 2131 */ 
/* 2132 */       this.preamble();
/* 2133 */ 
/* 2134 */       this.stackSlot = 0;
/* 2135 */       this.stackVars = [];
/* 2136 */       this.aliases = {};
/* 2137 */       this.registers = { list: [] };
/* 2138 */       this.hashes = [];
/* 2139 */       this.compileStack = [];
/* 2140 */       this.inlineStack = [];
/* 2141 */ 
/* 2142 */       this.compileChildren(environment, options);
/* 2143 */ 
/* 2144 */       this.useDepths = this.useDepths || environment.depths.list.length || this.options.compat;
/* 2145 */ 
/* 2146 */       var opcodes = environment.opcodes,
/* 2147 */           opcode,
/* 2148 */           i,
/* 2149 */           l;
/* 2150 */ 

/* handlebars-v2.0.0.js */

/* 2151 */       for (i = 0, l = opcodes.length; i < l; i++) {
/* 2152 */         opcode = opcodes[i];
/* 2153 */ 
/* 2154 */         this[opcode.opcode].apply(this, opcode.args);
/* 2155 */       }
/* 2156 */ 
/* 2157 */       // Flush any trailing content that might be pending.
/* 2158 */       this.pushSource('');
/* 2159 */ 
/* 2160 */       /* istanbul ignore next */
/* 2161 */       if (this.stackSlot || this.inlineStack.length || this.compileStack.length) {
/* 2162 */         throw new Exception('Compile completed with content left on stack');
/* 2163 */       }
/* 2164 */ 
/* 2165 */       var fn = this.createFunctionContext(asObject);
/* 2166 */       if (!this.isChild) {
/* 2167 */         var ret = {
/* 2168 */           compiler: this.compilerInfo(),
/* 2169 */           main: fn
/* 2170 */         };
/* 2171 */         var programs = this.context.programs;
/* 2172 */         for (i = 0, l = programs.length; i < l; i++) {
/* 2173 */           if (programs[i]) {
/* 2174 */             ret[i] = programs[i];
/* 2175 */           }
/* 2176 */         }
/* 2177 */ 
/* 2178 */         if (this.environment.usePartial) {
/* 2179 */           ret.usePartial = true;
/* 2180 */         }
/* 2181 */         if (this.options.data) {
/* 2182 */           ret.useData = true;
/* 2183 */         }
/* 2184 */         if (this.useDepths) {
/* 2185 */           ret.useDepths = true;
/* 2186 */         }
/* 2187 */         if (this.options.compat) {
/* 2188 */           ret.compat = true;
/* 2189 */         }
/* 2190 */ 
/* 2191 */         if (!asObject) {
/* 2192 */           ret.compiler = JSON.stringify(ret.compiler);
/* 2193 */           ret = this.objectLiteral(ret);
/* 2194 */         }
/* 2195 */ 
/* 2196 */         return ret;
/* 2197 */       } else {
/* 2198 */         return fn;
/* 2199 */       }
/* 2200 */     },

/* handlebars-v2.0.0.js */

/* 2201 */ 
/* 2202 */     preamble: function() {
/* 2203 */       // track the last context pushed into place to allow skipping the
/* 2204 */       // getContext opcode when it would be a noop
/* 2205 */       this.lastContext = 0;
/* 2206 */       this.source = [];
/* 2207 */     },
/* 2208 */ 
/* 2209 */     createFunctionContext: function(asObject) {
/* 2210 */       var varDeclarations = '';
/* 2211 */ 
/* 2212 */       var locals = this.stackVars.concat(this.registers.list);
/* 2213 */       if(locals.length > 0) {
/* 2214 */         varDeclarations += ", " + locals.join(", ");
/* 2215 */       }
/* 2216 */ 
/* 2217 */       // Generate minimizer alias mappings
/* 2218 */       for (var alias in this.aliases) {
/* 2219 */         if (this.aliases.hasOwnProperty(alias)) {
/* 2220 */           varDeclarations += ', ' + alias + '=' + this.aliases[alias];
/* 2221 */         }
/* 2222 */       }
/* 2223 */ 
/* 2224 */       var params = ["depth0", "helpers", "partials", "data"];
/* 2225 */ 
/* 2226 */       if (this.useDepths) {
/* 2227 */         params.push('depths');
/* 2228 */       }
/* 2229 */ 
/* 2230 */       // Perform a second pass over the output to merge content when possible
/* 2231 */       var source = this.mergeSource(varDeclarations);
/* 2232 */ 
/* 2233 */       if (asObject) {
/* 2234 */         params.push(source);
/* 2235 */ 
/* 2236 */         return Function.apply(this, params);
/* 2237 */       } else {
/* 2238 */         return 'function(' + params.join(',') + ') {\n  ' + source + '}';
/* 2239 */       }
/* 2240 */     },
/* 2241 */     mergeSource: function(varDeclarations) {
/* 2242 */       var source = '',
/* 2243 */           buffer,
/* 2244 */           appendOnly = !this.forceBuffer,
/* 2245 */           appendFirst;
/* 2246 */ 
/* 2247 */       for (var i = 0, len = this.source.length; i < len; i++) {
/* 2248 */         var line = this.source[i];
/* 2249 */         if (line.appendToBuffer) {
/* 2250 */           if (buffer) {

/* handlebars-v2.0.0.js */

/* 2251 */             buffer = buffer + '\n    + ' + line.content;
/* 2252 */           } else {
/* 2253 */             buffer = line.content;
/* 2254 */           }
/* 2255 */         } else {
/* 2256 */           if (buffer) {
/* 2257 */             if (!source) {
/* 2258 */               appendFirst = true;
/* 2259 */               source = buffer + ';\n  ';
/* 2260 */             } else {
/* 2261 */               source += 'buffer += ' + buffer + ';\n  ';
/* 2262 */             }
/* 2263 */             buffer = undefined;
/* 2264 */           }
/* 2265 */           source += line + '\n  ';
/* 2266 */ 
/* 2267 */           if (!this.environment.isSimple) {
/* 2268 */             appendOnly = false;
/* 2269 */           }
/* 2270 */         }
/* 2271 */       }
/* 2272 */ 
/* 2273 */       if (appendOnly) {
/* 2274 */         if (buffer || !source) {
/* 2275 */           source += 'return ' + (buffer || '""') + ';\n';
/* 2276 */         }
/* 2277 */       } else {
/* 2278 */         varDeclarations += ", buffer = " + (appendFirst ? '' : this.initializeBuffer());
/* 2279 */         if (buffer) {
/* 2280 */           source += 'return buffer + ' + buffer + ';\n';
/* 2281 */         } else {
/* 2282 */           source += 'return buffer;\n';
/* 2283 */         }
/* 2284 */       }
/* 2285 */ 
/* 2286 */       if (varDeclarations) {
/* 2287 */         source = 'var ' + varDeclarations.substring(2) + (appendFirst ? '' : ';\n  ') + source;
/* 2288 */       }
/* 2289 */ 
/* 2290 */       return source;
/* 2291 */     },
/* 2292 */ 
/* 2293 */     // [blockValue]
/* 2294 */     //
/* 2295 */     // On stack, before: hash, inverse, program, value
/* 2296 */     // On stack, after: return value of blockHelperMissing
/* 2297 */     //
/* 2298 */     // The purpose of this opcode is to take a block of the form
/* 2299 */     // `{{#this.foo}}...{{/this.foo}}`, resolve the value of `foo`, and
/* 2300 */     // replace it on the stack with the result of properly

/* handlebars-v2.0.0.js */

/* 2301 */     // invoking blockHelperMissing.
/* 2302 */     blockValue: function(name) {
/* 2303 */       this.aliases.blockHelperMissing = 'helpers.blockHelperMissing';
/* 2304 */ 
/* 2305 */       var params = [this.contextName(0)];
/* 2306 */       this.setupParams(name, 0, params);
/* 2307 */ 
/* 2308 */       var blockName = this.popStack();
/* 2309 */       params.splice(1, 0, blockName);
/* 2310 */ 
/* 2311 */       this.push('blockHelperMissing.call(' + params.join(', ') + ')');
/* 2312 */     },
/* 2313 */ 
/* 2314 */     // [ambiguousBlockValue]
/* 2315 */     //
/* 2316 */     // On stack, before: hash, inverse, program, value
/* 2317 */     // Compiler value, before: lastHelper=value of last found helper, if any
/* 2318 */     // On stack, after, if no lastHelper: same as [blockValue]
/* 2319 */     // On stack, after, if lastHelper: value
/* 2320 */     ambiguousBlockValue: function() {
/* 2321 */       this.aliases.blockHelperMissing = 'helpers.blockHelperMissing';
/* 2322 */ 
/* 2323 */       // We're being a bit cheeky and reusing the options value from the prior exec
/* 2324 */       var params = [this.contextName(0)];
/* 2325 */       this.setupParams('', 0, params, true);
/* 2326 */ 
/* 2327 */       this.flushInline();
/* 2328 */ 
/* 2329 */       var current = this.topStack();
/* 2330 */       params.splice(1, 0, current);
/* 2331 */ 
/* 2332 */       this.pushSource("if (!" + this.lastHelper + ") { " + current + " = blockHelperMissing.call(" + params.join(", ") + "); }");
/* 2333 */     },
/* 2334 */ 
/* 2335 */     // [appendContent]
/* 2336 */     //
/* 2337 */     // On stack, before: ...
/* 2338 */     // On stack, after: ...
/* 2339 */     //
/* 2340 */     // Appends the string value of `content` to the current buffer
/* 2341 */     appendContent: function(content) {
/* 2342 */       if (this.pendingContent) {
/* 2343 */         content = this.pendingContent + content;
/* 2344 */       }
/* 2345 */ 
/* 2346 */       this.pendingContent = content;
/* 2347 */     },
/* 2348 */ 
/* 2349 */     // [append]
/* 2350 */     //

/* handlebars-v2.0.0.js */

/* 2351 */     // On stack, before: value, ...
/* 2352 */     // On stack, after: ...
/* 2353 */     //
/* 2354 */     // Coerces `value` to a String and appends it to the current buffer.
/* 2355 */     //
/* 2356 */     // If `value` is truthy, or 0, it is coerced into a string and appended
/* 2357 */     // Otherwise, the empty string is appended
/* 2358 */     append: function() {
/* 2359 */       // Force anything that is inlined onto the stack so we don't have duplication
/* 2360 */       // when we examine local
/* 2361 */       this.flushInline();
/* 2362 */       var local = this.popStack();
/* 2363 */       this.pushSource('if (' + local + ' != null) { ' + this.appendToBuffer(local) + ' }');
/* 2364 */       if (this.environment.isSimple) {
/* 2365 */         this.pushSource("else { " + this.appendToBuffer("''") + " }");
/* 2366 */       }
/* 2367 */     },
/* 2368 */ 
/* 2369 */     // [appendEscaped]
/* 2370 */     //
/* 2371 */     // On stack, before: value, ...
/* 2372 */     // On stack, after: ...
/* 2373 */     //
/* 2374 */     // Escape `value` and append it to the buffer
/* 2375 */     appendEscaped: function() {
/* 2376 */       this.aliases.escapeExpression = 'this.escapeExpression';
/* 2377 */ 
/* 2378 */       this.pushSource(this.appendToBuffer("escapeExpression(" + this.popStack() + ")"));
/* 2379 */     },
/* 2380 */ 
/* 2381 */     // [getContext]
/* 2382 */     //
/* 2383 */     // On stack, before: ...
/* 2384 */     // On stack, after: ...
/* 2385 */     // Compiler value, after: lastContext=depth
/* 2386 */     //
/* 2387 */     // Set the value of the `lastContext` compiler value to the depth
/* 2388 */     getContext: function(depth) {
/* 2389 */       this.lastContext = depth;
/* 2390 */     },
/* 2391 */ 
/* 2392 */     // [pushContext]
/* 2393 */     //
/* 2394 */     // On stack, before: ...
/* 2395 */     // On stack, after: currentContext, ...
/* 2396 */     //
/* 2397 */     // Pushes the value of the current context onto the stack.
/* 2398 */     pushContext: function() {
/* 2399 */       this.pushStackLiteral(this.contextName(this.lastContext));
/* 2400 */     },

/* handlebars-v2.0.0.js */

/* 2401 */ 
/* 2402 */     // [lookupOnContext]
/* 2403 */     //
/* 2404 */     // On stack, before: ...
/* 2405 */     // On stack, after: currentContext[name], ...
/* 2406 */     //
/* 2407 */     // Looks up the value of `name` on the current context and pushes
/* 2408 */     // it onto the stack.
/* 2409 */     lookupOnContext: function(parts, falsy, scoped) {
/* 2410 */       /*jshint -W083 */
/* 2411 */       var i = 0,
/* 2412 */           len = parts.length;
/* 2413 */ 
/* 2414 */       if (!scoped && this.options.compat && !this.lastContext) {
/* 2415 */         // The depthed query is expected to handle the undefined logic for the root level that
/* 2416 */         // is implemented below, so we evaluate that directly in compat mode
/* 2417 */         this.push(this.depthedLookup(parts[i++]));
/* 2418 */       } else {
/* 2419 */         this.pushContext();
/* 2420 */       }
/* 2421 */ 
/* 2422 */       for (; i < len; i++) {
/* 2423 */         this.replaceStack(function(current) {
/* 2424 */           var lookup = this.nameLookup(current, parts[i], 'context');
/* 2425 */           // We want to ensure that zero and false are handled properly if the context (falsy flag)
/* 2426 */           // needs to have the special handling for these values.
/* 2427 */           if (!falsy) {
/* 2428 */             return ' != null ? ' + lookup + ' : ' + current;
/* 2429 */           } else {
/* 2430 */             // Otherwise we can use generic falsy handling
/* 2431 */             return ' && ' + lookup;
/* 2432 */           }
/* 2433 */         });
/* 2434 */       }
/* 2435 */     },
/* 2436 */ 
/* 2437 */     // [lookupData]
/* 2438 */     //
/* 2439 */     // On stack, before: ...
/* 2440 */     // On stack, after: data, ...
/* 2441 */     //
/* 2442 */     // Push the data lookup operator
/* 2443 */     lookupData: function(depth, parts) {
/* 2444 */       /*jshint -W083 */
/* 2445 */       if (!depth) {
/* 2446 */         this.pushStackLiteral('data');
/* 2447 */       } else {
/* 2448 */         this.pushStackLiteral('this.data(data, ' + depth + ')');
/* 2449 */       }
/* 2450 */ 

/* handlebars-v2.0.0.js */

/* 2451 */       var len = parts.length;
/* 2452 */       for (var i = 0; i < len; i++) {
/* 2453 */         this.replaceStack(function(current) {
/* 2454 */           return ' && ' + this.nameLookup(current, parts[i], 'data');
/* 2455 */         });
/* 2456 */       }
/* 2457 */     },
/* 2458 */ 
/* 2459 */     // [resolvePossibleLambda]
/* 2460 */     //
/* 2461 */     // On stack, before: value, ...
/* 2462 */     // On stack, after: resolved value, ...
/* 2463 */     //
/* 2464 */     // If the `value` is a lambda, replace it on the stack by
/* 2465 */     // the return value of the lambda
/* 2466 */     resolvePossibleLambda: function() {
/* 2467 */       this.aliases.lambda = 'this.lambda';
/* 2468 */ 
/* 2469 */       this.push('lambda(' + this.popStack() + ', ' + this.contextName(0) + ')');
/* 2470 */     },
/* 2471 */ 
/* 2472 */     // [pushStringParam]
/* 2473 */     //
/* 2474 */     // On stack, before: ...
/* 2475 */     // On stack, after: string, currentContext, ...
/* 2476 */     //
/* 2477 */     // This opcode is designed for use in string mode, which
/* 2478 */     // provides the string value of a parameter along with its
/* 2479 */     // depth rather than resolving it immediately.
/* 2480 */     pushStringParam: function(string, type) {
/* 2481 */       this.pushContext();
/* 2482 */       this.pushString(type);
/* 2483 */ 
/* 2484 */       // If it's a subexpression, the string result
/* 2485 */       // will be pushed after this opcode.
/* 2486 */       if (type !== 'sexpr') {
/* 2487 */         if (typeof string === 'string') {
/* 2488 */           this.pushString(string);
/* 2489 */         } else {
/* 2490 */           this.pushStackLiteral(string);
/* 2491 */         }
/* 2492 */       }
/* 2493 */     },
/* 2494 */ 
/* 2495 */     emptyHash: function() {
/* 2496 */       this.pushStackLiteral('{}');
/* 2497 */ 
/* 2498 */       if (this.trackIds) {
/* 2499 */         this.push('{}'); // hashIds
/* 2500 */       }

/* handlebars-v2.0.0.js */

/* 2501 */       if (this.stringParams) {
/* 2502 */         this.push('{}'); // hashContexts
/* 2503 */         this.push('{}'); // hashTypes
/* 2504 */       }
/* 2505 */     },
/* 2506 */     pushHash: function() {
/* 2507 */       if (this.hash) {
/* 2508 */         this.hashes.push(this.hash);
/* 2509 */       }
/* 2510 */       this.hash = {values: [], types: [], contexts: [], ids: []};
/* 2511 */     },
/* 2512 */     popHash: function() {
/* 2513 */       var hash = this.hash;
/* 2514 */       this.hash = this.hashes.pop();
/* 2515 */ 
/* 2516 */       if (this.trackIds) {
/* 2517 */         this.push('{' + hash.ids.join(',') + '}');
/* 2518 */       }
/* 2519 */       if (this.stringParams) {
/* 2520 */         this.push('{' + hash.contexts.join(',') + '}');
/* 2521 */         this.push('{' + hash.types.join(',') + '}');
/* 2522 */       }
/* 2523 */ 
/* 2524 */       this.push('{\n    ' + hash.values.join(',\n    ') + '\n  }');
/* 2525 */     },
/* 2526 */ 
/* 2527 */     // [pushString]
/* 2528 */     //
/* 2529 */     // On stack, before: ...
/* 2530 */     // On stack, after: quotedString(string), ...
/* 2531 */     //
/* 2532 */     // Push a quoted version of `string` onto the stack
/* 2533 */     pushString: function(string) {
/* 2534 */       this.pushStackLiteral(this.quotedString(string));
/* 2535 */     },
/* 2536 */ 
/* 2537 */     // [push]
/* 2538 */     //
/* 2539 */     // On stack, before: ...
/* 2540 */     // On stack, after: expr, ...
/* 2541 */     //
/* 2542 */     // Push an expression onto the stack
/* 2543 */     push: function(expr) {
/* 2544 */       this.inlineStack.push(expr);
/* 2545 */       return expr;
/* 2546 */     },
/* 2547 */ 
/* 2548 */     // [pushLiteral]
/* 2549 */     //
/* 2550 */     // On stack, before: ...

/* handlebars-v2.0.0.js */

/* 2551 */     // On stack, after: value, ...
/* 2552 */     //
/* 2553 */     // Pushes a value onto the stack. This operation prevents
/* 2554 */     // the compiler from creating a temporary variable to hold
/* 2555 */     // it.
/* 2556 */     pushLiteral: function(value) {
/* 2557 */       this.pushStackLiteral(value);
/* 2558 */     },
/* 2559 */ 
/* 2560 */     // [pushProgram]
/* 2561 */     //
/* 2562 */     // On stack, before: ...
/* 2563 */     // On stack, after: program(guid), ...
/* 2564 */     //
/* 2565 */     // Push a program expression onto the stack. This takes
/* 2566 */     // a compile-time guid and converts it into a runtime-accessible
/* 2567 */     // expression.
/* 2568 */     pushProgram: function(guid) {
/* 2569 */       if (guid != null) {
/* 2570 */         this.pushStackLiteral(this.programExpression(guid));
/* 2571 */       } else {
/* 2572 */         this.pushStackLiteral(null);
/* 2573 */       }
/* 2574 */     },
/* 2575 */ 
/* 2576 */     // [invokeHelper]
/* 2577 */     //
/* 2578 */     // On stack, before: hash, inverse, program, params..., ...
/* 2579 */     // On stack, after: result of helper invocation
/* 2580 */     //
/* 2581 */     // Pops off the helper's parameters, invokes the helper,
/* 2582 */     // and pushes the helper's return value onto the stack.
/* 2583 */     //
/* 2584 */     // If the helper is not found, `helperMissing` is called.
/* 2585 */     invokeHelper: function(paramSize, name, isSimple) {
/* 2586 */       this.aliases.helperMissing = 'helpers.helperMissing';
/* 2587 */ 
/* 2588 */       var nonHelper = this.popStack();
/* 2589 */       var helper = this.setupHelper(paramSize, name);
/* 2590 */ 
/* 2591 */       var lookup = (isSimple ? helper.name + ' || ' : '') + nonHelper + ' || helperMissing';
/* 2592 */       this.push('((' + lookup + ').call(' + helper.callParams + '))');
/* 2593 */     },
/* 2594 */ 
/* 2595 */     // [invokeKnownHelper]
/* 2596 */     //
/* 2597 */     // On stack, before: hash, inverse, program, params..., ...
/* 2598 */     // On stack, after: result of helper invocation
/* 2599 */     //
/* 2600 */     // This operation is used when the helper is known to exist,

/* handlebars-v2.0.0.js */

/* 2601 */     // so a `helperMissing` fallback is not required.
/* 2602 */     invokeKnownHelper: function(paramSize, name) {
/* 2603 */       var helper = this.setupHelper(paramSize, name);
/* 2604 */       this.push(helper.name + ".call(" + helper.callParams + ")");
/* 2605 */     },
/* 2606 */ 
/* 2607 */     // [invokeAmbiguous]
/* 2608 */     //
/* 2609 */     // On stack, before: hash, inverse, program, params..., ...
/* 2610 */     // On stack, after: result of disambiguation
/* 2611 */     //
/* 2612 */     // This operation is used when an expression like `{{foo}}`
/* 2613 */     // is provided, but we don't know at compile-time whether it
/* 2614 */     // is a helper or a path.
/* 2615 */     //
/* 2616 */     // This operation emits more code than the other options,
/* 2617 */     // and can be avoided by passing the `knownHelpers` and
/* 2618 */     // `knownHelpersOnly` flags at compile-time.
/* 2619 */     invokeAmbiguous: function(name, helperCall) {
/* 2620 */       this.aliases.functionType = '"function"';
/* 2621 */       this.aliases.helperMissing = 'helpers.helperMissing';
/* 2622 */       this.useRegister('helper');
/* 2623 */ 
/* 2624 */       var nonHelper = this.popStack();
/* 2625 */ 
/* 2626 */       this.emptyHash();
/* 2627 */       var helper = this.setupHelper(0, name, helperCall);
/* 2628 */ 
/* 2629 */       var helperName = this.lastHelper = this.nameLookup('helpers', name, 'helper');
/* 2630 */ 
/* 2631 */       this.push(
/* 2632 */         '((helper = (helper = ' + helperName + ' || ' + nonHelper + ') != null ? helper : helperMissing'
/* 2633 */           + (helper.paramsInit ? '),(' + helper.paramsInit : '') + '),'
/* 2634 */         + '(typeof helper === functionType ? helper.call(' + helper.callParams + ') : helper))');
/* 2635 */     },
/* 2636 */ 
/* 2637 */     // [invokePartial]
/* 2638 */     //
/* 2639 */     // On stack, before: context, ...
/* 2640 */     // On stack after: result of partial invocation
/* 2641 */     //
/* 2642 */     // This operation pops off a context, invokes a partial with that context,
/* 2643 */     // and pushes the result of the invocation back.
/* 2644 */     invokePartial: function(name, indent) {
/* 2645 */       var params = [this.nameLookup('partials', name, 'partial'), "'" + indent + "'", "'" + name + "'", this.popStack(), this.popStack(), "helpers", "partials"];
/* 2646 */ 
/* 2647 */       if (this.options.data) {
/* 2648 */         params.push("data");
/* 2649 */       } else if (this.options.compat) {
/* 2650 */         params.push('undefined');

/* handlebars-v2.0.0.js */

/* 2651 */       }
/* 2652 */       if (this.options.compat) {
/* 2653 */         params.push('depths');
/* 2654 */       }
/* 2655 */ 
/* 2656 */       this.push("this.invokePartial(" + params.join(", ") + ")");
/* 2657 */     },
/* 2658 */ 
/* 2659 */     // [assignToHash]
/* 2660 */     //
/* 2661 */     // On stack, before: value, ..., hash, ...
/* 2662 */     // On stack, after: ..., hash, ...
/* 2663 */     //
/* 2664 */     // Pops a value off the stack and assigns it to the current hash
/* 2665 */     assignToHash: function(key) {
/* 2666 */       var value = this.popStack(),
/* 2667 */           context,
/* 2668 */           type,
/* 2669 */           id;
/* 2670 */ 
/* 2671 */       if (this.trackIds) {
/* 2672 */         id = this.popStack();
/* 2673 */       }
/* 2674 */       if (this.stringParams) {
/* 2675 */         type = this.popStack();
/* 2676 */         context = this.popStack();
/* 2677 */       }
/* 2678 */ 
/* 2679 */       var hash = this.hash;
/* 2680 */       if (context) {
/* 2681 */         hash.contexts.push("'" + key + "': " + context);
/* 2682 */       }
/* 2683 */       if (type) {
/* 2684 */         hash.types.push("'" + key + "': " + type);
/* 2685 */       }
/* 2686 */       if (id) {
/* 2687 */         hash.ids.push("'" + key + "': " + id);
/* 2688 */       }
/* 2689 */       hash.values.push("'" + key + "': (" + value + ")");
/* 2690 */     },
/* 2691 */ 
/* 2692 */     pushId: function(type, name) {
/* 2693 */       if (type === 'ID' || type === 'DATA') {
/* 2694 */         this.pushString(name);
/* 2695 */       } else if (type === 'sexpr') {
/* 2696 */         this.pushStackLiteral('true');
/* 2697 */       } else {
/* 2698 */         this.pushStackLiteral('null');
/* 2699 */       }
/* 2700 */     },

/* handlebars-v2.0.0.js */

/* 2701 */ 
/* 2702 */     // HELPERS
/* 2703 */ 
/* 2704 */     compiler: JavaScriptCompiler,
/* 2705 */ 
/* 2706 */     compileChildren: function(environment, options) {
/* 2707 */       var children = environment.children, child, compiler;
/* 2708 */ 
/* 2709 */       for(var i=0, l=children.length; i<l; i++) {
/* 2710 */         child = children[i];
/* 2711 */         compiler = new this.compiler();
/* 2712 */ 
/* 2713 */         var index = this.matchExistingProgram(child);
/* 2714 */ 
/* 2715 */         if (index == null) {
/* 2716 */           this.context.programs.push('');     // Placeholder to prevent name conflicts for nested children
/* 2717 */           index = this.context.programs.length;
/* 2718 */           child.index = index;
/* 2719 */           child.name = 'program' + index;
/* 2720 */           this.context.programs[index] = compiler.compile(child, options, this.context, !this.precompile);
/* 2721 */           this.context.environments[index] = child;
/* 2722 */ 
/* 2723 */           this.useDepths = this.useDepths || compiler.useDepths;
/* 2724 */         } else {
/* 2725 */           child.index = index;
/* 2726 */           child.name = 'program' + index;
/* 2727 */         }
/* 2728 */       }
/* 2729 */     },
/* 2730 */     matchExistingProgram: function(child) {
/* 2731 */       for (var i = 0, len = this.context.environments.length; i < len; i++) {
/* 2732 */         var environment = this.context.environments[i];
/* 2733 */         if (environment && environment.equals(child)) {
/* 2734 */           return i;
/* 2735 */         }
/* 2736 */       }
/* 2737 */     },
/* 2738 */ 
/* 2739 */     programExpression: function(guid) {
/* 2740 */       var child = this.environment.children[guid],
/* 2741 */           depths = child.depths.list,
/* 2742 */           useDepths = this.useDepths,
/* 2743 */           depth;
/* 2744 */ 
/* 2745 */       var programParams = [child.index, 'data'];
/* 2746 */ 
/* 2747 */       if (useDepths) {
/* 2748 */         programParams.push('depths');
/* 2749 */       }
/* 2750 */ 

/* handlebars-v2.0.0.js */

/* 2751 */       return 'this.program(' + programParams.join(', ') + ')';
/* 2752 */     },
/* 2753 */ 
/* 2754 */     useRegister: function(name) {
/* 2755 */       if(!this.registers[name]) {
/* 2756 */         this.registers[name] = true;
/* 2757 */         this.registers.list.push(name);
/* 2758 */       }
/* 2759 */     },
/* 2760 */ 
/* 2761 */     pushStackLiteral: function(item) {
/* 2762 */       return this.push(new Literal(item));
/* 2763 */     },
/* 2764 */ 
/* 2765 */     pushSource: function(source) {
/* 2766 */       if (this.pendingContent) {
/* 2767 */         this.source.push(this.appendToBuffer(this.quotedString(this.pendingContent)));
/* 2768 */         this.pendingContent = undefined;
/* 2769 */       }
/* 2770 */ 
/* 2771 */       if (source) {
/* 2772 */         this.source.push(source);
/* 2773 */       }
/* 2774 */     },
/* 2775 */ 
/* 2776 */     pushStack: function(item) {
/* 2777 */       this.flushInline();
/* 2778 */ 
/* 2779 */       var stack = this.incrStack();
/* 2780 */       this.pushSource(stack + " = " + item + ";");
/* 2781 */       this.compileStack.push(stack);
/* 2782 */       return stack;
/* 2783 */     },
/* 2784 */ 
/* 2785 */     replaceStack: function(callback) {
/* 2786 */       var prefix = '',
/* 2787 */           inline = this.isInline(),
/* 2788 */           stack,
/* 2789 */           createdStack,
/* 2790 */           usedLiteral;
/* 2791 */ 
/* 2792 */       /* istanbul ignore next */
/* 2793 */       if (!this.isInline()) {
/* 2794 */         throw new Exception('replaceStack on non-inline');
/* 2795 */       }
/* 2796 */ 
/* 2797 */       // We want to merge the inline statement into the replacement statement via ','
/* 2798 */       var top = this.popStack(true);
/* 2799 */ 
/* 2800 */       if (top instanceof Literal) {

/* handlebars-v2.0.0.js */

/* 2801 */         // Literals do not need to be inlined
/* 2802 */         prefix = stack = top.value;
/* 2803 */         usedLiteral = true;
/* 2804 */       } else {
/* 2805 */         // Get or create the current stack name for use by the inline
/* 2806 */         createdStack = !this.stackSlot;
/* 2807 */         var name = !createdStack ? this.topStackName() : this.incrStack();
/* 2808 */ 
/* 2809 */         prefix = '(' + this.push(name) + ' = ' + top + ')';
/* 2810 */         stack = this.topStack();
/* 2811 */       }
/* 2812 */ 
/* 2813 */       var item = callback.call(this, stack);
/* 2814 */ 
/* 2815 */       if (!usedLiteral) {
/* 2816 */         this.popStack();
/* 2817 */       }
/* 2818 */       if (createdStack) {
/* 2819 */         this.stackSlot--;
/* 2820 */       }
/* 2821 */       this.push('(' + prefix + item + ')');
/* 2822 */     },
/* 2823 */ 
/* 2824 */     incrStack: function() {
/* 2825 */       this.stackSlot++;
/* 2826 */       if(this.stackSlot > this.stackVars.length) { this.stackVars.push("stack" + this.stackSlot); }
/* 2827 */       return this.topStackName();
/* 2828 */     },
/* 2829 */     topStackName: function() {
/* 2830 */       return "stack" + this.stackSlot;
/* 2831 */     },
/* 2832 */     flushInline: function() {
/* 2833 */       var inlineStack = this.inlineStack;
/* 2834 */       if (inlineStack.length) {
/* 2835 */         this.inlineStack = [];
/* 2836 */         for (var i = 0, len = inlineStack.length; i < len; i++) {
/* 2837 */           var entry = inlineStack[i];
/* 2838 */           if (entry instanceof Literal) {
/* 2839 */             this.compileStack.push(entry);
/* 2840 */           } else {
/* 2841 */             this.pushStack(entry);
/* 2842 */           }
/* 2843 */         }
/* 2844 */       }
/* 2845 */     },
/* 2846 */     isInline: function() {
/* 2847 */       return this.inlineStack.length;
/* 2848 */     },
/* 2849 */ 
/* 2850 */     popStack: function(wrapped) {

/* handlebars-v2.0.0.js */

/* 2851 */       var inline = this.isInline(),
/* 2852 */           item = (inline ? this.inlineStack : this.compileStack).pop();
/* 2853 */ 
/* 2854 */       if (!wrapped && (item instanceof Literal)) {
/* 2855 */         return item.value;
/* 2856 */       } else {
/* 2857 */         if (!inline) {
/* 2858 */           /* istanbul ignore next */
/* 2859 */           if (!this.stackSlot) {
/* 2860 */             throw new Exception('Invalid stack pop');
/* 2861 */           }
/* 2862 */           this.stackSlot--;
/* 2863 */         }
/* 2864 */         return item;
/* 2865 */       }
/* 2866 */     },
/* 2867 */ 
/* 2868 */     topStack: function() {
/* 2869 */       var stack = (this.isInline() ? this.inlineStack : this.compileStack),
/* 2870 */           item = stack[stack.length - 1];
/* 2871 */ 
/* 2872 */       if (item instanceof Literal) {
/* 2873 */         return item.value;
/* 2874 */       } else {
/* 2875 */         return item;
/* 2876 */       }
/* 2877 */     },
/* 2878 */ 
/* 2879 */     contextName: function(context) {
/* 2880 */       if (this.useDepths && context) {
/* 2881 */         return 'depths[' + context + ']';
/* 2882 */       } else {
/* 2883 */         return 'depth' + context;
/* 2884 */       }
/* 2885 */     },
/* 2886 */ 
/* 2887 */     quotedString: function(str) {
/* 2888 */       return '"' + str
/* 2889 */         .replace(/\\/g, '\\\\')
/* 2890 */         .replace(/"/g, '\\"')
/* 2891 */         .replace(/\n/g, '\\n')
/* 2892 */         .replace(/\r/g, '\\r')
/* 2893 */         .replace(/\u2028/g, '\\u2028')   // Per Ecma-262 7.3 + 7.8.4
/* 2894 */         .replace(/\u2029/g, '\\u2029') + '"';
/* 2895 */     },
/* 2896 */ 
/* 2897 */     objectLiteral: function(obj) {
/* 2898 */       var pairs = [];
/* 2899 */ 
/* 2900 */       for (var key in obj) {

/* handlebars-v2.0.0.js */

/* 2901 */         if (obj.hasOwnProperty(key)) {
/* 2902 */           pairs.push(this.quotedString(key) + ':' + obj[key]);
/* 2903 */         }
/* 2904 */       }
/* 2905 */ 
/* 2906 */       return '{' + pairs.join(',') + '}';
/* 2907 */     },
/* 2908 */ 
/* 2909 */     setupHelper: function(paramSize, name, blockHelper) {
/* 2910 */       var params = [],
/* 2911 */           paramsInit = this.setupParams(name, paramSize, params, blockHelper);
/* 2912 */       var foundHelper = this.nameLookup('helpers', name, 'helper');
/* 2913 */ 
/* 2914 */       return {
/* 2915 */         params: params,
/* 2916 */         paramsInit: paramsInit,
/* 2917 */         name: foundHelper,
/* 2918 */         callParams: [this.contextName(0)].concat(params).join(", ")
/* 2919 */       };
/* 2920 */     },
/* 2921 */ 
/* 2922 */     setupOptions: function(helper, paramSize, params) {
/* 2923 */       var options = {}, contexts = [], types = [], ids = [], param, inverse, program;
/* 2924 */ 
/* 2925 */       options.name = this.quotedString(helper);
/* 2926 */       options.hash = this.popStack();
/* 2927 */ 
/* 2928 */       if (this.trackIds) {
/* 2929 */         options.hashIds = this.popStack();
/* 2930 */       }
/* 2931 */       if (this.stringParams) {
/* 2932 */         options.hashTypes = this.popStack();
/* 2933 */         options.hashContexts = this.popStack();
/* 2934 */       }
/* 2935 */ 
/* 2936 */       inverse = this.popStack();
/* 2937 */       program = this.popStack();
/* 2938 */ 
/* 2939 */       // Avoid setting fn and inverse if neither are set. This allows
/* 2940 */       // helpers to do a check for `if (options.fn)`
/* 2941 */       if (program || inverse) {
/* 2942 */         if (!program) {
/* 2943 */           program = 'this.noop';
/* 2944 */         }
/* 2945 */ 
/* 2946 */         if (!inverse) {
/* 2947 */           inverse = 'this.noop';
/* 2948 */         }
/* 2949 */ 
/* 2950 */         options.fn = program;

/* handlebars-v2.0.0.js */

/* 2951 */         options.inverse = inverse;
/* 2952 */       }
/* 2953 */ 
/* 2954 */       // The parameters go on to the stack in order (making sure that they are evaluated in order)
/* 2955 */       // so we need to pop them off the stack in reverse order
/* 2956 */       var i = paramSize;
/* 2957 */       while (i--) {
/* 2958 */         param = this.popStack();
/* 2959 */         params[i] = param;
/* 2960 */ 
/* 2961 */         if (this.trackIds) {
/* 2962 */           ids[i] = this.popStack();
/* 2963 */         }
/* 2964 */         if (this.stringParams) {
/* 2965 */           types[i] = this.popStack();
/* 2966 */           contexts[i] = this.popStack();
/* 2967 */         }
/* 2968 */       }
/* 2969 */ 
/* 2970 */       if (this.trackIds) {
/* 2971 */         options.ids = "[" + ids.join(",") + "]";
/* 2972 */       }
/* 2973 */       if (this.stringParams) {
/* 2974 */         options.types = "[" + types.join(",") + "]";
/* 2975 */         options.contexts = "[" + contexts.join(",") + "]";
/* 2976 */       }
/* 2977 */ 
/* 2978 */       if (this.options.data) {
/* 2979 */         options.data = "data";
/* 2980 */       }
/* 2981 */ 
/* 2982 */       return options;
/* 2983 */     },
/* 2984 */ 
/* 2985 */     // the params and contexts arguments are passed in arrays
/* 2986 */     // to fill in
/* 2987 */     setupParams: function(helperName, paramSize, params, useRegister) {
/* 2988 */       var options = this.objectLiteral(this.setupOptions(helperName, paramSize, params));
/* 2989 */ 
/* 2990 */       if (useRegister) {
/* 2991 */         this.useRegister('options');
/* 2992 */         params.push('options');
/* 2993 */         return 'options=' + options;
/* 2994 */       } else {
/* 2995 */         params.push(options);
/* 2996 */         return '';
/* 2997 */       }
/* 2998 */     }
/* 2999 */   };
/* 3000 */ 

/* handlebars-v2.0.0.js */

/* 3001 */   var reservedWords = (
/* 3002 */     "break else new var" +
/* 3003 */     " case finally return void" +
/* 3004 */     " catch for switch while" +
/* 3005 */     " continue function this with" +
/* 3006 */     " default if throw" +
/* 3007 */     " delete in try" +
/* 3008 */     " do instanceof typeof" +
/* 3009 */     " abstract enum int short" +
/* 3010 */     " boolean export interface static" +
/* 3011 */     " byte extends long super" +
/* 3012 */     " char final native synchronized" +
/* 3013 */     " class float package throws" +
/* 3014 */     " const goto private transient" +
/* 3015 */     " debugger implements protected volatile" +
/* 3016 */     " double import public let yield"
/* 3017 */   ).split(" ");
/* 3018 */ 
/* 3019 */   var compilerWords = JavaScriptCompiler.RESERVED_WORDS = {};
/* 3020 */ 
/* 3021 */   for(var i=0, l=reservedWords.length; i<l; i++) {
/* 3022 */     compilerWords[reservedWords[i]] = true;
/* 3023 */   }
/* 3024 */ 
/* 3025 */   JavaScriptCompiler.isValidJavaScriptVariableName = function(name) {
/* 3026 */     return !JavaScriptCompiler.RESERVED_WORDS[name] && /^[a-zA-Z_$][0-9a-zA-Z_$]*$/.test(name);
/* 3027 */   };
/* 3028 */ 
/* 3029 */   __exports__ = JavaScriptCompiler;
/* 3030 */   return __exports__;
/* 3031 */ })(__module2__, __module5__);
/* 3032 */ 
/* 3033 */ // handlebars.js
/* 3034 */ var __module0__ = (function(__dependency1__, __dependency2__, __dependency3__, __dependency4__, __dependency5__) {
/* 3035 */   "use strict";
/* 3036 */   var __exports__;
/* 3037 */   /*globals Handlebars: true */
/* 3038 */   var Handlebars = __dependency1__;
/* 3039 */ 
/* 3040 */   // Compiler imports
/* 3041 */   var AST = __dependency2__;
/* 3042 */   var Parser = __dependency3__.parser;
/* 3043 */   var parse = __dependency3__.parse;
/* 3044 */   var Compiler = __dependency4__.Compiler;
/* 3045 */   var compile = __dependency4__.compile;
/* 3046 */   var precompile = __dependency4__.precompile;
/* 3047 */   var JavaScriptCompiler = __dependency5__;
/* 3048 */ 
/* 3049 */   var _create = Handlebars.create;
/* 3050 */   var create = function() {

/* handlebars-v2.0.0.js */

/* 3051 */     var hb = _create();
/* 3052 */ 
/* 3053 */     hb.compile = function(input, options) {
/* 3054 */       return compile(input, options, hb);
/* 3055 */     };
/* 3056 */     hb.precompile = function (input, options) {
/* 3057 */       return precompile(input, options, hb);
/* 3058 */     };
/* 3059 */ 
/* 3060 */     hb.AST = AST;
/* 3061 */     hb.Compiler = Compiler;
/* 3062 */     hb.JavaScriptCompiler = JavaScriptCompiler;
/* 3063 */     hb.Parser = Parser;
/* 3064 */     hb.parse = parse;
/* 3065 */ 
/* 3066 */     return hb;
/* 3067 */   };
/* 3068 */ 
/* 3069 */   Handlebars = create();
/* 3070 */   Handlebars.create = create;
/* 3071 */ 
/* 3072 */   Handlebars['default'] = Handlebars;
/* 3073 */ 
/* 3074 */   __exports__ = Handlebars;
/* 3075 */   return __exports__;
/* 3076 */ })(__module1__, __module7__, __module8__, __module11__, __module12__);
/* 3077 */ 
/* 3078 */   return __module0__;
/* 3079 */ }));
/* 3080 */ 

;
/* typeahead.js */

/* 1    */ /*!
/* 2    *|  * typeahead.js 0.10.2
/* 3    *|  * https://github.com/twitter/typeahead.js
/* 4    *|  * Copyright 2013-2014 Twitter, Inc. and other contributors; Licensed MIT
/* 5    *|  */
/* 6    */ 
/* 7    */ ! function(a) {
/* 8    */     var b = {
/* 9    */         isMsie: function() {
/* 10   */             return /(msie|trident)/i.test(navigator.userAgent) ? navigator.userAgent.match(/(msie |rv:)(\d+(.\d+)?)/i)[2] : !1
/* 11   */         },
/* 12   */         isBlankString: function(a) {
/* 13   */             return !a || /^\s*$/.test(a)
/* 14   */         },
/* 15   */         escapeRegExChars: function(a) {
/* 16   */             return a.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&")
/* 17   */         },
/* 18   */         isString: function(a) {
/* 19   */             return "string" == typeof a
/* 20   */         },
/* 21   */         isNumber: function(a) {
/* 22   */             return "number" == typeof a
/* 23   */         },
/* 24   */         isArray: a.isArray,
/* 25   */         isFunction: a.isFunction,
/* 26   */         isObject: a.isPlainObject,
/* 27   */         isUndefined: function(a) {
/* 28   */             return "undefined" == typeof a
/* 29   */         },
/* 30   */         bind: a.proxy,
/* 31   */         each: function(b, c) {
/* 32   */             function d(a, b) {
/* 33   */                 return c(b, a)
/* 34   */             }
/* 35   */             a.each(b, d)
/* 36   */         },
/* 37   */         map: a.map,
/* 38   */         filter: a.grep,
/* 39   */         every: function(b, c) {
/* 40   */             var d = !0;
/* 41   */             return b ? (a.each(b, function(a, e) {
/* 42   */                 return (d = c.call(null, e, a, b)) ? void 0 : !1
/* 43   */             }), !! d) : d
/* 44   */         },
/* 45   */         some: function(b, c) {
/* 46   */             var d = !1;
/* 47   */             return b ? (a.each(b, function(a, e) {
/* 48   */                 return (d = c.call(null, e, a, b)) ? !1 : void 0
/* 49   */             }), !! d) : d
/* 50   */         },

/* typeahead.js */

/* 51   */         mixin: a.extend,
/* 52   */         getUniqueId: function() {
/* 53   */             var a = 0;
/* 54   */             return function() {
/* 55   */                 return a++
/* 56   */             }
/* 57   */         }(),
/* 58   */         templatify: function(b) {
/* 59   */             function c() {
/* 60   */                 return String(b)
/* 61   */             }
/* 62   */             return a.isFunction(b) ? b : c
/* 63   */         },
/* 64   */         defer: function(a) {
/* 65   */             setTimeout(a, 0)
/* 66   */         },
/* 67   */         debounce: function(a, b, c) {
/* 68   */             var d, e;
/* 69   */             return function() {
/* 70   */                 var f, g, h = this,
/* 71   */                     i = arguments;
/* 72   */                 return f = function() {
/* 73   */                     d = null, c || (e = a.apply(h, i))
/* 74   */                 }, g = c && !d, clearTimeout(d), d = setTimeout(f, b), g && (e = a.apply(h, i)), e
/* 75   */             }
/* 76   */         },
/* 77   */         throttle: function(a, b) {
/* 78   */             var c, d, e, f, g, h;
/* 79   */             return g = 0, h = function() {
/* 80   */                 g = new Date, e = null, f = a.apply(c, d)
/* 81   */             },
/* 82   */             function() {
/* 83   */                 var i = new Date,
/* 84   */                     j = b - (i - g);
/* 85   */                 return c = this, d = arguments, 0 >= j ? (clearTimeout(e), e = null, g = i, f = a.apply(c, d)) : e || (e = setTimeout(h, j)), f
/* 86   */             }
/* 87   */         },
/* 88   */         noop: function() {}
/* 89   */     }, c = "0.10.2",
/* 90   */         d = function() {
/* 91   */             function a(a) {
/* 92   */                 return a.split(/\s+/)
/* 93   */             }
/* 94   */ 
/* 95   */             function b(a) {
/* 96   */                 return a.split(/\W+/)
/* 97   */             }
/* 98   */ 
/* 99   */             function c(a) {
/* 100  */                 return function(b) {

/* typeahead.js */

/* 101  */                     return function(c) {
/* 102  */                         return a(c[b])
/* 103  */                     }
/* 104  */                 }
/* 105  */             }
/* 106  */             return {
/* 107  */                 nonword: b,
/* 108  */                 whitespace: a,
/* 109  */                 obj: {
/* 110  */                     nonword: c(b),
/* 111  */                     whitespace: c(a)
/* 112  */                 }
/* 113  */             }
/* 114  */         }(),
/* 115  */         e = function() {
/* 116  */             function a(a) {
/* 117  */                 this.maxSize = a || 100, this.size = 0, this.hash = {}, this.list = new c
/* 118  */             }
/* 119  */ 
/* 120  */             function c() {
/* 121  */                 this.head = this.tail = null
/* 122  */             }
/* 123  */ 
/* 124  */             function d(a, b) {
/* 125  */                 this.key = a, this.val = b, this.prev = this.next = null
/* 126  */             }
/* 127  */             return b.mixin(a.prototype, {
/* 128  */                 set: function(a, b) {
/* 129  */                     var c, e = this.list.tail;
/* 130  */                     this.size >= this.maxSize && (this.list.remove(e), delete this.hash[e.key]), (c = this.hash[a]) ? (c.val = b, this.list.moveToFront(c)) : (c = new d(a, b), this.list.add(c), this.hash[a] = c, this.size++)
/* 131  */                 },
/* 132  */                 get: function(a) {
/* 133  */                     var b = this.hash[a];
/* 134  */                     return b ? (this.list.moveToFront(b), b.val) : void 0
/* 135  */                 }
/* 136  */             }), b.mixin(c.prototype, {
/* 137  */                 add: function(a) {
/* 138  */                     this.head && (a.next = this.head, this.head.prev = a), this.head = a, this.tail = this.tail || a
/* 139  */                 },
/* 140  */                 remove: function(a) {
/* 141  */                     a.prev ? a.prev.next = a.next : this.head = a.next, a.next ? a.next.prev = a.prev : this.tail = a.prev
/* 142  */                 },
/* 143  */                 moveToFront: function(a) {
/* 144  */                     this.remove(a), this.add(a)
/* 145  */                 }
/* 146  */             }), a
/* 147  */         }(),
/* 148  */         f = function() {
/* 149  */             function a(a) {
/* 150  */                 this.prefix = ["__", a, "__"].join(""), this.ttlKey = "__ttl__", this.keyMatcher = new RegExp("^" + this.prefix)

/* typeahead.js */

/* 151  */             }
/* 152  */ 
/* 153  */             function c() {
/* 154  */                 return (new Date).getTime()
/* 155  */             }
/* 156  */ 
/* 157  */             function d(a) {
/* 158  */                 return JSON.stringify(b.isUndefined(a) ? null : a)
/* 159  */             }
/* 160  */ 
/* 161  */             function e(a) {
/* 162  */                 return JSON.parse(a)
/* 163  */             }
/* 164  */             var f, g;
/* 165  */             try {
/* 166  */                 f = window.localStorage, f.setItem("~~~", "!"), f.removeItem("~~~")
/* 167  */             } catch (h) {
/* 168  */                 f = null
/* 169  */             }
/* 170  */             return g = f && window.JSON ? {
/* 171  */                 _prefix: function(a) {
/* 172  */                     return this.prefix + a
/* 173  */                 },
/* 174  */                 _ttlKey: function(a) {
/* 175  */                     return this._prefix(a) + this.ttlKey
/* 176  */                 },
/* 177  */                 get: function(a) {
/* 178  */                     return this.isExpired(a) && this.remove(a), e(f.getItem(this._prefix(a)))
/* 179  */                 },
/* 180  */                 set: function(a, e, g) {
/* 181  */                     return b.isNumber(g) ? f.setItem(this._ttlKey(a), d(c() + g)) : f.removeItem(this._ttlKey(a)), f.setItem(this._prefix(a), d(e))
/* 182  */                 },
/* 183  */                 remove: function(a) {
/* 184  */                     return f.removeItem(this._ttlKey(a)), f.removeItem(this._prefix(a)), this
/* 185  */                 },
/* 186  */                 clear: function() {
/* 187  */                     var a, b, c = [],
/* 188  */                         d = f.length;
/* 189  */                     for (a = 0; d > a; a++)(b = f.key(a)).match(this.keyMatcher) && c.push(b.replace(this.keyMatcher, ""));
/* 190  */                     for (a = c.length; a--;) this.remove(c[a]);
/* 191  */                     return this
/* 192  */                 },
/* 193  */                 isExpired: function(a) {
/* 194  */                     var d = e(f.getItem(this._ttlKey(a)));
/* 195  */                     return b.isNumber(d) && c() > d ? !0 : !1
/* 196  */                 }
/* 197  */             } : {
/* 198  */                 get: b.noop,
/* 199  */                 set: b.noop,
/* 200  */                 remove: b.noop,

/* typeahead.js */

/* 201  */                 clear: b.noop,
/* 202  */                 isExpired: b.noop
/* 203  */             }, b.mixin(a.prototype, g), a
/* 204  */         }(),
/* 205  */         g = function() {
/* 206  */             function c(b) {
/* 207  */                 b = b || {}, this._send = b.transport ? d(b.transport) : a.ajax, this._get = b.rateLimiter ? b.rateLimiter(this._get) : this._get
/* 208  */             }
/* 209  */ 
/* 210  */             function d(c) {
/* 211  */                 return function(d, e) {
/* 212  */                     function f(a) {
/* 213  */                         b.defer(function() {
/* 214  */                             h.resolve(a)
/* 215  */                         })
/* 216  */                     }
/* 217  */ 
/* 218  */                     function g(a) {
/* 219  */                         b.defer(function() {
/* 220  */                             h.reject(a)
/* 221  */                         })
/* 222  */                     }
/* 223  */                     var h = a.Deferred();
/* 224  */                     return c(d, e, f, g), h
/* 225  */                 }
/* 226  */             }
/* 227  */             var f = 0,
/* 228  */                 g = {}, h = 6,
/* 229  */                 i = new e(10);
/* 230  */             return c.setMaxPendingRequests = function(a) {
/* 231  */                 h = a
/* 232  */             }, c.resetCache = function() {
/* 233  */                 i = new e(10)
/* 234  */             }, b.mixin(c.prototype, {
/* 235  */                 _get: function(a, b, c) {
/* 236  */                     function d(b) {
/* 237  */                         c && c(null, b), i.set(a, b)
/* 238  */                     }
/* 239  */ 
/* 240  */                     function e() {
/* 241  */                         c && c(!0)
/* 242  */                     }
/* 243  */ 
/* 244  */                     function j() {
/* 245  */                         f--, delete g[a], l.onDeckRequestArgs && (l._get.apply(l, l.onDeckRequestArgs), l.onDeckRequestArgs = null)
/* 246  */                     }
/* 247  */                     var k, l = this;
/* 248  */                     (k = g[a]) ? k.done(d).fail(e) : h > f ? (f++, g[a] = this._send(a, b).done(d).fail(e).always(j)) : this.onDeckRequestArgs = [].slice.call(arguments, 0)
/* 249  */                 },
/* 250  */                 get: function(a, c, d) {

/* typeahead.js */

/* 251  */                     var e;
/* 252  */                     return b.isFunction(c) && (d = c, c = {}), (e = i.get(a)) ? b.defer(function() {
/* 253  */                         d && d(null, e)
/* 254  */                     }) : this._get(a, c, d), !! e
/* 255  */                 }
/* 256  */             }), c
/* 257  */         }(),
/* 258  */         h = function() {
/* 259  */             function c(b) {
/* 260  */                 b = b || {}, b.datumTokenizer && b.queryTokenizer || a.error("datumTokenizer and queryTokenizer are both required"), this.datumTokenizer = b.datumTokenizer, this.queryTokenizer = b.queryTokenizer, this.reset()
/* 261  */             }
/* 262  */ 
/* 263  */             function d(a) {
/* 264  */                 return a = b.filter(a, function(a) {
/* 265  */                     return !!a
/* 266  */                 }), a = b.map(a, function(a) {
/* 267  */                     return a.toLowerCase()
/* 268  */                 })
/* 269  */             }
/* 270  */ 
/* 271  */             function e() {
/* 272  */                 return {
/* 273  */                     ids: [],
/* 274  */                     children: {}
/* 275  */                 }
/* 276  */             }
/* 277  */ 
/* 278  */             function f(a) {
/* 279  */                 for (var b = {}, c = [], d = 0; d < a.length; d++) b[a[d]] || (b[a[d]] = !0, c.push(a[d]));
/* 280  */                 return c
/* 281  */             }
/* 282  */ 
/* 283  */             function g(a, b) {
/* 284  */                 function c(a, b) {
/* 285  */                     return a - b
/* 286  */                 }
/* 287  */                 var d = 0,
/* 288  */                     e = 0,
/* 289  */                     f = [];
/* 290  */                 for (a = a.sort(c), b = b.sort(c); d < a.length && e < b.length;) a[d] < b[e] ? d++ : a[d] > b[e] ? e++ : (f.push(a[d]), d++, e++);
/* 291  */                 return f
/* 292  */             }
/* 293  */             return b.mixin(c.prototype, {
/* 294  */                 bootstrap: function(a) {
/* 295  */                     this.datums = a.datums, this.trie = a.trie
/* 296  */                 },
/* 297  */                 add: function(a) {
/* 298  */                     var c = this;
/* 299  */                     a = b.isArray(a) ? a : [a], b.each(a, function(a) {
/* 300  */                         var f, g;

/* typeahead.js */

/* 301  */                         f = c.datums.push(a) - 1, g = d(c.datumTokenizer(a)), b.each(g, function(a) {
/* 302  */                             var b, d, g;
/* 303  */                             for (b = c.trie, d = a.split(""); g = d.shift();) b = b.children[g] || (b.children[g] = e()), b.ids.push(f)
/* 304  */                         })
/* 305  */                     })
/* 306  */                 },
/* 307  */                 get: function(a) {
/* 308  */                     var c, e, h = this;
/* 309  */                     return c = d(this.queryTokenizer(a)), b.each(c, function(a) {
/* 310  */                         var b, c, d, f;
/* 311  */                         if (e && 0 === e.length) return !1;
/* 312  */                         for (b = h.trie, c = a.split(""); b && (d = c.shift());) b = b.children[d];
/* 313  */                         return b && 0 === c.length ? (f = b.ids.slice(0), void(e = e ? g(e, f) : f)) : (e = [], !1)
/* 314  */                     }), e ? b.map(f(e), function(a) {
/* 315  */                         return h.datums[a]
/* 316  */                     }) : []
/* 317  */                 },
/* 318  */                 reset: function() {
/* 319  */                     this.datums = [], this.trie = e()
/* 320  */                 },
/* 321  */                 serialize: function() {
/* 322  */                     return {
/* 323  */                         datums: this.datums,
/* 324  */                         trie: this.trie
/* 325  */                     }
/* 326  */                 }
/* 327  */             }), c
/* 328  */         }(),
/* 329  */         i = function() {
/* 330  */             function d(a) {
/* 331  */                 return a.local || null
/* 332  */             }
/* 333  */ 
/* 334  */             function e(d) {
/* 335  */                 var e, f;
/* 336  */                 return f = {
/* 337  */                     url: null,
/* 338  */                     thumbprint: "",
/* 339  */                     ttl: 864e5,
/* 340  */                     filter: null,
/* 341  */                     ajax: {}
/* 342  */                 }, (e = d.prefetch || null) && (e = b.isString(e) ? {
/* 343  */                     url: e
/* 344  */                 } : e, e = b.mixin(f, e), e.thumbprint = c + e.thumbprint, e.ajax.type = e.ajax.type || "GET", e.ajax.dataType = e.ajax.dataType || "json", !e.url && a.error("prefetch requires url to be set")), e
/* 345  */             }
/* 346  */ 
/* 347  */             function f(c) {
/* 348  */                 function d(a) {
/* 349  */                     return function(c) {
/* 350  */                         return b.debounce(c, a)

/* typeahead.js */

/* 351  */                     }
/* 352  */                 }
/* 353  */ 
/* 354  */                 function e(a) {
/* 355  */                     return function(c) {
/* 356  */                         return b.throttle(c, a)
/* 357  */                     }
/* 358  */                 }
/* 359  */                 var f, g;
/* 360  */                 return g = {
/* 361  */                     url: null,
/* 362  */                     wildcard: "%QUERY",
/* 363  */                     replace: null,
/* 364  */                     rateLimitBy: "debounce",
/* 365  */                     rateLimitWait: 300,
/* 366  */                     send: null,
/* 367  */                     filter: null,
/* 368  */                     ajax: {}
/* 369  */                 }, (f = c.remote || null) && (f = b.isString(f) ? {
/* 370  */                     url: f
/* 371  */                 } : f, f = b.mixin(g, f), f.rateLimiter = /^throttle$/i.test(f.rateLimitBy) ? e(f.rateLimitWait) : d(f.rateLimitWait), f.ajax.type = f.ajax.type || "GET", f.ajax.dataType = f.ajax.dataType || "json", delete f.rateLimitBy, delete f.rateLimitWait, !f.url && a.error("remote requires url to be set")), f
/* 372  */             }
/* 373  */             return {
/* 374  */                 local: d,
/* 375  */                 prefetch: e,
/* 376  */                 remote: f
/* 377  */             }
/* 378  */         }();
/* 379  */     ! function(c) {
/* 380  */         function e(b) {
/* 381  */             b && (b.local || b.prefetch || b.remote) || a.error("one of local, prefetch, or remote is required"), this.limit = b.limit || 5, this.sorter = j(b.sorter), this.dupDetector = b.dupDetector || k, this.local = i.local(b), this.prefetch = i.prefetch(b), this.remote = i.remote(b), this.cacheKey = this.prefetch ? this.prefetch.cacheKey || this.prefetch.url : null, this.index = new h({
/* 382  */                 datumTokenizer: b.datumTokenizer,
/* 383  */                 queryTokenizer: b.queryTokenizer
/* 384  */             }), this.storage = this.cacheKey ? new f(this.cacheKey) : null
/* 385  */         }
/* 386  */ 
/* 387  */         function j(a) {
/* 388  */             function c(b) {
/* 389  */                 return b.sort(a)
/* 390  */             }
/* 391  */ 
/* 392  */             function d(a) {
/* 393  */                 return a
/* 394  */             }
/* 395  */             return b.isFunction(a) ? c : d
/* 396  */         }
/* 397  */ 
/* 398  */         function k() {
/* 399  */             return !1
/* 400  */         }

/* typeahead.js */

/* 401  */         var l, m;
/* 402  */         return l = c.Bloodhound, m = {
/* 403  */             data: "data",
/* 404  */             protocol: "protocol",
/* 405  */             thumbprint: "thumbprint"
/* 406  */         }, c.Bloodhound = e, e.noConflict = function() {
/* 407  */             return c.Bloodhound = l, e
/* 408  */         }, e.tokenizers = d, b.mixin(e.prototype, {
/* 409  */             _loadPrefetch: function(b) {
/* 410  */                 function c(a) {
/* 411  */                     f.clear(), f.add(b.filter ? b.filter(a) : a), f._saveToStorage(f.index.serialize(), b.thumbprint, b.ttl)
/* 412  */                 }
/* 413  */                 var d, e, f = this;
/* 414  */                 return (d = this._readFromStorage(b.thumbprint)) ? (this.index.bootstrap(d), e = a.Deferred().resolve()) : e = a.ajax(b.url, b.ajax).done(c), e
/* 415  */             },
/* 416  */             _getFromRemote: function(a, b) {
/* 417  */                 function c(a, c) {
/* 418  */                     b(a ? [] : f.remote.filter ? f.remote.filter(c) : c)
/* 419  */                 }
/* 420  */                 var d, e, f = this;
/* 421  */                 return a = a || "", e = encodeURIComponent(a), d = this.remote.replace ? this.remote.replace(this.remote.url, a) : this.remote.url.replace(this.remote.wildcard, e), this.transport.get(d, this.remote.ajax, c)
/* 422  */             },
/* 423  */             _saveToStorage: function(a, b, c) {
/* 424  */                 this.storage && (this.storage.set(m.data, a, c), this.storage.set(m.protocol, location.protocol, c), this.storage.set(m.thumbprint, b, c))
/* 425  */             },
/* 426  */             _readFromStorage: function(a) {
/* 427  */                 var b, c = {};
/* 428  */                 return this.storage && (c.data = this.storage.get(m.data), c.protocol = this.storage.get(m.protocol), c.thumbprint = this.storage.get(m.thumbprint)), b = c.thumbprint !== a || c.protocol !== location.protocol, c.data && !b ? c.data : null
/* 429  */             },
/* 430  */             _initialize: function() {
/* 431  */                 function c() {
/* 432  */                     e.add(b.isFunction(f) ? f() : f)
/* 433  */                 }
/* 434  */                 var d, e = this,
/* 435  */                     f = this.local;
/* 436  */                 return d = this.prefetch ? this._loadPrefetch(this.prefetch) : a.Deferred().resolve(), f && d.done(c), this.transport = this.remote ? new g(this.remote) : null, this.initPromise = d.promise()
/* 437  */             },
/* 438  */             initialize: function(a) {
/* 439  */                 return !this.initPromise || a ? this._initialize() : this.initPromise
/* 440  */             },
/* 441  */             add: function(a) {
/* 442  */                 this.index.add(a)
/* 443  */             },
/* 444  */             get: function(a, c) {
/* 445  */                 function d(a) {
/* 446  */                     var d = f.slice(0);
/* 447  */                     b.each(a, function(a) {
/* 448  */                         var c;
/* 449  */                         return c = b.some(d, function(b) {
/* 450  */                             return e.dupDetector(a, b)

/* typeahead.js */

/* 451  */                         }), !c && d.push(a), d.length < e.limit
/* 452  */                     }), c && c(e.sorter(d))
/* 453  */                 }
/* 454  */                 var e = this,
/* 455  */                     f = [],
/* 456  */                     g = !1;
/* 457  */                 f = this.index.get(a), f = this.sorter(f).slice(0, this.limit), f.length < this.limit && this.transport && (g = this._getFromRemote(a, d)), g || (f.length > 0 || !this.transport) && c && c(f)
/* 458  */             },
/* 459  */             clear: function() {
/* 460  */                 this.index.reset()
/* 461  */             },
/* 462  */             clearPrefetchCache: function() {
/* 463  */                 this.storage && this.storage.clear()
/* 464  */             },
/* 465  */             clearRemoteCache: function() {
/* 466  */                 this.transport && g.resetCache()
/* 467  */             },
/* 468  */             ttAdapter: function() {
/* 469  */                 return b.bind(this.get, this)
/* 470  */             }
/* 471  */         }), e
/* 472  */     }(this);
/* 473  */     var j = {
/* 474  */         wrapper: '<span class="twitter-typeahead"></span>',
/* 475  */         dropdown: '<span class="tt-dropdown-menu"></span>',
/* 476  */         dataset: '<div class="tt-dataset-%CLASS%"></div>',
/* 477  */         suggestions: '<span class="tt-suggestions"></span>',
/* 478  */         suggestion: '<div class="tt-suggestion"></div>'
/* 479  */     }, k = {
/* 480  */             wrapper: {
/* 481  */                 position: "relative",
/* 482  */                 display: "block"
/* 483  */             },
/* 484  */             hint: {
/* 485  */                 position: "absolute",
/* 486  */                 top: "0",
/* 487  */                 left: "0",
/* 488  */                 borderColor: "transparent",
/* 489  */                 boxShadow: "none"
/* 490  */             },
/* 491  */             input: {
/* 492  */                 position: "relative",
/* 493  */                 verticalAlign: "top",
/* 494  */                 backgroundColor: "transparent"
/* 495  */             },
/* 496  */             inputWithNoHint: {
/* 497  */                 position: "relative",
/* 498  */                 verticalAlign: "top"
/* 499  */             },
/* 500  */             dropdown: {

/* typeahead.js */

/* 501  */                 position: "absolute",
/* 502  */                 top: "100%",
/* 503  */                 left: "0",
/* 504  */                 zIndex: "100",
/* 505  */                 display: "none"
/* 506  */             },
/* 507  */             suggestions: {
/* 508  */                 display: "block"
/* 509  */             },
/* 510  */             suggestion: {
/* 511  */                 whiteSpace: "nowrap",
/* 512  */                 cursor: "pointer"
/* 513  */             },
/* 514  */             suggestionChild: {
/* 515  */                 whiteSpace: "normal"
/* 516  */             },
/* 517  */             ltr: {
/* 518  */                 left: "0",
/* 519  */                 right: "auto"
/* 520  */             },
/* 521  */             rtl: {
/* 522  */                 left: "auto",
/* 523  */                 right: " 0"
/* 524  */             }
/* 525  */         };
/* 526  */     b.isMsie() && b.mixin(k.input, {
/* 527  */         backgroundImage: "url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)"
/* 528  */     }), b.isMsie() && b.isMsie() <= 7 && b.mixin(k.input, {
/* 529  */         marginTop: "-1px"
/* 530  */     });
/* 531  */     var l = function() {
/* 532  */         function c(b) {
/* 533  */             b && b.el || a.error("EventBus initialized without el"), this.$el = a(b.el)
/* 534  */         }
/* 535  */         var d = "typeahead:";
/* 536  */         return b.mixin(c.prototype, {
/* 537  */             trigger: function(a) {
/* 538  */                 var b = [].slice.call(arguments, 1);
/* 539  */                 this.$el.trigger(d + a, b)
/* 540  */             }
/* 541  */         }), c
/* 542  */     }(),
/* 543  */         m = function() {
/* 544  */             function a(a, b, c, d) {
/* 545  */                 var e;
/* 546  */                 if (!c) return this;
/* 547  */                 for (b = b.split(i), c = d ? h(c, d) : c, this._callbacks = this._callbacks || {}; e = b.shift();) this._callbacks[e] = this._callbacks[e] || {
/* 548  */                     sync: [],
/* 549  */                     async: []
/* 550  */                 }, this._callbacks[e][a].push(c);

/* typeahead.js */

/* 551  */                 return this
/* 552  */             }
/* 553  */ 
/* 554  */             function b(b, c, d) {
/* 555  */                 return a.call(this, "async", b, c, d)
/* 556  */             }
/* 557  */ 
/* 558  */             function c(b, c, d) {
/* 559  */                 return a.call(this, "sync", b, c, d)
/* 560  */             }
/* 561  */ 
/* 562  */             function d(a) {
/* 563  */                 var b;
/* 564  */                 if (!this._callbacks) return this;
/* 565  */                 for (a = a.split(i); b = a.shift();) delete this._callbacks[b];
/* 566  */                 return this
/* 567  */             }
/* 568  */ 
/* 569  */             function e(a) {
/* 570  */                 var b, c, d, e, g;
/* 571  */                 if (!this._callbacks) return this;
/* 572  */                 for (a = a.split(i), d = [].slice.call(arguments, 1);
/* 573  */                     (b = a.shift()) && (c = this._callbacks[b]);) e = f(c.sync, this, [b].concat(d)), g = f(c.async, this, [b].concat(d)), e() && j(g);
/* 574  */                 return this
/* 575  */             }
/* 576  */ 
/* 577  */             function f(a, b, c) {
/* 578  */                 function d() {
/* 579  */                     for (var d, e = 0; !d && e < a.length; e += 1) d = a[e].apply(b, c) === !1;
/* 580  */                     return !d
/* 581  */                 }
/* 582  */                 return d
/* 583  */             }
/* 584  */ 
/* 585  */             function g() {
/* 586  */                 var a;
/* 587  */                 return a = window.setImmediate ? function(a) {
/* 588  */                     setImmediate(function() {
/* 589  */                         a()
/* 590  */                     })
/* 591  */                 } : function(a) {
/* 592  */                     setTimeout(function() {
/* 593  */                         a()
/* 594  */                     }, 0)
/* 595  */                 }
/* 596  */             }
/* 597  */ 
/* 598  */             function h(a, b) {
/* 599  */                 return a.bind ? a.bind(b) : function() {
/* 600  */                     a.apply(b, [].slice.call(arguments, 0))

/* typeahead.js */

/* 601  */                 }
/* 602  */             }
/* 603  */             var i = /\s+/,
/* 604  */                 j = g();
/* 605  */             return {
/* 606  */                 onSync: c,
/* 607  */                 onAsync: b,
/* 608  */                 off: d,
/* 609  */                 trigger: e
/* 610  */             }
/* 611  */         }(),
/* 612  */         n = function(a) {
/* 613  */             function c(a, c, d) {
/* 614  */                 for (var e, f = [], g = 0; g < a.length; g++) f.push(b.escapeRegExChars(a[g]));
/* 615  */                 return e = d ? "\\b(" + f.join("|") + ")\\b" : "(" + f.join("|") + ")", c ? new RegExp(e) : new RegExp(e, "i")
/* 616  */             }
/* 617  */             var d = {
/* 618  */                 node: null,
/* 619  */                 pattern: null,
/* 620  */                 tagName: "strong",
/* 621  */                 className: null,
/* 622  */                 wordsOnly: !1,
/* 623  */                 caseSensitive: !1
/* 624  */             };
/* 625  */             return function(e) {
/* 626  */                 function f(b) {
/* 627  */                     var c, d;
/* 628  */                     return (c = h.exec(b.data)) && (wrapperNode = a.createElement(e.tagName), e.className && (wrapperNode.className = e.className), d = b.splitText(c.index), d.splitText(c[0].length), wrapperNode.appendChild(d.cloneNode(!0)), b.parentNode.replaceChild(wrapperNode, d)), !! c
/* 629  */                 }
/* 630  */ 
/* 631  */                 function g(a, b) {
/* 632  */                     for (var c, d = 3, e = 0; e < a.childNodes.length; e++) c = a.childNodes[e], c.nodeType === d ? e += b(c) ? 1 : 0 : g(c, b)
/* 633  */                 }
/* 634  */                 var h;
/* 635  */                 e = b.mixin({}, d, e), e.node && e.pattern && (e.pattern = b.isArray(e.pattern) ? e.pattern : [e.pattern], h = c(e.pattern, e.caseSensitive, e.wordsOnly), g(e.node, f))
/* 636  */             }
/* 637  */         }(window.document),
/* 638  */         o = function() {
/* 639  */             function c(c) {
/* 640  */                 var e, f, h, i, j = this;
/* 641  */                 c = c || {}, c.input || a.error("input is missing"), e = b.bind(this._onBlur, this), f = b.bind(this._onFocus, this), h = b.bind(this._onKeydown, this), i = b.bind(this._onInput, this), this.$hint = a(c.hint), this.$input = a(c.input).on("blur.tt", e).on("focus.tt", f).on("keydown.tt", h), 0 === this.$hint.length && (this.setHint = this.getHint = this.clearHint = this.clearHintIfInvalid = b.noop), b.isMsie() ? this.$input.on("keydown.tt keypress.tt cut.tt paste.tt", function(a) {
/* 642  */                     g[a.which || a.keyCode] || b.defer(b.bind(j._onInput, j, a))
/* 643  */                 }) : this.$input.on("input.tt", i), this.query = this.$input.val(), this.$overflowHelper = d(this.$input)
/* 644  */             }
/* 645  */ 
/* 646  */             function d(b) {
/* 647  */                 return a('<pre aria-hidden="true"></pre>').css({
/* 648  */                     position: "absolute",
/* 649  */                     visibility: "hidden",
/* 650  */                     whiteSpace: "pre",

/* typeahead.js */

/* 651  */                     fontFamily: b.css("font-family"),
/* 652  */                     fontSize: b.css("font-size"),
/* 653  */                     fontStyle: b.css("font-style"),
/* 654  */                     fontVariant: b.css("font-variant"),
/* 655  */                     fontWeight: b.css("font-weight"),
/* 656  */                     wordSpacing: b.css("word-spacing"),
/* 657  */                     letterSpacing: b.css("letter-spacing"),
/* 658  */                     textIndent: b.css("text-indent"),
/* 659  */                     textRendering: b.css("text-rendering"),
/* 660  */                     textTransform: b.css("text-transform")
/* 661  */                 }).insertAfter(b)
/* 662  */             }
/* 663  */ 
/* 664  */             function e(a, b) {
/* 665  */                 return c.normalizeQuery(a) === c.normalizeQuery(b)
/* 666  */             }
/* 667  */ 
/* 668  */             function f(a) {
/* 669  */                 return a.altKey || a.ctrlKey || a.metaKey || a.shiftKey
/* 670  */             }
/* 671  */             var g;
/* 672  */             return g = {
/* 673  */                 9: "tab",
/* 674  */                 27: "esc",
/* 675  */                 37: "left",
/* 676  */                 39: "right",
/* 677  */                 13: "enter",
/* 678  */                 38: "up",
/* 679  */                 40: "down"
/* 680  */             }, c.normalizeQuery = function(a) {
/* 681  */                 return (a || "").replace(/^\s*/g, "").replace(/\s{2,}/g, " ")
/* 682  */             }, b.mixin(c.prototype, m, {
/* 683  */                 _onBlur: function() {
/* 684  */                     this.resetInputValue(), this.trigger("blurred")
/* 685  */                 },
/* 686  */                 _onFocus: function() {
/* 687  */                     this.trigger("focused")
/* 688  */                 },
/* 689  */                 _onKeydown: function(a) {
/* 690  */                     var b = g[a.which || a.keyCode];
/* 691  */                     this._managePreventDefault(b, a), b && this._shouldTrigger(b, a) && this.trigger(b + "Keyed", a)
/* 692  */                 },
/* 693  */                 _onInput: function() {
/* 694  */                     this._checkInputValue()
/* 695  */                 },
/* 696  */                 _managePreventDefault: function(a, b) {
/* 697  */                     var c, d, e;
/* 698  */                     switch (a) {
/* 699  */                         case "tab":
/* 700  */                             d = this.getHint(), e = this.getInputValue(), c = d && d !== e && !f(b);

/* typeahead.js */

/* 701  */                             break;
/* 702  */                         case "up":
/* 703  */                         case "down":
/* 704  */                             c = !f(b);
/* 705  */                             break;
/* 706  */                         default:
/* 707  */                             c = !1
/* 708  */                     }
/* 709  */                     c && b.preventDefault()
/* 710  */                 },
/* 711  */                 _shouldTrigger: function(a, b) {
/* 712  */                     var c;
/* 713  */                     switch (a) {
/* 714  */                         case "tab":
/* 715  */                             c = !f(b);
/* 716  */                             break;
/* 717  */                         default:
/* 718  */                             c = !0
/* 719  */                     }
/* 720  */                     return c
/* 721  */                 },
/* 722  */                 _checkInputValue: function() {
/* 723  */                     var a, b, c;
/* 724  */                     a = this.getInputValue(), b = e(a, this.query), c = b ? this.query.length !== a.length : !1, b ? c && this.trigger("whitespaceChanged", this.query) : this.trigger("queryChanged", this.query = a)
/* 725  */                 },
/* 726  */                 focus: function() {
/* 727  */                     this.$input.focus()
/* 728  */                 },
/* 729  */                 blur: function() {
/* 730  */                     this.$input.blur()
/* 731  */                 },
/* 732  */                 getQuery: function() {
/* 733  */                     return this.query
/* 734  */                 },
/* 735  */                 setQuery: function(a) {
/* 736  */                     this.query = a
/* 737  */                 },
/* 738  */                 getInputValue: function() {
/* 739  */                     return this.$input.val()
/* 740  */                 },
/* 741  */                 setInputValue: function(a, b) {
/* 742  */                     this.$input.val(a), b ? this.clearHint() : this._checkInputValue()
/* 743  */                 },
/* 744  */                 resetInputValue: function() {
/* 745  */                     this.setInputValue(this.query, !0)
/* 746  */                 },
/* 747  */                 getHint: function() {
/* 748  */                     return this.$hint.val()
/* 749  */                 },
/* 750  */                 setHint: function(a) {

/* typeahead.js */

/* 751  */                     this.$hint.val(a)
/* 752  */                 },
/* 753  */                 clearHint: function() {
/* 754  */                     this.setHint("")
/* 755  */                 },
/* 756  */                 clearHintIfInvalid: function() {
/* 757  */                     var a, b, c, d;
/* 758  */                     a = this.getInputValue(), b = this.getHint(), c = a !== b && 0 === b.indexOf(a), d = "" !== a && c && !this.hasOverflow(), !d && this.clearHint()
/* 759  */                 },
/* 760  */                 getLanguageDirection: function() {
/* 761  */                     return (this.$input.css("direction") || "ltr").toLowerCase()
/* 762  */                 },
/* 763  */                 hasOverflow: function() {
/* 764  */                     var a = this.$input.width() - 2;
/* 765  */                     return this.$overflowHelper.text(this.getInputValue()), this.$overflowHelper.width() >= a
/* 766  */                 },
/* 767  */                 isCursorAtEnd: function() {
/* 768  */                     var a, c, d;
/* 769  */                     return a = this.$input.val().length, c = this.$input[0].selectionStart, b.isNumber(c) ? c === a : document.selection ? (d = document.selection.createRange(), d.moveStart("character", -a), a === d.text.length) : !0
/* 770  */                 },
/* 771  */                 destroy: function() {
/* 772  */                     this.$hint.off(".tt"), this.$input.off(".tt"), this.$hint = this.$input = this.$overflowHelper = null
/* 773  */                 }
/* 774  */             }), c
/* 775  */         }(),
/* 776  */         p = function() {
/* 777  */             function c(c) {
/* 778  */                 c = c || {}, c.templates = c.templates || {}, c.source || a.error("missing source"), c.name && !f(c.name) && a.error("invalid dataset name: " + c.name), this.query = null, this.highlight = !! c.highlight, this.name = c.name || b.getUniqueId(), this.source = c.source, this.displayFn = d(c.display || c.displayKey), this.templates = e(c.templates, this.displayFn), this.$el = a(j.dataset.replace("%CLASS%", this.name))
/* 779  */             }
/* 780  */ 
/* 781  */             function d(a) {
/* 782  */                 function c(b) {
/* 783  */                     return b[a]
/* 784  */                 }
/* 785  */                 return a = a || "value", b.isFunction(a) ? a : c
/* 786  */             }
/* 787  */ 
/* 788  */             function e(a, c) {
/* 789  */                 function d(a) {
/* 790  */                     return "<p>" + c(a) + "</p>"
/* 791  */                 }
/* 792  */                 return {
/* 793  */                     empty: a.empty && b.templatify(a.empty),
/* 794  */                     header: a.header && b.templatify(a.header),
/* 795  */                     footer: a.footer && b.templatify(a.footer),
/* 796  */                     suggestion: a.suggestion || d
/* 797  */                 }
/* 798  */             }
/* 799  */ 
/* 800  */             function f(a) {

/* typeahead.js */

/* 801  */                 return /^[_a-zA-Z0-9-]+$/.test(a)
/* 802  */             }
/* 803  */             var g = "ttDataset",
/* 804  */                 h = "ttValue",
/* 805  */                 i = "ttDatum";
/* 806  */             return c.extractDatasetName = function(b) {
/* 807  */                 return a(b).data(g)
/* 808  */             }, c.extractValue = function(b) {
/* 809  */                 return a(b).data(h)
/* 810  */             }, c.extractDatum = function(b) {
/* 811  */                 return a(b).data(i)
/* 812  */             }, b.mixin(c.prototype, m, {
/* 813  */                 _render: function(c, d) {
/* 814  */                     function e() {
/* 815  */                         return p.templates.empty({
/* 816  */                             query: c,
/* 817  */                             isEmpty: !0
/* 818  */                         })
/* 819  */                     }
/* 820  */ 
/* 821  */                     function f() {
/* 822  */                         function e(b) {
/* 823  */                             var c;
/* 824  */                             return c = a(j.suggestion).append(p.templates.suggestion(b)).data(g, p.name).data(h, p.displayFn(b)).data(i, b), c.children().each(function() {
/* 825  */                                 a(this).css(k.suggestionChild)
/* 826  */                             }), c
/* 827  */                         }
/* 828  */                         var f, l;
/* 829  */                         return f = a(j.suggestions).css(k.suggestions), l = b.map(d, e), f.append.apply(f, l), p.highlight && n({
/* 830  */                             node: f[0],
/* 831  */                             pattern: c
/* 832  */                         }), f
/* 833  */                     }
/* 834  */ 
/* 835  */                     function l() {
/* 836  */                         return p.templates.header({
/* 837  */                             query: c,
/* 838  */                             isEmpty: !o
/* 839  */                         })
/* 840  */                     }
/* 841  */ 
/* 842  */                     function m() {
/* 843  */                         return p.templates.footer({
/* 844  */                             query: c,
/* 845  */                             isEmpty: !o
/* 846  */                         })
/* 847  */                     }
/* 848  */                     if (this.$el) {
/* 849  */                         var o, p = this;
/* 850  */                         this.$el.empty(), o = d && d.length, !o && this.templates.empty ? this.$el.html(e()).prepend(p.templates.header ? l() : null).append(p.templates.footer ? m() : null) : o && this.$el.html(f()).prepend(p.templates.header ? l() : null).append(p.templates.footer ? m() : null), this.trigger("rendered")

/* typeahead.js */

/* 851  */                     }
/* 852  */                 },
/* 853  */                 getRoot: function() {
/* 854  */                     return this.$el
/* 855  */                 },
/* 856  */                 update: function(a) {
/* 857  */                     function b(b) {
/* 858  */                         c.canceled || a !== c.query || c._render(a, b)
/* 859  */                     }
/* 860  */                     var c = this;
/* 861  */                     this.query = a, this.canceled = !1, this.source(a, b)
/* 862  */                 },
/* 863  */                 cancel: function() {
/* 864  */                     this.canceled = !0
/* 865  */                 },
/* 866  */                 clear: function() {
/* 867  */                     this.cancel(), this.$el.empty(), this.trigger("rendered")
/* 868  */                 },
/* 869  */                 isEmpty: function() {
/* 870  */                     return this.$el.is(":empty")
/* 871  */                 },
/* 872  */                 destroy: function() {
/* 873  */                     this.$el = null
/* 874  */                 }
/* 875  */             }), c
/* 876  */         }(),
/* 877  */         q = function() {
/* 878  */             function c(c) {
/* 879  */                 var e, f, g, h = this;
/* 880  */                 c = c || {}, c.menu || a.error("menu is required"), this.isOpen = !1, this.isEmpty = !0, this.datasets = b.map(c.datasets, d), e = b.bind(this._onSuggestionClick, this), f = b.bind(this._onSuggestionMouseEnter, this), g = b.bind(this._onSuggestionMouseLeave, this), this.$menu = a(c.menu).on("click.tt", ".tt-suggestion", e).on("mouseenter.tt", ".tt-suggestion", f).on("mouseleave.tt", ".tt-suggestion", g), b.each(this.datasets, function(a) {
/* 881  */                     h.$menu.append(a.getRoot()), a.onSync("rendered", h._onRendered, h)
/* 882  */                 })
/* 883  */             }
/* 884  */ 
/* 885  */             function d(a) {
/* 886  */                 return new p(a)
/* 887  */             }
/* 888  */             return b.mixin(c.prototype, m, {
/* 889  */                 _onSuggestionClick: function(b) {
/* 890  */                     this.trigger("suggestionClicked", a(b.currentTarget))
/* 891  */                 },
/* 892  */                 _onSuggestionMouseEnter: function(b) {
/* 893  */                     this._removeCursor(), this._setCursor(a(b.currentTarget), !0)
/* 894  */                 },
/* 895  */                 _onSuggestionMouseLeave: function() {
/* 896  */                     this._removeCursor()
/* 897  */                 },
/* 898  */                 _onRendered: function() {
/* 899  */                     function a(a) {
/* 900  */                         return a.isEmpty()

/* typeahead.js */

/* 901  */                     }
/* 902  */                     this.isEmpty = b.every(this.datasets, a), this.isEmpty ? this._hide() : this.isOpen && this._show(), this.trigger("datasetRendered")
/* 903  */                 },
/* 904  */                 _hide: function() {
/* 905  */                     this.$menu.hide()
/* 906  */                 },
/* 907  */                 _show: function() {
/* 908  */                     this.$menu.css("display", "block")
/* 909  */                 },
/* 910  */                 _getSuggestions: function() {
/* 911  */                     return this.$menu.find(".tt-suggestion")
/* 912  */                 },
/* 913  */                 _getCursor: function() {
/* 914  */                     return this.$menu.find(".tt-cursor").first()
/* 915  */                 },
/* 916  */                 _setCursor: function(a, b) {
/* 917  */                     a.first().addClass("tt-cursor"), !b && this.trigger("cursorMoved")
/* 918  */                 },
/* 919  */                 _removeCursor: function() {
/* 920  */                     this._getCursor().removeClass("tt-cursor")
/* 921  */                 },
/* 922  */                 _moveCursor: function(a) {
/* 923  */                     var b, c, d, e;
/* 924  */                     if (this.isOpen) {
/* 925  */                         if (c = this._getCursor(), b = this._getSuggestions(), this._removeCursor(), d = b.index(c) + a, d = (d + 1) % (b.length + 1) - 1, -1 === d) return void this.trigger("cursorRemoved"); - 1 > d && (d = b.length - 1), this._setCursor(e = b.eq(d)), this._ensureVisible(e)
/* 926  */                     }
/* 927  */                 },
/* 928  */                 _ensureVisible: function(a) {
/* 929  */                     var b, c, d, e;
/* 930  */                     b = a.position().top, c = b + a.outerHeight(!0), d = this.$menu.scrollTop(), e = this.$menu.height() + parseInt(this.$menu.css("paddingTop"), 10) + parseInt(this.$menu.css("paddingBottom"), 10), 0 > b ? this.$menu.scrollTop(d + b) : c > e && this.$menu.scrollTop(d + (c - e))
/* 931  */                 },
/* 932  */                 close: function() {
/* 933  */                     this.isOpen && (this.isOpen = !1, this._removeCursor(), this._hide(), this.trigger("closed"))
/* 934  */                 },
/* 935  */                 open: function() {
/* 936  */                     this.isOpen || (this.isOpen = !0, !this.isEmpty && this._show(), this.trigger("opened"))
/* 937  */                 },
/* 938  */                 setLanguageDirection: function(a) {
/* 939  */                     this.$menu.css("ltr" === a ? k.ltr : k.rtl)
/* 940  */                 },
/* 941  */                 moveCursorUp: function() {
/* 942  */                     this._moveCursor(-1)
/* 943  */                 },
/* 944  */                 moveCursorDown: function() {
/* 945  */                     this._moveCursor(1)
/* 946  */                 },
/* 947  */                 getDatumForSuggestion: function(a) {
/* 948  */                     var b = null;
/* 949  */                     return a.length && (b = {
/* 950  */                         raw: p.extractDatum(a),

/* typeahead.js */

/* 951  */                         value: p.extractValue(a),
/* 952  */                         datasetName: p.extractDatasetName(a)
/* 953  */                     }), b
/* 954  */                 },
/* 955  */                 getDatumForCursor: function() {
/* 956  */                     return this.getDatumForSuggestion(this._getCursor().first())
/* 957  */                 },
/* 958  */                 getDatumForTopSuggestion: function() {
/* 959  */                     return this.getDatumForSuggestion(this._getSuggestions().first())
/* 960  */                 },
/* 961  */                 update: function(a) {
/* 962  */                     function c(b) {
/* 963  */                         b.update(a)
/* 964  */                     }
/* 965  */                     b.each(this.datasets, c)
/* 966  */                 },
/* 967  */                 empty: function() {
/* 968  */                     function a(a) {
/* 969  */                         a.clear()
/* 970  */                     }
/* 971  */                     b.each(this.datasets, a), this.isEmpty = !0
/* 972  */                 },
/* 973  */                 isVisible: function() {
/* 974  */                     return this.isOpen && !this.isEmpty
/* 975  */                 },
/* 976  */                 destroy: function() {
/* 977  */                     function a(a) {
/* 978  */                         a.destroy()
/* 979  */                     }
/* 980  */                     this.$menu.off(".tt"), this.$menu = null, b.each(this.datasets, a)
/* 981  */                 }
/* 982  */             }), c
/* 983  */         }(),
/* 984  */         r = function() {
/* 985  */             function c(c) {
/* 986  */                 var e, f, g;
/* 987  */                 c = c || {}, c.input || a.error("missing input"), this.isActivated = !1, this.autoselect = !! c.autoselect, this.minLength = b.isNumber(c.minLength) ? c.minLength : 1, this.$node = d(c.input, c.withHint), e = this.$node.find(".tt-dropdown-menu"), f = this.$node.find(".tt-input"), g = this.$node.find(".tt-hint"), f.on("blur.tt", function(a) {
/* 988  */                     var c, d, g;
/* 989  */                     c = document.activeElement, d = e.is(c), g = e.has(c).length > 0, b.isMsie() && (d || g) && (a.preventDefault(), a.stopImmediatePropagation(), b.defer(function() {
/* 990  */                         f.focus()
/* 991  */                     }))
/* 992  */                 }), e.on("mousedown.tt", function(a) {
/* 993  */                     a.preventDefault()
/* 994  */                 }), this.eventBus = c.eventBus || new l({
/* 995  */                     el: f
/* 996  */                 }), this.dropdown = new q({
/* 997  */                     menu: e,
/* 998  */                     datasets: c.datasets
/* 999  */                 }).onSync("suggestionClicked", this._onSuggestionClicked, this).onSync("cursorMoved", this._onCursorMoved, this).onSync("cursorRemoved", this._onCursorRemoved, this).onSync("opened", this._onOpened, this).onSync("closed", this._onClosed, this).onAsync("datasetRendered", this._onDatasetRendered, this), this.input = new o({
/* 1000 */                     input: f,

/* typeahead.js */

/* 1001 */                     hint: g
/* 1002 */                 }).onSync("focused", this._onFocused, this).onSync("blurred", this._onBlurred, this).onSync("enterKeyed", this._onEnterKeyed, this).onSync("tabKeyed", this._onTabKeyed, this).onSync("escKeyed", this._onEscKeyed, this).onSync("upKeyed", this._onUpKeyed, this).onSync("downKeyed", this._onDownKeyed, this).onSync("leftKeyed", this._onLeftKeyed, this).onSync("rightKeyed", this._onRightKeyed, this).onSync("queryChanged", this._onQueryChanged, this).onSync("whitespaceChanged", this._onWhitespaceChanged, this), this._setLanguageDirection()
/* 1003 */             }
/* 1004 */ 
/* 1005 */             function d(b, c) {
/* 1006 */                 var d, f, h, i;
/* 1007 */                 d = a(b), f = a(j.wrapper).css(k.wrapper), h = a(j.dropdown).css(k.dropdown), i = d.clone().css(k.hint).css(e(d)), i.val("").removeData().addClass("tt-hint").removeAttr("id name placeholder").prop("disabled", !0).attr({
/* 1008 */                     autocomplete: "off",
/* 1009 */                     spellcheck: "false"
/* 1010 */                 }), d.data(g, {
/* 1011 */                     dir: d.attr("dir"),
/* 1012 */                     autocomplete: d.attr("autocomplete"),
/* 1013 */                     spellcheck: d.attr("spellcheck"),
/* 1014 */                     style: d.attr("style")
/* 1015 */                 }), d.addClass("tt-input").attr({
/* 1016 */                     autocomplete: "off",
/* 1017 */                     spellcheck: !1
/* 1018 */                 }).css(c ? k.input : k.inputWithNoHint);
/* 1019 */                 try {
/* 1020 */                     !d.attr("dir") && d.attr("dir", "auto")
/* 1021 */                 } catch (l) {}
/* 1022 */                 return d.wrap(f).parent().prepend(c ? i : null).append(h)
/* 1023 */             }
/* 1024 */ 
/* 1025 */             function e(a) {
/* 1026 */                 return {
/* 1027 */                     backgroundAttachment: a.css("background-attachment"),
/* 1028 */                     backgroundClip: a.css("background-clip"),
/* 1029 */                     backgroundColor: a.css("background-color"),
/* 1030 */                     backgroundImage: a.css("background-image"),
/* 1031 */                     backgroundOrigin: a.css("background-origin"),
/* 1032 */                     backgroundPosition: a.css("background-position"),
/* 1033 */                     backgroundRepeat: a.css("background-repeat"),
/* 1034 */                     backgroundSize: a.css("background-size")
/* 1035 */                 }
/* 1036 */             }
/* 1037 */ 
/* 1038 */             function f(a) {
/* 1039 */                 var c = a.find(".tt-input");
/* 1040 */                 b.each(c.data(g), function(a, d) {
/* 1041 */                     b.isUndefined(a) ? c.removeAttr(d) : c.attr(d, a)
/* 1042 */                 }), c.detach().removeData(g).removeClass("tt-input").insertAfter(a), a.remove()
/* 1043 */             }
/* 1044 */             var g = "ttAttrs";
/* 1045 */             return b.mixin(c.prototype, {
/* 1046 */                 _onSuggestionClicked: function(a, b) {
/* 1047 */                     var c;
/* 1048 */                     (c = this.dropdown.getDatumForSuggestion(b)) && this._select(c)
/* 1049 */                 },
/* 1050 */                 _onCursorMoved: function() {

/* typeahead.js */

/* 1051 */                     var a = this.dropdown.getDatumForCursor();
/* 1052 */                     this.input.setInputValue(a.value, !0), this.eventBus.trigger("cursorchanged", a.raw, a.datasetName)
/* 1053 */                 },
/* 1054 */                 _onCursorRemoved: function() {
/* 1055 */                     this.input.resetInputValue(), this._updateHint()
/* 1056 */                 },
/* 1057 */                 _onDatasetRendered: function() {
/* 1058 */                     this._updateHint()
/* 1059 */                 },
/* 1060 */                 _onOpened: function() {
/* 1061 */                     this._updateHint(), this.eventBus.trigger("opened")
/* 1062 */                 },
/* 1063 */                 _onClosed: function() {
/* 1064 */                     this.input.clearHint(), this.eventBus.trigger("closed")
/* 1065 */                 },
/* 1066 */                 _onFocused: function() {
/* 1067 */                     this.isActivated = !0, this.dropdown.open()
/* 1068 */                 },
/* 1069 */                 _onBlurred: function() {
/* 1070 */                     this.isActivated = !1, this.dropdown.empty(), this.dropdown.close()
/* 1071 */                 },
/* 1072 */                 _onEnterKeyed: function(a, b) {
/* 1073 */                     var c, d;
/* 1074 */                     c = this.dropdown.getDatumForCursor(), d = this.dropdown.getDatumForTopSuggestion(), c ? (this._select(c), b.preventDefault()) : this.autoselect && d && (this._select(d), b.preventDefault())
/* 1075 */                 },
/* 1076 */                 _onTabKeyed: function(a, b) {
/* 1077 */                     var c;
/* 1078 */                     (c = this.dropdown.getDatumForCursor()) ? (this._select(c), b.preventDefault()) : this._autocomplete(!0)
/* 1079 */                 },
/* 1080 */                 _onEscKeyed: function() {
/* 1081 */                     this.dropdown.close(), this.input.resetInputValue()
/* 1082 */                 },
/* 1083 */                 _onUpKeyed: function() {
/* 1084 */                     var a = this.input.getQuery();
/* 1085 */                     this.dropdown.isEmpty && a.length >= this.minLength ? this.dropdown.update(a) : this.dropdown.moveCursorUp(), this.dropdown.open()
/* 1086 */                 },
/* 1087 */                 _onDownKeyed: function() {
/* 1088 */                     var a = this.input.getQuery();
/* 1089 */                     this.dropdown.isEmpty && a.length >= this.minLength ? this.dropdown.update(a) : this.dropdown.moveCursorDown(), this.dropdown.open()
/* 1090 */                 },
/* 1091 */                 _onLeftKeyed: function() {
/* 1092 */                     "rtl" === this.dir && this._autocomplete()
/* 1093 */                 },
/* 1094 */                 _onRightKeyed: function() {
/* 1095 */                     "ltr" === this.dir && this._autocomplete()
/* 1096 */                 },
/* 1097 */                 _onQueryChanged: function(a, b) {
/* 1098 */                     this.input.clearHintIfInvalid(), b.length >= this.minLength ? this.dropdown.update(b) : this.dropdown.empty(), this.dropdown.open(), this._setLanguageDirection()
/* 1099 */                 },
/* 1100 */                 _onWhitespaceChanged: function() {

/* typeahead.js */

/* 1101 */                     this._updateHint(), this.dropdown.open()
/* 1102 */                 },
/* 1103 */                 _setLanguageDirection: function() {
/* 1104 */                     var a;
/* 1105 */                     this.dir !== (a = this.input.getLanguageDirection()) && (this.dir = a, this.$node.css("direction", a), this.dropdown.setLanguageDirection(a))
/* 1106 */                 },
/* 1107 */                 _updateHint: function() {
/* 1108 */                     var a, c, d, e, f, g;
/* 1109 */                     a = this.dropdown.getDatumForTopSuggestion(), a && this.dropdown.isVisible() && !this.input.hasOverflow() ? (c = this.input.getInputValue(), d = o.normalizeQuery(c), e = b.escapeRegExChars(d), f = new RegExp("^(?:" + e + ")(.+$)", "i"), g = f.exec(a.value), g ? this.input.setHint(c + g[1]) : this.input.clearHint()) : this.input.clearHint()
/* 1110 */                 },
/* 1111 */                 _autocomplete: function(a) {
/* 1112 */                     var b, c, d, e;
/* 1113 */                     b = this.input.getHint(), c = this.input.getQuery(), d = a || this.input.isCursorAtEnd(), b && c !== b && d && (e = this.dropdown.getDatumForTopSuggestion(), e && this.input.setInputValue(e.value), this.eventBus.trigger("autocompleted", e.raw, e.datasetName))
/* 1114 */                 },
/* 1115 */                 _select: function(a) {
/* 1116 */                     this.input.setQuery(a.value), this.input.setInputValue(a.value, !0), this._setLanguageDirection(), this.eventBus.trigger("selected", a.raw, a.datasetName), this.dropdown.close(), b.defer(b.bind(this.dropdown.empty, this.dropdown))
/* 1117 */                 },
/* 1118 */                 open: function() {
/* 1119 */                     this.dropdown.open()
/* 1120 */                 },
/* 1121 */                 close: function() {
/* 1122 */                     this.dropdown.close()
/* 1123 */                 },
/* 1124 */                 setVal: function(a) {
/* 1125 */                     this.isActivated ? this.input.setInputValue(a) : (this.input.setQuery(a), this.input.setInputValue(a, !0)), this._setLanguageDirection()
/* 1126 */                 },
/* 1127 */                 getVal: function() {
/* 1128 */                     return this.input.getQuery()
/* 1129 */                 },
/* 1130 */                 destroy: function() {
/* 1131 */                     this.input.destroy(), this.dropdown.destroy(), f(this.$node), this.$node = null
/* 1132 */                 }
/* 1133 */             }), c
/* 1134 */         }();
/* 1135 */     ! function() {
/* 1136 */         var c, d, e;
/* 1137 */         c = a.fn.typeahead, d = "ttTypeahead", e = {
/* 1138 */             initialize: function(c, e) {
/* 1139 */                 function f() {
/* 1140 */                     var f, g, h = a(this);
/* 1141 */                     b.each(e, function(a) {
/* 1142 */                         a.highlight = !! c.highlight
/* 1143 */                     }), g = new r({
/* 1144 */                         input: h,
/* 1145 */                         eventBus: f = new l({
/* 1146 */                             el: h
/* 1147 */                         }),
/* 1148 */                         withHint: b.isUndefined(c.hint) ? !0 : !! c.hint,
/* 1149 */                         minLength: c.minLength,
/* 1150 */                         autoselect: c.autoselect,

/* typeahead.js */

/* 1151 */                         datasets: e
/* 1152 */                     }), h.data(d, g)
/* 1153 */                 }
/* 1154 */                 return e = b.isArray(e) ? e : [].slice.call(arguments, 1), c = c || {}, this.each(f)
/* 1155 */             },
/* 1156 */             open: function() {
/* 1157 */                 function b() {
/* 1158 */                     var b, c = a(this);
/* 1159 */                     (b = c.data(d)) && b.open()
/* 1160 */                 }
/* 1161 */                 return this.each(b)
/* 1162 */             },
/* 1163 */             close: function() {
/* 1164 */                 function b() {
/* 1165 */                     var b, c = a(this);
/* 1166 */                     (b = c.data(d)) && b.close()
/* 1167 */                 }
/* 1168 */                 return this.each(b)
/* 1169 */             },
/* 1170 */             val: function(b) {
/* 1171 */                 function c() {
/* 1172 */                     var c, e = a(this);
/* 1173 */                     (c = e.data(d)) && c.setVal(b)
/* 1174 */                 }
/* 1175 */ 
/* 1176 */                 function e(a) {
/* 1177 */                     var b, c;
/* 1178 */                     return (b = a.data(d)) && (c = b.getVal()), c
/* 1179 */                 }
/* 1180 */                 return arguments.length ? this.each(c) : e(this.first())
/* 1181 */             },
/* 1182 */             destroy: function() {
/* 1183 */                 function b() {
/* 1184 */                     var b, c = a(this);
/* 1185 */                     (b = c.data(d)) && (b.destroy(), c.removeData(d))
/* 1186 */                 }
/* 1187 */                 return this.each(b)
/* 1188 */             }
/* 1189 */         }, a.fn.typeahead = function(a) {
/* 1190 */             return e[a] ? e[a].apply(this, [].slice.call(arguments, 1)) : e.initialize.apply(this, arguments)
/* 1191 */         }, a.fn.typeahead.noConflict = function() {
/* 1192 */             return a.fn.typeahead = c, this
/* 1193 */         }
/* 1194 */     }()
/* 1195 */ }(window.jQuery);

;
/* magnific.js */

/* 1    */ /*! Magnific Popup - v0.9.2 - 2013-07-15
/* 2    *|  * http://dimsemenov.com/plugins/magnific-popup/
/* 3    *|  * Copyright (c) 2013 Dmitry Semenov; */
/* 4    */ ;
/* 5    */ (function($) {
/* 6    */ 
/* 7    */     /*>>core*/
/* 8    */     /**
/* 9    *|      *
/* 10   *|      * Magnific Popup Core JS file
/* 11   *|      *
/* 12   *|      */
/* 13   */ 
/* 14   */ 
/* 15   */     /**
/* 16   *|      * Private static constants
/* 17   *|      */
/* 18   */     var CLOSE_EVENT = 'Close',
/* 19   */         BEFORE_CLOSE_EVENT = 'BeforeClose',
/* 20   */         AFTER_CLOSE_EVENT = 'AfterClose',
/* 21   */         BEFORE_APPEND_EVENT = 'BeforeAppend',
/* 22   */         MARKUP_PARSE_EVENT = 'MarkupParse',
/* 23   */         OPEN_EVENT = 'Open',
/* 24   */         CHANGE_EVENT = 'Change',
/* 25   */         NS = 'mfp',
/* 26   */         EVENT_NS = '.' + NS,
/* 27   */         READY_CLASS = 'mfp-ready',
/* 28   */         REMOVING_CLASS = 'mfp-removing',
/* 29   */         PREVENT_CLOSE_CLASS = 'mfp-prevent-close';
/* 30   */ 
/* 31   */ 
/* 32   */     /**
/* 33   *|      * Private vars
/* 34   *|      */
/* 35   */     var mfp, // As we have only one instance of MagnificPopup object, we define it locally to not to use 'this'
/* 36   */         MagnificPopup = function() {},
/* 37   */         _isJQ = !! (window.jQuery),
/* 38   */         _prevStatus,
/* 39   */         _window = $(window),
/* 40   */         _body,
/* 41   */         _document,
/* 42   */         _prevContentType,
/* 43   */         _wrapClasses,
/* 44   */         _currPopupType;
/* 45   */ 
/* 46   */ 
/* 47   */     /**
/* 48   *|      * Private functions
/* 49   *|      */
/* 50   */     var _mfpOn = function(name, f) {

/* magnific.js */

/* 51   */         mfp.ev.on(NS + name + EVENT_NS, f);
/* 52   */     },
/* 53   */         _getEl = function(className, appendTo, html, raw) {
/* 54   */             var el = document.createElement('div');
/* 55   */             el.className = 'mfp-' + className;
/* 56   */             if (html) {
/* 57   */                 el.innerHTML = html;
/* 58   */             }
/* 59   */             if (!raw) {
/* 60   */                 el = $(el);
/* 61   */                 if (appendTo) {
/* 62   */                     el.appendTo(appendTo);
/* 63   */                 }
/* 64   */             } else if (appendTo) {
/* 65   */                 appendTo.appendChild(el);
/* 66   */             }
/* 67   */             return el;
/* 68   */         },
/* 69   */         _mfpTrigger = function(e, data) {
/* 70   */             mfp.ev.triggerHandler(NS + e, data);
/* 71   */ 
/* 72   */             if (mfp.st.callbacks) {
/* 73   */                 // converts "mfpEventName" to "eventName" callback and triggers it if it's present
/* 74   */                 e = e.charAt(0).toLowerCase() + e.slice(1);
/* 75   */                 if (mfp.st.callbacks[e]) {
/* 76   */                     mfp.st.callbacks[e].apply(mfp, $.isArray(data) ? data : [data]);
/* 77   */                 }
/* 78   */             }
/* 79   */         },
/* 80   */         _setFocus = function() {
/* 81   */             (mfp.st.focus ? mfp.content.find(mfp.st.focus).eq(0) : mfp.wrap).trigger('focus');
/* 82   */         },
/* 83   */         _getCloseBtn = function(type) {
/* 84   */             if (type !== _currPopupType || !mfp.currTemplate.closeBtn) {
/* 85   */                 mfp.currTemplate.closeBtn = $(mfp.st.closeMarkup.replace('%title%', mfp.st.tClose));
/* 86   */                 _currPopupType = type;
/* 87   */             }
/* 88   */             return mfp.currTemplate.closeBtn;
/* 89   */         },
/* 90   */         // Initialize Magnific Popup only when called at least once
/* 91   */         _checkInstance = function() {
/* 92   */             if (!$.magnificPopup.instance) {
/* 93   */                 mfp = new MagnificPopup();
/* 94   */                 mfp.init();
/* 95   */                 $.magnificPopup.instance = mfp;
/* 96   */             }
/* 97   */         },
/* 98   */         // Check to close popup or not
/* 99   */         // "target" is an element that was clicked
/* 100  */         _checkIfClose = function(target) {

/* magnific.js */

/* 101  */ 
/* 102  */             if ($(target).hasClass(PREVENT_CLOSE_CLASS)) {
/* 103  */                 return;
/* 104  */             }
/* 105  */ 
/* 106  */             var closeOnContent = mfp.st.closeOnContentClick;
/* 107  */             var closeOnBg = mfp.st.closeOnBgClick;
/* 108  */ 
/* 109  */             if (closeOnContent && closeOnBg) {
/* 110  */                 return true;
/* 111  */             } else {
/* 112  */ 
/* 113  */                 // We close the popup if click is on close button or on preloader. Or if there is no content.
/* 114  */                 if (!mfp.content || $(target).hasClass('mfp-close') || (mfp.preloader && target === mfp.preloader[0])) {
/* 115  */                     return true;
/* 116  */                 }
/* 117  */ 
/* 118  */                 // if click is outside the content
/* 119  */                 if ((target !== mfp.content[0] && !$.contains(mfp.content[0], target))) {
/* 120  */                     if (closeOnBg) {
/* 121  */                         // last check, if the clicked element is in DOM, (in case it's removed onclick)
/* 122  */                         if ($.contains(document, target)) {
/* 123  */                             return true;
/* 124  */                         }
/* 125  */                     }
/* 126  */                 } else if (closeOnContent) {
/* 127  */                     return true;
/* 128  */                 }
/* 129  */ 
/* 130  */             }
/* 131  */             return false;
/* 132  */         },
/* 133  */         // CSS transition detection, http://stackoverflow.com/questions/7264899/detect-css-transitions-using-javascript-and-without-modernizr
/* 134  */         supportsTransitions = function() {
/* 135  */             var s = document.createElement('p').style, // 's' for style. better to create an element if body yet to exist
/* 136  */                 v = ['ms', 'O', 'Moz', 'Webkit']; // 'v' for vendor
/* 137  */ 
/* 138  */             if (s['transition'] !== undefined) {
/* 139  */                 return true;
/* 140  */             }
/* 141  */ 
/* 142  */             while (v.length) {
/* 143  */                 if (v.pop() + 'Transition' in s) {
/* 144  */                     return true;
/* 145  */                 }
/* 146  */             }
/* 147  */ 
/* 148  */             return false;
/* 149  */         };
/* 150  */ 

/* magnific.js */

/* 151  */ 
/* 152  */ 
/* 153  */     /**
/* 154  *|      * Public functions
/* 155  *|      */
/* 156  */     MagnificPopup.prototype = {
/* 157  */ 
/* 158  */         constructor: MagnificPopup,
/* 159  */ 
/* 160  */         /**
/* 161  *|          * Initializes Magnific Popup plugin.
/* 162  *|          * This function is triggered only once when $.fn.magnificPopup or $.magnificPopup is executed
/* 163  *|          */
/* 164  */         init: function() {
/* 165  */             var appVersion = navigator.appVersion;
/* 166  */             mfp.isIE7 = appVersion.indexOf("MSIE 7.") !== -1;
/* 167  */             mfp.isIE8 = appVersion.indexOf("MSIE 8.") !== -1;
/* 168  */             mfp.isLowIE = mfp.isIE7 || mfp.isIE8;
/* 169  */             mfp.isAndroid = (/android/gi).test(appVersion);
/* 170  */             mfp.isIOS = (/iphone|ipad|ipod/gi).test(appVersion);
/* 171  */             mfp.supportsTransition = supportsTransitions();
/* 172  */ 
/* 173  */             // We disable fixed positioned lightbox on devices that don't handle it nicely.
/* 174  */             // If you know a better way of detecting this - let me know.
/* 175  */             mfp.probablyMobile = (mfp.isAndroid || mfp.isIOS || /(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent));
/* 176  */             _body = $(document.body);
/* 177  */             _document = $(document);
/* 178  */ 
/* 179  */             mfp.popupsCache = {};
/* 180  */         },
/* 181  */ 
/* 182  */         /**
/* 183  *|          * Opens popup
/* 184  *|          * @param  data [description]
/* 185  *|          */
/* 186  */         open: function(data) {
/* 187  */ 
/* 188  */             var i;
/* 189  */ 
/* 190  */             if (data.isObj === false) {
/* 191  */                 // convert jQuery collection to array to avoid conflicts later
/* 192  */                 mfp.items = data.items.toArray();
/* 193  */ 
/* 194  */                 mfp.index = 0;
/* 195  */                 var items = data.items,
/* 196  */                     item;
/* 197  */                 for (i = 0; i < items.length; i++) {
/* 198  */                     item = items[i];
/* 199  */                     if (item.parsed) {
/* 200  */                         item = item.el[0];

/* magnific.js */

/* 201  */                     }
/* 202  */                     if (item === data.el[0]) {
/* 203  */                         mfp.index = i;
/* 204  */                         break;
/* 205  */                     }
/* 206  */                 }
/* 207  */             } else {
/* 208  */                 mfp.items = $.isArray(data.items) ? data.items : [data.items];
/* 209  */                 mfp.index = data.index || 0;
/* 210  */             }
/* 211  */ 
/* 212  */             // if popup is already opened - we just update the content
/* 213  */             if (mfp.isOpen) {
/* 214  */                 mfp.updateItemHTML();
/* 215  */                 return;
/* 216  */             }
/* 217  */ 
/* 218  */             mfp.types = [];
/* 219  */             _wrapClasses = '';
/* 220  */             if (data.mainEl && data.mainEl.length) {
/* 221  */                 mfp.ev = data.mainEl.eq(0);
/* 222  */             } else {
/* 223  */                 mfp.ev = _document;
/* 224  */             }
/* 225  */ 
/* 226  */             if (data.key) {
/* 227  */                 if (!mfp.popupsCache[data.key]) {
/* 228  */                     mfp.popupsCache[data.key] = {};
/* 229  */                 }
/* 230  */                 mfp.currTemplate = mfp.popupsCache[data.key];
/* 231  */             } else {
/* 232  */                 mfp.currTemplate = {};
/* 233  */             }
/* 234  */ 
/* 235  */ 
/* 236  */ 
/* 237  */             mfp.st = $.extend(true, {}, $.magnificPopup.defaults, data);
/* 238  */             mfp.fixedContentPos = mfp.st.fixedContentPos === 'auto' ? !mfp.probablyMobile : mfp.st.fixedContentPos;
/* 239  */ 
/* 240  */             if (mfp.st.modal) {
/* 241  */                 mfp.st.closeOnContentClick = false;
/* 242  */                 mfp.st.closeOnBgClick = false;
/* 243  */                 mfp.st.showCloseBtn = false;
/* 244  */                 mfp.st.enableEscapeKey = false;
/* 245  */             }
/* 246  */ 
/* 247  */ 
/* 248  */             // Building markup
/* 249  */             // main containers are created only once
/* 250  */             if (!mfp.bgOverlay) {

/* magnific.js */

/* 251  */ 
/* 252  */                 // Dark overlay
/* 253  */                 mfp.bgOverlay = _getEl('bg').on('click' + EVENT_NS, function() {
/* 254  */                     mfp.close();
/* 255  */                 });
/* 256  */ 
/* 257  */                 mfp.wrap = _getEl('wrap').attr('tabindex', -1).on('click' + EVENT_NS, function(e) {
/* 258  */                     if (_checkIfClose(e.target)) {
/* 259  */                         mfp.close();
/* 260  */                     }
/* 261  */                 });
/* 262  */ 
/* 263  */                 mfp.container = _getEl('container', mfp.wrap);
/* 264  */             }
/* 265  */ 
/* 266  */             mfp.contentContainer = _getEl('content');
/* 267  */             if (mfp.st.preloader) {
/* 268  */                 mfp.preloader = _getEl('preloader', mfp.container, mfp.st.tLoading);
/* 269  */             }
/* 270  */ 
/* 271  */ 
/* 272  */             // Initializing modules
/* 273  */             var modules = $.magnificPopup.modules;
/* 274  */             for (i = 0; i < modules.length; i++) {
/* 275  */                 var n = modules[i];
/* 276  */                 n = n.charAt(0).toUpperCase() + n.slice(1);
/* 277  */                 mfp['init' + n].call(mfp);
/* 278  */             }
/* 279  */             _mfpTrigger('BeforeOpen');
/* 280  */ 
/* 281  */ 
/* 282  */             if (mfp.st.showCloseBtn) {
/* 283  */                 // Close button
/* 284  */                 if (!mfp.st.closeBtnInside) {
/* 285  */                     mfp.wrap.append(_getCloseBtn());
/* 286  */                 } else {
/* 287  */                     _mfpOn(MARKUP_PARSE_EVENT, function(e, template, values, item) {
/* 288  */                         values.close_replaceWith = _getCloseBtn(item.type);
/* 289  */                     });
/* 290  */                     _wrapClasses += ' mfp-close-btn-in';
/* 291  */                 }
/* 292  */             }
/* 293  */ 
/* 294  */             if (mfp.st.alignTop) {
/* 295  */                 _wrapClasses += ' mfp-align-top';
/* 296  */             }
/* 297  */ 
/* 298  */ 
/* 299  */ 
/* 300  */             if (mfp.fixedContentPos) {

/* magnific.js */

/* 301  */                 mfp.wrap.css({
/* 302  */                     overflow: mfp.st.overflowY,
/* 303  */                     overflowX: 'hidden',
/* 304  */                     overflowY: mfp.st.overflowY
/* 305  */                 });
/* 306  */             } else {
/* 307  */                 mfp.wrap.css({
/* 308  */                     top: _window.scrollTop(),
/* 309  */                     position: 'absolute'
/* 310  */                 });
/* 311  */             }
/* 312  */             if (mfp.st.fixedBgPos === false || (mfp.st.fixedBgPos === 'auto' && !mfp.fixedContentPos)) {
/* 313  */                 mfp.bgOverlay.css({
/* 314  */                     height: _document.height(),
/* 315  */                     position: 'absolute'
/* 316  */                 });
/* 317  */             }
/* 318  */ 
/* 319  */ 
/* 320  */ 
/* 321  */             if (mfp.st.enableEscapeKey) {
/* 322  */                 // Close on ESC key
/* 323  */                 _document.on('keyup' + EVENT_NS, function(e) {
/* 324  */                     if (e.keyCode === 27) {
/* 325  */                         mfp.close();
/* 326  */                     }
/* 327  */                 });
/* 328  */             }
/* 329  */ 
/* 330  */             _window.on('resize' + EVENT_NS, function() {
/* 331  */                 mfp.updateSize();
/* 332  */             });
/* 333  */ 
/* 334  */ 
/* 335  */             if (!mfp.st.closeOnContentClick) {
/* 336  */                 _wrapClasses += ' mfp-auto-cursor';
/* 337  */             }
/* 338  */ 
/* 339  */             if (_wrapClasses)
/* 340  */                 mfp.wrap.addClass(_wrapClasses);
/* 341  */ 
/* 342  */ 
/* 343  */             // this triggers recalculation of layout, so we get it once to not to trigger twice
/* 344  */             var windowHeight = mfp.wH = _window.height();
/* 345  */ 
/* 346  */ 
/* 347  */             var windowStyles = {};
/* 348  */ 
/* 349  */             // if (mfp.fixedContentPos) {
/* 350  */             //     if (mfp._hasScrollBar(windowHeight)) {

/* magnific.js */

/* 351  */             //         var s = mfp._getScrollbarSize();
/* 352  */             //         if (s) {
/* 353  */             //             windowStyles.paddingRight = s;
/* 354  */             //         }
/* 355  */             //     }
/* 356  */             // }
/* 357  */ 
/* 358  */             // if (mfp.fixedContentPos) {
/* 359  */             //     if (!mfp.isIE7) {
/* 360  */             //         windowStyles.overflow = 'hidden';
/* 361  */             //     } else {
/* 362  */             //         // ie7 double-scroll bug
/* 363  */             //         $('body, html').css('overflow', 'hidden');
/* 364  */             //     }
/* 365  */             // }
/* 366  */ 
/* 367  */ 
/* 368  */ 
/* 369  */             var classesToadd = mfp.st.mainClass;
/* 370  */             if (mfp.isIE7) {
/* 371  */                 classesToadd += ' mfp-ie7';
/* 372  */             }
/* 373  */             if (classesToadd) {
/* 374  */                 mfp._addClassToMFP(classesToadd);
/* 375  */             }
/* 376  */ 
/* 377  */             // add content
/* 378  */             mfp.updateItemHTML();
/* 379  */ 
/* 380  */             _mfpTrigger('BuildControls');
/* 381  */ 
/* 382  */ 
/* 383  */             // remove scrollbar, add padding e.t.c
/* 384  */             $('html').css(windowStyles);
/* 385  */ 
/* 386  */             // add everything to DOM
/* 387  */             mfp.bgOverlay.add(mfp.wrap).prependTo(document.body);
/* 388  */ 
/* 389  */ 
/* 390  */ 
/* 391  */             // Save last focused element
/* 392  */             mfp._lastFocusedEl = document.activeElement;
/* 393  */ 
/* 394  */             // Wait for next cycle to allow CSS transition
/* 395  */             setTimeout(function() {
/* 396  */ 
/* 397  */                 if (mfp.content) {
/* 398  */                     mfp._addClassToMFP(READY_CLASS);
/* 399  */                     _setFocus();
/* 400  */                 } else {

/* magnific.js */

/* 401  */                     // if content is not defined (not loaded e.t.c) we add class only for BG
/* 402  */                     mfp.bgOverlay.addClass(READY_CLASS);
/* 403  */                 }
/* 404  */ 
/* 405  */                 // Trap the focus in popup
/* 406  */                 _document.on('focusin' + EVENT_NS, function(e) {
/* 407  */                     if (e.target !== mfp.wrap[0] && !$.contains(mfp.wrap[0], e.target)) {
/* 408  */                         _setFocus();
/* 409  */                         return false;
/* 410  */                     }
/* 411  */                 });
/* 412  */ 
/* 413  */             }, 16);
/* 414  */ 
/* 415  */             mfp.isOpen = true;
/* 416  */             mfp.updateSize(windowHeight);
/* 417  */             _mfpTrigger(OPEN_EVENT);
/* 418  */         },
/* 419  */ 
/* 420  */         /**
/* 421  *|          * Closes the popup
/* 422  *|          */
/* 423  */         close: function() {
/* 424  */             if (!mfp.isOpen) return;
/* 425  */             _mfpTrigger(BEFORE_CLOSE_EVENT);
/* 426  */ 
/* 427  */             mfp.isOpen = false;
/* 428  */             // for CSS3 animation
/* 429  */             if (mfp.st.removalDelay && !mfp.isLowIE && mfp.supportsTransition) {
/* 430  */                 mfp._addClassToMFP(REMOVING_CLASS);
/* 431  */                 setTimeout(function() {
/* 432  */                     mfp._close();
/* 433  */                 }, mfp.st.removalDelay);
/* 434  */             } else {
/* 435  */                 mfp._close();
/* 436  */             }
/* 437  */         },
/* 438  */ 
/* 439  */         /**
/* 440  *|          * Helper for close() function
/* 441  *|          */
/* 442  */         _close: function() {
/* 443  */             _mfpTrigger(CLOSE_EVENT);
/* 444  */ 
/* 445  */             var classesToRemove = REMOVING_CLASS + ' ' + READY_CLASS + ' ';
/* 446  */ 
/* 447  */             mfp.bgOverlay.detach();
/* 448  */             mfp.wrap.detach();
/* 449  */             mfp.container.empty();
/* 450  */ 

/* magnific.js */

/* 451  */             if (mfp.st.mainClass) {
/* 452  */                 classesToRemove += mfp.st.mainClass + ' ';
/* 453  */             }
/* 454  */ 
/* 455  */             mfp._removeClassFromMFP(classesToRemove);
/* 456  */ 
/* 457  */             if (mfp.fixedContentPos) {
/* 458  */                 var windowStyles = {
/* 459  */                     paddingRight: ''
/* 460  */                 };
/* 461  */                 // if (mfp.isIE7) {
/* 462  */                 //     $('body, html').css('overflow', '');
/* 463  */                 // } else {
/* 464  */                 //     windowStyles.overflow = '';
/* 465  */                 // }
/* 466  */                 $('html').css(windowStyles);
/* 467  */             }
/* 468  */ 
/* 469  */             _document.off('keyup' + EVENT_NS + ' focusin' + EVENT_NS);
/* 470  */             mfp.ev.off(EVENT_NS);
/* 471  */ 
/* 472  */             // clean up DOM elements that aren't removed
/* 473  */             mfp.wrap.attr('class', 'mfp-wrap').removeAttr('style');
/* 474  */             mfp.bgOverlay.attr('class', 'mfp-bg');
/* 475  */             mfp.container.attr('class', 'mfp-container');
/* 476  */ 
/* 477  */             // remove close button from target element
/* 478  */             if (mfp.st.showCloseBtn &&
/* 479  */                 (!mfp.st.closeBtnInside || mfp.currTemplate[mfp.currItem.type] === true)) {
/* 480  */                 if (mfp.currTemplate.closeBtn)
/* 481  */                     mfp.currTemplate.closeBtn.detach();
/* 482  */             }
/* 483  */ 
/* 484  */ 
/* 485  */             if (mfp._lastFocusedEl) {
/* 486  */                 $(mfp._lastFocusedEl).trigger('focus'); // put tab focus back
/* 487  */             }
/* 488  */             mfp.currItem = null;
/* 489  */             mfp.content = null;
/* 490  */             mfp.currTemplate = null;
/* 491  */             mfp.prevHeight = 0;
/* 492  */ 
/* 493  */             _mfpTrigger(AFTER_CLOSE_EVENT);
/* 494  */         },
/* 495  */ 
/* 496  */         updateSize: function(winHeight) {
/* 497  */ 
/* 498  */             if (mfp.isIOS) {
/* 499  */                 // fixes iOS nav bars https://github.com/dimsemenov/Magnific-Popup/issues/2
/* 500  */                 var zoomLevel = document.documentElement.clientWidth / window.innerWidth;

/* magnific.js */

/* 501  */                 var height = window.innerHeight * zoomLevel;
/* 502  */                 mfp.wrap.css('height', height);
/* 503  */                 mfp.wH = height;
/* 504  */             } else {
/* 505  */                 mfp.wH = winHeight || _window.height();
/* 506  */             }
/* 507  */             // Fixes #84: popup incorrectly positioned with position:relative on body
/* 508  */             if (!mfp.fixedContentPos) {
/* 509  */                 mfp.wrap.css('height', mfp.wH);
/* 510  */             }
/* 511  */ 
/* 512  */             _mfpTrigger('Resize');
/* 513  */ 
/* 514  */         },
/* 515  */ 
/* 516  */         /**
/* 517  *|          * Set content of popup based on current index
/* 518  *|          */
/* 519  */         updateItemHTML: function() {
/* 520  */             var item = mfp.items[mfp.index];
/* 521  */ 
/* 522  */             // Detach and perform modifications
/* 523  */             mfp.contentContainer.detach();
/* 524  */ 
/* 525  */             if (mfp.content)
/* 526  */                 mfp.content.detach();
/* 527  */ 
/* 528  */             if (!item.parsed) {
/* 529  */                 item = mfp.parseEl(mfp.index);
/* 530  */             }
/* 531  */ 
/* 532  */             var type = item.type;
/* 533  */ 
/* 534  */             _mfpTrigger('BeforeChange', [mfp.currItem ? mfp.currItem.type : '', type]);
/* 535  */             // BeforeChange event works like so:
/* 536  */             // _mfpOn('BeforeChange', function(e, prevType, newType) { });
/* 537  */ 
/* 538  */             mfp.currItem = item;
/* 539  */ 
/* 540  */ 
/* 541  */ 
/* 542  */ 
/* 543  */ 
/* 544  */             if (!mfp.currTemplate[type]) {
/* 545  */                 var markup = mfp.st[type] ? mfp.st[type].markup : false;
/* 546  */ 
/* 547  */                 // allows to modify markup
/* 548  */                 _mfpTrigger('FirstMarkupParse', markup);
/* 549  */ 
/* 550  */                 if (markup) {

/* magnific.js */

/* 551  */                     mfp.currTemplate[type] = $(markup);
/* 552  */                 } else {
/* 553  */                     // if there is no markup found we just define that template is parsed
/* 554  */                     mfp.currTemplate[type] = true;
/* 555  */                 }
/* 556  */             }
/* 557  */ 
/* 558  */             if (_prevContentType && _prevContentType !== item.type) {
/* 559  */                 mfp.container.removeClass('mfp-' + _prevContentType + '-holder');
/* 560  */             }
/* 561  */ 
/* 562  */             var newContent = mfp['get' + type.charAt(0).toUpperCase() + type.slice(1)](item, mfp.currTemplate[type]);
/* 563  */             mfp.appendContent(newContent, type);
/* 564  */ 
/* 565  */             item.preloaded = true;
/* 566  */ 
/* 567  */             _mfpTrigger(CHANGE_EVENT, item);
/* 568  */             _prevContentType = item.type;
/* 569  */ 
/* 570  */             // Append container back after its content changed
/* 571  */             mfp.container.prepend(mfp.contentContainer);
/* 572  */ 
/* 573  */             _mfpTrigger('AfterChange');
/* 574  */         },
/* 575  */ 
/* 576  */ 
/* 577  */         /**
/* 578  *|          * Set HTML content of popup
/* 579  *|          */
/* 580  */         appendContent: function(newContent, type) {
/* 581  */             mfp.content = newContent;
/* 582  */ 
/* 583  */             if (newContent) {
/* 584  */                 if (mfp.st.showCloseBtn && mfp.st.closeBtnInside &&
/* 585  */                     mfp.currTemplate[type] === true) {
/* 586  */                     // if there is no markup, we just append close button element inside
/* 587  */                     if (!mfp.content.find('.mfp-close').length) {
/* 588  */                         mfp.content.append(_getCloseBtn());
/* 589  */                     }
/* 590  */                 } else {
/* 591  */                     mfp.content = newContent;
/* 592  */                 }
/* 593  */             } else {
/* 594  */                 mfp.content = '';
/* 595  */             }
/* 596  */ 
/* 597  */             _mfpTrigger(BEFORE_APPEND_EVENT);
/* 598  */             mfp.container.addClass('mfp-' + type + '-holder');
/* 599  */ 
/* 600  */             mfp.contentContainer.append(mfp.content);

/* magnific.js */

/* 601  */         },
/* 602  */ 
/* 603  */ 
/* 604  */ 
/* 605  */ 
/* 606  */         /**
/* 607  *|          * Creates Magnific Popup data object based on given data
/* 608  *|          * @param  {int} index Index of item to parse
/* 609  *|          */
/* 610  */         parseEl: function(index) {
/* 611  */             var item = mfp.items[index],
/* 612  */                 type = item.type;
/* 613  */ 
/* 614  */             if (item.tagName) {
/* 615  */                 item = {
/* 616  */                     el: $(item)
/* 617  */                 };
/* 618  */             } else {
/* 619  */                 item = {
/* 620  */                     data: item,
/* 621  */                     src: item.src
/* 622  */                 };
/* 623  */             }
/* 624  */ 
/* 625  */             if (item.el) {
/* 626  */                 var types = mfp.types;
/* 627  */ 
/* 628  */                 // check for 'mfp-TYPE' class
/* 629  */                 for (var i = 0; i < types.length; i++) {
/* 630  */                     if (item.el.hasClass('mfp-' + types[i])) {
/* 631  */                         type = types[i];
/* 632  */                         break;
/* 633  */                     }
/* 634  */                 }
/* 635  */ 
/* 636  */                 item.src = item.el.attr('data-mfp-src');
/* 637  */                 if (!item.src) {
/* 638  */                     item.src = item.el.attr('href');
/* 639  */                 }
/* 640  */             }
/* 641  */ 
/* 642  */             item.type = type || mfp.st.type || 'inline';
/* 643  */             item.index = index;
/* 644  */             item.parsed = true;
/* 645  */             mfp.items[index] = item;
/* 646  */             _mfpTrigger('ElementParse', item);
/* 647  */ 
/* 648  */             return mfp.items[index];
/* 649  */         },
/* 650  */ 

/* magnific.js */

/* 651  */ 
/* 652  */         /**
/* 653  *|          * Initializes single popup or a group of popups
/* 654  *|          */
/* 655  */         addGroup: function(el, options) {
/* 656  */             var eHandler = function(e) {
/* 657  */                 e.mfpEl = this;
/* 658  */                 mfp._openClick(e, el, options);
/* 659  */             };
/* 660  */ 
/* 661  */             if (!options) {
/* 662  */                 options = {};
/* 663  */             }
/* 664  */ 
/* 665  */             var eName = 'click.magnificPopup';
/* 666  */             options.mainEl = el;
/* 667  */ 
/* 668  */             if (options.items) {
/* 669  */                 options.isObj = true;
/* 670  */                 el.off(eName).on(eName, eHandler);
/* 671  */             } else {
/* 672  */                 options.isObj = false;
/* 673  */                 if (options.delegate) {
/* 674  */                     el.off(eName).on(eName, options.delegate, eHandler);
/* 675  */                 } else {
/* 676  */                     options.items = el;
/* 677  */                     el.off(eName).on(eName, eHandler);
/* 678  */                 }
/* 679  */             }
/* 680  */         },
/* 681  */         _openClick: function(e, el, options) {
/* 682  */             var midClick = options.midClick !== undefined ? options.midClick : $.magnificPopup.defaults.midClick;
/* 683  */ 
/* 684  */ 
/* 685  */             if (!midClick && (e.which === 2 || e.ctrlKey || e.metaKey)) {
/* 686  */                 return;
/* 687  */             }
/* 688  */ 
/* 689  */             var disableOn = options.disableOn !== undefined ? options.disableOn : $.magnificPopup.defaults.disableOn;
/* 690  */ 
/* 691  */             if (disableOn) {
/* 692  */                 if ($.isFunction(disableOn)) {
/* 693  */                     if (!disableOn.call(mfp)) {
/* 694  */                         return true;
/* 695  */                     }
/* 696  */                 } else { // else it's number
/* 697  */                     if (_window.width() < disableOn) {
/* 698  */                         return true;
/* 699  */                     }
/* 700  */                 }

/* magnific.js */

/* 701  */             }
/* 702  */ 
/* 703  */             if (e.type) {
/* 704  */                 e.preventDefault();
/* 705  */ 
/* 706  */                 // This will prevent popup from closing if element is inside and popup is already opened
/* 707  */                 if (mfp.isOpen) {
/* 708  */                     e.stopPropagation();
/* 709  */                 }
/* 710  */             }
/* 711  */ 
/* 712  */ 
/* 713  */             options.el = $(e.mfpEl);
/* 714  */             if (options.delegate) {
/* 715  */                 options.items = el.find(options.delegate);
/* 716  */             }
/* 717  */             mfp.open(options);
/* 718  */         },
/* 719  */ 
/* 720  */ 
/* 721  */         /**
/* 722  *|          * Updates text on preloader
/* 723  *|          */
/* 724  */         updateStatus: function(status, text) {
/* 725  */ 
/* 726  */             if (mfp.preloader) {
/* 727  */                 if (_prevStatus !== status) {
/* 728  */                     mfp.container.removeClass('mfp-s-' + _prevStatus);
/* 729  */                 }
/* 730  */ 
/* 731  */                 if (!text && status === 'loading') {
/* 732  */                     text = mfp.st.tLoading;
/* 733  */                 }
/* 734  */ 
/* 735  */                 var data = {
/* 736  */                     status: status,
/* 737  */                     text: text
/* 738  */                 };
/* 739  */                 // allows to modify status
/* 740  */                 _mfpTrigger('UpdateStatus', data);
/* 741  */ 
/* 742  */                 status = data.status;
/* 743  */                 text = data.text;
/* 744  */ 
/* 745  */                 mfp.preloader.html(text);
/* 746  */ 
/* 747  */                 mfp.preloader.find('a').on('click', function(e) {
/* 748  */                     e.stopImmediatePropagation();
/* 749  */                 });
/* 750  */ 

/* magnific.js */

/* 751  */                 mfp.container.addClass('mfp-s-' + status);
/* 752  */                 _prevStatus = status;
/* 753  */             }
/* 754  */         },
/* 755  */ 
/* 756  */ 
/* 757  */         /*
/* 758  *| 		"Private" helpers that aren't private at all
/* 759  *| 	 */
/* 760  */         _addClassToMFP: function(cName) {
/* 761  */             mfp.bgOverlay.addClass(cName);
/* 762  */             mfp.wrap.addClass(cName);
/* 763  */         },
/* 764  */         _removeClassFromMFP: function(cName) {
/* 765  */             this.bgOverlay.removeClass(cName);
/* 766  */             mfp.wrap.removeClass(cName);
/* 767  */         },
/* 768  */         _hasScrollBar: function(winHeight) {
/* 769  */             return ((mfp.isIE7 ? _document.height() : document.body.scrollHeight) > (winHeight || _window.height()));
/* 770  */         },
/* 771  */         _parseMarkup: function(template, values, item) {
/* 772  */             var arr;
/* 773  */             if (item.data) {
/* 774  */                 values = $.extend(item.data, values);
/* 775  */             }
/* 776  */             _mfpTrigger(MARKUP_PARSE_EVENT, [template, values, item]);
/* 777  */ 
/* 778  */             $.each(values, function(key, value) {
/* 779  */                 if (value === undefined || value === false) {
/* 780  */                     return true;
/* 781  */                 }
/* 782  */                 arr = key.split('_');
/* 783  */                 if (arr.length > 1) {
/* 784  */                     var el = template.find(EVENT_NS + '-' + arr[0]);
/* 785  */ 
/* 786  */                     if (el.length > 0) {
/* 787  */                         var attr = arr[1];
/* 788  */                         if (attr === 'replaceWith') {
/* 789  */                             if (el[0] !== value[0]) {
/* 790  */                                 el.replaceWith(value);
/* 791  */                             }
/* 792  */                         } else if (attr === 'img') {
/* 793  */                             if (el.is('img')) {
/* 794  */                                 el.attr('src', value);
/* 795  */                             } else {
/* 796  */                                 el.replaceWith('<img src="' + value + '" class="' + el.attr('class') + '" />');
/* 797  */                             }
/* 798  */                         } else {
/* 799  */                             el.attr(arr[1], value);
/* 800  */                         }

/* magnific.js */

/* 801  */                     }
/* 802  */ 
/* 803  */                 } else {
/* 804  */                     template.find(EVENT_NS + '-' + key).html(value);
/* 805  */                 }
/* 806  */             });
/* 807  */         },
/* 808  */ 
/* 809  */         _getScrollbarSize: function() {
/* 810  */             // thx David
/* 811  */             if (mfp.scrollbarSize === undefined) {
/* 812  */                 var scrollDiv = document.createElement("div");
/* 813  */                 scrollDiv.id = "mfp-sbm";
/* 814  */                 scrollDiv.style.cssText = 'width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;';
/* 815  */                 document.body.appendChild(scrollDiv);
/* 816  */                 mfp.scrollbarSize = scrollDiv.offsetWidth - scrollDiv.clientWidth;
/* 817  */                 document.body.removeChild(scrollDiv);
/* 818  */             }
/* 819  */             return mfp.scrollbarSize;
/* 820  */         }
/* 821  */ 
/* 822  */     }; /* MagnificPopup core prototype end */
/* 823  */ 
/* 824  */ 
/* 825  */ 
/* 826  */ 
/* 827  */     /**
/* 828  *|      * Public static functions
/* 829  *|      */
/* 830  */     $.magnificPopup = {
/* 831  */         instance: null,
/* 832  */         proto: MagnificPopup.prototype,
/* 833  */         modules: [],
/* 834  */ 
/* 835  */         open: function(options, index) {
/* 836  */             _checkInstance();
/* 837  */ 
/* 838  */             if (!options)
/* 839  */                 options = {};
/* 840  */ 
/* 841  */             options.isObj = true;
/* 842  */             options.index = index || 0;
/* 843  */             return this.instance.open(options);
/* 844  */         },
/* 845  */ 
/* 846  */         close: function() {
/* 847  */             return $.magnificPopup.instance.close();
/* 848  */         },
/* 849  */ 
/* 850  */         registerModule: function(name, module) {

/* magnific.js */

/* 851  */             if (module.options) {
/* 852  */                 $.magnificPopup.defaults[name] = module.options;
/* 853  */             }
/* 854  */             $.extend(this.proto, module.proto);
/* 855  */             this.modules.push(name);
/* 856  */         },
/* 857  */ 
/* 858  */         defaults: {
/* 859  */ 
/* 860  */             // Info about options is in docs:
/* 861  */             // http://dimsemenov.com/plugins/magnific-popup/documentation.html#options
/* 862  */ 
/* 863  */             disableOn: 0,
/* 864  */ 
/* 865  */             key: null,
/* 866  */ 
/* 867  */             midClick: true,
/* 868  */ 
/* 869  */             mainClass: '',
/* 870  */ 
/* 871  */             preloader: true,
/* 872  */ 
/* 873  */             focus: '', // CSS selector of input to focus after popup is opened
/* 874  */ 
/* 875  */             closeOnContentClick: false,
/* 876  */ 
/* 877  */             closeOnBgClick: true,
/* 878  */ 
/* 879  */             closeBtnInside: false,
/* 880  */ 
/* 881  */             showCloseBtn: true,
/* 882  */ 
/* 883  */             enableEscapeKey: true,
/* 884  */ 
/* 885  */             modal: false,
/* 886  */ 
/* 887  */             alignTop: false,
/* 888  */ 
/* 889  */             removalDelay: 300,
/* 890  */ 
/* 891  */             fixedContentPos: 'auto',
/* 892  */ 
/* 893  */             fixedBgPos: 'auto',
/* 894  */ 
/* 895  */             overflowY: 'auto',
/* 896  */ 
/* 897  */             closeMarkup: '<button title="%title%" type="button" class="mfp-close">&times;</button>',
/* 898  */ 
/* 899  */             tClose: 'Close (Esc)',
/* 900  */ 

/* magnific.js */

/* 901  */             tLoading: 'Loading...',
/* 902  */ 
/* 903  */             callbacks: {
/* 904  */                 beforeOpen: function() {
/* 905  */                     this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
/* 906  */                     if(typeof this.st!='undefined' && typeof this.st.el!="undefined")
/* 907  */                     this.st.mainClass = this.st.el.attr('data-effect');
/* 908  */                 }
/* 909  */             }
/* 910  */ 
/* 911  */         }
/* 912  */     };
/* 913  */ 
/* 914  */ 
/* 915  */ 
/* 916  */     $.fn.magnificPopup = function(options) {
/* 917  */         _checkInstance();
/* 918  */ 
/* 919  */         var jqEl = $(this);
/* 920  */ 
/* 921  */         // We call some API method of first param is a string
/* 922  */         if (typeof options === "string") {
/* 923  */ 
/* 924  */             if (options === 'open') {
/* 925  */                 var items,
/* 926  */                     itemOpts = _isJQ ? jqEl.data('magnificPopup') : jqEl[0].magnificPopup,
/* 927  */                     index = parseInt(arguments[1], 10) || 0;
/* 928  */ 
/* 929  */                 if (itemOpts.items) {
/* 930  */                     items = itemOpts.items[index];
/* 931  */                 } else {
/* 932  */                     items = jqEl;
/* 933  */                     if (itemOpts.delegate) {
/* 934  */                         items = items.find(itemOpts.delegate);
/* 935  */                     }
/* 936  */                     items = items.eq(index);
/* 937  */                 }
/* 938  */                 mfp._openClick({
/* 939  */                     mfpEl: items
/* 940  */                 }, jqEl, itemOpts);
/* 941  */             } else {
/* 942  */                 if (mfp.isOpen)
/* 943  */                     mfp[options].apply(mfp, Array.prototype.slice.call(arguments, 1));
/* 944  */             }
/* 945  */ 
/* 946  */         } else {
/* 947  */ 
/* 948  */             /*
/* 949  *|              * As Zepto doesn't support .data() method for objects
/* 950  *|              * and it works only in normal browsers

/* magnific.js */

/* 951  *|              * we assign "options" object directly to the DOM element. FTW!
/* 952  *|              */
/* 953  */             if (_isJQ) {
/* 954  */                 jqEl.data('magnificPopup', options);
/* 955  */             } else {
/* 956  */                 jqEl[0].magnificPopup = options;
/* 957  */             }
/* 958  */ 
/* 959  */             mfp.addGroup(jqEl, options);
/* 960  */ 
/* 961  */         }
/* 962  */         return jqEl;
/* 963  */     };
/* 964  */ 
/* 965  */ 
/* 966  */     //Quick benchmark
/* 967  */     /*
/* 968  *| var start = performance.now(),
/* 969  *| 	i,
/* 970  *| 	rounds = 1000;
/* 971  *| 
/* 972  *| for(i = 0; i < rounds; i++) {
/* 973  *| 
/* 974  *| }
/* 975  *| console.log('Test #1:', performance.now() - start);
/* 976  *| 
/* 977  *| start = performance.now();
/* 978  *| for(i = 0; i < rounds; i++) {
/* 979  *| 
/* 980  *| }
/* 981  *| console.log('Test #2:', performance.now() - start);
/* 982  *| */
/* 983  */ 
/* 984  */ 
/* 985  */     /*>>core*/
/* 986  */ 
/* 987  */     /*>>inline*/
/* 988  */ 
/* 989  */     var INLINE_NS = 'inline',
/* 990  */         _hiddenClass,
/* 991  */         _inlinePlaceholder,
/* 992  */         _lastInlineElement,
/* 993  */         _putInlineElementsBack = function() {
/* 994  */             if (_lastInlineElement) {
/* 995  */                 _inlinePlaceholder.after(_lastInlineElement.addClass(_hiddenClass)).detach();
/* 996  */                 _lastInlineElement = null;
/* 997  */             }
/* 998  */         };
/* 999  */ 
/* 1000 */     $.magnificPopup.registerModule(INLINE_NS, {

/* magnific.js */

/* 1001 */         options: {
/* 1002 */             hiddenClass: 'hide', // will be appended with `mfp-` prefix
/* 1003 */             markup: '',
/* 1004 */             tNotFound: 'Content not found'
/* 1005 */         },
/* 1006 */         proto: {
/* 1007 */ 
/* 1008 */             initInline: function() {
/* 1009 */                 mfp.types.push(INLINE_NS);
/* 1010 */ 
/* 1011 */                 _mfpOn(CLOSE_EVENT + '.' + INLINE_NS, function() {
/* 1012 */                     _putInlineElementsBack();
/* 1013 */                 });
/* 1014 */             },
/* 1015 */ 
/* 1016 */             getInline: function(item, template) {
/* 1017 */ 
/* 1018 */                 _putInlineElementsBack();
/* 1019 */ 
/* 1020 */                 if (item.src) {
/* 1021 */                     var inlineSt = mfp.st.inline,
/* 1022 */                         el = $(item.src);
/* 1023 */ 
/* 1024 */                     if (el.length) {
/* 1025 */ 
/* 1026 */                         // If target element has parent - we replace it with placeholder and put it back after popup is closed
/* 1027 */                         var parent = el[0].parentNode;
/* 1028 */                         if (parent && parent.tagName) {
/* 1029 */                             if (!_inlinePlaceholder) {
/* 1030 */                                 _hiddenClass = inlineSt.hiddenClass;
/* 1031 */                                 _inlinePlaceholder = _getEl(_hiddenClass);
/* 1032 */                                 _hiddenClass = 'mfp-' + _hiddenClass;
/* 1033 */                             }
/* 1034 */                             // replace target inline element with placeholder
/* 1035 */                             _lastInlineElement = el.after(_inlinePlaceholder).detach().removeClass(_hiddenClass);
/* 1036 */                         }
/* 1037 */ 
/* 1038 */                         mfp.updateStatus('ready');
/* 1039 */                     } else {
/* 1040 */                         mfp.updateStatus('error', inlineSt.tNotFound);
/* 1041 */                         el = $('<div>');
/* 1042 */                     }
/* 1043 */ 
/* 1044 */                     item.inlineElement = el;
/* 1045 */                     return el;
/* 1046 */                 }
/* 1047 */ 
/* 1048 */                 mfp.updateStatus('ready');
/* 1049 */                 mfp._parseMarkup(template, {}, item);
/* 1050 */                 return template;

/* magnific.js */

/* 1051 */             }
/* 1052 */         }
/* 1053 */     });
/* 1054 */ 
/* 1055 */     /*>>inline*/
/* 1056 */ 
/* 1057 */     /*>>ajax*/
/* 1058 */     var AJAX_NS = 'ajax',
/* 1059 */         _ajaxCur,
/* 1060 */         _removeAjaxCursor = function() {
/* 1061 */             if (_ajaxCur) {
/* 1062 */                 _body.removeClass(_ajaxCur);
/* 1063 */             }
/* 1064 */         };
/* 1065 */ 
/* 1066 */     $.magnificPopup.registerModule(AJAX_NS, {
/* 1067 */ 
/* 1068 */         options: {
/* 1069 */             settings: null,
/* 1070 */             cursor: 'mfp-ajax-cur',
/* 1071 */             tError: '<a href="%url%">The content</a> could not be loaded.'
/* 1072 */         },
/* 1073 */ 
/* 1074 */         proto: {
/* 1075 */             initAjax: function() {
/* 1076 */                 mfp.types.push(AJAX_NS);
/* 1077 */                 _ajaxCur = mfp.st.ajax.cursor;
/* 1078 */ 
/* 1079 */                 _mfpOn(CLOSE_EVENT + '.' + AJAX_NS, function() {
/* 1080 */                     _removeAjaxCursor();
/* 1081 */                     if (mfp.req) {
/* 1082 */                         mfp.req.abort();
/* 1083 */                     }
/* 1084 */                 });
/* 1085 */             },
/* 1086 */ 
/* 1087 */             getAjax: function(item) {
/* 1088 */ 
/* 1089 */                 if (_ajaxCur)
/* 1090 */                     _body.addClass(_ajaxCur);
/* 1091 */ 
/* 1092 */                 mfp.updateStatus('loading');
/* 1093 */ 
/* 1094 */                 var opts = $.extend({
/* 1095 */                     url: item.src,
/* 1096 */                     success: function(data, textStatus, jqXHR) {
/* 1097 */                         var temp = {
/* 1098 */                             data: data,
/* 1099 */                             xhr: jqXHR
/* 1100 */                         };

/* magnific.js */

/* 1101 */ 
/* 1102 */                         _mfpTrigger('ParseAjax', temp);
/* 1103 */ 
/* 1104 */                         mfp.appendContent($(temp.data), AJAX_NS);
/* 1105 */ 
/* 1106 */                         item.finished = true;
/* 1107 */ 
/* 1108 */                         _removeAjaxCursor();
/* 1109 */ 
/* 1110 */                         _setFocus();
/* 1111 */ 
/* 1112 */                         setTimeout(function() {
/* 1113 */                             mfp.wrap.addClass(READY_CLASS);
/* 1114 */                         }, 16);
/* 1115 */ 
/* 1116 */                         mfp.updateStatus('ready');
/* 1117 */ 
/* 1118 */                         _mfpTrigger('AjaxContentAdded');
/* 1119 */                     },
/* 1120 */                     error: function() {
/* 1121 */                         _removeAjaxCursor();
/* 1122 */                         item.finished = item.loadError = true;
/* 1123 */                         mfp.updateStatus('error', mfp.st.ajax.tError.replace('%url%', item.src));
/* 1124 */                     }
/* 1125 */                 }, mfp.st.ajax.settings);
/* 1126 */ 
/* 1127 */                 mfp.req = $.ajax(opts);
/* 1128 */ 
/* 1129 */                 return '';
/* 1130 */             }
/* 1131 */         }
/* 1132 */     });
/* 1133 */ 
/* 1134 */ 
/* 1135 */ 
/* 1136 */ 
/* 1137 */ 
/* 1138 */ 
/* 1139 */ 
/* 1140 */     /*>>ajax*/
/* 1141 */ 
/* 1142 */     /*>>image*/
/* 1143 */     var _imgInterval,
/* 1144 */         _getTitle = function(item) {
/* 1145 */             if (item.data && item.data.title !== undefined)
/* 1146 */                 return item.data.title;
/* 1147 */ 
/* 1148 */             var src = mfp.st.image.titleSrc;
/* 1149 */ 
/* 1150 */             if (src) {

/* magnific.js */

/* 1151 */                 if ($.isFunction(src)) {
/* 1152 */                     return src.call(mfp, item);
/* 1153 */                 } else if (item.el) {
/* 1154 */                     return item.el.attr(src) || '';
/* 1155 */                 }
/* 1156 */             }
/* 1157 */             return '';
/* 1158 */         };
/* 1159 */ 
/* 1160 */     $.magnificPopup.registerModule('image', {
/* 1161 */ 
/* 1162 */         options: {
/* 1163 */             markup: '<div class="mfp-figure">' +
/* 1164 */                 '<div class="mfp-close"></div>' +
/* 1165 */                 '<div class="mfp-img"></div>' +
/* 1166 */                 '<div class="mfp-bottom-bar">' +
/* 1167 */                 '<div class="mfp-title"></div>' +
/* 1168 */                 '<div class="mfp-counter"></div>' +
/* 1169 */                 '</div>' +
/* 1170 */                 '</div>',
/* 1171 */             cursor: 'mfp-zoom-out-cur',
/* 1172 */             titleSrc: 'title',
/* 1173 */             verticalFit: true,
/* 1174 */             tError: '<a href="%url%">The image</a> could not be loaded.'
/* 1175 */         },
/* 1176 */ 
/* 1177 */         proto: {
/* 1178 */             initImage: function() {
/* 1179 */                 var imgSt = mfp.st.image,
/* 1180 */                     ns = '.image';
/* 1181 */ 
/* 1182 */                 mfp.types.push('image');
/* 1183 */ 
/* 1184 */                 _mfpOn(OPEN_EVENT + ns, function() {
/* 1185 */                     if (mfp.currItem.type === 'image' && imgSt.cursor) {
/* 1186 */                         _body.addClass(imgSt.cursor);
/* 1187 */                     }
/* 1188 */                 });
/* 1189 */ 
/* 1190 */                 _mfpOn(CLOSE_EVENT + ns, function() {
/* 1191 */                     if (imgSt.cursor) {
/* 1192 */                         _body.removeClass(imgSt.cursor);
/* 1193 */                     }
/* 1194 */                     _window.off('resize' + EVENT_NS);
/* 1195 */                 });
/* 1196 */ 
/* 1197 */                 _mfpOn('Resize' + ns, mfp.resizeImage);
/* 1198 */                 if (mfp.isLowIE) {
/* 1199 */                     _mfpOn('AfterChange', mfp.resizeImage);
/* 1200 */                 }

/* magnific.js */

/* 1201 */             },
/* 1202 */             resizeImage: function() {
/* 1203 */                 var item = mfp.currItem;
/* 1204 */                 if (!item.img) return;
/* 1205 */ 
/* 1206 */                 if (mfp.st.image.verticalFit) {
/* 1207 */                     var decr = 0;
/* 1208 */                     // fix box-sizing in ie7/8
/* 1209 */                     if (mfp.isLowIE) {
/* 1210 */                         decr = parseInt(item.img.css('padding-top'), 10) + parseInt(item.img.css('padding-bottom'), 10);
/* 1211 */                     }
/* 1212 */                     item.img.css('max-height', mfp.wH - decr);
/* 1213 */                 }
/* 1214 */             },
/* 1215 */             _onImageHasSize: function(item) {
/* 1216 */                 if (item.img) {
/* 1217 */ 
/* 1218 */                     item.hasSize = true;
/* 1219 */ 
/* 1220 */                     if (_imgInterval) {
/* 1221 */                         clearInterval(_imgInterval);
/* 1222 */                     }
/* 1223 */ 
/* 1224 */                     item.isCheckingImgSize = false;
/* 1225 */ 
/* 1226 */                     _mfpTrigger('ImageHasSize', item);
/* 1227 */ 
/* 1228 */                     if (item.imgHidden) {
/* 1229 */                         if (mfp.content)
/* 1230 */                             mfp.content.removeClass('mfp-loading');
/* 1231 */ 
/* 1232 */                         item.imgHidden = false;
/* 1233 */                     }
/* 1234 */ 
/* 1235 */                 }
/* 1236 */             },
/* 1237 */ 
/* 1238 */             /**
/* 1239 *|              * Function that loops until the image has size to display elements that rely on it asap
/* 1240 *|              */
/* 1241 */             findImageSize: function(item) {
/* 1242 */ 
/* 1243 */                 var counter = 0,
/* 1244 */                     img = item.img[0],
/* 1245 */                     mfpSetInterval = function(delay) {
/* 1246 */ 
/* 1247 */                         if (_imgInterval) {
/* 1248 */                             clearInterval(_imgInterval);
/* 1249 */                         }
/* 1250 */                         // decelerating interval that checks for size of an image

/* magnific.js */

/* 1251 */                         _imgInterval = setInterval(function() {
/* 1252 */                             if (img.naturalWidth > 0) {
/* 1253 */                                 mfp._onImageHasSize(item);
/* 1254 */                                 return;
/* 1255 */                             }
/* 1256 */ 
/* 1257 */                             if (counter > 200) {
/* 1258 */                                 clearInterval(_imgInterval);
/* 1259 */                             }
/* 1260 */ 
/* 1261 */                             counter++;
/* 1262 */                             if (counter === 3) {
/* 1263 */                                 mfpSetInterval(10);
/* 1264 */                             } else if (counter === 40) {
/* 1265 */                                 mfpSetInterval(50);
/* 1266 */                             } else if (counter === 100) {
/* 1267 */                                 mfpSetInterval(500);
/* 1268 */                             }
/* 1269 */                         }, delay);
/* 1270 */                     };
/* 1271 */ 
/* 1272 */                 mfpSetInterval(1);
/* 1273 */             },
/* 1274 */ 
/* 1275 */             getImage: function(item, template) {
/* 1276 */ 
/* 1277 */                 var guard = 0,
/* 1278 */ 
/* 1279 */                     // image load complete handler
/* 1280 */                     onLoadComplete = function() {
/* 1281 */                         if (item) {
/* 1282 */                             if (item.img[0].complete) {
/* 1283 */                                 item.img.off('.mfploader');
/* 1284 */ 
/* 1285 */                                 if (item === mfp.currItem) {
/* 1286 */                                     mfp._onImageHasSize(item);
/* 1287 */ 
/* 1288 */                                     mfp.updateStatus('ready');
/* 1289 */                                 }
/* 1290 */ 
/* 1291 */                                 item.hasSize = true;
/* 1292 */                                 item.loaded = true;
/* 1293 */ 
/* 1294 */                                 _mfpTrigger('ImageLoadComplete');
/* 1295 */ 
/* 1296 */                             } else {
/* 1297 */                                 // if image complete check fails 200 times (20 sec), we assume that there was an error.
/* 1298 */                                 guard++;
/* 1299 */                                 if (guard < 200) {
/* 1300 */                                     setTimeout(onLoadComplete, 100);

/* magnific.js */

/* 1301 */                                 } else {
/* 1302 */                                     onLoadError();
/* 1303 */                                 }
/* 1304 */                             }
/* 1305 */                         }
/* 1306 */                     },
/* 1307 */ 
/* 1308 */                     // image error handler
/* 1309 */                     onLoadError = function() {
/* 1310 */                         if (item) {
/* 1311 */                             item.img.off('.mfploader');
/* 1312 */                             if (item === mfp.currItem) {
/* 1313 */                                 mfp._onImageHasSize(item);
/* 1314 */                                 mfp.updateStatus('error', imgSt.tError.replace('%url%', item.src));
/* 1315 */                             }
/* 1316 */ 
/* 1317 */                             item.hasSize = true;
/* 1318 */                             item.loaded = true;
/* 1319 */                             item.loadError = true;
/* 1320 */                         }
/* 1321 */                     },
/* 1322 */                     imgSt = mfp.st.image;
/* 1323 */ 
/* 1324 */ 
/* 1325 */                 var el = template.find('.mfp-img');
/* 1326 */                 if (el.length) {
/* 1327 */                     var img = new Image();
/* 1328 */                     img.className = 'mfp-img';
/* 1329 */                     item.img = $(img).on('load.mfploader', onLoadComplete).on('error.mfploader', onLoadError);
/* 1330 */                     img.src = item.src;
/* 1331 */ 
/* 1332 */                     // without clone() "error" event is not firing when IMG is replaced by new IMG
/* 1333 */                     // TODO: find a way to avoid such cloning
/* 1334 */                     if (el.is('img')) {
/* 1335 */                         item.img = item.img.clone();
/* 1336 */                     }
/* 1337 */                     if (item.img[0].naturalWidth > 0) {
/* 1338 */                         item.hasSize = true;
/* 1339 */                     }
/* 1340 */                 }
/* 1341 */ 
/* 1342 */                 mfp._parseMarkup(template, {
/* 1343 */                     title: _getTitle(item),
/* 1344 */                     img_replaceWith: item.img
/* 1345 */                 }, item);
/* 1346 */ 
/* 1347 */                 mfp.resizeImage();
/* 1348 */ 
/* 1349 */                 if (item.hasSize) {
/* 1350 */                     if (_imgInterval) clearInterval(_imgInterval);

/* magnific.js */

/* 1351 */ 
/* 1352 */                     if (item.loadError) {
/* 1353 */                         template.addClass('mfp-loading');
/* 1354 */                         mfp.updateStatus('error', imgSt.tError.replace('%url%', item.src));
/* 1355 */                     } else {
/* 1356 */                         template.removeClass('mfp-loading');
/* 1357 */                         mfp.updateStatus('ready');
/* 1358 */                     }
/* 1359 */                     return template;
/* 1360 */                 }
/* 1361 */ 
/* 1362 */                 mfp.updateStatus('loading');
/* 1363 */                 item.loading = true;
/* 1364 */ 
/* 1365 */                 if (!item.hasSize) {
/* 1366 */                     item.imgHidden = true;
/* 1367 */                     template.addClass('mfp-loading');
/* 1368 */                     mfp.findImageSize(item);
/* 1369 */                 }
/* 1370 */ 
/* 1371 */                 return template;
/* 1372 */             }
/* 1373 */         }
/* 1374 */     });
/* 1375 */ 
/* 1376 */ 
/* 1377 */ 
/* 1378 */     /*>>image*/
/* 1379 */ 
/* 1380 */     /*>>zoom*/
/* 1381 */     var hasMozTransform,
/* 1382 */         getHasMozTransform = function() {
/* 1383 */             if (hasMozTransform === undefined) {
/* 1384 */                 hasMozTransform = document.createElement('p').style.MozTransform !== undefined;
/* 1385 */             }
/* 1386 */             return hasMozTransform;
/* 1387 */         };
/* 1388 */ 
/* 1389 */     $.magnificPopup.registerModule('zoom', {
/* 1390 */ 
/* 1391 */         options: {
/* 1392 */             enabled: false,
/* 1393 */             easing: 'ease-in-out',
/* 1394 */             duration: 300,
/* 1395 */             opener: function(element) {
/* 1396 */                 return element.is('img') ? element : element.find('img');
/* 1397 */             }
/* 1398 */         },
/* 1399 */ 
/* 1400 */         proto: {

/* magnific.js */

/* 1401 */ 
/* 1402 */             initZoom: function() {
/* 1403 */                 var zoomSt = mfp.st.zoom,
/* 1404 */                     ns = '.zoom';
/* 1405 */ 
/* 1406 */                 if (!zoomSt.enabled || !mfp.supportsTransition) {
/* 1407 */                     return;
/* 1408 */                 }
/* 1409 */ 
/* 1410 */                 var duration = zoomSt.duration,
/* 1411 */                     getElToAnimate = function(image) {
/* 1412 */                         var newImg = image.clone().removeAttr('style').removeAttr('class').addClass('mfp-animated-image'),
/* 1413 */                             transition = 'all ' + (zoomSt.duration / 1000) + 's ' + zoomSt.easing,
/* 1414 */                             cssObj = {
/* 1415 */                                 position: 'fixed',
/* 1416 */                                 zIndex: 9999,
/* 1417 */                                 left: 0,
/* 1418 */                                 top: 0,
/* 1419 */                                 '-webkit-backface-visibility': 'hidden'
/* 1420 */                             },
/* 1421 */                             t = 'transition';
/* 1422 */ 
/* 1423 */                         cssObj['-webkit-' + t] = cssObj['-moz-' + t] = cssObj['-o-' + t] = cssObj[t] = transition;
/* 1424 */ 
/* 1425 */                         newImg.css(cssObj);
/* 1426 */                         return newImg;
/* 1427 */                     },
/* 1428 */                     showMainContent = function() {
/* 1429 */                         mfp.content.css('visibility', 'visible');
/* 1430 */                     },
/* 1431 */                     openTimeout,
/* 1432 */                     animatedImg;
/* 1433 */ 
/* 1434 */                 _mfpOn('BuildControls' + ns, function() {
/* 1435 */                     if (mfp._allowZoom()) {
/* 1436 */ 
/* 1437 */                         clearTimeout(openTimeout);
/* 1438 */                         mfp.content.css('visibility', 'hidden');
/* 1439 */ 
/* 1440 */                         // Basically, all code below does is clones existing image, puts in on top of the current one and animated it
/* 1441 */ 
/* 1442 */                         image = mfp._getItemToZoom();
/* 1443 */ 
/* 1444 */                         if (!image) {
/* 1445 */                             showMainContent();
/* 1446 */                             return;
/* 1447 */                         }
/* 1448 */ 
/* 1449 */                         animatedImg = getElToAnimate(image);
/* 1450 */ 

/* magnific.js */

/* 1451 */                         animatedImg.css(mfp._getOffset());
/* 1452 */ 
/* 1453 */                         mfp.wrap.append(animatedImg);
/* 1454 */ 
/* 1455 */                         openTimeout = setTimeout(function() {
/* 1456 */                             animatedImg.css(mfp._getOffset(true));
/* 1457 */                             openTimeout = setTimeout(function() {
/* 1458 */ 
/* 1459 */                                 showMainContent();
/* 1460 */ 
/* 1461 */                                 setTimeout(function() {
/* 1462 */                                     animatedImg.remove();
/* 1463 */                                     image = animatedImg = null;
/* 1464 */                                     _mfpTrigger('ZoomAnimationEnded');
/* 1465 */                                 }, 16); // avoid blink when switching images 
/* 1466 */ 
/* 1467 */                             }, duration); // this timeout equals animation duration
/* 1468 */ 
/* 1469 */                         }, 16); // by adding this timeout we avoid short glitch at the beginning of animation
/* 1470 */ 
/* 1471 */ 
/* 1472 */                         // Lots of timeouts...
/* 1473 */                     }
/* 1474 */                 });
/* 1475 */                 _mfpOn(BEFORE_CLOSE_EVENT + ns, function() {
/* 1476 */                     if (mfp._allowZoom()) {
/* 1477 */ 
/* 1478 */                         clearTimeout(openTimeout);
/* 1479 */ 
/* 1480 */                         mfp.st.removalDelay = duration;
/* 1481 */ 
/* 1482 */                         if (!image) {
/* 1483 */                             image = mfp._getItemToZoom();
/* 1484 */                             if (!image) {
/* 1485 */                                 return;
/* 1486 */                             }
/* 1487 */                             animatedImg = getElToAnimate(image);
/* 1488 */                         }
/* 1489 */ 
/* 1490 */ 
/* 1491 */                         animatedImg.css(mfp._getOffset(true));
/* 1492 */                         mfp.wrap.append(animatedImg);
/* 1493 */                         mfp.content.css('visibility', 'hidden');
/* 1494 */ 
/* 1495 */                         setTimeout(function() {
/* 1496 */                             animatedImg.css(mfp._getOffset());
/* 1497 */                         }, 16);
/* 1498 */                     }
/* 1499 */ 
/* 1500 */                 });

/* magnific.js */

/* 1501 */ 
/* 1502 */                 _mfpOn(CLOSE_EVENT + ns, function() {
/* 1503 */                     if (mfp._allowZoom()) {
/* 1504 */                         showMainContent();
/* 1505 */                         if (animatedImg) {
/* 1506 */                             animatedImg.remove();
/* 1507 */                         }
/* 1508 */                     }
/* 1509 */                 });
/* 1510 */             },
/* 1511 */ 
/* 1512 */             _allowZoom: function() {
/* 1513 */                 return mfp.currItem.type === 'image';
/* 1514 */             },
/* 1515 */ 
/* 1516 */             _getItemToZoom: function() {
/* 1517 */                 if (mfp.currItem.hasSize) {
/* 1518 */                     return mfp.currItem.img;
/* 1519 */                 } else {
/* 1520 */                     return false;
/* 1521 */                 }
/* 1522 */             },
/* 1523 */ 
/* 1524 */             // Get element postion relative to viewport
/* 1525 */             _getOffset: function(isLarge) {
/* 1526 */                 var el;
/* 1527 */                 if (isLarge) {
/* 1528 */                     el = mfp.currItem.img;
/* 1529 */                 } else {
/* 1530 */                     el = mfp.st.zoom.opener(mfp.currItem.el || mfp.currItem);
/* 1531 */                 }
/* 1532 */ 
/* 1533 */                 var offset = el.offset();
/* 1534 */                 var paddingTop = parseInt(el.css('padding-top'), 10);
/* 1535 */                 var paddingBottom = parseInt(el.css('padding-bottom'), 10);
/* 1536 */                 offset.top -= ($(window).scrollTop() - paddingTop);
/* 1537 */ 
/* 1538 */ 
/* 1539 */                 /*
/* 1540 *| 			
/* 1541 *| 			Animating left + top + width/height looks glitchy in Firefox, but perfect in Chrome. And vice-versa.
/* 1542 *| 
/* 1543 *| 			 */
/* 1544 */                 var obj = {
/* 1545 */                     width: el.width(),
/* 1546 */                     // fix Zepto height+padding issue
/* 1547 */                     height: (_isJQ ? el.innerHeight() : el[0].offsetHeight) - paddingBottom - paddingTop
/* 1548 */                 };
/* 1549 */ 
/* 1550 */                 // I hate to do this, but there is no another option

/* magnific.js */

/* 1551 */                 if (getHasMozTransform()) {
/* 1552 */                     obj['-moz-transform'] = obj['transform'] = 'translate(' + offset.left + 'px,' + offset.top + 'px)';
/* 1553 */                 } else {
/* 1554 */                     obj.left = offset.left;
/* 1555 */                     obj.top = offset.top;
/* 1556 */                 }
/* 1557 */                 return obj;
/* 1558 */             }
/* 1559 */ 
/* 1560 */         }
/* 1561 */     });
/* 1562 */ 
/* 1563 */ 
/* 1564 */ 
/* 1565 */     /*>>zoom*/
/* 1566 */ 
/* 1567 */     /*>>iframe*/
/* 1568 */ 
/* 1569 */     var IFRAME_NS = 'iframe',
/* 1570 */         _emptyPage = '//about:blank',
/* 1571 */ 
/* 1572 */         _fixIframeBugs = function(isShowing) {
/* 1573 */             if (mfp.currTemplate[IFRAME_NS]) {
/* 1574 */                 var el = mfp.currTemplate[IFRAME_NS].find('iframe');
/* 1575 */                 if (el.length) {
/* 1576 */                     // reset src after the popup is closed to avoid "video keeps playing after popup is closed" bug
/* 1577 */                     if (!isShowing) {
/* 1578 */                         el[0].src = _emptyPage;
/* 1579 */                     }
/* 1580 */ 
/* 1581 */                     // IE8 black screen bug fix
/* 1582 */                     if (mfp.isIE8) {
/* 1583 */                         el.css('display', isShowing ? 'block' : 'none');
/* 1584 */                     }
/* 1585 */                 }
/* 1586 */             }
/* 1587 */         };
/* 1588 */ 
/* 1589 */     $.magnificPopup.registerModule(IFRAME_NS, {
/* 1590 */ 
/* 1591 */         options: {
/* 1592 */             markup: '<div class="mfp-iframe-scaler">' +
/* 1593 */                 '<div class="mfp-close"></div>' +
/* 1594 */                 '<iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe>' +
/* 1595 */                 '</div>',
/* 1596 */ 
/* 1597 */             srcAction: 'iframe_src',
/* 1598 */ 
/* 1599 */             // we don't care and support only one default type of URL by default
/* 1600 */             patterns: {

/* magnific.js */

/* 1601 */                 youtube: {
/* 1602 */                     index: 'youtube.com',
/* 1603 */                     id: 'v=',
/* 1604 */                     src: '//www.youtube.com/embed/%id%?autoplay=1'
/* 1605 */                 },
/* 1606 */                 vimeo: {
/* 1607 */                     index: 'vimeo.com/',
/* 1608 */                     id: '/',
/* 1609 */                     src: '//player.vimeo.com/video/%id%?autoplay=1'
/* 1610 */                 },
/* 1611 */                 gmaps: {
/* 1612 */                     index: '//maps.google.',
/* 1613 */                     src: '%id%&output=embed'
/* 1614 */                 }
/* 1615 */             }
/* 1616 */         },
/* 1617 */ 
/* 1618 */         proto: {
/* 1619 */             initIframe: function() {
/* 1620 */                 mfp.types.push(IFRAME_NS);
/* 1621 */ 
/* 1622 */                 _mfpOn('BeforeChange', function(e, prevType, newType) {
/* 1623 */                     if (prevType !== newType) {
/* 1624 */                         if (prevType === IFRAME_NS) {
/* 1625 */                             _fixIframeBugs(); // iframe if removed
/* 1626 */                         } else if (newType === IFRAME_NS) {
/* 1627 */                             _fixIframeBugs(true); // iframe is showing
/* 1628 */                         }
/* 1629 */                     } // else {
/* 1630 */                     // iframe source is switched, don't do anything
/* 1631 */                     //}
/* 1632 */                 });
/* 1633 */ 
/* 1634 */                 _mfpOn(CLOSE_EVENT + '.' + IFRAME_NS, function() {
/* 1635 */                     _fixIframeBugs();
/* 1636 */                 });
/* 1637 */             },
/* 1638 */ 
/* 1639 */             getIframe: function(item, template) {
/* 1640 */                 var embedSrc = item.src;
/* 1641 */                 var iframeSt = mfp.st.iframe;
/* 1642 */ 
/* 1643 */                 $.each(iframeSt.patterns, function() {
/* 1644 */                     if (embedSrc.indexOf(this.index) > -1) {
/* 1645 */                         if (this.id) {
/* 1646 */                             if (typeof this.id === 'string') {
/* 1647 */                                 embedSrc = embedSrc.substr(embedSrc.lastIndexOf(this.id) + this.id.length, embedSrc.length);
/* 1648 */                             } else {
/* 1649 */                                 embedSrc = this.id.call(this, embedSrc);
/* 1650 */                             }

/* magnific.js */

/* 1651 */                         }
/* 1652 */                         embedSrc = this.src.replace('%id%', embedSrc);
/* 1653 */                         return false; // break;
/* 1654 */                     }
/* 1655 */                 });
/* 1656 */ 
/* 1657 */                 var dataObj = {};
/* 1658 */                 if (iframeSt.srcAction) {
/* 1659 */                     dataObj[iframeSt.srcAction] = embedSrc;
/* 1660 */                 }
/* 1661 */                 mfp._parseMarkup(template, dataObj, item);
/* 1662 */ 
/* 1663 */                 mfp.updateStatus('ready');
/* 1664 */ 
/* 1665 */                 return template;
/* 1666 */             }
/* 1667 */         }
/* 1668 */     });
/* 1669 */ 
/* 1670 */ 
/* 1671 */ 
/* 1672 */     /*>>iframe*/
/* 1673 */ 
/* 1674 */     /*>>gallery*/
/* 1675 */     /**
/* 1676 *|      * Get looped index depending on number of slides
/* 1677 *|      */
/* 1678 */     var _getLoopedId = function(index) {
/* 1679 */         var numSlides = mfp.items.length;
/* 1680 */         if (index > numSlides - 1) {
/* 1681 */             return index - numSlides;
/* 1682 */         } else if (index < 0) {
/* 1683 */             return numSlides + index;
/* 1684 */         }
/* 1685 */         return index;
/* 1686 */     },
/* 1687 */         _replaceCurrTotal = function(text, curr, total) {
/* 1688 */             return text.replace('%curr%', curr + 1).replace('%total%', total);
/* 1689 */         };
/* 1690 */ 
/* 1691 */     $.magnificPopup.registerModule('gallery', {
/* 1692 */ 
/* 1693 */         options: {
/* 1694 */             enabled: false,
/* 1695 */             arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
/* 1696 */             preload: [0, 2],
/* 1697 */             navigateByImgClick: true,
/* 1698 */             arrows: true,
/* 1699 */ 
/* 1700 */             tPrev: 'Previous (Left arrow key)',

/* magnific.js */

/* 1701 */             tNext: 'Next (Right arrow key)',
/* 1702 */             tCounter: '%curr% of %total%'
/* 1703 */         },
/* 1704 */ 
/* 1705 */         proto: {
/* 1706 */             initGallery: function() {
/* 1707 */ 
/* 1708 */                 var gSt = mfp.st.gallery,
/* 1709 */                     ns = '.mfp-gallery',
/* 1710 */                     supportsFastClick = Boolean($.fn.mfpFastClick);
/* 1711 */ 
/* 1712 */                 mfp.direction = true; // true - next, false - prev
/* 1713 */ 
/* 1714 */                 if (!gSt || !gSt.enabled) return false;
/* 1715 */ 
/* 1716 */                 _wrapClasses += ' mfp-gallery';
/* 1717 */ 
/* 1718 */                 _mfpOn(OPEN_EVENT + ns, function() {
/* 1719 */ 
/* 1720 */                     if (gSt.navigateByImgClick) {
/* 1721 */                         mfp.wrap.on('click' + ns, '.mfp-img', function() {
/* 1722 */                             if (mfp.items.length > 1) {
/* 1723 */                                 mfp.next();
/* 1724 */                                 return false;
/* 1725 */                             }
/* 1726 */                         });
/* 1727 */                     }
/* 1728 */ 
/* 1729 */                     _document.on('keydown' + ns, function(e) {
/* 1730 */                         if (e.keyCode === 37) {
/* 1731 */                             mfp.prev();
/* 1732 */                         } else if (e.keyCode === 39) {
/* 1733 */                             mfp.next();
/* 1734 */                         }
/* 1735 */                     });
/* 1736 */                 });
/* 1737 */ 
/* 1738 */                 _mfpOn('UpdateStatus' + ns, function(e, data) {
/* 1739 */                     if (data.text) {
/* 1740 */                         data.text = _replaceCurrTotal(data.text, mfp.currItem.index, mfp.items.length);
/* 1741 */                     }
/* 1742 */                 });
/* 1743 */ 
/* 1744 */                 _mfpOn(MARKUP_PARSE_EVENT + ns, function(e, element, values, item) {
/* 1745 */                     var l = mfp.items.length;
/* 1746 */                     values.counter = l > 1 ? _replaceCurrTotal(gSt.tCounter, item.index, l) : '';
/* 1747 */                 });
/* 1748 */ 
/* 1749 */                 _mfpOn('BuildControls' + ns, function() {
/* 1750 */                     if (mfp.items.length > 1 && gSt.arrows && !mfp.arrowLeft) {

/* magnific.js */

/* 1751 */                         var markup = gSt.arrowMarkup,
/* 1752 */                             arrowLeft = mfp.arrowLeft = $(markup.replace('%title%', gSt.tPrev).replace('%dir%', 'left')).addClass(PREVENT_CLOSE_CLASS),
/* 1753 */                             arrowRight = mfp.arrowRight = $(markup.replace('%title%', gSt.tNext).replace('%dir%', 'right')).addClass(PREVENT_CLOSE_CLASS);
/* 1754 */ 
/* 1755 */                         var eName = supportsFastClick ? 'mfpFastClick' : 'click';
/* 1756 */                         arrowLeft[eName](function() {
/* 1757 */                             mfp.prev();
/* 1758 */                         });
/* 1759 */                         arrowRight[eName](function() {
/* 1760 */                             mfp.next();
/* 1761 */                         });
/* 1762 */ 
/* 1763 */                         // Polyfill for :before and :after (adds elements with classes mfp-a and mfp-b)
/* 1764 */                         if (mfp.isIE7) {
/* 1765 */                             _getEl('b', arrowLeft[0], false, true);
/* 1766 */                             _getEl('a', arrowLeft[0], false, true);
/* 1767 */                             _getEl('b', arrowRight[0], false, true);
/* 1768 */                             _getEl('a', arrowRight[0], false, true);
/* 1769 */                         }
/* 1770 */ 
/* 1771 */                         mfp.container.append(arrowLeft.add(arrowRight));
/* 1772 */                     }
/* 1773 */                 });
/* 1774 */ 
/* 1775 */                 _mfpOn(CHANGE_EVENT + ns, function() {
/* 1776 */                     if (mfp._preloadTimeout) clearTimeout(mfp._preloadTimeout);
/* 1777 */ 
/* 1778 */                     mfp._preloadTimeout = setTimeout(function() {
/* 1779 */                         mfp.preloadNearbyImages();
/* 1780 */                         mfp._preloadTimeout = null;
/* 1781 */                     }, 16);
/* 1782 */                 });
/* 1783 */ 
/* 1784 */ 
/* 1785 */                 _mfpOn(CLOSE_EVENT + ns, function() {
/* 1786 */                     _document.off(ns);
/* 1787 */                     mfp.wrap.off('click' + ns);
/* 1788 */ 
/* 1789 */                     if (mfp.arrowLeft && supportsFastClick) {
/* 1790 */                         mfp.arrowLeft.add(mfp.arrowRight).destroyMfpFastClick();
/* 1791 */                     }
/* 1792 */                     mfp.arrowRight = mfp.arrowLeft = null;
/* 1793 */                 });
/* 1794 */ 
/* 1795 */             },
/* 1796 */             next: function() {
/* 1797 */                 mfp.direction = true;
/* 1798 */                 mfp.index = _getLoopedId(mfp.index + 1);
/* 1799 */                 mfp.updateItemHTML();
/* 1800 */             },

/* magnific.js */

/* 1801 */             prev: function() {
/* 1802 */                 mfp.direction = false;
/* 1803 */                 mfp.index = _getLoopedId(mfp.index - 1);
/* 1804 */                 mfp.updateItemHTML();
/* 1805 */             },
/* 1806 */             goTo: function(newIndex) {
/* 1807 */                 mfp.direction = (newIndex >= mfp.index);
/* 1808 */                 mfp.index = newIndex;
/* 1809 */                 mfp.updateItemHTML();
/* 1810 */             },
/* 1811 */             preloadNearbyImages: function() {
/* 1812 */                 var p = mfp.st.gallery.preload,
/* 1813 */                     preloadBefore = Math.min(p[0], mfp.items.length),
/* 1814 */                     preloadAfter = Math.min(p[1], mfp.items.length),
/* 1815 */                     i;
/* 1816 */ 
/* 1817 */                 for (i = 1; i <= (mfp.direction ? preloadAfter : preloadBefore); i++) {
/* 1818 */                     mfp._preloadItem(mfp.index + i);
/* 1819 */                 }
/* 1820 */                 for (i = 1; i <= (mfp.direction ? preloadBefore : preloadAfter); i++) {
/* 1821 */                     mfp._preloadItem(mfp.index - i);
/* 1822 */                 }
/* 1823 */             },
/* 1824 */             _preloadItem: function(index) {
/* 1825 */                 index = _getLoopedId(index);
/* 1826 */ 
/* 1827 */                 if (mfp.items[index].preloaded) {
/* 1828 */                     return;
/* 1829 */                 }
/* 1830 */ 
/* 1831 */                 var item = mfp.items[index];
/* 1832 */                 if (!item.parsed) {
/* 1833 */                     item = mfp.parseEl(index);
/* 1834 */                 }
/* 1835 */ 
/* 1836 */                 _mfpTrigger('LazyLoad', item);
/* 1837 */ 
/* 1838 */                 if (item.type === 'image') {
/* 1839 */                     item.img = $('<img class="mfp-img" />').on('load.mfploader', function() {
/* 1840 */                         item.hasSize = true;
/* 1841 */                     }).on('error.mfploader', function() {
/* 1842 */                         item.hasSize = true;
/* 1843 */                         item.loadError = true;
/* 1844 */                     }).attr('src', item.src);
/* 1845 */                 }
/* 1846 */ 
/* 1847 */ 
/* 1848 */                 item.preloaded = true;
/* 1849 */             }
/* 1850 */         }

/* magnific.js */

/* 1851 */     });
/* 1852 */ 
/* 1853 */     /*
/* 1854 *| Touch Support that might be implemented some day
/* 1855 *| 
/* 1856 *| addSwipeGesture: function() {
/* 1857 *| 	var startX,
/* 1858 *| 		moved,
/* 1859 *| 		multipleTouches;
/* 1860 *| 
/* 1861 *| 		return;
/* 1862 *| 
/* 1863 *| 	var namespace = '.mfp',
/* 1864 *| 		addEventNames = function(pref, down, move, up, cancel) {
/* 1865 *| 			mfp._tStart = pref + down + namespace;
/* 1866 *| 			mfp._tMove = pref + move + namespace;
/* 1867 *| 			mfp._tEnd = pref + up + namespace;
/* 1868 *| 			mfp._tCancel = pref + cancel + namespace;
/* 1869 *| 		};
/* 1870 *| 
/* 1871 *| 	if(window.navigator.msPointerEnabled) {
/* 1872 *| 		addEventNames('MSPointer', 'Down', 'Move', 'Up', 'Cancel');
/* 1873 *| 	} else if('ontouchstart' in window) {
/* 1874 *| 		addEventNames('touch', 'start', 'move', 'end', 'cancel');
/* 1875 *| 	} else {
/* 1876 *| 		return;
/* 1877 *| 	}
/* 1878 *| 	_window.on(mfp._tStart, function(e) {
/* 1879 *| 		var oE = e.originalEvent;
/* 1880 *| 		multipleTouches = moved = false;
/* 1881 *| 		startX = oE.pageX || oE.changedTouches[0].pageX;
/* 1882 *| 	}).on(mfp._tMove, function(e) {
/* 1883 *| 		if(e.originalEvent.touches.length > 1) {
/* 1884 *| 			multipleTouches = e.originalEvent.touches.length;
/* 1885 *| 		} else {
/* 1886 *| 			//e.preventDefault();
/* 1887 *| 			moved = true;
/* 1888 *| 		}
/* 1889 *| 	}).on(mfp._tEnd + ' ' + mfp._tCancel, function(e) {
/* 1890 *| 		if(moved && !multipleTouches) {
/* 1891 *| 			var oE = e.originalEvent,
/* 1892 *| 				diff = startX - (oE.pageX || oE.changedTouches[0].pageX);
/* 1893 *| 
/* 1894 *| 			if(diff > 20) {
/* 1895 *| 				mfp.next();
/* 1896 *| 			} else if(diff < -20) {
/* 1897 *| 				mfp.prev();
/* 1898 *| 			}
/* 1899 *| 		}
/* 1900 *| 	});

/* magnific.js */

/* 1901 *| },
/* 1902 *| */
/* 1903 */ 
/* 1904 */ 
/* 1905 */     /*>>gallery*/
/* 1906 */ 
/* 1907 */     /*>>retina*/
/* 1908 */ 
/* 1909 */     var RETINA_NS = 'retina';
/* 1910 */ 
/* 1911 */     $.magnificPopup.registerModule(RETINA_NS, {
/* 1912 */         options: {
/* 1913 */             replaceSrc: function(item) {
/* 1914 */                 return item.src.replace(/\.\w+$/, function(m) {
/* 1915 */                     return '@2x' + m;
/* 1916 */                 });
/* 1917 */             },
/* 1918 */             ratio: 1 // Function or number.  Set to 1 to disable.
/* 1919 */         },
/* 1920 */         proto: {
/* 1921 */             initRetina: function() {
/* 1922 */                 if (window.devicePixelRatio > 1) {
/* 1923 */ 
/* 1924 */                     var st = mfp.st.retina,
/* 1925 */                         ratio = st.ratio;
/* 1926 */ 
/* 1927 */                     ratio = !isNaN(ratio) ? ratio : ratio();
/* 1928 */ 
/* 1929 */                     if (ratio > 1) {
/* 1930 */                         _mfpOn('ImageHasSize' + '.' + RETINA_NS, function(e, item) {
/* 1931 */                             item.img.css({
/* 1932 */                                 'max-width': item.img[0].naturalWidth / ratio,
/* 1933 */                                 'width': '100%'
/* 1934 */                             });
/* 1935 */                         });
/* 1936 */                         _mfpOn('ElementParse' + '.' + RETINA_NS, function(e, item) {
/* 1937 */                             item.src = st.replaceSrc(item, ratio);
/* 1938 */                         });
/* 1939 */                     }
/* 1940 */                 }
/* 1941 */ 
/* 1942 */             }
/* 1943 */         }
/* 1944 */     });
/* 1945 */ 
/* 1946 */     /*>>retina*/
/* 1947 */ 
/* 1948 */     /*>>fastclick*/
/* 1949 */     /**
/* 1950 *|      * FastClick event implementation. (removes 300ms delay on touch devices)

/* magnific.js */

/* 1951 *|      * Based on https://developers.google.com/mobile/articles/fast_buttons
/* 1952 *|      *
/* 1953 *|      * You may use it outside the Magnific Popup by calling just:
/* 1954 *|      *
/* 1955 *|      * $('.your-el').mfpFastClick(function() {
/* 1956 *|      *     console.log('Clicked!');
/* 1957 *|      * });
/* 1958 *|      *
/* 1959 *|      * To unbind:
/* 1960 *|      * $('.your-el').destroyMfpFastClick();
/* 1961 *|      *
/* 1962 *|      *
/* 1963 *|      * Note that it's a very basic and simple implementation, it blocks ghost click on the same element where it was bound.
/* 1964 *|      * If you need something more advanced, use plugin by FT Labs https://github.com/ftlabs/fastclick
/* 1965 *|      *
/* 1966 *|      */
/* 1967 */ 
/* 1968 */     (function() {
/* 1969 */         var ghostClickDelay = 1000,
/* 1970 */             supportsTouch = 'ontouchstart' in window,
/* 1971 */             unbindTouchMove = function() {
/* 1972 */                 _window.off('touchmove' + ns + ' touchend' + ns);
/* 1973 */             },
/* 1974 */             eName = 'mfpFastClick',
/* 1975 */             ns = '.' + eName;
/* 1976 */ 
/* 1977 */ 
/* 1978 */         // As Zepto.js doesn't have an easy way to add custom events (like jQuery), so we implement it in this way
/* 1979 */         $.fn.mfpFastClick = function(callback) {
/* 1980 */ 
/* 1981 */             return $(this).each(function() {
/* 1982 */ 
/* 1983 */                 var elem = $(this),
/* 1984 */                     lock;
/* 1985 */ 
/* 1986 */                 if (supportsTouch) {
/* 1987 */ 
/* 1988 */                     var timeout,
/* 1989 */                         startX,
/* 1990 */                         startY,
/* 1991 */                         pointerMoved,
/* 1992 */                         point,
/* 1993 */                         numPointers;
/* 1994 */ 
/* 1995 */                     elem.on('touchstart' + ns, function(e) {
/* 1996 */                         pointerMoved = false;
/* 1997 */                         numPointers = 1;
/* 1998 */ 
/* 1999 */                         point = e.originalEvent ? e.originalEvent.touches[0] : e.touches[0];
/* 2000 */                         startX = point.clientX;

/* magnific.js */

/* 2001 */                         startY = point.clientY;
/* 2002 */ 
/* 2003 */                         _window.on('touchmove' + ns, function(e) {
/* 2004 */                             point = e.originalEvent ? e.originalEvent.touches : e.touches;
/* 2005 */                             numPointers = point.length;
/* 2006 */                             point = point[0];
/* 2007 */                             if (Math.abs(point.clientX - startX) > 10 ||
/* 2008 */                                 Math.abs(point.clientY - startY) > 10) {
/* 2009 */                                 pointerMoved = true;
/* 2010 */                                 unbindTouchMove();
/* 2011 */                             }
/* 2012 */                         }).on('touchend' + ns, function(e) {
/* 2013 */                             unbindTouchMove();
/* 2014 */                             if (pointerMoved || numPointers > 1) {
/* 2015 */                                 return;
/* 2016 */                             }
/* 2017 */                             lock = true;
/* 2018 */                             e.preventDefault();
/* 2019 */                             clearTimeout(timeout);
/* 2020 */                             timeout = setTimeout(function() {
/* 2021 */                                 lock = false;
/* 2022 */                             }, ghostClickDelay);
/* 2023 */                             callback();
/* 2024 */                         });
/* 2025 */                     });
/* 2026 */ 
/* 2027 */                 }
/* 2028 */ 
/* 2029 */                 elem.on('click' + ns, function() {
/* 2030 */                     if (!lock) {
/* 2031 */                         callback();
/* 2032 */                     }
/* 2033 */                 });
/* 2034 */             });
/* 2035 */         };
/* 2036 */ 
/* 2037 */         $.fn.destroyMfpFastClick = function() {
/* 2038 */             $(this).off('touchstart' + ns + ' click' + ns);
/* 2039 */             if (supportsTouch) _window.off('touchmove' + ns + ' touchend' + ns);
/* 2040 */         };
/* 2041 */     })();
/* 2042 */ 
/* 2043 */     /*>>fastclick*/
/* 2044 */ })(window.jQuery || window.Zepto);
