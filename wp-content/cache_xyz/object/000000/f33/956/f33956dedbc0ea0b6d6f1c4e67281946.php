廡ｪW<?php exit; ?>a:1:{s:7:"content";a:4:{s:5:"child";a:1:{s:0:"";a:1:{s:3:"rss";a:1:{i:0;a:6:{s:4:"data";s:3:"


";s:7:"attribs";a:1:{s:0:"";a:1:{s:7:"version";s:3:"2.0";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:1:{s:7:"channel";a:1:{i:0;a:6:{s:4:"data";s:44:"











































";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:40:"Your African Safari Planning your safari";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:36:"http://www.yourafricansafari.com/rss";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:198:"What to know while planning your ideal African safari, from what vaccinations you need to have prior to leaving to whether or not you need to get a visa and what sort of camera lens you might want. ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"language";a:1:{i:0;a:5:{s:4:"data";s:2:"en";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 09 Aug 2016 09:10:20 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"item";a:36:{i:0;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:38:"Tips for a successful safari with kids";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:80:"http://www.yourafricansafari.com/articles/tips-for-a-successful-safari-with-kids";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:401:"Going on safari with kids: Part I

We recently traveled to Tanzania for a safari and side trip to Zanzibar. Being a close family we naturally planned to take our boys with us without a second thought. Traveling with kids can sometimes be a challenge, and traveling with three boys ages, 14, 12 and 8 is often unpredictable. As we planned the big trip we thought about what we would encounter along ...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:80:"http://www.yourafricansafari.com/articles/tips-for-a-successful-safari-with-kids";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 17 Dec 2015 19:33:40 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:3409:"Going on safari with kids: Part I

We recently traveled to Tanzania for a safari and side trip to Zanzibar. Being a close family we naturally planned to take our boys with us without a second thought. Traveling with kids can sometimes be a challenge, and traveling with three boys ages, 14, 12 and 8 is often unpredictable. As we planned the big trip we thought about what we would encounter along the way and what we could do to mitigate any challenges to make the experience a positive one for us all. We learned along the way and ultimately it was one of the best experiences we have had together.



Choose a good airline

The most daunting part of the trip we thought would be the flight. Tanzania is a very long way from the East Coast of the United States. The flight would take us from Boston to Istanbul (10 hours), then Istanbul to Arusha (8 hours). Add to that the travel time to and from airports, as well as the layover times and we would end up traveling for 23 hours.



Our perceived negative turned out to be not all that bad. Turkish Airlines was really great. Not only did they have nice little gifts for the kids to play with but, with each seat having its own entertainment system, each child had dozens of shows to watch between naps. Turkish Airlines is known for their outstanding cuisine and their food service every few hours really broke up the monotony of the flight. Despite the uneventful, albeit long flights, our heads on a pillow in an actual bed at our lodge was very welcomed.

Make the long days in the safari vehicle fun!

As there is no walking about without an armed guard, safaris in Tanzaniaﾃ｢ﾂ�ﾂ冱 National Parks means long days in a safari vehicle. We found that the safari vehicle, a 4x4 with a pop-up roof, was big enough to allow for moving about inside and allowing for legs to get stretched as needed. To keep the boys interested and active in the comfortable but confined space, our days out for game drives often turned into scavenger hunts. My husband put up a 500 shilling reward for spotting certain animals during the day. Often times the kids could double their winnings with a little wagering. The kids loved it!



During down times the boys would chat with our guide Salehe, to learn about his life, family and to pick up a few words in Swahili. To make our little game more interesting they could sometimes earn a bit more if they could find an animal and use its correct Swahili name. These games worked well for days until a conversation at our lodge one night revealed that 500 shillings was about 25 cents in the US. While our little deception was found out, the joy continued when we arrived at a markets in Karatu and the boys found they could use their winnings for a special trinket as a memento of their journey.



It is also advisable when out on game drives not far from camp, to return for a hot lunch rather than taking a boxed lunch with you. This breaks up the day and helps keep the kids from getting too tired with long days. In northern Serengeti we would start our day early, heading out immediately after breakfast and returning after 4-5 hours. Returning to camp at this time gave us all a break. It was also the time we used to relax and take our daily shower before heading back out for a late afternoon drive.

Stay tuned for the second half of this article, where I'll cover the remaining tips on taking kids on safari.ﾃつ�
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2016-07-18T17:47:36+01:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:1;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:41:"How to have a successful safari with kids";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:83:"http://www.yourafricansafari.com/articles/how-to-have-a-successful-safari-with-kids";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:400:"In August 2015, my husband and I took our three boys on their, and our, first safari. We were nervous about many things, such as the long flights and the fact that they'd be in a safari vehicle for long spells. In the end, our safari turned out to be one of our best family vacations yet. Here are some things that helped our kids really enjoy their Tanzania safari.


Choose camps/lodges with ent...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:83:"http://www.yourafricansafari.com/articles/how-to-have-a-successful-safari-with-kids";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 04 Mar 2016 23:01:56 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:5129:"In August 2015, my husband and I took our three boys on their, and our, first safari. We were nervous about many things, such as the long flights and the fact that they'd be in a safari vehicle for long spells. In the end, our safari turned out to be one of our best family vacations yet. Here are some things that helped our kids really enjoy their Tanzania safari.


Choose camps/lodges with entertainment or kid activities

We thought we would have problems keeping the boys occupied and entertained at camp. We found that this concern was unfounded. At the ﾃつ�Lemala Mara Tented Camp ﾃつ�the staff was outstanding. Our tent attendant, Bruno, was fantastic with the kids. He immediately took them off to play soccer, showed them how to shoot a bow and arrow, and gave them a Lemala backpack filled with bedtime stories and coloring books. The lounge tent had playing cards and games, and even a movie on a laptop for them to watch. There is something pretty special about watchingﾃつ� The Lion Kingﾃつ� in the heart of the Serengeti ! It gave us great peace of mind knowing that they, while never out of our sight, were entertained and looked after. This allowed us to enjoy our sundowners by the fire. Chatting with other adults from around the world without a care was relaxing.



Another highlight was our stay atﾃつ� Rhotia Valley Tented Camp ﾃつ�in the Karatu area. The boys loved having the opportunity to splash about, releasing pent up energy and cooling off in the pool after a long day driving in Ngorongoro Crater. This lodge is quite lovely as well. The views from the deck overlooking the valley are breathtaking. The tents are built around a beautiful garden filled with herbs and vegetables that the chef uses to prepare dinner. The boys used it as a maze to play around. The lodge is associated with the Rhotia Valley Childrenﾃ｢ﾂ�ﾂ冱 Home located on the hilltop opposite the lodge. Before dinner we had the opportunity to visit the home and meet the 35 kids that lived there. Our boys got to run around and show the kids how to use the Frisbees we brought as gifts.

Make theﾃつ�safari educational for youngsters

Traveling on extended trips with younger kids sometimes interferes with the school responsibilities, so we try to make sure to incorporate some educational aspects into the trips. Often times with the boys not realizing they are learning! These educational aspects donﾃ｢ﾂ�ﾂ冲 have to be boring; the more interactive the better. We planned a few stops on our safari. These stops were outside the national parks so the boys got to move about a bit more. We included visits to Olduvai Gorge, a cultural visit with the Hadzabe Bushmen near Lake Eyasi, a Masai boma visit outside Tarangire National Park , and learned about the slave trade and the Muslim culture on Zanzibar.



For the boys, the highlight would have to be visiting with the Hadzabe. We got the opportunity to trek through the bush and watch as they hunted and climbed baobab trees to collect fruit. We were all amazed to see how effortlessly they built a fire without matches, and were able to prepare, cook and feed us samples of the small dik dik antelope they caught. They truly live entirely off the land and our Boyﾃつ�Scouts from suburbia learned a few things to share with their friends at home.

Allowﾃつ�kids to enjoy the safari adventure

One of the best parts of traveling with kids is their unending sense of adventure and wonder. Everything is new and interesting to them. Let them ask question and when it is safe, let them explore. Our safari visited six different parks/regions in northern Tanzania. Each location was dramatically different from the other. Each place offered delights and often the travel between locations was a great interlude. They enjoyed climbing on the rock in search of lizards at Naabi Hill Gate just as much as they enjoys watching a lion stalk a zebra inside Ngorongoro Crater.

We gave our boys as much independence as we safely could. They slept in their own tent each night. Despite each camp having an armed guard at night, we were a bit apprehensive that they would wonder out if they slept alone. The first camp alleviated that concern as the staff was able to impart on them the importance of staying in. Their first encounter with a hyena on the way to the bathroom at dinner one night helped reinforce this warning!

The adventure continued in Stone Town in Zanzibar. Finding ourselves suddenly in the middle of a busy city after 12 days on safari was a bit of a shock. The boys learn quickly that we all had to rise quite early with the call to prayer coming from multiple mosques nearby. With only a map from our hotel, the boys successfully guided us from one site to the next through the narrow alleys lined with shops and homes in the historic heart of the city. It was an adventure the boys would not be able to find in the United States.

We have traveled quite a bit with our boys and each trip has its ups and downs. With each trip, we gain more insight into how to have a positive experience to ensure we have great memories as a family. Onward to our next adventure!

";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2016-07-18T17:42:12+01:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:2;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:23:"Knowing me, knowing you";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:64:"http://www.yourafricansafari.com/articles/knowing-me-knowing-you";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:413:"Planning a safari with a physical limitationﾃつ�

Who knows best what you can and cannot do?ﾃつ� It should be you.ﾃつ� But, can you be objective to your agent if your heart is set on something?ﾃつ� So how do you tackle this subject with a travel agent?ﾃつ� There is no easy answer.ﾃつ�They want your business but, at the same time, if you have used them before they will want to ensure repeat business and not ...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:64:"http://www.yourafricansafari.com/articles/knowing-me-knowing-you";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 01 Mar 2016 22:39:30 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:2713:"Planning a safari with a physical limitationﾃつ�

Who knows best what you can and cannot do?ﾃつ� It should be you.ﾃつ� But, can you be objective to your agent if your heart is set on something?ﾃつ� So how do you tackle this subject with a travel agent?ﾃつ� There is no easy answer.ﾃつ�They want your business but, at the same time, if you have used them before they will want to ensure repeat business and not advise incorrectly.

You, too, can see the lemurs in Madagascar

I visitedﾃつ� Madagascar ﾃつ�in September 2011, with no reference that I recall of relaying my physical condition to my agent.ﾃつ� Whilst theﾃつ� Andasibe-Mantadia National Park ﾃつ�terrain was challenging the guide accommodated my disability (the pace at which we walked and inclines we attempted) and to my delight we found an indri family.ﾃつ�



However, at the Ranomafana National Park, I soon told my guide that I had had enough.ﾃつ� Why?ﾃつ� The lemurs here were on the move, and quicker than I could move.ﾃつ�I was slipping and sliding uncomfortably through being dragged by a much abler and adept guide and my lack of required mobility was hindering my travel companionﾃ｢ﾂ�ﾂ冱 sightings.

You, too, can track mountain gorrilas

I went gorilla trekking in Bwindi Impenetrable Forest in November 2015 and re-reading the advisory blurb in January 2016 ﾃ｢ﾂ�ﾂ� Only those who are 100% fit and are capable of walking long distances at high altitude over slippery, muddy terrain should attempt gorilla tracking - really?ﾃつ� Where is the nearest madhouse?ﾃつ�



My agent was told my condition and advised that he had relayed it to the lodge manager and he felt that I could manage gorilla tracking, with the sedan chair option (locally called the stretcher ) available (at a price) should I decide against using my own legs.ﾃつ� It was the hardest thing I have physically achieved. The return journey was farﾃつ�harder than reaching the gorillas andﾃつ�became almost an out of body experience ﾃ｢ﾂ�ﾂ� thus the mention of an asylum. But, I did it and am so glad I did not back down when others doubted my ability to achieve this goal.

Would I have continued with the gorilla tracking if I had been with someone?ﾃつ� I donﾃ｢ﾂ�ﾂ冲 know.ﾃつ� The other seven who tracked with me said afterwards that they were pleased that we tracked at my pace.

I am not providing answers ﾃ｢ﾂ�ﾂｦ but hopefully thoughts that only YOU can answer given all the information that the internet now provides.

Bottom line ﾃ｢ﾂ�ﾂ� know yourself.ﾃつ� What achieving something is worth to you.ﾃつ� How much you want it.ﾃつ� Only you know.ﾃつ� And base your questions on what challenges you physically day-to-day.

Next time ﾃ｢ﾂ�ﾂ� help asked for is generally given.
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2016-06-08T01:07:16+01:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:3;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:70:"Why South Africa is the ideal honeymoon destination for active couples";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:112:"http://www.yourafricansafari.com/articles/why-south-africa-is-the-ideal-honeymoon-destination-for-active-couples";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:405:"Choosing South Africa for yourﾂ�honeymoon

Are you an active couple who's getting married this year and in need of that perfect honeymoon destination? As an active Colorado couple, we definitely wanted something special for our summer honeymoon last year.ﾂ�My husband and I contemplated a few locations for our honeymoon in July 2015, includingﾂ�South America andﾂ�Australia, but ultimately decided on ...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:112:"http://www.yourafricansafari.com/articles/why-south-africa-is-the-ideal-honeymoon-destination-for-active-couples";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 25 Jan 2016 03:44:29 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:4367:"Choosing South Africa for yourﾂ�honeymoon

Are you an active couple who's getting married this year and in need of that perfect honeymoon destination? As an active Colorado couple, we definitely wanted something special for our summer honeymoon last year.ﾂ�My husband and I contemplated a few locations for our honeymoon in July 2015, includingﾂ�South America andﾂ�Australia, but ultimately decided on South Africa .ﾂ�

Our trip would come completeﾂ�with a trip up to Zambia / Zimbabwe to see Victoria Falls ,ﾂ�and we are so glad we opted for South Africa! While planning our honeymoon, we were also planning a wedding and were very busy with our jobs, so after a few weeks of trying to figure out an itinerary on our own, we decided to enlist the help of a travel agent who specializesﾂ�in African travel (Jenny with Extraordinary Journeys ).ﾂ�



Researching your honeymoon activities

Before contacting her, we had a list of places we definitely wanted to see within South Africa.ﾂ�Although our travel agent recommended we do the safari first (because we would be jetlagged and getting up early anyway), we wanted a few days to decompress after the wedding and theﾂ�30ﾂ�hours of flying.



Shark diving was high on our list of to-dosﾂ�(I am obsessed with great white sharks), so we decided to spend the first two nights of our honeymoon at a bed and breakfast in Hermanus, which is right on the coast and absolutely beautiful (and RELAXING). While our travel agent booked all of our hotels and intercontinental flights in SA, we selected most of the activities ourselves after looking on a variety of reference sites, which I highly recommend. Doing so helped allowed us to not only save money, but also find some activities that travel agents wouldn't know about. For instance, we found a small, family-owned companyﾂ�that does aerial whale watching tours! This was such a cool way to not only see the Southern Right whales and their calves (Julyﾂ�is prime season for whale watching), and it also gave us an incredible view of the area and coast.ﾂ�

Our South Africa honeymoon itinerary

After Hermanus, we went to Cape Town where we stayed at a great hotel recommended by our travel agent, the Four Rosmead. It was a beautiful bed and breakfast, right on the foothills of Table Mountain (prime location), that pulled out all of the stops for our honeymoon suite with wine, chocolates, etc. We planned all of our Cape Town activities ourselves and researched the best restaurants (and made reservations beforehand). We loved Cape Town and were glad we had a full five days and four nights to be there.ﾂ�



Next we went to Franschhoek in SA's wine region where we stayed at another beautiful hotel, Le Quartier Francais. They also gave us a bottle of wine as well as a beautiful honeymoon suite. Make sure to make a reservation at the Tasting Room! #1 rated restaurant in South Africa and an absolute treat. We booked our own wine tour (based on reviews) which saved us money and allowed us to find the most reputable company.ﾂ�



After the winelands, we flew to Zambia for Victoria Falls, where we stayed at my favorite hotel of the trip: The Royal Livingstone. Our travel agent helped us book one excursion to Livingstone Island, butﾂ�we planned all of our other excursions ourselves. Please see my review on Victoria Falls for more details!ﾂ�



Our trip ended with our safari at Motswari Private Game Reserve, a private game reserve that is part of the Greater Kruger National Park Conservancyﾂ�- another recommendation from our travel agent. We had an amazing time and I was thoroughly impressed with the recommendation our travel agent made aboutﾂ�this safari group.ﾂ�



All in all, I was really happy we had a travel agent to help us book hotels, our intercontinental fights, ourﾂ�ground transfersﾂ�(i.e. car service from Cape Town airport to Hermanus, Hermanus to Cape Town; Cape Town toﾂ�Franschhoek) and to give us recommendations on what to pack, etc. However, I loved that we also took time to research the destinations where we were going to make sure we could select what activities we most wanted to do, places we wanted to eat, and places we wanted to see. ﾂ�Even if you are using a travel agent to book your travel, I think it makes your travel experience more personal (and gets you more excited) if you take an active part in the planning.ﾂ�
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2016-03-30T16:13:53+01:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:4;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:29:"Taking a disability on safari";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:71:"http://www.yourafricansafari.com/articles/taking-a-disability-on-safari";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:410:"An African safari for those with limitations

This is a huge subject.ﾃつ�Not meant to be as an inane opener as it reads.ﾃつ�To my mind not aided by internet shopping.ﾃつ� In the old days, note not necessarily good old days, you and your travel agent met across a deskﾃ｢ﾂ�ﾂｦbut even then had they actually experienced the sort of safari holiday you were after first hand

Introductions.ﾃつ�I am Neil Andrews....";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:71:"http://www.yourafricansafari.com/articles/taking-a-disability-on-safari";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 25 Jan 2016 21:30:29 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:2574:"An African safari for those with limitations

This is a huge subject.ﾃつ�Not meant to be as an inane opener as it reads.ﾃつ�To my mind not aided by internet shopping.ﾃつ� In the old days, note not necessarily good old days, you and your travel agent met across a deskﾃ｢ﾂ�ﾂｦbut even then had they actually experienced the sort of safari holiday you were after first hand

Introductions.ﾃつ�I am Neil Andrews.ﾃつ�Arthrogryposis Multiplex Congenita is my conditionﾃ｢ﾂ�ﾂｦdisability.ﾃつ� Sixty-one years old with a lot of attitude, most of it good, with a smile. And a naughty sense of humour.ﾃつ�Had to be British with the word naughty and the erroneous u!

It was 1996 when I went on my first African safari holiday and have been back at least once every year since, with two already booked for 2016.ﾃつ� If you are mobile, that is can generally move without or with limited assistance, and neither visually nor orally impaired then, from my personal experience, your disability is of marginal consideration for a traditional ﾃ｢ﾂ�ﾂ話um on seatﾃ｢ﾂ�ﾂ� safari.

Vehicle seating on safari

The toughest element of the day will be climbing aboard and disembarking the safari vehicle.ﾃつ� If a 4x4 the camp will often have steps as well as personnel on hand to help you.ﾃつ� Brilliant.ﾃつ� Except at one camp where the last row of seats of the vehicle was particularly high off the ground the steps did not travel with us, so therefore descending for pit stops/sundowners was not so easyﾃ｢ﾂ�ﾂｦ Kasanka in Zambia a case in pointﾃ｢ﾂ�ﾂｦand I had my sundowner on board.

Boarding and disembarking boats and mokoros (i.e. canoes) can be challengingﾃつ�for, apart from perhaps simply wishing to keep oneﾃ｢ﾂ�ﾂ冱 feet dry, the stepping in to the vessel may not be that easy.ﾃつ�Experience has taught me to ask for assistance and explain what the issue or level of disability restricts you from doing.

Know where your cabin/tent/room is in relation to the main building

Not all safari camp sites are on the flat therefore on of the keys things is to ensure that your agent has requested that your tent/room/chalet is the nearest to the boma/main building ﾃ｢ﾂ�ﾂｦ if that means the shortest distance on the flat (not always the case). In daylight ensure that you know of small rises, rough edges that even with a torch may not be so visible after dark.

Hopefully those of us with a disability have extra common sense.ﾃつ�Hopefully.

Next time ﾃ｢ﾂ�ﾂ� how do you and your internet agent judge your physical capabilities?

Have your own recommendations / tips for travelers? Please emails us !
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2016-03-24T16:22:09+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:5;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:20:"Yoga safari retreats";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:62:"http://www.yourafricansafari.com/articles/yoga-safari-retreats";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:405:"Yoga retreats: not just for beaches anymoreﾂ�

Yoga retreats are not a novelty. For years, people have been escaping winter's gray skies and frosty conditionsﾂ�for the opportunity to get their chakra on in a warm,ﾂ�focused and supportive environment. But, normally, when one thinks of a yoga retreat, one thinks of destinations like Costa Rica, Greece, Indonesia, Turkey and Thailand. But,ﾂ�African cou...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:62:"http://www.yourafricansafari.com/articles/yoga-safari-retreats";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 24 Dec 2015 20:26:35 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:2851:"Yoga retreats: not just for beaches anymoreﾂ�

Yoga retreats are not a novelty. For years, people have been escaping winter's gray skies and frosty conditionsﾂ�for the opportunity to get their chakra on in a warm,ﾂ�focused and supportive environment. But, normally, when one thinks of a yoga retreat, one thinks of destinations like Costa Rica, Greece, Indonesia, Turkey and Thailand. But,ﾂ�African countries are appearing on the yoga retreat radar, and yoga safari retreats are being offered by more and more tour companies and safari lodges.

Yoga safaris: balancing the inner and outer self

If yoga has always been something you've been wanting to dip your toes into then a yoga ﾂ�retreat is for you.ﾂ�The aim is to give guestsﾂ�the confidence to evolve and become the best version of themselves. Guestsﾂ�will find themselvesﾂ�in a stress-free, judgement-free space; perfect for the beginner yogi! Yoga retreatsﾂ�encourage peopleﾂ�to move freely with a clear mind and heart, while building confidence, courage and happiness! One theﾂ�best way to free yourself and to find that inner connection is in a natural setting in paradise.

Yoga safari retreat with Campi ya Kanzi

One company that is already offering yoga safaris is Campi ya Kanzi , a luxury ecolodge in Kenya.ﾂ�Based in the middle of the Chyulu Hills, surrounded by 280,000 acres of pristine wilderness at your fingertips, Campi ya Kanzi has created a space where youﾂ�have the confidence to evolve and become a transformed version of yourself. You will find yourself in a stress-free, judgement free space perfect for the beginner yogi. This retreat is focused on strengthening the relationship you have with your inner yourself and moving with ease to become requainted with all the little nuances within your body through softness and breath...

ﾃ｢ﾂ�ﾂ�

Their intention is for you to move freely with a clear mind and heart, build confidence, courage and happiness. They believe thatﾂ�the best way to free yourself and to find that inner connection is in a natural setting in paradise.

Campi ya Kanzi is offering aﾂ�7 day/6 night yoga safari in one of the most beautiful places on the earth: Chyulu Hills. Become immersed in the beauty of amazing wildlife and give yourself a chance to reconnect, transform and be inspired daily by the asanas and meditation. Your safari activities include a daily sunrise yoga practice of two hours, or at lunchtime,ﾂ�and game drives/hikes with a professional wildlife Maasai guide/tracker. Safari costs include:ﾂ�daily practices, accommodation on a fully inclusive basis, all meals, snacks, drinks, laundry, all safari activities and service of a professional guide.ﾂ�Internal private flights, international flights, travel insurance, visa costs, personal items and gratuities are not included.ﾂ�

For more info, please contact Campi ya Kanzi .ﾂ�
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2016-02-02T18:59:10+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:6;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:33:"Where to stay in Arusha, Tanzania";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:74:"http://www.yourafricansafari.com/articles/where-to-stay-in-arusha-tanzania";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:406:"The majority of people visiting the Serengeti or Ngorongoro Crater will pass through Kilimanjaro International Airport (JRO). JRO is about a 45 minute drive to Arusha, where most northern Tanzanian safaris start. Since many international flights land in the evening, or sometimes in the very early morning, itﾃ｢ﾂ�ﾂ冱 common for tour operators to book one night in Arusha before guests embark on their mu...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:74:"http://www.yourafricansafari.com/articles/where-to-stay-in-arusha-tanzania";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 12 Mar 2015 19:45:10 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:3279:"The majority of people visiting the Serengeti or Ngorongoro Crater will pass through Kilimanjaro International Airport (JRO). JRO is about a 45 minute drive to Arusha, where most northern Tanzanian safaris start. Since many international flights land in the evening, or sometimes in the very early morning, itﾃ｢ﾂ�ﾂ冱 common for tour operators to book one night in Arusha before guests embark on their much-anticipated safari. Arushaﾃ｢ﾂ�ﾂ冱 lodging options have grown tremendously in the past 10-20 years, and now travelers can find options ranging from basic to opulent, and everything in between. As everyoneﾃ｢ﾂ�ﾂ冱 tastes vary, this article is meant only to serve as a guide for those who may be undecided on how to plan their pre- or post-safari sleeps.

Staying in Arusha before a safari

With many international flights to JRO landing at unsociable hours, most visitors donﾃ｢ﾂ�ﾂ冲 get to see much of Arusha when they arrive. If your safari is slated to start the next day, it makes sense to stay either in town or, if youﾃ｢ﾂ�ﾂ决e taking a domestic flight the next day, near the Arusha airport. The Arusha Coffee Lodge has an idyllic setting in a coffee plantation, with Mt. Meru as a backdrop. It is also within minutes of the Arusha Airport. It is, however, not a budget option and may not be the best use of the fundsﾃつ�if youﾃ｢ﾂ�ﾂ决e only staying one night. Arumeru Lodgeﾃつ� is another option that is slightly lower in budget but also very nice and quiet. Itﾃ｢ﾂ�ﾂ冱 also within minutes of Arusha airport. In town are numerous options, too numerous to list them all. But, places such as African Tulip , Kibo Palace and at the end of the budget spectrum, The Outpost , offer clean, quiet places to sleep. The Outpost is no-frills, with mosquito tents and spartan rooms, but itﾃ｢ﾂ�ﾂ冱 really all one needs for one nightﾃ｢ﾂ�ﾂ冱 sleep.

Staying in Arusha after a safari

A safari is an exhilarating but also an exhausting experience. Pre-dawn starts and post-drive nights serenaded by hyenas, hippos and lions (oh my), can take their toll. If you find yourself with at least a full day to kill in Arusha, then you are in luck. Arusha offers ample lodges that almost feel like retreats. If you wish to stay in town, the now is the time to try Arusha Coffee Lodge. But, if you wish to get away from the hustle and bustle of a modern city, and are ok with a 30-45 minute drive, then youﾃ｢ﾂ�ﾂ况e ample choice. At the lower end of the budget is Kigongoni Lodge is a great option. Itﾃ｢ﾂ�ﾂ冱 tranquil, affordable and each guest gets their own little lodge. ﾃつ�Those seeking something a bit more unique and personal may find Onsea House to their liking. Thereﾃ｢ﾂ�ﾂ冱 nothing like enjoying a sundowner on their gorgeous veranda. If itﾃ｢ﾂ�ﾂ冱 a massage and a good yoga class youﾃ｢ﾂ�ﾂ决e after, then places like Karama Lodge and Ngare Sero have you covered. These are only a few of the options available. Sites such as TripAdvisor and many tour operator sites can provide comprehensive lists.

Regardless of whether youﾃ｢ﾂ�ﾂ决e planning a pre- or post-safari stay in Arusha, you should definitely try to at least spend a couple of hours there to check out the many markets. This is one of the best places for buying souvenirs and your best opportunityﾃつ�for landing a good deal.ﾃつ�
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2016-01-21T17:21:10+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:7;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:59:"Internal flights on safari: Are they worth the extra cost? ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:98:"http://www.yourafricansafari.com/articles/internal-flights-on-safari-are-they-worth-the-extra-cost";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:405:"The first Tanzania safari I took came with a bit of sticker shock, I will admit it. I had completely different expectations of lodging and transportation costs and was especially surprised to see that internal flights in Tanzania were similarly priced to those in Europe or North America. A one-hour flight from Arusha to central Serengeti can run range from $200-$300ﾃつ�and made me reconsider if we s...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:98:"http://www.yourafricansafari.com/articles/internal-flights-on-safari-are-they-worth-the-extra-cost";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 19 Apr 2013 21:22:25 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:3092:"The first Tanzania safari I took came with a bit of sticker shock, I will admit it. I had completely different expectations of lodging and transportation costs and was especially surprised to see that internal flights in Tanzania were similarly priced to those in Europe or North America. A one-hour flight from Arusha to central Serengeti can run range from $200-$300ﾃつ�and made me reconsider if we should just suck it up and drive there. After a few email exchanges with my tour operator of choice, I decided to do a mixture and fly in but drive back.

Internal flights on African safaris

I will never forget our first internal flight from Arusha to central Serengeti. The Cessna held about eightﾃつ�people, not including the pilots, and had a bit of room for luggage ( see the luggage section on why you should keep it light ). As soon as we were airborne, I watched the lush green landscape of Arusha slowly get smaller and smaller. As we headed west, I saw some of the Masai lodges and villages and could even see Kilimanjaro! What a treat! The further west we headed, the browner the landscape became. As we began our descent into central Serengeti, I watched as the miniature acacia trees became larger and larger. From high above, they resembled bushy clumps of broccoli. What a change in such a short flight.ﾃつ� I wanted to take in every second of my first African safari experience.
Wait! What was that that just moved? I took a closer look and was nearly giddy at my first sighting of a giraffe! From the air! How tiny they looked from above. Before I knew it, we were on the ground and shaking hands with our guide. I was on a giraffe-high for the rest of the afternoon.



On the way back, we drove from Serengeti to Ngorongoro Crater and then on to Lake Manyara and Tarangire (this was over several days, of course). After Tarangire we set off for Arusha. Itﾃ｢ﾂ�ﾂ冱 about a three-hour drive on the tarmacked roads. This was one of the first tarmacked roads weﾃ｢ﾂ�ﾂ囘 been on, and the first thing we had to do before we set off was put the top on the vehicle. We had to do this as weﾃ｢ﾂ�ﾂ囘 be driving at much higher speeds now that we were off the dirt roads. Suddenly, my safari was over and I was just riding in some large vehicle. I could have been anywhere. The three hours did seem to drag on after a while.
I am glad I opted to experience both methods of transportation and would not change it if I had the opportunity to do so. I do think itﾃ｢ﾂ�ﾂ冱 a great opportunity to sample both transportation methods and advise those who are deliberating over the ﾃ｢ﾂ�ﾂ�to fly or not to flyﾃ｢ﾂ�ﾂ� quandary to do a mixture of both. That is, of course, unless you are considering a super-short safari of only a few days. In which case, I would advise flying in and out of the Serengeti and forgoing the other parks as you will absolutely need at least three full days in this wondrous park. Iﾃ｢ﾂ�ﾂ况e since been on a few more safaris in Tanzania, and have opted to incorporate some internal flying on each one. I cannot imagine a more dramatic way to enter or exit a safari park!
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2016-01-21T17:20:52+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:8;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:36:"Price of Uganda tourist visa doubles";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:78:"http://www.yourafricansafari.com/articles/price-of-uganda-tourist-visa-doubles";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:401:"As of July 1, 2015, the price of a tourist visa to Uganda has doubled in price, from $50 US to $100. Its neighbor, Kenya, doubled its fees earlier, so it seems Uganda is following suit. Whether or not Uganda will also implement Kenya's new policy of online applications only, remains to be seen. There are three ways to obtain a tourist visa for Uganda.

Obtaining a tourist visa on arrival in Ugan...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:78:"http://www.yourafricansafari.com/articles/price-of-uganda-tourist-visa-doubles";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 08 Jul 2015 17:14:19 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:3029:"As of July 1, 2015, the price of a tourist visa to Uganda has doubled in price, from $50 US to $100. Its neighbor, Kenya, doubled its fees earlier, so it seems Uganda is following suit. Whether or not Uganda will also implement Kenya's new policy of online applications only, remains to be seen. There are three ways to obtain a tourist visa for Uganda.

Obtaining a tourist visa on arrival in Uganda

The easiest way to obtain a tourist visa is upon arrival at the Entebbeﾃつ�airport or at any border crossing. To obtain your tourist visa, you will need to fill out the visa form and provide $100 US in cash. The tourist visa is good for 90 days.ﾃつ�No photos are needed.ﾃつ�If you are planning on visiting Rwanda and/or Kenya on your holiday, it makes sense to go for the second visa option andﾃつ�get the East African Visa instead. At present, East African Visas can be purchased on arrival in Uganda. The visa allows entrance into Uganda, Rwanda and Kenya and is also valid for 90 days. Its price is also $100 US, making it a far better deal. Be sure to ask for the East African Visa when purchasing your tourist visa. There have been reports of some authorities trying to push the local visa instead.ﾃつ�

Obtaining a tourist visa from your local embassy

If you are able to apply for a tourist visa in person at your local embassy, this is another option for obtaining your visa. You will need to provide two passport photos along with your application. If you are unable to go in person, you can download the form from the embassy website, fill it out, and send in your application. This method is more expensive, though, as there are costs to ship the visa and passport, which run around $40 US plus the cost of the passport photos.ﾃつ�

Items you will need to obtain a tourist visa upon entry into Uganda


	A valid passport that does not expire for at least six months.
	International Health Certificate with proof of Yellow Fever vaccination.
	On occasion, proof of return ticket.ﾃつ�


Visiting Uganda

Uganda is a top destination for big game safari and primate safaris. It is home to many scenic parks, from big game parksﾃつ� Murchison Falls in the north andﾃつ� Queen Elizabeth National Park ﾃつ�in the central regions,ﾃつ�to excellent primate parks likeﾃつ� Bwindi Impenetrable National Park and Mgahinga Gorilla National Park ﾃつ�in the southwest. For those wishing to see mountain gorillas on their safari, Uganda offers the most established tourism infrastructure. Uganda is a popular safari destination, particularly for other African countries, and roughly 50% of Ugandaﾃ｢ﾂ�ﾂ冱 tourists come from neighboring Kenya. Ugandaﾃ｢ﾂ�ﾂ冱 range of terrain also makes it a great choice for those seeking a more standard safari coupled with a mountain gorilla or chimpanzee viewing.ﾃつ�The mighty Nile River also has its orgins in Ugranda.ﾃつ�

If you are interesting in visiting Uganda and would like more information on planning a safari, here are a list of tour operators who offer tours and safaris in Uganda.ﾃつ�
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2016-01-21T17:20:30+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:9;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:40:"Gear for gorilla trekking: What to bring";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:81:"http://www.yourafricansafari.com/articles/gear-for-gorilla-trekking-what-to-bring";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:407:"Gorilla trekking is one of the most sought-after tour activities for those visiting Rwanda, Uganda and DR Congo. For many, it is a once-in-a-lifetime opportuntity, due to the high price of trekking permits.ﾃつ�ﾃつ�To get the best experience out of your trek, you need to prepare well and pack the right gear. Below are things we recommend you bring to ensure you make the most of your trekking experience...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:81:"http://www.yourafricansafari.com/articles/gear-for-gorilla-trekking-what-to-bring";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 12 Oct 2015 20:26:22 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:4004:"Gorilla trekking is one of the most sought-after tour activities for those visiting Rwanda, Uganda and DR Congo. For many, it is a once-in-a-lifetime opportuntity, due to the high price of trekking permits.ﾃつ�ﾃつ�To get the best experience out of your trek, you need to prepare well and pack the right gear. Below are things we recommend you bring to ensure you make the most of your trekking experience.ﾃつ�

Hiking boots

Gorilla trekking involves trekking/hiking and sometimes travelling long distances in search for the endangered mountain gorillas. Because of the nature of the forest, combined with the highﾃつ�altitude, you need light weight hiking boots to help you trek in sometime steep and muddy environments.

Gardening gloves

You will certainly appreciate havingﾃつ�gloves to protect you hands during your gorilla trek. As you move in this forest, remember that it can get slippery and somehow you need to hold on to some plants, trees or even climbing trees. These gloves will therefore protect you from nettlesﾃつ�and other items that may scratch your hands.

Rain jacket and fleece

Mountain gorillas live on the slopes in the tropical rainforestﾃつ�and, as such, rain is received through out the whole year. You can never tell when it is going to rain so you need a rain jacket or a fleece for this purpose. Many clients prefer travelling in traditional drier months of June, July, August and beginning of September and try to avoid rainy seasons of March, April and May. But, with global climatic changes, one needs to prepare for any eventualities.

Long-sleeved shirts and full-length trousers

Ideally, wear thick, long trousers and long sleeved top during your gorilla trek so as to protect against vicious stinging nettles, plants and bush thorns. It is often cold when you set out, so start out with a sweatshirt or jerseys which also help protect against nettles. Whatever clothes you wear to go tracking will likely to get very dirty as you slip and slideﾃつ�in the mud, so if you have pre-muddied clothes, you might as well wear them!

Energy-giving snacks

Gorilla trekking can take from 30 minutes to eight or so hours, which isﾃつ�why a certain level of fitness is required. To add on that, we advise that you carry some energy giving snacks as supplements. Before departing from your lodge, you need to carry your packed lunch and lots of drinking water as you never know when you will return.

Cameras and extra batteries

Photography is an important component of your safari and thus ensure that you have your camera ready, charge your batteries very well and if necessary, carry extra batteries as you will need to take as many photographs in the one hour you will have with the gorillas. Please make sure that you switch off your flash while taking gorilla photographs. A water proof bag is also important to protect your cameras from rain water.

Hat and sun glasses

If it is hot, please carry a hat to protect you from the strong sun rays. You may also need to carry some sun glasses too. These are extra optionals.

Pair of binoculars

Donﾃ｢ﾂ�ﾂ冲 forget that you are on safari and you want to have clear views of everything. Bwindi Impenetrable National park is home to over 350 birds, chimpanzees, black and white colubus monkeys among other primates, forest elephants and buffalo. If you'll be visiting htis park, you'll also wantﾃつ�a chance to spot some of the above.

Porters for your gorilla trek

This is an extra optional item and we advise all our clients to take a porter for their gorilla trekking. As earlier noted, gorilla trekking can be strenuous and thus you will need someone to give you a push or a pull on the way to the gorillas. These porters go for a fee, but it is worth it. Remember that these porters are actually school going students either in their vacation or have been chased out of school due to school fees. By paying some money to have a porter with you, you will have contributed to community development directly or indirectly.
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2016-01-12T20:00:10+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:10;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:42:"Is British Airways abandoning East Africa?";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:83:"http://www.yourafricansafari.com/articles/is-british-airways-abandoning-east-africa";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:406:"The recent decision by British Airways to cancel their London Heathrow to Entebbe route has come as somewhat of a surprise,ﾃつ�as well as leaving many passengers frustrated. As of October 3, 2015, flights between the UK capital and Ugandaﾃ｢ﾂ�ﾂ冱 international hub will cease and travellers between the two destinations will have to look to alternative airlines.

Reasoning behind the BA cancellation to ...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:83:"http://www.yourafricansafari.com/articles/is-british-airways-abandoning-east-africa";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sun, 09 Aug 2015 03:56:38 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:4075:"The recent decision by British Airways to cancel their London Heathrow to Entebbe route has come as somewhat of a surprise,ﾃつ�as well as leaving many passengers frustrated. As of October 3, 2015, flights between the UK capital and Ugandaﾃ｢ﾂ�ﾂ冱 international hub will cease and travellers between the two destinations will have to look to alternative airlines.

Reasoning behind the BA cancellation to Entebbe

While British Airways has not made a formal statement about why they are cancelling the route (currently operating three times weekly), there is much speculation that it has to do with greater competition from other airlines, particularly those based in the United Arab Emirates, and the route no longer being commercially viable for them. Many other airlines were offering cheaper fares with newer aircraft and a greater level of comfort than BAﾃつ� - factors favorable to many passengers, despite longer connections and transit times.

The cancellation comes following the suspension of British Airways routes into the Zambian capital, Lusaka, in 2013 and the Tanzanian capital, Dar es Salaam in 2012. British Airways clearly stated when they cancelled the Lusaka route that they ﾃ｢ﾂ�ﾂ腕emain fully committed to Africa and will continue to serve East Africaﾃ｢ﾂ�ﾂ� but the recent announcement questions that commitment. At this stage the airline is keeping its route into the Kenyan capital of Nairobi, with connections available from here into Entebbe with other airlines.

Alternative airlines/routes on offer

The news has left many travellers (particularly frequent business travellers) dismayed, and while BA is trying to re-route passengers already booked before October 3, the alternative options they have made available often involveﾃつ�considerably longer transit times. For those booked on code share American Airlines and Iberiaﾃつ�flights, passengersﾃつ�are required to cancel tickets and rebook with other airlines, often at a premium.

Currently, there is the option of flying with BA to Nairobi and connecting from there, or re-routing through Abu Dhabi with Etihad or Brussels with Brussels Airlines. While there are plenty of airlines waiting in the wings to pick up the slack and keep transit times to a minimum, frequent travellers on the route with BA who have accrued miles with the One World Alliance over years of loyalty are also left with few options for redeeming them with these new airlines.

For those looking to fly to Uganda from London in the future, alternative routes include via Dubai with Emirates, through Doha with Qatar Airways, via Istanbul with Turkish Airlines, and through Amsterdam with KLM. African airlines servicing both destinations include South African Airways connecting through Johannesburg, Ethiopian Airlines flying through Addis Ababa and Kenya Airways through Nairobi.

While British Airways was a preferred airline for many coming from North America, code sharing with American Airlines, those flying from the United States and Canada will now have to go through Dubai, Addis Ababa, Amsterdam, Brussels or Istanbul. However transit times are still around the 20 hour mark and fares competitive with what BA was offering.

The end of an era for British Airways

Twenty years ago, British Airways was one of the few international carriers flying into Uganda, stopping in Nairobi en route.ﾃつ�But, as new operators see the potential of using the burgeoning African market widenﾃつ�their networks, it seems the competition is all too much for the UK-based airline. The colonial British have long held sway in many East African countries, but the increase in trade between the continent and places like the UAE and China is resulting in a natural shift in the demands and desires of passengers, together with the viability of flight routes. Considering the majority of tourists to East Africa today hail fromﾃつ�North America and Europe, and the long-term loyalty of frequent business travellers on the British Airways/Oneworldﾃつ�route, the airlineﾃ｢ﾂ�ﾂ冱 handling of the cancellation has left many unhappy.
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2015-11-04T10:41:16+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:11;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:40:"Big changes to Kenya&#039;s visa process";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:76:"http://www.yourafricansafari.com/articles/big-changes-to-kenyas-visa-process";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:406:"Travelers to many African countries have been accustomed to obtaining visas upon arrival at their port of entry. This was true for Kenya for a long time, however changes are afoot. Kenyaﾃ｢ﾂ�ﾂ冱 Department of Immigration Services has confirmed that the process to obtain a visa to Kenya will change on July 1, 2015. Effective that day, nationals requiring a visa to visit Kenya can visit www.ecitizen.go....";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:76:"http://www.yourafricansafari.com/articles/big-changes-to-kenyas-visa-process";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 01 Jul 2015 00:18:24 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:3026:"Travelers to many African countries have been accustomed to obtaining visas upon arrival at their port of entry. This was true for Kenya for a long time, however changes are afoot. Kenyaﾃ｢ﾂ�ﾂ冱 Department of Immigration Services has confirmed that the process to obtain a visa to Kenya will change on July 1, 2015. Effective that day, nationals requiring a visa to visit Kenya can visit www.ecitizen.go.ke to begin the new online visa application process. Visitors will still be able to obtain visas upon airport arrival until the end of August, however all arrivals from September 1, 2015 must apply and pay for visas online to enter Kenya. After application and payment, the approval process is expected to take a minimum of 7 days, so it is advisable to begin the process a minimum of two weeks in advance of travel to account for delays.

The website is straightforward and easy-to-use. The following steps are required to obtain a visa:

ﾃつｷﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ� Register on www.ecitizen.go.ke as a visitor

ﾃつｷﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ� Verify your account via email

ﾃつｷﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ� Upload a passport photo (maximum dimensions 500px by 500px)

ﾃつｷﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ� Choose the ﾃ｢ﾂ�ﾂ魯epartment of Immigration Servicesﾃ｢ﾂ�ﾂ�

ﾃつｷﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ� Select ﾃ｢ﾂ�ﾂ牢ubmit Applicationﾃ｢ﾂ�ﾂ�

ﾃつｷﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ� Select ﾃ｢ﾂ�ﾂ婁enyan Visaﾃ｢ﾂ�ﾂ�

ﾃつｷﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ� Select the type of visa you require ﾃ｢ﾂ�ﾂ� single entry or transit

oﾃつ�ﾃつ� A single entry visa is Issued for those nationalities requiring a visa to enter Kenya either for business, tourism or medical reasons

oﾃつ�ﾃつ� A transit visa is issued to those connecting through Kenya to other destinations for a period not exceeding 72 hours

ﾃつｷﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ� Read the instructions carefully and hit the ﾃ｢ﾂ�ﾂ連pply Nowﾃ｢ﾂ�ﾂ� button

ﾃつｷﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ� Fill out every requested detail in the application form

ﾃつｷﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ� Attach a scanned copy or photo of your passportﾃ｢ﾂ�ﾂ冱 bio data page

ﾃつｷﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ� Pay with a credit or debit card

ﾃつｷﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ� Await approval via email, then download and print the eVisa

ﾃつｷﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ� Present your eVisa to the immigration officer at the point of entry.

Keep in mind that the visa processing fee is nonrefundable and that incomplete applications will be rejected. While the visa fees have not changed, a $1USD ﾃ｢ﾂ�ﾂ湾rocessing feeﾃ｢ﾂ�ﾂ� has been added to the online transactions. Additionally, as with many countries, your passport must be valid for at least six months beyond the date of travel. Effective with September 1, 2015 arrivals, if you do not follow the process to obtain a visa in advance according to the guidelines above, you will be denied entry to Kenya. This is a country you do not want to miss, so be sure to follow the simple instructions to ensure smooth entry.
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2015-10-12T21:28:51+01:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:12;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:57:"Photographic safaris: In search of the perfect photograph";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:98:"http://www.yourafricansafari.com/articles/photographic-safaris-in-search-of-the-perfect-photograph";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:404:"Photographic safaris are arguably one the wildlife tourism categories with the lowest wildlife and natural environment impact.ﾃつ� Regardless of your photographic skill level, photographic safaris are created with photographers in mind ﾃ｢ﾂ�ﾂ� arrive with serious photographic aspirations and you find will opportunities of a lifetime!

Capturing the best of Africa

Photographic safaris in Africa has ...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:98:"http://www.yourafricansafari.com/articles/photographic-safaris-in-search-of-the-perfect-photograph";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 21 Nov 2013 06:04:08 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:5198:"Photographic safaris are arguably one the wildlife tourism categories with the lowest wildlife and natural environment impact.ﾃつ� Regardless of your photographic skill level, photographic safaris are created with photographers in mind ﾃ｢ﾂ�ﾂ� arrive with serious photographic aspirations and you find will opportunities of a lifetime!

Capturing the best of Africa

Photographic safaris in Africa has gained extensive ground and today, regions such as Tanzaniaﾃ｢ﾂ�ﾂ冱 Serengeti, Kenyaﾃ｢ﾂ�ﾂ冱 Masai Mara, Botswanaﾃ｢ﾂ�ﾂ冱 Okavango Delta and South Africaﾃ｢ﾂ�ﾂ冱 Kruger National Park are widely recommended as Africaﾃ｢ﾂ�ﾂ冱 most photographic rich environments.ﾃつ� Whether East or Southern Africa interests you, there are few limits when it comes to photographic opportunities in Africa.ﾃつ� Although pure wildlife photography focusing on Africaﾃ｢ﾂ�ﾂ冱 greatest and most impressive wildlife is most popular, do not forget about spectacular nature landscapes ﾃ｢ﾂ�ﾂ� places such as Namibiaﾃ｢ﾂ�ﾂ冱 dunes which will make you feel like youﾃ｢ﾂ�ﾂ况e discovered a lost world.

Why professional photographic safaris are your best choice

When it comes to professional photographic safaris, there are two key Roleplayers: the leading professional photographer (acting as your photography mentor) and the professional guide.ﾃつ� In some elite cases, one extremely talented individual can act as both!ﾃつ� The essence here is to realize that you need an experienced photographer and host who knows the environment, to guide you to the best location, someone that can guarantee that you will find the perfect photographic opportunities.ﾃつ� After all, youﾃ｢ﾂ�ﾂ况e traveled extensively to a remote corner of the world to find that perfect shot!ﾃつ� The importance of having an experienced guide around is that usually guides can predict wildlife movement and behavior fairly accurately.ﾃつ� This may very well offer you the opportunity for a quick change of direction or angle, and hence why an experienced guide can perfectly complement the photographic safari team.ﾃつ� The top quality photographic safaris are lead by qualified photographers and although most of the day is usually spent tracking wildlife, top quality photographic safaris will offer some opportunities for learning from the best.ﾃつ� Whether by means of a short lunchtime workshop or guidance and advice while shooting location, seek the opportunity to learn from Africaﾃ｢ﾂ�ﾂ冱 greatest wildlife photographers.

Safari vehicle bound or on foot?

Whether your photographic safari will allow you to move freely from the usual safari vehicle depends on your safari operator and most importantly the location. ﾃつ�Most photographic safaris offer shooting from an open air safari vehicle. ﾃつ�These open air vehicles allow the photographers some level of free movement, while remaining within the safety regulations of the relevant park.ﾃつ� Shooting from a safari vehicle has one huge benefit and that is that you can get within an extremely close distance to your desired wildlife objects (but always remember that wildlife can move quickly, so be prepared for that once in a lifetime photo!)ﾃつ� Regardless of skill level, shooting from a safari vehicle does have a few challenges, including the fact that you will most likely shoot from a seated position and always need to compensate for imperfect stability.ﾃつ� Most professional photographic safari operators offer ways around this stability issue, including vehicles optimized for serious photography.ﾃつ� There are also a small selection of photographic safaris that specialize off-the-beaten-track locations, including on foot shooting without the constraints of a safari vehicle.ﾃつ� Again, this is only advisable if leaving your safari vehicle is allowed by the park or regionﾃ｢ﾂ�ﾂ冱 safari guidelines.

Expenses

Photographic safaris are expensive.ﾃつ� Firstly because you hire a professional photographer to assist your journey, special locations are sought, the number of people are limited and expensive equipment is used.ﾃつ� To keep the budget under control, consider hiring universal photography equipment (it will save you on luggage!)ﾃつ� Most photographic safari operators offer tripods, lenses and other equipment at rates varying between US$5 and US$50 per day.

Season and location

In deciding when and where to undertake a photographic safari, it is essential to bear the one most important factor of any photographic safari in mind: seeking a high concentration of wildlife (or birds), without safari crowds.ﾃつ� Peak summer is not recommended as the best time as wildlife can easily disappear in lush vegetation.ﾃつ� If you are after wildlife phenomena such as the Serengeti Migration, choose your timing carefully.ﾃつ�

A photographic safari will leave you with lifetime memories - memories that you can print, frame and show the world!ﾃつ� Most photographers will tell you that photographing wildlife takes some practice; look for creative ways to practice at your local national park or zoo!ﾃつ� And once youﾃ｢ﾂ�ﾂ况e set foot in Africa, challenge yourself to capture Africaﾃ｢ﾂ�ﾂ冱 dynamic spirit, you will not leave the continent untouched. ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�

ﾃつ�
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2015-03-26T16:44:52+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:13;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:48:"What you need to know about Kruger National Park";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:90:"http://www.yourafricansafari.com/articles/what-you-need-to-know-about-kruger-national-park";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:403:"Kruger National Park is one of the largest game parks in all of Africa and one of the most-visited parks, especially in South Africa. It is a popular destination for those seeking an unforgettable safari experience and is easily accessible by direct flight from many South African major cities.ﾂ�
The Kruger National Park is truly the flagship of the South African national parks, Kruger is home to a...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:90:"http://www.yourafricansafari.com/articles/what-you-need-to-know-about-kruger-national-park";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 17 Jun 2014 20:27:35 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:5001:"Kruger National Park is one of the largest game parks in all of Africa and one of the most-visited parks, especially in South Africa. It is a popular destination for those seeking an unforgettable safari experience and is easily accessible by direct flight from many South African major cities.ﾂ�
The Kruger National Park is truly the flagship of the South African national parks, Kruger is home to an impressive number of species: 336 trees, 49 fish, 34 amphibians, 114 reptiles, 507 birds and 147 mammals, including all of the big five ﾂ�game animals . Man's interaction with the Lowveld environment over many centuries--from bushman rock paintings to majestic archaeological sites like Masorini and Thulamela--is very evident in the Kruger National Park.
Most importantly, itﾂ�features all of the ﾂ�as well as many other amazing animals such as wild dogs, giraffe, hippoﾂ�and cheetah.

Useful information on Kruger National Park

Self drive is a good option if you have time to travel and explore the park.ﾂ� But a guided tour for one or a couple days will give you a better experience and a understanding for the Kruger Park and how it works.ﾂ� Early morning and late afternoon is your best time to view animals so dont sleep late and then go out after breakfast you will be very disappointed and your money and visit will be a waste.ﾂ�

Bush walks can also be arranged from the main gamp and most of the entry gates to the park.

Lodging and accommodation is available, but make sure you book well in advance as the park is very popular in local school holidays. The affordable bungalows in the park cost around about $80 for 2 to 3 people sharing a night.

The restaurants and shops have been updated is much better than in the past with service and food options. The local shops also have all the basics you will need on your trip.

Kruger has 12 main rest camps, fiveﾂ�bushveld camps, two bush lodges, two overnight bird hidesﾂ�and four satellite camps. It also has nine entry gates.

Main rest camps of Kruger National Park

Berg-en-Dal (with satellite Malelane)
Crocodile Bridge
Letaba
Lower Sabie
Mopani (with satellite Tsendze)
Olifants (with satellite Balule)
Orpen (with satellites Maroela and Tamboti camp)
Pretoriuskop
Punda Maria
Satara
Shingwedzi
Skukuzaﾂ�

Bushveld camps of Kruger National Park

Bateleur
Biyamiti
Shimuwini
Sirheni
Talamati

Bush lodges of Kruger National Park

Boulders
Roodewal

Overnight bird hides of Kruger National Park

Sable
Shipandani

Entry gates of Kruger National Park

Crocodile Bridgeﾂ�
Malelaneﾂ�
Numbi Entranceﾂ�
Orpenﾂ�
Pafuri
Paul Krugerﾂ�
Phabeniﾂ�
Phalaborwaﾂ�
Punda Mariaﾂ�

Kruger wilderness trails

Guided hiking trails and guest stay overnight in the bush at small rustic temporary camps.


	
		
			Wilderness Trail
			Restcamp
			Nearest Entrance Gate
		
	
	
		
			
			Bushman
			
			
			Berg-en-Dal
			
			
			Malelane
			
		
		
			
			Metsi-Metsi
			
			
			Skukuza
			
			
			Kruger
			
		
		
			
			Napi
			
			
			Pretoriuskop
			
			
			Numbi
			
		
		
			
			Nyalaland
			
			
			Punda Maria
			
			
			Punda Maria
			
		
		
			
			Olifants
			
			
			Letaba
			
			
			Phalaborwa
			
		
		
			
			Sweni
			
			
			Satara
			
			
			Orpen
			
		
		
			
			Wolhuter
			
			
			Berg-en-Dal
			
			
			Malelane
			
		
	


Kruger National Parkﾂ�Standard Daily Conservation Fee (foreign visitors)

R248 per adult, per dayﾂ�
R124 per child, per day

Reopening ofﾂ�Skukuza Airport

The old Skukuza airport was closed down a couple of years ago and as of June 1st, 2014 has re-opened with twice-daily flights.ﾂ�This well-situated airport is in the middle of the bush close to Skukuza, with t he main camp of Skukuzaﾂ�only 10 minutes away.

Some things to know about flying to Skukuza before you go


	Make sure you join a tour on the other side or you have your transfer sorted before you fly as this is not like other normal airports where taxi's and shuttle busses are waiting for you. These services must be arranged before hand.
	You will be charged a conservation fee of R248 per person entering the terminal building if not already included in your reservation or organized tour. This is like your daily entrance fee for international tourist.ﾂ�
	Tours can be arranged prior to arrival with Private Kruger Safaris based in Hazyview. The park can also assists in basic 3 hour turn around tours.
	Avis does have an office in Skukuza and a shuttle where you can hire normal vehicles for self drive - this is not recommended for first-time visitors to Kruger. Theﾂ�park is huge and and you have to make sure where you are going and if you will make it on time as entry and campﾂ�gates close and open at different times during different seasons.
	South African Airways is offering the flights twice-daily to Skukuza from O R Thambo, Johannesburg. With connecting flights, you can fly from Cape Town to Skukuza.
	Transfers can be arranged to private lodges and to lodges outside of Kruger Park with a game drive included from the airport.

";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2015-03-26T16:44:23+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:14;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:60:"When is the best time to see the great wildebeest migration?";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:101:"http://www.yourafricansafari.com/articles/when-is-the-best-time-to-see-the-great-wildebeest-migration";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:412:"The great annual wildebeest migration is a spectacle many of us, even those whoﾃ｢ﾂ�ﾂ况e not been on safari before, are familiar with. Courtesy of some excellent nature documentaries, weﾃ｢ﾂ�ﾂ况e been able to get up close and personal to the wildebeests as they desperately try to cross the crocodile-laden Mara River. Weﾃ｢ﾂ�ﾂ况e seen them traverse a river by the hundreds, and gasped as the unlucky ones are na...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:101:"http://www.yourafricansafari.com/articles/when-is-the-best-time-to-see-the-great-wildebeest-migration";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 30 Apr 2014 20:01:45 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:6643:"The great annual wildebeest migration is a spectacle many of us, even those whoﾃ｢ﾂ�ﾂ况e not been on safari before, are familiar with. Courtesy of some excellent nature documentaries, weﾃ｢ﾂ�ﾂ况e been able to get up close and personal to the wildebeests as they desperately try to cross the crocodile-laden Mara River. Weﾃ｢ﾂ�ﾂ况e seen them traverse a river by the hundreds, and gasped as the unlucky ones are nabbed by the swift blow of a crocodile jaw. If itﾃ｢ﾂ�ﾂ冱 this scintillating via television or internet, imagine how it must be in person!

Where is the great wildebeest migration?

Before jumping to the best times to see the great migration, letﾃ｢ﾂ�ﾂ冱 give a bit more detail on the great migration and where one can see it. Every year, around 1.5 to 2 million wildebeest, or gnu, migrate in a clockwise fashion in search of lush, green grasses as they follow the rain patterns. They start the year in Serengeti National Park, located in Tanzania, and move in a clockwise fashion up into the Masai Mara, which is found in Kenya, and then head south back into Tanzania.
Tanzania and Kenya are the two countries where one can witness this incredible event.

The wildebeest may be the stars of the migration, but they are not the only participants. They are joined by other ungulates such as zebra, gazelles, eland and impala. The old adage ﾃ｢ﾂ�ﾂ�thereﾃ｢ﾂ�ﾂ冱 safety in numbersﾃ｢ﾂ�ﾂ� certainly holds true here.ﾃつ� By traveling with other animals, the group is able to use the various strengths each animal possesses to help ensure they survive. Zebra have excellent eyesight but rely on the keen sense of hearing and smell, which their companions the wildebeest possess, to help get them from point A to point B safely.

When to see the great migration

ﾃ｢ﾂ�ﾂ聾hen is the best time to see the great migration?ﾃ｢ﾂ�ﾂ� This is the question I get asked most often, since the majority of people plan their safari around work or school schedules, and are left with only few months as viable contenders. Although there is no bad time to see the migration, some months may be more attractive depending on what one wishes to see. The migration also varies from year to year, and no two years are ever the same, making planning a bit tricky. Safari companies do their very best to put together an itinerary that puts you right in the middle of the migratory action, but itﾃ｢ﾂ�ﾂ冱 always advisable to spend several days in either the Serengeti or Masai Mara, to give your guide adequate time to locate the herd, should they not be exactly where your itinerary said theyﾃ｢ﾂ�ﾂ囘 be. ﾃつ�


Below are the general patterns of the migration, with the caveat that nature is never entirely predictable.

December, January, February and March

December through March sees the migration primarily in the eastern and southern areas of the Serengeti, as well as in parts of the Ngorongoro Conservation area.

January marks the start of calving season for the wildebeest, which continues through March. The start of the year is a particularly popular time for those who are keen to see the tiny faces dotted throughout the plains.
February is the start of the long rain season, which is more pronounced in eastern Tanzania. Those planning on visiting Lake Manyara and Tarangire during the Serengeti safari may wish to allow for a few extra days.

April and May

April is the peak of the long rain season and with the rains come beautiful flowers and lush green grass. Itﾃ｢ﾂ�ﾂ冱 also low season for most hotels. Those seeking reduced prices and crowds may wish to consider visiting the Serengeti in April.ﾃつ� Somewhere towards the end of April to mid-May, the migration beings its journey west into central Serengeti. Central Serengeti has a wide array of lodging options and is also one of the areas where one can enjoy a hot air balloon rides.
Rut season begins sometime in March, and if youﾃ｢ﾂ�ﾂ况e never heard a wildebeest in rut, you are in for a surprise.ﾃつ� You only have to stop and listen and your ears will be met with a cacophony of noisy grunts that permeate the air.

June and July

By June, the migration is fully in the western corridor of the Serengeti. The Grumeti River runs through this part of the Serengeti and those who visit during this time of year will likely get to see them cross this famous river. Sometime in July, depending on how much moisture thereﾃ｢ﾂ�ﾂ冱 been during the year, the migration starts heading north, towards Kenya. They first must pass over the Mara River, which is a haven for photographers looking for that perfect river-crossing photo.
The lodging options in northern Serengeti tend to be higher-end, and the northern part of the Serengeti ﾃつ�is the other area where hot air balloon rides are offered. July is the start of the dry season, and days usually bright and dry and nights can be chilly. Most northern hemisphere schools are now on summer break, so lodges tend to book up quickly.

August, September, October and November

By August, the migration is in the northern part of the Serengeti, or has crossed into the Masai Mara. These months are the peak safari months for those wishing to visit the Masai Mara. They are also peak season for hotels, which often book months in advance.
In November, the herd has normally returned back to the Serengeti or is well on its way. Depending on the yearﾃ｢ﾂ�ﾂ冱 weather, the migration can be found in northern or central Serengeti. Come December, the herd moves back to central and eastern Serengeti and begins the cycle all over again.

Planning your safari great wildebeest migration

The great migration is often a once-in-a-lifetime event. Some may spend several years saving up for the safari of their dreams. Itﾃ｢ﾂ�ﾂ冱 important to remember that nature is not predictable and that the migration doesnﾃ｢ﾂ�ﾂ冲 always move in a forward motion. Sometimes it moves in a lateral direction and sometimes in a retrograde one. No one can precisely pinpoint the location of the migration and the best way to ensure youﾃ｢ﾂ�ﾂ决e able to witness this event is to allocate enough time. By giving yourself and extra day or two, you give your guide the ability to talk to other guides and to help get you there with enough time to experience the spectacle over the course of a few days.
One thing is for sure, no matter what time of year you decide to visit either of these majestic places, you will be spoiled for choice when it comes to wildlife viewing opportunities.ﾃつ�

Are you an expert on the great migration? Would you like to add more info to this article, or contribute to another migration-related article? Please contact us at safaris@yourafricansafari.com.
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2015-03-05T17:16:32+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:15;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:52:"Ebola: Why now is the time to book an African safari";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:93:"http://www.yourafricansafari.com/articles/ebola-why-now-is-the-time-to-book-an-african-safari";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:413:"Ebola virus and your African holiday

Thereﾃ｢ﾂ�ﾂ冱 no disputing that latest Ebola outbreak is the worst in history. Itﾃ｢ﾂ�ﾂ冱 impacted many thousands of people and can beﾃつ�a very deadly disease for primates. Unfortunately,ﾃつ�most media haveﾃつ�selectively highlighted the horrifying details of the virus and glossed over the non-sensational bits. For the most part, it's caused unnecessary fear and stress f...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:93:"http://www.yourafricansafari.com/articles/ebola-why-now-is-the-time-to-book-an-african-safari";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 08 Dec 2014 23:04:37 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:5690:"Ebola virus and your African holiday

Thereﾃ｢ﾂ�ﾂ冱 no disputing that latest Ebola outbreak is the worst in history. Itﾃ｢ﾂ�ﾂ冱 impacted many thousands of people and can beﾃつ�a very deadly disease for primates. Unfortunately,ﾃつ�most media haveﾃつ�selectively highlighted the horrifying details of the virus and glossed over the non-sensational bits. For the most part, it's caused unnecessary fear and stress for people who are traveling to areas that are not even remotely close to the outbreak.ﾃつ�
Ifﾃつ�you are contemplatingﾃつ�postponing, or even cancelling, an upcoming African safari, please allow usﾃつ�to highlight some importantﾃつ�facts that the media haveﾃつ�largely ignored.


	There are no direct flights from any of the three infected countries to any of the 14 safari countries YAS features.ﾃつ�
	Air France hasnﾃ｢ﾂ�ﾂ冲 suspended any of its flights and still operates a daily direct flight between Conakry, Guinea and Paris, France. Brussels Air operates an indirect flight to Freetown, Sierra Leone. The flight stops in Dakar, Senegal and then Conakry, Guinea, before reaching Freetown. In other words, it goes to two of the three Ebola countries prior to landing in Brussels.ﾃつ�
	Cote d'Ivoire, or Ivory Coast, shares a border with Guinea and Liberia. It has managed to keep the virus out of the country by banning all flights from Ebola countries and strict border controls.ﾃつ�
	South Africa has put a ban on all travelers from Ebola countries
	Kenya Airways has frozen routes to Liberia and Sierra Leone
	Ebola has been present in Uganda and DRC for many years and both countries, but especially Uganda, have been effective at containing it.


Ebola and its impact on African tourism

YAS was able to speak with many of the tour operators recently at the World Travel Market trade show in London this past November. It spoke with tour operators who represent operations in the following countries: Botswana, Kenya, Madagascar, South Africa, Tanzania, Uganda and Zambia. Even if the responses were mixed on whether or not the virus has impacted current and future bookings, the message about marketing expenditures in 2015, or at least the first half of 2015, was consistent. Tour operators wonﾃ｢ﾂ�ﾂ冲 be increasing their budgets and many have either frozen or cut back budgets while they wait to see how things shake out. They also believe the media has played a large role in consumers' fear to travel to East and South Africa and to cancel upcoming safaris. Many tour operators and safari lodges have been forced to waive all cancellation fees, for fear of losing future business.ﾃつ�ﾃつ�

Positive note:ﾃつ�More people going to Madagascar

ﾃδ｢ﾃつ�ﾃつ軌ne country may have a positive story to tell from the Ebola crisis. Nearly all of the tour operators we interviewed who operate in this country have seen a small to moderate increase in the amount of tours booked in late 2014 and 2015. Only one tour operator reported a cancelled safari, and that was because the itinerary included a mainland Africa portion. It is estimated that the number of visitors to Madagascar would be even larger were it not for the Bubonic Plague scare. The plague is endemic to the island and WHO are not recommending travel bans and the US State Department has issued no travel advisories

Ebola is not new to Africa

March 2014 marked the worst Ebola outbreak to date, but it was first identified nearly 4 0 y ears ago, in 19 76 . The original outbreak occurred in two African countries: Zaire, which is now the Democratic Republic of the Congo (DRC) and Sudan. In DRC, the Ebolaﾃつ� virus infected 318 people, of whom 280, or 88%, died. ﾃつ�In Sudan, 284 people were infected and 151, or 53%, died. The DRC was hit hard once again in 1995, when 315 people became infected and 250 died. Between 2000 and 2005, over 600 people were infected. Uganda saw 425 people infected, with just ove r 50% succumbing to the virus. Uganda had another outbreak in late 2007, with 149 cases report. A remarkable 112 cases were able to pull through, with only 37 deaths reported.


2014 Ebola Outbreak




The 2014 Ebola epidemic is thought to have originated in December 2013, in the small Guinean town of Meliandou. The village is located within the forest and is it believed that the fruit bat is the main transmitter of the virus. According to the CDC, only mammals have shown the ability to spread and become infected with the Ebola virus. There is no evidence that mosquitos or other insects can transmit the virus. The 2014 Ebola outbreak would eventually impact several western Africa countries: Guinea, Liberia, Sierra Leone, Mali, Nigeria and Senegal. It al so impacted a small number of citizens of Span and the United States. WHO has since declared outbreaks in Nigeria and Senegal as officially over.

At the time of publishing this article, the number of Ebola virus cases is leveling off in two of the three main countries. The World Health Organization reports that their target to get 70 percent of Ebola-infected people into treatment has been met in Liberia and Guinea. Sierra Leoneﾃ｢ﾂ�ﾂ冱 Ebola outbreak case number has now surpassed that of Liberia.

Although this Ebola outbreak has taken a devastating toll on three of Africa's countries, its center is further away from all African safari countries than it is from Rio de Janeiro. Both South and East Africa have taken stringent measures to prevent the virus from spreading. If you've any doubts about the virus and or potential risks, get an informed opinion, rather than blindly following hearsay. At the time of publication, the US State Department has issued no travel alerts or warnings for any African safari country.


";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2015-02-22T19:17:10+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:16;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:17:"Altitude sickness";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:59:"http://www.yourafricansafari.com/articles/altitude-sickness";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:399:"Sometimes it is not the lack of fitness that prevents people from reaching summit, it can also be the altitude sickness. The possiblity of reaching the peak mainly depends on how you cope with the altitute. There are things that you can possibly do to avoid the symptoms of acute mountain sickness when climbing Kilimanjaro.

How to Prevent altitute Sickness?

Altitude sickness is not only based...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:59:"http://www.yourafricansafari.com/articles/altitude-sickness";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 28 Mar 2013 22:34:05 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:2979:"Sometimes it is not the lack of fitness that prevents people from reaching summit, it can also be the altitude sickness. The possiblity of reaching the peak mainly depends on how you cope with the altitute. There are things that you can possibly do to avoid the symptoms of acute mountain sickness when climbing Kilimanjaro.

How to Prevent altitute Sickness?

Altitude sickness is not only based on the absolute height you are at, it also depends on how fast you got there. There are a lot of things you can do before and during climd that can reduce the risk of altitute sickness symptoms.

Choosing the Kili Route:

A big factor in having good climb is choosing the right route to Climb Kilimanjaro. There are a huge number of tour operators; Kilimanjaro is so expensive, so it is easy to look for the cheapest deal.

The cheapest Kilimanjaro Climbs are also the shortest. However, most responsible tour operators do not offer four day climbs to people who have no previous experience at high altitude trekking and are not acclimatized.

The statistics from the registration books at Mount Kilimanjaro National Park indicate that of all climbers on five days routes, only little over a quarter reach the summit! All the five day routes offer an opportunity to add an extra day for acclimatization.

If you want to increase your chances of reaching the summit further, consider choosing one of the longer routes. (Just a hint, this is recommended for people who are used to camping out, if you are not and if you donﾃ｢ﾂ�ﾂ冲 like or donﾃ｢ﾂ�ﾂ冲 sleep well in a tent, then a longer Kilimanjaro Trek can have a negative effect.)

The right Operator for Kilimanjaro:

When comparing price for Kili climbs you will find huge differences between operators, there is temptation to look up for the cheap deal. However, the saying ﾃ｢ﾂ�ﾂ�you get what you pay forﾃ｢ﾂ�ﾂ� certainly holds true for treks on Kilimanjaro.

The quality of your guides, equipment, food, etc. is reflected in the price. The Operators who cut costs in every corner will not be able to offer you the care and equipment needed for a safe experience.

It is also worth noting that the low budget operators not only have lower success rates, there is also a big risk that something goes seriously wrong, and on Kilimanjaro that can be very dangerous.

What to do on Kili Climb?

ﾃ｢ﾂ�ﾂ抓low and Steadyﾃ｢ﾂ�ﾂ� Pole pole (Swahili word), you will hear this word day to day from your guide. It is the most important thing to keep in mind during climb. No matter what you want or expect, you will be surprised HOW slow your guides make you walk, everything on Kili happens in slow motion.

You walk so slowly, the first days it seems ridiculous. You may even feel you just canﾃ｢ﾂ�ﾂ冲 walk that slowly. Do not be tempted into speeding up because others are walking faster. Another group overtaking? Let me them go! You will pass their tired bodies soon enough ﾃ｢ﾂ�ﾂｦ There is nothing to gain on Kilimanjaro by being the first.
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2015-02-02T15:54:21+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:17;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:30:"Safari advice for first-timers";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:72:"http://www.yourafricansafari.com/articles/safari-advice-for-first-timers";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:419:"Does ﾃ｢ﾂ�ﾂ詫ess impulsive travelerﾃ｢ﾂ�ﾂ� describe your travel style?ﾃつ� If so, you may very well have debated and planned your first safari for some time.ﾃつ� But now that the planning is done, do you know what to expect, what to do or how to behave on your very first safari?ﾃつ� Hereﾃ｢ﾂ�ﾂ冱 the blueprint:

Never attempt your first safari without a guide

Africaﾃ｢ﾂ�ﾂ冱 bush necessitates experience.ﾃつ� As a firs...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:72:"http://www.yourafricansafari.com/articles/safari-advice-for-first-timers";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 08 Oct 2013 20:16:40 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:3836:"Does ﾃ｢ﾂ�ﾂ詫ess impulsive travelerﾃ｢ﾂ�ﾂ� describe your travel style?ﾃつ� If so, you may very well have debated and planned your first safari for some time.ﾃつ� But now that the planning is done, do you know what to expect, what to do or how to behave on your very first safari?ﾃつ� Hereﾃ｢ﾂ�ﾂ冱 the blueprint:

Never attempt your first safari without a guide

Africaﾃ｢ﾂ�ﾂ冱 bush necessitates experience.ﾃつ� As a first-time safari traveler, never go without a guide.ﾃつ� Not only is this wise for your own safety, but also so that you can gain the most from your safari experience.ﾃつ� The inexperienced safari traveler will easily cross paths with wildlife without even noticing it.ﾃつ� An experienced guide will be able to show you the footprints of wildlife in Africaﾃ｢ﾂ�ﾂ冱 dust--something that you will never ever look past again. Guides usually know their parks and reserves like the palm of their hands and will point out wildlife in their hidden places, big and small.ﾃつ� Respect any guidelines or restrictions which your guide may share: wildlife will always be unpredictable and out in the open there is little other protection than adhering to your guideﾃ｢ﾂ�ﾂ冱 safety precautions. ﾃつ� ﾃつ�

What to take along?

The last thing you want is to be weighed down by electronic devices, unnecessary clothing and other possessions.ﾃつ� A safari is your unique opportunity to experience the undisturbed nature of the natural environment--do not spoil this by fretting over belongings or by interrupting the sounds of the wild by constant digital sounds.ﾃつ� You need only a few basic items to make the most of your safari experience.ﾃつ� Two very important items that you cannot go without is a set of binoculars and a camera.ﾃつ� When it comes to taking photographs, you surely want some excellent memories to take back home, but do not get consumed by finding the ultimate shot.ﾃつ� To get the most from your safari, take some time to put your camera away and truly experience the uniqueness of Africa.

Clothing

Even though you might find yourself on African soil, do not get caught at dusk or dawn without a sweater or blazer--even in Africa it can get rather cool when the sun is not out. Always take a warm sweater and hat.ﾃつ� If any of your safari activities will include walking in the bushveld, you will benefit from neutral colored clothing. Opting for neutral and natural colors rather than bright colors is usually advised by experienced guides and tour operators.ﾃつ�

Get to know the local environment, wildlife and community

Whether you booked a safari holiday in Kenya , South Africa or Botswana , take some time before your departure to read up about the country you will be visiting (perhaps even learn a few words in the native language!).ﾃつ� While on safari, you can benefit immensely by learning from your guide or any other local community member.ﾃつ� Local knowledge about wildlife, indigenous plants and the environment usually surpasses what you read in books and magazines thus make the most of the opportunity in front of you.ﾃつ� You will return home enriched with bushveld knowledge!ﾃつ� No safari can take place in isolation of the local community and country you are visiting.ﾃつ� Always show respect for the locals and you will most likely be greeted with warm smiles and a genuine interest. ﾃつ�

Most safari travelers will agree that a safari is essentially about showing respect for Africaﾃ｢ﾂ�ﾂ冱 wildlife and environment. Thus, if you ever find yourself in doubt of how to behave or what to do on safari, always remember to respect the African soil youﾃ｢ﾂ�ﾂ决e walking on, the animals that roam the land and the people that have lived there for centuries.ﾃつ� There are few experiences as enriching as a safari. Take your time to absorbﾃつ�it all and leave a footprint as small as possible.
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2015-02-02T15:54:02+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:18;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:67:"What to know if you&#039;re traveling with children to South Africa";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:103:"http://www.yourafricansafari.com/articles/what-to-know-if-youre-traveling-with-children-to-south-africa";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:404:"More paperwork, stricter rules, but hopefully safer for children ﾃ｢ﾂ�ﾂ� South Africa is introducing new traveling rules for families or individuals traveling with children to and from the country. Initially set for implementation from September 1, 2014, the new regulations proved some practical obstacles which necessitate more time, but now the new regulations will apply from October 1, 2014.

Here...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:103:"http://www.yourafricansafari.com/articles/what-to-know-if-youre-traveling-with-children-to-south-africa";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sat, 19 Jul 2014 19:27:52 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:4541:"More paperwork, stricter rules, but hopefully safer for children ﾃ｢ﾂ�ﾂ� South Africa is introducing new traveling rules for families or individuals traveling with children to and from the country. Initially set for implementation from September 1, 2014, the new regulations proved some practical obstacles which necessitate more time, but now the new regulations will apply from October 1, 2014.

Hereﾃ｢ﾂ�ﾂ冱 what to know if you are planning to travel with children to South Africa after this date:ﾃつ�

Birth certificates to accompany passport and visas 

The new rules will enforce families or individuals traveling with children ﾃ｢ﾂ�ﾂ� whether family or non-family children, to produce complete and unaltered birth certificates as part of standard traveling documents. This will be relevant for all children under the age of 18, regardless of nationality and or language. Where birth certificates are in a foreign language, officials will request birth certificates to be accompanied by a sworn English translation. The new regulations require that birth certificates must either be the original version, or a legally approved copy ﾃ｢ﾂ�ﾂ� no ordinary copy will be accepted. Families need to plan ahead and allow the necessary time to obtain, or legally prepare, such documents.

South African travel laws for children: Even stricter rules for single parents 

The new travel regulations will be even stricter for single parents. Where single parents want to enter or leave South Africa with their children, further documentation representing, or on behalf of the absent parent, will be required. Valid documentation in such cases can include an affidavit in which the absent parent declares and give consent for the childﾃ｢ﾂ�ﾂ冱 travel arrangements, court documents confirming guardianship, or a death certificate, where applicable. Documents needed will vary on individual family cases, and need to be decided accordingly. Travel agents and or a legal representative should be able to advise and assist families. In the case of an affidavit, the date of issue will be important: affidavits on behalf of the absent parent will only be considered valid if used within three months from date of issue.

Youth groups traveling to South Africa 

These new regulations apply to all youth traveling to and from South Africa, whether traveling with both parents, a single parent, grandparents or without parents ﾃ｢ﾂ�ﾂ� as would be the case of school groups or outreach groups. Where children are accompanied by adults other than their parents as part of school or outreach groups, the responsible adults need to be in possession of valid affidavits, in which both parents give consent and approval of their childrenﾃ｢ﾂ�ﾂ冱 travel arrangements.

Opposing views to new South African travel laws

To date, the tourism industry has reacted with opposing views. All of this is amounting to a whole lot of documentation complexities, something the South African tourism industry fears will negatively impact family travel plans to South Africa. This has already caused the South African government to delay the implementation of the new regulations to October 1, 2014. Other sectors of tourism industry and public safety departments are however encouraging the new regulations ﾃ｢ﾂ�ﾂ� including airlines which say that the stricter rules will improve child safety in and outside of South Africa.

Why the new regulations are critical for child safety in South Africa

These regulations may sound like a whole bunch of additional paperwork, planning and even expenses. However, there are solid reasons why South Africa's government is implementing these regulations: a child is kidnapped every six hours in South Africa. As for child trafficking, statistics show that at least 45,000 children are trafficked in South Africa on an annual basis. Kidnapping, abductiona and trafficking -ﾃつ�these crimes are targeted at children in South Africa under the age of 14 years. But the new child travel documentation regulations can work significantly towards addressing these crimes.

For those wanting to travel to South Africa soon, the best way to deal with these new regulations is to prepare in detail. Know exactly what documents your family requires, and if processes are necessary to obtain these, know how much time will be needed to obtain the documents. South Africa is committed to both tourism and the safety of children in the country ﾃ｢ﾂ�ﾂ� prepare your applications carefully and a South African adventure will still be waiting for your family.
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2015-01-11T19:18:45+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:19;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:30:"A beginner窶冱 guide to Africa";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:69:"http://www.yourafricansafari.com/articles/a-beginners-guide-to-africa";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:404:"First-time visitors to Africa

The thing that attracts most first-time visitors to Africa is the prospect of a safari - the chance to see some of this continentﾃ｢ﾂ�ﾂ冱 unique and fascinating wildlife, up close and personal. But planning your first trip to Africa, your first safari holiday, can be a bit overwhelming - so many choices, so many options. In this article we give you a few hints and tips...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:69:"http://www.yourafricansafari.com/articles/a-beginners-guide-to-africa";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 01 Oct 2014 00:39:34 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:3481:"First-time visitors to Africa

The thing that attracts most first-time visitors to Africa is the prospect of a safari - the chance to see some of this continentﾃ｢ﾂ�ﾂ冱 unique and fascinating wildlife, up close and personal. But planning your first trip to Africa, your first safari holiday, can be a bit overwhelming - so many choices, so many options. In this article we give you a few hints and tips and a little guidance on how to get the most out of your first expedition to Africa.

African safari: Are you ready for the journey?

The word ﾃ｢ﾂ�ﾂ徭afariﾃ｢ﾂ�ﾂ� translates from the Swahili language as ﾃ｢ﾂ�ﾂ徊ourneyﾃ｢ﾂ�ﾂ�. While you can always pay a bit more for additional luxury and comfort, a typical safari involves travelling by road from one camp or lodge to the next, starting each day before dawn (to see the animals at their most active). The trip between different lodges or camps are generally long, dusty, hot, and plenty of close encounters with insects. This style of travel doesnﾃ｢ﾂ�ﾂ冲 suit everyone - itﾃ｢ﾂ�ﾂ冱 not lying by a pool with a cocktail while you work on your tan - but it is a travel experience like no other and one that you will treasure for a lifetime.

Prepare and plan for your African safari

As well as researching where you want to go and what you want to see and do, you need to make sure that you have taken the necessary health precautions and packed the right clothes. Malaria is the main concern but talk with a health professional about where you will be travelling and any immunisations that might be required for your trip. Youﾃ｢ﾂ�ﾂ冤l need practical walking shoes and clothing that can cope with heat, dust, and long days of travelling.

Choose the safari that is right for you

Thereﾃ｢ﾂ�ﾂ冱 really two key considerations that you need to have decided before you even start looking at the huge range of safari options that you can choose from - the first is to determine what your budget is, and the second is to be fairly clear on what you want to see on your safari. If you are just looking for a classic safari experience that delivers all of the main animals, then it becomes more of a discussion around whether you want to travel in luxury or budget-style. But if you are wanting to see something a bit more specific such as the wildebeest migration, gorillas, or a white antelope, then that obviously narrows your search. For your first visit to Africa you are probably better to stick to a fairly general safari - get a feel for this continent and how a safari works, and youﾃ｢ﾂ�ﾂ冤l soon be planning your next visit.

Where to go on safari

There are a huge range of safari options across the continent, but an easy introduction to Africa is to plan your safari in East Africa. Spectacular landscapes and relatively easy access to wildlife. East Africa also offers the full range of styles of travel, from budget through to super-luxury.

When to go on safari

Each season on this continent brings its own unique character, and where you decide to go in Africa will mean the conditions that you encounter will vary. If you have decided on East Africa then the dry season from June to October is generally considered one of the the best times. You need to avoid the long rainﾃつ�season, which is normally between March and May, when many of the roads become too difficult to drive down and a number of the lodges and camps close.ﾃつ�

Your perfect safari in Africa is waiting for you. You just need to be ready to discover it.
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-12-31T18:02:17+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:20;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:31:"Ebola virus and African safaris";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:73:"http://www.yourafricansafari.com/articles/ebola-virus-and-african-safaris";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:410:"The impact of the Ebola virus on African tourism

As the world is watching with great concern as Africa is fighting the outbreak of the feared Ebola virus ﾃ｢ﾂ�ﾂ� the largest outbreak in history ﾃ｢ﾂ�ﾂ� many travelers are thinking about canceling or postponing travel plans to Africa. The Ebola virus is among the worldﾃ｢ﾂ�ﾂ冱 most dangerous viruses, yet it is nothing new to the African continent. There are ...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:73:"http://www.yourafricansafari.com/articles/ebola-virus-and-african-safaris";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 04 Sep 2014 05:14:58 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:4186:"The impact of the Ebola virus on African tourism

As the world is watching with great concern as Africa is fighting the outbreak of the feared Ebola virus ﾃ｢ﾂ�ﾂ� the largest outbreak in history ﾃ｢ﾂ�ﾂ� many travelers are thinking about canceling or postponing travel plans to Africa. The Ebola virus is among the worldﾃ｢ﾂ�ﾂ冱 most dangerous viruses, yet it is nothing new to the African continent. There are plenty of misconceptions about the affected areas, and these misconceptions are proving to impact on the African travel trade quite drastically. For this reason, leaders across the travel industry are stepping up to the challenge of informing travelers where it is safe to go, and which countries to avoid for the immediate term. Hereﾃ｢ﾂ�ﾂ冱 what to know if you are planning on traveling to African soon:

Ebola: affected countries

To date, the Ebola virus claimed victims in three West African countries, namely Liberia, Sierra Leoneﾃつ�and Guinea. A limited number of Ebola cases recorded for Nigeria has placed this country on high alert.

East Africaﾃ｢ﾂ�ﾂ冱 Kenya and Tanzania , alongside South Africa and the rest of southern Africa--Africaﾃ｢ﾂ�ﾂ冱 key tourism countries, remain entirely unaffected by Ebola. At present, there are no risks announced for travelers to or from these countries. The World Health Organization (WHO) concurs that the risk of the Ebola virus spreading on airplanes is rather unlikely. Their statement is based on the medically accepted understanding that the virus cannot be transmitted through air. Although air travelers are not at particular risk of contracting the feared virus, Kenya has temporarily banned flights to the affected West African countries Liberia and Sierra Leone, in attempt to close down any potential outbreak of the disease in Kenya.
Other prominent West African countries, including the Ivory Coast and Senegal, are taking serious precautionsﾃつ�and have halted all air travel between these destinations. New cases have been confirmed for the Democratic Republic of the Congo (DRC) ﾃ｢ﾂ�ﾂ� although one should take note that the WHO is confident that these case are unrelated to the West African cases, meaning that the virus has not spread between these countries. These are isolated incidences in remote DRC and have been confirmed as genetically different from the West Africa virus strain ﾃ｢ﾂ�ﾂ� it has not entered the DRC via travelers between African countries.

Ebola outlook

The humanitarian and medical organization Doctors Without Borders, who are actively working in the Ebola stricken areas to combat the virus, indicated that they believe another six months will be needed to bring the outbreak under control. At the same time, travelers are reminded that the Ebola virus can, under no circumstances be contagious, unless physical symptoms of the virus are visible.

African safari tourism

For many African countries, including South and East Africa, tourism is a pivotal economic sector. Turn down tourism and the local communities will feel the effect. Itﾃ｢ﾂ�ﾂ冱 been with great pride and joy that sub-Saharan Africa and East Africa have experienced tourism surges in recent years, and now the industry fears that misconceptions and fears about the Ebola virus could hamper all of this. To prevent the potential spreading of the virus and to reassure both citizens and travelers, South Africa and East African countries have stringent medical measurements for people entering the country, which include temperature monitoring via means of digital electronic devices. It may be somewhat of an inconvenience, but travelers should know that these measures are there to protect all from the virus. If you have plans to travel to East or South Africa in the coming months and have any doubts about the virus and or potential risks, get an informed opinion,ﾃつ�rather than blindly following hearsay. There are plenty of myths about the Ebola virus, and your intended African destination may very well be entirely safe to visit still.
ﾃδ｢ﾃつ�ﾃつ毅e informed, know which countries are safe with no travel restrictions, and which countries should be avoided until this outbreak has been brought under control.ﾃつ�
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-12-11T19:02:20+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:21;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:83:"First comprehensive travel site aimed exclusively at African safaris launches today";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:125:"http://www.yourafricansafari.com/articles/first-comprehensive-travel-site-aimed-exclusively-at-african-safaris-launches-today";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:401:"FIRST COMPREHENSIVE AFRICAN TRAVEL SITE LAUNCHED INCLUDES 1200+ SAFARI TOUR OPERATORS IN 14 COUNTRIES 

Unparalleled range of features: safari reviews, tour packages, photos, articles, social networking and much moreﾃつ�

A new travel site, www.yourafricansafari.com (YAS), launched today that will forever change how people plan their safaris. YAS, a feature-rich travel site, is especially geared...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:125:"http://www.yourafricansafari.com/articles/first-comprehensive-travel-site-aimed-exclusively-at-african-safaris-launches-today";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 28 Oct 2014 01:19:53 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:3560:"FIRST COMPREHENSIVE AFRICAN TRAVEL SITE LAUNCHED INCLUDES 1200+ SAFARI TOUR OPERATORS IN 14 COUNTRIES 

Unparalleled range of features: safari reviews, tour packages, photos, articles, social networking and much moreﾃつ�

A new travel site, www.yourafricansafari.com (YAS), launched today that will forever change how people plan their safaris. YAS, a feature-rich travel site, is especially geared towards travelers who do not know which tour operator they wish to use, which countries or parks they wish to visit, or who need to educate themselves more by studying safari photos and reviews. Robust functions allow travelers to quickly examine tour operators, parks, itineraries, reviews and travel articles.

Your African Safari fills a market void by being the first website specifically geared toward people looking to book African safaris and the companies who provide the safari tours. Its multi-purpose design includes the ability to: do complex searches on safari itineraries, short-list tour companies, submit one-click safari quote requests, find out information on safari countries and share the safari experience via social media. YAS understands the popularity of these travel destinations and presently features 100+ articles, 1230+ tour operators, 1500+ safari itineraries and 4400+ photos from over 75 top African safari destinations. These numbers are steadily growing as the site gains traction.

According to UNWTO, Africa is slated to be the second-fastest growing international travel destination for 2014 [i] . In 2013, it reached a new record with over 56 million visitors. 65% of visitors travelled to sub-Saharan Africa, where the safari countries are located. South Africa, the top sub-Saharan destination, enjoyed a 10% increase in international travelers in 2012 while Tanzania, home of the Serengeti, saw a whopping 24% increase and Madagascar a 14% increase [ii] . The number of international travelers to East Africa is predicted to triple by 2030 to 37 million.ﾃつ� ﾃつ�
ﾃつ�

A function-rich website


	
	Visitors to YAS will find a wealth of information at their fingertips, from detailed information on 14 African countries including electrical plug type used, languages spoken and local currencies, to required malaria regimens for 76 game parks.
	
	
	Once users choose their safari destination, they can view which safari companies operate in the selected country. Or, they can search tour operators via their tour packages using specific criteria such as number of days, price and activity level. YAS then gives users the ability to shortlist and compare up to five tour operators at the same time. Users can solicit input on selected itineraries by sharing their lists with others on Facebook, Google+, Twitter, LinkedIn or Pinterest.
	
	
	Prospective travelers can send all five tour operators a request for a safari quote with the click of one button. Members can interact with other safari enthusiasts by liking reviews, photos and safari itineraries. Even well-established travel websites like Trip Advisor do not have these robust features.
	


ﾃ｢ﾂ�ﾂ弑nlike other travel review sites, YAS was designed with both the traveler and the tour operator in mind,ﾃ｢ﾂ�ﾂ� said managing partner and CTO, Javier Luque. ﾃ｢ﾂ�ﾂ弋ravel is a very personal thing, and people whoﾃ｢ﾂ�ﾂ况e been on an African safari are passionate about their travels and keen to share their experiences with others. YAS provides the platform for them to share it all, from tour operator and park reviews to that best photo of an elusive leopard.ﾃ｢ﾂ�ﾂ�
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-11-17T22:49:07+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:22;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:55:"East Africa窶冱 single tourist visa launch date delayed";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:94:"http://www.yourafricansafari.com/articles/east-africas-single-tourist-visa-launch-date-delayed";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:409:"East African countries Rwanda, Uganda and Kenya failed to implement the all-new three-country single tourist visa on 1 January, as was promised at last year's World Travel Market in London.ﾃつ�For months, tourists, tour operators and local tourism industriesﾃつ�anticipated the single tourist visa, which will enable tourists to move freely between these countries with only a single visa. ﾃつ�The single j...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:94:"http://www.yourafricansafari.com/articles/east-africas-single-tourist-visa-launch-date-delayed";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 06 Jan 2014 10:12:07 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:2785:"East African countries Rwanda, Uganda and Kenya failed to implement the all-new three-country single tourist visa on 1 January, as was promised at last year's World Travel Market in London.ﾃつ�For months, tourists, tour operators and local tourism industriesﾃつ�anticipated the single tourist visa, which will enable tourists to move freely between these countries with only a single visa. ﾃつ�The single joint visa promisesﾃつ�costs and administrative savings, and more convenient movement between these member countries.ﾃつ�But it was with great disappointment that everyone learnt about the delay in getting the system up and running.ﾃつ�

On 2 January, an official statement was issued by the tourism sectors of these countries confirming a delay ﾃ｢ﾂ�ﾂ� but little information was provided on why the delay and more importantly, how soon the issues will be resolved.ﾃつ�More positive news has come forth, and now the tourism world can expect the single tourist visa to be available 'any day now'.

Good news: active single visa system

The lack of official preparation for the single three-country visa has left many confused, especially as the promises of a single tourist visa prompted tourists to adapt their travel plans, duration of stay and of course travel budgets.ﾃつ�It seems as if logistical constraints were the culprit ﾃ｢ﾂ�ﾂ� after all, working together on issuingﾃつ� a single tourist visa is a whole new tourism territory of these three countries.ﾃつ�The good news is that the tourism and national affairs departments of these countries are seemingly sorting the issues and delay, and there are promises of an active single visa system.ﾃつ�According to Rwandaﾃ｢ﾂ�ﾂ冱 Directorate of Immigration & Emigration, everything is in place to start issuing the single tourist visa without further delay.ﾃつ�

The single tourist visa will be available from the member countryﾃ｢ﾂ�ﾂ冱 entry point, or at relevant foreign mission offices.ﾃつ�Tourists seeking to obtain this three-country visa need to present national identity cards, which will also serve as an acceptable travel document in these countries.

Tapping into the regionﾃ｢ﾂ�ﾂ冱 possibilities as one single tourism destination, the single tourist visa system will only be launched officially later this year ﾃ｢ﾂ�ﾂ� and will take place alongside tourism marketing for this new single East African travel destination.

Although officials sound more positive on the implementing of the new single tourist visa system and the issuing of the visas, tourists are reminded that this is a totally new system, necessitating the three member countries to work together.ﾃつ�Glitches are to be expected, but the good news is that the single tourist visa is expected from these East African countries without further major delays.
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-09-26T18:11:41+01:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:23;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:48:"Bringing kids on safari: What is the right age? ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:87:"http://www.yourafricansafari.com/articles/bringing-kids-on-safari-what-is-the-right-age";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:404:"A safari is a great learning experience and can also be an excellent way to spend time with your friends and family. This is the one time youﾃ｢ﾂ�ﾂ冤l have the opportunity to see untamed wilderness up close and with your own personal guide to offer as much information as you can handle.

So what exactly is the right age when children can be considered safari-ready? I get this question a lot and it p...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:87:"http://www.yourafricansafari.com/articles/bringing-kids-on-safari-what-is-the-right-age";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 28 Mar 2013 20:52:03 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:3089:"A safari is a great learning experience and can also be an excellent way to spend time with your friends and family. This is the one time youﾃ｢ﾂ�ﾂ冤l have the opportunity to see untamed wilderness up close and with your own personal guide to offer as much information as you can handle.

So what exactly is the right age when children can be considered safari-ready? I get this question a lot and it probably comes as no surprise to learn that there is no ﾃ｢ﾂ�ﾂ腕ightﾃ｢ﾂ�ﾂ� age. Children mature at different rates and in different ways, and no two are alike. The onus is on the parent or guardian to determine when a child is old enough to have the patience and attention span to endure the long game drives.

With private safaris, there is some flexibility as it is only your group in the safari vehicle. So, if your son or daughter is unable to sit still after three or four hours into the game drive, there is the option to head back to camp. But you must bear in mind that you may be an hour away from camp. With group safaris you are not afforded this liberty. I have seen more than once a safari vehicle returning to camp, small child in the front, crying their eyes out, grimacing faces in the rear, upset at the thought of missing an exciting afternoon of animal watching.ﾃつ� Even with a private safari, itﾃ｢ﾂ�ﾂ冱 still important to think of the others in your party. For example, if you are going with another couple who has older children or who isnﾃ｢ﾂ�ﾂ冲 bringing children, bringing along a young child will have an impact on everyoneﾃ｢ﾂ�ﾂ冱 itinerary.

Itﾃ｢ﾂ�ﾂ冱 always exhilarating when you encounter wildlife. I never tire of seeing a giraffe, with its long, spindly neck, emerge out of the background, nicely camouflaged by the Acacia trees. Animal babies of any species put a wide grin on my face and I could spend literally hours watching them at play. However, itﾃ｢ﾂ�ﾂ冱 very important to bear in mind that when youﾃ｢ﾂ�ﾂ决e observing these animals in the wild, that you are absolutely forbidden to leave the confines of your safari vehicle. For younger children, this can be extremely difficult, especially when Mother Nature calls. ﾃつ�

To conclude, I normally try to encourage children eight years and older to go on safari. Not only are they old enough to remember the experience in enough detail, but they also are starting to get to the age where they have the attention span and patience required on safari. However, younger children are always considered on a case by case basis.

Things to consider when booking a safari with younger children

Some questions I suggest parents and guardians ask themselves when considering booking a safari with younger children include:


	How does my child cope with very early morning starts?
	Can my child go 3-4 hours without a toilet break?
	Does my child do well on long drives? Can they tolerate being in a vehicle for over four hours at a time?
	Is my child into wildlife and nature?
	Is my child attentive and willing to listen to the guide as they explain the wildlife and surroundings to other guests in my party?

";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-09-11T19:05:23+01:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:24;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:22:"Malaria: What to know ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:62:"http://www.yourafricansafari.com/articles/malaria-what-to-know";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:404:"Malaria medication options

There are several anti-malarial medications on the market and what you choose depends on how you react to the medication and what your tolerance is for dosage requirements. Iﾃ｢ﾂ�ﾂ况e been to several countries that are high on the malaria-risk scale. I am poaching this next paragraph from www.drwisetravel.com, as they say it much better than I could. Antimalarial medicati...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:62:"http://www.yourafricansafari.com/articles/malaria-what-to-know";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 08 May 2013 01:15:26 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:2973:"Malaria medication options

There are several anti-malarial medications on the market and what you choose depends on how you react to the medication and what your tolerance is for dosage requirements. Iﾃ｢ﾂ�ﾂ况e been to several countries that are high on the malaria-risk scale. I am poaching this next paragraph from www.drwisetravel.com, as they say it much better than I could. Antimalarial medications do not prevent infection with the malaria parasite, rather, they suppress the symptoms of the infection by killing the parasites either in the liver or as they leave the liver and enter the bloodstream. This should be considered a good thing when one realizes that headache, coma and death are three of the symptoms. There is no perfect antimalarial - i.e. one that is 100% effectiveﾃつ�and always without side effects.

The choice of antimalarial depends upon the destination and the patient, i.e. medical problems, medications and past experience with antimalarials.

Mefloquine, Doxycycline and Malarone

The first medication they mention is Mefloquine. I have not tried it but understand there are some issues. The second is Doxycycline (Vibramycin). I did use this on my trip to India and would probably not take it again for a few reasons. Firstly, one cannot have dairy within two hours and if itﾃ｢ﾂ�ﾂ冱 taken on an empty stomach it can cause stomach upset. I like to take my meds first thing the morning. I also like a big latte. Not good. So, I tried skipping the latte once and just taking it on an empty stomach. I felt horrible, as if I were going to vomit. Also, itﾃ｢ﾂ�ﾂ冱 such a strong antibiotic that they recommend you not lie down for at least 15 minﾃ｢ﾂ�ﾂｦNo thanks.

Secondly and most importantly, it is photosensitive. I had on factor 50 and was burning after 10 minutes. Normally, a SPF 25 would suffice. It was very difficult to see the sites, even when wearing a hat, while on this medication. Not everyone has the same sensitivity as I did, though, to exposure to sun, so this might work for some.

The third mentioned is Malarone. This happens to be my drug of choice, but it comes at a price. I like it becauseﾃつ�I only have to take this pill once a day, and I can be in direct sunlight with no problems.ﾃつ�Personally, I would recommend Malarone, but know this does not suit everyoneﾃ｢ﾂ�ﾂ冱 budget. When I was in Madagascar, I did a lot of trekking in damp, cool forests. I had on three-quarterﾃつ�length trousers and, as a result, my calves, particularly my left one, was chewed to bits. I didnﾃ｢ﾂ�ﾂ冲 think much of it as I was on Malarone. However, when we reached Tanzania, I knew something was wrong. My body ached, my head ached, my eyesﾃつ�hurt when I looked left or right. I had malaria. Luckily, we told the doctor weﾃ｢ﾂ�ﾂ囘 be gone for four more days than we really were. I was able to double up on the meds and after a few days, I felt much better.
I recommend getting a few extra daysﾃ｢ﾂ�ﾂ� worth of medication, if budget allows.
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-09-11T19:03:16+01:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:25;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:56:"4 reasons you should try a private mobile camping safari";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:98:"http://www.yourafricansafari.com/articles/4-reasons-you-should-try-a-private-mobile-camping-safari";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:413:"A mobile camping safari has many advantages over its luxury lodge competitors, yet many people eschew this incredible experience because they arenﾃ｢ﾂ�ﾂ冲 keen on ﾃ｢ﾂ�ﾂ腕oughing itﾃ｢ﾂ�ﾂ� or on sleeping in a tent. Iﾃ｢ﾂ�ﾂ冦 here to set the record straight, because most mobile camping safaris are far from rough, and they are always the highlight of my time in the bush.

Mobile camping safaris: the ultimate in A...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:98:"http://www.yourafricansafari.com/articles/4-reasons-you-should-try-a-private-mobile-camping-safari";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 15 May 2014 21:48:49 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:4989:"A mobile camping safari has many advantages over its luxury lodge competitors, yet many people eschew this incredible experience because they arenﾃ｢ﾂ�ﾂ冲 keen on ﾃ｢ﾂ�ﾂ腕oughing itﾃ｢ﾂ�ﾂ� or on sleeping in a tent. Iﾃ｢ﾂ�ﾂ冦 here to set the record straight, because most mobile camping safaris are far from rough, and they are always the highlight of my time in the bush.

Mobile camping safaris: the ultimate in African nature

If youﾃ｢ﾂ�ﾂ决e in the processing of planning a safari in Africa, you have an interest in nature and in being a part of it. What better way to enjoy the stellar flora and fauna you traveled so far to see than to completely immerse yourself in it? ﾃつ�A private mobile tent is exactly what it purports to be: a very high-end tent that is set up in a very remote area at a specific time of year to coincide with wildlife migrations.

Mobile camping safaris: the greener choice

What better way to preserve the stellar flora and fauna you traveled so far to see than to use a solar-heated, gravity-fed shower and have limited access to generator-provided electricity? Have I lost you now? Is this pushing it too far? The entire safari doesnﾃ｢ﾂ�ﾂ冲 have to consist of private mobile camping. Even spending only a few nights in a mobile tented camp is enough to give you an authentic safari experience. ﾃつ�It also makes you realize how focused you can be when youﾃ｢ﾂ�ﾂ决e not plugged in, literally, to all the noise of the modern world. I relished the post-dinner part of the safari, particularly during the mobile camping part. With no modern cons to distract us, we found ourselves sat around the campfire each night, recounting our favorite part of the day.

Private mobile camping safaris: the ideal setting for natureﾃ｢ﾂ�ﾂ冱 soundtrack 

As I mentioned earlier, itﾃ｢ﾂ�ﾂ冱 incredible how focused one can be when one isnﾃ｢ﾂ�ﾂ冲 distracted by ancillary, modern noises. Lying in our tent each night in complete and utter darkness, our other senses were heightened. One night we heard the faintest mewing noises off in the distance. Excitedly, weﾃ｢ﾂ�ﾂ囘 turn to each other in the pitch darkness and muse about the source of the noise. The next morning I learned that a Masai spotted a lioness and her three cubs only meters from our tent! Had I been lying in a public camp site, surrounded by numerous other campers, I probably would not have heard this slight noise. On another camping trip, we were awakened to what sounded like a washing machine doing a heavy load. It was a very loud and repetitive noise, which had my other half sitting up straight as a board. I realized quickly that it was a grazing hippo, munching away happily right outside our tent. Needless to say, we didnﾃ｢ﾂ�ﾂ冲 sleep as much as we would have liked that night, but it was also one of the most memorable nights Iﾃ｢ﾂ�ﾂ况e ever spent on safari.
On all mobile tented safaris, come early morning, well before sunrise, weﾃ｢ﾂ�ﾂ囘 be serenaded by hyenas calling to each other. It was simultaneously eerie and beautiful. Then, when we thought it couldnﾃ｢ﾂ�ﾂ冲 get any better, weﾃ｢ﾂ�ﾂ囘 hear the distinct, thunderous roar of the lions. I had tingles throughout my body!

Private mobile tented safaris: the ultimate choice for star gazers

The top highlight of my safaris is, without a doubt, waking up well before sunrise, when the sky is still resplendent with twinkling stars. This is particularly rewarding if youﾃ｢ﾂ�ﾂ决e staying in a tent or in a very small lodge, where the electricity is turned off at night.
I recommend doing this in the early hours for a couple of reasons:



1.ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ� Most people are absolutely exhausted after a full day of game driving and are keen to call it a day as soon as theyﾃ｢ﾂ�ﾂ况e filled their bellies.ﾃつ�

ﾃつ�

2.ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ�ﾃつ� You will probably hear lions roaring, or hyenas whooping, as the morning light begins to fill the sky. This makes for the most wonderful soundtrack to your star gazing!

ﾃつ�

Once up, youﾃ｢ﾂ�ﾂ冤l get to see extra-bright constellations only visible in the southern hemisphere, like Crux, the Southern Crossﾃ｢ﾂ�ﾂ覇xciting stuff for us northerners. The first time I saw the Southern Cross, I thought of Crosby, Stills and Nash, and instinctively knew why Iﾃ｢ﾂ�ﾂ囘 come on safari. It was a magical experience that Iﾃ｢ﾂ�ﾂ况e been fortunate to repeat many times now. Avid stargazers may notice familiar constellations appear ﾃ｢ﾂ�ﾂ�upside downﾃ｢ﾂ�ﾂ�, like Orion the hunter. Famous Betelgeuse is one of the bottom two stars when viewed from below the Equator. And, if youﾃ｢ﾂ�ﾂ决e lucky enough to have your safari coincide with a crescent moon phase, youﾃ｢ﾂ�ﾂ冤l probably notice the crescent orientation is reversed, or if youﾃ｢ﾂ�ﾂ决e close to the equator, that itﾃ｢ﾂ�ﾂ冱 not even a crescent.

So, try to get up very early one morning and turn your eyes towards the heavensﾃ｢ﾂ�ﾂ輩ou wonﾃ｢ﾂ�ﾂ冲 be disappointed. And, please, give private mobile camping a try! Youﾃ｢ﾂ�ﾂ冤l never view the bush the same way again.
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-07-09T18:10:51+01:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:26;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:66:"I want to help the local schools and orphanages: What can I bring?";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:106:"http://www.yourafricansafari.com/articles/i-want-to-help-the-local-schools-and-orphanages-what-can-i-bring";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:403:"Often travel to Africa brings an awareness of the abundance of material resources that we enjoy in our home country. Inevitably the urge rises to donate to those who have less than ourselves, especially the children seen in schools and orphanages. But with that desire to truly help the local African communities better their lives, what donations are going to be most appreciated and beneficial to t...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:106:"http://www.yourafricansafari.com/articles/i-want-to-help-the-local-schools-and-orphanages-what-can-i-bring";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sun, 02 Jun 2013 15:54:16 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:3890:"Often travel to Africa brings an awareness of the abundance of material resources that we enjoy in our home country. Inevitably the urge rises to donate to those who have less than ourselves, especially the children seen in schools and orphanages. But with that desire to truly help the local African communities better their lives, what donations are going to be most appreciated and beneficial to those that we wish to assist?

First be mindful that you do not treat African communities as disaster relief zones or dumping grounds for items that you wouldn't want for yourself. Handing over used clothing may make you feel that someone who needs it will use it gratefully, but it also can set off a destructive cycle of charity and a loss of dignity for the recipient, and the items may not be needed or desired. When donating to a community, it is essential that you keep their needs foremost in your mind. Simply put, the donation needs to be for them and their needs and not for yourself.

Donations should also be sustainable and include community support. Rather than dropping off items to the less fortunate, truly useful giving involves a conversation with the community as to how you can best support their goals. If children see begging as beneficial and effective, they may opt not to attend school. If a community sees the foreigner visitor as a 'giver', they may not invest themselves in projects, making them dependent on aid vs. allowing them to 'own' their own projects.

Books are one item that is often seen as lacking and drives are organized to deliver reading material to those without it. But, what often is delivered are books that have no relevance for the people who are receiving them. A good rule of thumb for durable goods such as clothing and books is to purchase items locally and donate them. This not only prevents you from carrying over large amounts of the wrong items but also supports the local economy by making the purchase within the community. Books may be a heavy item to bring, particularly for those wishing to do an internal flight on safari. Paperback books are definitely more practical for luggage and, while not as durable as hardcover books, will be just as appreciated by the children and teachers.

School supplies are always in need and highly appreciated. While school may be free to attend, parents are left to shoulder expenses for items such as uniforms and simply items like pencils and those costs may prohit school attendance for some children. Please contact the school in advance so that your visit will not disrupt the daily routine and ask what items they would appreciate most. Hand supplies to the head of the school and not to the children directly and make the visit about your interaction with the children, not just the giving of pencils. Electronics may seem to be a need for the communities, but without steady electricity and technicians, these gifts can often not be used. Pens, pencils, notebook and other small items are easy to carry and are always in demand.

Sometimes, you might wish to bring something 'fun' rather than utilitarian, thinking the children must crave toys. Sadly, these toys often quickly end up discarded and while it may not meet our standard of a what to give, practical items are much more valued. Sweets are another thing that may seem like a special treat, but without dental care, the families may not want these either.

Giving to both children and the community at large ultimately needs to be a careful undertaking. We can't stress it enough: always ask first to be sure what you intend is what they need. It may be the last group already brought pencils and they'd much prefer something else. A little bit may go a long way in Africa, but the though behind the gift is even more valuable, especially when it allows true interaction and appreciation of the community and their goals.
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-06-04T20:58:03+01:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:27;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:41:"Air Namibia cancels another three flights";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:83:"http://www.yourafricansafari.com/articles/air-namibia-cancels-another-three-flights";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:399:"People planning to go from Frankfurt to Windhoek via Air Namibia should be aware of the following cancellations :

SW 285 from Windhoek (WHK) to Frankfurt (FRA) ﾃ｢ﾂ�ﾂ� June 25 & June 27

SW 286 from Frankfurt (FRA) to Windhoek (WHK) ﾃ｢ﾂ�ﾂ� June 26

Reason: The maintenance of the Airbus 340-300 takes longer than expected.

History of the past months:

The strike of its pilots in November 2012 b...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:83:"http://www.yourafricansafari.com/articles/air-namibia-cancels-another-three-flights";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 18 Jun 2013 19:27:56 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:1844:"People planning to go from Frankfurt to Windhoek via Air Namibia should be aware of the following cancellations :

SW 285 from Windhoek (WHK) to Frankfurt (FRA) ﾃ｢ﾂ�ﾂ� June 25 & June 27

SW 286 from Frankfurt (FRA) to Windhoek (WHK) ﾃ｢ﾂ�ﾂ� June 26

Reason: The maintenance of the Airbus 340-300 takes longer than expected.

History of the past months:

The strike of its pilots in November 2012 brought a lot of problems to Air Namibia. The Company was not ready to pay more money ﾃ｢ﾂ�ﾂ� so a lot of passengers had to face the problems of cancelled flights. Very often customers had to stay in Windhoek Hotels to wait for another Airline to transport them instead of Air Namibia. At the end the pilots declared the strike off to end the problems for the public flight traffic. Air Namibia ﾃ｢ﾂ�ﾂ忤onﾃ｢ﾂ�ﾂ� the showdown ﾃ｢ﾂ�ﾂ� losing a lot of money.

At the end of 2012 Air Namibia had a big problem to pay its petrol bills to the International Airport of Windhoek. Several times planes on their way to Frankfurt had to land in Luanda (Angola) to get the needed petrol ﾃ｢ﾂ�ﾂ� because they were not allowed to tank in Windhoek anymore.

As Air Namibia is a public enterprise they needed help from the government. In March 2013 it was proclaimed: The government decided to ﾃ｢ﾂ�ﾂ彿nvestﾃ｢ﾂ�ﾂ� 1.1 billion N$ (about 110 million US$) for ﾃ｢ﾂ�ﾂ彗n Update to the business plan and payment of outstanding debtsﾃ｢ﾂ�ﾂ�.

The frequency of flying to Frankfurt was shortened to four times a week. With the 25 th of June Air Namibia wanted to return to the old schedule ﾃ｢ﾂ�ﾂ� but then the maintenance of the Airbus made a problem ﾃ｢ﾂ�ﾂｦ

So keep in mind that currently Air Namibia cannot always keep what it sells. Also take other routes into consideration ﾃ｢ﾂ�ﾂ� like via Johannesburg, or even via Dubai flights of the same price category are to find.

ﾃつ�
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-06-04T20:57:33+01:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:28;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:28:"How good is my safari guide?";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:69:"http://www.yourafricansafari.com/articles/how-good-is-my-safari-guide";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:413:"Below the ever romantic cover of an African safari, there is a thick layer of knowledge, experience, tradition, and adventure.ﾃつ� If you are looking to spot a good and experienced safari guide, or even the industry expertsﾃ｢ﾂ�ﾂ杯he Calvin Cottar and Reed brothers of the industryﾃ｢ﾂ�ﾂ輩ou need to understand the authentic being of a safari guide.ﾃつ� Follow this, and you will find yourself a brilliant safari...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:69:"http://www.yourafricansafari.com/articles/how-good-is-my-safari-guide";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sat, 12 Oct 2013 07:24:11 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:3323:"Below the ever romantic cover of an African safari, there is a thick layer of knowledge, experience, tradition, and adventure.ﾃつ� If you are looking to spot a good and experienced safari guide, or even the industry expertsﾃ｢ﾂ�ﾂ杯he Calvin Cottar and Reed brothers of the industryﾃ｢ﾂ�ﾂ輩ou need to understand the authentic being of a safari guide.ﾃつ� Follow this, and you will find yourself a brilliant safari guide.

Safari guide knowledge

Guides that stand tall above the average, have an in-depth knowledge of their environment.ﾃつ� Not only knowledge of the charismatic and big wildlife, but also the smaller, even unnoticed wildlife species.ﾃつ� The best guides also have a wider understanding of ecosystemsﾃ｢ﾂ�ﾂ派ow each species, whether big or small, contributes to the overall harmony, existence and flourishing of Africaﾃ｢ﾂ�ﾂ冱 ecosystems.

Personality & dedication of your safari guide

Without a personality to entertain people, no guide will attain popularity.ﾃつ� Coupled with their outstanding knowledge, expert guides will have endless tales, stories and secrets to share about the bushﾃ｢ﾂ�ﾂ蚤 real advantage in those quiet hours when animals are nowhere to be found.ﾃつ� But besides a people personality, the best of guides will show their commitment to wildlife conservation.ﾃつ� Conservation and wildlife protection is like a golden thread throughout any good guideﾃ｢ﾂ�ﾂ冱 career and life.

African safaris: culture & tradition

The best of guides have a deep insight into local and African cultures.ﾃつ� Africa cannot be separated from its cultures.ﾃつ� Often, local cultures have preserved their environment through years of sustainable harvesting and hunting.ﾃつ� It is often among the older generation that this man-nature tradition is still observed.ﾃつ� The best of guides will be able to share the ways in which local communities have supported themselves in traditional ways, yet protected the natural resources for future generations.

Sense of adventure coupled with respect

A sense of adventure describes good guides, but for the great guides, this is coupled with a sense of respect for wildlife and acknowledging of their clientsﾃ｢ﾂ�ﾂ� safety.ﾃつ� To be able to handle this intricate balance, the best of guides have a fine sense for interpreting wildlife behavior and are able to react quickly on any warning signs.ﾃつ� No good guide will show any fear for wildlife.ﾃつ� With little if any fear for the bush, guides are able to handle any dangerous situation.ﾃつ� If your guide is a pro, nothing will escape his or her eyes, or other sense that is!ﾃつ� Being perfectly in tune with their environment, and watching every movement, while still entertaining clientsﾃ｢ﾂ�ﾂ杯hat is the best among the best!

Safari guide licenses

Throughout Africa several training institutions, such as the Field Guides Association of Southern Africa, offer accredited safari guide programs.ﾃつ� As an outcome, guides are accredited with professional licenses.ﾃつ� Ask your tour operator or guide about their guide licenses or similar accreditations.

At the bottom of every good safari guideﾃ｢ﾂ�ﾂ冱 heart is a keen observer, someone with a quest to learn and understand Africaﾃ｢ﾂ�ﾂ冱 wildlifeﾃ｢ﾂ�ﾂ蚤nd to share this with others.ﾃつ� So keep this in mind and be able to tell whether your guide is a pro! ﾃつ�
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-03-10T02:09:49+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:29;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:72:"Halal, Kosher, vegetarian and vegan dietary requirements while on safari";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:112:"http://www.yourafricansafari.com/articles/halal-kosher-vegetarian-and-vegan-dietary-requirements-while-on-safari";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:401:"Catering for special dietary needs while on African safari

A commonly asked question about a safari tour is what will the food be like. This is especially important if one is on a special diet, as many times such specifications to the menu are not as common in Africa as at home. The good news is that the majority of special diets on safari can readily be accommodated with advance notice. The ad...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:112:"http://www.yourafricansafari.com/articles/halal-kosher-vegetarian-and-vegan-dietary-requirements-while-on-safari";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 29 May 2013 21:23:29 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:2384:"Catering for special dietary needs while on African safari

A commonly asked question about a safari tour is what will the food be like. This is especially important if one is on a special diet, as many times such specifications to the menu are not as common in Africa as at home. The good news is that the majority of special diets on safari can readily be accommodated with advance notice. The advance notice is important, so that the proper ingredients can be acquired for you, especially in remote locales.

A typical safari menu for a day may look like this, but can vary greatly depending on the luxury level of safari:

Breakfast: Coffee, tea, porridge, fresh fruits (particularly melon, mango, bananas, etc), fruit juice, scrambled eggs/omelet, sausage, toast, margarine, honey, jam.

Lunch: Hot tea, coffee, sandwiches and wraps, fresh soups, local grains and pulses, biscuits, chips, fresh fruits and vegetables.

Dinner: Soup/stew, cooked meat or vegetarian meal (these include chicken/beef/goat with rice or maize, sliced fresh carrots and green beans, mashed potatoes, salads), fruits, fruit juice and variety of hot drinks.

Meals that are served either in a buffet or family style allow you to readily choose the items you wish to eat and skip the rest. For higher-end private tented lodges, you will have a choice of items and can be a bit more fussy. To ensure your dietary needs are met, the best advice is to let the kitchen know a day in advance so they can source any ingredients they may not have on-hand.

Carbohydrates are usually plentiful, but easily avoided. Protein is usually meat, but vegetarian meals can be arranged. Vegan meals are also possible, however you may need to be a bit proactive and educate your hosts on what is included in your diet and what is not. Diabetic meals are also commonly prepared by request.

Food allergies are bit trickier, and if you are allergic to several ingredients, you may wish to carry some supplemental snacks on your person. Depending on country, kosher and halal may not be as commonplace or easy to accommodate. However, in some countries with a large Muslim population, like Tanzania, halal is quite easy to accommodate. If you have a kosher or halal requirement, it is important that you mention this upfront as it may not always be possible on a given itinerary, especially at tented camps and remote lodges.
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-03-01T01:51:42+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:30;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:23:"What to buy in Tanzania";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:65:"http://www.yourafricansafari.com/articles/what-to-buy-in-tanzania";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:407:"Buying Tanzanian souvenirs

Many people who plan a safari in Tanzania will often allocate a day to visiting the markets, so that they can buy some locally-made souvenirs. As with any market, it's important to shop around and to get an understanding of quality. Wood carvings made from ebony woodﾃ｢ﾂ�ﾂ廃articularly animal and people carvings, bowls and candlesticksﾃ｢ﾂ�ﾂ蚤re popular and ubiquitous choices...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:65:"http://www.yourafricansafari.com/articles/what-to-buy-in-tanzania";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 28 Mar 2013 21:45:48 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:1359:"Buying Tanzanian souvenirs

Many people who plan a safari in Tanzania will often allocate a day to visiting the markets, so that they can buy some locally-made souvenirs. As with any market, it's important to shop around and to get an understanding of quality. Wood carvings made from ebony woodﾃ｢ﾂ�ﾂ廃articularly animal and people carvings, bowls and candlesticksﾃ｢ﾂ�ﾂ蚤re popular and ubiquitous choices. Itﾃ｢ﾂ�ﾂ冱 advised to shop with a local, or with someone who is familiar with the various markets, as quality and prices vary considerably from place to place. When in the market, it is common to be called friend ﾃ｢ﾂ�ﾂ腕afikiﾃ｢ﾂ�ﾂ�, and to be summoned into their shop for a browse and chat. In some markets the number of vendors is overwhelming and it can be difficult to remember which store piqued your interest.
It is also possible to shop in stores that have been certified by the government as places that sell authentic, high-quality souvenirs. For the most part, the quality is noticeably higher, but this also comes at a higher price. Haggling is also restricted in these stores and, while you may be able to knock the price down a bit, it will not be anywhere near the amount youﾃ｢ﾂ�ﾂ囘 be able to get discounted in a non-regulated store. Tanzanian Shillings and new US bills are accepted. You may be able to haggle a bit more if paying in USD.
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-03-01T01:42:22+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:31;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:58:"In 2014: a single East African visa for seamless traveling";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:99:"http://www.yourafricansafari.com/articles/in-2014-a-single-east-african-visa-for-seamless-traveling";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:410:"Three East African countries made an important tourist visa announcement at last weekﾃ｢ﾂ�ﾂ冱 World Travel Market.ﾃつ� At this popular tourism marketing event held in London, the three East African neighboring countries of Uganda, Kenya and Rwanda revealed that from January 1, 2014, tourists traveling to these countries will be able to travel effortlessly with the convenience of a single visa.ﾃつ� This si...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:99:"http://www.yourafricansafari.com/articles/in-2014-a-single-east-african-visa-for-seamless-traveling";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 11 Nov 2013 13:54:30 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:3706:"Three East African countries made an important tourist visa announcement at last weekﾃ｢ﾂ�ﾂ冱 World Travel Market.ﾃつ� At this popular tourism marketing event held in London, the three East African neighboring countries of Uganda, Kenya and Rwanda revealed that from January 1, 2014, tourists traveling to these countries will be able to travel effortlessly with the convenience of a single visa.ﾃつ� This single visa will replace the individual country visas that are currently required by these three countries.ﾃつ� This single visa is expected to boost tourism significantly because not only will a single visa do away with much of the current administration hassles, it will also bring about some cost reductions for tourists.ﾃつ�

Becoming tourism friendly

The biggest motivation for these three East African countries to join forces, administration and otherwise, in this single visa collaboration is to open its doors even wider to the international traveler market.ﾃつ� On the cards are multi-destination packages that will offer highlights of all three countries, all included in one travel package.ﾃつ� Kenyaﾃ｢ﾂ�ﾂ冱 President, Uhuru Kenyatta, is reported to have said that the intention is to develop Kenya (and its neighboring countries) as prime destinations, and not merely stopovers on route somewhere else.ﾃつ� These East African countries have further plans to market the region collaboratively, rather than each country aiming for its own tourism agenda.ﾃつ� The planned joint East African visa is a confirmation of the work that has been happeningﾃつ� behind the scenes to offer international tourists even better African experiences and seamless traveling.ﾃつ� For example, in Kenyaﾃ｢ﾂ�ﾂ冱 prominent safari regions, airstrip renovations have been undertaken to not only improve current air traveling but also to offer tourists a greater variety of air travel to choose from.ﾃつ�

What will it cost?

As Africaﾃ｢ﾂ�ﾂ冱 tourism sectors expanded and even blossomed in some regions, it brought about an inevitable increase in travel costs. Currently, international tourists are paying $50.00 for a Kenyan visa, $50.00 for a Ugandan visa and $30.00 for a Rwandan visa.ﾃつ� The combined single visa is expected to cost $100.00, thus a sure cost benefit.

Country attractions

Rwanda with its thousand hills, its special mountain gorilla and bird species among volcanic mounts, offer exciting and fascinating outdoor tourism experiences.ﾃつ� Uganda, or the ﾃ｢ﾂ�ﾂ弃earl of Africaﾃ｢ﾂ�ﾂ� as it is known, offers savanna and Nile activities.ﾃつ� Kenya of course is a safari must-see with all of Africaﾃ｢ﾂ�ﾂ冱 most prominent wildlife species.ﾃつ�

But what about Tanzania?

Initially, Tanzania was involved with the joint visa discussions, but formally announced its withdrawal from the Uganda, Rwanda and Kenyaﾃ｢ﾂ�ﾂ冱 visa collaboration.ﾃつ� In explaining its withdrawal, Tanzaniaﾃ｢ﾂ�ﾂ冱 Department of Natural Resources and Tourism announced that Tanzania will not partake in this agreement until certain financial and security administration have been secured among these East African countries.ﾃつ� With the announcement of the single visa, Tanzania remained unconvinced that security and revenue matters have been thoroughly ground-truthed.ﾃつ� But Kenya was quick to remind that the benefits of both East African countries and international tourists measured far more than Tanzaniaﾃ｢ﾂ�ﾂ冱 concerns.ﾃつ�

The possibility remains that Tanzania may, in the future, re-consider its collaboration in this cross-national visa agreement.ﾃつ� But until the time being, travelers heading to Eastern Africa will surely reap the benefits of this promising East African cross-border collaboration. ﾃつ� ﾃつ�
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-01-06T16:56:00+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:32;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:30:"Luggage restrictions on safari";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:72:"http://www.yourafricansafari.com/articles/luggage-restrictions-on-safari";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:409:"An African safari is one of those times where less is truly more. From my experience, the best type of luggage to bring on safari is something that is soft-sided. Not only will this fit better in the safari vehicle, but itﾃ｢ﾂ�ﾂ冱 also lighter and easier for the porters to carry. Thereﾃ｢ﾂ�ﾂ冱 not much tarmac out in the African wilderness, so wheeled luggage or hard-sided suitcases are impractical and awk...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:72:"http://www.yourafricansafari.com/articles/luggage-restrictions-on-safari";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 15 Apr 2013 20:32:12 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:1715:"An African safari is one of those times where less is truly more. From my experience, the best type of luggage to bring on safari is something that is soft-sided. Not only will this fit better in the safari vehicle, but itﾃ｢ﾂ�ﾂ冱 also lighter and easier for the porters to carry. Thereﾃ｢ﾂ�ﾂ冱 not much tarmac out in the African wilderness, so wheeled luggage or hard-sided suitcases are impractical and awkward to carry. For those who are taking internal flights on safari, you will want to keep it light. Most local airlines restrict luggage weights to 15 or 20 kg max, so give some careful thought to what you really need to bring with you. Chances are youﾃ｢ﾂ�ﾂ冤l also wish to purchase some souvenirs while youﾃ｢ﾂ�ﾂ决e out there, so youﾃ｢ﾂ�ﾂ冤l want to leave some room for those. African safaris are extremely informal, so thereﾃ｢ﾂ�ﾂ冱 absolutely no need for any fancy clothing. Leave any expensive jewelry at home, too. For a two-week safari, one can easily get away with a few pairs of trousers, a couple pair of shorts, some sandals and some hiking boots. For the upper body, layers are essential. I bring a light, cotton scarf that protects my neck from the intense African sun. It also helps keep me warm on the night and early-morning game drives. The drives can be dusty and dirty, so I would keep a few outfits for the game drives and then, once we returned back to the camp for the evening, I would shower and change into my ﾃ｢ﾂ�ﾂ賄innerﾃ｢ﾂ�ﾂ� attire. This was also something very informal, such as a linen shirt and khakis, and I only needed the one outfit for the duration of my safari. Many of the upscale lodges also offer a laundry service for those who are staying more than one night. ﾃつ�

ﾃつ�
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2013-11-05T00:49:50+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:33;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:28:"Safaris with senior citizens";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:70:"http://www.yourafricansafari.com/articles/safaris-with-senior-citizens";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:409:"Planning a family African safari for a special occasion is a great idea. Iﾃ｢ﾂ�ﾂ况e helped plan weddings, birthdays, graduation gifts, honeymoons and anniversaries. I recently helped plan a 50-year wedding anniversary. I was honored to be tasked with such a request and I immediately asked if it was just the lovely couple attending or if others were going. It turns out the coupleﾃ｢ﾂ�ﾂ冱 children were orga...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:70:"http://www.yourafricansafari.com/articles/safaris-with-senior-citizens";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 21 May 2013 23:15:32 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:2453:"Planning a family African safari for a special occasion is a great idea. Iﾃ｢ﾂ�ﾂ况e helped plan weddings, birthdays, graduation gifts, honeymoons and anniversaries. I recently helped plan a 50-year wedding anniversary. I was honored to be tasked with such a request and I immediately asked if it was just the lovely couple attending or if others were going. It turns out the coupleﾃ｢ﾂ�ﾂ冱 children were organizing it and were going to join them. In this instance, I set up two vehicles for them. One for the younger crowd who may wish to do more driving and who may want to get some hikes in, and one for the couple. At first they were reticent about the need for two vehicles, but when I explained the freedom this gave them, they understood. Turns out the two vehicles came in more useful than even I had originally anticipated. On days where some folks wanted to get out early and be in the bush all day, it came in very handy to have the second vehicle, as older and younger folks can tire out more easily. Those who wished to head back during the day for a siesta would go back in one vehicle while the others stayed out.

In another instance, I had a large group of folks in the 60s and up. They only had a few days to spend in northern Tanzania and had quite an aggressive agenda in mind. I recommended they take internal flights to cut back on the amount of driving. Internal flights will add to the price of a safari, but they also free up several hours of time. This is time that could be spent on a game drive or relaxing at camp. The group was divided, with one group adamant that they wanted to drive. So they ended up doing two separate safaris, with one group driving in and out and the other flying. The group that drove said it really did make their journey long and that they wished theyﾃ｢ﾂ�ﾂ囘 done internal flights. Not only did the internal flight group look more relaxed, but they were able to spend more time in the bush.

An African safari is definitely possible for senior citizens or those with mobility issues, but it is advisable that more time is allotted so that they are not rushed. It is also advisable that driving between parks or lodges be kept to a minimum or that the drive be replaced by an internal flight. Drives between parks on dirt roads can be rough and bumpy, and this may be hard to handle for elderly. Internal flights eliminate much of this and free up time that can be spent relaxing or game viewing.ﾃつ�
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2013-09-05T17:39:54+01:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:34;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:34:"Safaris for the disabled traveller";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:76:"http://www.yourafricansafari.com/articles/safaris-for-the-disabled-traveller";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:398:"



Have you ever felt that an African safari is something you wouldn't ever be able to experience due to your own physical limitations? A trip to Africa, complete with a full safari experience, tops the bucket lists of many people; yet for those who have physical challenges, that dream can seem out of reach.
Often a safari is portrayed as a very active endeavor, but the truth is that there a...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:76:"http://www.yourafricansafari.com/articles/safaris-for-the-disabled-traveller";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 27 May 2013 15:14:59 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:4673:"



Have you ever felt that an African safari is something you wouldn't ever be able to experience due to your own physical limitations? A trip to Africa, complete with a full safari experience, tops the bucket lists of many people; yet for those who have physical challenges, that dream can seem out of reach.
Often a safari is portrayed as a very active endeavor, but the truth is that there are many styles of safari and not all safaris have a high physical demand. Many companies now exist that cater to the special needs market and focus specifically on making a safari possible for those who are not able-bodied. These companies sell safaris that are tailored to the special needs traveler such as the mobility challenged but also for those with vision and hearing disabilities and other health issues. An advantage of booking through a tour operator is that you will have in-county support and assistance should anything not go as expected.
Special safari vehicles are equipped with hydraulic lifts and locking systems to easily deal with chair transport and assist anyone with mobility issues. Accomodations are also made for accessible hotel rooms and bathrooms while on safari. Lodging options vary from tented camping to remote rustic lodges to modern bush hotels. When making your choice in where to lodge, ask questions that will help you be sure your needs are covered- how high are the beds, is there power through the night (tented camps may have generators only during the day hours), what are the bathrooms like?
Because not everyone's needs are the same, it is important that you are clear at all times about what you require for your disability and comfort. While your tour operator will convey the information to ground suppliers, always assume you may need to further explain your issues to all personnel you encounter.
Bring any equipment and supplies along and do not expect them to be available in Africa. Be aware that medical services may not be nearby in Africa and may not be to the level that they are at home. If you do require a certain proximity to medical services, this is something to discuss with the tour operator when making your plans so they can take that into account in setting up your arrangements. Always being the right electrical adapaters for any neccesary equipment. Bring copies of your prescriptions and any special instructions from your doctor. Always pack medications and necessary supplies in your carryon bag and not your checked luggage. Be aware that not everything operates on Western schedules in Africa and carry what you need on your person.
Days on safari start and end early. You'll want to be up for the dawn game drive after which there will be a hearty meal and an afternoon rest period. Evening will bring another game drive, and it's usually 'lights out' as soon as it's fully dark. Being prepared for this schedule can assist you in both your enjoyment of the safari experience as well as your comfort.
There are many additional resources on the the internet that relate to African travel and safaris for disabled travellers:
Able Travel - http://www.able-travel.com/index.php Able Travel has specific information for Kenya, Tanzania, South Africa, Botswana, Namibia and Zambia, as well as general disabled travel tips and first-hand accounts from those who have gone on safaris as disabled travelers.

Disabled World ﾃ｢ﾂ�ﾂ� African Safari Travel Tips http://www.disabled-world.com/travel/africa/african-safari-tips.php This site contains general travel tips as well as traveller stories and a list of tour operators.

East Africa Shuttles ﾃ｢ﾂ�ﾂ� Kenya Wildlife Wheelchair Safari http://www.eastafricashuttles.com/kenyasafaridisabled/handicappedsafaris.htm Here you can find a sample African safari itinerary and description of accommodations.

Emerging Horizons. http://emerginghorizons.com/ This site features travel tips relating to accessibility around the world.

Endeavor Safaris. http://www.endeavour-safaris.com/safari-bus.html Endeavor Safaris is a leader in disabled safaris for many types of special needs.

Gimp on the Go. http://www.gimponthego.com/index.htm Gimp on the Go is disabled travel publication and forum.

Mobility International USA. http://www.miusa.org/ncde/tipsheets/powerchairs This is a guide for all things electrical for international travel.

Victoria Safaris ﾃ｢ﾂ�ﾂ� Disability Tours. http://www.victoriasafaris.com/africa/disabilitytours.htm This site includes general information and links to their own tours.

World on Wheelz. http://worldonwheelz.com/ World on Wheelz has both group and independent travel options. Find sample itineraries here for Africa and beyond.
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2013-09-05T17:24:16+01:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:35;a:6:{s:4:"data";s:8:"







";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:37:"Adventure safaris for active people		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:77:"http://www.yourafricansafari.com/articles/adventure-safaris-for-active-people";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:406:"An African safari often evokes images of long journeys in large 4x4s. While game drives are certainly a quintessential feature to most safaris, they donﾃ｢ﾂ�ﾂ冲 have to be the only feature. Many game parks allow for walks or hikes, provided they are with a safari ranger. These can be an excellent way to not only get some exercise, but to also to get out and explore the environment. It is true that aft...";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:77:"http://www.yourafricansafari.com/articles/adventure-safaris-for-active-people";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:4:"true";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 22 May 2013 00:16:42 -0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:2753:"An African safari often evokes images of long journeys in large 4x4s. While game drives are certainly a quintessential feature to most safaris, they donﾃ｢ﾂ�ﾂ冲 have to be the only feature. Many game parks allow for walks or hikes, provided they are with a safari ranger. These can be an excellent way to not only get some exercise, but to also to get out and explore the environment. It is true that after a long day in a safari vehicle, watching the wildlife from a safe distance, one begins to feel a bit removed from nature. A walk is a great way to do this and is highly recommended in areas that allow it. It doesnﾃ｢ﾂ�ﾂ冲 just stop at hikes; itﾃ｢ﾂ�ﾂ冱 also possible to do your wildlife viewing from a boat or canoe.
Itﾃ｢ﾂ�ﾂ冱 important to let your tour operator know, at the time of booking, that you are seeking a high level of activity on your safari. If they have proper notice, they can work on incorporating activities into your daily game drives. In Tanzania, for example, itﾃ｢ﾂ�ﾂ冱 possible to schedule a half-day or a day of hunting with local tribesmen. Night hikes up active volcanoes are also a possibility. The walk is steep and strenuous and is done at night as the glowing lava is more dramatic. Itﾃ｢ﾂ�ﾂ冱 also planned so that guests reach the summit an hour or so before sunrise and are able to witness a gorgeous African sunrise from an amazing vantage point.

Game drives arenﾃ｢ﾂ�ﾂ冲 applicable for some parks and require you to trek in. Those wishing to see primates, for example, will need to bring sturdy shoes and be prepared to trek for hours to see these magnificent creatures. These treks can be strenuous in nature and often will be on moderate to steep inclines. Trekking safaris are perfect for those wishing to get up close to the local fauna and flora. ﾃつ�Other countries, such as Namibia and Botswana, offer ample hiking opportunities in many of their parks.

Active safari add-ons

Adding on a few days of adventure to your safari is also an option. It is not uncommon to see an itinerary with 5-7 days of game driving flanked by a day of golf or mountain biking. For parks nearer to the coast, itﾃ｢ﾂ�ﾂ冱 also common to see SCUBA and snorkeling packages that can be purchased in addition to the game viewing. A very popular and adventurous addition is a climb on Kilimanjaro. The vast majority of safari companies operating in Tanzania will offer a Kilimanjaro climb addendum. It is recommended that visitors do the climb before the safari, so that they enough energy to summit.

There are a wide range of options for those wishing to do more than enjoy a game drive. Itﾃ｢ﾂ�ﾂ冱 important to choose your safari company carefully and, once you have chosen them, to communicate clearly your activity-level expectations
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:25:"http://purl.org/dc/terms/";a:1:{s:8:"modified";a:1:{i:0;a:5:{s:4:"data";s:25:"2013-08-18T23:09:19+01:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}s:27:"http://www.w3.org/2005/Atom";a:1:{s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:4:"href";s:66:"http://www.yourafricansafari.com/rss/articles/planning-your-safari";s:3:"rel";s:4:"self";s:4:"type";s:19:"application/rss+xml";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}}}}}}s:4:"type";i:128;s:7:"headers";a:11:{s:4:"date";s:29:"Tue, 09 Aug 2016 09:10:20 GMT";s:12:"content-type";s:34:"application/rss+xml; charset=UTF-8";s:14:"content-length";s:6:"169582";s:10:"connection";s:5:"close";s:10:"set-cookie";a:2:{i:0;s:138:"__cfduid=d943cffa268dfeba0e6ee2d7064fd38c31470733819; expires=Wed, 09-Aug-17 09:10:19 GMT; path=/; domain=.yourafricansafari.com; HttpOnly";i:1;s:64:"currency_code=USD; path=/; expires=Wed, 09-Aug-2017 09:10:20 GMT";}s:10:"x-catalyst";s:7:"5.90076";s:7:"expires";s:29:"Tue, 09 Aug 2016 09:10:19 GMT";s:13:"cache-control";a:2:{i:0;s:8:"no-cache";i:1;s:44:"no-cache, must-revalidate, private, no-store";}s:6:"pragma";s:8:"no-cache";s:6:"server";s:16:"cloudflare-nginx";s:6:"cf-ray";s:20:"2cfa1305852f050e-SEA";}s:5:"build";s:14:"20160809071328";}}