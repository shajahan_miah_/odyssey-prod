��W<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:6560;s:11:"post_author";s:2:"19";s:9:"post_date";s:19:"2016-03-27 12:04:43";s:13:"post_date_gmt";s:19:"2016-03-27 16:04:43";s:12:"post_content";s:1867:"<div class="full-blog-post">

<strong><img class="attachment-post-thumbnail wp-post-image" src="https://www.safaribookings.com/blog/wp-content/uploads/2015/12/The-Owl_BW_650px.jpg" alt="The Owl" width="650" height="350" /></strong>

<strong>This week Alan takes a close look at one of Africa’s most fearsome, little seen predators. One that strikes with military precision.</strong>

I can remember being at a birds of prey exhibition. They are quite common in Africa. You often see eagles, hawks and falcons flying and dive-bombing through the air with extraordinary manoeuvrability. These birds literally fly through hoops to eventually swoop on a morsel of meat.

<strong>The Silent Hunter</strong>
I was picked out of the crowd to stand in between two trainers. One held a Barn owl, the other some meat. I was exactly in the middle and was told to close my eyes and stand still. Hmmm…trusting aren’t I?

The owl flew straight past me, literally inches above my head, and I did not hear or feel it at all. They did the demonstration twice and I had no idea the owl was so close on either occasion. The second time I was really trying to sense it too. Stealth and owls are synonymous.

Nocturnal predators in Africa are often epitomised by the rippling power of a lion, the raw muscle of a leopard, or the skulking cunning of a hyena. But few can challenge owls: the great hunters of the night sky.

<strong>Silent Assassins</strong>
Owls are just so well adapted to their environment. These silent assassins are a picture of evolutionary beauty. Huge eyes give excellent night vision, but their hearing is so good that they can triangulate the slightest tell-tale sound of movement. They actually form a 3D map in their head. An owl can hunt very successfully in total <a href="https://www.safaribookings.com/blog/325">Read More</a>

</div>";s:10:"post_title";s:55:"Africa’s most fearsome, little seen predator: The Owl";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:41:"africas-fearsome-little-seen-predator-owl";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2016-07-26 15:57:04";s:17:"post_modified_gmt";s:19:"2016-07-26 19:57:04";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:63:"https://www.odysseysafaris.com/?post_type=blog_post&#038;p=6560";s:10:"menu_order";i:0;s:9:"post_type";s:9:"blog_post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}