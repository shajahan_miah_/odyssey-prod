��W<?php exit; ?>a:1:{s:7:"content";a:1:{i:0;O:8:"stdClass":4:{s:7:"meta_id";s:5:"21599";s:7:"post_id";s:4:"6431";s:8:"meta_key";s:13:"tours_program";s:10:"meta_value";s:19757:"a:13:{i:0;a:2:{s:5:"title";s:49:"Day 1:   Cape Town airport ‐ Cape Town (29 kms)";s:4:"desc";s:2037:" Meet and greet at Cape Town International Airport. This personalised service sees your guide meet you off your flight. While you
enjoy a complimentary beverage, our guide will explain your itinerary in detail, give you general advice and information that will
help you along your trip. Before handing over your travel documents and escorting you to your onward transportation, our guide
will assist you with any queries you may have before you embark on your holiday.
 
‐ Half day Cape Town city tour. Be captivated by the multiethnic character and historical wealth of the Mother City. Cape Town is a
rare  cultural  gem,  resulting  from  the  amalgamation  of  different  nationalities  and  indigenous  tribes.  Alongside  high‐rise  office
blocks, a harmonious blend of architectural styles has been meticulously preserved, including Edwardian, Victorian and Cape Dutch.
Narrow, cobblestone streets and the Islamic character of the Bo‐Kaap enhance the cosmopolitan ambiance. The itinerary takes you
to the City Hall, the Castle of Good Hope – this pentagon‐shaped fort is the oldest surviving building in South Africa; the Company's
Garden – a large public park, originally Jan van Riebeeck's vegetable garden, which he grew to feed the original colony as early as
1652; and Signal Hill with spectacular panoramas over the city.
 
‐ Take the cableway up Table Mountain. In 2011, Table Mountain was named among the New7Wonders of Nature. You can get to
the top of Cape Town's most famous icon in just five minutes by taking a cable car up. The Aerial Cableway, established in 1929,
takes visitors to the top in one of two cable cars, each with rotating floors and huge windows to ensure your views while travelling
are almost as spectacular as those on the summit. Cable cars depart every 10 to 15 minutes. The Cableway operates only when
weather permits and does not take bookings.
 
‐ Dinner: The Capetonian Hotel
 
‐ Night: The Capetonian Hotel (Superior Room ‐ Dinner, Bed and Breakfast)";}i:1;a:2:{s:5:"title";s:17:"Day 2:  Cape Town";s:4:"desc";s:709:" Breakfast
 
‐ Full day Cape Peninsula tour. Discover both the Atlantic and Indian Ocean coastlines. The first stop is at the charming fishing village
of Hout Bay. Then continue along the Atlantic coastline to the Cape of Good Hope Nature Reserve and Cape Point – the most south‐
westerly tip of Africa and the mythical meeting place of two great oceans. On the return trip, pass through the naval town of
Simon's Town and then traverse the fishing port of Kalk Bay and the lush suburb of Constantia. The day will end with a visit to the
renowned Kirstenbosch Botanical Gardens.
 
‐ Dinner: The Capetonian Hotel
 
‐ Night: The Capetonian Hotel (Superior Room ‐ Dinner, Bed and Breakfast)";}i:2;a:2:{s:5:"title";s:18:"Day 3:   Cape Town";s:4:"desc";s:773:"‐ Breakfast
 
‐ Full day Cape Wine Route tour. Learn everything there is to know about wines, from the vineyard to the bottle. The Cape Wine
Route is defined by the triangle that forms the towns of Stellenbosch, Paarl and Franschhoek. Stellenbosch is a charming old town
with oak‐shaded avenues and historical buildings. Franschhoek owes its name to the French Huguenots, who came to settle there
in 1685 and planted the first quality vineyards, while Paarl has been a centre for the Afrikaner culture since the 19th century. Along
the Route, wine estates welcome you to taste wines and cheeses and invite you to learn how the wines are made.
 
‐ Dinner: The Capetonian Hotel
 
‐ Night: The Capetonian Hotel (Superior Room ‐ Dinner, Bed and Breakfast)";}i:3;a:2:{s:5:"title";s:37:"Day 4:  Cape Town ‐ Durban (33 kms)";s:4:"desc";s:1512:"‐ Breakfast
 
‐ Take your time to explore the Victoria and Alfred Waterfront. The V&A Waterfront is situated on South Africa’s oldest working
harbour in the centre of Cape Town and offers over 450 retail outlets selling everything from high‐end fashion and jewellery to food
and crafts. It is one of Cape Town’s most popular destinations, attracting in excess of 23 million visitors a year. Besides offering
vibrant and chic indoor and outdoor shopping and restaurants there is the added bonus of watching fishing boats dock with fresh
fish or see a container ship being towed into the harbour by a tug boat. Draw‐ bridges over the water open and close every so often
to allow smaller sailing vessels access to the docks, while the Cape Wheel gives unsurpassed 360 degree views of the city, and the
outdoor amphitheatre plays host to music, dance and theatrical performances throughout the year. There is also a variety of other
activities on offer ranging from historical walking tours to air charters and kids clubs.
 
‐ Lunch at own expense
 
‐ Transfer from Cape Town to Cape Town airport
 
‐ Flight CapeTown ‐ Durban (Economy Class) (Optional rates: Flight CPT ‐ Durban )
 
‐ Meet and Greet at the airport.
 
‐ Minibus with Guide Overland : service starts at Durban airport and ends at Johannesburg airport (D11 13/03/16) ‐ 8 days
 
‐ Dinner: The Balmoral Hotel
 
‐ Night: The Balmoral Hotel (Standard Non Sea Facing Room ‐ Dinner, Bed and Breakfast)";}i:4;a:2:{s:5:"title";s:27:"Day 5:  Durban ‐ Hluhluwe";s:4:"desc";s:2776:" Breakfast
 
‐ Discover Durban's “Golden Mile”. This six kilometre long stretch of sandy beach starts roughly at South Beach and uShaka Marine
World and ends at the Suncoast Casino and Entertainment World to the north. The Golden Mile is also a huge pleasure resort with
dozens of swimming and splash pools, fountains and waterslides, curio markets and merry‐go‐rounds, uShaka Marine World, exotic
restaurants, nightclubs and many hotels and apartment complexes directly at the beach. Swimming is good all year round due to
the warm, subtropical climate, and the beaches are well monitored and protected by shark nets. An unusual feature at the Golden
Mile is the Zulu Rikshas. The vehicles and their drivers are colourfully decorated. For a small fee, you can let yourself be pulled along
the beach promenade.
 
‐ Lunch
 
‐ Visit to DumaZulu Cultural Village. Experience traditional hospitality and learn about Zulu life at this informative and authentic
cultural village in Hluhluwe, KwaZulu‐Natal. The village is a living museum of traditional Zulu customs, crafts and way of life where
you can feel the beat of the Zulu drums and discover the rich cultural significance of the tribe's famous beadwork, weaving and
pottery. Zulu social values are integrated into their arts and crafts and their artefacts, besides being beautifully made, also hold
strong cultural significance. The village is the largest in South Africa and not only affords guests the opportunity to learn about the
rich history of tribal Africa but also serves as a means for the local community to continue practicing their rural ways of living as
well as earning an income from selling their crafts. Other interesting activities on offer include tastings of locally crafted beer and
dancing demonstrations.
 
‐ Afternoon 4x4 safari in the Hluhluwe reserve. For Big Five game viewing in extremely scenic surroundings, Hluhluwe Game Reserve
is hard to match. Proclaimed in 1897, This KwaZulu‐Natal game reserve north of Durban is one of the oldest in Africa. Hluhluwe
Game Reserve is world renowned for its role in Rhino conservation and the Hluhluwe‐Imfolozi Centenary Capture Centre sets a
benchmark for animal capture and sustainable utilization throughout Africa. This KwaZulu‐Natal Game Reserve stretches over 96
000 hectares of hilly topography and contains a diverse range of fauna and flora. This area was once the exclusive hunting ground
of  King  Shaka.  View  the  Big  Five  in  Hluhluwe  Game  Reserve  as  well  as  several  more  elusive  animals,  such  Wild  Dog,  Giraffe,
Cheetah and Nyala.
 
‐ Dinner: Zulu Nyala Heritage Safari Lodge
 
‐ Night: Zulu Nyala Heritage Safari Lodge (Standard Room ‐ Dinner, Bed and Breakfast)";}i:5;a:2:{s:5:"title";s:29:"Day 6: Hluhluwe ‐ Swaziland";s:4:"desc";s:1286:"Breakfast
 
‐ The plains in the south‐western part of Swaziland contrast sharply with the mountainous landscapes of the north. You will discover
lush sugarcane fields alternating with bush savannah dotted with Swazi kraals. Near the southern border post of Lavumisa, the
Lubombo Mountains are very apparent from the road. This mountain range covers practically the entire eastern side of the country
and forms the natural border between Swaziland and Mozambique.
 
‐ Lunch Menu 2
 
‐ Visit  the  Swazi  candles  at  Malkerns.  On  weekdays  you  will  be  able  to  view  the  craftspeople  moulding  the  colourful  wax  into
fascinating  shapes  and  sizes.  These  delightful  candles  are  exported  across  the  world  and  have  become  very  popular  as  Swazi
mementos for friends and family back home.
 
‐ Discover  the  Manzini  market.  Typical  of  an  African  market  place,  you  will  find  it  buzzing  with  activity  during  the  daytime;  its colourful stalls offering anything from fruit to traditional medicine and displaying an interesting mixture of traditional and modern
customs.The market is closed on sundays and puyblic holidays.
 
‐ Dinner: Mantenga Lodge
 
‐ Night: Mantenga Lodge (Sheba Room ‐ Dinner, Bed and Breakfast)";}i:6;a:2:{s:5:"title";s:28:"Day 7:  Swaziland ‐ Kruger";s:4:"desc";s:549:"‐ Breakfast
 
‐ Visit  the  Ngwenya  Glass  factory.  Aside  from  providing  charming  glass  souvenirs  and  glassware,  this  enterprise  also  has  an
important social function. Entrepreneurs ranging from school children to adults collect discarded glass objects and bring them to
the factory in return for payment. An interesting way of combining environmental care with social upliftment!
 
‐ Lunch
 
‐ Dinner: Hippo Hollow Country Estate
 
‐ Night: Hippo Hollow Country Estate (Standard Room ‐ Dinner, Bed and Breakfast)";}i:7;a:2:{s:5:"title";s:36:"Day 8:  Kruger ‐ Kruger (Karongwe)";s:4:"desc";s:1604:" Breakfast
 
‐ Arrival in Karongwe Private Game reserve, in the heart of the Limpopo. It lies adjacent to the Greater Makalali Private Game
Reserve just 40 minutes due east of the Kruger National Park. This is the land of combretum and acacia thornveld, a land where
bushveld reigns supreme and the city skyline is a thing only of the imagination; where sightings of major mammals such as giraffe,
wildebeest, rhino and zebra are regular, and night visits by aardvark, bush babies, genets and porcupines a reality. The night skies
are dark, peppered by a myriad stars, the Milky Way, and the calls of nocturnal birds. This is the wild as portrayed in postcards.
Karongwe Game reserve lies in the heart of the bushveld, given over to the conservation of what is left of South Africa's wild
animals. Witness dramatic sunsets ‐ a time in the bush where the wild begin to stir, where drama is doubtless brewing, and where
sightings of game is guaranteed, whilst glorious views of the Drakensberg and Murchison Mountains litter the horizon with gentle
undulations and create gorgeous backdrops to your experience. Within Karongwe are numerous nature reserves, lodges, safaris
and trails. You can look forward to an encounter with over twenty different plains game species and the Big Five (buffalo, leopard,
elephan, lion and rhino). (Activities included in your package as per program below)
 
‐ Lunch
 
‐ Afternoon 4x4 Game Drive
 
‐ Dinner: Karongwe River Lodge (ex Edeni River Lodge)
 
‐ Night: Karongwe River Lodge (ex Edeni River Lodge) (Luxury Suite ‐ Fully Inclusive)";}i:8;a:2:{s:5:"title";s:42:"Day 9:  Kruger (Karongwe) ‐ Johannesburg";s:4:"desc";s:2456:"‐ Morning 4x4 game drive.
 
‐ Breakfast
 
‐ The Blyde River Canyon is the third deepest canyon in the world (after the Grand Canyon in the western U.S. and Namibia's Fish
River Canyon). It ranks as one of the most spectacular sights in Africa. The mountain scenery and panoramic views over the Klein
Drakensberg escarpment are quite spectacular and give the area its name of the Panorama Route. Viewpoints are named for the
vistas they offer, and God's Window and Wonder View hint at the magnitude of the scenery. The 'Pinnacle' is a single quartzite
column  rising  out  of  the  deep  wooded  canyon  and  the  Three  Rondavels  (also  called  'Three  Sisters')  are  three  huge  spirals  of
dolomite rock rising out of the far wall of the Blyde River Canyon. The entire canyon extends over twenty kilometres in length.
 
‐ Lunch
 
‐ Visit  Pilgrim's  Rest.  The  village  is  situated  on  the  magnificent  Panorama  Route  on  the  eastern  escarpment  region  of  the
Mpumalanga province. The entire town of Pilgrim's Rest has been declared a national monument in 1986. Here, visitors can relive
the days of the old Transvaal gold rush. Pilgrim's Rest was declared a gold field in 1873, soon after digger Alec "Wheelbarrow"
Patterson discovered gold deposits in Pilgrim's Creek. The Valley proved to be rich in gold and by the end of the year, there were
about 1500 diggers working in the area. As a result, Pilgrim's Rest became a social centre of the diggings. There is plenty to do and
experience here, from browsing through exciting curio and craft shops to discovering fascinating historical sights.
 
‐ Travel to Johannesburg
 
‐ Dinner: Carnivore Restaurant
A franchise of the world‐renowned Carnivore restaurant in Nairobi, Kenya, the restaurant is located in some natural surroundings
overlooking the Krugersdorp hills. Its main attraction is a large circular open fire with 52 converted Masaai tribal spears skewed
with a variety of meat, such as pork, lamb, beef, chicken, ribs and sausages, and also crocodile, zebra, giraffe, impala, ostrich and
kudu, roasting slowly and waiting for you to begin your feast! Waiters walk from the fire to your table holding spears skewed with
the meat and carve the cuts of your choice directly onto your plate until you have eaten your fill. Surely Africa's Greatest Eating
Experience.
 
‐ Night: Misty Hills (Delux Room ‐ Bed and Breakfast (Ger/Fre))";}i:9;a:2:{s:5:"title";s:45:"Day 10: Johannesburg ‐ Johannesburg airport";s:4:"desc";s:2187:"‐ Breakfast
 
‐ Visit the Apartheid Museum, dedicated to recounting the history of the rise and fall of apartheid in 20th century South Africa.
Interactive  multimedia  displays  recreate  the  experience  of  racial  segregation  with  exhibits  dedicated  to  historical  facts
accompanied by personal testimony : harrowing, unforgettable and uniquely South African. (No Guided tours on Mondays)
 
‐ Visit Soweto. Cursed child of apartheid, Johannesburg's South Western Township today comprises close to 4 million inhabitants.
The revolts of '76 that took place there marked the beginning of the decline of the segregation policy.
 
‐ Buffet lunch: Sakhumzi Restaurant 
Sakhumzi Restaurant is nestled in the heart of Soweto, in Orlando west's famous Vilakazi Street. The Street has a rich historical
background  that  boasts  two  Nobel  prize  winners:  Archbishop  Desmond  Mpilo  Tutu  and  former  president  Nelson  Mandela.
Previously  a  "Shebeen",  illegal  bar  during  Apartheid,  Sakhumzi  is  now  a  traditional  local  restaurant  that  specializes  in  typical
Sowetan cuisine. The buffet lunch ranges from pap(corn flour), rice, dumplings, vegetables, grilled chicken, stews and curries to mogodu (tripe).
 
‐ Orientation tour of Pretoria, South Africa's capital. The city was founded in 1855 and became the capital of the Boer state, Zuid
Afrikaanse Republiek, 5 years later. Many reminders of the history of the Boers can still be seen whilst driving around the city; such
as  Church  Square  with  its  imposing  statue  of  Uncle  Paul  Kruger  ‐  ex‐president  of  the  ZAR  and  much  loved  Boer  leader.  The
Voortrekker Monument, rising like a sentinel in the south of Pretoria, is an essential piece of the puzzle in understanding the
Afrikaner people. The Union Buildings, which dominate the western side of the city, form the official seat of the South African
government, and feature a nine‐metre‐tall statue of the former president of South Africa, Nelson Mandela, erected shortly after his
death on 5 December 2013.
 
‐ Dinner at own expense
 
‐ Night: Peermont Metcourt (Classic Room ‐ Bed and Breakfast)";}i:10;a:2:{s:5:"title";s:47:"Day 11: Johannesburg airport ‐ Victoria Falls";s:4:"desc";s:922:" Breakfast
 
‐ Flight Johannesburg ‐ Victoria Falls (Eco Class) One way (Optional rates: Flight JNB ‐ VFA)
 
‐ Meet and Greet at the Victoria Falls Airport. Upon your arrival you will be welcomed by our guide. Before escorting you to your
onward  transportation  our  guide  will  hand  over  your  travel  documents  and  explain  in  detail  all  your  trip  itinerary  and
arrangements as well as provide you with all the advise and information you might need during your journey.
 
‐ Transfer
 
‐ Boat trip on the Zambezi with the setting sun. Your vehicle will take you to the pier upstream from the falls, then you will tranquilly
drift along and watch the animals come and quench their thirst on the riverbanks. Aperitifs and snacks will be served on board.
 
‐ Dinner: Rainbow Hotel Victoria Falls
 
‐ Night: Rainbow Hotel Victoria Falls (Standard Room ‐ Dinner, Bed and Breakfast)";}i:11;a:2:{s:5:"title";s:22:"Day 12: Victoria Falls";s:4:"desc";s:982:"‐ Breakfast
 
‐ Day trip to Chobe. This tour departs daily to the Chobe National Park in Botswana. You will be transferred to the Kazungula border
where you will meet your Botswana Guide. From here you will proceed straight to the River where you will spend the morning
game viewing along the Chobe River. Tea, coffee & biscuits are provided. The morning cruise ends at around 12h30 and you are
then taken to a hotel on the riverbank for lunch. After lunch, you will board safari vehicles for an afternoon game drive in the Chobe
National Park where you will have the opportunity to enjoy an abundance of wildlife. After the game drive, you are taken back to
the  Kazungula  Border  where  you  will  be  met  by  the  Zimbabwean  Guide  for  your  return  transfer  to  Victoria  Falls,  arriving  at
approximately 18h30
 
‐ Lunch
 
‐ Dinner: Rainbow Hotel Victoria Falls
 
‐ Night: Rainbow Hotel Victoria Falls (Standard Room ‐ Dinner, Bed and Breakfast)";}i:12;a:2:{s:5:"title";s:47:"Day 13: Victoria Falls ‐ Johannesburg airport";s:4:"desc";s:853:"‐ Breakfast
 
‐ Visit of the falls on the Zimbabwe side. A 1,7 km long curtain falls down from a cliff with a height of 108 m at the deepest.
Discovered by Livingstone in 1855 (known to the local people for much longer), the numerous viewpoints count among the most
exceptional on the planet. A show that justifies crossing the continent for.
 
‐ Transfer
 
‐ Flight Victoria Falls ‐ Johannesburg (Economy Class) (Optional rates: Flight ‐ VFA ‐ JNB)
 
‐ End of our services
Note: Distances indicated in the itinerary are calculated from the GPS coordinates for each service included in the itinerary, they are given as
an indication. The photographs displayed in the itinerary serve as general illustration of the destination and providers only and do not
constitute a contractually binding description of specific services.";}}";}}}