��W<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:110;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2014-11-21 08:58:24";s:13:"post_date_gmt";s:19:"2014-11-21 08:58:24";s:12:"post_content";s:14709:"All Form elements based on <a href="http://getbootstrap.com">Bootstrap 3</a>. Check out form elements <a href="http://getbootstrap.com/css/#forms">page</a> for more demos
<div class="gap gap-small"></div>

<hr />

<h2>Basic demo</h2>
<div class="row row-wrap">
<div class="col-md-4">
<h4>Large Size</h4>
<form>
<div class="form-group form-group-lg"><label>Email address</label>
<input class="form-control" type="email" placeholder="Enter email" /></div>
<div class="form-group form-group-lg"><label>Password</label>
<input class="form-control" type="password" placeholder="Password" /></div>
<div class="checkbox"><label>
<input type="checkbox" />Check me out</label></div>
<input class="btn btn-primary" type="submit" value="Log In" />

</form></div>
<div class="col-md-4">
<h4>Normal Size</h4>
<form>
<div class="form-group"><label>Email address</label>
<input class="form-control" type="email" placeholder="Enter email" /></div>
<div class="form-group"><label>Password</label>
<input class="form-control" type="password" placeholder="Password" /></div>
<div class="checkbox"><label>
<input type="checkbox" />Check me out</label></div>
<input class="btn btn-primary" type="submit" value="Log In" />

</form></div>
<div class="col-md-4">
<h4>Small Size</h4>
<form>
<div class="form-group form-group-sm"><label>Email address</label>
<input class="form-control" type="email" placeholder="Enter email" /></div>
<div class="form-group form-group-sm"><label>Password</label>
<input class="form-control" type="password" placeholder="Password" /></div>
<div class="checkbox"><label>
<input type="checkbox" />Check me out</label></div>
<input class="btn btn-primary" type="submit" value="Log In" />

</form></div>
</div>

<hr />

<h2>Advanced Input Effects</h2>
<div class="row row-wrap">
<div class="col-md-4">
<div class="form-group form-group-label-highlight"><label>Label Highlight</label>
<input class="form-control" type="text" placeholder="Write something" /></div>
<div class="form-group form-group-label-anim"><label>Label Animation</label>
<input class="form-control" type="text" placeholder="Write something" /></div>
</div>
<div class="col-md-8">
<div class="row">
<div class="col-md-6">
<div class="form-group form-group-icon-left"><i class="fa fa-star input-icon"></i><label>Left Input Icon</label><input class="form-control" type="text" placeholder="Write something" /></div>
<div class="form-group form-group-icon-left"><i class="fa fa-rocket input-icon input-icon-hightlight"></i><label>Icon Hightlight</label><input class="form-control" type="text" placeholder="Write something" /></div>
<div class="form-group form-group-icon-left"><i class="fa fa-gear input-icon input-icon-bounce"></i><label>Icon Bounce</label><input class="form-control" type="text" placeholder="Write something" /></div>
<div class="form-group form-group-icon-left"><i class="fa fa-retweet input-icon input-icon-swing"></i><label>Icon Swing</label><input class="form-control" type="text" placeholder="Write something" /></div>
</div>
<div class="col-md-6">
<div class="form-group form-group-icon-right"><i class="fa fa-map-marker input-icon"></i><label>Left Input Icon</label><input class="form-control" type="text" placeholder="Write something" /></div>
<div class="form-group form-group-icon-left"><i class="fa fa-user input-icon input-icon-show"></i><label>Icon Show</label><input class="form-control" type="text" placeholder="Write something" /></div>
<div class="form-group form-group-icon-left"><i class="fa fa-search input-icon input-icon-tada"></i><label>Icon Tada</label><input class="form-control" type="text" placeholder="Write something" /></div>
<div class="form-group form-group-icon-left"><i class="fa fa-plane input-icon input-icon-shake"></i><label>Icon Shake</label><input class="form-control" type="text" placeholder="Write something" /></div>
</div>
</div>
You can use any icon gliph from <a href="#">fontawesome</a> or custom <a href="#">iconmoon</a> icon font.

You also can use icon effects with any input size (normal, large, small)

</div>
</div>

<hr />

<h2>Advanced Checkboxes and Radios</h2>
<div class="row row-wrap">
<div class="col-md-4">
<h5>Checkboxes</h5>
<div class="checkbox"><label>
<input class="i-check" type="checkbox" />Checkbox 1</label></div>
<div class="checkbox"><label>
<input class="i-check" type="checkbox" />Checkbox 2</label></div>
<div class="checkbox"><label>
<input class="i-check" disabled="disabled" type="checkbox" />Disabled</label></div>
<div class="checkbox"><label>
<input class="i-check" checked="checked" disabled="disabled" type="checkbox" />Disabled &amp; checked</label></div>
</div>
<div class="col-md-4">
<h5>Radios</h5>
<div class="radio"><label>
<input class="i-radio" name="ratioOption" type="radio" />Radio 1</label></div>
<div class="radio"><label>
<input class="i-radio" name="ratioOption" type="radio" />Radio 2</label></div>
<div class="radio"><label>
<input class="i-radio" disabled="disabled" name="disabledRadio" type="radio" />Disabled</label></div>
<div class="radio"><label>
<input class="i-radio" checked="checked" disabled="disabled" name="disabledChekedRadio" type="radio" />Disabled &amp; checked</label></div>
</div>
<div class="col-md-4">
<h5>Inline</h5>
<div class="checkbox-inline"><label>
<input class="i-check" type="checkbox" />Checkbox 1</label></div>
<div class="checkbox-inline"><label>
<input class="i-check" type="checkbox" />Checkbox 2</label></div>
<div class="gap gap-mini"></div>
<div class="radio-inline"><label>
<input class="i-radio" name="myRadiolist" type="radio" />Radio 1</label></div>
<div class="radio-inline"><label>
<input class="i-radio" name="myRadiolist" type="radio" />Radio 2</label></div>
</div>
</div>
<h4>Smaller Size</h4>
<div class="row row-wrap">
<div class="col-md-4">
<div class="checkbox checkbox-small"><label>
<input class="i-check" type="checkbox" />Checkbox 1</label></div>
<div class="checkbox checkbox-small"><label>
<input class="i-check" type="checkbox" />Checkbox 2</label></div>
<div class="checkbox checkbox-small"><label>
<input class="i-check" disabled="disabled" type="checkbox" />Disabled</label></div>
<div class="checkbox checkbox-small"><label>
<input class="i-check" checked="checked" disabled="disabled" type="checkbox" />Disabled &amp; checked</label></div>
</div>
<div class="col-md-4">
<div class="radio radio-small"><label>
<input class="i-radio" name="mySmallRadioList" type="radio" />Radio 1</label></div>
<div class="radio radio-small"><label>
<input class="i-radio" name="mySmallRadioList" type="radio" />Radio 2</label></div>
<div class="radio radio-small"><label>
<input class="i-radio" disabled="disabled" name="smallDisabledRadio" type="radio" />Disabled</label></div>
<div class="radio radio-small"><label>
<input class="i-radio" checked="checked" disabled="disabled" name="smallDisabledChekedRadio" type="radio" />Disabled &amp; checked</label></div>
</div>
<div class="col-md-4">
<div class="checkbox-inline checkbox-small"><label>
<input class="i-check" type="checkbox" />Checkbox 1</label></div>
<div class="checkbox-inline checkbox-small"><label>
<input class="i-check" type="checkbox" />Checkbox 2</label></div>
<div class="gap gap-mini"></div>
<div class="radio-inline radio-small"><label>
<input class="i-radio" name="myRadiolist" type="radio" />Radio 1</label></div>
<div class="radio-inline radio-small"><label>
<input class="i-radio" name="myRadiolist" type="radio" />Radio 2</label></div>
</div>
</div>
<h4>Bigger Size</h4>
<div class="row row-wrap">
<div class="col-md-4">
<div class="checkbox checkbox-lg"><label>
<input class="i-check" type="checkbox" />Checkbox 1</label></div>
<div class="checkbox checkbox-lg"><label>
<input class="i-check" type="checkbox" />Checkbox 2</label></div>
<div class="checkbox checkbox-lg"><label>
<input class="i-check" disabled="disabled" type="checkbox" />Disabled</label></div>
<div class="checkbox checkbox-lg"><label>
<input class="i-check" checked="checked" disabled="disabled" type="checkbox" />Disabled &amp; checked</label></div>
</div>
<div class="col-md-4">
<div class="radio radio-lg"><label>
<input class="i-radio" name="mylgRadioList" type="radio" />Radio 1</label></div>
<div class="radio radio-lg"><label>
<input class="i-radio" name="mylgRadioList" type="radio" />Radio 2</label></div>
<div class="radio radio-lg"><label>
<input class="i-radio" disabled="disabled" name="lgDisabledRadio" type="radio" />Disabled</label></div>
<div class="radio radio-lg"><label>
<input class="i-radio" checked="checked" disabled="disabled" name="lgDisabledChekedRadio" type="radio" />Disabled &amp; checked</label></div>
</div>
<div class="col-md-4">
<div class="checkbox-inline checkbox-lg"><label>
<input class="i-check" type="checkbox" />Check 1</label></div>
<div class="checkbox-inline checkbox-lg"><label>
<input class="i-check" type="checkbox" />Check 2</label></div>
<div class="gap gap-mini"></div>
<div class="radio-inline radio-lg"><label>
<input class="i-radio" name="myRadiolist" type="radio" />Radio 1</label></div>
<div class="radio-inline radio-lg"><label>
<input class="i-radio" name="myRadiolist" type="radio" />Radio 2</label></div>
</div>
</div>
<h4>Stroke Style</h4>
<div class="row row-wrap">
<div class="col-md-4">
<div class="checkbox checkbox-stroke"><label>
<input class="i-check" type="checkbox" />Checkbox 1</label></div>
<div class="checkbox checkbox-stroke"><label>
<input class="i-check" type="checkbox" />Checkbox 2</label></div>
<div class="checkbox checkbox-stroke"><label>
<input class="i-check" disabled="disabled" type="checkbox" />Disabled</label></div>
<div class="checkbox checkbox-stroke"><label>
<input class="i-check" checked="checked" disabled="disabled" type="checkbox" />Disabled &amp; checked</label></div>
</div>
<div class="col-md-4">
<div class="radio radio-stroke"><label>
<input class="i-radio" name="mystrokeRadioList" type="radio" />Radio 1</label></div>
<div class="radio radio-stroke"><label>
<input class="i-radio" name="mystrokeRadioList" type="radio" />Radio 2</label></div>
<div class="radio radio-stroke"><label>
<input class="i-radio" disabled="disabled" name="strokeDisabledRadio" type="radio" />Disabled</label></div>
<div class="radio radio-stroke"><label>
<input class="i-radio" checked="checked" disabled="disabled" name="strokeDisabledChekedRadio" type="radio" />Disabled &amp; checked</label></div>
</div>
<div class="col-md-4">
<div class="checkbox-inline checkbox-stroke"><label>
<input class="i-check" type="checkbox" />Checkbox 1</label></div>
<div class="checkbox-inline checkbox-stroke"><label>
<input class="i-check" type="checkbox" />Checkbox 2</label></div>
<div class="gap gap-mini"></div>
<div class="radio-inline radio-stroke"><label>
<input class="i-radio" name="myRadiolist" type="radio" />Radio 1</label></div>
<div class="radio-inline radio-stroke"><label>
<input class="i-radio" name="myRadiolist" type="radio" />Radio 2</label></div>
</div>
</div>
<h4>Switch Style</h4>
<div class="row row-wrap">
<div class="col-md-4">
<div class="checkbox checkbox-switch"><label>
<input class="i-check" type="checkbox" />Checkbox 1</label></div>
<div class="checkbox checkbox-switch"><label>
<input class="i-check" type="checkbox" />Checkbox 2</label></div>
<div class="checkbox checkbox-switch"><label>
<input class="i-check" disabled="disabled" type="checkbox" />Disabled</label></div>
<div class="checkbox checkbox-switch"><label>
<input class="i-check" checked="checked" disabled="disabled" type="checkbox" />Disabled &amp; checked</label></div>
</div>
<div class="col-md-4">
<div class="radio radio-switch"><label>
<input class="i-radio" name="myswitchRadioList" type="radio" />Radio 1</label></div>
<div class="radio radio-switch"><label>
<input class="i-radio" name="myswitchRadioList" type="radio" />Radio 2</label></div>
<div class="radio radio-switch"><label>
<input class="i-radio" disabled="disabled" name="switchDisabledRadio" type="radio" />Disabled</label></div>
<div class="radio radio-switch"><label>
<input class="i-radio" checked="checked" disabled="disabled" name="switchDisabledChekedRadio" type="radio" />Disabled &amp; checked</label></div>
</div>
<div class="col-md-4">
<div class="checkbox-inline checkbox-switch"><label>
<input class="i-check" type="checkbox" />Check 1</label></div>
<div class="checkbox-inline checkbox-switch"><label>
<input class="i-check" type="checkbox" />Check 2</label></div>
<div class="gap gap-mini"></div>
<div class="radio-inline radio-switch"><label>
<input class="i-radio" name="myRadiolist" type="radio" />Radio 1</label></div>
<div class="radio-inline radio-switch"><label>
<input class="i-radio" name="myRadiolist" type="radio" />Radio 2</label></div>
</div>
</div>

<hr />

<h2>Date and Time Pick Inputs</h2>
<div class="row row-wrap">
<div class="col-md-6">
<div class="row">
<div class="col-md-6">
<div class="form-group"><label>Basic Date picker</label>
<input class="date-pick form-control" type="text" /></div>
</div>
<div class="col-md-6">
<div class="form-group"><label>Custom Date format</label>
<input class="date-pick form-control" type="text" data-date-format="DD d MM yyyy" /></div>
</div>
</div>
<h4>Range Layout</h4>
<div class="input-daterange">
<div class="row">
<div class="col-md-6">
<div class="form-group"><label>Start Date</label>
<input class="form-control" name="start" type="text" /></div>
</div>
<div class="col-md-6">
<div class="form-group"><label>End Date</label>
<input class="form-control" name="end" type="text" /></div>
</div>
</div>
</div>

<hr />

<div class="row">
<div class="col-md-6">
<div class="form-group"><label>Time picker</label>
<input class="time-pick form-control" type="text" /></div>
</div>
</div>
You can combine date and time pick inputs with any input icon elements and input size (normal, large, small)

</div>
<div class="col-md-6"><label for="">Inline Layout</label>
<div class="date-pick-inline"></div>
</div>
</div>

<hr />

<h2>Range Slider</h2>
<div class="row">
<div class="col-md-6"><input id="price-slider" type="text" /></div>
</div>

<hr />

<h2>Credit Card Form</h2>
<div class="row">
<div class="col-md-6"><form class="cc-form">
<div>
<div class="form-group form-group-cc-number"><label>Card Number</label>
<input class="form-control" type="text" placeholder="xxxx xxxx xxxx xxxx" /></div>
<div class="form-group form-group-cc-cvc"><label>CVC</label>
<input class="form-control" type="text" placeholder="xxxx" /></div>
</div>
<div>
<div class="form-group form-group-cc-name"><label>Cardholder Name</label>
<input class="form-control" type="text" /></div>
<div class="form-group form-group-cc-date"><label>Valid Thru</label>
<input class="form-control" type="text" placeholder="mm/yy" /></div>
</div>
</form></div>
</div>";s:10:"post_title";s:20:"Page : Form Elements";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:18:"page-form-elements";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2014-11-21 08:58:24";s:17:"post_modified_gmt";s:19:"2014-11-21 08:58:24";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:59:"http://localhost/shinetheme/Travel/travel-code/?page_id=110";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}