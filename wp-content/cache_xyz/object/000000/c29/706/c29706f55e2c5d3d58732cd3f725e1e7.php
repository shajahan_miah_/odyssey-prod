JʩW<?php exit; ?>a:1:{s:7:"content";a:49:{s:11:"rate_review";a:1:{i:0;s:1:"0";}s:10:"sale_price";a:1:{i:0;s:2:"40";}s:10:"_edit_lock";a:1:{i:0;s:12:"1469724849:2";}s:10:"_edit_last";a:1:{i:0;s:1:"1";}s:17:"_vc_post_settings";a:1:{i:0;s:50:"a:2:{s:7:"vc_grid";a:0:{}s:10:"vc_grid_id";a:0:{}}";}s:11:"is_featured";a:1:{i:0;s:3:"off";}s:11:"id_location";a:1:{i:0;s:4:"5837";}s:7:"map_lat";a:1:{i:0;s:9:"-1.376967";}s:7:"map_lng";a:1:{i:0;s:9:"36.773751";}s:8:"map_zoom";a:1:{i:0;s:2:"13";}s:5:"price";a:1:{i:0;s:2:"40";}s:16:"is_sale_schedule";a:1:{i:0;s:3:"off";}s:13:"gallery_style";a:1:{i:0;s:4:"grid";}s:10:"type_price";a:1:{i:0;s:10:"tour_price";}s:9:"type_tour";a:1:{i:0;s:10:"daily_tour";}s:12:"duration_day";a:1:{i:0;s:1:"1";}s:10:"max_people";a:1:{i:0;s:2:"24";}s:13:"tours_program";a:1:{i:0;s:2472:"a:1:{i:0;a:2:{s:5:"title";s:26:"Sheldrick Animal Orphanage";s:4:"desc";s:2389:"Located in a secluded area of the Nairobi National Park the orphanage cares for baby elephants and other orphaned species with a dedicated team of conservationists. The orphanage is run by Daphne Sheldrick, the wife of the late famous Naturalist, David William Sheldrick who was the founder Warden of Tsavo East National Park in Kenya from its inception in 1948 to 1976.
<br /><br />

 

At 11:00 am, the baby animals are brought from the National Park for a mud-bath at the orphanage, where for an hour you can get close to them and sometimes touch them.

 <br /><br />

You may not feed the animals but will get a chance to view the handlers feeding them. The Private Session strictly between 5:00 pm to 6:00 pm each day and allows you to see and interact with the animals in private; advance booking required.

 <br /><br />

The drive to the orphanage takes approximately 45 minutes and is open to the public for only one hour a day from 11:00 am – 12:00 pm.

 <br /><br />

In addition, one of the conservationists will give a talk about the elephants and where they came from, how they managing at the orphanage and how some of the previous orphans are progressing.  During this talk you are observing the feeding, play and bathing of the elephants and other orphaned species.
<br /><br />
 

Price:  $40 per person

Includes:  Transport to and from orphanage, transport to and from lunch location of your choice (if necessary/desired, lunch additional), entry fee to orphanage.

<br /><br /> 

Private Showing - Adopting a Baby Elephant For a Year
It's hard not to be touched when you see the orphans, and the dedication and hard work demanded of the conservationists to keep them happy and healthy is inspiring.  Feeding them every three hours around the clock, keeping them warm and playing with them, requires huge efforts and of course money.   Starting at $50, you can adopt an orphan, and the money goes directly to the project.  You receive regular updates on your orphan via e-mail, as well as a copy of his biography, an adoption certificate, a water color painting of the orphan, and most importantly -- the knowledge that you have made a difference. Once you adopt, you may also make an appointment to see your baby when it goes to bed without the crowds during the public showing.

Entry fee and adoption required for the private showing.";}}";}s:19:"accommodation_video";a:1:{i:0;s:72:"https://www.youtube.com/playlist?list=PLjqSFpDvnZb44Sidwc4T8RDU1L6yXfGX3";}s:20:"_accommodation_video";a:1:{i:0;s:19:"field_55c5e8f26c5e9";}s:11:"video_title";a:1:{i:0;s:23:"Nairobi Area Activities";}s:12:"_video_title";a:1:{i:0;s:19:"field_55c5ed93ae5be";}s:10:"video_text";a:1:{i:0;s:60:"David Sheldrick Wildlife Trust<br />
Giraffe Center<br />
";}s:11:"_video_text";a:1:{i:0;s:19:"field_55c5edabae5bf";}s:3:"map";a:1:{i:0;s:354:"<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d63819.78396355141!2d36.79779485!3d-1.33428595!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x182f0547b2e1aee9%3A0x448d07077ae6dc63!2sDavid+Sheldrick+Wildlife+Trust!5e0!3m2!1sen!2sus!4v1439577440548" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>";}s:4:"_map";a:1:{i:0;s:19:"field_55c5fc80010d9";}s:21:"international_airfare";a:1:{i:0;s:3:"N/A";}s:22:"_international_airfare";a:1:{i:0;s:19:"field_55c609ee3dfdc";}s:15:"departure_dates";a:1:{i:0;s:5:"Daily";}s:16:"_departure_dates";a:1:{i:0;s:19:"field_55c609f63dfdd";}s:9:"tour_type";a:1:{i:0;s:7:"Private";}s:10:"_tour_type";a:1:{i:0;s:19:"field_55c60a373dfde";}s:16:"post_views_count";a:1:{i:0;s:0:"";}s:13:"_thumbnail_id";a:1:{i:0;s:4:"5856";}s:5:"video";a:1:{i:0;s:72:"https://www.youtube.com/playlist?list=PLjqSFpDvnZb44Sidwc4T8RDU1L6yXfGX3";}s:4:"what";a:1:{i:0;s:112:"Pick up and transport to orphanage
Guiding and entry fee to orphanage
Transport and drop off 
Bottled water
";}s:5:"_what";a:1:{i:0;s:19:"field_563c35c45737f";}s:15:"whats__included";a:1:{i:0;s:16:"Souvenir's, food";}s:16:"_whats__included";a:1:{i:0;s:19:"field_563c367c57380";}s:19:"summary_of_wildlife";a:1:{i:0;s:15:"Elephant, rhino";}s:20:"_summary_of_wildlife";a:1:{i:0;s:19:"field_563c36ac57381";}s:15:"whats_included_";a:1:{i:0;s:20:"a:1:{i:0;s:3:"Yes";}";}s:16:"_whats_included_";a:1:{i:0;s:19:"field_564040b08ad76";}s:29:"click_show_whats_not_included";a:1:{i:0;s:20:"a:1:{i:0;s:3:"Yes";}";}s:30:"_click_show_whats_not_included";a:1:{i:0;s:19:"field_564041e58ad77";}s:25:"click_summary_of_wildlife";a:1:{i:0;s:20:"a:1:{i:0;s:3:"Yes";}";}s:26:"_click_summary_of_wildlife";a:1:{i:0;s:19:"field_564042388ad78";}s:8:"order_by";a:1:{i:0;s:1:"6";}s:9:"_order_by";a:1:{i:0;s:19:"field_566026addd597";}}}