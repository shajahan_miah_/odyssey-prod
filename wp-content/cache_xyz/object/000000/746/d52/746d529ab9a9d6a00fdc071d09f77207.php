X�W<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:7052;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2016-06-09 01:27:49";s:13:"post_date_gmt";s:19:"2016-06-09 05:27:49";s:12:"post_content";s:6310:"My Groupon vacation almost got me killed (and why I’d do it all over again)
By Linda Stasi

May 1, 2012 | 4:00am 

Modal Trigger   My Groupon vacation almost got me killed (and why I’d do it all over again)  
KEEP IT ZIPPED: Tent accommodations at Samburu Camp in Kenya. Photo:  
 
<a href="https://www.odysseysafaris.com/wp-content/uploads/2016/06/untitled.png"><img src="https://www.odysseysafaris.com/wp-content/uploads/2016/06/untitled-300x300.png" alt="untitled" width="300" height="300" class="alignnone size-medium wp-image-7053" /></a>

The setting: Kenya’s Masai Mara National Reserve, a warm March evening. Warm enough that I can’t figure out why the guy who brings me my towels has zipped up all the flaps on the tent. No, I want some air in here. 

I unzipped the big front flap — it’s maybe 10’ x 7’ — and hook it to the outside roof ledge. That’s better. I leave only the mosquito-netting flap closed. I want to feel the breeze, and hear the animals off in the distance. We’re safe — there’s an electric fence between them and us. I tuck into bed and drift off to sleep. 

Then: What is that? I’m startled awake by a frantic, desperate clawing on the side of the tent. It jumps at it, trying to scale the thing, emitting a very deep, low grunting. 

Something’s trying to break in — and it’s not human. 

I sit straight up, my heart pounding. I wake my sleeping guy. Whatever the thing is, it jumps back down and begins slamming into the wall of the tent, clawing and trying to rip through the canvas 2 feet from where we lay. 

Then it gets worse. Slinking across the wooden porch, a few feet from our bed, is the shadow of an animal more terrifying than whatever it is that is trying to claw its way in. Lion? Leopard? The lights suddenly go out all over camp. It’s midnight — electricity is shut down. It is now pitch black. There’s a beast on one side of our tent, a big cat in front, and we’ve got nothing but mosquito netting for protection.

Welcome to my Groupon discount safari. 

There are two things that I’ve wanted to do for the longest time: Take a safari (way too expensive) and try one of those vacations sold at deep discounts on Groupon.com. 

So, when a deal from Odyssey Safaris came up on the site — Kenya at 40 percent off! — I immediately inquired. Odyssey’s co-owner, Mark Cahill, was most helpful on the phone and through email. For about $3,600 per person, my guy and I would get an eight-night safari traveling nearly 1,000 miles through Nairobi. Total cost for two: $7,200 with airfare.

To save even more, we flew from New York using miles and got to deduct $1,900. Final cost for two people for an eight-day safari: $5,300.

This is what Odyssey offered: Day one, we’d be met at the airport and driven to the luxurious Safari Park Hotel. Next morning, we’d meet our group (who quickly became our new BFF’s) in the lobby. From there we’d be assigned with four others to a Jeep with a driver/guide who’d be with us for the entire trip. 

Our trip, including visits at Masai, Nukuru and Sambur villages, would begin with a drive to the Samburu National Reserve, home to massive elephant and ape populations, alligators, big cats, zebras, ostriches, oryx and giraffes. 

Next would be Lake Nakuru National Park for two days. (National parks in Kenya are the size of small states in the US.) Lake Nakuru is home to rhinos, cats, apes and millions of flamingoes. Last two days were spent at the Masai Mara National Reserve, which is filled with cheetahs, lions and leopards, as well as herds of water buffalo, gazelles, zebras, gazelles, giraffes, apes, elephants, hundreds of species of birds including 53 species of birds of prey. 

All meals and luxury tents (permanent back wall, beds, bathroom, canvas sides and roof, often a wooden front porch) would be included, as well as anything else we might need/want.

Luckily, we were assigned great traveling companions: three very fit and funny female educators from Colorado, and one hilarious male doctor from Philadelphia. They were ready for anything, and so were we. OK, not death, but anything else up to that point. 

Every aspect of this safari — from the warm, friendly, helpful staff to photo possibilities, meals, fun and accommodations — was first rate. Even at the deeply discounted price! 

Everything until now. 

The night crawled by, as we waited for something — anything — to happen. In the pitch black, and then in pouring rain, we realized eventually that while nobody was going to come to our rescue, there was one small — cold, but small — comfort: This wasn’t so much about us as it was a fight to the finish between two large, angry, unidentifiable beasts. At 3:00 a.m., with the brutality showing no sign of slowing, we took our chances and rushed outside in the rain to unhook the canvas flap. I ran back in, zipping it up as my guy held it closed. The finish finally arrived near dawn, when the animals finally crashed, screaming, to the ground.

Next morning outside our tent lay the aftermath: part of an intestine, liver and spleen. Our guide, examining the scene, surmised that somehow a baboon and a leopard had breached the electric fence. Where they ended up would forever remain a mystery. 

Had this happened because we were on a discounted trip, I wondered? Anything’s possible, but in the retelling of the story, I came across two friends who had gone on one of the most expensive safaris money can buy, returning home with scary stories of their own. One was in a Jeep charged by an elephant, and another had a lion appear at the pool of their luxury resort sending them running for their lives. Africa isn’t a theme park, after all. 

But hey, if you’re going to be scared to death, it may as well be at a 40 percent discount — right?

Stasi is a television columnist for The Post. She has already booked her next Groupon adventure: 10 days in China this summer, for $1,790 per person (with air). 

<strong>
Source: </strong> <a href="http://nypost.com/2012/05/01/my-groupon-vacation-almost-got-me-killed-and-why-id-do-it-all-over-again/">http://nypost.com/2012/05/01/my-groupon-vacation-almost-got-me-killed-and-why-id-do-it-all-over-again/</a>";s:10:"post_title";s:77:"My Groupon vacation almost got me killed (and why I’d do it all over again)";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:72:"my-groupon-vacation-almost-got-me-killed-and-why-id-do-it-all-over-again";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2016-06-09 01:34:02";s:17:"post_modified_gmt";s:19:"2016-06-09 05:34:02";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:66:"https://www.odysseysafaris.com/?post_type=nooz_release&#038;p=7052";s:10:"menu_order";i:0;s:9:"post_type";s:12:"nooz_release";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}