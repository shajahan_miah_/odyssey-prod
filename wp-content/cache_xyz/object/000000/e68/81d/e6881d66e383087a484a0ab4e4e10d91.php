��W<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:7302;s:11:"post_author";s:2:"30";s:9:"post_date";s:19:"2016-08-08 15:36:09";s:13:"post_date_gmt";s:19:"2016-08-08 19:36:09";s:12:"post_content";s:3164:"When you are on safari in Africa, the Masai Mara and Serengeti give you the best opportunity to spot a cheetah - the world's fastest land animal.  Here are some facts you need to know about this amazing cat.
<ul>
 	<li class="style1">
<div class="style33" align="left"><em>The cheetah is the fastest land animal in the world and can reach a top speed of around 113 km per hour in just a few seconds.</em></div></li>
 	<li class="style1">
<div class="style33" align="left"><em>Cheetahs are extremely fast however, they tire quickly and can only keep up their top speed for a few minutes before they are too exhausted to continue.</em></div></li>
 	<li class="style1">
<div class="style33" align="left"><em>Cheetahs are smaller than other members of the big cat family with adults weighing only 45 – 60 kilograms.</em></div></li>
 	<li class="style1">
<div class="style33" align="left"><em>One way to recognize a cheetah is by the long, black markings which run from the inside of each eye to the mouth.  These are usually called “tear lines” and scientists believe they help protect the cheetah’s eyes from the harsh sun while helping them to see long distances.</em></div></li>
 	<li class="style1">
<div class="style33" align="left"><em>Cheetahs are the only big cat that cannot roar.  They can purr and usually do so most loudly when they are grooming or sitting near other cheetahs.</em></div></li>
 	<li class="style1">
<div class="style33" align="left"><em> Cheetahs hunt for food during the day while lions and leopards usually do their hunting at night.</em></div></li>
 	<li class="style1">
<div class="style33" align="left"><em>A cheetah has amazing eyesight during the day and can spot prey from 5 km away.</em></div></li>
 	<li class="style1">
<div class="style33" align="left"><em>Cheetahs cannot climb trees and have poor night vision.</em></div></li>
 	<li class="style1">
<div class="style33" align="left"><em>A cheetahs bones, like a bird, are less dense and therefore lighter than normal for an animal of their size.  This lighter weight is one of the reasons cheetahs are able to run at high speed.</em></div></li>
 	<li class="style1">
<div class="style33" align="left"><em>With their light body weight and blunt claws, cheetahs are not well designed to protect themselves or their prey.  When a larger or more aggressive animal approaches a cheetah in the wild, it will give up its catch to avoid a fight.</em></div></li>
 	<li class="style1">
<div class="style33" align="left"><em>Cheetahs only need to drink once every three to four days.  </em></div></li>
 	<li class="style1">
<div class="style33" align="left"><em>Cheetahs are the most successful hunters of all the big cats.
</em></div></li>
</ul>
<div align="left">

[caption id="attachment_7321" align="alignleft" width="600"]<a href="https://www.odysseysafaris.com/wp-content/uploads/2016/08/AF7G4189.jpg"><img class="wp-image-7321 size-full" style="border: solid #5e1e03 2px;" src="https://www.odysseysafaris.com/wp-content/uploads/2016/08/AF7G4189.jpg" alt="Cheetah" width="600" height="400" /></a> Cheetah on the hunt.[/caption]

</div>";s:10:"post_title";s:17:"Know Your Cheetah";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:17:"know-your-cheetah";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2016-08-08 15:36:09";s:17:"post_modified_gmt";s:19:"2016-08-08 19:36:09";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:63:"https://www.odysseysafaris.com/?post_type=blog_post&#038;p=7302";s:10:"menu_order";i:0;s:9:"post_type";s:9:"blog_post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}