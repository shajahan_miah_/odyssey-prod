B�W<?php exit; ?>a:1:{s:7:"content";a:1:{i:0;O:8:"stdClass":4:{s:7:"meta_id";s:4:"5361";s:7:"post_id";s:4:"1029";s:8:"meta_key";s:13:"tours_program";s:10:"meta_value";s:7811:"a:10:{i:0;a:2:{s:5:"title";s:22:"Day 1:  Arrive Nairobi";s:4:"desc";s:371:"Following your flight odyssey Safaris staff will greet you at the airport and load your bags for transport to the
hotel. Your guide will assist you with check in on arrival and give the group a short briefing on the days ahead.
If you arrive early enough in the day we can arrange for activities in and around Nairobi for this time as well.
Overnight at House of Waine";}i:1;a:2:{s:5:"title";s:32:"Day 2:  Tsavo East National Park";s:4:"desc";s:760:"Breakfast is at 7:00am and at 8:00am your driver/guide loads your luggage for the start of the 300 km drive to
Ithumba Camp which takes between 5 to 6 hours; you arrive in time for lunch. Ithumba Camp is hidden within
the depths of the remote northern area of Tsavo East National Park. The camp features a backdrop of the
beautiful Ithumba hill which boasts stunning views of the Yatta Plateau, famously known as the longest lava
flow in the world. At 2:30pm we take a 1 hour game walk to the plateau and exploring Ithumba conservancy.
At 4pm we return to the stockades for the feeding of the baby elephants which lasts until 5:30pm and dinner
is served at 6:00pm.
Overnight at Ithumba Camp
Travel time: 5 - 6 hours
Meal Plan: Breakfast, lunch, dinner";}i:2;a:2:{s:5:"title";s:32:"Day 3:  Tsavo East National Park";s:4:"desc";s:535:"Today at 6:00am wake up for a full breakfast and at 7am depart for morning game walk searching for wildlife
and ending with the wonderful opportunity to walk within the stockades and see how the elephants live and
are cared for. You return to the camp at 10 am to enjoy the rest of the morning before you return to the
elephants for their mud bath. Lunch is served from 1pm to 3 pm, 4pm-6pm stockades evening feeding with
dinner served 6 pm after the evening feeding.
Overnight at Ithumba Camp
Meal Plan: Breakfast, lunch, dinner";}i:3;a:2:{s:5:"title";s:30:"Day 4:  Amboseli National Park";s:4:"desc";s:657:"Sleep in or spend another morning with the elephants before breakfast at 8am. After breakfast, you depart for
Amboseli National Park, a 4 hour journey arriving in time for lunch. Amboseli National Park is famous for its
breath-taking views of Kilimanjaro. The Park also shelters more than 420 species of birds. An enormous
elephant population and other big game are attracted to the area by underground springs fed from the melting
snows of Mount Kilimanjaro. At 3:30 pm you depart on an afternoon game drive, return to the lodge at
6:30pm for dinner.
Overnight at Amboseli Serena Safari Lodge
Drive time: 4 hours
Meal Plan: Breakfast, lunch, dinner";}i:4;a:2:{s:5:"title";s:30:"Day 5:  Amboseli National Park";s:4:"desc";s:1172:"You rise early for coffee and tea with the hopes of clear skies for the incredible Mount Kilimanjaro. You depart
on morning game drive returning to the lodge for breakfast at 9:00 am.
Amboseli is an excellent place to view a multitude of wildlife, including everything from wildebeest to giraffes
and baboons. At 10:30 am you visit a Masai village to learn about their traditions and customs. You will
experience their dances and see inside their homes for a look at how they live in this unique environment.
Amboseli is home to the legendary Masai tribesmen. Masai warriors are famous and are proud nomadic tribe
whose legendary prowess in battle and single-handed acts of bravery in fights with the wild animals are known
across the world. Mid-day finds us at observation hill, followed by another game drive as we head back to the
lodge for lunch.

Following lunch you enjoy some leisure time and at 3:30 pm you depart for an afternoon game drive and
return to the lodge at 6:30 pm for refreshments before your private bush dinner under the canopy of the
Acacia tortilis trees.
Overnight at Amboseli Serena Safari Lodge
Meal Plan: Breakfast, lunch, dinner";}i:6;a:2:{s:5:"title";s:24:"Day 6:  Lewa Conservancy";s:4:"desc";s:784:"Today you start with an early morning game drive and some more views of Mount Kilimanjaro followed by
breakfast at the camp. We check out and head for the airstrip for our flight to Lewa Conservancy via Nairobi.
Lewa Conservancy is located in the central highlands in the foothills of Mount Kenya, Africa's second highest
mountain. Lewa is at the forefront of wildlife conservation in Africa employing the most advanced technology,
such as unmanded airial drones, in the fight against illegal poaching. In additional to the amazing wildlife
viewing and spectacular views, you have the opportunity to better understand the challenges facing the
conservation community and their efforts to meet those challenges.
Overnight at Lewa Safari Camp
Meal Plan: Breakfast, lunch, dinner";}i:7;a:2:{s:5:"title";s:24:"Day 7:  Lewa Conservancy";s:4:"desc";s:419:"An early morning game drive is followed by breakfast at the camp. Following breakfast you depart for a tour of
the conservation facility and meet with the rangers on the front lines of the fight against poaching to learn
about their strategies and see first hand the successes and failures in this effort. Lunch and afternoon game
drive and dinner.
Overnight at Lewa Safari Camp
Meal Plan: Breakfast, lunch, dinner";}i:8;a:2:{s:5:"title";s:35:"Day 8:  Masai Mara National Reserve";s:4:"desc";s:769:"Leisurely wake up with breakfast and a flight from the Conservancy airstrip to the Masai Mara National
Reserve. Recently voted Africa's finest wildlife reserve the wildlife and classic savannah landscape will leave
you with memories to last a lifetime.
Following arrival at the airstrip, you transfer to the camp while conducting a short game drive en-route. Lunch
is taken there, and after you have settled into your room, you depart with your driver guide into the reserve to
serach for wildlife. Located on the Mara River this camp is in the heart of the Masai Mara and offers an
excellent start point for accessing the plains or river area where most of the large species and big cats are
found.
Overnight at Naibor Camp
Meal Plan: Breakfast, lunch, dinner";}i:9;a:2:{s:5:"title";s:35:"Day 9:  Masai Mara National Reserve";s:4:"desc";s:885:"Collective information of the guides at the camp and what we gathered from the game drives yesterday, we
take a picnic lunch and depart on a full day game drive in the Mara. Depending on what you want to see and
the situation on the ground your guide will plan the day accordingly. If there is a herd of wildebeest preparing
to cross the river we might set up in an advantageous location to view that crossing or we might have received
information on a lion pride actively hunting and so we would proceed to track and observe them in their effort.
The Mara provides numerous and incredible opportunity to view these animals in their natural habitat and
observe their behaviors and interactions.
Picnic lunch in a scenic location in the Mara and more wildlife viewing in the afternoon with dinner at the camp.
Overnight at Naibor Camp
Meal Plan: Breakfast, picnic lunch, dinner";}i:10;a:2:{s:5:"title";s:29:"Day 10:  Masai Mara - Nairobi";s:4:"desc";s:657:"Enjoy a leisurely breakfast and check out of the camp for the short drive to the airstrip for your flight to Nairobi.
On arrival in Nairobi you are transferred to the hotel and a day room where you can store your gear and
freshen up before your flight home. You take lunch here and then depart with your guide to the Sheldrick Trust
in Nairobi where for a sponsor's visit where you you can meet your adopted elephant and their handlers. We
return to the hotel to pack up for the flight, check out of the day room, dinner in the restaurant followed by
your transfer to the airport.
Day room at House of Waine
Meal Plan: Breakfast, picnic lunch, dinner";}}";}}}