=��W<?php exit; ?>a:1:{s:7:"content";a:1:{i:0;O:8:"stdClass":4:{s:7:"meta_id";s:5:"14373";s:7:"post_id";s:4:"5938";s:8:"meta_key";s:13:"tours_program";s:10:"meta_value";s:3897:"a:7:{i:0;a:2:{s:5:"title";s:22:"Day 1:  Arrive Nairobi";s:4:"desc";s:59:"Meet & greet at the airport and transfer to hotel. <br />
";}i:1;a:2:{s:5:"title";s:30:"Day 2:  Amboseli National Park";s:4:"desc";s:361:"In the morning, you’ll be driven to Amboseli National Park, where you’ll experience your first game drive. You’ll also get your first glimpse of Mount Kilimanjaro rising up from the savanna.

    Herds of elephants drink from freshwater lakes here. The park is a refuge for hundreds of the tusked mammals.
    Keep an eye out for hyenas and cheetahs.
";}i:3;a:2:{s:5:"title";s:28:"Day 3:  Arusha National Park";s:4:"desc";s:508:"Day 4: Arusha National Park, Tanzania

Today you’ll move into Tanzania, where your home base will be the city of Arusha. It’s located close to some of Africa’s most famous landscapes—Mount Kilimanjaro is only about 70 miles northeast.

    Arusha National Park is home to black-and-white colobus monkeys, warthogs, and flamingos. You might see leopards slinking around in the late afternoon.
    The centerpiece of the park is Mount Meru, an active stratovolcano that rises nearly 15,000 feet.
";}i:4;a:2:{s:5:"title";s:40:"Day 5: Tarangire National Park, Tanzania";s:4:"desc";s:540:"Day 5: Tarangire National Park, Tanzania

This park is home to more than 500 bird species and large herds of elephants and giraffes. Because it’s relatively small, it has a denser concentration of wildlife, so you have a greater chance of seeing animals in action.

    Rare animals: Gerenuk, lesser kudu, greater kudu, oryx, and even eland may be more easily found here than in other parks thanks to its size and altitude.
    Elephants on parade: Herds in the park can be as many as 300 strong, making for an impressive photo op.
";}i:5;a:2:{s:5:"title";s:33:"Day 6: Lake Manyara National Park";s:4:"desc";s:376:"Day 6: Lake Manyara National Park, Tanzania

Ernest Hemingway called this park the loveliest he had seen in Africa. It’s centered on Lake Manyara, whose dramatic stone rift reaches more than 1,900 feet in height.

    Action-packed day: Day 6 includes three game drives and a picnic lunch.
    Animals to look out for: elephants, hippos, zebras, giraffes, and baboons
";}i:6;a:2:{s:5:"title";s:49:"Days 7 and 8: Ngorongoro Crater Conservation Area";s:4:"desc";s:712:"Days 7 and 8: Ngorongoro Crater Conservation Area, Tanzania

Often called the Eighth Wonder of the World, Ngorongoro Conservation Area encompasses a soaring plateau of volcanic highlands and craters. It was originally designed to accommodate indigenous Masai communities and visitors, so you might see villagers grazing their cattle and donkeys.

    The Big Five: View buffalo, elephants, leopards, lions, and rhinos on a full-day game drive with lunch.
    Ngorongoro Crater is the world’s largest unbroken volcanic caldera. It’s densely packed with some 250,000 wild animals, including the prehistoric black rhinoceros. On day 7, you’ll head to the floor of the caldera on a full-day crater tour.
";}i:7;a:2:{s:5:"title";s:50:"Days 9 and 10: Depart Tanzania or Extend Your Trip";s:4:"desc";s:734:"Days 9 and 10: Depart Tanzania or Extend Your Trip

Depending on the option you choose, on day 9 you’ll either do a game drive in the morning and lunch in Arusha before you begin your journey home, or you’ll take an extended adventure to Mount Kilimanjaro.

    If you stay: You’ll spend day 9 hiking the mountain and depart on day 10, arriving home on day 11. You’ll get an extra day’s worth of meals, too.
    Mount Kilimanjaro: You will have seen the peak rising in the distance throughout your trip; this is your chance to see the behemoth up close and explore its rocky gorges.
    Additional extensions: You may also extend your trip to travel to the Serengeti or Zanzibar. Contact Odyssey Safaris for details.
";}}";}}}