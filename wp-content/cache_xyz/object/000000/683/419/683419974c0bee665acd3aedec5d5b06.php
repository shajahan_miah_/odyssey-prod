��W<?php exit; ?>a:1:{s:7:"content";a:1:{i:0;O:8:"stdClass":4:{s:7:"meta_id";s:5:"21869";s:7:"post_id";s:4:"6439";s:8:"meta_key";s:13:"tours_program";s:10:"meta_value";s:16510:"a:10:{i:0;a:2:{s:5:"title";s:49:"Day 1:   Cape Town airport ‐ Cape Town (38 kms)";s:4:"desc";s:1987:" Meet and greet at Cape Town International Airport. This personalised service sees your guide meet you off your flight. While you
enjoy a complimentary beverage, our guide will explain your itinerary in detail, give you general advice and information that will
help you along your trip. Before handing over your travel documents and escorting you to your onward transportation, our guide
will assist you with any queries you may have before you embark on your holiday.
 
‐ Half day Cape Town city tour. Be captivated by the multiethnic character and historical wealth of the Mother City. Cape Town is a
rare  cultural  gem,  resulting  from  the  amalgamation  of  different  nationalities  and  indigenous  tribes.  Alongside  high‐rise  office
blocks, a harmonious blend of architectural styles has been meticulously preserved, including Edwardian, Victorian and Cape Dutch.
Narrow, cobblestone streets and the Islamic character of the Bo‐Kaap enhance the cosmopolitan ambiance. The itinerary takes you
to the City Hall, the Castle of Good Hope – this pentagon‐shaped fort is the oldest surviving building in South Africa; the Company's
Garden – a large public park, originally Jan van Riebeeck's vegetable garden, which he grew to feed the original colony as early as
1652; and Signal Hill with spectacular panoramas over the city.
 
‐ Take the cableway up Table Mountain. In 2011, Table Mountain was named among the New7Wonders of Nature. You can get to
the top of Cape Town's most famous icon in just five minutes by taking a cable car up. The Aerial Cableway, established in 1929,
takes visitors to the top in one of two cable cars, each with rotating floors and huge windows to ensure your views while travelling
are almost as spectacular as those on the summit. Cable cars depart every 10 to 15 minutes. The Cableway operates only when
weather permits and does not take bookings.
 
‐ Night: Vineyard Hotel (Courtyard Room ‐ Bed and Breakfast)";}i:1;a:2:{s:5:"title";s:17:"Day 2:  Cape Town";s:4:"desc";s:659:" Breakfast
 
‐ Full day Cape Peninsula tour. Discover both the Atlantic and Indian Ocean coastlines. The first stop is at the charming fishing village
of Hout Bay. Then continue along the Atlantic coastline to the Cape of Good Hope Nature Reserve and Cape Point – the most south‐
westerly tip of Africa and the mythical meeting place of two great oceans. On the return trip, pass through the naval town of
Simon's Town and then traverse the fishing port of Kalk Bay and the lush suburb of Constantia. The day will end with a visit to the
renowned Kirstenbosch Botanical Gardens.
 
‐ Night: Vineyard Hotel (Courtyard Room ‐ Bed and Breakfast)";}i:2;a:2:{s:5:"title";s:17:"Day 3:  Cape Town";s:4:"desc";s:720:" Breakfast
 
‐ Full day Cape Wine Route tour. Learn everything there is to know about wines, from the vineyard to the bottle. The Cape Wine
Route is defined by the triangle that forms the towns of Stellenbosch, Paarl and Franschhoek. Stellenbosch is a charming old town
with oak‐shaded avenues and historical buildings. Franschhoek owes its name to the French Huguenots, who came to settle there
in 1685 and planted the first quality vineyards, while Paarl has been a centre for the Afrikaner culture since the 19th century. Along
the Route, wine estates welcome you to taste wines and cheeses and invite you to learn how the wines are made.
 
‐ Night: Vineyard Hotel (Courtyard Room ‐ Bed and Breakfast)";}i:3;a:2:{s:5:"title";s:37:"Day 4:  Cape Town ‐ Durban (62 kms)";s:4:"desc";s:1492:"Breakfast
 
‐ Transfer from Cape Town to Cape Town airport
 
‐ Take your time to explore the Victoria and Alfred Waterfront. The V&A Waterfront is situated on South Africa’s oldest working
harbour in the centre of Cape Town and offers over 450 retail outlets selling everything from high‐end fashion and jewellery to food
and crafts. It is one of Cape Town’s most popular destinations, attracting in excess of 23 million visitors a year. Besides offering
vibrant and chic indoor and outdoor shopping and restaurants there is the added bonus of watching fishing boats dock with fresh
fish or see a container ship being towed into the harbour by a tug boat. Draw‐ bridges over the water open and close every so often
to allow smaller sailing vessels access to the docks, while the Cape Wheel gives unsurpassed 360 degree views of the city, and the
outdoor amphitheatre plays host to music, dance and theatrical performances throughout the year. There is also a variety of other
activities on offer ranging from historical walking tours to air charters and kids clubs.
 
‐ Flight CapeTown ‐ Durban (Economy Class) (Optional rates: Flight CPT ‐ Durban )
 
‐ Meet and Greet at the airport.
 
‐ Minibus with Guide Overland : service starts at Durban airport and ends at Johannesburg airport (D10 12/03/16) ‐ 7 days
 
‐ Dinner: Southern Sun Elangeni & Maharani
 
‐ Night: Southern Sun Elangeni & Maharani (Standard Room ‐ Dinner, Bed and Breakfast)";}i:4;a:2:{s:5:"title";s:36:"Day 5: Durban ‐ Hluhluwe (273 kms)";s:4:"desc";s:2753:"Breakfast
 
‐ Discover Durban's “Golden Mile”. This six kilometre long stretch of sandy beach starts roughly at South Beach and uShaka Marine
World and ends at the Suncoast Casino and Entertainment World to the north. The Golden Mile is also a huge pleasure resort with
dozens of swimming and splash pools, fountains and waterslides, curio markets and merry‐go‐rounds, uShaka Marine World, exotic
restaurants, nightclubs and many hotels and apartment complexes directly at the beach. Swimming is good all year round due to
the warm, subtropical climate, and the beaches are well monitored and protected by shark nets. An unusual feature at the Golden
Mile is the Zulu Rikshas. The vehicles and their drivers are colourfully decorated. For a small fee, you can let yourself be pulled along
the beach promenade.
 
‐ Visit to DumaZulu Cultural Village. Experience traditional hospitality and learn about Zulu life at this informative and authentic
cultural village in Hluhluwe, KwaZulu‐Natal. The village is a living museum of traditional Zulu customs, crafts and way of life where
you can feel the beat of the Zulu drums and discover the rich cultural significance of the tribe's famous beadwork, weaving and
pottery. Zulu social values are integrated into their arts and crafts and their artefacts, besides being beautifully made, also hold
strong cultural significance. The village is the largest in South Africa and not only affords guests the opportunity to learn about the
rich history of tribal Africa but also serves as a means for the local community to continue practicing their rural ways of living as
well as earning an income from selling their crafts. Other interesting activities on offer include tastings of locally crafted beer and
dancing demonstrations.
 
‐ Lunch
 
‐ Afternoon 4x4 safari in the Hluhluwe reserve. For Big Five game viewing in extremely scenic surroundings, Hluhluwe Game Reserve
is hard to match. Proclaimed in 1897, This KwaZulu‐Natal game reserve north of Durban is one of the oldest in Africa. Hluhluwe
Game Reserve is world renowned for its role in Rhino conservation and the Hluhluwe‐Imfolozi Centenary Capture Centre sets a
benchmark for animal capture and sustainable utilization throughout Africa. This KwaZulu‐Natal Game Reserve stretches over 96
000 hectares of hilly topography and contains a diverse range of fauna and flora. This area was once the exclusive hunting ground
of  King  Shaka.  View  the  Big  Five  in  Hluhluwe  Game  Reserve  as  well  as  several  more  elusive  animals,  such  Wild  Dog,  Giraffe,
Cheetah and Nyala.
 
‐ Dinner: Zulu Nyala Game Lodge
 
‐ Night: Zulu Nyala Game Lodge (Standard Room ‐ Dinner, Bed and Breakfast)";}i:5;a:2:{s:5:"title";s:39:"Day 6: Hluhluwe ‐ Swaziland (249 kms)";s:4:"desc";s:950:" Breakfast
 
‐ The plains in the south‐western part of Swaziland contrast sharply with the mountainous landscapes of the north. You will discover
lush sugarcane fields alternating with bush savannah dotted with Swazi kraals. Near the southern border post of Lavumisa, the
Lubombo Mountains are very apparent from the road. This mountain range covers practically the entire eastern side of the country
and forms the natural border between Swaziland and Mozambique.
 
‐ Lunch Menu 2
 
‐ Visit  the  Swazi  candles  at  Malkerns.  On  weekdays  you  will  be  able  to  view  the  craftspeople  moulding  the  colourful  wax  into
fascinating  shapes  and  sizes.  These  delightful  candles  are  exported  across  the  world  and  have  become  very  popular  as  Swazi
mementos for friends and family back home.
 
‐ Dinner: Summerfield Luxury Resort
 
‐ Night: Summerfield Luxury Resort (Luxury Suite ‐ Bed and Breakfast)";}i:6;a:2:{s:5:"title";s:38:"Day 7:  Swaziland ‐ Kruger (331 kms)";s:4:"desc";s:2342:"Breakfast
 
‐ Visit  the  Ngwenya  Glass  factory.  Aside  from  providing  charming  glass  souvenirs  and  glassware,  this  enterprise  also  has  an
important social function. Entrepreneurs ranging from school children to adults collect discarded glass objects and bring them to
the factory in return for payment. An interesting way of combining environmental care with social upliftment!
 
‐ Arrival in Sabi Sands Game reserve, the most famous and well established of the private reserves, home to some of the finest safari
lodges in South Africa. The 650 square kilometres wildlife sanctuary forms part of the greater Kruger National Park area. The
reserve is the birthplace of sustainable wildlife tourism in Southern Africa, and is the oldest of all the private reserves in the country.
The Sabi Reserve was proclaimed in 1898. The history of today's Sabi Sands Reserve as a formal association dates back to 1948
when the landowners formed the private nature reserve, it is still largely owned and operated by 3rd & 4th generation families who
share a common vision with their ancestors. It has only ever been a wilderness area and is home to a vast wildlife population,
including  The  Big  Five,  and  a  number  of  endangered  species  such  as:  Wild  Dog,  Bats,  Honey  Badgers,  Oxpeckers  and  Ground
Hornbill. Open safari vehicles head out into the wild under the expert guidance of experienced rangers and insightful trackers. You
will be able to get in close contact with Africa's most exciting wildlife species, and will be presented with unparalleled photographic
opportunities. Early morning safaris head out as the sun rises and as the bush stirs to life. The ranger and tracker team searches for
animals by tracking their spoor, and by listening for the telltale signs of activity. Late afternoon safaris commence as the sun burns
its  way  towards  the  horizon.  The  animals  are  now  getting  more  active,  many  preparing  for  the  great  hunt  after  dark.  After
sundown, the drive continues with the aid of powerful spotlights which reveal the nocturnal species. (Activities included in your
package as per program below)
 
‐ Lunch
 
‐ Afternoon 4x4 Game Drive
 
‐ Dinner: Lion Sands : River Lodge
 
‐ Night: Lion Sands : River Lodge (Luxury Room ‐ Fully Inclusive)";}i:7;a:2:{s:5:"title";s:14:"Day 8:  Kruger";s:4:"desc";s:203:" Morning 4x4 Game drive
 
‐ Breakfast
 
‐ Lunch
 
‐ Afternoon 4x4 Game Drive
 
‐ Dinner: Lion Sands : River Lodge
 
‐ Night: Lion Sands : River Lodge (Luxury Room ‐ Fully Inclusive)";}i:8;a:2:{s:5:"title";s:41:"Day 9:  Kruger ‐ Johannesburg (522 kms)";s:4:"desc";s:2442:" Morning 4x4 Game drive
 
‐ Breakfast
 
‐ The Blyde River Canyon is the third deepest canyon in the world (after the Grand Canyon in the western U.S. and Namibia's Fish
River Canyon). It ranks as one of the most spectacular sights in Africa. The mountain scenery and panoramic views over the Klein
Drakensberg escarpment are quite spectacular and give the area its name of the Panorama Route. Viewpoints are named for the
vistas they offer, and God's Window and Wonder View hint at the magnitude of the scenery. The 'Pinnacle' is a single quartzite
column  rising  out  of  the  deep  wooded  canyon  and  the  Three  Rondavels  (also  called  'Three  Sisters')  are  three  huge  spirals  of
dolomite rock rising out of the far wall of the Blyde River Canyon. The entire canyon extends over twenty kilometres in length.
 
‐ Lunch
 
‐ Visit  Pilgrim's  Rest.  The  village  is  situated  on  the  magnificent  Panorama  Route  on  the  eastern  escarpment  region  of  the
Mpumalanga province. The entire town of Pilgrim's Rest has been declared a national monument in 1986. Here, visitors can relive
the days of the old Transvaal gold rush. Pilgrim's Rest was declared a gold field in 1873, soon after digger Alec "Wheelbarrow"
Patterson discovered gold deposits in Pilgrim's Creek. The Valley proved to be rich in gold and by the end of the year, there were
about 1500 diggers working in the area. As a result, Pilgrim's Rest became a social centre of the diggings. There is plenty to do and
experience here, from browsing through exciting curio and craft shops to discovering fascinating historical sights.
 
‐ Travel to Johannesburg
 
‐ Dinner: Carnivore Restaurant
A franchise of the world‐renowned Carnivore restaurant in Nairobi, Kenya, the restaurant is located in some natural surroundings
overlooking the Krugersdorp hills. Its main attraction is a large circular open fire with 52 converted Masaai tribal spears skewed
with a variety of meat, such as pork, lamb, beef, chicken, ribs and sausages, and also crocodile, zebra, giraffe, impala, ostrich and
kudu, roasting slowly and waiting for you to begin your feast! Waiters walk from the fire to your table holding spears skewed with
the meat and carve the cuts of your choice directly onto your plate until you have eaten your fill. Surely Africa's Greatest Eating
Experience.
 
‐ Night: 54 on Bath (Deluxe Room ‐ Bed and Breakfast)";}i:9;a:2:{s:5:"title";s:43:"Day 10: Johannesburg ‐ Pretoria (110 kms)";s:4:"desc";s:2118:"‐ Breakfast
 
‐ Visit the Apartheid Museum, dedicated to recounting the history of the rise and fall of apartheid in 20th century South Africa.
Interactive  multimedia  displays  recreate  the  experience  of  racial  segregation  with  exhibits  dedicated  to  historical  facts
accompanied by personal testimony : harrowing, unforgettable and uniquely South African. (No Guided tours on Mondays)
 
‐ Visit Soweto. Cursed child of apartheid, Johannesburg's South Western Township today comprises close to 4 million inhabitants.

The revolts of '76 that took place there marked the beginning of the decline of the segregation policy.
 
‐ Buffet lunch: Sakhumzi Restaurant 
Sakhumzi Restaurant is nestled in the heart of Soweto, in Orlando west's famous Vilakazi Street. The Street has a rich historical
background  that  boasts  two  Nobel  prize  winners:  Archbishop  Desmond  Mpilo  Tutu  and  former  president  Nelson  Mandela.
Previously  a  "Shebeen",  illegal  bar  during  Apartheid,  Sakhumzi  is  now  a  traditional  local  restaurant  that  specializes  in  typical
Sowetan cuisine. The buffet lunch ranges from pap(corn flour), rice, dumplings, vegetables, grilled chicken, stews and curries to
mogodu (tripe).
 
‐ Orientation tour of Pretoria, South Africa's capital. The city was founded in 1855 and became the capital of the Boer state, Zuid
Afrikaanse Republiek, 5 years later. Many reminders of the history of the Boers can still be seen whilst driving around the city; such
as  Church  Square  with  its  imposing  statue  of  Uncle  Paul  Kruger  ‐  ex‐president  of  the  ZAR  and  much  loved  Boer  leader.  The
Voortrekker Monument, rising like a sentinel in the south of Pretoria, is an essential piece of the puzzle in understanding the
Afrikaner people. The Union Buildings, which dominate the western side of the city, form the official seat of the South African
government, and feature a nine‐metre‐tall statue of the former president of South Africa, Nelson Mandela, erected shortly after his
death on 5 December 2013.
 
‐ End of our services";}}";}}}