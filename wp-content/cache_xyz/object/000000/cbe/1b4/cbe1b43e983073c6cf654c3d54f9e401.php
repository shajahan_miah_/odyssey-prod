��W<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:6409;s:11:"post_author";s:2:"19";s:9:"post_date";s:19:"2016-03-12 15:21:08";s:13:"post_date_gmt";s:19:"2016-03-12 20:21:08";s:12:"post_content";s:5449:"[vc_row][vc_column][st_image_effect st_type="title" st_image="6884" st_title="Safe Travel"][/st_image_effect][vc_column_text]
<h1 class="western"></h1>
Safe and secure travel on your safari is our highest priority. Our safaris have been carefully planned and researched by our local guides to safeguard against possible contingencies. Odyssey Safaris takes a number of important steps to preempt any possible problems and also provides additional measures in the event of an incident.

&nbsp;
<h2 class="western">Flying Doctors</h2>
Complimentary enrollment in this emergency medical service at no extra charge.

The Flying Doctors Service operated by AMREF is one of the most comprehensive air ambulance services in Africa, evacuating approximately 600 people annually. Flying Doctors are on call 24 hours a day, 365 days a year. Staff includes trauma doctors and nurses. The organization maintains 24-hour contact by direct radio, telephone, and fax between their radio room in Nairobi and 120 sites across East Africa. Flying Doctors has been operating for 40 years and owns and operates five aircraft. <a href="http://www.amref.org/">http://www.amref.org</a>
<h2 class="western">Current Health Information</h2>
Prior to your departure you will receive Odyssey Safaris pre-departure welcome packet which includes information about health issues for your travel in East Africa. We provide health checklists and questions to ask your doctor as well as up-to-date information from Centers for Disease Control about traveling in East Africa. Additionally, pre-departure briefings prepare you with safe practices that will maximize your safety and security. We give you detailed instructions on local laws, customs, and safari practices, so that you will know what to expect at all times. For added convenience, we give you phone and email contacts you can leave with friends and family at home to assure access if they need to reach you.

&nbsp;

&nbsp;
<h2 class="western">Security Awareness</h2>
Kenya's tourism industry is a major part of its economy and the government goes out of its way to protect tourists and tourist sites. Odyssey Safaris is in contact with police both locally and nationally, the Kenyan government security services, and the US Embassy. In the event of an incident ranging from a political protest to something more serious we are notified by these services and communicate with our safari drivers via the vehicle radios and cell phones to ensure the safety of our guests. We also assist you in enrolling in the Smart Traveler Program via the US Embassy which keeps travelers informed of security developments.

&nbsp;
<h2 class="western">Reliable Vehicles</h2>
Odyssey Safaris provides 4x4 Toyota Safari Land Cruisers for all our safaris regardless of budget (no mini-buses). Our vehicles are rigorously maintained, inspected, cleaned, and tested before and after every safari. All vehicles are equipped with 2 spare tires, a 1st aid kit, and a radio to communicate with our main office, park rangers, base camp, Flying Doctors, and the Kenyan police. These are spacious, comfortable vehicles capable of challenging off road terrain while still large enough to guarantee our guests a window seat.

&nbsp;
<h2 class="western">Local Guides</h2>
Odyssey Safaris guides are all native Kenyans and Tanzanians fluent in both English and Swahili with a minimum 12 years safari experience; many speak 4 languages including their tribal language and either French or German. Odyssey Safaris guides intimate knowledge of local customs, history, and context will enlighten your cultural experience as well as keep your safari trouble free; their experience will keep you safe on safari.

&nbsp;
<h2 class="western"></h2>
<h2 class="western"></h2>
<h2 class="western">Animal Knowledge</h2>
&nbsp;

Odyssey Safaris guides are your personal naturalists, keeping your informed and entertained when sighting species but also safe and secure. Many of Africa's species can present a threat to humans, but usually only when provoked. Our guides are experienced with every species you'll see on safari and understand how to view them safely while providing the most exciting safari experience possible. Our guides understanding of animal safety not only in the vehicle but at the campsite will ensure you a safe and enjoyable safari.

&nbsp;
<h2 class="western">Bottled Water, Hygienic Food Prep &amp; Complimentary Bottled Water Daily</h2>
Waterborne diseases present the greatest source of discomfort to travelers. Odyssey Safaris provides complimentary bottled water for its guests and uses it for all food preparation. We also inspect the kitchens of the lodges and camps used by our guests to ensure the highest standards of food safety.

&nbsp;
<h2 class="western">Mountain Safety</h2>
High altitude trekking in East Africa presents unique challenges to the hiker. Odyssey Safaris mountain guides are all NOLS East Africa graduates with experience on both Mt. Kenya and Mt. Kilimanjaro. Their knowledge of these distinct mountain environments and the ever changing weather conditions is essential for a positive experience. Your mountain crew is equipped with radios and cell phones for communication amongst the group and with mountain rangers. They are all trained in 1st aid and carry a medical bag for emergencies.[/vc_column_text][/vc_column][/vc_row][vc_row][vc_column][/vc_column][/vc_row]";s:10:"post_title";s:11:"Safe Travel";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:11:"safe-travel";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2016-06-04 23:30:26";s:17:"post_modified_gmt";s:19:"2016-06-05 03:30:26";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:44:"https://www.odysseysafaris.com/?page_id=6409";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}