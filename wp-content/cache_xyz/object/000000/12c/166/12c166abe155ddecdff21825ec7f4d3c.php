JʩW<?php exit; ?>a:1:{s:7:"content";a:1:{i:0;O:8:"stdClass":4:{s:7:"meta_id";s:5:"13095";s:7:"post_id";s:4:"5836";s:8:"meta_key";s:13:"tours_program";s:10:"meta_value";s:2472:"a:1:{i:0;a:2:{s:5:"title";s:26:"Sheldrick Animal Orphanage";s:4:"desc";s:2389:"Located in a secluded area of the Nairobi National Park the orphanage cares for baby elephants and other orphaned species with a dedicated team of conservationists. The orphanage is run by Daphne Sheldrick, the wife of the late famous Naturalist, David William Sheldrick who was the founder Warden of Tsavo East National Park in Kenya from its inception in 1948 to 1976.
<br /><br />

 

At 11:00 am, the baby animals are brought from the National Park for a mud-bath at the orphanage, where for an hour you can get close to them and sometimes touch them.

 <br /><br />

You may not feed the animals but will get a chance to view the handlers feeding them. The Private Session strictly between 5:00 pm to 6:00 pm each day and allows you to see and interact with the animals in private; advance booking required.

 <br /><br />

The drive to the orphanage takes approximately 45 minutes and is open to the public for only one hour a day from 11:00 am – 12:00 pm.

 <br /><br />

In addition, one of the conservationists will give a talk about the elephants and where they came from, how they managing at the orphanage and how some of the previous orphans are progressing.  During this talk you are observing the feeding, play and bathing of the elephants and other orphaned species.
<br /><br />
 

Price:  $40 per person

Includes:  Transport to and from orphanage, transport to and from lunch location of your choice (if necessary/desired, lunch additional), entry fee to orphanage.

<br /><br /> 

Private Showing - Adopting a Baby Elephant For a Year
It's hard not to be touched when you see the orphans, and the dedication and hard work demanded of the conservationists to keep them happy and healthy is inspiring.  Feeding them every three hours around the clock, keeping them warm and playing with them, requires huge efforts and of course money.   Starting at $50, you can adopt an orphan, and the money goes directly to the project.  You receive regular updates on your orphan via e-mail, as well as a copy of his biography, an adoption certificate, a water color painting of the orphan, and most importantly -- the knowledge that you have made a difference. Once you adopt, you may also make an appointment to see your baby when it goes to bed without the crowds during the public showing.

Entry fee and adoption required for the private showing.";}}";}}}