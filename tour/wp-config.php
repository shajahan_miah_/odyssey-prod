<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'odyssey_tour');

/** MySQL database username */
define('DB_USER', 'odyssey_root');

/** MySQL database password */
define('DB_PASSWORD', 'ypRGhrN;7tM}');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Sv`b|DT%Z*R7B,xx`}Ma~+93V(&U3x4wqA<e&ZJbQP4}=+Ok1!&Cv5t=2Eb?MaK2');
define('SECURE_AUTH_KEY',  'bDg^$6(<;-`!eg]`lqN 4S<!Ng+Rt|+DpnS05MXi(-2ILzzKmE|N/3u]6B$Hr>RA');
define('LOGGED_IN_KEY',    ',lt)=k|eYl.x1e$,rxe#|?o6^C8i|sJ.eD+sW+LYH7|/JN3W^[NZ9mK/L^Ar~cT2');
define('NONCE_KEY',        'Lk){v-4Gl kLF2.!d=_(]S5IItpLo(D0+|c~Nsc}W+-z0%}@IJQ[~yWsKGEYI(YL');
define('AUTH_SALT',        'fe*-pf5-=@O+ChJ`Aj.R3u~(8sO|L@VfzId2_UZY_!KJL.=-+r=7p>$0oL2bWj?k');
define('SECURE_AUTH_SALT', 'E|I:K&bvPetk8lFfW~RbBe?0{S#SXm[0-E#J&V[:zNM+T<yn0<iuQzsD+{YJ%T)1');
define('LOGGED_IN_SALT',   '>,i>tN2u}%)M[g@lgM5.1,y)QAw6aC!h-;kMw_I =d=Z>C6Nn@D%|o:6 e@oo.-d');
define('NONCE_SALT',       'Kzun[/i2BK^B1]M7|e3$H-Q=z-;0du@gWnHN#nEv|z<B]*H2.%cVTk9-|x:*18_N');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
