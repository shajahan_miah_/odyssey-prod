<?php
/**
 * @package WordPress
 * @subpackage Traveler
 * @since 1.0
 *
 * function
 *
 * Created by ShineTheme
 *
 */

if(!defined('ST_TEXTDOMAIN'))
define ('ST_TEXTDOMAIN','traveler');

$status=load_theme_textdomain(ST_TEXTDOMAIN,get_template_directory().'/language');


//require get_template_directory().'/inc/class.traveler.php';

get_template_part('inc/class.traveler');

st();
get_template_part('demo/demo_functions');
if ( ! isset( $content_width ) ) $content_width = 900;

?><?php 
add_action( 'wpcf7_before_send_mail', 'my_conversion' );
function my_conversion( $cf7 )
{
	$submission = WPCF7_Submission::get_instance();

if ( $submission ) {
  $email = $submission->get_posted_data("your-email");
  $first_name  = $submission->get_posted_data("your-name");
  $last_name  =$submission->get_posted_data("your-lastname");
  $phone = $submission->get_posted_data("your-phone");
  $company ="odysseysafaris.com";
  $comments  = $submission->get_posted_data("your-message");
  $city  = $submission->get_posted_data("city");
  $state =$submission->get_posted_data("StateProvince");
  $country =$submission->get_posted_data("Country");
  $destination =$submission->get_posted_data("Destination");
  $SafariType =$submission->get_posted_data("SafariType");
  $Nd00000055KT2 =$submission->get_posted_data("AirFare");
  $Nd00000053S7C =$submission->get_posted_data("HereAboutUs");
  $Nd00000053S0k =$submission->get_posted_data("NewsletterOffers");
  $Nd00000053S5F =$submission->get_posted_data("Budget");
  $Nd00000053S5U = $submission->get_posted_data("LengthofTravel");
  $Nd00000053S8j = $submission->get_posted_data("NumberofTravelers");
  $Nd00000053S5t = $submission->get_posted_data("TravelTime");
  $title = "odysseySafari";
 
  $post_items[] = 'oid=00Dd0000000f6Jg';
  $post_items[] = 'first_name=' . $first_name;
  $post_items[] = 'last_name=' . $last_name;
  $post_items[] = 'email=' . $email;
  $post_items[] = 'phone=' . $phone;
  $post_items[] = 'company=' . $company;
  $post_items[] = 'description=' . $comments;
  $post_items[] = 'city=' . $city;
  $post_items[] = 'state=' . $state;
  $post_items[] = 'country=' . $country;
  $post_items[] = 'destination=' . $destination;
  $post_items[] = '00Nd00000053S5K=' . $SafariType;
  $post_items[] = '00Nd00000053S5F=' . $Nd00000053S5F;
  $post_items[] = '00Nd00000055KT2=' . $Nd00000055KT2;
  $post_items[] = '00Nd00000053S7C=' . $Nd00000053S7C;
  $post_items[] = '00Nd00000053S0k=' . $Nd00000053S0k;
  $post_items[] = '00Nd00000053S5U=' . $Nd00000053S5U;
  $post_items[] = '00Nd00000053S8j=' . $Nd00000053S8j;
  $post_items[] = '00Nd00000053S5t=' . $Nd00000053S5t;
  
  $post_items[] ='title=' . $title;
}
  if(!empty($first_name)  && !empty($email) )
  {
    $post_string = implode ('&', $post_items);
    // Create a new cURL resource
    $ch = curl_init();
 
    if (curl_error($ch) != "")
    {
      // error handling
    }
 
    $con_url = 'https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8';
    curl_setopt($ch, CURLOPT_URL, $con_url);
    // Set the method to POST
    curl_setopt($ch, CURLOPT_POST, 1);
    // Pass POST data
    curl_setopt( $ch, CURLOPT_POSTFIELDS, $post_string);
    curl_exec($ch); // Post to Salesforce
    curl_close($ch); // close cURL resource
  }
}

add_filter( 'rewrite_rules_array', 'lab_make_sub_page' );
function lab_make_sub_page($rules){

    $new_rule = array(
        'safari-special-offers\.html$' 		=> 'index.php?pagename=splashnew&postname=special-offers',
        'scheduled-departure-safaris\.html$' 	=> 'index.php?pagename=splashnew&postname=scheduled-departure',
        'short-safari-and-day-tours\.html$' 	=> 'index.php?pagename=splashnew&postname=day-tours-and',
        'wildlife-safaris\.html$' 		=> 'index.php?pagename=splashnew&postname=wildlife',
        'family-safaris.html$' 			=> 'index.php?pagename=splashnew&postname=family',
        'luxury-safaris.html$' 			=> 'index.php?pagename=splashnew&postname=luxury',
        'mountaineering-safaris.html$' 		=> 'index.php?pagename=splashnew&postname=mountaineering',
        'kenya-safaris.html$' 			=> 'index.php?pagename=splashnew&con=kenya',
        'tanzania-safaris.html$' 		=> 'index.php?pagename=splashnew&con=tanzania',
        'south-africa-safaris.html$' 		=> 'index.php?pagename=splashnew&con=South',
    );
    $rules = array_merge( $new_rule, $rules );
    return $rules;
}
add_filter( 'query_vars', 'add_lab_query_vars' );
function add_lab_query_vars( $vars ){
    $vars[] = "postname";
    $vars[] = "con";
    return $vars;
}
// REMOVE - show template name
// add_action('wp_head', 'show_template');
  //function show_template() {
  //global $template;
 //global $current_user;
//get_currentuserinfo();
 //if ($current_user->user_level == 10 ) print_r($template);
  //}
// REMOVE - end show template name
?>