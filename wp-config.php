<?php
/**** Enable W3 Total Cache Edge Mode **/
define('W3TC_EDGE_MODE', true); // Added by W3 Total Cache



/***
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //

define('WP_HOME','http://odysseydev.odysseysafaris.com');
define('WP_SITEURL','http://odysseydev.odysseysafaris.com');


/** The name of the database for WordPress */
define('DB_NAME', 'odyssey_prod');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'qwer12#$');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'LwNQX!N~gLMLB+/@Y=&-!iXt{]ld/RbT>?my4qeY2k1!-D[Ml3J:M9|~X6&3^5l+');
define('SECURE_AUTH_KEY',  ')>cTaL&r+eM?FB4r+Gq6|kRi-Ow+iII<w|xuk-d]-CR+W]*YJ]@TAT0y@{#8l.$a');
define('LOGGED_IN_KEY',    '9[+7ZpiV]dRS $|%KVKhA@L*7kb/+%jY;xs+p/.}T{Q!0?FkjYUIN~%IzJ8<LLXv');
define('NONCE_KEY',        'Wb :yH~o2Z]Gki=ubsK8.RK>,ZDLMD-e$7XB+UQX$l?/meIpfq-z0EsmCN_wV~{4');
define('AUTH_SALT',        'mLGm87~tqFAZZYb;(|XYD&wrmZJOyP{1OZ5Rquas*5|Qi-3zMNt PM#-,%rxR`N;');
define('SECURE_AUTH_SALT', 'H6~aP8a)IAh#%86.hJ^I1F!v^6+:@7?6g[>^u1IMy!aoRo;c+lijs:G{ncaqtUx/');
define('LOGGED_IN_SALT',   '}+5/ |AY&gBaxQ-T(R!#paCCr0-[F`+Q-`{J!DP]`jWJH[|L[}8OBjn(e%1+#kVA');
define('NONCE_SALT',       'F{$S+L@.aF,(z|f{ k)PWPudns,To*2hAepu[Z(?[6{<kgSRj.j4Xjtfnv[6RhzW');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
